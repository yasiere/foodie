<?php
session_start();
if(!empty($_SESSION['ak_id'])){
include "inc/db.php";
include "inc/tgl_indo.php";
include 'inc/user.php';
include 'inc/seo.php';

?>
<?php
  function cutText($text, $length, $mode = 2){
  	if ($mode != 1)	{
  		$char = $text{$length - 1};
  		switch($mode)		{
  			case 2:
  				while($char != ' ') {
  					$char = $text{--$length};
  				}
  			case 3:
  				while($char != ' ') {
  					$char = $text{
              ++$num_char
            };
  				}
  		}
  	}
  	return substr($text, 0, $length);
  }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo $base ?>/assets/img/favicon/apple-touch-icon-57x57.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $base ?>/assets/img/favicon/apple-touch-icon-114x114.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $base ?>/assets/img/favicon/apple-touch-icon-72x72.png" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $base ?>/assets/img/favicon/apple-touch-icon-144x144.png" />
        <link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo $base ?>/assets/img/favicon/apple-touch-icon-60x60.png" />
        <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo $base ?>/assets/img/favicon/apple-touch-icon-120x120.png" />
        <link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo $base ?>/assets/img/favicon/apple-touch-icon-76x76.png" />
        <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo $base ?>/assets/img/favicon/apple-touch-icon-152x152.png" />
        <link rel="icon" type="image/png" href="<?php echo $base ?>/assets/img/favicon/favicon-196x196.png" sizes="196x196" />
        <link rel="icon" type="image/png" href="<?php echo $base ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96" />
        <link rel="icon" type="image/png" href="<?php echo $base ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32" />
        <link rel="icon" type="image/png" href="<?php echo $base ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16" />
        <link rel="icon" type="image/png" href="<?php echo $base ?>/assets/img/favicon/favicon-128.png" sizes="128x128" />
        <link rel="shortcut icon" href="<?php echo $base ?>/assets/img/favicon/favicon.ico" type="image/x-icon" />

        <meta name="application-name" content="&nbsp;" />
        <meta name="msapplication-TileColor" content="#FFFFFF" />
        <meta name="msapplication-TileImage" content="<?php echo $base ?>/assets/img/favicon/mstile-144x144.png" />
        <meta name="msapplication-square70x70logo" content="<?php echo $base ?>/assets/img/favicon/mstile-70x70.png" />
        <meta name="msapplication-square150x150logo" content="<?php echo $base ?>/assets/img/favicon/mstile-150x150.png" />
        <meta name="msapplication-wide310x150logo" content="<?php echo $base ?>/assets/img/favicon/mstile-310x150.png" />
        <meta name="msapplication-square310x310logo" content="<?php echo $base ?>/assets/img/favicon/mstile-310x310.png" />

        <title>CMS - Foodieguidances</title>
        <link href="css/style.default.css" rel="stylesheet">
        <link href="css/select2.css" rel="stylesheet" />
        <link href="css/bootstrap-timepicker.css" rel="stylesheet">
        <link href="css/bootstrap.min.css" rel="stylesheet" />
        <script src="js/jquery-1.10.2.js"></script>
        <link href="css/bootstrap-wysihtml5.css" rel="stylesheet" />
        <link href="css/dataTables.css" rel="stylesheet">
        <script src="ckeditor/ckeditor.js"></script>
        <link href="css/dataTables.responsive.css" rel="stylesheet">
        <link href="css/wickedpicker.css" rel="stylesheet">

        <style>
        .upload {
            width: 137px;
            min-height: 110px;
            height: auto;
            margin-top: 10px;
            margin-bottom: 10px;
            background: url("https://www.foodieguidances.com/assets/css/../img/theme/bg_upload.jpg") no-repeat;
          }
          div.accordion {
              background-color: #ffeed0;
              color: #444;
              cursor: pointer;
              padding: 18px;
              width: 100%;
              border: none;
              text-align: left;
              outline: none;
              font-size: 15px;
              transition: 0.4s; margin-bottom: 15px;
          }

          div.accordion.active, div.accordion:hover {
              background-color: #ffe1ad;
          }

          div.accordion:after {
              content: '\02795';
              font-size: 13px;
              color: #777;
              float: right;
              margin-left: 5px;
          }

          div.accordion.active:after {
              content: "\2796";
          }

          div.panels {
              padding: 0 18px;
              background-color: white;
              max-height: 0;
              overflow: hidden;
              transition: 0.6s ease-in-out;

          }

          div.panels.show {
              opacity: 1;
              max-height: 5000px;
          }
        </style>

    </head>
    <body>
		<?php
			include "inc/header.php";
			$mod=$_GET['mod'];
			if($_GET['ale']==1){
				include "mod/$mod/konten.php";
			}
      elseif($_GET['ale']==3){
        include "mod/$mod/print.php";
      }
			else{
				echo $mod;
				include "mod/$mod/form.php";
			}
		?>
    </body>
</html>






<?php
}
else{
	header('location:not-found.htm');
}
?>

<script src="js/jquery-1.10.2.js"></script>
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/pace.min.js"></script>
<script src="js/retina.min.js"></script>
<script src="js/jquery.cookies.js"></script>

<script src="js/select2.min.js"></script>
<script src="js/bootstrap-timepicker.min.js"></script>
<script src="js/bootstrap.file-input.js"></script>
<script src="js/custom.js"></script>
<script type="text/javascript" src="js/wickedpicker.js"></script>
<script src="js/jasny-bootstrap.min.js"></script>



<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/plug-ins/725b2a2115b/integration/bootstrap/3/dataTables.bootstrap.js"></script>
<!-- <script src="//cdn.datatables.net/responsive/1.0.1/js/dataTables.responsive.js"></script> -->



<script src="js/bootstrap-rating.min.js"></script>
<script src="js/iscroll.min.js"></script>
<script src="js/drawer.min.js" charset="utf-8"></script>

<link href="css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script>
  $('.timae').timepicker({
    defaultTime:''
  });
</script>
<script src="<?php echo"$base"; ?>/assets/js/select2.min.js"></script>
<script src="<?php echo"$base"; ?>/assets/js/jquery.validate.min.js"></script>
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        this.nextElementSibling.classList.toggle("show");
  }
}
</script>


<script>
$(document).ready(function(){
	$('#aledata').dataTable({
		"sPaginationType": "full_numbers",
		"iDisplayLength": 25,
		"aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, "Semua"]]
    });
});
</script>
