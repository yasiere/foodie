<section>
    <div class="mainwrapper">
        <?php include "inc/kiri.php"; ?>
        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="pageicon pull-left">
                        <i class="fa fa-tags"></i>
                    </div>
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href="mod-beranda-1.htm"><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="mod-beranda-1.htm">Beranda</a></li>
                            <li>Informasi Kategori</li>
                        </ul>
                        <h4>Informasi Kategori</h4>
                    </div>
                </div><!-- media -->
            </div><!-- pageheader -->

            <div class="contentpanel">
				<?php
				if(isset($_GET['info'])){
					if($_GET['info']==1){
						echo"<div class='alert alert-success'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Sukses!</strong> data berhasil dihapus.
						</div>";
					}
					elseif($_GET['info']==2){
						echo"<div class='alert alert-danger'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Banner Depan Max 3 Kategori</strong>
						</div>";
					}
				}
				?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                                <div class="col-sm-11">
                                    <h4 class="panel-title">Daftar Kategori</h4>
                                    <p><a href="add-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-primary">Tambah Data</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            
                            <table id="aledataa" class="table table-striped table-bordered responsive">
                                <thead class="">
                                    <tr>
                                        <th>No</th>
                                        <th>Data</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td width="50px" align="center">1</td>
                                        <td>Air Conditioning</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-air_conditioning-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="50px" align="center">2</td>
                                        <td>Ambience</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-ambience-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr> 
                                    <tr>
                                        <td width="50px" align="center">3</td>
                                        <td>Attire</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-attire-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr> 
                                    <tr>
                                        <td width="50px" align="center">4</td>
                                        <td>Beverage Category</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-beverage_category-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr> 
                                    <tr>
                                        <td width="50px" align="center">5</td>
                                        <td>Clean Washroom</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-clean_washroom-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr> 
                                    <tr>
                                        <td width="50px" align="center">6</td>
                                        <td>Cooking Methode</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-cooking_methode-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr> 
                                    <tr>
                                        <td width="50px" align="center">7</td>
                                        <td>Cuisine</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-cuisine-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr> 
                                    <tr>
                                        <td width="50px" align="center">8</td>
                                        <td>Difficulty</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-difficulty-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr> 
                                    <tr>
                                        <td width="50px" align="center">9</td>
                                        <td>Duration</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-duration-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr> 
                                    <tr>
                                        <td width="50px" align="center">10</td>
                                        <td>Facility</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-facility-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="50px" align="center">11</td>
                                        <td>Food Category</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-food_category-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="50px" align="center">12</td>
                                        <td>Heating System</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-heating_system-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="50px" align="center">13</td>
                                        <td>Landmark</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-landmark-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="50px" align="center">14</td>
                                        <td>Making Methode</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-making_methode-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="50px" align="center">15</td>
                                        <td>Mall</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-mall-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="50px" align="center">16</td>
                                        <td>Msg Level</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-msg_level-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="50px" align="center">17</td>
                                        <td>Noise Level</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-noise_level-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="50px" align="center">18</td>
                                        <td>Operation Hour</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-operation_hour-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="50px" align="center">19</td>
                                        <td>Parking Spaces</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-parking_spaces-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="50px" align="center">20</td>
                                        <td>Photo Category</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-photo_category-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="50px" align="center">21</td>
                                        <td>Premise Fire Safety</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-premise_fire_safety-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="50px" align="center">22</td>
                                        <td>Premise Hygiene</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-premise_hygiene-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="50px" align="center">23</td>
                                        <td>Premise Maintenance</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-premise_maintenance-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="50px" align="center">24</td>
                                        <td>Premise Security</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-premise_security-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="50px" align="center">25</td>
                                        <td>Price Index</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-price_index-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="50px" align="center">26</td>
                                        <td>Recipe Category</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-recipe_category-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="50px" align="center">27</td>
                                        <td>Serving</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-serving-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="50px" align="center">28</td>
                                        <td>Serving Pax</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-serving_pax-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="50px" align="center">29</td>
                                        <td>Serving Time</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-serving_time-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="50px" align="center">29</td>
                                        <td>Suitable For</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-suitable_for-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="50px" align="center">30</td>
                                        <td>Tables Availability</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-tables_availability-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="50px" align="center">31</td>
                                        <td>Term of Payment</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-term_of_payment-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="50px" align="center">32</td>
                                        <td>Type of Business</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-type_of_business-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="50px" align="center">33</td>
                                        <td>Type of Service</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-type_of_service-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="50px" align="center">34</td>
                                        <td>Type of Serving</td>
                                        <td width="50px" align="center"><?php echo "<a href='edit-$mod-type_of_serving-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>"; ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                </div><!-- panel -->
            </div><!-- contentpanel -->
        </div><!-- mainpanel -->
    </div><!-- mainwrapper -->
</section>

<!-- <script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/pace.min.js"></script>
<script src="js/retina.min.js"></script>
<script src="js/jquery.cookies.js"></script>

<script src="js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/plug-ins/725b2a2115b/integration/bootstrap/3/dataTables.bootstrap.js"></script>
<script src="//cdn.datatables.net/responsive/1.0.1/js/dataTables.responsive.js"></script>
<script src="js/custom.js"></script> -->
<script>
jQuery(document).ready(function(){
	jQuery('#aledataa').dataTable({
		"sPaginationType": "full_numbers",
		"iDisplayLength": 25,
		"aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, "Semua"]]
    });
});
</script>
