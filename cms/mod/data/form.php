<section>
    <div class="mainwrapper">
        <?php include "inc/kiri.php"; ?>
         <div class="mainpanel">
             <div class="pageheader">
                <div class="media">
                    <div class="pageicon pull-left">
                        <i class="fa fa-home"></i>
                    </div>
                    <div class="media-body">
						<ul class="breadcrumb">
                            <li><a href="mod-beranda.htm"><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="mod-beranda.htm">Beranda</a></li>
                            <li>Informasi Kategori</li>
                        </ul>
                        <h4>Informasi Kategori</h4>
                    </div>
                </div><!-- media -->
            </div><!-- pageheader -->
            <div class="contentpanel">
				<?php if($_GET['proses']==1){ ?>
				<form action="mod/<?php echo"$mod/aksi.php?mod=$mod&ale=2&url=$_GET[url]"; ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data">
				<?php
				if(isset($_GET['info'])){
					if($_GET['info']==1){
						echo"<div class='alert alert-success'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Sukses!</strong> data berhasil disimpan.
						</div>";
					}
					elseif($_GET['info']==2){
						echo"<div class='alert alert-danger'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Kesalahan!</strong> tipe file tidak cocok, anda harus mengupload file berekstensi *.JPG.
						</div>";
					}
					else{
						echo"<div class='alert alert-danger'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Kesalahan!</strong> ukuran file terlalu besar, ukuran maksimal yang diperbolehkan adalah 300kb.
						</div>";
					}
				}
				?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-btns">
                            <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                        </div><!-- panel-btns -->
						<h4 class="panel-title">Data Kategori</h4>
                        <p>Silahkan lengkapi data dibawah ini.</p>
                    </div>
                    <div class="panel-body nopadding">
                       <div class="form-group">
                           <label class="col-sm-2 control-label">Nama Kategori :</label>
                           <div class="col-sm-10">
                               <input type="text" name="nama_kategori" class="form-control" required>
                           </div>
                       </div>
                       <div class="form-group">
                           <label class="col-sm-2 control-label">Detil Kategori:</label>
                           <div class="col-sm-10">
                               <textarea rows="10" class="form-control ckeditor" name="detil_kategori"></textarea>
                           </div>
                       </div>
                       <div class="form-group">
                            <label class="col-sm-2 control-label">Gambar:</label>
                        	<div class="col-sm-10">
							    <input type="file" name="fupload" title="Pilih" class="btn btn-primary"> <br> <input type="text" name="alt_kategori" class="form-control" placeholder="alt Gambar...">
                            </div>
                        </div>
						<!-- form-group -->
                    </div><!-- panel-body -->
                </div><!-- panel -->
				<div class="panel panel-default">
					<div class="panel-footer">
                        <button class="btn btn-primary mr5">Simpan</button>
                        <a href="mod-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-warning">Kembali</a>
                     </div><!-- panel-footer -->
                </div><!-- panel -->
				</form>
				<?php
				}


				else{
				
				?>
				
				<?php
				if(isset($_GET['info'])){
					if($_GET['info']==1){
						echo"<div class='alert alert-success'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Sukses!</strong> data berhasil disimpan.
						</div>";
					}
					elseif($_GET['info']==2){
						echo"<div class='alert alert-danger'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Kesalahan!</strong> tipe file tidak cocok, anda harus mengupload file berekstensi *.JPG.
						</div>";
					}
					else{
						echo"<div class='alert alert-danger'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Kesalahan!</strong> ukuran file terlalu besar, ukuran maksimal yang diperbolehkan adalah 300kb.
						</div>";
					}
				}
				?>
                <div class="panel panel-default">
                	<form action="mod/<?php echo"$mod/aksi.php?mod=$mod&ale=3&url=$_GET[url]&proses=$_GET[id]"; ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data">

                    <div class="panel-heading">
                        <div class="panel-btns">
                            <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                        </div><!-- panel-btns -->
						<h4 class="panel-title">Detail Data Kategori</h4>
                        <p>Silahkan lengkapi data dibawah ini.</p>
                    </div>
                    <div class="panel-body nopadding">
                    	<table id="aledataa" class="tbl1 table table-striped table-bordered responsive">
                    		<thead>
                    			<tr>
									<th width="1%"> No.</th>
									<?php
										if ($_GET['id'] == 'mall') {
									?>	
									<th width="20%">City</th>	
									<?php } ?>
									<th width="99%">
										Data
									</th>
								</tr>
                    		</thead>                    		
							<tbody>
									<?php 
										$dat=mysqli_query($koneksi,"select * from $_GET[id] order by nama_$_GET[id] asc");
										$val = "nama_".$_GET['id'];
										$idval = "id_".$_GET['id'];
										$no = 1;
										while ($d=mysqli_fetch_array($dat)) {
			                        ?>
			                        <tr>
										<td> <?php echo $no; ?></td>
										<?php 
											if ($_GET['id'] == 'mall') { 

												$mall = mysqli_fetch_array(mysqli_query($koneksi, "SELECT * FROM negara n LEFT join propinsi p on n.id_negara=p.id_negara LEFT JOIN kota k on p.id_propinsi=k.id_propinsi WHERE id_kota = $d[id_kota]"));
										?>	
										<td>
											<select class="select2 form-control" name="kotaku[]" data-kode='<?php echo"$d[$idval]"; ?>'>
												<option value="<?php echo"$mall[id_kota]"; ?>"><?php echo"$mall[nama_negara]"; ?> / <?php echo"$mall[nama_propinsi]"; ?> / <?php echo"$mall[nama_kota]"; ?></option>
											</select>
											<!-- <input type="text" name="kota" class="form-control kode" data-kode='<?php echo"$d[$idval]"; ?>'>
											<div class="auto<?php echo"$d[$idval]"; ?>"></div> -->
										</td>
										<?php } ?>
										<td>
											<input type="hidden" name="id[]" class="form-control" value="<?php echo"$d[$idval]"; ?>">
											<input style="width: 100%" type="text" name="nama[]" class="form-control col-sm-12" value="<?php echo"$d[$val]"; ?>">
										</td>
									</tr>
								    
			                        <?php $no++;} ?>
		                        
                        	</tbody>
						</table>
						<div class="col-sm-12"><br><br>
							<a href="javascript:void(0);" class="btn btn-success" id="anc_add">Add Row</a> 
							<a href="javascript:void(0);" class="btn btn-danger" id="anc_rem">Remove Row</a>
						</div>
                    </div><!-- panel-body -->
                </div><!-- panel -->
				<div class="panel panel-default">
					<div class="panel-footer">
                        <button class="btn btn-primary mr5">Simpan</button>
                        <a href="mod-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-warning">Kembali</a>
                     </div><!-- panel-footer -->
                </div><!-- panel -->
				
				<?php } ?>
				</form>
			</div><!-- contentpanel -->
        </div><!-- mainpanel -->
    </div><!-- mainwrapper -->
</section>

<!-- <script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/pace.min.js"></script>
<script src="js/retina.min.js"></script>
<script src="js/jquery.cookies.js"></script>

<script src="js/select2.min.js"></script>
<script src="js/bootstrap-timepicker.min.js"></script>
<script src="js/bootstrap.file-input.js"></script>
<script src="js/custom.js"></script>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
<script type="text/javascript">
  $(function(){
       $(".select2").select2({
           minimumInputLength: 3,
           allowClear: true,
           placeholder: 'Masukkan Nama Kota',
           ajax: {
              dataType: 'json',
              url: 'mod/data/data.php',
              delay: 800,
              data: function(params) {
                return {
                  search: params.term
                }
              },
              processResults: function (data, page) {
              return {
                results: data 
                              
              };
            },
          }
      }).on('select2:select', function (evt) {
        var data = $(".select2 option:selected").text();
      });

 });

jQuery(document).ready(function(){
	jQuery('#aledataa').dataTable({
		"sPaginationType": "full_numbers",
		"iDisplayLength": 25,
		"aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, "Semua"]]
    });
});
$(document).ready(function() {


  $(".pilih").select2();


 //    $(".kode").keyup(function() {
	// 	var kode = $(this).attr('data-kode');
	// 	var id = $(this).val();

	// 	$.getJSON('mod/<?php echo"$mod/data.php"; ?>', {
	// 		action: 'getkode',
	// 		data: $(this).val()
	// 	}, function(json) {
	// 		$.each(json, function(index, row) {
	// 			// $('.tanggal[data-kode="' + kode + '"]').val(row.MS_TANGGAL);
	// 			// $('.berat[data-kode="' + kode + '"]').val(row.MS_BERAT);
	// 			// $('.money[data-kode="' + kode + '"]').mask('#.##0', {
	// 			// 	reverse: true
	// 			// });
	// 			alert(row.nama_kota);
	// 		});
	// 	});
	// });
});



</script>
<script>
$(document).ready(function() {
	var cnt = 2;
	$(document).on("click", "#anc_add", function() {
		<?php if ($_GET['id'] == 'mall') { ?>
		 	$('.tbl1 tr').last().after('<tr class="dr"><td></td><td><select class="select2 form-control col-sm-12" style="width: 100%" name="nama_kotaku[]"></select></td><td><input type="text" name="nama_input[]" style="width: 100%" class="form-control col-sm-12" value=""></td></tr>');
		<?php } else{ ?>
			$('.tbl1 tr').last().after('<tr class="dr"><td></td><td><input type="text" name="nama_input[]" class="form-control" value=""></td></tr>');
		<?php } ?>

		cnt++;

		$(".select2").select2({
           minimumInputLength: 3,
           allowClear: true,
           placeholder: 'Masukkan Nama Kota',
           ajax: {
              dataType: 'json',
              url: 'mod/data/data.php',
              delay: 800,
              data: function(params) {
                return {
                  search: params.term
                }
              },
              processResults: function (data, page) {
              return {
                results: data 
                              
              };
            },
          }
      }).on('select2:select', function (evt) {
        var data = $(".select2 option:selected").text();
      });

	});
	$("#anc_rem").click(function() {
		if ($('.tbl1 tr').size() > 1) {
			$('.tbl1 tr.dr:last-child').remove();
		} else {
			alert('One row should be present in table');
		}
	});
});
</script>
<script>
jQuery(document).ready(function() {
	$('input[type=file]').bootstrapFileInput();
	jQuery('.select-search-hide').select2({
        minimumResultsForSearch: -1
    });
	// jQuery('#cekin').timepicker({
	// 	showMeridian: false,
	// 	<?php
	// 	if($s['cek_in']<>""){
	// 		echo"defaultTime:'$s[cek_in]'";
	// 	}
	// 	else{
	// 		echo"defaultTime:'12:00'";
	// 	}
	// 	?>
	// });
	// jQuery('#cekout').timepicker({
	// 	showMeridian: false,
	// 	<?php
	// 	if($s['cek_out']<>""){
	// 		echo"defaultTime:'$s[cek_out]'";
	// 	}
	// 	else{
	// 		echo"defaultTime:'13:00'";
	// 	}
	// 	?>
	// });
	// $("#provinsi").change(function(){
	// 	var id = $("#provinsi").val();
	// 	$.ajax({
	// 		type:"POST",
	// 		url: "../ajax/provinsi.php",
	// 		data: "id=" + id,
	// 		success: function(data){
	// 			$("#kota").html(data);
	// 			$("#kota").fadeIn(2000);
	// 		}
	// 	});
	// });
	// $("#kota").change(function(){
	// 	var id = $("#kota").val();
	// 	$.ajax({
	// 		type:"POST",
	// 		url: "../ajax/kota.php",
	// 		data: "id=" + id,
	// 		success: function(data){
	// 			$("#kecamatan").html(data);
	// 			$("#kecamatan").fadeIn(2000);
	// 		}
	// 	});
	// });
});
</script>
