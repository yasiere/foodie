<section>
    <div class="mainwrapper">
        <?php include "inc/kiri.php"; ?>
        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="pageicon pull-left">
                        <i class="fa fa-tags"></i>
                    </div>
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href="mod-beranda-1.htm"><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="mod-beranda-1.htm">Beranda</a></li>
                            <li>Informasi Kategori</li>
                        </ul>
                        <h4>Informasi Kategori</h4>
                    </div>
                </div><!-- media -->
            </div><!-- pageheader -->

            <div class="contentpanel">
				<?php
				if(isset($_GET['info'])){
					if($_GET['info']==1){
						echo"<div class='alert alert-success'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Sukses!</strong> data berhasil dihapus.
						</div>";
					}
					elseif($_GET['info']==2){
						echo"<div class='alert alert-danger'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Banner Depan Max 3 Kategori</strong>
						</div>";
					}
				}
				?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                                <div class="col-sm-11">
                                    <h4 class="panel-title">Daftar Kategori</h4>
                                    <p><a href="add-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-primary">Tambah Data</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered responsive" style="width: 25%;">
                                <tr>
                                <?
                                $tombol = mysqli_query($koneksi,"select * from kategori where banner_kategori = '1'");
                                while($tombolx = mysqli_fetch_array($tombol)) {
                                    echo "<td style='text-align: center'><span style='font-size: 16px; text-decoration: underline;'>$tombolx[nama_kategori]</span><br style='margin-bottom: 10px;'>";	
                                    echo "<img src='$base/assets/img/$tombolx[gambar_kategori]' width='150px'></td>";
                                }
                                ?>
                                </tr>
                                </table>
                            </div>
                            <table id="aledata" class="table table-striped table-bordered responsive">
                                <thead class="">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Kategori </th>
                                        <!--<th>Banner Depan </th>-->
                                        <th>Gambar </th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no=1;
                                    $sql=mysqli_query($koneksi,"select * from kategori k order by k.nama_kategori asc");
                                    while($r=mysqli_fetch_array($sql)){
										if($r['banner_kategori'] == 1)
										{
											$banner = "<a href='mod/$mod/aksi.php?mod=$mod&id=$r[id_kategori]&bnr=0&ale=4&url=$_GET[url]' class='btn btn-success btn-rounded btn-sm'> <i class='fa fa-toggle-on'></i> ON </a>";
										}
										else
										{
											$banner = "<a href='mod/$mod/aksi.php?mod=$mod&id=$r[id_kategori]&bnr=1&ale=4&url=$_GET[url]' class='btn btn-default btn-rounded btn-sm'> <i class='fa fa-toggle-off'></i> OFF </a>";
										}
										
                                        echo"<tr>
                                            <td width='30px'>$no.</td>
                                            <td>$r[nama_kategori]</td>
											
											<td style='text-align: center' width='100px'>"; if(!empty($r['gambar_kategori'])) { echo "<img src='$base/assets/img/$r[gambar_kategori]' width='100px' class='img-thumbnail'>"; } echo "</td>
                                            <td width='170px'>
                                                    <a href='edit-$mod-$r[id_kategori]-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>
                                                    <a class='btn btn-danger btn-rounded btn-sm' href='mod/$mod/aksi.php?mod=$mod&id=$r[id_kategori]&gambar=$r[gambar_kategori]&ale=1&url=$_GET[url]' onClick=\"return confirm('apakah anda yakin akan menghapus data ini ?')\"><i class='fa fa-trash-o'></i> Hapus</a>
                                            </td>
                                        </tr>";
                      				$no++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                </div><!-- panel -->
            </div><!-- contentpanel -->
        </div><!-- mainpanel -->
    </div><!-- mainwrapper -->
</section>

<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/pace.min.js"></script>
<script src="js/retina.min.js"></script>
<script src="js/jquery.cookies.js"></script>

<script src="js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/plug-ins/725b2a2115b/integration/bootstrap/3/dataTables.bootstrap.js"></script>
<script src="//cdn.datatables.net/responsive/1.0.1/js/dataTables.responsive.js"></script>
<script src="js/custom.js"></script>
<script>
jQuery(document).ready(function(){
	jQuery('#aledata').dataTable({
		"sPaginationType": "full_numbers",
		"iDisplayLength": 25,
		"aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, "Semua"]]
    });
});
</script>
