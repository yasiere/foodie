<?php
session_start();
if(!empty($_SESSION['ak_id'])){
	include "../../inc/db.php";
	include "../../inc/upload.php";

	$mod					= $_GET['mod'];
	$ukuran_maksimal		= 30000000;
	$acak            		= rand(0000,9999);
	$lebar           		= 370;
	
	if($_POST['posisi_banner'] == 'Left Side' || $_POST['posisi_banner'] == 'Right Side')
		$tinggi			 		= 670;
	else
	{
		$tinggi			 		= 320;
	}

	if($_GET['ale']==1){
		mysqli_query($koneksi,"DELETE FROM banner WHERE id_banner='$_GET[id]'");
		unlink("../../../assets/img/$_GET[gambar]");
		header('location:../../notif-'.$mod.'-'.$_GET['url'].'-1.htm');
	}
	elseif($_GET['ale']==2){
		//$fold			= $_POST['folder'];
		$folder         = "assets/img/";
		$lokasi_file 	= $_FILES['fupload']['tmp_name'];
		$tipe_file      = $_FILES['fupload']['type'];
		$nama_file 		= $_FILES['fupload']['name'];
		$ukuran_file 	= $_FILES['fupload']['size'];
		$nama_file   	= str_replace(' ', '_', $nama_file);
		$nama_file_unik	= $acak.$nama_file;
		
		if (!empty($lokasi_file)){
			if($ukuran_file < $ukuran_maksimal){
				if ($tipe_file != "image/jpeg" AND $tipe_file != "image/pjpeg" AND $tipe_file != "video/mp4"){
					header('location:../../form-'.$mod.'-'.$_GET['url'].'-2.htm');
				}
				else{
					upload($nama_file_unik,$folder,$lokasi_file,$lebar,$tinggi);
					mysqli_query($koneksi,"INSERT INTO banner(posisi_banner,gambar_banner,alt_banner) VALUES('$_POST[posisi_banner]','$nama_file_unik','$_POST[alt_banner]')");			
					header('location:../../form-'.$mod.'-'.$_GET['url'].'-1.htm');
				}
			}
			else{
				header('location:../../form-'.$mod.'-'.$_GET['url'].'-3.htm');
			}
		}
		elseif(empty($lokasi_file))
		{
			mysqli_query($koneksi,"INSERT INTO banner(posisi_banner) VALUES('$_POST[posisi_banner]')");
			$idbaru = mysqli_insert_id($koneksi);
			
			if(empty($lokasi_file) && !empty($_POST['alt_banner']))
			{
				mysqli_query($koneksi, "update banner set alt_banner = '$_POST[alt_banner]' where id_banner = '$idbaru'");
			}
			
			header('location:../../form-'.$mod.'-'.$_GET['url'].'-1.htm');
		}
	}
	elseif($_GET['ale']==3){
		//$fold			= $_POST['folder'];
		$folder         = "assets/img/";
		$lokasi_file    = $_FILES['fupload']['tmp_name'];
		$tipe_file      = $_FILES['fupload']['type'];
		$nama_file      = $_FILES['fupload']['name'];
		$ukuran_file    = $_FILES['fupload']['size'];
		$nama_file      = str_replace(' ', '_', $nama_file);
		$nama_file_unik	= $acak.$nama_file;
		
		$fat_seo = $_POST['fitur'];
		$fat=implode(',',$fat_seo);

		if (!empty($lokasi_file)){
			if($ukuran_file < $ukuran_maksimal){
				if ($tipe_file != "image/jpeg" AND $tipe_file != "image/pjpeg" AND $tipe_file != "video/mp4"){
					header('location:../../miss-'.$mod.'-'.$_POST['id'].'-'.$_GET['url'].'-2.htm');
				}
				else{
					upload($nama_file_unik,$folder,$lokasi_file,$lebar,$tinggi);
					mysqli_query($koneksi,"UPDATE banner SET posisi_banner = '$_POST[posisi_banner]', gambar_banner = '$nama_file_unik', alt_banner = '$_POST[alt_banner]' WHERE id_banner = '$_POST[id]'");
					unlink("../../../assets/img/$_POST[gambar_banner]");
					header('location:../../miss-'.$mod.'-'.$_POST['id'].'-'.$_GET['url'].'-1.htm');
				}
			}
			else{
				header('location:../../miss-'.$mod.'-'.$_POST['id'].'-'.$_GET['url'].'-3.htm');
			}
		}
		else{ //jika yang diubah hanya isi tanpa mengubah gambar
			mysqli_query($koneksi,"UPDATE banner SET posisi_banner = '$_POST[posisi_banner]' WHERE id_banner = '$_POST[id]'");
			
			if(empty($lokasi_file) && !empty($_POST['alt_banner']))
			{
				mysqli_query($koneksi, "update banner set alt_banner = '$_POST[alt_banner]' where id_banner = '$_POST[id]'");
			}
			header('location:../../miss-'.$mod.'-'.$_POST['id'].'-'.$_GET['url'].'-1.htm');
		}
	}
}
else{
	header('location:not-found.htm');
}
?>
