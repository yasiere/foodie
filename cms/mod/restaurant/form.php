<style>
html, body {
	height: 100%;
	margin: 0;
	padding: 0;
}
#map {
	width: 100%;
	height: 400px;
}
.controls {
	margin-top: 10px;
	border: 1px solid transparent;
	border-radius: 2px 0 0 2px;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	height: 32px;
	outline: none;
	box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
}
#searchInput {
	background-color: #fff;
	font-family: Roboto;
	font-size: 15px;
	font-weight: 300;
	margin-left: 12px;
	padding: 0 11px 0 13px;
	text-overflow: ellipsis;
	width: 50%;
}
#searchInput:focus {
 	border-color: #4d90fe;
}

</style>

<section>
    <div class="mainwrapper">
        <?php include "inc/kiri.php"; ?>
         <div class="mainpanel">
             <div class="pageheader">
                <div class="media">
                    <div class="pageicon pull-left">
                        <i class="fa fa-home"></i>
                    </div>
                    <div class="media-body">
						<ul class="breadcrumb">
                            <li><a href="mod-beranda.htm"><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="mod-beranda.htm">Beranda</a></li>
                            <li>Informasi Restaurant</li>
                        </ul>
                        <h4>Informasi Restaurant</h4>
                    </div>
                </div><!-- media -->
            </div><!-- pageheader -->
            <div class="contentpanel">
				<?php if($_GET['proses']==1){ ?>
					<?php
					if(isset($_GET['info'])){
						if($_GET['info']==1){
							echo"<div class='alert alert-success'>
								<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
								<strong>Sukses!</strong> data berhasil disimpan.
							</div>";
						}
						elseif($_GET['info']==2){
							echo"<div class='alert alert-danger'>
								<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
								<strong>Kesalahan!</strong> tipe file tidak cocok, anda harus mengupload file berekstensi *.JPG.
							</div>";
						}
						else{
							echo"<div class='alert alert-danger'>
								<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
								<strong>Kesalahan!</strong> ukuran file terlalu besar, ukuran maksimal yang diperbolehkan adalah 300kb.
							</div>";
						}
					}
					?>
	        <form action="mod/<?php echo"$mod/aksi.php?mod=$mod&ale=2&url=$_GET[url]"; ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" id="signup-daftar">
	          <div class="panel panel-default">
	            <div class="panel-heading">
	                <div class="panel-btns">
	                  <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
	                </div><!-- panel-btns -->
			            <h4 class="panel-title">Data Restaurant</h4>
	                <p>Silahkan lengkapi data dibawah ini.</p>
	            </div>
	            <div class="panel-body nopadding">
	              	<div class="form-group">
	                  <label class="col-sm-2 control-label">Member :</label>
	                  <div class="col-sm-10">
	                    <select name="id_member" class="pilih" style="width: 100%;">
	                    <?php
	                    	$sql=mysqli_query($koneksi,"select id_member,username from member order by username asc");
	                    	while($b=mysqli_fetch_array($sql)){
	                    		echo"<option value='$b[id_member]'>$b[username]</option>";
	                    	}
	                    ?>
	                    </select>
	                  </div>
	                </div>
	                <div class="form-group">
	                   <label class="col-sm-2 control-label">Restaurant Name :</label>
	                   <div class="col-sm-10">
	                       <input type="text" name="restaurant_name" class="form-control" id="username">
	                   </div>
	                </div>
	                <div class="form-group">
	                   <label class="col-sm-2 control-label">Business Type :</label>
	                   <div class="col-sm-10">
	                    <select name="type_of_business" class="pilih" style="width: 100%;">
	                    	<?php
	                    		$sql=mysqli_query($koneksi,"select id_type_of_business,nama_type_of_business from type_of_business order by nama_type_of_business asc");
	                    		while($b=mysqli_fetch_array($sql)){
	                    			echo"<option value='$b[id_type_of_business]'>$b[nama_type_of_business]</option>";
	                    		}
	                    	?>
	                    </select>
	                   </div>
	                </div>
	                <div class="form-group">
	                   <label class="col-sm-2 control-label">Cover Photo :</label>
	                   <div class="col-sm-10">
	                     <div class="fileinput fileinput-new" data-provides="fileinput">
	                        <div class="fileinput-preview upload" data-trigger="fileinput"></div>
	                        <input type="file" name="cover_photo" class="hidden">
	                      </div>
	                      <p class="help-block">Image used have to be in JPG, PNG, and GIF format and the maximum total images size is 50MB also minimal dimension must be 800x600 pixel.</p>
	                   </div>
	                </div>
	                <div class="form-group">
	                   <label class="col-sm-2 control-label">Tag :</label>
	                   <div class="col-sm-10">
	                       <input type="text" name="tag" class="form-control">
	                   </div>
	                </div>


	                <div class="accordion">Basic Information</div>
	                <div class="panels">
	                  <div class="form-group">
	                     <label class="col-sm-2 control-label">Street Address :</label>
	                     <div class="col-sm-10">
	                        <input type="text" name="street_address" class="form-control">
	                     </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-sm-2 control-label">Country :</label>
	                    <div class="col-sm-10">
	                      <select name="negara" class="pilih" style="width: 100%;" id="negara">
	                      <option value="">Select Country</option>
	                       	<?php
	                						$sql=mysqli_query($koneksi,"select id_negara,nama_negara from negara order by nama_negara asc");
	                						while($b=mysqli_fetch_array($sql)){
	                							echo"<option value='$b[id_negara]'>$b[nama_negara]</option>";
	                						}
	                					?>
	                       </select>
	                    </div>
	                  </div>
	                  <div class="form-group">
	                     <label class="col-sm-2 control-label">State/Province :</label>
	                     <div class="col-sm-10">
	                         <select name="propinsi" class="pilih" style="width: 100%;" id="propinsi">
	                          	<option value="">Select State/ Province</option>
	                          </select>
	                     </div>
	                  </div>
	                  <div class="form-group">
	                     <label class="col-sm-2 control-label">City :</label>
	                     <div class="col-sm-10">
	                         <select name="kota" class="pilih" style="width: 100%;" id="kota">
	                          	<option value="">Select City</option>
	                          </select>
	                     </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-sm-2 control-label">Shopping Mall :</label>
	                    <div class="col-sm-10">
	                        <select name="mall" class="pilih" style="width: 100%;" id="mall">
	                        		<option value="">Select Shopping Mall</option>
	                         </select>
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-sm-2 control-label">Landmark :</label>
	                    <div class="col-sm-10">
	                       <select name="landmark" class="pilih" style="width: 100%;">
	                       <option value="">Select Landmark</option>
	                       		<?php
	                    				$sql=mysqli_query($koneksi,"select * from landmark order by nama_landmark asc");
	                    				while($d=mysqli_fetch_array($sql)){
	                    					echo"<option value='$d[id_landmark]'>$d[nama_landmark]</option>";
	                    				}
	                    				?>
	                        </select>
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-sm-2 control-label">Telephone :</label>
	                    <div class="col-sm-10">
	                      <input type="text" name="telephone" class="form-control">
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-sm-2 control-label">Facsimile :</label>
	                    <div class="col-sm-10">
	                      <input type="text" name="fax" class="form-control">
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-sm-2 control-label">Postal Code :</label>
	                    <div class="col-sm-10">
	                       <input type="text" name="postal_code" class="form-control">
	                    </div>
	                  </div>
	                  <div class="form-group">
	                     <label class="col-sm-2 control-label">Reservation :</label>
	                     <div class="col-sm-10">
	                         <input type="text" name="reservation_phone" class="form-control">
	                     </div>
	                  </div>
	                  <div class="form-group">
	                     <label class="col-sm-2 control-label">Email :</label>
	                     <div class="col-sm-10">
	                         <input type="text" name="email_resto" class="form-control">
	                     </div>
	                  </div>
	                  <div class="form-group">
	                     <label class="col-sm-2 control-label">Website :</label>
	                     <div class="col-sm-10">
	                         <input type="text" name="web" class="form-control">
	                     </div>
	                  </div>
	                  <div class="form-group">
	                     <label class="col-sm-2 control-label">Social Media :</label>
	                     <div class="col-sm-10">
	                          <input type="text" class="form-control mb10" placeholder="Write Facebook Url" name="facebook" value="">
	                  				<input type="text" class="form-control mb10" placeholder="Write Twitter Url" name="twitter" value="">
	                  				<input type="text" class="form-control mb10" placeholder="Write Instagram Url" name="instagram" value="">
	                  				<input type="text" class="form-control" placeholder="Write Pinterest Url" name="pinterest" value="">
	                     </div>
	                  </div>
	                  <div class="form-group">
	                     <label class="col-sm-2 control-label">Branch :</label>
	                     <div class="col-sm-10">
	                         <input type="text" name="branch_name" class="form-control">
	                     </div>
	                  </div>
	                </div>

	                <div class="accordion">Main Information</div>
	                  <div class="panels">
	                    <div class="form-group">
	                    		<label class="col-sm-2 control-label">Pork Serving :</label>
	                        <div class="col-sm-10">
	                        	<div class="radio radio-inline">
	                    				<input type="radio" checked="" name="pork" value="Yes" id="pork1">
	                    				Yes
	                    			</div>
	                    			<div class="radio radio-inline radio-danger">
	                    				<input type="radio" name="pork" value="No" id="pork2">
	                    				No
	                    			</div>
	                    			<div class="radio radio-inline radio-danger">
	                    				<input type="radio" name="pork" value="Halal" id="pork3">
	                    				Halal
	                    			</div>
	                          <div class="radio radio-inline radio-danger">
	                    				<input type="radio" name="pork" value="vege" id="pork4">
	                    				Vegetable
	                    			</div>
	                        </div>
	                    </div>
	                    <div class="form-group">
	                    		<label class="col-sm-2 control-label">Alcohol Serving :</label>
	                        <div class="col-sm-10">
	                            <div class="radio radio-inline radio-danger">
	                              <input type="radio" checked="" name="alcohol" value="Yes" id="alcohol1">
	                                Yes
	                            </div>
	                            <div class="radio radio-inline radio-danger">
	                              <input type="radio" name="alcohol" value="No" id="alcohol2">
	                                No
	                            </div>
	                        </div>
	                    </div>
	                    <div class="form-group">
	                    		<label class="col-sm-2 control-label">Price Index</label>
	                        <div class="col-sm-10">
	                        	<select name="price_index" class="pilih" style="width: 100%;">
	                            <option value="">Select Price Index</option>
	                           		<?php
	                    						$sql=mysqli_query($koneksi,"select * from price_index");
	                    						while($d=mysqli_fetch_array($sql)){
	                    							echo"<option value='$d[id_price_index]'>$d[nama_price_index]</option>";
	                    						}
	                    					?>
	                            </select>
	                        </div>
	                    </div>
	                    <div class="form-group">
	                    		<label class="col-sm-2 control-label">Suitable For</label>
	                        <div class="col-sm-10">
	                        	<select name="suitable_for" class="pilih" style="width: 100%;">
	                            <option value="">Select Suitable For</option>
	                           		<?php
	                    						$sql=mysqli_query($koneksi,"select * from suitable_for order by nama_suitable_for asc");
	                    						while($h=mysqli_fetch_array($sql)){
	                    							echo"<option value='$h[id_suitable_for]'>$h[nama_suitable_for]</option>";
	                    						}
	                    					?>
	                            </select>
	                        </div>
	                    </div>
	                    <div class="form-group">
	                    		<label class="col-sm-2 control-label">Wi-Fi</label>
	                        <div class="col-sm-10">
	                        	<select name="wifi" class="pilih" style="width: 100%;">
	                            	<option value="">Select Wi-Fi</option>
	                    						<?php
	                    							$sql=mysqli_query($koneksi,"select * from wifi order by nama_wifi asc");
	                    							while($n=mysqli_fetch_array($sql)){
	                    								echo"<option value='$n[id_wifi]'>$n[nama_wifi]</option>";
	                    							}
	                    						?>
	                            </select>
	                        </div>
	                    </div>
	                    <div class="form-group">
	                    		<label class="col-sm-2 control-label">Payment</label>
	                        <div class="col-sm-10">
	                        	<select name="payment" class="pilih" style="width: 100%;">
	                    				<option value="">Select Payment</option>
	                    				<?php
	                    				$sql=mysqli_query($koneksi,"select * from term_of_payment order by nama_term_of_payment asc");
	                    				while($o=mysqli_fetch_array($sql)){
	                    					echo"<option value='$o[id_term_of_payment]'>$o[nama_term_of_payment]</option>";
	                    				}
	                    				?>
	                          </select>
	                        </div>
	                    </div>
	                    <div class="form-group">
	                    		<label class="col-sm-2 control-label">Serving Time</label>
	                        <div class="col-sm-10">
	                        	<select name="serving_time" class="pilih" style="width: 100%;">
	                    				<option value="0">Select Serving Time</option>
	                    				<?php
	                    				$sql=mysqli_query($koneksi,"select * from serving_time");
	                    				while($k=mysqli_fetch_array($sql)){
	                    					echo"<option value='$k[id_serving_time]'>$k[nama_serving_time]</option>";
	                    				}
	                    				?>
	                            </select>
	                        </div>
	                    </div>
	                    <!-- <div class="form-group">
	                    		<label class="col-sm-2 control-label">Type Of Service</label>
	                        <div class="col-sm-10">
	                        	<select name="type_of_service" class="pilih" style="width: 100%;">
	                    				<option value="0">Select Type of Service</option>
	                    				<?php
	                    				$sql=mysqli_query($koneksi,"select * from type_of_service order by nama_type_of_service asc");
	                    				while($l=mysqli_fetch_array($sql)){
	                    					echo"<option value='$l[id_type_of_service]'>$l[nama_type_of_service]</option>";
	                    				}
	                    				?>
	                          </select>
	                        </div>
	                    </div> -->
	                    <div class="form-group">
	                    		<label class="col-sm-2 control-label">Air Conditioning</label>
	                        <div class="col-sm-10">
	                        	<select name="air_conditioning" class="pilih" style="width: 100%;">
	                    				<option value="0">Select Air Conditioning</option>
	                    				<?php
	                    				$sql=mysqli_query($koneksi,"select * from air_conditioning order by nama_air_conditioning asc");
	                    				while($l1=mysqli_fetch_array($sql)){
	                    					echo"<option value='$l1[id_air_conditioning]'>$l1[nama_air_conditioning]</option>";
	                    				}
	                    				?>
	                            </select>
	                        </div>
	                    </div>
	                    <div class="form-group">
	                    		<label class="col-sm-2 control-label">Heating System</label>
	                        <div class="col-sm-10">
	                        	<select name="heating_system" class="pilih" style="width: 100%;">
	                    				<option value="0">Select Heating Conditioning</option>
	                    				<?php
	                    				$sql=mysqli_query($koneksi,"select * from heating_system order by nama_heating_system asc");
	                    				while($l2=mysqli_fetch_array($sql)){
	                    					echo"<option value='$l2[id_heating_system]'>$l2[nama_heating_system]</option>";
	                    				}
	                    				?>
	                          </select>
	                        </div>
	                    </div>
	                    <div class="form-group">
	                  		<label class="col-sm-2 control-label">Premise's Security</label>
	                      <div class="col-sm-10">
	                      	<select name="premise_security" class="pilih" style="width: 100%;">
	                  				<option value="0">Select premise's Security</option>
	                  				<?php
	                  				$sql=mysqli_query($koneksi,"select * from premise_security order by nama_premise_security asc");
	                  				while($p=mysqli_fetch_array($sql)){
	                  					echo"<option value='$p[id_premise_security]'>$p[nama_premise_security]</option>";
	                  				}
	                  				?>
	                        </select>
	                      </div>
	                    </div>

	                    <div class="form-group">
	                  		<label class="col-sm-2 control-label">Parking Spaces</label>
	                      <div class="col-sm-10">
	                      	<select name="parking_spaces" class="pilih" style="width: 100%;">
	                  				<option value="0">Select Parking Spaces</option>
	                  				<?php
	                  				$sql=mysqli_query($koneksi,"select * from parking_spaces order by nama_parking_spaces asc");
	                  				while($q=mysqli_fetch_array($sql)){
	                  					echo"<option value='$q[id_parking_spaces]'>$q[nama_parking_spaces]</option>";
	                  				}
	                  				?>
	                        </select>
	                      </div>
	                    </div>

	                    <div class="form-group">
	                  		<label class="col-sm-2 control-label">Premise's Fire Safety</label>
	                      <div class="col-sm-10">
	                      	<select name="premise_fire_safety" class="pilih" style="width: 100%;">
	                  				<option value="0">Select Premise's Fire Safety</option>
	                  				<?php
	                  				$sql=mysqli_query($koneksi,"select * from premise_fire_safety order by nama_premise_fire_safety asc");
	                  				while($r=mysqli_fetch_array($sql)){
	                  					echo"<option value='$r[id_premise_fire_safety]'>$r[nama_premise_fire_safety]</option>";
	                  				}
	                  				?>
	                        </select>
	                      </div>
	                    </div>

	                    <div class="form-group">
	                  		<label class="col-sm-2 control-label">Premise's Hygiene</label>
	                      <div class="col-sm-10">
	                      	<select name="premise_hygiene" class="pilih" style="width: 100%;">
	                  				<option value="0">Select Premise's Hygiene</option>
	                  				<?php
	                  				$sql=mysqli_query($koneksi,"select * from premise_hygiene order by nama_premise_hygiene asc");
	                  				while($r1=mysqli_fetch_array($sql)){
	                  					echo"<option value='$r1[id_premise_hygiene]'>$r1[nama_premise_hygiene]</option>";
	                  				}
	                  				?>
	                        </select>
	                      </div>
	                    </div>

	                    <div class="form-group">
	                    		<label class="col-sm-2 control-label">Premise's Maintenance</label>
	                        <div class="col-sm-10">
	                        	<select name="premise_maintenance" class="pilih" style="width: 100%;">
	                    				<option value="0">Select Premise Maintenance</option>
	                    				<?php
	                    				$sql=mysqli_query($koneksi,"select * from premise_maintenance order by nama_premise_maintenance asc");
	                    				while($s=mysqli_fetch_array($sql)){
	                    					echo"<option value='$s[id_premise_maintenance]'>$s[nama_premise_maintenance]</option>";
	                    				}
	                    				?>
	                          </select>
	                        </div>
	                    </div>

	                    <div class="form-group">
	                  		<label class="col-sm-2 control-label">Ambience</label>
	                      <div class="col-sm-10">
	                      	<select name="ambience" class="pilih" style="width: 100%;">
	                  				<option value="0">Select Ambience</option>
	                  				<?php
	                  				$sql=mysqli_query($koneksi,"select * from ambience order by nama_ambience asc");
	                  				while($t=mysqli_fetch_array($sql)){
	                  					echo"<option value='$t[id_ambience]'>$t[nama_ambience]</option>";
	                  				}
	                  				?>
	                        </select>
	                      </div>
	                    </div>

	                    <div class="form-group">
	                  		<label class="col-sm-2 control-label">Attire</label>
	                      <div class="col-sm-10">
	                      	<select name="attire" class="pilih" style="width: 100%;">
	                  				<option value="0">Select Attire </option>
	                  				<?php
	                  				$sql=mysqli_query($koneksi,"select * from attire order by nama_attire asc");
	                  				while($u=mysqli_fetch_array($sql)){
	                  					echo"<option value='$u[id_attire]'>$u[nama_attire]</option>";
	                  				}
	                  				?>
	                          </select>
	                      </div>
	                    </div>

	                    <div class="form-group">
	                    		<label class="col-sm-2 control-label">Clean Washroom</label>
	                        <div class="col-sm-10">
	                        	<select name="clean_washroom" class="pilih" style="width: 100%;">
	                    				<option value="0">Select Clean Washroom</option>
	                    				<?php
	                    				$sql=mysqli_query($koneksi,"select * from clean_washroom order by nama_clean_washroom asc");
	                    				while($v=mysqli_fetch_array($sql)){
	                    					echo"<option value='$v[id_clean_washroom]'>$v[nama_clean_washroom]</option>";
	                    				}
	                    				?>
	                          </select>
	                        </div>
	                    </div>
	                    <div class="form-group">
	                    	<label class="col-sm-2 control-label">Table Availability</label>
	                      <div class="col-sm-10">
	                      	<select name="tables_availability" class="pilih" style="width: 100%;">
	                    			<option value="0">Select Tables Availability</option>
	                    			<?php
	                    			$sql=mysqli_query($koneksi,"select * from tables_availability order by nama_tables_availability asc");
	                    			while($w=mysqli_fetch_array($sql)){
	                    				echo"<option value='$w[id_tables_availability]'>$w[nama_tables_availability]</option>";
	                    			}
	                    			?>
	                          </select>
	                      </div>
	                    </div>
	                    <div class="form-group">
	                    	<label class="col-sm-2 control-label">Noise Level</label>
	                      <div class="col-sm-10">
	                      	<select name="noise_level" class="pilih" style="width: 100%;">
	                    			<option value="0">Select Noise Level</option>
	                    			<?php
	                    			$sql=mysqli_query($koneksi,"select * from noise_level");
	                    			while($x=mysqli_fetch_array($sql)){
	                    				echo"<option value='$x[id_noise_level]'>$x[nama_noise_level]</option>";
	                    			}
	                    			?>
	                          </select>
	                      </div>
	                    </div>
	                    <div class="form-group">
	                    	<label class="col-sm-2 control-label">Water Tipping</label>
	                      <div class="col-sm-10">
	                      	<select name="waiter_tipping" class="pilih" style="width: 100%;">
	                    			<option value="0">Select Waiter Tipping</option>
	                    			<?php
	                    			$sql=mysqli_query($koneksi,"select * from waiter_tipping order by nama_waiter_tipping asc");
	                    			while($y=mysqli_fetch_array($sql)){
	                    				echo"<option value='$y[id_waiter_tipping]'>$y[nama_waiter_tipping]</option>";
	                    			}
	                    			?>
	                          </select>
	                      </div>
	                    </div>
	                  </div>


	                  <div class="accordion">Business Hours</div>
	                       <div id="foo" class="panels">
	                         <div class="form-group">
	                         		<label class="col-sm-2 control-label">Operational</label>
	                              <div class="col-sm-10">
	                              	<select name="operation_hour" class="pilih" style="width: 100%;">
	                									<?php
	                										$sql=mysqli_query($koneksi,"select * from operation_hour order by nama_operation_hour asc");
	                										while($e1=mysqli_fetch_array($sql)){
	                											$selected="";
	                											if($e1['id_operation_hour']==$e['id_operation_hour']){
	                												$selected="selected";
	                											}
	                											echo"<option value='$e1[id_operation_hour]' $selected>$e1[nama_operation_hour]</option>";
	                										}
	                									?>
	                                </select>
	                              </div>
	                         </div>
	                         <div class="form-group">
	                         		<label class="col-sm-2 control-label">Restaurant Hour</label>
	                              <div class="col-sm-10 table-responsive">
	                              	<table class="table">
	              										<tr>
	              											<td></td>
	              											<th colspan="2">Restaurant Hour 1</th>
	              											<th colspan="2">Restaurant Hour 2</th>
	                                    <th colspan="2">Option</th>
	              										</tr>
	              										<tr>
	              											<th class="vc">Sunday</th>
	              											<td><input type="text" class="form-control timae" Placeholder="Open Time" id="resto_time1" name="resto_time1"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Close Time" id="resto_time2" name="resto_time2"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Open Time" id="resto_time1a" name="resto_time1a"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Close Time" id="resto_time2a" name="resto_time2a"></td>
	                                  <td>
	                                    <div class="checkbox checkbox-inline checkbox-danger">
	                                   		<input name="resto_sunday" value="Close" id="resto_sunday" type="checkbox">
	                                   		<label for="resto_sunday"> Close</label>
	                                   	</div>
	                                  </td>
	                                  <td>
	                                    <div class="checkbox checkbox-inline checkbox-danger">
	                             				  <div class="btn btn-danger" id="duplicate">Apply All Days</div>
	                               			</div>
	                                   </td>
	              										</tr>
	              										<tr>
	              											<th class="vc">Monday</th>
	              											<td><input type="text" class="form-control timae" Placeholder="Open Time" id="resto_time3" name="resto_time3"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Close Time" id="resto_time4" name="resto_time4"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Open Time" id="resto_time3a" name="resto_time3a"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Close Time" id="resto_time4a" name="resto_time4a"></td>
	                                     <td>
	                                      <div class="checkbox checkbox-inline checkbox-danger">
	                                 				<input name="resto_monday" value="Close" id="resto_monday" type="checkbox">
	                                 				<label for="resto_monday"> Close</label>
	                                 			</div>
	                                     </td>
	              										</tr>
	              										<tr>
	              											<th class="vc">Tuesday</th>
	              											<td><input type="text" class="form-control timae" Placeholder="Open Time" id="resto_time5" name="resto_time5"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Close Time" id="resto_time6" name="resto_time6"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Open Time" id="resto_time5a" name="resto_time5a"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Close Time" id="resto_time6a" name="resto_time6a"></td>
	                                   <td>
	                                    <div class="checkbox checkbox-inline checkbox-danger">
	                               				<input name="resto_tuesday" value="Close" id="resto_tuesday" type="checkbox">
	                               				<label for="resto_tuesday"> Close</label>
	                               			</div>
	                                   </td>
	                                   <td></td>
	              										</tr>
	              										<tr>
	              											<th class="vc">Wednesday</th>
	              											<td><input type="text" class="form-control timae" Placeholder="Open Time" id="resto_time7" name="resto_time7"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Close Time" id="resto_time8" name="resto_time8"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Open Time" id="resto_time7a" name="resto_time7a"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Close Time" id="resto_time8a" name="resto_time8a"></td>
	                                     <td>
	                                      <div class="checkbox checkbox-inline checkbox-danger">
	                                 				<input name="resto_wednesday" value="Close" id="resto_wednesday" type="checkbox">
	                                 				<label for="resto_wednesday"> Close</label>
	                                 			</div>
	                                     </td>
	                                     <td></td>
	              										</tr>
	              										<tr>
	              											<th class="vc">Thursday</th>
	              											<td><input type="text" class="form-control timae" Placeholder="Open Time"  id="resto_time9" name="resto_time9"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Close Time" id="resto_time10" name="resto_time10"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Open Time" id="resto_time9a" name="resto_time9a"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Close Time" id="resto_time10a" name="resto_time10a"></td>
	                                     <td>
	                                        <div class="checkbox checkbox-inline checkbox-danger">
	                                   				<input name="resto_thursday" value="Close" id="resto_thursday" type="checkbox">
	                                   				<label for="resto_thursday"> Close</label>
	                                   			</div>
	                                     </td>
	                                    <td></td>
	              										</tr>
	              										<tr>
	              											<th class="vc">Friday</th>
	              											<td><input type="text" class="form-control timae" Placeholder="Open Time" id="resto_time11" name="resto_time11"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Close Time" id="resto_time12" name="resto_time12"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Open Time" id="resto_time11a" name="resto_time11a"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Close Time" id="resto_time12a" name="resto_time12a"></td>
	                                   <td>
	                                      <div class="checkbox checkbox-inline checkbox-danger">
	                                 				<input name="resto_friday" value="Close" id="resto_friday" type="checkbox">
	                                 				<label for="resto_friday"> Close</label>
	                                 			</div>
	                                   </td>
	                                   <td></td>
	              										</tr>
	              										<tr>
	              											<th class="vc">Saturday</th>
	              											<td><input type="text" class="form-control timae" Placeholder="Open Time" id="resto_time13" name="resto_time13"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Select Time" id="resto_time14" name="resto_time14"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Open Time" id="resto_time13a" name="resto_time13a"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Select Time" id="resto_time14a" name="resto_time14a"></td>
	                                     <td>
	                                        <div class="checkbox checkbox-inline checkbox-danger">
	                                   				<input name="resto_saturday" value="Close" id="resto_saturday" type="checkbox">
	                                   				<label for="resto_saturday"> Close</label>
	                                   			</div>
	                                     </td>
	                                    <td></td>
	              										</tr>
	              									</table>
	                              </div>
	                         </div>

	                         <div class="form-group">
	                         		<label class="col-sm-2 control-label">Bar Hour</label>
	                              <div class="col-sm-10 table-responsive">
	                              	<table class="table">
	              										<tr>
	              											<td></td>
	              											<th colspan="2">Bar Hour 1</th>
	              											<th colspan="2">Bar Hour 2</th>
	                                    <th colspan="2">Option</th>
	              										</tr>
	              										<tr>
	              											<th class="vc">Sunday</th>
	              											<td><input type="text" class="form-control timae" Placeholder="Open Time" id="bar_time1" name="bar_time1"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Close Time" id="bar_time2" name="bar_time2"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Open Time" id="bar_time1a" name="bar_time1a"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Close Time" id="bar_time2a" name="bar_time2a"></td>
	                                  <td>
	                                    <div class="checkbox checkbox-inline checkbox-danger">
	                                   		<input name="resto_sunday" value="Close" id="resto_sunday" type="checkbox">
	                                   		<label for="resto_sunday"> Close</label>
	                                   	</div>
	                                  </td>
	                                  <td>
	                                      <div class="checkbox checkbox-inline checkbox-danger">
	                               				     <div class="btn btn-danger" id="duplicate2">Apply All Days</div>
	                                 			</div>
	                                   </td>
	              										</tr>
	              										<tr>
	              											<th class="vc">Monday</th>
	              											<td><input type="text" class="form-control timae" Placeholder="Open Time" id="bar_time3" name="bar_time3"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Close Time" id="bar_time4" name="bar_time4"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Open Time" id="bar_time3a" name="bar_time3a"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Close Time" id="bar_time4a" name="bar_time4a"></td>
	                                     <td>
	                                      <div class="checkbox checkbox-inline checkbox-danger">
	                                 				<input name="resto_monday" value="Close" id="resto_monday" type="checkbox">
	                                 				<label for="resto_monday"> Close</label>
	                                 			</div>
	                                     </td>
	              										</tr>
	              										<tr>
	              											<th class="vc">Tuesday</th>
	              											<td><input type="text" class="form-control timae" Placeholder="Open Time" id="bar_time5" name="bar_time5"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Close Time" id="bar_time6" name="bar_time6"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Open Time" id="bar_time5a" name="bar_time5a"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Close Time" id="bar_time6a" name="bar_time6a"></td>
	                                   <td>
	                                    <div class="checkbox checkbox-inline checkbox-danger">
	                               				<input name="resto_tuesday" value="Close" id="resto_tuesday" type="checkbox">
	                               				<label for="resto_tuesday"> Close</label>
	                               			</div>
	                                   </td>
	                                   <td></td>
	              										</tr>
	              										<tr>
	              											<th class="vc">Wednesday</th>
	              											<td><input type="text" class="form-control timae" Placeholder="Open Time" id="bar_time7" name="bar_time7"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Close Time" id="bar_time8" name="bar_time8"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Open Time" id="bar_time7a" name="bar_time7a"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Close Time" id="bar_time8a" name="bar_time8a"></td>
	                                     <td>
	                                      <div class="checkbox checkbox-inline checkbox-danger">
	                                 				<input name="resto_wednesday" value="Close" id="resto_wednesday" type="checkbox">
	                                 				<label for="resto_wednesday"> Close</label>
	                                 			</div>
	                                     </td>
	                                     <td></td>
	              										</tr>
	              										<tr>
	              											<th class="vc">Thursday</th>
	              											<td><input type="text" class="form-control timae" Placeholder="Open Time"  id="bar_time9" name="bar_time9"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Close Time" id="bar_time10" name="bar_time10"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Open Time" id="bar_time9a" name="bar_time9a"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Close Time" id="bar_time10a" name="bar_time10a"></td>
	                                     <td>
	                                        <div class="checkbox checkbox-inline checkbox-danger">
	                                   				<input name="resto_thursday" value="Close" id="resto_thursday" type="checkbox">
	                                   				<label for="resto_thursday"> Close</label>
	                                   			</div>
	                                     </td>
	                                    <td></td>
	              										</tr>
	              										<tr>
	              											<th class="vc">Friday</th>
	              											<td><input type="text" class="form-control timae" Placeholder="Open Time" id="bar_time11" name="bar_time11"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Close Time" id="bar_time12" name="bar_time12"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Open Time" id="bar_time11a" name="bar_time11a"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Close Time" id="bar_time12a" name="bar_time12a"></td>
	                                   <td>
	                                      <div class="checkbox checkbox-inline checkbox-danger">
	                                 				<input name="resto_friday" value="Close" id="resto_friday" type="checkbox">
	                                 				<label for="resto_friday"> Close</label>
	                                 			</div>
	                                   </td>
	                                   <td></td>
	              										</tr>
	              										<tr>
	              											<th class="vc">Saturday</th>
	              											<td><input type="text" class="form-control timae" Placeholder="Open Time" id="bar_time13" name="bar_time13"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Select Time" id="bar_time14" name="bar_time14"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Open Time" id="bar_time13a" name="bar_time13a"></td>
	              											<td><input type="text" class="form-control timae" Placeholder="Select Time" id="bar_time14a" name="bar_time14a"></td>
	                                     <td>
	                                        <div class="checkbox checkbox-inline checkbox-danger">
	                                   				<input name="resto_saturday" value="Close" id="resto_saturday" type="checkbox">
	                                   				<label for="resto_saturday"> Close</label>
	                                   			</div>
	                                     </td>
	                                    <td></td>
	              										</tr>
	              									</table>
	                              </div>
	                         </div>
	                       </div>


	                      <div class="accordion">Map</div>
	                       <div class="panels">
	             						<div class="box-panel">
	             							<div class="row mb20">
	             								<div class="col-4"><label class="control-label">Enter Restaurant Location</label></div>
	             								<div class="col-6">

	             								</div>
	             							</div><!-- row -->
	             							<div class="row">
	             								<div class="col-16">
	                              <input id="searchInput" class="controls" onkeydown="if (event.keyCode == 13) {return false;}" type="text" placeholder="Enter a location">
	                              <div id="map"></div>
	                              <ul id="geoData">
	                                  <input type="hidden" id="lat" name="lat">
	                                  <input type="hidden" id="lon" name="lng">
	                              </ul>
	             									<p class="f-12">Please select the district &amp; landmark for this restaurant. If the restaurant does not belong to any of the landmarks listed, please place the anchor at the restaurant’s correct location with your mouse.</p>
	             								</div><!-- col-sm-6 -->
	             							</div>
	             						</div>
	                       </div>

	                       <div class="accordion">Restaurant Description</div>
	                       <div class="panels">
	                         <div class="form-group">
	                              <label class="col-sm-2 control-label">Description :</label>
	                              <div class="col-sm-10">
	                                <textarea class="form-control" name="description" placeholder="Write Restaurant Description" rows="4"></textarea>
	                              </div>
	                          </div>
	                       </div>

	                       <div class="accordion">Business Status</div>
	                       <div class="panels">
	                         <div class="form-group">
	                              <label class="col-sm-2 control-label">Status :</label>
	                              <div class="col-sm-10">
	                                <div class="radio radio-inline radio-danger">
	              										<input type="radio" checked="" name="business_status" value="Ongoing" id="status1">
	              										<label for="status1"> Ongoing </label>
	              									</div>
	              									<div class="radio radio-inline radio-danger">
	              										<input type="radio" name="business_status" value="Renovation" id="status2">
	              										<label for="status2"> Renovation </label>
	              									</div>
	              									<div class="radio radio-inline radio-danger">
	              										<input type="radio" name="business_status" value="Relocate" id="status3">
	              										<label for="status3"> Relocate </label>
	              									</div>
	              									<div class="radio radio-inline radio-danger">
	              										<input type="radio" name="business_status" value="Ceased Operation" id="status4">
	              										<label for="status4"> Ceased Operation </label>
	              									</div>
	                              </div>
	                          </div>
	                          <div class="form-group">
	                               <label class="col-sm-2 control-label">Description :</label>
	                               <div class="col-sm-10">
	                                 <textarea class="form-control" name="business_description" placeholder="Write further information if applicable" rows="4"></textarea>
	                               </div>
	                           </div>
	                       </div>

	                       <div class="accordion">Features and Facility</div>
	                       <div class="panels">
	                         <div class="form-group">
	                           <?php
	             								$sql=mysqli_query($koneksi,"select * from facility order by nama_facility asc");
	             							
	             								while($s=mysqli_fetch_array($sql)){
	             									echo"<div class='col-md-5'>
	             										<div class='checkbox checkbox-inline checkbox-danger'>
	             											<input type='checkbox' name='facility[]' value='$s[nama_facility]' id='facility'>
	             											<label for='facility'> $s[nama_facility]</label>
	             										</div>
	             									</div>";
	             									
	             								}
	             								?>
	                         </div>
	                       </div>

	                       <div class="accordion">Cuisine</div>
	                       <div class="panels">
	                         <div class="form-group">
	                           <?php
	             								$sql=mysqli_query($koneksi,"select * from cuisine order by nama_cuisine asc");
	             								
	             								while($c=mysqli_fetch_array($sql)){
	             									echo"<div class='col-md-5'>
	             										<div class='checkbox checkbox-inline checkbox-danger'>
	             											<input type='checkbox' name='cuisine[]' value='$c[nama_cuisine]' id='cuisine'>
	             											<label for='cuisine'> $c[nama_cuisine]</label>
	             										</div>
	             									</div>";
	             								}
	             								
	             								?>
	                         </div>
	                       </div>

	                       <div class="accordion">Serving</div>
	                       <div class="panels">
	                         <div class="form-group">
	                           <?php
	             								$sql=mysqli_query($koneksi,"select * from serving order by nama_serving asc");
	             								
	             								while($i=mysqli_fetch_array($sql)){
	             									echo"<div class='col-sm-5'>
	             										<div class='checkbox checkbox-inline checkbox-danger'>
	             											<input type='checkbox' name='serving[]' value='$i[nama_serving]' id='serving'>
	             											<label for='serving'> $i[nama_serving]</label>
	             										</div>
	             									</div>";
	             								}
	             								
	             								?>
	                         </div>
	                       </div>

	                       <div class="accordion">Type of Serving</div>
	                       <div class="panels">
	                         <div class="form-group">
	                           <?php
	           								$sql=mysqli_query($koneksi,"select * from type_of_serving order by nama_type_of_serving asc");
	           								
	           								while($j=mysqli_fetch_array($sql)){
	           									echo"<div class='col-md-5'>
	           										<div class='checkbox checkbox-inline checkbox-danger'>
	           											<input type='checkbox' name='type_of_serving[]' value='$j[nama_type_of_serving]' id='type_of_serving'>
	           											<label for='type_of_serving'> $j[nama_type_of_serving]</label>
	           										</div>
	           									</div>";
	           								}
	           								
	           								?>

	                         </div>
	                       </div>

	                       <div class="accordion">Menu Photo</div>
	                       <div class="panels">
	                         <div class="box-panel dn">
	             							<div class="menu_wrap">
	             								<div class="row mb20">
	             									<div class="col-md-12">
	             										<div class="fileinput fileinput-new" data-provides="fileinput">
	             										  <div class="fileinput-preview upload" data-trigger="fileinput"></div>
	             										  <input type="file" name="menu[]" class="hidden">
	             										</div>
	             									</div>
	             									<div class="col-md-6"><input type="text"  autocomplete="off" class="form-control mt30" placeholder="Photo Caption" name="menu_caption[]"></div>
	             								</div>
	             							</div>
	             							<button type="button" class="tambah_menu btn btn-success">Add More Photo</button>
	             						</div>
	                       </div>

	                       <div class="accordion">Restaurant Rate</div>
	                       <div class="panels">
	                         <div role="tabpanel" id="tab-vote">
	           								<!-- Nav tabs -->
	           								<ul class="nav nav-tabs" role="tablist">
	           									<li role="presentation" class="active"><a href="#feature" aria-controls="feature" role="tab" data-toggle="tab">Feature Vote</a></li>
	           									<li role="presentation"><a href="#quick" aria-controls="quick" role="tab" data-toggle="tab">Quick Vote</a></li>
	           								</ul>
	           								<!-- Tab panes -->
	           								<div class="tab-content">
	           									<div role="tabpanel" class="tab-pane tab-panel active fade in" id="feature">
	           										<table width="100%">
	           											<tr>
	           												<td>Cleanliness <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall cleanliness of the restaurant, including the washroom, dining area, food & beverage, kitchens, cooks and the servers."></i></td>
	           												<td><input type="hidden" class="rating" id="rat1" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="cleanlines"></td>
	           											</tr>
	           											<tr>
	           												<td>Customer Service <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall customer services of the restaurant including the recepcionist, the phone operators, the food & beverage servers, manages and cashiers."></i></td>
	           												<td><input type="hidden" class="rating" id="rat2" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="customer_services"></td>
	           											</tr>
	           											<tr>
	           												<td>Food &amp; Beverage <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall food & beverage assessment based on cleanliness, flavor, ingredient freshness, cooking or making, presentation & aroma and value for money."></i></td>
	           												<td><input type="hidden" class="rating" id="rat3" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="food_beverage"></td>
	           											</tr>
	           											<tr>
	           												<td>Comfort <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment of the restaurant's comfortless on the sitting arrangement whether the table is too close to each other or the light is too direct to your eyes to be uncomforted or the coziness environment which make you feel home and comfort, etc."></i></td>
	           												<td><input type="hidden" class="rating" id="rat4" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="comfort"></td>
	           											</tr>
	           											<tr>
	           												<td>Value for Money <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment on the restaurant's food & beverage, comfortless, and services they provides to us a whole compare to the price we paid."></i></td>
	           												<td><input type="hidden" class="rating" id="rat5" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="value_money"></td>
	           											</tr>
	           										</table>
	           									</div>
	           									<div role="tabpanel" class="tab-pane tab-panel fade" id="quick">
	           										<table width="100%">
	           											<tr>
	           												<td><strong>Give your vote</strong></td>
	           												<td>
	                                    <input type="hidden" class="rating" id="rat6" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" value="" name="rating">
	                                  </td>
	           											</tr>
	           										</table>
	           									</div>
	           								</div>
	                              <ul class="list-inline">
	                    						<li><i class="fa fa-star"></i> Poor</li>
	                    						<li><i class="fa fa-star"></i><i class="fa fa-star"></i> Below Average</li>
	                    						<li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Average</li>
	                    						<li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Very Good</li>
	                    						<li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Excellent</li>
	                    					</ul>
	           							</div>
	                       </div>
	                    </div><!-- panel-body -->
	                </div><!-- panel -->

	        				<div class="panel panel-default">
	        					<div class="panel-footer">
	                    <button class="btn btn-primary mr5">Simpan</button>
	                    <a href="mod-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-warning">Kembali</a>
	                  </div><!-- panel-footer -->
	                </div><!-- panel -->
					</form>
					<?php
				}





































				elseif ($_GET['proses']==3) {

          ?>
          <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-btns">
                    <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                </div><!-- panel-btns -->
                <h4 class="panel-title">Data Restaurant</h4>
            </div>
            <div class="panel-body nopadding">
              <div class="form-group">
                <label class="col-sm-1 control-label">Total</label>
                <div class="col-sm-10">
                    <div class="col-sm-10 control-label"> :
                    	<?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT COUNT(*) as user FROM `restaurant` GROUP by restaurant_name")); echo "$juto"; ?>
					</div>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-1 control-label">Country</label>
                <div class="col-sm-10">
                  <div class="col-sm-10 control-label"> :
                    <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT n.nama_negara,n.id_negara, COUNT(*) FROM restaurant m JOIN negara n ON m.id_negara = n.id_negara GROUP BY n.nama_negara")); 
						echo "$juto"; ?>
					<br><br>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
					<select name="country-list" class="pilih" style="width: 200px" onchange="document.location.href=this.value">
						<option value="0">Select Country</option>
						<?php
							$idnegara = "";
							if (!empty($_GET['negara'])) {
								$idnegara = "AND id_negara='$_GET[negara]'";
							}

							$sql = mysqli_query($koneksi, "SELECT n.nama_negara,n.id_negara, COUNT(*) FROM restaurant m JOIN negara n ON m.id_negara = n.id_negara GROUP BY n.nama_negara");
							while ($a = mysqli_fetch_array($sql)) {
								if ($_GET['negara']==$a['id_negara']) {
									echo "<option value='detail-restaurant-3-negara$a[id_negara].htm' selected>$a[nama_negara]</option>";
								}
								else {
									echo "<option value='detail-restaurant-3-country$a[id_negara].htm'>$a[nama_negara]</option>";
								}

							}
						?>
					</select>
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Country</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
                    <?php
                    $sql=mysqli_query($koneksi, "SELECT n.nama_negara,
											(SELECT COUNT(*) FROM restaurant) as total,n.id_negara,COUNT(*) as jumlah ,
											concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM restaurant )) * 100 ),2),'%') AS 'percentage'
											FROM restaurant m JOIN negara n ON m.id_negara = n.id_negara GROUP BY n.nama_negara");
										$jumsql=mysqli_num_rows($sql);
                    while ($juto=mysqli_fetch_array($sql)) {
											$total= $juto['total'];
                      echo"
                        <tr>
                          <td width='30%'>
                            <a href='https://www.foodieguidances.com/pages/restaurant/search/?country=$juto[id_negara]'>
                            	<span style='width: 40px;display: inline-block;'>$juto[id_negara]</span> $juto[nama_negara]
                            </a>
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };

                     ?>
					<tr style="border-top: 1px solid; padding: 5px 0">
					 <td> <label>Sum</label>
					 </td>
						<td>
							<label><?php echo $total; ?></label>
						</td>
					<td>
						<label> 100% </label>
					</td>

					</tr>
                   </table>
                </div>
              </div>

				<div class="form-group">
	                <label class="col-sm-1 control-label">Total propinsi</label>
	                <div class="col-sm-10">
						<?php
							$idnegara = "";
							if (!empty($_GET['negara'])) {
								$idnegara = "AND m.id_negara='$_GET[negara]'";
							}
						?>
	                :  <?php $juto=mysqli_num_rows(mysqli_query($koneksi,
										"SELECT n.nama_propinsi,n.id_propinsi, COUNT(*) FROM restaurant m JOIN propinsi n ON m.id_propinsi = n.id_propinsi $idnegara GROUP BY n.id_propinsi")); echo "$juto"; ?>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Propinsi</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
                    <?php
                    $jumpro = "";
                    $sql=mysqli_query($koneksi, "SELECT ne.nama_negara, ne.id_negara, n.nama_propinsi,
                    	(SELECT COUNT(*) FROM restaurant) as total,n.id_propinsi,COUNT(*) as jumlah , 
                    	concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM restaurant )) * 100 ),2),'%') AS 'percentage'
										FROM restaurant m 
										LEFT JOIN negara ne ON m.id_negara   =  ne.id_negara
										LEFT JOIN propinsi n ON m.id_propinsi = n.id_propinsi 
										$idnegara GROUP BY n.nama_propinsi ORDER BY `n`.`id_negara` ASC");

                    while ($juto=mysqli_fetch_array($sql)) {
											$total= $juto['total'];
											$jumpro = $juto['jumlah'] + $jumpro;
                      echo"
                        <tr>
                          <td width='40%'>
                            <a href='https://www.foodieguidances.com/pages/restaurant/search/?country=$juto[id_negara]&state=$juto[id_propinsi]'>
                            	<span style='width: 40px;display: inline-block;'>$juto[id_propinsi]</span> $juto[nama_negara] - $juto[nama_propinsi]
                            </a>
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
						 <tr style="border-top: 1px solid; padding: 5px 0">
							 <td> <label>Sum</label>
							 </td>
						 	<td>
						 		<label><?php	echo $jumpro; ?></label>
						 	</td>
							<td>
								<label> 100% </label>
							</td>

						 </tr>
                   </table>
                </div>
                </div>
				<div class="form-group">
	                <label class="col-sm-1 control-label">Total City</label>
	                <div class="col-sm-10">
										<?php
											$idnegara = "";
											if (!empty($_GET['negara'])) {
												$idnegara = "AND m.id_negara='$_GET[negara]'";
											}
										?>
	                  :  <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT n.nama_kota,n.id_kota, COUNT(*) FROM restaurant m JOIN kota n ON m.id_kota = n.id_kota $idnegara GROUP BY n.id_kota")); echo "$juto"; ?>

	                </div>
                </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>City</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
                    <?php
                    $jumcity ="";
                    $sql=mysqli_query($koneksi, "SELECT ne.id_negara, ne.nama_negara, pro.id_propinsi, pro.nama_propinsi, n.nama_kota,(SELECT COUNT(*) FROM restaurant) as total,n.id_kota,COUNT(*) as jumlah , concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM restaurant )) * 100 ),2),'%') AS 'percentage' 
                    		FROM restaurant m 
                    		LEFT JOIN negara ne ON m.id_negara   =  ne.id_negara
							LEFT JOIN propinsi pro ON m.id_propinsi = pro.id_propinsi 
                    		JOIN kota n ON m.id_kota = n.id_kota 
                    		$idnegara 
                    		GROUP BY n.nama_kota order by ne.id_negara asc ");
                    while ($juto=mysqli_fetch_array($sql)) {
											$jumcity = $juto['jumlah'] + $jumcity;
                      echo"
                        <tr>
                          <td width='40%'>
                          	<a href='https://www.foodieguidances.com/pages/restaurant/search/?country=$juto[id_negara]&state=$juto[id_propinsi]&city=$juto[id_kota]'>
                            	<span style='width: 40px;display: inline-block;'>$juto[id_kota]</span> $juto[nama_negara] - $juto[nama_kota]
                            </a>
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
						 <tr style="border-top: 1px solid; padding: 5px 0">
							 <td> <label>Sum</label>
							 </td>
						 	<td>
						 		<label><?php echo $jumcity; ?></label>
						 	</td>
							<td>
								<label> 100% </label>
							</td>
						 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Business Type</label>
                <div class="col-sm-10">
                  :
					<?php
						$idnegara = "";
						$idnegaras = "";
						if (!empty($_GET['negara'])) {
							$idnegara = "AND m.id_negara='$_GET[negara]'";
							$idnegaras = "WHERE id_negara='$_GET[negara]'";
						}
					?>
					<?php
					$juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT n.nama_type_of_business,n.id_type_of_business, COUNT(*) FROM restaurant m JOIN type_of_business n ON m.id_type_of_business = n.id_type_of_business $idnegara GROUP BY n.id_type_of_business")); echo "$juto"; ?>
                </div>
              </div>
				<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Business Type</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
                    <?php
                    $jumbt ="";
                    $sql=mysqli_query($koneksi, "SELECT n.nama_type_of_business, (SELECT COUNT(*) FROM restaurant) as total,n.id_type_of_business,COUNT(*) as jumlah , concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM restaurant $idnegaras )) * 100 ),2),'%') AS 'percentage' FROM restaurant m JOIN type_of_business n ON m.id_type_of_business = n.id_type_of_business $idnegara GROUP BY n.nama_type_of_business");
                    while ($juto=mysqli_fetch_array($sql)) {
											$jumbt = $juto['jumlah'] + $jumbt;
                      echo"
                        <tr>
                          <td width='40%'>
                            <a href='https://www.foodieguidances.com/pages/restaurant/search/?business_type=$juto[id_type_of_business]'>
                             	$juto[nama_type_of_business]
                            </a>
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
						<tr style="border-top: 1px solid; padding: 5px 0">
							<td> 
								<label>Sum</label>
							</td>
						 	<td>
						 		<label><?php echo $jumbt ; ?></label>
						 	</td>
							<td>
								<label> 100% </label>
							</td>
						</tr>
                   </table>
                </div>
              </div>

				<div class="form-group">
	                <label class="col-sm-1 control-label">Shopping Mall</label>
	                <div class="col-sm-10"> :
						<?php
							$idnegara = "";
							$idnegaras ="";
							if (!empty($_GET['negara'])) {
								$idnegara = "WHERE id_negara='$_GET[negara]'";
								$idnegaras = "WHERE id_negara='$_GET[negara]'";
							}
						?>
						<?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT COUNT(*) FROM restaurant $idnegara GROUP BY id_mall")); echo "$juto"; ?>
	                </div>
	            </div>
				<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Shoping Mall</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
                    <?php
                    $jumsma ="";
						$idnegara = "";
						if (!empty($_GET['negara'])) {
							$idnegara = "WHERE r.id_mall !='' AND r.id_negara='$_GET[negara]'";
						}
                    $sql=mysqli_query($koneksi, "SELECT ne.id_negara, ne.nama_negara, pro.id_propinsi, k.id_kota, k.nama_kota,m.id_mall, m.nama_mall ,COUNT(*) as jumlah ,(SELECT COUNT(*) FROM restaurant ) AS total, concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM restaurant $idnegaras)) * 100 ),2),'%') AS 'percentage' 
                    	FROM restaurant r 
                    	LEFT JOIN negara ne ON r.id_negara   =  ne.id_negara
						LEFT JOIN propinsi pro ON r.id_propinsi = pro.id_propinsi 
                		left JOIN kota k ON r.id_kota = k.id_kota 
                    	left join mall m on r.id_mall=m.id_mall $idnegara GROUP BY r.id_mall");
                    while ($juto=mysqli_fetch_array($sql)) {
											$jumsma = $juto['jumlah'] + $jumsma;
                      echo"
                        <tr>
                          <td width='40%'>";
										if ($juto['nama_mall']=='') {
											echo "Unknown";
										}else {
											echo"
                            <a href='https://www.foodieguidances.com/pages/restaurant/search/?country=$juto[id_negara]&state=$juto[id_propinsi]&city=$juto[id_kota]&mall=$juto[id_mall]'>
                            	$juto[nama_negara] / $juto[nama_kota] - $juto[nama_mall]
                            </a>"; }
                            echo"
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $jumsma; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Landmark</label>
                <div class="col-sm-10">
                  :
									<?php
										$idnegara = "";
										$idnegaras ="";
										if (!empty($_GET['negara'])) {
											$idnegara = "WHERE id_negara='$_GET[negara]'";
											$idnegaras = "WHERE id_negara='$_GET[negara]'";
										}
									?>
									<?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT COUNT(*) FROM restaurant $idnegaras GROUP BY id_landmark")); echo "$juto"; ?>

                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Landmark</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
                    <?php
                    $jumland = "";
                    $sql=mysqli_query($koneksi, "SELECT m.id_landmark, m.nama_landmark ,COUNT(*) as jumlah ,(SELECT COUNT(*) FROM restaurant ) AS total, concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM restaurant $idnegaras )) * 100 ),2),'%') AS 'percentage' FROM restaurant r left join landmark m on r.id_landmark=m.id_landmark $idnegara GROUP BY r.id_landmark");
                    while ($juto=mysqli_fetch_array($sql)) {
											$jumland = $juto['jumlah'] + $jumland;
                      echo"
                        <tr>
                          <td width='40%'>";
													if ($juto['nama_landmark']=='') {
														echo "Unknown";
													}echo"
                            <a href='https://www.foodieguidances.com/pages/restaurant/search/?landmark=$juto[id_landmark]'>
                             	$juto[nama_landmark]
                            </a>
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $jumland; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>

										 </tr>
                   </table>
                </div>
              </div>
							<?php
								$idnegara = "";
								$idnegaras ="";
								if (!empty($_GET['negara'])) {
									$idnegara = "WHERE id_negara='$_GET[negara]'";
									$idnegaras = "AND id_negara='$_GET[negara]'";
								}
							?>
							<div class="form-group">
                <label class="col-sm-1 control-label">Telephone</label>
                <div class="col-sm-10">
                  :  <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT COUNT(*) FROM restaurant where telephone!='' $idnegaras GROUP BY telephone")); echo "$juto"; ?>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Facemail</label>
                <div class="col-sm-10">
                  :  <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT COUNT(*) FROM restaurant where fax!='' $idnegaras GROUP BY fax")); echo "$juto"; ?>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Postal Code</label>
                <div class="col-sm-10">
                  :  <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT COUNT(*) FROM restaurant where postal_code!='' $idnegaras GROUP BY postal_code")); echo "$juto"; ?>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Reservation</label>
                <div class="col-sm-10">
                  :  <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT COUNT(*) FROM restaurant where reservation_phone!='' $idnegaras GROUP BY reservation_phone")); echo "$juto"; ?>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Email</label>
                <div class="col-sm-10">
                  :  <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT COUNT(*) FROM restaurant where email_resto!='' $idnegaras GROUP BY email_resto")); echo "$juto"; ?>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Web</label>
                <div class="col-sm-10">
                  :  <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT COUNT(*) FROM restaurant where web!='' $idnegaras GROUP BY web")); echo "$juto"; ?>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Social Media</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  Facebook : <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT COUNT(*) FROM restaurant where facebook!='' $idnegaras GROUP BY facebook")); echo "$juto"; ?>
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  Twitter : <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT COUNT(*) FROM restaurant where twitter!='' $idnegaras GROUP BY twitter")); echo "$juto"; ?>
                </div>
              </div>
							<div class="form-group">
								<label class="col-sm-1 control-label"></label>
								<div class="col-sm-10">
									Instagram : <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT COUNT(*) FROM restaurant where instagram!='' $idnegaras GROUP BY instagram")); echo "$juto"; ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-1 control-label"></label>
								<div class="col-sm-10">
									Pinterest : <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT COUNT(*) FROM restaurant where pinterest!='' $idnegaras GROUP BY pinterest")); echo "$juto"; ?>
								</div>
							</div>
							<div class="form-group">
                <label class="col-sm-1 control-label">Branch</label>
                <div class="col-sm-10">
                  :  <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT COUNT(*) FROM restaurant where branch_name!='' $idnegaras GROUP BY branch_name")); echo "$juto"; ?>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Pork Serving</label>
                <div class="col-sm-10">
                  :
									<?php
									$idnegara = "";
									$idnegaras ="";
									if (!empty($_GET['negara'])) {
										$idnegara = "WHERE r.id_negara='$_GET[negara]'";
										$idnegaras = "WHERE id_negara='$_GET[negara]'";
									}
									?>
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Pork Serving</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
                    <?php
                    $jumpork = ""; 
                    $sql=mysqli_query($koneksi, "SELECT pork_serving, COUNT(*) as jumlah ,(SELECT COUNT(*) FROM restaurant ) AS total, concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM restaurant $idnegaras )) * 100 ),2),'%') AS 'percentage' FROM restaurant r $idnegara GROUP BY r.pork_serving");
										while ($juto=mysqli_fetch_array($sql)) {
											$jumpork = $juto['jumlah'] + $jumpork;
                      echo"
                        <tr>
                          <td width='40%'>
                            <a href='https://www.foodieguidances.com/pages/restaurant/search/?pork=$juto[pork_serving]'>
                            	$juto[pork_serving]
                            </a>
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $jumpork; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Alcohol Serving</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Alcohol Serving</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
                    <?php
										$jumas = "";
                    $sql=mysqli_query($koneksi, "SELECT alcohol_serving, COUNT(*) as jumlah ,(SELECT COUNT(*) FROM restaurant ) AS total,
										concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM restaurant $idnegaras )) * 100 ),2),'%') AS 'percentage' FROM restaurant r $idnegara  GROUP BY r.alcohol_serving
										ORDER BY `r`.`alcohol_serving`  DESC");
                    while ($juto=mysqli_fetch_array($sql)) {
											$jumas= $juto['jumlah'] + $jumas;
                      echo"
                        <tr>
                          <td width='40%'>";
													if ($juto['alcohol_serving']=='') {
														echo "Unknown";
													}else {echo"
													<a href='https://www.foodieguidances.com/pages/restaurant/search/?alcohol=$juto[alcohol_serving]'>
														$juto[alcohol_serving]
													</a>
													";} echo"
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $jumas; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Price Index</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Price Index</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras ="";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE m.id_negara='$_GET[negara]'";
												$idnegaras = "WHERE id_negara='$_GET[negara]'";
											}
										?>
                    <?php
                    $jumpi = "";
                    $sql=mysqli_query($koneksi, "SELECT n.id_price_index, n.nama_price_index,(SELECT COUNT(*) FROM restaurant) as total,COUNT(*) as jumlah ,
										concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM restaurant $idnegara)) * 100 ),2),'%') AS 'percentage' FROM restaurant m
										JOIN price_index n ON m.id_price_index = n.id_price_index $idnegaras GROUP BY n.nama_price_index");
                    while ($juto=mysqli_fetch_array($sql)) {
											$jumpi = $juto['jumlah'] + $jumpi;
                      echo"
                        <tr>
                          	<td width='40%'>
	                            <a href='https://www.foodieguidances.com/pages/restaurant/search/?price_index=$juto[id_price_index]'>
	                            	$juto[nama_price_index]
	                            </a>
                          	</td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $jumpi; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Suitable For</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Suitable For</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
										$idnegara = "";
										$idnegaras ="";
										if (!empty($_GET['negara'])) {
											$idnegara = "WHERE m.id_negara='$_GET[negara]'";
											$idnegaras = "WHERE id_negara='$_GET[negara]'";
										}
										?>
                    <?php
                    $jumsf ="";
                    $sql=mysqli_query($koneksi, "SELECT n.id_suitable_for, n.nama_suitable_for,(SELECT COUNT(*) FROM restaurant) as total,COUNT(*) as jumlah ,
										concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM restaurant $idnegaras)) * 100 ),2),'%') AS 'percentage' FROM restaurant m
										JOIN suitable_for n ON m.id_suitable_for = n.id_suitable_for $idnegara GROUP BY n.nama_suitable_for");
                    while ($juto=mysqli_fetch_array($sql)) {
											$jumsf = $juto['jumlah'] + $jumsf;
                      echo"
                        <tr>
                          <td width='40%'>
                             <a href='https://www.foodieguidances.com/pages/restaurant/search/?suitable_for=$juto[id_suitable_for]'>
                             $juto[nama_suitable_for]
                             </a>
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $jumsf; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Wifi</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Wifi</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras ="";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE m.id_negara='$_GET[negara]'";
												$idnegaras = "WHERE id_negara='$_GET[negara]'";
											}
										?>
                    <?php
                    $jumwf ="";
                    $sql=mysqli_query($koneksi, "SELECT n.id_wifi, n.nama_wifi,(SELECT COUNT(*) FROM restaurant) as total,COUNT(*) as jumlah ,
										concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM restaurant $idnegara )) * 100 ),2),'%') AS 'percentage' FROM restaurant m
										JOIN wifi n ON m.id_wifi = n.id_wifi $idnegaras GROUP BY n.nama_wifi");
                    while ($juto=mysqli_fetch_array($sql)) {
											$jumwf = $juto['jumlah'] + $jumwf;
                      echo"
                        <tr>
                          <td width='40%'>
                          	<a href='https://www.foodieguidances.com/pages/restaurant/search/?wifi=$juto[id_wifi]'>
                            	$juto[nama_wifi]
                            </a>
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $jumwf; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Payment</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Payment</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras ="";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE m.id_negara='$_GET[negara]'";
												$idnegaras = "WHERE id_negara='$_GET[negara]'";
											}
										?>
                    <?php
                    $totalnto = "";
                    $sql=mysqli_query($koneksi, "SELECT n.id_term_of_payment, n.nama_term_of_payment,(SELECT COUNT(*) FROM restaurant) as total,COUNT(*) as jumlah ,
										concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM restaurant $idnegaras )) * 100 ),2),'%') AS 'percentage' FROM restaurant m
										JOIN term_of_payment n ON m.id_wifi = n.id_term_of_payment $idnegara GROUP BY n.nama_term_of_payment");
                    while ($juto=mysqli_fetch_array($sql)) {
											$totalnto = $juto['jumlah'] + $totalnto;
                      echo"
                        <tr>
                          <td width='40%'>
                          	<a href='https://www.foodieguidances.com/pages/restaurant/search/?term_of_payment=$juto[id_term_of_payment]'>
                             	$juto[nama_term_of_payment]
                            </a>
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
						<tr style="border-top: 1px solid; padding: 5px 0">
							 <td> <label>Sum</label>
							 </td>
						 	<td>
						 		<label><?php echo $totalnto; ?></label>
						 	</td>
							<td>
								<label> 100% </label>
							</td>
						</tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Serving Time</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Serving Time</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras ="";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE m.id_negara='$_GET[negara]'";
												$idnegaras = "WHERE id_negara='$_GET[negara]'";
											}
										?>
                    <?php
										$jumnst = "";
                    $sql=mysqli_query($koneksi, "SELECT n.id_serving_time, n.nama_serving_time,(SELECT COUNT(*) FROM restaurant) as total,COUNT(*) as jumlah ,
										concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM restaurant $idnegaras)) * 100 ),2),'%') AS 'percentage' FROM restaurant m
										JOIN serving_time n ON m.id_serving_time = n.id_serving_time $idnegara GROUP BY n.nama_serving_time ORDER BY `n`.`id_serving_time` ASC");

                    while ($juto=mysqli_fetch_array($sql)) {
											$jumnst = $juto['jumlah'] + $jumnst;
                      echo"
                        <tr>
                          <td width='40%'>
                          	<a href='https://www.foodieguidances.com/pages/restaurant/search/?serving_time=$juto[id_serving_time]'>
                            	$juto[nama_serving_time]
                            </a>
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $jumnst	; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Type of Service</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Type of Service</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras ="";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE r.id_negara='$_GET[negara]'";
												$idnegaras = "WHERE id_negara='$_GET[negara]'";
											}
										?>
                    <?php
                    $totaltos = "";
                    $sql=mysqli_query($koneksi, "SELECT m.id_type_of_service, m.nama_type_of_service ,COUNT(*) as jumlah ,(SELECT COUNT(*) FROM restaurant ) AS total, concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM restaurant $idnegaras )) * 100 ),2),'%') AS 'percentage' FROM restaurant r left join type_of_service m on r.id_type_of_service=m.id_type_of_service $idnegara GROUP BY r.id_type_of_service");
                    while ($juto=mysqli_fetch_array($sql)) {
											$totaltos = $juto['jumlah'] + $totaltos;
                      echo"
                        <tr>
                          <td width='40%'>";
													if ($juto['nama_type_of_service']=='') {
														echo "Unknown";
													}echo"
                            <a href='https://www.foodieguidances.com/pages/restaurant/search/?type_of_service=$juto[id_type_of_service]'>
                            	$juto[nama_type_of_service]
                            </a>
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $totaltos; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Air Conditioning</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Air Conditioning</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras ="";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE r.id_negara='$_GET[negara]'";
												$idnegaras = "WHERE id_negara='$_GET[negara]'";
											}
										?>
                    <?php
                    $totalac = "";
                    $sql=mysqli_query($koneksi, "SELECT m.id_air_conditioning, m.nama_air_conditioning ,COUNT(*) as jumlah ,(SELECT COUNT(*) FROM restaurant ) AS total, concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM restaurant $idnegaras )) * 100 ),2),'%') AS 'percentage' FROM restaurant r left join air_conditioning m on r.id_air_conditioning=m.id_air_conditioning $idnegara GROUP BY r.id_air_conditioning");
                    while ($juto=mysqli_fetch_array($sql)) {
											$totalac = $juto['jumlah'] + $totalac;
                      echo"
                        <tr>
                          <td width='40%'>";
													if ($juto['nama_air_conditioning']=='') {
														echo "Unknown";
													}echo"
                            <a href='https://www.foodieguidances.com/pages/restaurant/search/?air_conditioning=$juto[id_air_conditioning]'>
                            	$juto[nama_air_conditioning]
                            </a>
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $totalac; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Heating System</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Heating System</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras ="";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE r.id_negara='$_GET[negara]'";
												$idnegaras = "WHERE id_negara='$_GET[negara]'";
											}
										?>
                    <?php
                    $totalhs ="";
                    $sql=mysqli_query($koneksi, "SELECT m.id_heating_system, m.nama_heating_system ,COUNT(*) as jumlah ,(SELECT COUNT(*) FROM restaurant ) AS total,
										concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM restaurant $idnegaras )) * 100 ),2),'%') AS 'percentage'
										FROM restaurant r left join heating_system m on r.id_heating_system=m.id_heating_system $idnegara GROUP BY r.id_heating_system");
                    while ($juto=mysqli_fetch_array($sql)) {
											$totalhs = $juto['jumlah'] + $totalhs;
                      echo"
                        <tr>
                          <td width='40%'>";
													if ($juto['nama_heating_system']=='') {
														echo "Unknown";
													}echo"
							<a href='https://www.foodieguidances.com/pages/restaurant/search/?heating_system=$juto[id_heating_system]'>
                            	$juto[nama_heating_system]
                            </a>
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $totalhs; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Premise's Security</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Premise's Security</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras ="";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE r.id_negara='$_GET[negara]'";
												$idnegaras = "WHERE id_negara='$_GET[negara]'";
											}
										?>
                    <?php
                    $totalps ="";
                    $sql=mysqli_query($koneksi, "SELECT m.id_premise_security, m.nama_premise_security ,COUNT(*) as jumlah ,(SELECT COUNT(*) FROM restaurant ) AS total,
										concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM restaurant $idnegara )) * 100 ),2),'%') AS 'percentage'
										FROM restaurant r left join premise_security m on r.id_premise_security=m.id_premise_security $idnegaras GROUP BY r.id_premise_security");
                    while ($juto=mysqli_fetch_array($sql)) {
											$totalps = $juto['jumlah'] + $totalps;
                      echo"
                        <tr>
                          <td width='40%'>";
													if ($juto['nama_premise_security']=='') {
														echo "Unknown";
													}echo"
                            <a href='https://www.foodieguidances.com/pages/restaurant/search/?premise_security=$juto[id_premise_security]'>
                             $juto[nama_premise_security]
                            </a>
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $totalps; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Parking Spaces</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Parking Spaces</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras ="";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE r.id_negara='$_GET[negara]'";
												$idnegaras = "WHERE id_negara='$_GET[negara]'";
											}
										?>
                    <?php
                    $totalpss ="";
                    $sql=mysqli_query($koneksi, "SELECT m.id_parking_spaces, m.nama_parking_spaces ,COUNT(*) as jumlah ,(SELECT COUNT(*) FROM restaurant ) AS total,
										concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM restaurant $idnegaras )) * 100 ),2),'%') AS 'percentage'
										FROM restaurant r left join parking_spaces m on r.id_parking_spaces=m.id_parking_spaces $idnegara GROUP BY r.id_parking_spaces");
                    while ($juto=mysqli_fetch_array($sql)) {
											$totalpss = $juto['jumlah'] + $totalpss;
                      echo"
                        <tr>
                          <td width='40%'>";
													if ($juto['nama_parking_spaces']=='') {
														echo "Unknown";
													}echo"
							<a href='https://www.foodieguidances.com/pages/restaurant/search/?parking_spaces=$juto[id_parking_spaces]'>
                            	$juto[nama_parking_spaces]
                            </a>
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $totalpss; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Premise's Fire Safety</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Premise's Fire Safety</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras ="";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE r.id_negara='$_GET[negara]'";
												$idnegaras = "WHERE id_negara='$_GET[negara]'";
											}
										?>
                    <?php
										$totalpfs = "";
                    $sql=mysqli_query($koneksi, "SELECT m.id_premise_fire_safety, m.nama_premise_fire_safety ,COUNT(*) as jumlah ,(SELECT COUNT(*) FROM restaurant ) AS total,
										concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM restaurant $idnegaras )) * 100 ),2),'%') AS 'percentage'
										FROM restaurant r left join premise_fire_safety m on r.id_premise_fire_safety=m.id_premise_fire_safety $idnegara GROUP BY r.id_premise_fire_safety");
                    while ($juto=mysqli_fetch_array($sql)) {
											$totalpfs = $juto['jumlah'] + $totalpfs;
                      echo"
                        <tr>
                          <td width='40%'>";
													if ($juto['nama_premise_fire_safety']=='') {
														echo "Unknown";
													}echo"
							<a href='https://www.foodieguidances.com/pages/restaurant/search/?premise_fire_safety=$juto[id_premise_fire_safety]'>
                            	$juto[nama_premise_fire_safety]
                            </a>
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $totalpfs; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Premise's Hygiene</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Premise's Hygiene</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras ="";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE r.id_negara='$_GET[negara]'";
												$idnegaras = "WHERE id_negara='$_GET[negara]'";
											}
										?>
                    <?php
										$totalph ="";
                    $sql=mysqli_query($koneksi, "SELECT m.id_premise_hygiene, m.nama_premise_hygiene ,COUNT(*) as jumlah ,(SELECT COUNT(*) FROM restaurant ) AS total,
										concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM restaurant $idnegaras )) * 100 ),2),'%') AS 'percentage'
										FROM restaurant r left join premise_hygiene m on r.id_premise_hygiene=m.id_premise_hygiene $idnegara GROUP BY r.id_premise_hygiene");
                    while ($juto=mysqli_fetch_array($sql)) {
											$totalph = $juto['jumlah'] + $totalph;
                      echo"
                        <tr>
                          <td width='40%'>";
													if ($juto['nama_premise_hygiene']=='') {
														echo "Unknown";
													}echo"
							<a href='https://www.foodieguidances.com/pages/restaurant/search/?premise_hygiene=$juto[id_premise_hygiene]'>
                            	$juto[nama_premise_hygiene]
                            </a>
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $totalph; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Ambience</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Ambience</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras ="";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE r.id_negara='$_GET[negara]'";
												$idnegaras = "WHERE id_negara='$_GET[negara]'";
											}
										?>
                    <?php
										$totalam = "";
                    $sql=mysqli_query($koneksi, "SELECT m.id_ambience, m.nama_ambience ,COUNT(*) as jumlah ,(SELECT COUNT(*) FROM restaurant ) AS total,
										concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM restaurant $idnegaras)) * 100 ),2),'%') AS 'percentage'
										FROM restaurant r left join ambience m on r.id_ambience=m.id_ambience $idnegara GROUP BY r.id_ambience");
                    while ($juto=mysqli_fetch_array($sql)) {
											$totalam = $juto['jumlah'] + $totalam;
                      echo"
                        <tr>
                          <td width='40%'>";
													if ($juto['nama_ambience']=='') {
														echo "Unknown";
													}echo"
							<a href='https://www.foodieguidances.com/pages/restaurant/search/?ambience=$juto[id_ambience]'>
                            	$juto[nama_ambience]
                            </a>
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $totalam; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Attire</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Attire</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras ="";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE r.id_negara='$_GET[negara]'";
												$idnegaras = "WHERE id_negara='$_GET[negara]'";
											}
										?>
                    <?php
										$totalat = "";
                    $sql=mysqli_query($koneksi, "SELECT m.id_attire, m.nama_attire ,COUNT(*) as jumlah ,(SELECT COUNT(*) FROM restaurant ) AS total,
										concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM restaurant $idnegaras )) * 100 ),2),'%') AS 'percentage'
										FROM restaurant r left join attire m on r.id_attire=m.id_attire $idnegara GROUP BY r.id_attire");
                    while ($juto=mysqli_fetch_array($sql)) {
											$totalat = $juto['jumlah'] + $totalat;
                      echo"
                        <tr>
                          <td width='40%'>";
													if ($juto['nama_attire']=='') {
														echo "Unknown";
													}echo"
							<a href='https://www.foodieguidances.com/pages/restaurant/search/?attire=$juto[id_attire]'>
                            	$juto[nama_attire]
                            </a>
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $totalat; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Clean Washroom</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Clean Washroom</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
						<?php
							$idnegara = "";
							$idnegaras ="";
							if (!empty($_GET['negara'])) {
								$idnegara = "WHERE r.id_negara='$_GET[negara]'";
								$idnegaras = "WHERE id_negara='$_GET[negara]'";
							}
						?>
                    <?php
					$totalcw = "";
                    $sql=mysqli_query($koneksi, "SELECT m.id_clean_washroom, m.nama_clean_washroom ,COUNT(*) as jumlah ,(SELECT COUNT(*) FROM restaurant ) AS total,
										concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM restaurant $idnegaras)) * 100 ),2),'%') AS 'percentage'
										FROM restaurant r left join clean_washroom m on r.id_clean_washroom=m.id_clean_washroom $idnegara GROUP BY r.id_clean_washroom");
                    while ($juto=mysqli_fetch_array($sql)) {
											$totalcw = $juto['jumlah'] + $totalcw;
                      echo"
                        <tr>
                          <td width='40%'>";
													if ($juto['nama_clean_washroom']=='') {
														echo "Unknown";
													}echo"
							<a href='https://www.foodieguidances.com/pages/restaurant/search/?clean_washroom=$juto[id_clean_washroom]'>
                            	$juto[nama_clean_washroom]
                            </a>
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
						 <tr style="border-top: 1px solid; padding: 5px 0">
							 <td> <label>Sum</label>
							 </td>
						 	<td>
						 		<label><?php echo $totalcw; ?></label>
						 	</td>
							<td>
								<label> 100% </label>
							</td>
						 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Tables Availability</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Tables Availability</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras ="";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE r.id_negara='$_GET[negara]'";
												$idnegaras = "WHERE id_negara='$_GET[negara]'";
											}
										?>
                    <?php
										$totalta = "";
                    $sql=mysqli_query($koneksi, "SELECT m.id_tables_availability, m.nama_tables_availability ,COUNT(*) as jumlah ,(SELECT COUNT(*) FROM restaurant ) AS total,
										concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM restaurant $idnegaras)) * 100 ),2),'%') AS 'percentage'
										FROM restaurant r left join tables_availability m on r.id_tables_availability=m.id_tables_availability $idnegara GROUP BY r.id_tables_availability");
                    while ($juto=mysqli_fetch_array($sql)) {
											$totalta = $juto['jumlah'] + $totalta;
                      echo"
                        <tr>
                          <td width='40%'>";
													if ($juto['nama_tables_availability']=='') {
														echo "Unknown";
													}echo"
                            <a href='https://www.foodieguidances.com/pages/restaurant/search/?tables_availability=$juto[id_tables_availability]'>
                            	$juto[nama_tables_availability]
                            </a>
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $totalta; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Noise Level</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Noise Level</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras ="";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE r.id_negara='$_GET[negara]'";
												$idnegaras = "WHERE id_negara='$_GET[negara]'";
											}
										?>
                    <?php
										$totalnl = "";
                    $sql=mysqli_query($koneksi, "SELECT m.id_noise_level, m.nama_noise_level ,COUNT(*) as jumlah ,(SELECT COUNT(*) FROM restaurant ) AS total,
										concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM restaurant $idnegaras )) * 100 ),2),'%') AS 'percentage'
										FROM restaurant r left join noise_level m on r.id_noise_level=m.id_noise_level $idnegara GROUP BY r.id_noise_level");
                    while ($juto=mysqli_fetch_array($sql)) {
											$totalnl = $juto['jumlah'] + $totalnl;
                      echo"
                        <tr>
                          <td width='40%'>";
													if ($juto['nama_noise_level']=='') {
														echo "Unknown";
													}echo"
                            <a href='https://www.foodieguidances.com/pages/restaurant/search/?noise_level=$juto[id_noise_level]'>
                            	$juto[nama_noise_level]
                            </a>
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $totalnl; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Waiter Tipping</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Waiter Tipping</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras ="";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE r.id_negara='$_GET[negara]'";
												$idnegaras = "WHERE id_negara='$_GET[negara]'";
											}
										?>
                    <?php
										$totalwt = "";
                    $sql=mysqli_query($koneksi, "SELECT m.id_waiter_tipping, m.nama_waiter_tipping ,COUNT(*) as jumlah ,(SELECT COUNT(*) FROM restaurant ) AS total,
										concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM restaurant $idnegaras )) * 100 ),2),'%') AS 'percentage'
										FROM restaurant r left join waiter_tipping m on r.id_waiter_tipping=m.id_waiter_tipping $idnegara GROUP BY r.id_waiter_tipping");
                    while ($juto=mysqli_fetch_array($sql)) {
											$totalwt = $juto['jumlah'] + $totalwt;
                      echo"
                        <tr>
                          <td width='40%'>";
													if ($juto['nama_waiter_tipping']=='') {
														echo "Unknown";
													}echo"
                            <a href='https://www.foodieguidances.com/pages/restaurant/search/?waiter_tipping=$juto[id_waiter_tipping]'>
                            	$juto[nama_waiter_tipping]
                            </a>
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $totalwt; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Map</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Map</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras ="";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE r.id_negara='$_GET[negara]'";
												$idnegaras = "AND id_negara='$_GET[negara]'";
											}
										?>
                    <?php
                    $sqlmap=mysqli_query($koneksi, "SELECT * FROM restaurant where latitude!='' $idnegaras");
										$sqlamap=mysqli_query($koneksi, "SELECT * FROM restaurant where latitude='' $idnegaras");
											$yes= mysqli_num_rows($sqlmap);
											$no= mysqli_num_rows($sqlamap);
											$totalmap = ($yes+$no);
                      echo"
                        <tr>
                          <td width='40%'>
							Yes
                          </td>
                          <td width='30%'>
                            $yes
                          </td>
                          <td width='30%'>
                            ".round($yes/$totalmap * '100', 2)."%
                          </td>
                        </tr>
												<tr>
                          <td width='40%'>
														No
                          </td>
                          <td width='30%'>
                            $no
                          </td>
                          <td width='30%'>
                            ".round($no/$totalmap * '100',2)."%
                          </td>
                        </tr>
                      ";
                    ;
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $totalmap; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Restaurant Description</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Restaurant Description</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
										</tr>
										<?php
											$idnegara = "";
											$idnegaras ="";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE r.id_negara='$_GET[negara]'";
												$idnegaras = "AND id_negara='$_GET[negara]'";
											}
										?>
                    <?php
                    $sql=mysqli_query($koneksi, "SELECT * FROM restaurant where restaurant_description!='' $idnegaras");
										$sqla=mysqli_query($koneksi, "SELECT * FROM restaurant where restaurant_description='' $idnegaras");

											$yes= mysqli_num_rows($sql);
											$no= mysqli_num_rows($sqla);
											$total=($yes+$no);
                      echo"
                        <tr>
                          <td width='40%'>
														Yes
                          </td>
                          <td width='30%'>
                            $yes
                          </td>
                          <td width='30%'>
                            ".round($yes/$total * '100',2)."%
                          </td>
                        </tr>
												<tr>
                          <td width='40%'>
														No
                          </td>
                          <td width='30%'>
                            $no
                          </td>
                          <td width='30%'>
                            ".round($no/$total * '100',2)."%
                          </td>
                        </tr>
                      ";
                    ;
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $total; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Business Status</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Business Status</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras ="";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE r.id_negara='$_GET[negara]'";
												$idnegaras = "AND id_negara='$_GET[negara]'";
											}
										?>
                    <?php
											$unk=mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM restaurant where business_status='' $idnegaras"));
											$ongoing=mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM restaurant where business_status='Ongoing' $idnegaras"));
											$renovation=mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM restaurant where business_status='Renovation' $idnegaras"));
											$relocate=mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM restaurant where business_status='Relocate' $idnegaras"));
											$ceased=mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM restaurant where business_status='Ceased Operation' $idnegaras"));
											$total=($ongoing+$renovation+$relocate+$ceased+$unk);
                      echo"
												<tr>
													<td width='40%'>
														Unknown
													</td>
													<td width='30%'>
														$unk
													</td>
													<td width='30%'>
														".round($unk/$total * '100',2)."%
													</td>
												</tr>
												<tr>
                          <td width='40%'>
														Ongoing
                          </td>
                          <td width='30%'>
                            $ongoing
                          </td>
                          <td width='30%'>
                            ".round($ongoing/$total * '100',2)."%
                          </td>
                        </tr>
												<tr>
                          <td width='40%'>
														Renovation
                          </td>
                          <td width='30%'>
                            $renovation
                          </td>
                          <td width='30%'>
                            ".round($renovation/$total * '100',2)."%
                          </td>
                        </tr>
												<tr>
                          <td width='40%'>
														Relocate
                          </td>
                          <td width='30%'>
                            $relocate
                          </td>
                          <td width='30%'>
                            ".round($relocate/$total * '100',2)."%
                          </td>
                        </tr>
												<tr>
                          <td width='40%'>
														Ceased Operation
                          </td>
                          <td width='30%'>
                            $ceased
                          </td>
                          <td width='30%'>
                            ".round($ceased/$total * '100',2)."%
                          </td>
                        </tr>
                      ";
                    ;
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $total; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-2 control-label">Business Status Description</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Business Status Description</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras ="";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE r.id_negara='$_GET[negara]'";
												$idnegaras = "AND id_negara='$_GET[negara]'";
											}
										?>
                    <?php
                    $sql=mysqli_query($koneksi, "SELECT * FROM restaurant where business_status_description!='' $idnegaras");
										$sqla=mysqli_query($koneksi, "SELECT * FROM restaurant where business_status_description='' $idnegaras");

											$yes= mysqli_num_rows($sql);
											$no= mysqli_num_rows($sqla);
											$total=($yes+$no);
                      echo"
                        <tr>
                          <td width='40%'>
														Yes
                          </td>
                          <td width='30%'>
                            $yes
                          </td>
                          <td width='30%'>
                            ".round($yes/$total * '100',2)."%
                          </td>
                        </tr>
												<tr>
                          <td width='40%'>
														No
                          </td>
                          <td width='30%'>
                            $no
                          </td>
                          <td width='30%'>
                            ".round($no/$total * '100',2)."%
                          </td>
                        </tr>
                      ";
                    ;
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $total; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-2 control-label">Features and Facility</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Features and Facility</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras ="";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE id_negara='$_GET[negara]'";
												$idnegaras = "AND id_negara='$_GET[negara]'";
											}
										?>
                    <?php
	                    $sql=mysqli_query($koneksi, "SELECT * FROM facility");
											$sqla=mysqli_query($koneksi, "SELECT * FROM facility");
											$no=1;
											$totalnf = "";
											$totalnfa = "";
											while ($ja=mysqli_fetch_array($sqla)) {
												$urutana = mysqli_num_rows(mysqli_query($koneksi, "SELECT * from restaurant where facility like '%$ja[nama_facility]%' $idnegaras"));
												$totalnfa  = $urutana + $totalnfa;
											}
	                    while ($j=mysqli_fetch_array($sql)) {
												$urutan = mysqli_num_rows(mysqli_query($koneksi, "SELECT * from restaurant where facility like '%$j[nama_facility]%' $idnegaras"));
												$totalnf  = $urutan + $totalnf;

												if ($urutan <> 0) {

		                      echo"
		                        <tr>
		                          <td width='40%'>";
															if ($j['nama_facility']=='') {
																echo "Unknown";
															}echo"
		                            <a href='https://www.foodieguidances.com/pages/restaurant/search/?facility[]=$j[nama_facility]'>
		                            	$j[nama_facility]
		                            </a>
		                          </td>
		                          <td width='30%'>
		                            $urutan
		                          </td>
		                          <td width='30%'>
		                            ".round($urutan/$totalnfa * '100',2)."%
		                          </td>
		                        </tr>
		                      ";
												}
												$no++;
	                    };
                    ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php $la=$totalnf; echo $totalnf; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-2 control-label">Cuisine</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Cuisine</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras ="";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE id_negara='$_GET[negara]'";
												$idnegaras = "AND id_negara='$_GET[negara]'";
											}
										?>
                    <?php
                    $sql  = mysqli_query($koneksi, "SELECT * FROM cuisine");
										$sqla = mysqli_query($koneksi, "SELECT * FROM cuisine");
										$no=1;
										$totalcu  = "";
										$totalcua = "";
										while ($j=mysqli_fetch_array($sqla)) {
											$urutana = mysqli_num_rows(mysqli_query($koneksi, "SELECT * from restaurant where cuisine like '%$j[nama_cuisine]%' $idnegaras"));
											$totalcua = $urutana + $totalcua;
										}
                    while ($j=mysqli_fetch_array($sql)) {
											$urutan = mysqli_num_rows(mysqli_query($koneksi, "SELECT * from restaurant where cuisine like '%$j[nama_cuisine]%' $idnegaras"));
											$totalcu = $urutan + $totalcu;
											if ($urutan <> 0) {
	                      echo"
	                        <tr>
	                          <td width='40%'>";
														if ($j['nama_cuisine']=='') {
															echo "Unknown";
														}echo"
	                            <a href='https://www.foodieguidances.com/pages/restaurant/search/?cuisine[]=$j[nama_cuisine]'>
	                            	$j[nama_cuisine]
	                            </a>
	                          </td>
	                          <td width='30%'>
	                            $urutan
	                          </td>
	                          <td width='30%'>
	                            ".round($urutan/$totalcua * '100',2)."%
	                          </td>
	                        </tr>
	                      ";
											}
											$no++;
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $totalcu; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Serving</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-11">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Serving</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras ="";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE id_negara='$_GET[negara]'";
												$idnegaras = "AND id_negara='$_GET[negara]'";
											}
										?>
                    <?php
                    $sql=mysqli_query($koneksi, "SELECT * FROM serving");
										$sqla=mysqli_query($koneksi, "SELECT * FROM serving");
										$no=1;
										$totalsera = "";
										$totalser ="";
										while ($j=mysqli_fetch_array($sqla)) {
											$urutana = mysqli_num_rows(mysqli_query($koneksi, "SELECT * from restaurant where serving like '%$j[nama_serving]%' $idnegaras"));
											$totalsera = $urutana + $totalsera;
										}
                    while ($j=mysqli_fetch_array($sql)) {
											$urutan = mysqli_num_rows(mysqli_query($koneksi, "SELECT * from restaurant where serving like '%$j[nama_serving]%' $idnegaras"));
											$totalser = $urutan + $totalser;
											if ($urutan <> 0) {
	                      echo"
	                        <tr>
	                          <td width='40%'>";
														if ($j['nama_serving']=='') {
															echo "Unknown";
														}echo"
	                            <a href='https://www.foodieguidances.com/pages/restaurant/search/?serving[]=$j[nama_serving]'>
	                            	$j[nama_serving]
	                            </a>
	                          </td>
	                          <td width='30%'>
	                            $urutan
	                          </td>
	                          <td width='30%'>
	                            ".round($urutan/$totalsera * '100',2)."%
	                          </td>
	                        </tr>
	                      ";
											}
											$no++;
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $totalsera; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Type of Serving</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-11">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Type of Serving</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras ="";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE id_negara='$_GET[negara]'";
												$idnegaras = "AND id_negara='$_GET[negara]'";
											}
										?>
                    <?php
                    $sql=mysqli_query($koneksi, "SELECT * FROM type_of_serving");
										$sqla=mysqli_query($koneksi, "SELECT * FROM type_of_serving");
										$no=1;
										$totaltosa = "";
										$totaltosaa = "";
										while ($j=mysqli_fetch_array($sqla)) {
											$urutana = mysqli_num_rows(mysqli_query($koneksi, "SELECT * from restaurant where type_of_serving like '%$j[nama_type_of_serving]%' $idnegaras"));
											$totaltosaa = $urutana + $totaltosaa;
										}
                    while ($j=mysqli_fetch_array($sql)) {
											$urutan = mysqli_num_rows(mysqli_query($koneksi, "SELECT * from restaurant where type_of_serving like '%$j[nama_type_of_serving]%' $idnegaras"));
											$totaltosa = $urutan + $totaltosa;
											if ($urutan <> 0) {
	                      echo"
	                        <tr>
	                          <td width='40%'>";
														if ($j['nama_type_of_serving']=='') {
															echo "Unknown";
														}echo"
	                            <a href='https://www.foodieguidances.com/pages/restaurant/search/?type_of_serving[]=$j[nama_type_of_serving]'>
	                            	$j[nama_type_of_serving]
	                            </a>
	                          </td>
	                          <td width='30%'>
	                            $urutan
	                          </td>
	                          <td width='30%'>
	                            ".round($urutan/$totaltosaa * '100',2)."%
	                          </td>
	                        </tr>
	                      ";
											}
											$no++;
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $totaltosaa; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Menu Photo</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-11">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Menu Photo</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras ="";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE r.id_negara='$_GET[negara]'";
												$idnegaras = "AND r.id_negara='$_GET[negara]'";
											}
										?>
                    <?php
                    $sql=mysqli_query($koneksi, "SELECT r.id_restaurant, m.nama_menu FROM `restaurant` r left join menu m on m.id_restaurant=r.id_restaurant where m.nama_menu!='' $idnegaras");
										$sqla=mysqli_query($koneksi, "SELECT r.id_restaurant FROM `restaurant` r $idnegara");

											$yes= mysqli_num_rows($sql);
											$no= mysqli_num_rows($sqla);
											$total=$no;
											$ys = $no-$yes;
                      echo"
                        <tr>
                          <td width='40%'>
														Yes
                          </td>
                          <td width='30%'>
                            $yes
                          </td>
                          <td width='30%'>
                            ".round($yes/$total * '100',2)."%
                          </td>
                        </tr>
												<tr>
                          <td width='40%'>
														No
                          </td>
                          <td width='30%'>
                            $ys
                          </td>
                          <td width='30%'>
                            ".round(($no-$yes)/$total * '100',2)."%
                          </td>
                        </tr>
                      ";
                    ;
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $total; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Restaurant Rating</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-11">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Stars</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras ="";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE r.id_negara='$_GET[negara]'";
												$idnegaras = "AND r.id_negara='$_GET[negara]'";
											}
										?>
                    <?php
										$sqlaaaa = mysqli_query($koneksi, "
										SELECT r.id_restaurant, COUNT(*) as jumlah,
										ROUND((SELECT (SUM(f.cleanlines) + SUM(f.customer_services) + SUM(f.food_beverage) + SUM(f.comfort) + SUM(f.value_money))	/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant =r.id_restaurant), 1) AS total_rate
										FROM restaurant r where ROUND((SELECT (SUM(f.cleanlines) + SUM(f.customer_services) + SUM(f.food_beverage) + SUM(f.comfort) + SUM(f.value_money)) / COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant = r.id_restaurant), 1) $idnegaras GROUP by total_rate ORDER BY `total_rate`  DESC");

											$no= mysqli_num_rows($sqlaaaa);



											while ($a = mysqli_fetch_array($sqlaaaa)) {
												echo"
	                        <tr>
	                          <td width='40%'>
															$a[total_rate]
	                          </td>
	                          <td width='30%'>
	                            $a[jumlah]
	                          </td>
	                          <td width='30%'>
	                            ".round($a['jumlah']/$total * '100',2)."%
	                          </td>
	                        </tr>
	                      ";
											}

                    ;
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $total; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>



            </div>
          </div>
        <?php }























				else{
          $edit=mysqli_query($koneksi,"select * from restaurant where id_restaurant = '$_GET[id]'");
          $e=mysqli_fetch_array($edit);
          function tulis_cekbox($field,$koneksi,$judul) {
          	$query ="select * from ".$judul." order by nama_".$judul;
          	$r = mysqli_query($koneksi,$query);
          	$_arrNilai = explode('+', $field);
          	$str = '';
          	$no=1;
          	while ($w = mysqli_fetch_array($r)) {
          		$_ck = (array_search($w[1], $_arrNilai) === false)? '' : 'checked';
          		$str .= "<div class='col-md-6 col-4b'>
          			<div class='checkbox checkbox-inline checkbox-danger'>
          				<input type='checkbox' name='".$judul."[]' value='$w[1]' id='$judul$no' $_ck>
          				<label for='$judul$no'> $w[1]</label>
          			</div>
          		</div>";
          		$no++;
          	}
          	return $str;
          }
				?>
        <form action="mod/<?php echo"$mod/aksi.php?mod=$mod&ale=3&url=$_GET[url]"; ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" id="signup-daftar">
					<input type="hidden" name="id" value="<?php echo $e['id_restaurant'] ?>">
					<div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-btns">
                  <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                </div><!-- panel-btns -->
                <h4 class="panel-title">Data Restaurant</h4>
                <p>Silahkan lengkapi data dibawah ini.</p>
            </div>
            <div class="panel-body nopadding">
                <div class="form-group">
                  <label class="col-sm-2 control-label">Member :</label>
                  <div class="col-sm-10">
                    <select name="id_member" class="pilih" style="width: 100%;">
                    <?php
                      $sql=mysqli_query($koneksi,"select id_member,username from member order by username asc");
                      while($b=mysqli_fetch_array($sql)){
                        echo"<option value='$b[id_member]'"; if($b['id_member']==$e['id_member']){ echo "selected";} echo ">$b[username]</option>";
                      }
                    ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                   <label class="col-sm-2 control-label">Restaurant Name :</label>
                   <div class="col-sm-10">
                       <input type="text" name="restaurant_name" class="form-control" id="username" value="<?php echo"$e[restaurant_name]" ?>">
                   </div>
                </div>
                <div class="form-group">
                   <label class="col-sm-2 control-label">Business Type :</label>
                   <div class="col-sm-10">
                    <select name="type_of_business" class="pilih" style="width: 100%;">
                      <option value="">Choose Business Type</option>
                      <?php
                        $sql=mysqli_query($koneksi,"select id_type_of_business,nama_type_of_business from type_of_business order by nama_type_of_business asc");
                        while($b=mysqli_fetch_array($sql)){
                          echo"<option value='$b[id_type_of_business]'"; if($b['id_type_of_business']==$e['id_type_of_business']){ echo "selected";} echo ">$b[nama_type_of_business]</option>";
                        }
                      ?>
                    </select>
                   </div>
                </div>

                <div class="form-group">
                   <label class="col-sm-2 control-label">Tag :</label>
                   <div class="col-sm-10">
                       <input type="text" name="tag" class="form-control" value="<?php echo"$e[tag]" ?>">
                   </div>
                </div>


                <div class="accordion">Basic Information</div>
                <div class="panels">
                  <div class="form-group">
                     <label class="col-sm-2 control-label">Street Address :</label>
                     <div class="col-sm-10">
                        <input type="text" name="street_address" class="form-control" value="<?php echo"$e[street_address]" ?>">
                     </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Country :</label>
                    <div class="col-sm-10">
                      <select name="negara" class="pilih" style="width: 100%;" id="negara">
                      <option value="">Select Country</option>
                        <?php
                            $sql=mysqli_query($koneksi,"select id_negara,nama_negara from negara order by nama_negara asc");
                            while($b=mysqli_fetch_array($sql)){
                              echo"<option value='$b[id_negara]'"; if($b['id_negara']==$e['id_negara']){ echo "selected";} echo ">$b[nama_negara]</option>";
                            }
                          ?>
                       </select>
                    </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-2 control-label">State/Province :</label>
                     <div class="col-sm-10">
                         <select name="propinsi" class="pilih" style="width: 100%;" id="propinsi">
                            <option value="">Select State/ Province</option>
														<?php
		                            $sql=mysqli_query($koneksi,"select * from propinsi order by nama_propinsi asc");
		                            while($b=mysqli_fetch_array($sql)){
		                              echo"<option value='$b[id_propinsi]'"; if($b['id_propinsi']==$e['id_propinsi']){ echo "selected";} echo ">$b[nama_propinsi]</option>";
		                            }
		                          ?>
                          </select>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-2 control-label">City :</label>
                     <div class="col-sm-10">
                         <select name="kota" class="pilih" style="width: 100%;" id="kota">
                            <option value="">Select City</option>
														<?php
	                            $sql=mysqli_query($koneksi,"select * from kota order by nama_kota asc");
	                            while($b=mysqli_fetch_array($sql)){
	                              echo"<option value='$b[id_kota]'"; if($b['id_kota']==$e['id_kota']){ echo "selected";} echo ">$b[nama_kota]</option>";
	                            }
	                          ?>
                          </select>
                     </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Shopping Mall :</label>
                    <div class="col-sm-10">
                        <select name="mall" class="pilih" style="width: 100%;" id="mall">
                            <option value="">Select Shopping Mall</option>
														<?php
	                            $sql=mysqli_query($koneksi,"select * from mall order by nama_mall asc");
	                            while($b=mysqli_fetch_array($sql)){
	                              echo"<option value='$b[id_mall]'"; if($b['id_mall']==$e['id_mall']){ echo "selected";} echo ">$b[nama_mall]</option>";
	                            }
	                          ?>
                         </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Landmark :</label>
                    <div class="col-sm-10">
                       <select name="landmark" class="pilih" style="width: 100%;">
                       <option value="">Select Landmark</option>
                          <?php
                            $sql=mysqli_query($koneksi,"select * from landmark order by nama_landmark asc");
                            while($d=mysqli_fetch_array($sql)){
                              echo"<option value='$d[id_landmark]'"; if($d['id_landmark']==$e['id_landmark']){ echo "selected";} echo ">$d[nama_landmark]</option>";
                            }
                            ?>
                        </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Telephone :</label>
                    <div class="col-sm-10">
                      <input type="text" name="telephone" class="form-control" value="<?php echo"$e[telephone]" ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Facsimile :</label>
                    <div class="col-sm-10">
                      <input type="text" name="facsimile" class="form-control" value="<?php echo"$e[fax]" ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Postal Code :</label>
                    <div class="col-sm-10">
                       <input type="text" name="postal_code" class="form-control"  value="<?php echo"$e[postal_code]" ?>">
                    </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-2 control-label">Reservation :</label>
                     <div class="col-sm-10">
                         <input type="text" name="reservation_phone" class="form-control"  value="<?php echo"$e[reservation_phone]" ?>">
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-2 control-label">Email :</label>
                     <div class="col-sm-10">
                         <input type="text" name="email_resto" class="form-control"  value="<?php echo"$e[email_resto]" ?>">
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-2 control-label">Website :</label>
                     <div class="col-sm-10">
                         <input type="text" name="web" class="form-control" value="<?php echo"$e[web]" ?>">
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-2 control-label">Social Media :</label>
                     <div class="col-sm-10">
                          <input type="text" class="form-control mb10" placeholder="Write Facebook Url" name="facebook" value="<?php echo"$e[facebook]" ?>">
                          <input type="text" class="form-control mb10" placeholder="Write Twitter Url" name="twitter" value="<?php echo"$e[twitter]" ?>">
                          <input type="text" class="form-control mb10" placeholder="Write Instagram Url" name="instagram" value="<?php echo"$e[instagram]" ?>">
                          <input type="text" class="form-control" placeholder="Write Pinterest Url" name="pinterest" value="<?php echo"$e[pinterest]" ?>">
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-2 control-label">Branch :</label>
                     <div class="col-sm-10">
                         <input type="text" name="branch_name" class="form-control" value="<?php echo"$e[branch_name]" ?>">
                     </div>
                  </div>
                </div>

                <div class="accordion">Main Information</div>
                  <div class="panels">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Pork Serving :</label>
                        <div class="col-sm-10">
                          <div class="radio radio-inline">
                            <input type="radio" name="pork" <?php if ($e['pork_serving']=='Yes') { echo "checked"; } ?> value="Yes" id="pork1">
                            Yes
                          </div>
                          <div class="radio radio-inline radio-danger">
                            <input type="radio" name="pork" <?php if ($e['pork_serving']=='No') { echo "checked"; } ?> value="No" id="pork2">
                            No
                          </div>
                          <div class="radio radio-inline radio-danger">
                            <input type="radio" name="pork" value="Halal" id="pork3" <?php if ($e['pork_serving']=='Halal') { echo "checked"; } ?>>
                            Halal
                          </div>
                          <div class="radio radio-inline radio-danger">
                            <input type="radio" name="pork" value="vege" id="pork4" <?php if ($e['pork_serving']=='vege') { echo "checked"; } ?>>
                            Vegetable
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Alcohol Serving :</label>
                        <div class="col-sm-10">
                            <div class="radio radio-inline radio-danger">
                              <input type="radio" checked="" name="alcohol" value="Yes" <?php if ($e['alcohol_serving']=='Yes') { echo "checked"; } ?> id="alcohol1">
                                Yes
                            </div>
                            <div class="radio radio-inline radio-danger">
                              <input type="radio" name="alcohol" value="No" id="alcohol2" <?php if ($e['alcohol_serving']=='No') { echo "checked"; } ?>>
                                No
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Price Index</label>
                        <div class="col-sm-10">
                          <select name="price_index" class="pilih" style="width: 100%;">
                            <option value="">Select Price Index</option>
                              <?php
                                $sql=mysqli_query($koneksi,"select * from price_index");
                                while($d=mysqli_fetch_array($sql)){
                                  echo"<option value='$d[id_price_index]'"; if($d['id_price_index']==$e['id_price_index']){ echo "selected";} echo ">$d[nama_price_index]</option>";
                                }
                              ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Suitable For</label>
                        <div class="col-sm-10">
                          <select name="suitable_for" class="pilih" style="width: 100%;">
                            <option value="">Select Suitable For</option>
                              <?php
                                $sql=mysqli_query($koneksi,"select * from suitable_for order by nama_suitable_for asc");
                                while($h=mysqli_fetch_array($sql)){
                                  echo"<option value='$h[id_suitable_for]'"; if($h['id_suitable_for']==$e['id_suitable_for']){ echo "selected";} echo ">$h[nama_suitable_for]</option>";
                                }
                              ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Wi-Fi</label>
                        <div class="col-sm-10">
                          <select name="wifi" class="pilih" style="width: 100%;">
                              <option value="">Select Wi-Fi</option>
                                <?php
                                  $sql=mysqli_query($koneksi,"select * from wifi order by nama_wifi asc");
                                  while($n=mysqli_fetch_array($sql)){
                                    echo"<option value='$n[id_wifi]'"; if($n['id_wifi']==$e['id_wifi']){ echo "selected";} echo ">$n[nama_wifi]</option>";
                                  }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Payment</label>
                        <div class="col-sm-10">
                          <select name="payment" class="pilih" style="width: 100%;">
                            <option value="">Select Payment</option>
                            <?php
                            $sql=mysqli_query($koneksi,"select * from term_of_payment order by nama_term_of_payment asc");
                            while($o=mysqli_fetch_array($sql)){
                              echo"<option value='$o[id_term_of_payment]'"; if($o['id_term_of_payment']==$e['id_term_of_payment']){ echo "selected";} echo ">$o[nama_term_of_payment]</option>";
                            }
                            ?>
                          </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Serving Time</label>
                        <div class="col-sm-10">
                          <select name="serving_time" class="pilih" style="width: 100%;">
                            <option value="0">Select Serving Time</option>
                            <?php
                            $sql=mysqli_query($koneksi,"select * from serving_time");
                            while($k=mysqli_fetch_array($sql)){
                              echo"<option value='$k[id_serving_time]'"; if($k['id_serving_time']==$e['id_serving_time']){ echo "selected";} echo ">$k[nama_serving_time]</option>";
                            }
                            ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Type Of Service</label>
                        <div class="col-sm-10">
                          <select name="type_of_service" class="pilih" style="width: 100%;">
                            <option value="0">Select Type of Service</option>
                            <?php
                            $sql=mysqli_query($koneksi,"select * from type_of_service order by nama_type_of_service asc");
                            while($l=mysqli_fetch_array($sql)){
                              echo"<option value='$l[id_type_of_service]'"; if($l['id_type_of_service']==$e['id_type_of_service']){ echo "selected";} echo ">$l[nama_type_of_service]</option>";
                            }
                            ?>
                          </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Air Conditioning</label>
                        <div class="col-sm-10">
                          <select name="air_conditioning" class="pilih" style="width: 100%;">
                            <option value="0">Select Air Conditioning</option>
                            <?php
                            $sql=mysqli_query($koneksi,"select * from air_conditioning order by nama_air_conditioning asc");
                            while($l1=mysqli_fetch_array($sql)){
                              echo"<option value='$l1[id_air_conditioning]'"; if($l1['id_air_conditioning']==$e['id_air_conditioning']){ echo "selected";} echo ">$l1[nama_air_conditioning]</option>";
                            }
                            ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Heating System</label>
                        <div class="col-sm-10">
                          <select name="heating_system" class="pilih" style="width: 100%;">
                            <option value="0">Select Heating Conditioning</option>
                            <?php
                            $sql=mysqli_query($koneksi,"select * from heating_system order by nama_heating_system asc");
                            while($l2=mysqli_fetch_array($sql)){
                              echo"<option value='$l2[id_heating_system]'"; if($l2['id_heating_system']==$e['id_heating_system']){ echo "selected";} echo ">$l2[nama_heating_system]</option>";
                            }
                            ?>
                          </select>
                        </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 control-label">Premise's Security</label>
                      <div class="col-sm-10">
                        <select name="premise_security" class="pilih" style="width: 100%;">
                          <option value="0">Select premise's Security</option>
                          <?php
                          $sql=mysqli_query($koneksi,"select * from premise_security order by nama_premise_security asc");
                          while($p=mysqli_fetch_array($sql)){
                            echo"<option value='$p[id_premise_security]'"; if($p['id_premise_security']==$e['id_premise_security']){ echo "selected";} echo ">$p[nama_premise_security]</option>";
                          }
                          ?>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Parking Spaces</label>
                      <div class="col-sm-10">
                        <select name="parking_spaces" class="pilih" style="width: 100%;">
                          <option value="0">Select Parking Spaces</option>
                          <?php
                          $sql=mysqli_query($koneksi,"select * from parking_spaces order by nama_parking_spaces asc");
                          while($q=mysqli_fetch_array($sql)){
                            echo"<option value='$q[id_parking_spaces]'"; if($q['id_parking_spaces']==$e['id_parking_spaces']){ echo "selected";} echo ">$q[nama_parking_spaces]</option>";
                          }
                          ?>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Premise's Fire Safety</label>
                      <div class="col-sm-10">
                        <select name="premise_fire_safety" class="pilih" style="width: 100%;">
                          <option value="0">Select Premise's Fire Safety</option>
                          <?php
                          $sql=mysqli_query($koneksi,"select * from premise_fire_safety order by nama_premise_fire_safety asc");
                          while($r=mysqli_fetch_array($sql)){
                            echo"<option value='$r[id_premise_fire_safety]'"; if($r['id_premise_fire_safety']==$e['id_premise_fire_safety']){ echo "selected";} echo ">$r[nama_premise_fire_safety]</option>";
                          }
                          ?>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Premise's Hygiene</label>
                      <div class="col-sm-10">
                        <select name="premise_hygiene" class="pilih" style="width: 100%;">
                          <option value="0">Select Premise's Hygiene</option>
                          <?php
                          $sql=mysqli_query($koneksi,"select * from premise_hygiene order by nama_premise_hygiene asc");
                          while($r1=mysqli_fetch_array($sql)){
                            echo"<option value='$r1[id_premise_hygiene]'"; if($r1['id_premise_hygiene']==$e['id_premise_hygiene']){ echo "selected";} echo ">$r1[nama_premise_hygiene]</option>";
                          }
                          ?>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Premise's Maintenance</label>
                        <div class="col-sm-10">
                          <select name="premise_maintenance" class="pilih" style="width: 100%;">
                            <option value="0">Select Premise Maintenance</option>
                            <?php
                            $sql=mysqli_query($koneksi,"select * from premise_maintenance order by nama_premise_maintenance asc");
                            while($s=mysqli_fetch_array($sql)){
                              echo"<option value='$s[id_premise_maintenance]'"; if($s['id_premise_maintenance']==$e['id_premise_maintenance']){ echo "selected";} echo ">$s[nama_premise_maintenance]</option>";
                            }
                            ?>
                          </select>
                        </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Ambience</label>
                      <div class="col-sm-10">
                        <select name="ambience" class="pilih" style="width: 100%;">
                          <option value="0">Select Ambience</option>
                          <?php
                          $sql=mysqli_query($koneksi,"select * from ambience order by nama_ambience asc");
                          while($t=mysqli_fetch_array($sql)){
                            echo"<option value='$t[id_ambience]'"; if($t['id_ambience']==$e['id_ambience']){ echo "selected";} echo ">$t[nama_ambience]</option>";
                          }
                          ?>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Attire</label>
                      <div class="col-sm-10">
                        <select name="attire" class="pilih" style="width: 100%;">
                          <option value="0">Select Attire </option>
                          <?php
                          $sql=mysqli_query($koneksi,"select * from attire order by nama_attire asc");
                          while($u=mysqli_fetch_array($sql)){
                            echo"<option value='$u[id_attire]'"; if($u['id_attire']==$e['id_attire']){ echo "selected";} echo ">$u[nama_attire]</option>";
                          }
                          ?>
                          </select>
                      </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Clean Washroom</label>
                        <div class="col-sm-10">
                          <select name="clean_washroom" class="pilih" style="width: 100%;">
                            <option value="0">Select Clean Washroom</option>
                            <?php
                            $sql=mysqli_query($koneksi,"select * from clean_washroom order by nama_clean_washroom asc");
                            while($v=mysqli_fetch_array($sql)){
                              echo"<option value='$v[id_clean_washroom]'"; if($v['id_clean_washroom']==$e['id_clean_washroom']){ echo "selected";} echo ">$v[nama_clean_washroom]</option>";
                            }
                            ?>
                          </select>
                        </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 control-label">Table Availability</label>
                      <div class="col-sm-10">
                        <select name="tables_availability" class="pilih" style="width: 100%;">
                          <option value="0">Select Tables Availability</option>
                          <?php
                          $sql=mysqli_query($koneksi,"select * from tables_availability order by nama_tables_availability asc");
                          while($w=mysqli_fetch_array($sql)){
                            echo"<option value='$w[id_tables_availability]'"; if($w['id_tables_availability']==$e['id_tables_availability']){ echo "selected";} echo ">$w[nama_tables_availability]</option>";
                          }
                          ?>
                          </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 control-label">Noise Level</label>
                      <div class="col-sm-10">
                        <select name="noise_level" class="pilih" style="width: 100%;">
                          <option value="0">Select Noise Level</option>
                          <?php
                          $sql=mysqli_query($koneksi,"select * from noise_level");
                          while($x=mysqli_fetch_array($sql)){
                            echo"<option value='$x[id_noise_level]'"; if($x['id_noise_level']==$e['id_noise_level']){ echo "selected";} echo ">$x[nama_noise_level]</option>";
                          }
                          ?>
                          </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 control-label">Water Tipping</label>
                      <div class="col-sm-10">
                        <select name="waiter_tipping" class="pilih" style="width: 100%;">
                          <option value="0">Select Waiter Tipping</option>
                          <?php
                          $sql=mysqli_query($koneksi,"select * from waiter_tipping order by nama_waiter_tipping asc");
                          while($y=mysqli_fetch_array($sql)){
                            echo"<option value='$y[id_waiter_tipping]'"; if($y['id_waiter_tipping']==$e['id_waiter_tipping']){ echo "selected";} echo ">$y[nama_waiter_tipping]</option>";
                          }
                          ?>
                          </select>
                      </div>
                    </div>
                  </div>


                  <div class="accordion">Business Hours</div>
                       <div id="foo" class="panels">
                         <div class="form-group">
                            <label class="col-sm-2 control-label">Operational</label>
                              <div class="col-sm-10">
                                <select name="operation_hour" class="pilih" style="width: 100%;">
                                  <?php
                                    $sql=mysqli_query($koneksi,"select * from operation_hour order by nama_operation_hour asc");
                                    while($e1=mysqli_fetch_array($sql)){
                                      $selected="";
                                      if($e1['id_operation_hour']==$e['id_operation_hour']){
                                        $selected="selected";
                                      }
                                      echo"<option value='$e1[id_operation_hour]' $selected>$e1[nama_operation_hour]</option>";
                                    }
                                  ?>
                                </select>
                              </div>
                         </div>
                         <div class="form-group">
                            <label class="col-sm-2 control-label">Restaurant Hour</label>
                              <div class="col-sm-10 table-responsive">
                                <table class="table">
                                  <tr>
                                    <td></td>
                                    <th colspan="2">Restaurant Hour 1</th>
                                    <th colspan="2">Restaurant Hour 2</th>
                                    <th colspan="2">Option</th>
                                  </tr>
                                  <tr>
                                    <th class="vc">Sunday</th>
                                    <td><input type="text" class="form-control timae" Placeholder="Open Time" id="resto_time1" name="resto_time1" value="<?php echo"$e[resto_time1]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Close Time" id="resto_time2" name="resto_time2"  value="<?php echo"$e[resto_time2]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Open Time" id="resto_time1a" name="resto_time1a" value="<?php echo"$e[resto_time1a]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Close Time" id="resto_time2a" name="resto_time2a" value="<?php echo"$e[resto_time2a]" ?>"></td>
                                  <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                                      <input name="resto_sunday" value="Close" id="resto_sunday" type="checkbox">
                                      <label for="resto_sunday"> Close</label>
                                    </div>
                                  </td>
                                  <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                                      <div class="btn btn-danger" id="duplicate">Apply All Days</div>
                                    </div>
                                   </td>
                                  </tr>
                                  <tr>
                                    <th class="vc">Monday</th>
                                    <td><input type="text" class="form-control timae" Placeholder="Open Time" id="resto_time3" name="resto_time3" value="<?php echo"$e[resto_time3]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Close Time" id="resto_time4" name="resto_time4" value="<?php echo"$e[resto_time4]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Open Time" id="resto_time3a" name="resto_time3a" value="<?php echo"$e[resto_time3a]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Close Time" id="resto_time4a" name="resto_time4a" value="<?php echo"$e[resto_time4a]" ?>"></td>
                                     <td>
                                      <div class="checkbox checkbox-inline checkbox-danger">
                                        <input name="resto_monday" value="Close" id="resto_monday" type="checkbox">
                                        <label for="resto_monday"> Close</label>
                                      </div>
                                     </td>
                                  </tr>
                                  <tr>
                                    <th class="vc">Tuesday</th>
                                    <td><input type="text" class="form-control timae" Placeholder="Open Time" id="resto_time5" name="resto_time5" value="<?php echo"$e[resto_time5]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Close Time" id="resto_time6" name="resto_time6" value="<?php echo"$e[resto_time6]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Open Time" id="resto_time5a" name="resto_time5a" value="<?php echo"$e[resto_time5a]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Close Time" id="resto_time6a" name="resto_time6a" value="<?php echo"$e[resto_time6a]" ?>"></td>
                                   <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                                      <input name="resto_tuesday" value="Close" id="resto_tuesday" type="checkbox">
                                      <label for="resto_tuesday"> Close</label>
                                    </div>
                                   </td>
                                   <td></td>
                                  </tr>
                                  <tr>
                                    <th class="vc">Wednesday</th>
                                    <td><input type="text" class="form-control timae" Placeholder="Open Time" id="resto_time7" name="resto_time7" value="<?php echo"$e[resto_time7]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Close Time" id="resto_time8" name="resto_time8" value="<?php echo"$e[resto_time8]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Open Time" id="resto_time7a" name="resto_time7a" value="<?php echo"$e[resto_time7a]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Close Time" id="resto_time8a" name="resto_time8a" value="<?php echo"$e[resto_time8a]" ?>"></td>
                                     <td>
                                      <div class="checkbox checkbox-inline checkbox-danger">
                                        <input name="resto_wednesday" value="Close" id="resto_wednesday" type="checkbox">
                                        <label for="resto_wednesday"> Close</label>
                                      </div>
                                     </td>
                                     <td></td>
                                  </tr>
                                  <tr>
                                    <th class="vc">Thursday</th>
                                    <td><input type="text" class="form-control timae" Placeholder="Open Time"  id="resto_time9" name="resto_time9" value="<?php echo"$e[resto_time9]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Close Time" id="resto_time10" name="resto_time10" value="<?php echo"$e[resto_time10]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Open Time" id="resto_time9a" name="resto_time9a" value="<?php echo"$e[resto_time9a]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Close Time" id="resto_time10a" name="resto_time10a" value="<?php echo"$e[resto_time10a]" ?>"></td>
                                     <td>
                                        <div class="checkbox checkbox-inline checkbox-danger">
                                          <input name="resto_thursday" value="Close" id="resto_thursday" type="checkbox">
                                          <label for="resto_thursday"> Close</label>
                                        </div>
                                     </td>
                                    <td></td>
                                  </tr>
                                  <tr>
                                    <th class="vc">Friday</th>
                                    <td><input type="text" class="form-control timae" Placeholder="Open Time" id="resto_time11" name="resto_time11" value="<?php echo"$e[resto_time11]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Close Time" id="resto_time12" name="resto_time12" value="<?php echo"$e[resto_time12]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Open Time" id="resto_time11a" name="resto_time11a" value="<?php echo"$e[resto_time11a]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Close Time" id="resto_time12a" name="resto_time12a" value="<?php echo"$e[resto_time12a]" ?>"></td>
                                   <td>
                                      <div class="checkbox checkbox-inline checkbox-danger">
                                        <input name="resto_friday" value="Close" id="resto_friday" type="checkbox">
                                        <label for="resto_friday"> Close</label>
                                      </div>
                                   </td>
                                   <td></td>
                                  </tr>
                                  <tr>
                                    <th class="vc">Saturday</th>
                                    <td><input type="text" class="form-control timae" Placeholder="Open Time" id="resto_time13" name="resto_time13" value="<?php echo"$e[resto_time13]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Select Time" id="resto_time14" name="resto_time14" value="<?php echo"$e[resto_time14]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Open Time" id="resto_time13a" name="resto_time13a" value="<?php echo"$e[resto_time13a]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Select Time" id="resto_time14a" name="resto_time14a" value="<?php echo"$e[resto_time14a]" ?>"></td>
                                     <td>
                                        <div class="checkbox checkbox-inline checkbox-danger">
                                          <input name="resto_saturday" value="Close" id="resto_saturday" type="checkbox">
                                          <label for="resto_saturday"> Close</label>
                                        </div>
                                     </td>
                                    <td></td>
                                  </tr>
                                </table>
                              </div>
                         </div>

                         <div class="form-group">
                            <label class="col-sm-2 control-label">Bar Hour</label>
                              <div class="col-sm-10 table-responsive">
                                <table class="table">
                                  <tr>
                                    <td></td>
                                    <th colspan="2">Bar Hour 1</th>
                                    <th colspan="2">Bar Hour 2</th>
                                    <th colspan="2">Option</th>
                                  </tr>
                                  <tr>
                                    <th class="vc">Sunday</th>
                                    <td><input type="text" class="form-control timae" Placeholder="Open Time" id="bar_time1" name="bar_time1" value="<?php echo"$e[bar_time1]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Close Time" id="bar_time2" name="bar_time2" value="<?php echo"$e[bar_time1a]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Open Time" id="bar_time1a" name="bar_time1a" value="<?php echo"$e[bar_time1a]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Close Time" id="bar_time2a" name="bar_time2a" value="<?php echo"$e[bar_time2a]" ?>"></td>
                                  <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                                      <input name="resto_sunday" value="Close" id="resto_sunday" type="checkbox">
                                      <label for="resto_sunday"> Close</label>
                                    </div>
                                  </td>
                                  <td>
                                      <div class="checkbox checkbox-inline checkbox-danger">
                                           <div class="btn btn-danger" id="duplicate2">Apply All Days</div>
                                      </div>
                                   </td>
                                  </tr>
                                  <tr>
                                    <th class="vc">Monday</th>
                                    <td><input type="text" class="form-control timae" Placeholder="Open Time" id="bar_time3" name="bar_time3" value="<?php echo"$e[bar_time3]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Close Time" id="bar_time4" name="bar_time4" value="<?php echo"$e[bar_time4]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Open Time" id="bar_time3a" name="bar_time3a" value="<?php echo"$e[bar_time3a]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Close Time" id="bar_time4a" name="bar_time4a" value="<?php echo"$e[bar_time4a]" ?>"></td>
                                     <td>
                                      <div class="checkbox checkbox-inline checkbox-danger">
                                        <input name="resto_monday" value="Close" id="resto_monday" type="checkbox">
                                        <label for="resto_monday"> Close</label>
                                      </div>
                                     </td>
                                  </tr>
                                  <tr>
                                    <th class="vc">Tuesday</th>
                                    <td><input type="text" class="form-control timae" Placeholder="Open Time" id="bar_time5" name="bar_time5" value="<?php echo"$e[bar_time5]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Close Time" id="bar_time6" name="bar_time6" value="<?php echo"$e[bar_time6]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Open Time" id="bar_time5a" name="bar_time5a" value="<?php echo"$e[bar_time5a]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Close Time" id="bar_time6a" name="bar_time6a" value="<?php echo"$e[bar_time6a]" ?>"></td>
                                   <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                                      <input name="resto_tuesday" value="Close" id="resto_tuesday" type="checkbox">
                                      <label for="resto_tuesday"> Close</label>
                                    </div>
                                   </td>
                                   <td></td>
                                  </tr>
                                  <tr>
                                    <th class="vc">Wednesday</th>
                                    <td><input type="text" class="form-control timae" Placeholder="Open Time" id="bar_time7" name="bar_time7" value="<?php echo"$e[bar_time7]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Close Time" id="bar_time8" name="bar_time8" value="<?php echo"$e[bar_time8]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Open Time" id="bar_time7a" name="bar_time7a" value="<?php echo"$e[bar_time7a]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Close Time" id="bar_time8a" name="bar_time8a" value="<?php echo"$e[bar_time8a]" ?>"></td>
                                     <td>
                                      <div class="checkbox checkbox-inline checkbox-danger">
                                        <input name="resto_wednesday" value="Close" id="resto_wednesday" type="checkbox">
                                        <label for="resto_wednesday"> Close</label>
                                      </div>
                                     </td>
                                     <td></td>
                                  </tr>
                                  <tr>
                                    <th class="vc">Thursday</th>
                                    <td><input type="text" class="form-control timae" Placeholder="Open Time"  id="bar_time9" name="bar_time9" value="<?php echo"$e[bar_time9]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Close Time" id="bar_time10" name="bar_time10" value="<?php echo"$e[bar_time10]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Open Time" id="bar_time9a" name="bar_time9a" value="<?php echo"$e[bar_time9a]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Close Time" id="bar_time10a" name="bar_time10a" value="<?php echo"$e[bar_time10a]" ?>"></td>
                                     <td>
                                        <div class="checkbox checkbox-inline checkbox-danger">
                                          <input name="resto_thursday" value="Close" id="resto_thursday" type="checkbox">
                                          <label for="resto_thursday"> Close</label>
                                        </div>
                                     </td>
                                    <td></td>
                                  </tr>
                                  <tr>
                                    <th class="vc">Friday</th>
                                    <td><input type="text" class="form-control timae" Placeholder="Open Time" id="bar_time11" name="bar_time11" value="<?php echo"$e[bar_time11]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Close Time" id="bar_time12" name="bar_time12" value="<?php echo"$e[bar_time12]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Open Time" id="bar_time11a" name="bar_time11a" value="<?php echo"$e[bar_time11a]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Close Time" id="bar_time12a" name="bar_time12a" value="<?php echo"$e[bar_time12a]" ?>"></td>
                                   <td>
                                      <div class="checkbox checkbox-inline checkbox-danger">
                                        <input name="resto_friday" value="Close" id="resto_friday" type="checkbox">
                                        <label for="resto_friday"> Close</label>
                                      </div>
                                   </td>
                                   <td></td>
                                  </tr>
                                  <tr>
                                    <th class="vc">Saturday</th>
                                    <td><input type="text" class="form-control timae" Placeholder="Open Time" id="bar_time13" name="bar_time13" value="<?php echo"$e[bar_time13]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Select Time" id="bar_time14" name="bar_time14" value="<?php echo"$e[bar_time14]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Open Time" id="bar_time13a" name="bar_time13a" value="<?php echo"$e[bar_time13]" ?>"></td>
                                    <td><input type="text" class="form-control timae" Placeholder="Select Time" id="bar_time14a" name="bar_time14a" value="<?php echo"$e[bar_time14a]" ?>"></td>
                                     <td>
                                        <div class="checkbox checkbox-inline checkbox-danger">
                                          <input name="resto_saturday" value="Close" id="resto_saturday" type="checkbox">
                                          <label for="resto_saturday"> Close</label>
                                        </div>
                                     </td>
                                    <td></td>
                                  </tr>
                                </table>
                              </div>
                         </div>
                       </div>


                      <div class="accordion">Map</div>
                       <div class="panels">
                        <div class="box-panel">
                          <div class="row mb20">
                            <div class="col-4"><label class="control-label">Enter Restaurant Location</label></div>
                            <div class="col-6">

                            </div>
                          </div><!-- row -->
                          <!-- <div class="row">
                            <div class="col-16">
                              <input id="searchInput" class="controls" onkeydown="if (event.keyCode == 13) {return false;}" type="text" placeholder="Enter a location">
                              <div id="map"></div>
                              <ul id="geoData">
                                  <input type="hidden" id="lat" name="lat" value="<?php echo $e['latitude'] ?>">
                                  <input type="hidden" id="lon" name="lng" value="<?php echo $e['longitude'] ?>">
                              </ul>
                              <p class="f-12">Please select the district &amp; landmark for this restaurant. If the restaurant does not belong to any of the landmarks listed, please place the anchor at the restaurant’s correct location with your mouse.</p>
                            </div>
                          </div> -->
                          <div class="row mb20">
							<div class="col-4 col-4b">
								<label class="control-label">Input Address</label>
							</div>
							<div class="col-6 col-4b">
								<input id="search_address" class="controls form-control" type="text" value="<?php echo"$e[restaurant_name]";?>" placeholder="Enter a location" /><br>
								<div id="geoloc" class="btn btn-danger">Your Location</div>
								<br>
							</div>
						
						
							<div class="col-16"><br>
			                  <div id="map-canvas" style="height:500px"></div>
								<ul id="geoData">
					                <input id="lat" name="lat" value="<?php echo"$e[latitude]";?>" type="hidden" />
					                <input id="long" name="lng" value="<?php echo"$e[longitude]";?>" type="hidden" />
								</ul>
			                  <?php
			                  $sqls=mysqli_query($koneksi,"select * from negara where id_negara='$e[id_negara]' order by nama_negara asc");
			                  $bs=mysqli_fetch_array($sqls);

			                  if (empty($e['latitude'])) {
			                    $lat = str_replace(',', '.', $bs['latitude_negara']) ;  $lng = str_replace(',', '.', $bs['longitude_negara']); $nama= $bs['nama_negara'];
			                  }
			                  else {
			                    $lat = $e['latitude'];  $lng = $e['longitude']; $nama= $e['restaurant_name'];
			                  }
			                    //echo "$bs[latitude_negara] --- $bs[longitude_negara]";
			                   ?>
			                </div><!-- col-sm-6 -->
                        </div>
                       </div>
						</div>

                       <div class="accordion">Restaurant Description</div>
                       <div class="panels">
                         <div class="form-group">
                              <label class="col-sm-2 control-label">Description :</label>
                              <div class="col-sm-10">
                                <textarea class="form-control" name="description" placeholder="Write Restaurant Description" rows="4"><?php echo"$e[restaurant_description]" ?></textarea>
                              </div>
                          </div>
                       </div>

                       <div class="accordion">Business Status</div>
                       <div class="panels">
                         <div class="form-group">
                              <label class="col-sm-2 control-label">Status :</label>
                              <div class="col-sm-10">
                                <div class="radio radio-inline radio-danger">
                                  <input type="radio" name="business_status" <?php if ($e['business_status']=='Ongoing') { echo "checked"; } ?> value="Ongoing" id="status1">
                                  <label for="status1"> Ongoing </label>
                                </div>
                                <div class="radio radio-inline radio-danger">
                                  <input type="radio" name="business_status" <?php if ($e['business_status']=='Renovation') { echo "checked"; } ?> value="Renovation" id="status2">
                                  <label for="status2"> Renovation </label>
                                </div>
                                <div class="radio radio-inline radio-danger">
                                  <input type="radio" name="business_status" <?php if ($e['business_status']=='Relocate') { echo "checked"; } ?> value="Relocate" id="status3">
                                  <label for="status3"> Relocate </label>
                                </div>
                                <div class="radio radio-inline radio-danger">
                                  <input type="radio" name="business_status" <?php if ($e['business_status']=='Ceased Operation') { echo "checked"; } ?> value="Ceased Operation" id="status4">
                                  <label for="status4"> Ceased Operation </label>
                                </div>
                              </div>
                          </div>
                          <div class="form-group">
                               <label class="col-sm-2 control-label">Description :</label>
                               <div class="col-sm-10">
                                 <textarea class="form-control" name="business_description" placeholder="Write further information if applicable" rows="4"><?php echo"$e[business_status_description]" ?></textarea>
                               </div>
                           </div>
                       </div>

                       <div class="accordion">Features and Facility</div>
                       <div class="panels">
                         <div class="form-group">
													<?php
						 								$fasilitas=tulis_cekbox($e['facility'],$koneksi,'facility');
						 								echo"$fasilitas";
					 								?>
                         </div>
                       </div>

                       <div class="accordion">Cuisine</div>
                       <div class="panels">
                         <div class="form-group">
													<?php
	 													$cuisine=tulis_cekbox($e['cuisine'],$koneksi,'cuisine');
	 													echo"$cuisine";
 													?>
                         </div>
                       </div>

                       <div class="accordion">Serving</div>
                       <div class="panels">
                         <div class="form-group">
													<?php
	 													$serving=tulis_cekbox($e['serving'],$koneksi,'serving');
	 													echo"$serving";
 													?>
                         </div>
                       </div>

                       <div class="accordion">Type of Serving</div>
                       <div class="panels">
                         <div class="form-group">
													<?php
	 													$type_serving=tulis_cekbox($e['type_of_serving'],$koneksi,'type_of_serving');
	 													echo"$type_serving";
 													?>
                         </div>
                       </div>

                    </div><!-- panel-body -->
                </div><!-- panel -->

                <div class="panel panel-default">
                  <div class="panel-footer">
                    <button class="btn btn-primary mr5">Simpan</button>
                    <a href="mod-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-warning">Kembali</a>
                  </div><!-- panel-footer -->
                </div><!-- panel -->
          </form>

				<?php
						if (!empty($e['latitude']) && !empty($e['longitude'])) {
							$lat=$e['latitude']; $lng=$e['longitude']; $nama=$e['restaurant_name'];
						}
						else {
							$lat="-7.2574719";
							$lng="112.75208829999997";
							$nama="Foodieguidances";
						}
				  } ?>
			</div><!-- contentpanel -->
        </div><!-- mainpanel -->
    </div><!-- mainwrapper -->
</section>


<script src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
  $('#search_address').on('keypress', function(e) {
    return e.which !== 13;
  });

  $('.form-control').on('keypress', function (e) {
      var ingnore_key_codes = [34, 39];
      if ($.inArray(e.which, ingnore_key_codes) >= 0) {
          e.preventDefault();
          alert("\'\" is not allowed");
      } else {}
  });

    function initialize() {
    var marker;
    var latlng = new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lng; ?>);
    var geocoder = new google.maps.Geocoder();
    var mapOptions = {
      center: latlng,
      zoom: 17,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      draggableCursor: "pointer",
      streetViewControl: false
    };

    var imageicon = 'https://www.foodieguidances.com/assets/images/foodie-point.png';

    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
          var markers = new google.maps.Marker({
            position: latlng,
            map: map,
            icon : imageicon
          });

    google.maps.event.addListener(map, "click", function (location) {
      //map.setCenter(location.latLng);
      setLatLong(location.latLng.lat(), location.latLng.lng());

      marker.setMap(null);
      markers.setMap(null);

      marker = new google.maps.Marker({
        position: location.latLng,
        map: map,
        icon : imageicon
      });
      marker.setPosition(location.latLng);
      setGeoCoder(location.latLng);
    });

    var input = (document.getElementById('search_address'));

    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);
    autocomplete.setTypes([]);

    var infowindow = new google.maps.InfoWindow();
    marker = new google.maps.Marker({
      map: map,
      anchorPoint: new google.maps.Point(0, -29),
      icon : imageicon
    });

    google.maps.event.addListener(autocomplete, 'place_changed', function() {
      //infowindow.close();
      marker.setVisible(true);
      var place = autocomplete.getPlace();

      if (!place.geometry) return;

      // If the place has a geometry, then present it on a map.
      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);  // Why 17? Because it looks good.
      }
      marker.setIcon(imageicon);
      map.setCenter(20);
      marker.setPosition(place.geometry.location);
      marker.setVisible(true);
      setLatLong(place.geometry.location.lat(), place.geometry.location.lng());

    });

    document.getElementById('geoloc').onclick=function(){
      markers.setMap(null);
      if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

          map.setCenter(pos);
          marker.setPosition(pos);

          setGeoCoder(pos);
          setLatLong(position.coords.latitude, position.coords.longitude);

        }, function() {
          handleNoGeolocation(true);
        });
      } else {
        // Browser doesn't support Geolocation
        handleNoGeolocation(false);
      }
    };


    function setLatLong(lat, long) {
      document.getElementById('lat').value=lat;
      document.getElementById('long').value=long;
    }

    function setGeoCoder(pos) {
      geocoder.geocode({'location': pos}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[0]) {
            document.getElementById('search_address').value=results[0].formatted_address;
          } else {
            document.getElementById('search_address').value='';
          }
        } else {
          document.getElementById('search_address').value='';
        }
      });
    }
    }


    google.maps.event.addDomListener(window, 'load', initialize);

</script>
<script async defer
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCwYMs0LtJBgwkV937PkNKKR3dbEiD0zA&callback=initialize&libraries=places">
</script>



<script type="text/javascript">
$(document).ready(function() {
  $(".pilih").select2();
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('.tip').tooltip();

  var x = 1;
  $(".tambah_menu").click(function(e){ //on add input button click
    e.preventDefault();
    if(x < 30){ //max input box allowed
      x++; //text box increment
      $(".menu_wrap").append('<div class="row mb20"><div class="col-md-12 "><div class="fileinput fileinput-new" data-provides="fileinput"> <div class="fileinput-preview upload" data-trigger="fileinput"></div><input type="file" name="menu[]" class="hidden"></div></div><div class="col-md-6"><input type="text" class="form-control mt30" placeholder="Photo Caption" name="menu_caption[]"></div><div class="col-1"><button type="button" class="hapus_menu btn btn-danger mt30">Remove</button></div></div>'); //add input box
    }
    if(x > 29){
      $(".tambah_menu").hide();
    }
  });

  $(".menu_wrap").on("click",".hapus_menu", function(e){ //user click on remove text
    e.preventDefault(); $(this).parent().parent('.row').remove(); x--;
    if($(".tambah_menu").is(":hidden")){
      $(".tambah_menu").show();
    }
  });


		$("#negara").change(function(){
			var id = $("#negara").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base"; ?>/config/func/ajax_propinsi.php",
				data: "id=" + id,
				success: function(data){
					$("#propinsi").html(data);
					$("#propinsi").fadeIn(2000);
				}
			});
		});
		$("#propinsi").change(function(){
			var id = $("#propinsi").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base"; ?>/config/func/ajax_kota.php",
				data: "id=" + id,
				success: function(data){
					$("#kota").html(data);
					$("#kota").fadeIn(2000);
				}
			});
		});
      $("#kota").change(function(){
			var id = $("#kota").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base"; ?>/config/func/ajax_mall.php",
				data: "id=" + id,
				success: function(data){
					$("#mall").html(data);
					$("#mall").fadeIn(2000);
				}
			});
		});
});



      $("#duplicate").click(function(){
         var resto_time1 = $("#resto_time1").val();
         var resto_time2 = $("#resto_time2").val();
         var resto_time3 = $("#resto_time3").val();
         var resto_time4 = $("#resto_time4").val();
         var resto_time5 = $("#resto_time5").val();
         var resto_time6 = $("#resto_time6").val();
         var resto_time7 = $("#resto_time7").val();
         var resto_time8 = $("#resto_time8").val();
         var resto_time9 = $("#resto_time9").val();
         var resto_time10 = $("#resto_time10").val();
         var resto_time11 = $("#resto_time11").val();
         var resto_time12 = $("#resto_time12").val();
         var resto_time13 = $("#resto_time13").val();
         var resto_time14 = $("#resto_time14").val();

         var resto_time1a = $("#resto_time1a").val();
         var resto_time2a = $("#resto_time2a").val();
         var resto_time3a = $("#resto_time3a").val();
         var resto_time4a = $("#resto_time4a").val();
         var resto_time5a = $("#resto_time5a").val();
         var resto_time6a = $("#resto_time6a").val();
         var resto_time7a = $("#resto_time7a").val();
         var resto_time8a = $("#resto_time8a").val();
         var resto_time9a = $("#resto_time9a").val();
         var resto_time10a = $("#resto_time10a").val();
         var resto_time11a = $("#resto_time11a").val();
         var resto_time12a = $("#resto_time12a").val();
         var resto_time13a = $("#resto_time13a").val();
         var resto_time14a = $("#resto_time14a").val();

         $("#resto_time3").val(resto_time1);
         $("#resto_time4").val(resto_time2);
         $("#resto_time5").val(resto_time1);
         $("#resto_time6").val(resto_time2);
         $("#resto_time7").val(resto_time1);
         $("#resto_time8").val(resto_time2);
         $("#resto_time9").val(resto_time1);
         $("#resto_time10").val(resto_time2);
         $("#resto_time11").val(resto_time1);
         $("#resto_time12").val(resto_time2);
         $("#resto_time13").val(resto_time1);
         $("#resto_time14").val(resto_time2);

         $("#resto_time3a").val(resto_time1a);
         $("#resto_time4a").val(resto_time2a);
         $("#resto_time5a").val(resto_time1a);
         $("#resto_time6a").val(resto_time2a);
         $("#resto_time7a").val(resto_time1a);
         $("#resto_time8a").val(resto_time2a);
         $("#resto_time9a").val(resto_time1a);
         $("#resto_time10a").val(resto_time2a);
         $("#resto_time11a").val(resto_time1a);
         $("#resto_time12a").val(resto_time2a);
         $("#resto_time13a").val(resto_time1a);
         $("#resto_time14a").val(resto_time2a);
      });

			$("#duplicate2").click(function(){
         var bar_time1 = $("#bar_time1").val();
         var bar_time2 = $("#bar_time2").val();
         var bar_time3 = $("#bar_time3").val();
         var bar_time4 = $("#bar_time4").val();
         var bar_time5 = $("#bar_time5").val();
         var bar_time6 = $("#bar_time6").val();
         var bar_time7 = $("#bar_time7").val();
         var bar_time8 = $("#bar_time8").val();
         var bar_time9 = $("#bar_time9").val();
         var bar_time10 = $("#bar_time10").val();
         var bar_time11 = $("#bar_time11").val();
         var bar_time12 = $("#bar_time12").val();
         var bar_time13 = $("#bar_time13").val();
         var bar_time14 = $("#bar_time14").val();

         var bar_time1a = $("#bar_time1a").val();
         var bar_time2a = $("#bar_time2a").val();
         var bar_time3a = $("#bar_time3a").val();
         var bar_time4a = $("#bar_time4a").val();
         var bar_time5a = $("#bar_time5a").val();
         var bar_time6a = $("#bar_time6a").val();
         var bar_time7a = $("#bar_time7a").val();
         var bar_time8a = $("#bar_time8a").val();
         var bar_time9a = $("#bar_time9a").val();
         var bar_time10a = $("#bar_time10a").val();
         var bar_time11a = $("#bar_time11a").val();
         var bar_time12a = $("#bar_time12a").val();
         var bar_time13a = $("#bar_time13a").val();
         var bar_time14a = $("#bar_time14a").val();

         $("#bar_time3").val(bar_time1);
         $("#bar_time4").val(bar_time2);
         $("#bar_time5").val(bar_time1);
         $("#bar_time6").val(bar_time2);
         $("#bar_time7").val(bar_time1);
         $("#bar_time8").val(bar_time2);
         $("#bar_time9").val(bar_time1);
         $("#bar_time10").val(bar_time2);
         $("#bar_time11").val(bar_time1);
         $("#bar_time12").val(bar_time2);
         $("#bar_time13").val(bar_time1);
         $("#bar_time14").val(bar_time2);

         $("#bar_time3a").val(bar_time1a);
         $("#bar_time4a").val(bar_time2a);
         $("#bar_time5a").val(bar_time1a);
         $("#bar_time6a").val(bar_time2a);
         $("#bar_time7a").val(bar_time1a);
         $("#bar_time8a").val(bar_time2a);
         $("#bar_time9a").val(bar_time1a);
         $("#bar_time10a").val(bar_time2a);
         $("#bar_time11a").val(bar_time1a);
         $("#bar_time12a").val(bar_time2a);
         $("#bar_time13a").val(bar_time1a);
         $("#bar_time14a").val(bar_time2a);
      });

</script>
