<?php
include '../../../config/database/db.php';

require_once("../../dompdf/dompdf_config.inc.php");
// require_once("../../dompdf/auto.php");

$sql=mysqli_query($koneksi,"SELECT *,r.id_negara as kode_negara, d.id_kota,
							(SELECT COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS jumlah_vote,
							(SELECT COUNT(f.id_member) FROM restaurant_like f WHERE f.id_restaurant=r.id_restaurant) AS jumlah_like,
							(SELECT COUNT(f.id_member) FROM restaurant_rekomendasi f WHERE f.id_restaurant=r.id_restaurant) AS jumlah_rekomendasi,
							(SELECT SUM(f.cleanlines)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_cleanlines,
							(SELECT SUM(f.customer_services)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_customer,
							(SELECT SUM(f.food_beverage)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_food,
							(SELECT SUM(f.comfort)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_comfort,
							(SELECT SUM(f.value_money)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_money,
							(SELECT (SUM(f.cleanlines) + SUM(f.customer_services) + SUM(f.food_beverage) + SUM(f.comfort) + SUM(f.value_money)) / COUNT(f.id_member)
							FROM restaurant_rating f WHERE f.id_restaurant =r.id_restaurant) AS total_rate
							FROM restaurant r
							LEFT JOIN type_of_business a ON r.id_type_of_business = a.id_type_of_business
							LEFT JOIN negara b ON r.id_negara = b.id_negara
							LEFT JOIN propinsi c ON r.id_propinsi = c.id_propinsi
							LEFT JOIN kota d ON r.id_kota = d.id_kota
							LEFT JOIN landmark e ON r.id_landmark = e.id_landmark
							LEFT JOIN mall f ON r.id_mall = f.id_mall
							LEFT JOIN operation_hour g ON r.id_operation_hour = g.id_operation_hour
							LEFT JOIN price_index i ON r.id_price_index = i.id_price_index
							LEFT JOIN suitable_for j ON r.id_suitable_for = j.id_suitable_for
							LEFT JOIN serving_time m ON r.id_serving_time = m.id_serving_time
							LEFT JOIN type_of_service n ON r.id_type_of_service = n.id_type_of_service
							LEFT JOIN wifi p ON r.id_wifi = p.id_wifi
							LEFT JOIN term_of_payment q ON r.id_term_of_payment = q.id_term_of_payment
							LEFT JOIN premise_security s ON r.id_premise_security = s.id_premise_security
							LEFT JOIN premise_fire_safety t ON r.id_premise_fire_safety = t.id_premise_fire_safety
							LEFT JOIN premise_maintenance u ON r.id_premise_maintenance = u.id_premise_maintenance
							LEFT JOIN parking_spaces v ON r.id_parking_spaces = v.id_parking_spaces
							LEFT JOIN ambience w ON r.id_ambience = w.id_ambience
							LEFT JOIN attire x ON r.id_attire = x.id_attire
							LEFT JOIN clean_washroom y ON r.id_clean_washroom = y.id_clean_washroom
							LEFT JOIN tables_availability z ON r.id_tables_availability = z.id_tables_availability
							LEFT JOIN noise_level ON r.id_noise_level = noise_level.id_noise_level
							LEFT JOIN waiter_tipping ON r.id_waiter_tipping = waiter_tipping.id_waiter_tipping
							LEFT JOIN member ON r.id_member = member.id_member
							LEFT JOIN air_conditioning ON r.id_air_conditioning = air_conditioning.id_air_conditioning
							LEFT JOIN heating_system ON r.id_heating_system = heating_system.id_heating_system
							LEFT JOIN premise_hygiene ON r.id_premise_hygiene = premise_hygiene.id_premise_hygiene where r.id_restaurant='$_GET[id]'");

                    	$r = mysqli_fetch_array($sql);
                    	$total_rating=number_format((float)$r['total_rate'], 2, '.', '');
                    	$totalku = $total_rating * 20;
				        
				        $global=mysqli_fetch_array(mysqli_query($koneksi, "SELECT *	FROM ( SELECT @curRank := @curRank + 1 AS rank, b.id_restaurant, b.restaurant_name , b.jumlah , b.rank_vote , b.rank_like , b.id_negara FROM restaurant b CROSS JOIN (SELECT @curRank := 0) vars  ORDER BY jumlah desc, rank_vote DESC, rank_like DESC, restaurant_name ASC) p WHERE p.id_restaurant='$_GET[id]'"));
				        $rank_global=$global['rank'];

				        $negara=mysqli_fetch_array(mysqli_query($koneksi, "SELECT *	FROM ( SELECT @curRank := @curRank + 1 AS rank , b.id_restaurant, b.restaurant_name, b.jumlah, b.rank_vote, b.rank_like, b.id_negara FROM restaurant b  CROSS JOIN (SELECT @curRank := 0) vars WHERE b.id_negara=$r[kode_negara] ORDER BY jumlah desc, rank_vote DESC, rank_like DESC, restaurant_name ASC ) p WHERE p.id_restaurant='$_GET[id]'"));
				        $rank_negara=$negara['rank'];

				        $state=mysqli_fetch_array(mysqli_query($koneksi, "SELECT *	FROM ( SELECT @curRank := @curRank + 1 AS rank, b.id_restaurant, b.restaurant_name, b.jumlah, b.rank_vote, b.rank_like, b.id_propinsi FROM restaurant b  CROSS JOIN (SELECT @curRank := 0) vars WHERE b.id_propinsi=$r[id_propinsi] ORDER BY jumlah desc, rank_vote DESC, rank_like DESC, restaurant_name ASC ) p WHERE p.id_restaurant='$_GET[id]'"));
			            $rank_state=$state['rank'];

			            $city=mysqli_fetch_array(mysqli_query($koneksi, "SELECT *	FROM ( SELECT @curRank := @curRank + 1 AS rank, b.id_restaurant, b.restaurant_name, b.jumlah, b.rank_vote, b.rank_like, b.id_kota FROM restaurant b CROSS JOIN (SELECT @curRank := 0) vars WHERE b.id_kota=$r[id_kota]  ORDER BY jumlah desc, rank_vote DESC, rank_like DESC, restaurant_name ASC) p WHERE p.id_restaurant='$_GET[id]'"));
				        $rank_city=$city['rank'];

				        if ($r['id_mall']<>0) {
				        	$pr=mysqli_fetch_array(mysqli_query($koneksi, "SELECT * FROM ( SELECT @curRank := @curRank + 1 AS rank, b.id_restaurant, b.restaurant_name, b.jumlah, b.rank_vote, b.rank_like, b.id_mall FROM restaurant b CROSS JOIN (SELECT @curRank := 0) vars WHERE b.id_mall=$r[id_mall] ORDER BY jumlah desc, rank_vote DESC, rank_like DESC, restaurant_name ASC) p WHERE p.id_restaurant='$_GET[id]'"));
					        $rank_mall=$pr['rank'];
					    }
				        else {
				        	$rank_mall="N.A.";
				        }
                    		$html = "
                    		<html style='margin: 0;'>";
                    		
                                $html .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>';
                            
                    		$html .="<style> 
                            
                            @font-face {
                              font-family: YaHei;
                              src: url('../../fonts/Yahei_Bold.ttf') format('truetype');
                              font-weight: bolder !important;
                              font-weight: 600;
                                font-style: normal;
                                font-stretch: normal;
                            }
                            @font-face {
                              font-family: arialu;
                              src: url('../../fonts/Arial-Unicode-Bold.ttf') format('truetype');
                              font-weight: bolder !important;
                              font-weight: 600;
                                font-style: normal;
                                font-stretch: normal;
                            }

.fa {
    display: inline;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 1;
    font-family: FontAwesome;
    font-size: inherit;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}                   			
@font-face {
	font-family: guardianregular;														  
	src: url('../../fonts/GuardianEgyp-Regular.ttf');
	font-weight: 400;
	font-style: normal;
	font-stretch: normal;
}
@font-face {
	font-family: guardianbold;
	src: url('../../fonts/GuardianEgyp-Semibold.ttf');
	font-weight: 600;
	font-style: normal;
	font-stretch: normal;
}
@font-face {
	font-family: guardianitalic;
	src: url('../../fonts/GuardianEgyp-RegularIt.ttf');
	font-weight: 400;
	font-style: italic;
	font-stretch: normal;
}
@font-face {
	font-family: myriadprobold;
	src: url('../../fonts/MyriadPro-Bold.ttf');
	font-style: normal;
	font-stretch: normal;
}

b{
	font-family: guardianbold;
}								
.serti{
    position: absolute;
    overflow: hidden;
    font-size: 218px;
    z-index: 9;
    font-family: YaHei !important;
    margin-top: 1170px;
    margin-left: auto;
    font-weight: bolder !important;
    width: 100%;
    text-align: center;
    color: #c01d28;
}
.alamat{
    position: absolute;
    overflow: hidden;
    font-size: 116px;
    z-index: 9;
    font-family: guardianregular;
    margin-top: 1511px;
    margin-left: auto;
    width: 100%;
    text-align: center;
    color: #000000;
}
.negara{
    position: absolute;
    overflow: hidden;
    font-size: 116px;
    z-index: 9;
    font-family: guardianregular;
    margin-top: 1730px;
    margin-left: auto;
    width: 100%;
    text-align: center;
    color: #000000;
}
.vote{
	position: absolute;
    overflow: hidden;
    font-size: 126px;
    z-index: 9;
    font-family: guardianbold;
    margin-top: 2630px;
    margin-left: auto;
    width: 100%;
    text-align: center;
    color: #000000;
}
.juvote {
    font-family: guardianitalic;
    font-size: 76px;
}
.bintang {
    position: absolute;
    top: 2194px;
    font-size: 105px;
    text-align: center;
    width: 1807px;
    color: #c20016;
    z-index: 999;
    margin-left: 643px;
}

.awal {
	width: 1806px;
    height: 341px;
    overflow: hidden;
    position: absolute;
    margin-left: 0;
    margin-top: 15px;
}
.awal img {
    position: absolute;
    left: 0%;
    margin: auto;
    min-height: 100%;
    min-width: 100%;
    height: 341px;
}
.akhir {
	width: 0%;
    height: 341px;
    overflow: hidden;
    position: absolute;
    margin-left: 0;
    margin-top: 15px;
    max-width: 1845px;
}
.akhir img {
    position: absolute;
    left: 0%;
    margin: auto;
    min-height: 100%;
    min-width: 100%;
    height: 341px;
}
.vglobal {
    position: absolute;
    overflow: hidden;
    font-size: 73px;
    z-index: 9;
    font-family: guardianbold;
    margin-top: 3000px;
    margin-left: 531px;
    text-align: center;
}

.vglobal span{
	color: #c4212d;
}
.vnegara {
    position: absolute;
    overflow: hidden;
    font-size: 73px;
    z-index: 9;
    font-family: guardianbold;
    margin-top: 3000px;
    margin-left: 1050px;
    text-align: center;    
}
.vnegara span{
	color: #c4212d;
}
.vstate {
    position: absolute;
    overflow: hidden;
    font-size: 73px;
    z-index: 9;
    font-family: guardianbold;
    margin-top: 3000px;
    margin-left: 1570px;
    text-align: center;    
}
.vstate span{
	color: #c4212d;
}
.vcity {
    position: absolute;
    overflow: hidden;
    font-size: 73px;
    z-index: 9;
    font-family: guardianbold;
    margin-top: 3000px;
    margin-left: 2090px;
    text-align: center; 
}
.vcity span{
	color: #c4212d;
}
.vmall {
    position: absolute;
    overflow: hidden;
    font-size: 73px;
    z-index: 9;
    font-family: guardianbold;
    margin-top: 3000px;
    margin-left: 2607px;
    text-align: center; 
}
.vmall span{
	color: #c4212d;
}
.tahun {
	position: absolute;
    overflow: hidden;
    font-size: 112px;
    z-index: 9;
    font-family: myriadprobold;
    margin-top: 3310px;
    margin-left: 1864px;
    text-align: center;
    color: #c4212d;
    letter-spacing: -4px;
}
.tahunbawah {
    position: absolute;
    overflow: hidden;
    font-size: 145px;
    z-index: 9;
    font-family: myriadprobold;
    margin-top: 3480px;
    margin-left: 2359px;
    text-align: center;
    color: #cc8e20;
    letter-spacing: -1;
}
.cn {
    position: absolute;
    overflow: hidden;
    font-size: 88px;
    z-index: 9;
    font-family: guardianregular;
    margin-top: 173px;
    margin-left: 2427px;
    text-align: center;
    color: #000000;
    width: 445px;
}
                    		</style>
                    		<body style='margin: 0;'>
	                    	<div style='width: 84.1cm; height: 118.9cm; position: absolute;'>
	                    		<div class='cn'>";
                                // $t = preg_split ('/(?<=[^а-я])(?=[а-я]+)/ius', $r['restaurant_name'], NULL, PREG_SPLIT_NO_EMPTY);
                                // $jum = count($t);
                                
                                
                                //     $CharNo = ord($t); 
                                //         if ($CharNo == 163) { 
                                //             echo "C";
                                //         } // keep £ 
                                        
                                //     }
                                
                               // if (preg_match('!!u', $r['restaurant_name']))
                               //  {
                               //    echo "UTF-8";
                               //  }
                               //  else 
                               //  {
                               //     echo "not utf-8"; 
                               //  }

                                // function remove_bs($Str) {  
                                //   $StrArr = str_split($Str); $NewStr = '';
                                //   foreach ($StrArr as $Char) {    
                                //     $CharNo = ord($Char);
                                //     if ($CharNo == 163) { $NewStr .= $Char; continue; } // keep £ 
                                //     if ($CharNo > 31 && $CharNo < 127) {
                                //       $NewStr .= $Char;    
                                //     }
                                //   }  
                                //   return $NewStr;
                                // }
                                // echo remove_bs($r['restaurant_name']);

                                // function remove_bsa($Str) {  
                                //   $StrArr = str_split($Str); 
                                //   $NewStr = '';
                                //   foreach ($StrArr as $Char) {    
                                //     $CharNo = ord($Char);                                    
                                //     if ($CharNo > 31 && $CharNo < 127) {
                                         
                                //     }
                                //     else{
                                //         $NewStr .= $Char; 
                                //     }
                                //   }  
                                //   return $NewStr;
                                // }
                                // echo remove_bsa($r['restaurant_name']);


	                    		$tgl = date("y");
	                    		if (strlen($_GET['id']) == 1) {
	                    			$nol = "0000";
	                    		}
	                    		elseif (strlen($_GET['id']) == 2) {
	                    			$nol = "000";
	                    		}
	                    		elseif (strlen($_GET['id']) == 3) {
	                    			$nol = "00";
	                    		}
	                    		elseif (strlen($_GET['id']) == 4) {
	                    			$nol = "0";
	                    		}
	                    		elseif (strlen($_GET['id']) == 5) {
	                    			$nol = "";
	                    		}
	                    		$cn = $tgl.$nol.$_GET['id'];

	                    		 $html .="$cn</div>
	                    		";	                    		
	                    			if (strlen($r['restaurant_name']) <= 25) {
	                    				$html .="<div class='serti'><span>$r[restaurant_name]</span></div>";
	                    			}
	                    			elseif(strlen($r['restaurant_name']) >= 26 && strlen($r['restaurant_name']) <= 30 ){
	                    				$html .="<div class='serti' style='margin-top: 1250px;'><span style='font-size: 139px'>$r[restaurant_name]</span></div>";
	                    			}
	                    			elseif(strlen($r['restaurant_name']) >= 35 && strlen($r['restaurant_name']) <= 45 ){
	                    				$html .="<div class='serti' style='margin-top: 1250px;'><b style='font-size: 125px'>$r[restaurant_name]</b></div>";
	                    			}
	                    			$html .="					            
						        
						        <div class='alamat'>";

                                    if (strlen($r['street_address']) <= 51) {
                                        $html .="<div>$r[street_address]</div>";
                                    }
                                    elseif(strlen($r['street_address']) >= 52 && strlen($r['street_address']) <= 63 ){
                                        $html .="<div style='font-size: 90px; margin-top: 22px;'>$r[street_address]</div>";
                                    }
                                    elseif(strlen($r['street_address']) >= 64){
                                        $html .="<div style='font-size: 74px;margin-top: 40px;'>$r[street_address]</div>";
                                    }
                                    //$r[street_address]
                                    //
                                    //<div style='font-size: 74px;margin-top: 40px;'>aaa sss sss sss www eee lll ddd ttt yyy kkk ppp ttt, www xxx bbb uuu ttt eee rrr</div>
                                $html .="
                                </div>
						        <div class='negara'>$r[nama_negara]</div>
						        <div class='bintang'>
						        	<div class='awal'><img src='../../images/st.png'></div>
						        	<div class='akhir' style='width: $totalku%;'><img src='../../images/sta.png'></div>
						        </div>						        
						        <div class='vote'>$total_rating / <i class='juvote'>$r[jumlah_vote] vote(s)</i></div>
						        <div class='vglobal'>
						        	# <span>$rank_global</span>
						        </div>
						        <div class='vnegara'>
						        	# <span>$rank_negara</span>
						        </div>
						        <div class='vstate'>
						        	# <span>$rank_state</span>
						        </div>
						        <div class='vcity'>
						        	# <span>$rank_city</span>
						        </div>
						        <div class='vmall'>
						        	# <span>$rank_mall</span>
						        </div>
						        <div class='tahun'>
						        	 ".date("Y")."
						        </div>
						        <div class='tahunbawah'>
						        	 ".date("Y")."
						        </div>
								<img src='../../images/certificate_a0.jpg' class='' style='width:100%; float: unset; box-shadow: 0px 0px 15px 0px #bbbbbb;'>
	                    	</div>
	                    	</body>
	                    	</html>";


	                    
							//This might be too large, but depends on the data set
							
                            // require_once '../../dompdf/autoload.inc.php';
                            // use Dompdf\Dompdf;

                            $dompdf = new DOMPDF();
                            $dompdf->load_html($html, 'UTF-8');		
                            $dompdf->set_paper("A0", "portrait");
                            $dompdf->render();
                            $dompdf->stream('restaurant_a0_'.$r[id_restaurant].'.pdf');


                            
                
							echo "$html";
?>
