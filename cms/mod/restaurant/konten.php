<section>
    <div class="mainwrapper">
        <?php include "inc/kiri.php"; ?>
        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="pageicon pull-left">
                        <i class="fa fa-tags"></i>
                    </div>
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href="mod-beranda-1.htm"><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="mod-beranda-1.htm">Beranda</a></li>
                            <li>Informasi Restaurant</li>
                        </ul>
                        <h4>Informasi Restaurant</h4>
                    </div>
                </div><!-- media -->
            </div><!-- pageheader -->

            <div class="contentpanel">
    				<?php
            if(isset($_SESSION['notif'])){
    					if($_SESSION['notif']=="hapus"){
    						echo"<div class='alert alert-success'>
        				 			<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
        				 			<strong>Success !</strong> Data has been deleted.
        				 		</div>";
    					}
    					unset($_SESSION['notif']);
            }
    				?>
                <div class="panel panel-default">
                    <div class="panel-heading">
						                <div class="row">
							                <div class="col-sm-11">
								            	<h4 class="panel-title">Daftar Restaurant</h4>
								                <p><a href="add-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-primary">Tambah Data</a></p>
							                </div>
						                </div>
					         </div>
                    <div class="panel-body">
						<table id="aledata" class="table table-striped table-bordered responsive">
							<thead class="">
								<tr>
									<th>No</th>
									<th>Restaurant </th>
									<th>Business Type </th>
									<th>Country </th>
									<th>Province </th>
									<th>City </th>
									<th>Thumbnail Pic </th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no=1;
								// select * from restaurant r 
								// left JOIN negara n on n.id_negara = r.id_negara
								// LEFT JOIN type_of_business tob on r.id_type_of_business = tob.id_type_of_business
								// LEFT JOIN restaurant_photo rp on r.id_restaurant = rp.id_restaurant
								// LEFT JOIN propinsi p on r.id_propinsi = p.id_propinsi
								// left JOIN kota k on r.id_kota = k.id_kota order by r.restaurant_name asc
								$sql=mysqli_query($koneksi,"select * from restaurant r left JOIN negara n on n.id_negara = r.id_negara LEFT JOIN type_of_business tob on r.id_type_of_business = tob.id_type_of_business LEFT JOIN restaurant_photo rp on r.id_restaurant = rp.id_restaurant LEFT JOIN propinsi p on r.id_propinsi = p.id_propinsi left JOIN kota k on r.id_kota = k.id_kota GROUP by r.id_restaurant order by r.restaurant_name asc");
								while($r=mysqli_fetch_array($sql)){
									echo"<tr>
										<td width='30px'>$no.</td>
										<td>$r[restaurant_name]</td>
										<td width='120px'>$r[nama_type_of_business]</td>
										<td width=''>$r[nama_negara]</td>
										<td width=''>$r[nama_propinsi]</td>
										<td width=''>$r[nama_kota]</td>
										<td width=''>"; if(!empty($r['gambar_restaurant_photo'])) { echo "<img src='$base/assets/img/restaurant/small_$r[gambar_restaurant_photo]' width='100px' class='img-thumbnail'>"; } echo "</td>
										<td width='180px'>
											<a href='edit-$mod-$r[id_restaurant]-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i></a>
											<a class='btn btn-danger btn-rounded btn-sm' href='mod/$mod/aksi.php?mod=$mod&id=$r[id_restaurant]&gambarthumb=$r[gambar_restaurant_photo]&ale=1&url=$_GET[url]&ale=1' onClick=\"return confirm('apakah anda yakin akan menghapus data ini ?')\"><i class='fa fa-trash-o'></i></a>
											<form action='mod/restaurant/print.php' mothod='get' style='width: 103px;float: right;'>
												<select name='size' class='form-control'>
													<option value='a4'>A4</option>
													<option value='a3'>A3</option>
													<option value='a0'>A0</option>
												</select>
												<input type='hidden' name='id' value='$r[id_restaurant]'>
												<button type='submit' class='btn btn-warning btn-rounded btn-sm'><i class='fa fa-print'></i></button>
												
											</form>
										</td>
									</tr>";
                  				$no++;
								}
								?>
							</tbody>
						</table>
					</div>
                </div><!-- panel -->
            </div><!-- contentpanel -->
        </div><!-- mainpanel -->
    </div><!-- mainwrapper -->
</section>
