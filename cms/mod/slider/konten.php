<section>
    <div class="mainwrapper">
        <?php include "inc/kiri.php"; ?>
        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="pageicon pull-left">
                        <i class="fa fa-tags"></i>
                    </div>
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href="mod-beranda-1.htm"><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="mod-beranda-1.htm">Beranda</a></li>
                            <li>Informasi Slider</li>
                        </ul>
                        <h4>Informasi Slider</h4>
                    </div>
                </div><!-- media -->
            </div><!-- pageheader -->

            <div class="contentpanel">
				<?php
				if(isset($_GET['info'])){
					if($_GET['info']==1){
						echo"<div class='alert alert-success'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Sukses!</strong> data berhasil dihapus.
						</div>";
					}
				}
				?>
                <div class="panel panel-default">
                    <div class="panel-heading">
						                <div class="row">
							                <div class="col-sm-11">
								            	<h4 class="panel-title">Daftar Slider</h4>
								               <p><a href="add-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-primary">Add Data</a></p>
							                </div>
						                </div>
					         </div>
                    <div class="panel-body">
						<table id="aledata" class="table table-striped table-bordered responsive">
							<thead class="">
								<tr>
									<th>No</th>
                                    <th>Gambar </th>
                                    <th>Urutan</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no=1;
								$sql=mysqli_query($koneksi,"SELECT * FROM `slideshow` ORDER BY `slideshow`.`urutan` ASC");
								while($r=mysqli_fetch_array($sql)){
									echo"<tr>
										<td width='30px'>$no.</td>
										<td style='text-align: center'>"; if(!empty($r['gambar'])) { echo "<img src='$base/assets/img/slideshow/$r[gambar]' width='500px' class='img-thumbnail'>"; } echo "</td>
										<td>$r[urutan]</td>
										<td width='80px'>
											<a href='edit-$mod-$r[id_slideshow]-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i></a>
											<a class='btn btn-danger btn-rounded btn-sm' href='mod/$mod/aksi.php?mod=$mod&id=$r[id_slideshow]&gambar=$r[gambar]&ale=1&url=$_GET[url]' onClick=\"return confirm('apakah anda yakin akan menghapus data ini ?')\"><i class='fa fa-trash-o'></i></a>
										</td>
									</tr>";
                  				$no++;
								}
								?>
                                
							</tbody>
						</table>
					</div>
                </div><!-- panel -->
            </div><!-- contentpanel -->
        </div><!-- mainpanel -->
    </div><!-- mainwrapper -->
</section>


<script>
jQuery(document).ready(function(){
	jQuery('#aledata').dataTable({
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
     // Bold the grade for all 'A' grade browsers
     if ( aData[4] == "A" )
     {
       $('td:eq(4)', nRow).html( '<b>A</b>' );
     }
   }
 } );
		"sPaginationType": "full_numbers",
		"iDisplayLength": 25,
		"aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, "Semua"]]
    });
});
</script>
