<section>
    <div class="mainwrapper">
        <?php include "inc/kiri.php"; ?>
        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="pageicon pull-left">
                        <i class="fa fa-tags"></i>
                    </div>
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href="mod-beranda-1.htm"><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="mod-beranda-1.htm">Beranda</a></li>
                            <li>Informasi About</li>
                        </ul>
                        <h4>Informasi About</h4>
                    </div>
                </div><!-- media -->
            </div><!-- pageheader -->

            <div class="contentpanel">
				<?php
				if(isset($_GET['info'])){
					if($_GET['info']==1){
						echo"<div class='alert alert-success'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Sukses!</strong> data berhasil dihapus.
						</div>";
					}
				}
				?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                                <div class="col-sm-11">
                                    <h4 class="panel-title">About</h4>
                                    <!--<p><a href="add-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-primary">Tambah Data</a></p>-->
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table id="aledata" class="table table-striped table-bordered responsive">
                                <thead class="">
                                    <tr>
                                        <th>Judul</th>
                                        <th>Isi </th>
                                        <th>Gambar </th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no=1;
                                    $sql=mysqli_query($koneksi,"select * from about");
                                    while($r=mysqli_fetch_array($sql)){
                                        echo"<tr>
                                            <td>$r[judul_about]</td>
											<td>$r[isi_about]</td>
											<td style='text-align: center' width='100px'>"; if(!empty($r['gambar_about'])) { echo "<img src='$base/assets/img/$r[gambar_about]' width='100px' class='img-thumbnail'>"; } echo "</td>
                                            <td width='80px'>
                                                    <a href='edit-$mod-$r[id_about]-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>
                                            </td>
                                        </tr>";
                      				$no++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                </div><!-- panel -->
            </div><!-- contentpanel -->
        </div><!-- mainpanel -->
    </div><!-- mainwrapper -->
</section>

<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/pace.min.js"></script>
<script src="js/retina.min.js"></script>
<script src="js/jquery.cookies.js"></script>

<script src="js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/plug-ins/725b2a2115b/integration/bootstrap/3/dataTables.bootstrap.js"></script>
<script src="//cdn.datatables.net/responsive/1.0.1/js/dataTables.responsive.js"></script>
<script src="js/custom.js"></script>
<script>
jQuery(document).ready(function(){
	jQuery('#aledata').dataTable({
		"sPaginationType": "full_numbers",
		"iDisplayLength": 25,
		"aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, "Semua"]]
    });
});
</script>
