<?php
session_start();
if(!empty($_SESSION['ak_id'])){
	include "../../inc/db.php";
	include "../../inc/upload.php";
	include "../../inc/id_masking.php";
	include '../../inc/seo.php';
	include "../../../config/func/upload_gambar_bms.php";

	$mod			= $_GET['mod'];
	$tglhariini		= date("Y-m-d");
	$ukuran_maks	= 552428800;
	$error_cover 	= 0;
	$error_profil 	= 0;

	if($_GET['ale']==1){
		mysqli_query($koneksi,"DELETE FROM article WHERE id_article='$_GET[id]'");
		mysqli_query($koneksi,"DELETE FROM article_photo WHERE id_article='$_GET[id]'");
		unlink("../../../assets/img/article/small_".$_GET['gambarthumb']);
		unlink("../../../assets/img/article/medium_".$_GET['gambarthumb']);
		unlink("../../../assets/img/article/big_".$_GET['gambarthumb']);
		header("Location: ../../mod-article-12.htm");
	}
	elseif($_GET['ale']==2){ 
		date_default_timezone_set("Asia/Jakarta");
		$sekarang=date("Y-m-d H:i:s");

		$ukuran_maks	= 552428800;

		$nama_cover			= $_FILES['gambar_photo']['name'];
		$ukuran_cover 		= $_FILES['gambar_photo']['size'];
		$lokasi_cover		= $_FILES['gambar_photo']['tmp_name'];
		list($width_cover, $height_cover) = getimagesize($lokasi_cover);
		if($ukuran_cover > $ukuran_maks or !preg_match("/.(gif|jpg|png)$/i", $nama_cover) or $width_cover < 800 or $height_cover < 600){
			$_SESSION['notif']     = "gambar";
			header("Location: ../../add-article-12.htm");
			exit();
		}
		else{
			$post = $_POST['inputs'];
			$sql=mysqli_query($koneksi,"INSERT INTO article(". implode(',', array_keys($post)) .", tgl_post) VALUES('". implode("','", array_values($post)) ."','{$sekarang}')");
			if($sql){
				$post = $_POST['inputs'];
				$cover=upload_gambar($nama_cover,$lokasi_cover,"../../../assets/img/article");
				$id_article=mysqli_insert_id($koneksi);
				mysqli_query($koneksi,"INSERT INTO article_photo (id_member,id_article,nama_article_photo,gambar_article_photo) VALUES('$post[id_member]','$id_article','$post[nama_article]','$cover')");
				mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('article','$id_article')");
				// if($_POST['feed_submit']==1){
					mysqli_query($koneksi,"INSERT INTO activity (id_member,kat_act,deskripsi,tgl) VALUES('$post[id_member]','feed_submit','Submit new article review','$sekarang')");
				// }
				$id = id_masking($id_article);
				header("Location: ../../mod-article-12.htm");
				exit();
			}
		}
	}



	elseif($_GET['ale']==3){
		date_default_timezone_set("Asia/Jakarta");
		$sekarang=date("Y-m-d H:i:s");
				
		$upadate = array();
		foreach($_POST['inputs'] as $name => $val)
		{
			$upadate[] = $name ."='". $val ."'";
		}

		mysqli_query($koneksi,"update article set ". implode(',', $upadate) ." where id_article='$_POST[id]'");
		$_SESSION['notif'] = "sukses";
		header("Location: ../../mod-article-12.htm");
		exit();
	}
}
else{
	header('location:not-found.htm');
}
?>
