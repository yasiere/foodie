<style>
	html, body {
		height: 100%;
		margin: 0;
		padding: 0;
	}
	#map {
		width: 100%;
		height: 400px;
	}
	.controls {
		margin-top: 10px;
		border: 1px solid transparent;
		border-radius: 2px 0 0 2px;
		box-sizing: border-box;
		-moz-box-sizing: border-box;
		height: 32px;
		outline: none;
		box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
	}
	#searchInput {
		background-color: #fff;
		font-family: Roboto;
		font-size: 15px;
		font-weight: 300;
		margin-left: 12px;
		padding: 0 11px 0 13px;
		text-overflow: ellipsis;
		width: 50%;
	}
	#searchInput:focus {
	 	border-color: #4d90fe;
	}
</style>

<section>
    <div class="mainwrapper">
        <?php include "inc/kiri.php"; ?>
         <div class="mainpanel">
             <div class="pageheader">
                <div class="media">
                    <div class="pageicon pull-left">
                        <i class="fa fa-picture-o"></i>
                    </div>
                    <div class="media-body">
											<ul class="breadcrumb">
                          <li><a href="mod-beranda.htm"><i class="glyphicon glyphicon-home"></i></a></li>
                          <li><a href="mod-beranda.htm">Beranda</a></li>
                          <li>Restaurant Menu</li>
                      </ul>
                      <h4>Restaurant Menu</h4>
                    </div>
                </div><!-- media -->
            </div><!-- pageheader -->
            <div class="contentpanel">
				<?php if($_GET['proses']==1){ ?>

				<?php
				if(isset($_GET['info'])){
					if($_GET['info']==1){
						echo"<div class='alert alert-success'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Sukses!</strong> data berhasil disimpan.
						</div>";
					}
					elseif($_GET['info']==2){
						echo"<div class='alert alert-danger'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Kesalahan!</strong> tipe file tidak cocok, anda harus mengupload file berekstensi *.JPG.
						</div>";
					}
					else{
						echo"<div class='alert alert-danger'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Kesalahan!</strong> ukuran file terlalu besar, ukuran maksimal yang diperbolehkan adalah 300kb.
						</div>";
					}
				}
				?>
        <form action="mod/<?php echo"$mod/aksi.php?mod=$mod&ale=2&url=$_GET[url]"; ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" id="signup-daftar">
          <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-btns">
                  <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                </div><!-- panel-btns -->
		            <h4 class="panel-title">Data Restaurant</h4>
                <p>Silahkan lengkapi data dibawah ini.</p>
            </div>
            <div class="panel-body nopadding">
							<div class="form-group">
								<label class="col-sm-2 control-label">Member :</label>
								<div class="col-sm-10">
									<select name="id_member" class="pilih" style="width: 100%;">
									<?php
										$sql=mysqli_query($koneksi,"select id_member,username from member order by username asc");
										while($b=mysqli_fetch_array($sql)){
											echo"<option value='$b[id_member]'>$b[username]</option>";
										}
									?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Country :</label>
								<div class="col-sm-10">
									<select name="negara" class="pilih" style="width: 100%;" id="negara">
									<option value="">Select Country</option>
										<?php
												$sql=mysqli_query($koneksi,"select id_negara,nama_negara from negara order by nama_negara asc");
												while($b=mysqli_fetch_array($sql)){
													echo"<option value='$b[id_negara]'>$b[nama_negara]</option>";
												}
											?>
									 </select>
								</div>
							</div>
							<div class="form-group">
								 <label class="col-sm-2 control-label">State/Province :</label>
								 <div class="col-sm-10">
										 <select name="propinsi" class="pilih" style="width: 100%;" id="propinsi">
												<option value="">Select State/ Province</option>
											</select>
								 </div>
							</div>
							<div class="form-group">
								 <label class="col-sm-2 control-label">City :</label>
								 <div class="col-sm-10">
										 <select name="kota" class="pilih" style="width: 100%;" id="kota">
												<option value="">Select City</option>
											</select>
								 </div>
							</div>
							<div class="form-group">
                 <label class="col-sm-2 control-label">Restaurant Name :</label>
                 <div class="col-sm-10">
                     <input type="text" name="restaurant_name" class="form-control" id="search-box">
										 <div id="suggesstion-box"></div>
                 </div>
              </div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Photo Title <span class="f-merah">*</span></label>
								<div class="col-sm-10">
									<input type="text" class="form-control" placeholder="Write Name" name="photo_name" required maxlength="50">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Cover Photo <span class="f-merah">*</span></label>
								<div class="col-sm-10">
									<div class="fileinput fileinput-new" data-provides="fileinput">
									  <div class="fileinput-preview upload" data-trigger="fileinput"></div>
									  <input type="file" name="gambar_photo" class="hidden" required>
									</div>
									<p class="help-block">Image used have to be in JPG, PNG, and GIF format and the maximum total images size is 50MB also minimal dimension must be 800x600 pixel.</p>
								</div>
							</div>
            </div><!-- panel-body -->
          </div><!-- panel -->
					<div class="panel panel-default">
						<div class="panel-footer">
	            <button class="btn btn-primary mr5">Simpan</button>
	            <a href="mod-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-warning">Kembali</a>
	          </div><!-- panel-footer -->
	        </div><!-- panel -->
				</form>


















				<?php
				}else{
          $edit=mysqli_query($koneksi,"select * from menu where id_menu = '$_GET[id]'");
          $e=mysqli_fetch_array($edit);
				?>
				<form action="mod/<?php echo"$mod/aksi.php?mod=$mod&ale=3&url=$_GET[url]"; ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" id="signup-daftar">
					<input type="hidden" name="id" value="<?php echo $e['id_menu']; ?>">
					<div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-btns">
                  <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                </div><!-- panel-btns -->
		            <h4 class="panel-title">Data Restaurant Photo</h4>
                <p>Silahkan lengkapi data dibawah ini.</p>
            </div>
            <div class="panel-body nopadding">
							<div class="form-group">
								<label class="col-sm-2 control-label">Member :</label>
								<div class="col-sm-10">
									<select name="id_member" class="pilih" style="width: 100%;">
									<?php
										$sql=mysqli_query($koneksi,"select id_member,username from member order by username asc");
										while($b=mysqli_fetch_array($sql)){
											echo"<option value='$b[id_member]'"; if($b['id_member']==$e['id_member']){ echo "selected";} echo ">$b[username]</option>";
										}
									?>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label">Photo Title <span class="f-merah">*</span></label>
								<div class="col-sm-10">
									<input type="text" class="form-control" placeholder="Write Name" name="photo_name" required maxlength="50" value="<?php echo"$e[nama_menu]" ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Cover Photo <span class="f-merah">*</span></label>
								<div class="col-sm-10">
									<div class="fileinput fileinput-new" data-provides="fileinput">
									  <div class="fileinput-preview upload" data-trigger="fileinput">
		                  <img src="<?php echo"$base/assets/img/menu/small_$e[gambar_menu]"; ?>">
										</div>
									  <input type="file" name="gambar_photo" class="hidden">
									</div>
									<p class="help-block">Image used have to be in JPG, PNG, and GIF format and the maximum total images size is 50MB also minimal dimension must be 800x600 pixel.</p>
								</div>
							</div>
            </div><!-- panel-body -->
          </div><!-- panel -->
					<div class="panel panel-default">
						<div class="panel-footer">
	            <button class="btn btn-primary mr5">Simpan</button>
	            <a href="mod-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-warning">Kembali</a>
	          </div><!-- panel-footer -->
	        </div><!-- panel -->
				</form>

				<?php

				  } ?>
			</div><!-- contentpanel -->
        </div><!-- mainpanel -->
    </div><!-- mainwrapper -->
</section>


<script src="js/jquery-1.11.1.min.js"></script>



<script type="text/javascript">
$(document).ready(function() {
  $(".pilih").select2();
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('.tip').tooltip();

  var x = 1;
  $(".tambah_menu").click(function(e){ //on add input button click
    e.preventDefault();
    if(x < 30){ //max input box allowed
      x++; //text box increment
      $(".menu_wrap").append('<div class="row mb20"><div class="col-md-12 "><div class="fileinput fileinput-new" data-provides="fileinput"> <div class="fileinput-preview upload" data-trigger="fileinput"></div><input type="file" name="menu[]" class="hidden"></div></div><div class="col-md-6"><input type="text" class="form-control mt30" placeholder="Photo Caption" name="menu_caption[]"></div><div class="col-1"><button type="button" class="hapus_menu btn btn-danger mt30">Remove</button></div></div>'); //add input box
    }
    if(x > 29){
      $(".tambah_menu").hide();
    }
  });

  $(".menu_wrap").on("click",".hapus_menu", function(e){ //user click on remove text
    e.preventDefault(); $(this).parent().parent('.row').remove(); x--;
    if($(".tambah_menu").is(":hidden")){
      $(".tambah_menu").show();
    }
  });


		$("#negara").change(function(){
			var id = $("#negara").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base"; ?>/config/func/ajax_propinsi.php",
				data: "id=" + id,
				success: function(data){
					$("#propinsi").html(data);
					$("#propinsi").fadeIn(2000);
				}
			});
		});
		$("#propinsi").change(function(){
			var id = $("#propinsi").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base"; ?>/config/func/ajax_kota.php",
				data: "id=" + id,
				success: function(data){
					$("#kota").html(data);
					$("#kota").fadeIn(2000);
				}
			});
		});
    $("#kota").change(function(){
			var id = $("#kota").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base"; ?>/config/func/ajax_mall.php",
				data: "id=" + id,
				success: function(data){
					$("#mall").html(data);
					$("#mall").fadeIn(2000);
				}
			});
		});
		$("#search-box").keyup(function(){
			var id = $("#kota").val();
			$.ajax({
			type: "POST",
			url: "<?php echo"$base"; ?>/config/func/resto.php",
			data:'keyword='+$(this).val() + '&id='+id,
				success: function(data){
					$("#suggesstion-box").show();
					$("#suggesstion-box").html(data);
					$("#search-box").css("background","#FFF");
				}
			});
		});

});
function selectCountry(val) {
	 $("#search-box").val(val);
	 $("#suggesstion-box").hide();
}


      $("#duplicate").click(function(){
         var resto_time1 = $("#resto_time1").val();
         var resto_time2 = $("#resto_time2").val();
         var resto_time3 = $("#resto_time3").val();
         var resto_time4 = $("#resto_time4").val();
         var resto_time5 = $("#resto_time5").val();
         var resto_time6 = $("#resto_time6").val();
         var resto_time7 = $("#resto_time7").val();
         var resto_time8 = $("#resto_time8").val();
         var resto_time9 = $("#resto_time9").val();
         var resto_time10 = $("#resto_time10").val();
         var resto_time11 = $("#resto_time11").val();
         var resto_time12 = $("#resto_time12").val();
         var resto_time13 = $("#resto_time13").val();
         var resto_time14 = $("#resto_time14").val();

         var resto_time1a = $("#resto_time1a").val();
         var resto_time2a = $("#resto_time2a").val();
         var resto_time3a = $("#resto_time3a").val();
         var resto_time4a = $("#resto_time4a").val();
         var resto_time5a = $("#resto_time5a").val();
         var resto_time6a = $("#resto_time6a").val();
         var resto_time7a = $("#resto_time7a").val();
         var resto_time8a = $("#resto_time8a").val();
         var resto_time9a = $("#resto_time9a").val();
         var resto_time10a = $("#resto_time10a").val();
         var resto_time11a = $("#resto_time11a").val();
         var resto_time12a = $("#resto_time12a").val();
         var resto_time13a = $("#resto_time13a").val();
         var resto_time14a = $("#resto_time14a").val();

         $("#resto_time3").val(resto_time1);
         $("#resto_time4").val(resto_time2);
         $("#resto_time5").val(resto_time1);
         $("#resto_time6").val(resto_time2);
         $("#resto_time7").val(resto_time1);
         $("#resto_time8").val(resto_time2);
         $("#resto_time9").val(resto_time1);
         $("#resto_time10").val(resto_time2);
         $("#resto_time11").val(resto_time1);
         $("#resto_time12").val(resto_time2);
         $("#resto_time13").val(resto_time1);
         $("#resto_time14").val(resto_time2);

         $("#resto_time3a").val(resto_time1a);
         $("#resto_time4a").val(resto_time2a);
         $("#resto_time5a").val(resto_time1a);
         $("#resto_time6a").val(resto_time2a);
         $("#resto_time7a").val(resto_time1a);
         $("#resto_time8a").val(resto_time2a);
         $("#resto_time9a").val(resto_time1a);
         $("#resto_time10a").val(resto_time2a);
         $("#resto_time11a").val(resto_time1a);
         $("#resto_time12a").val(resto_time2a);
         $("#resto_time13a").val(resto_time1a);
         $("#resto_time14a").val(resto_time2a);
      });

			$("#duplicate2").click(function(){
         var bar_time1 = $("#bar_time1").val();
         var bar_time2 = $("#bar_time2").val();
         var bar_time3 = $("#bar_time3").val();
         var bar_time4 = $("#bar_time4").val();
         var bar_time5 = $("#bar_time5").val();
         var bar_time6 = $("#bar_time6").val();
         var bar_time7 = $("#bar_time7").val();
         var bar_time8 = $("#bar_time8").val();
         var bar_time9 = $("#bar_time9").val();
         var bar_time10 = $("#bar_time10").val();
         var bar_time11 = $("#bar_time11").val();
         var bar_time12 = $("#bar_time12").val();
         var bar_time13 = $("#bar_time13").val();
         var bar_time14 = $("#bar_time14").val();

         var bar_time1a = $("#bar_time1a").val();
         var bar_time2a = $("#bar_time2a").val();
         var bar_time3a = $("#bar_time3a").val();
         var bar_time4a = $("#bar_time4a").val();
         var bar_time5a = $("#bar_time5a").val();
         var bar_time6a = $("#bar_time6a").val();
         var bar_time7a = $("#bar_time7a").val();
         var bar_time8a = $("#bar_time8a").val();
         var bar_time9a = $("#bar_time9a").val();
         var bar_time10a = $("#bar_time10a").val();
         var bar_time11a = $("#bar_time11a").val();
         var bar_time12a = $("#bar_time12a").val();
         var bar_time13a = $("#bar_time13a").val();
         var bar_time14a = $("#bar_time14a").val();

         $("#bar_time3").val(bar_time1);
         $("#bar_time4").val(bar_time2);
         $("#bar_time5").val(bar_time1);
         $("#bar_time6").val(bar_time2);
         $("#bar_time7").val(bar_time1);
         $("#bar_time8").val(bar_time2);
         $("#bar_time9").val(bar_time1);
         $("#bar_time10").val(bar_time2);
         $("#bar_time11").val(bar_time1);
         $("#bar_time12").val(bar_time2);
         $("#bar_time13").val(bar_time1);
         $("#bar_time14").val(bar_time2);

         $("#bar_time3a").val(bar_time1a);
         $("#bar_time4a").val(bar_time2a);
         $("#bar_time5a").val(bar_time1a);
         $("#bar_time6a").val(bar_time2a);
         $("#bar_time7a").val(bar_time1a);
         $("#bar_time8a").val(bar_time2a);
         $("#bar_time9a").val(bar_time1a);
         $("#bar_time10a").val(bar_time2a);
         $("#bar_time11a").val(bar_time1a);
         $("#bar_time12a").val(bar_time2a);
         $("#bar_time13a").val(bar_time1a);
         $("#bar_time14a").val(bar_time2a);
      });

</script>
<style media="screen">
#country-list {
	margin: 0;
	list-style: none;
	padding: 0px;
	max-height: 155px;
	position: relative;
	overflow: auto;
}
.ko {
	border: 1px solid #CCC;
	padding: 5px;
	color: #9C9999;
	border-top: none;
	cursor: pointer;
	transition: 0.7s all 0s ease;
}
.ko:hover{
	background: #ffeed0;
  transition: 0.3s all 0s ease;
}
</style>
