
<section>
    <div class="mainwrapper">
        <?php include "inc/kiri.php"; ?>
         <div class="mainpanel">
             <div class="pageheader">
                <div class="media">
                    <div class="pageicon pull-left">
                        <i class="fa fa-home"></i>
                    </div>
                    <div class="media-body">
						<ul class="breadcrumb">
                            <li><a href="mod-beranda.htm"><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="mod-beranda.htm">Beranda</a></li>
                            <li>Informasi News Event</li>
                        </ul>
                        <h4>Informasi News Event</h4>
                    </div>
                </div><!-- media -->
            </div><!-- pageheader -->
            <div class="contentpanel">
				<?php if($_GET['proses']==1){ ?>
				<form action="mod/<?php echo"$mod/aksi.php?mod=$mod&ale=2&url=$_GET[url]"; ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data">
				<?php
				if(isset($_GET['info'])){
					if($_GET['info']==1){
						echo"<div class='alert alert-success'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Sukses!</strong> data berhasil disimpan.
						</div>";
					}
					elseif($_GET['info']==2){
						echo"<div class='alert alert-danger'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Kesalahan!</strong> tipe file tidak cocok, anda harus mengupload file berekstensi *.JPG.
						</div>";
					}
					else{
						echo"<div class='alert alert-danger'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Kesalahan!</strong> ukuran file terlalu besar, ukuran maksimal yang diperbolehkan adalah 300kb.
						</div>";
					}
				}
				?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-btns">
                            <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                        </div><!-- panel-btns -->
						<h4 class="panel-title">Data News Event</h4>
                        <p>Silahkan lengkapi data dibawah ini.</p>
                    </div>
                    <div class="panel-body nopadding">
                      <div class="form-group">
                           <label class="col-sm-2 control-label">Tanggal :</label>
                           <div class="col-sm-10">
                               <input type="text" name="tgl_newsevent" class="form-control" required id="datepicker">
                           </div>
                       </div>
                       <div class="form-group">
                           <label class="col-sm-2 control-label">Judul :</label>
                           <div class="col-sm-10">
                               <input type="text" name="judul_newsevent" class="form-control">
                           </div>
                       </div>
                       <div class="form-group">
                           <label class="col-sm-2 control-label">Isi :</label>
                           <div class="col-sm-10">
                               <textarea rows="10" class="form-control ckeditor" name="isi_newsevent"></textarea>
                           </div>
                       </div>
						            <div class="form-group">
                            <label class="col-sm-2 control-label">Gambar :</label>
                            <div class="col-sm-10">
								              <input type="file" name="fupload" title="Pilih" class="btn btn-primary" required>
                            </div>
                        </div><!-- form-group -->
                    </div><!-- panel-body -->
                </div><!-- panel -->



				<div class="panel panel-default">
					<div class="panel-footer">
                        <button class="btn btn-primary mr5">Simpan</button>
                        <a href="mod-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-warning">Kembali</a>
                     </div><!-- panel-footer -->
                </div><!-- panel -->
				</form>
				<?php
				}else{
				$dat=mysqli_query($koneksi,"select * from newsevent where id_newsevent='$_GET[id]'");
				$d=mysqli_fetch_array($dat);
				?>
				<form action="mod/<?php echo"$mod/aksi.php?mod=$mod&ale=3&url=$_GET[url]"; ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php echo"$_GET[id]"; ?>" >
				<?php
				if(isset($_GET['info'])){
					if($_GET['info']==1){
						echo"<div class='alert alert-success'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Sukses!</strong> data berhasil disimpan.
						</div>";
					}
					elseif($_GET['info']==2){
						echo"<div class='alert alert-danger'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Kesalahan!</strong> tipe file tidak cocok, anda harus mengupload file berekstensi *.JPG.
						</div>";
					}
					else{
						echo"<div class='alert alert-danger'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Kesalahan!</strong> ukuran file terlalu besar, ukuran maksimal yang diperbolehkan adalah 300kb.
						</div>";
					}
				}
				?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-btns">
                            <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                        </div><!-- panel-btns -->
						<h4 class="panel-title">Detail Data News Event</h4>
                        <p>Silahkan lengkapi data dibawah ini.</p>
                    </div>
                    <div class="panel-body nopadding">
                   		 <div class="form-group">
                            <label class="col-sm-2 control-label">Tanggal :</label>
                            <div class="col-sm-10">
                                <input type="text" name="tgl_newsevent" class="form-control" required value="<?php echo"$d[tgl_newsevent]"; ?>" id="datepicker">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Judul :</label>
                            <div class="col-sm-10">
                                <input type="text" name="judul_newsevent" class="form-control" value="<?php echo"$d[judul_newsevent]"; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Isi :</label>
                            <div class="col-sm-10">
                                <textarea rows="10" class="form-control ckeditor" name="isi_newsevent"><?php echo"$d[isi_newsevent]"; ?></textarea>
                            </div>
                        </div>
                      <div class="form-group">
                           <label class="col-sm-2 control-label">Gambar :</label>
                           <div class="col-sm-10">
                             <input type="file" name="fupload" title="Pilih" class="btn btn-primary">
                             <br />
                             <?
                             $ext = strtolower(pathinfo($d['gmb_newsevent'], PATHINFO_EXTENSION)); // Using strtolower to overcome case sensitive
                             $supported_image = array(
                       				'gif',
                       				'jpg',
                       				'jpeg',
                       				'png'
                       			  );
                              if (in_array($ext, $supported_image)) {
                     					echo "<img src='../newsevent/".$d['gmb_newsevent']."' width='300'> ";

                     			    } else {
                     					echo "<video width='300' controls>
                     							<source src='../assets/img/".$d['gmb_newsevent']."' type='video/mp4'>
                     							Your browser does not support the video tag.
                     							</video> ";
                     				  }
                             ?>

                             <input type="hidden" name="gmb_newsevent" value="<? echo "$d[gmb_newsevent]";?>">
                           </div>
                      </div>
                    </div><!-- panel-body -->
                </div><!-- panel -->



				<div class="panel panel-default">

					<div class="panel-footer">
                        <button class="btn btn-primary mr5">Simpan</button>
                        <a href="mod-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-warning">Kembali</a>
                     </div><!-- panel-footer -->
                </div><!-- panel -->
				</form>
				<?php } ?>
			</div><!-- contentpanel -->
        </div><!-- mainpanel -->
    </div><!-- mainwrapper -->
</section>

<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/pace.min.js"></script>
<script src="js/retina.min.js"></script>
<script src="js/jquery.cookies.js"></script>

<script src="js/select2.min.js"></script>
<script src="js/bootstrap-timepicker.min.js"></script>
<script src="js/bootstrap.file-input.js"></script>
<script src="js/custom.js"></script>
<script>
jQuery(document).ready(function() {
	$('input[type=file]').bootstrapFileInput();
	jQuery('.select-search-hide').select2({
        minimumResultsForSearch: -1
    });
	jQuery('#cekin').timepicker({
		showMeridian: false,
		<?php
		if($s['cek_in']<>""){
			echo"defaultTime:'$s[cek_in]'";
		}
		else{
			echo"defaultTime:'12:00'";
		}
		?>
	});
	jQuery('#cekout').timepicker({
		showMeridian: false,
		<?php
		if($s['cek_out']<>""){
			echo"defaultTime:'$s[cek_out]'";
		}
		else{
			echo"defaultTime:'13:00'";
		}
		?>
	});
	$("#provinsi").change(function(){
		var id = $("#provinsi").val();
		$.ajax({
			type:"POST",
			url: "../ajax/provinsi.php",
			data: "id=" + id,
			success: function(data){
				$("#kota").html(data);
				$("#kota").fadeIn(2000);
			}
		});
	});
	$("#kota").change(function(){
		var id = $("#kota").val();
		$.ajax({
			type:"POST",
			url: "../ajax/kota.php",
			data: "id=" + id,
			success: function(data){
				$("#kecamatan").html(data);
				$("#kecamatan").fadeIn(2000);
			}
		});
	});
});
</script>
