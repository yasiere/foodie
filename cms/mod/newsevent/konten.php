<section>
    <div class="mainwrapper">
        <?php include "inc/kiri.php"; ?>
        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="pageicon pull-left">
                        <i class="fa fa-tags"></i>
                    </div>
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href="mod-beranda-1.htm"><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="mod-beranda-1.htm">Beranda</a></li>
                            <li>Informasi News and Event</li>
                        </ul>
                        <h4>Informasi News and Event </h4>
                    </div>
                </div><!-- media -->
            </div><!-- pageheader -->

            <div class="contentpanel">
				<?php
				if(isset($_GET['info'])){
					if($_GET['info']==1){
						echo"<div class='alert alert-success'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Sukses!</strong> data berhasil dihapus.
						</div>";
					}
				}
				?>
                <div class="panel panel-default">
                    <div class="panel-heading">
						                <div class="row">
							                <div class="col-sm-11">
								                    <h4 class="panel-title">Daftar News & Event</h4>
								                            <p><a href="add-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-primary">Tambah Data</a></p>
							                </div>
						                </div>
					         </div>
                    <div class="panel-body">
						<table id="aledata" class="table table-striped table-bordered responsive">
							<thead class="">
								<tr>
									<th>No</th>
									<th>Judul</th>
									<th>Isi</th>
									<th>Tanggal</th>
                                    <th>Gambar</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no=1;
								$sql=mysqli_query($koneksi,"select * from newsevent order by id_newsevent ASC");
								while($r=mysqli_fetch_array($sql)){
									$tgl_event = tgl_indo($r['tgl_newsevent']);
									$ext = strtolower(pathinfo($r['gmb_newsevent'], PATHINFO_EXTENSION)); // Using strtolower to overcome case sensitive
									$supported_image = array(
											'gif',
											'jpg',
											'jpeg',
											'png'
										);
									echo"<tr>
										<td width='30px'>$no.</td>
										<td>$r[judul_newsevent]</td>
										<td>$r[isi_newsevent]</td>
										<td>$tgl_event</td>";
										if (in_array($ext, $supported_image)) {
													echo "<td style='text-align: center' width='100px' > <img src='../newsevent/".$r['gmb_newsevent']."' width='100px' class='img-thumbnail'> </td>";
					
												} else {
													echo "<td style='text-align: center'>	<video width='300'>
															<source src='../assets/img/".$r['gmb_newsevent']."' type='video/mp4'>
															Your browser does not support the video tag.
															</video> </td> ";
												}
										echo "
										<td width='170px'>
											<a href='edit-$mod-$r[id_newsevent]-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>
											<a class='btn btn-danger btn-rounded btn-sm' href='mod/$mod/aksi.php?mod=$mod&id=$r[id_newsevent]&gambar=$r[gmb_newsevent]&ale=1&url=$_GET[url]' onClick=\"return confirm('apakah anda yakin akan menghapus data ini ?')\"><i class='fa fa-trash-o'></i> Hapus</a>
										</td>
									</tr>";
                  $no++;
								}
								?>
							</tbody>
						</table>
					</div>
                </div><!-- panel -->
            </div><!-- contentpanel -->
        </div><!-- mainpanel -->
    </div><!-- mainwrapper -->
</section>

<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/pace.min.js"></script>
<script src="js/retina.min.js"></script>
<script src="js/jquery.cookies.js"></script>

<script src="js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/plug-ins/725b2a2115b/integration/bootstrap/3/dataTables.bootstrap.js"></script>
<script src="//cdn.datatables.net/responsive/1.0.1/js/dataTables.responsive.js"></script>
<script src="js/custom.js"></script>
<script>
jQuery(document).ready(function(){
	jQuery('#aledata').dataTable({
		"sPaginationType": "full_numbers",
		"iDisplayLength": 25,
		"aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, "Semua"]]
    });
});
</script>
