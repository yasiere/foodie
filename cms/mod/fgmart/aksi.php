<?php
session_start();
if(!empty($_SESSION['ak_id'])){
	include "../../inc/db.php";
	include "../../inc/upload.php";
	include "../../inc/id_masking.php";
	include '../../inc/seo.php';
	include "../../../config/func/upload_gambar_bms.php";

	$mod			= $_GET['mod'];
	$tglhariini		= date("Y-m-d");
	$ukuran_maks	= 552428800;
	$error_cover 	= 0;
	$error_profil 	= 0;

	if($_GET['ale']==1){
		mysqli_query($koneksi,"DELETE FROM fgmart WHERE id_fgmart='$_GET[id]'");
		unlink("../../../assets/img/fgmart/small_".$_GET['gambarthumb']);
		unlink("../../../assets/img/fgmart/medium_".$_GET['gambarthumb']);
		unlink("../../../assets/img/fgmart/big_".$_GET['gambarthumb']);
		header("Location: ../../mod-fgmart-15.htm");
	}
	elseif($_GET['ale']==2){
		date_default_timezone_set("Asia/Jakarta");
		$sekarang=date("Y-m-d H:i:s");

		$ukuran_maks	= 552428800;

		$nama_cover			= $_FILES['gambar_photo']['name'];
		$ukuran_cover 		= $_FILES['gambar_photo']['size'];
		$lokasi_cover		= $_FILES['gambar_photo']['tmp_name'];

		$nama_covera			= $_FILES['file_link']['name'];
		$ukuran_covera 		= $_FILES['file_link']['size'];
		$lokasi_covera		= $_FILES['file_link']['tmp_name'];

		list($width_cover, $height_cover) = getimagesize($lokasi_cover);
		if($ukuran_cover > $ukuran_maks or !preg_match("/.(gif|jpg|png)$/i", $nama_cover) or $width_cover < 800 or $height_cover < 600){
			$_SESSION['notif']     = "gambar";
			header("Location: ../../add-fgmart-15.htm");
			exit();
		}
		else{
			$post = $_POST['inputs'];
			$posta = $_POST['inputsa'];
			$post['tgl_post'] = $sekarang;
			$sql=mysqli_query($koneksi,"INSERT INTO fgmart(". implode(',', array_keys($post)) .") VALUES('". implode("','", array_values($post)) ."')");

			if($sql){
				$post = $_POST['inputs'];
				$cover=upload_gambar($nama_cover,$lokasi_cover,"../../../assets/img/fgmart");
				$id_fgmart=mysqli_insert_id($koneksi);
				$pid = id_masking($id_fgmart);
				mysqli_query($koneksi,"INSERT INTO fgmart_item (`id_fgmart`, `jenis`, `dimensi`, `resolusi`, `ukuran_file`, `harga`, `file_link`)
					VALUES ( '{$id_fgmart}', '$_POST[jenis]', '$_POST[dimensi]', '$_POST[resolusi]', '$_POST[ukuran_file]', '$_POST[harga]', '$nama_covera')" );
				move_uploaded_file($lokasi_covera,"../../../stock/$nama_covera");

				mysqli_query($koneksi,"UPDATE fgmart SET gambar_fgmart='{$cover}' WHERE id_fgmart='$id_fgmart'");
				mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('fgmart','{$id_fgmart}')");
				// if($_POST['feed_submit']==1){
				//	mysqli_query($koneksi,"INSERT INTO activity (id_member,kat_act,deskripsi,tgl) VALUES('$post[id_member]','feed_submit','Submit new fgmart review','$sekarang')");
				// }
				//$id = id_masking($id_fgmart);
				header("Location: ../../mod-fgmart-15.htm");
				exit();
			}
		}
	}



	elseif($_GET['ale']==3){

		date_default_timezone_set("Asia/Jakarta");
		$sekarang=date("Y-m-d H:i:s");

		$update = array();
		foreach($_POST['inputs'] as $name => $val)
		{
			$upadate[] = $name ."='". $val ."'";
		}

		$update = implode(',', $upadate);

		$ukuran_maks	= 552428800;
		if(!empty($_FILES['gambar_photo']['tmp_name']) || !empty($_FILES['file_link']['tmp_name'])){
			$nama_cover			= $_FILES['gambar_photo']['name'];
			$ukuran_cover 		= $_FILES['gambar_photo']['size'];
			$lokasi_cover		= $_FILES['gambar_photo']['tmp_name'];
			list($width_cover, $height_cover) = getimagesize($lokasi_cover);
			if(!empty($_FILES['gambar_photo']['tmp_name'])){
				if($ukuran_cover > $ukuran_maks or !preg_match("/.(gif|jpg|png)$/i", $nama_cover) or $width_cover < 800 or $height_cover < 600){
					$_SESSION['notif']     = "gambar";
					header("Location: ../../mod-fgmart-15.htm");
					exit();
				}
				else{
					unlink("../../../assets/img/fgmart/small_".$_POST['gambar_old']);
					unlink("../../../assets/img/fgmart/medium_".$_POST['gambar_old']);
					unlink("../../../assets/img/fgmart/big_".$_POST['gambar_old']);
					$cover=upload_gambar($nama_cover,$lokasi_cover,"../../../assets/img/fgmart");
					mysqli_query($koneksi,"UPDATE fgmart SET {$update}, gambar_fgmart='{$cover}' WHERE id_fgmart='$_POST[id]'");
					mysqli_query($koneksi,"UPDATE fgmart_item SET `jenis`='$_POST[jenis]', `dimensi`= '$_POST[dimensi]', `resolusi` = '$_POST[resolusi]',
						`ukuran_file` = '$_POST[ukuran_file]', `harga` = '$_POST[harga]' WHERE id_fgmart='$_POST[id]' ");
				}
			}
			if(!empty($_FILES['file_link']['tmp_name'])){
				$nama_covera			= $_FILES['file_link']['name'];
				$ukuran_covera 		= $_FILES['file_link']['size'];
				$lokasi_covera		= $_FILES['file_link']['tmp_name'];
				mysqli_query($koneksi,"UPDATE fgmart_item SET `file_link`= '$nama_covera' WHERE id_fgmart='$_POST[id]'" );
				move_uploaded_file($lokasi_covera,"../../../stock/$nama_covera");
				header("Location: ../../mod-fgmart-15.htm");
			}
			$_SESSION['notif']     = "sukses";
			header("Location: ../../mod-fgmart-15.htm");
			exit();
		}

		else{
			mysqli_query($koneksi,"UPDATE fgmart SET {$update} WHERE id_fgmart='{$_POST['id']}'");
			mysqli_query($koneksi,"UPDATE fgmart_item SET `jenis`='$_POST[jenis]', `dimensi`= '$_POST[dimensi]', `resolusi` = '$_POST[resolusi]',
				`ukuran_file` = '$_POST[ukuran_file]', `harga` = '$_POST[harga]' WHERE id_fgmart='$_POST[id]' ");
			$_SESSION['notif'] = "sukses";
			header("Location: ../../mod-fgmart-15.htm");
			exit();
		}



	}
}
else{
	header('location:not-found.htm');
}
?>
