<section>
    <div class="mainwrapper">
        <?php
			include "inc/kiri.php";
			$pageTitle = 'FGMart';
			?>
        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="pageicon pull-left">
                        <i class="fa fa-picture-o"></i>
                    </div>
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href="mod-beranda.htm"><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="mod-beranda.htm">Beranda</a></li>
                            <li>Informasi <?php echo $pageTitle;?></li>
                        </ul>
                        <h4>Informasi <?php echo $pageTitle;?></h4>
                    </div>
                </div>
                <!-- media -->
            </div>
            <!-- pageheader -->
            <div class="contentpanel">
                <?php if($_GET['proses']==1){ ?>
					      <?php if(isset($_GET['info'])) include "inc/form-alert.php"; ?>
                <form action="mod/<?php echo"$mod/aksi.php?mod=$mod&ale=2&url=$_GET[url]"; ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" id="signup-daftar">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-btns">
                          <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                        </div>
                        <!-- panel-btns -->
                        <h4 class="panel-title">Data <?php echo $pageTitle;?></h4>
                        <p>Silahkan lengkapi data dibawah ini.</p>
                    </div>
                    <div class="panel-body nopadding">
                      <div class="form-group">
                          <label class="col-sm-2 control-label">Category :</label>
                          <div class="col-sm-10">
                              <select name="inputs[id_photo_category]" class="pilih" style="width: 100%;">
                              <?php
                                  $sql=mysqli_query($koneksi,"SELECT id_photo_category, nama_photo_category FROM photo_category ORDER BY nama_photo_category ASC");
                                  while($b=mysqli_fetch_array($sql)){
                                  	echo"<option value='$b[id_photo_category]'>$b[nama_photo_category]</option>";
                                  }
                                  ?>
                              </select>
                          </div>
                      </div>
        							<div class="form-group">
        								<label class="col-sm-2 control-label">FGMart Title :</label>
        								<div class="col-sm-10">
        									<input type="text" name="inputs[nama_fgmart]" class="form-control" required>
        								</div>
        							</div>


                      <div class="form-group">
        								<label class="col-sm-2 control-label">FGMart Type :</label>
        								<div class="col-sm-10">
                          <select class="form-control" name="jenis">
                            <option value="large">Large</option>
                            <option value="medium">Medium</option>
                            <option value="small">Small</option>
                          </select>
        								</div>
        							</div>
                      <div class="form-group">
        								<label class="col-sm-2 control-label">FGMart Size (Px) :</label>
        								<div class="col-sm-10">
        									<input type="text" name="dimensi" class="form-control" required>
        								</div>
        							</div>
                      <div class="form-group">
        								<label class="col-sm-2 control-label">FGMart Resolution (Px) :</label>
        								<div class="col-sm-10">
        									<input type="text" name="dimensi" class="form-control" required>
        								</div>
        							</div>
                      <div class="form-group">
        								<label class="col-sm-2 control-label">FGMart File Size :</label>
        								<div class="col-sm-10">
        									<input type="text" name="ukuran_file" class="form-control" required>
        								</div>
        							</div>
                      <div class="form-group">
        								<label class="col-sm-2 control-label">FGMart Price :</label>
        								<div class="col-sm-10">
        									<input type="text" name="harga" class="form-control" required>
        								</div>
        							</div>
                      <div class="form-group">
        								<label class="col-sm-2 control-label">File Download <span class="f-merah">*</span></label>
        								<div class="col-sm-10">
        										<input type="file" name="file_link" required>
        									<p class="help-block">File used have to be in RAR and ZIB format and the maximum total images size is 50MB</p>
        								</div>
        							</div>

        							<div class="form-group">
        								<label class="col-sm-2 control-label">Photo <span class="f-merah">*</span></label>
        								<div class="col-sm-10">
        									<div class="fileinput fileinput-new" data-provides="fileinput">
        										<div class="fileinput-preview upload" data-trigger="fileinput"></div>
        										<input type="file" name="gambar_photo" class="hidden" required>
        									</div>
        									<p class="help-block">Image used have to be in JPG, PNG, and GIF format and the maximum total images size is 50MB also minimal dimension must be 800x600 pixel.</p>
        								</div>
        							</div>
                    </div>
                      <!-- panel-body -->
                  </div>
                  <!-- panel -->
                  <div class="panel panel-default">
                      <div class="panel-footer">
                          <button class="btn btn-primary mr5">Simpan</button>
                          <a href="mod-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-warning">Kembali</a>
                      </div>
                      <!-- panel-footer -->
                  </div>
                  <!-- panel -->
                </form>
                <?php

                    }else{
                          $edit=mysqli_query($koneksi,"select * from fgmart where id_fgmart = '$_GET[id]'");
                          $e=mysqli_fetch_array($edit);

                          $edita=mysqli_query($koneksi,"select * from fgmart_item where id_fgmart = '$_GET[id]'");
                          $ea=mysqli_fetch_array($edita);
                    ?>
                <form action="mod/<?php echo"$mod/aksi.php?mod=$mod&ale=3&url=$_GET[url]"; ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" id="signup-daftar">
                    <input type="hidden" name="id" value="<?php echo $e['id_fgmart']; ?>">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns">
                                <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                            </div>
                            <!-- panel-btns -->
                            <h4 class="panel-title">Data  <?php echo $pageTitle;?></h4>
                            <p>Silahkan lengkapi data dibawah ini.</p>
                        </div>
                        <div class="panel-body nopadding">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Category :</label>
                                <div class="col-sm-10">
                                    <select name="inputs[id_photo_category]" class="pilih" style="width: 100%;">
                                    <?php
                                        $sql=mysqli_query($koneksi,"SELECT id_photo_category, nama_photo_category FROM photo_category ORDER BY nama_photo_category ASC");
                                        while($b=mysqli_fetch_array($sql)){
											$selected = ($b['id_photo_category'] == $e['id_photo_category'])? "selected": "";
                                        	echo"<option value='$b[id_photo_category]' {$selected}>$b[nama_photo_category]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
              							<div class="form-group">
              								<label class="col-sm-2 control-label">FGMart Title :</label>
              								<div class="col-sm-10">
              									<input type="text" name="inputs[nama_fgmart]" class="form-control" value="<?php echo $e['nama_fgmart']?>" required>
              								</div>
              							</div>
                            <div class="form-group">
              								<label class="col-sm-2 control-label">FGMart Size (Px) :</label>
              								<div class="col-sm-10">
              									<input type="text" name="dimensi" class="form-control" value="<?php echo $ea['dimensi']?>" >
              								</div>
              							</div>
                            <div class="form-group">
              								<label class="col-sm-2 control-label">FGMart Resolution (Px) :</label>
              								<div class="col-sm-10">
              									<input type="text" name="resolusi" class="form-control" value="<?php echo $ea['resolusi']?>">
              								</div>
              							</div>
                            <div class="form-group">
              								<label class="col-sm-2 control-label">FGMart File Size :</label>
              								<div class="col-sm-10">
              									<input type="text" name="ukuran_file" class="form-control" value="<?php echo $ea['ukuran_file']?>">
              								</div>
              							</div>
                            <div class="form-group">
              								<label class="col-sm-2 control-label">FGMart Price :</label>
              								<div class="col-sm-10">
              									<input type="text" name="harga" class="form-control" value="<?php echo $ea['harga']?>">
              								</div>
              							</div>
                            <div class="form-group">
              								<label class="col-sm-2 control-label">File Download <span class="f-merah">*</span></label>
              								<div class="col-sm-10">
              										<input type="file" name="file_link">
              									<p class="help-block">File used have to be in RAR and ZIB format and the maximum total images size is 50MB</p>
              								</div>
              							</div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Cover Photo <span class="f-merah">*</span></label>
                                <div class="col-sm-10">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview upload" data-trigger="fileinput">
                                            <img src="<?php echo"$base/assets/img/fgmart/small_$e[gambar_fgmart]"; ?>">
                                        </div>
                                        <input type="file" name="gambar_photo" class="hidden">
                                        <input type="hidden" name="gambar_old" value="<?php echo $e['gambar_fgmart'];?>">
                                    </div>
                                    <p class="help-block">Image used have to be in JPG, PNG, and GIF format and the maximum total images size is 50MB also minimal dimension must be 800x600 pixel.</p>
                                </div>
                            </div>
                        </div>
                        <!-- panel-body -->
                    </div>
                    <!-- panel -->
                    <div class="panel panel-default">
                        <div class="panel-footer">
                            <button class="btn btn-primary mr5">Simpan</button>
                            <a href="mod-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-warning">Kembali</a>
                        </div>
                        <!-- panel-footer -->
                    </div>
                    <!-- panel -->
                </form>
                <?php
                    } ?>
            </div>
            <!-- contentpanel -->
        </div>
        <!-- mainpanel -->
    </div>
    <!-- mainwrapper -->
</section>
<style media="screen">
    #country-list {
    margin: 0;
    list-style: none;
    padding: 0px;
    max-height: 155px;
    position: relative;
    overflow: auto;
    }
    .ko {
    border: 1px solid #CCC;
    padding: 5px;
    color: #9C9999;
    border-top: none;
    cursor: pointer;
    transition: 0.7s all 0s ease;
    }
    .ko:hover{
    background: #ffeed0;
    transition: 0.3s all 0s ease;
    }
</style>
<script src="js/jquery-1.11.1.min.js"></script>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$(".pilih").select2();

		tinymce.init({ selector:'textarea' });
	});
</script>
