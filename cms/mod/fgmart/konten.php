<section>
    <div class="mainwrapper">
        <?php 
			include "inc/kiri.php"; 
			$pageTitle = 'FGMart';
			?>
        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="pageicon pull-left">
                        <i class="fa fa-tags"></i>
                    </div>
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href="mod-beranda-1.htm"><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="mod-beranda-1.htm">Beranda</a></li>
                            <li>Informasi <?php echo $pageTitle;?></li>
                        </ul>
                        <h4>Informasi <?php echo $pageTitle;?></h4>
                    </div>
                </div>
                <!-- media -->
            </div>
            <!-- pageheader -->
            <div class="contentpanel">
                <?php if(isset($_GET['info']) && $_GET['info']==1): ?>
					<div class="alert alert-success">
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button
						<strong>Sukses!</strong> data berhasil dihapus.
					</div>
				<?php endif; ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-sm-11">
                                <p><a href="add-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-primary">New Submit</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table id="aledata" class="table table-striped table-bordered responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Image </th>
                                    <th>Title </th>
                                    <th>Category </th>
                                    <th>Post Date </th>
                                    <th>Viewed </th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $no = 0;
									$fgmart=mysqli_query($koneksi,"SELECT * FROM fgmart F LEFT JOIN photo_category P ON F.id_photo_category=P.id_photo_category ORDER BY F.id_fgmart DESC");
                                    while( $r = mysqli_fetch_array($fgmart) ){
										$no++;
                                    	echo"<tr>
                                    		<td width='30px'>{$no}.</td>
											<td><img src='{$base}/assets/img/fgmart/small_{$r['gambar_fgmart']}' width='100px' class='img-thumbnail'></td>
                                    		<td>{$r['nama_fgmart']}</td>
                                    		<td>{$r['nama_photo_category']}</td>
                                    		<td>{$r['tgl_post']}</td>
                                    		<td>{$r['dilihat']}</td>
                                    		<td width='170px'>
												<a href='edit-{$mod}-{$r['id_fgmart']}-{$_GET['url']}.htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>
												<a class='btn btn-danger btn-rounded btn-sm' href='mod/{$mod}/aksi.php?mod={$mod}&id={$r['id_fgmart']}&gambarthumb={$r['gambar_fgmart']}&ale=1&url={$_GET['url']}' onClick=\"return confirm('apakah anda yakin akan menghapus data ini ?')\"><i class='fa fa-trash-o'></i> Hapus</a>
                                    		</td>
                                    	</tr>";
                                    }
                                    ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- panel -->
            </div>
            <!-- contentpanel -->
        </div>
        <!-- mainpanel -->
    </div>
    <!-- mainwrapper -->
</section>

