<section>
    <div class="mainwrapper">
        <?php
			include "inc/kiri.php";
			$pageTitle = 'Advertisement';
			?>
        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="pageicon pull-left">
                        <i class="fa fa-tags"></i>
                    </div>
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href="mod-beranda-1.htm"><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="mod-beranda-1.htm">Beranda</a></li>
                            <li>Informasi <?php echo $pageTitle;?></li>
                        </ul>
                        <h4>Informasi <?php echo $pageTitle;?></h4>
                    </div>
                </div>
                <!-- media -->
            </div>
            <!-- pageheader -->
            <div class="contentpanel">
                <?php if(isset($_GET['info']) && $_GET['info']==1): ?>
					<div class="alert alert-success">
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button
						<strong>Sukses!</strong> data berhasil dihapus.
					</div>
				<?php endif; ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-sm-11">
                                <p><a href="add-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-primary">New Submit</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table id="aledata" class="table table-striped table-bordered responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Page</th>
                                    <th>Image Desktop</th>
                                    <th>Image Mobile</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $no = 0;
									                  $sql=mysqli_query($koneksi,"SELECT * FROM iklan ORDER BY id_iklan DESC");
                                    while( $r = mysqli_fetch_array($sql) ){
										                  $no++;
                                    	echo"<tr>
                                    		<td width='30px'>{$no}.</td>
                                        <td> {$r['type']} </td>
											                  <td><img src='{$base}/assets/img/banner/{$r['gambar']}' width='100px' class='img-thumbnail'></td>
                                    		<td><img src='{$base}/assets/img/banner/{$r['mobile']}' width='100px' class='img-thumbnail'></td>
                                    		<td width='170px'>
                  												<a href='edit-{$mod}-{$r['id_iklan']}-{$_GET['url']}.htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>
                  												<a class='btn btn-danger btn-rounded btn-sm' href='mod/{$mod}/aksi.php?mod={$mod}&id={$r['id_iklan']}&gambar={$r['gambar']}&mobile={$r['mobile']}&ale=1&url={$_GET['url']}' onClick=\"return confirm('apakah anda yakin akan menghapus data ini ?')\"><i class='fa fa-trash-o'></i> Hapus</a>
                                    		</td>
                                    	</tr>";
                                    }
                                    ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- panel -->
            </div>
            <!-- contentpanel -->
        </div>
        <!-- mainpanel -->
    </div>
    <!-- mainwrapper -->
</section>
