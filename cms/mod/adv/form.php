<style>
    html, body {
    height: 100%;
    margin: 0;
    padding: 0;
    }
    #map {
    width: 100%;
    height: 400px;
    }
    .controls {
    margin-top: 10px;
    border: 1px solid transparent;
    border-radius: 2px 0 0 2px;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    height: 32px;
    outline: none;
    box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    }
    #searchInput {
    background-color: #fff;
    font-family: Roboto;
    font-size: 15px;
    font-weight: 300;
    margin-left: 12px;
    padding: 0 11px 0 13px;
    text-overflow: ellipsis;
    width: 50%;
    }
    #searchInput:focus {
    border-color: #4d90fe;
    }
</style>
<section>
    <div class="mainwrapper">
        <?php
			include "inc/kiri.php";
			$pageTitle = 'Advertisement';
			?>
        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="pageicon pull-left">
                        <i class="fa fa-picture-o"></i>
                    </div>
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href="mod-beranda.htm"><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="mod-beranda.htm">Beranda</a></li>
                            <li>Informasi <?php echo $pageTitle;?></li>
                        </ul>
                        <h4>Informasi <?php echo $pageTitle;?></h4>
                    </div>
                </div>
                <!-- media -->
            </div>
            <!-- pageheader -->
            <div class="contentpanel">
                <?php if($_GET['proses']==1){ ?>
					<?php if(isset($_GET['info']))
          include "inc/form-alert.php"; ?>
                <form action="mod/<?php echo"$mod/aksi.php?mod=$mod&ale=2&url=$_GET[url]"; ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" id="signup-daftar">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns">
                                <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                            </div>
                            <!-- panel-btns -->
                            <h4 class="panel-title">Data <?php echo $pageTitle;?></h4>
                            <p>Silahkan lengkapi data dibawah ini.</p>
                        </div>
                        <div class="panel-body nopadding">
                          <div class="form-group">
            								<label class="col-sm-2 control-label">Advertisement Page :</label>
            								<div class="col-sm-10">
            									<select class="form-control" name="type">
                                <option value="home">Home</option>
                                <option value="restaurant">Restaurant</option>
                                <option value="restaurant_info">Restaurant Info</option>
                                <option value="restaurant_menu">Restaurant Menu</option>
                                <option value="restaurant_food">Restaurant Food</option>
                                <option value="restaurant_beverage">Restaurant Beverage</option>
                                <option value="restaurant_photo">Restaurant Photo</option>
                                <option value="restaurant_voter">Restaurant Voter</option>
                                <option value="restaurant_map">Restaurant Map</option>
                                <option value="food">Food</option>
                                <option value="food_detail">Food Detail</option>
                                <option value="food_photo">Food Photo</option>
                                <option value="food_voter">Food Voter</option>
                                <option value="food_map">Food Map</option>
                                <option value="beverage">Beverage</option>
                                <option value="beverage_info">Beverage Detail</option>
                                <option value="beverage_photo">Beverage Photo</option>
                                <option value="beverage_voter">Beverage Voter</option>
                                <option value="beverage_map">Beverage Map</option>
                                <option value="recipe">Recipe</option>
                                <option value="recipe_detail">Recipe Detail</option>
                                <option value="fgmart">Fgmart</option>
                                <option value="fgmart_detail">Fgmart Detail</option>
                                <option value="coupon">Coupon</option>
                                <option value="coupon_detail">Coupon Detail</option>
                                <option value="article">Article</option>
                                <option value="article_detail">Article Detail</option>
                                <option value="video">Video</option>
                                <option value="video_detail">Video Detail</option>
                                <option value="all">Advance Search</option>
            									</select>
            								</div>
            							</div>
            							<div class="form-group">
            								<label class="col-sm-2 control-label">Photo Desktop<span class="f-merah">*</span></label>
            								<div class="col-sm-10">
            									<div class="fileinput fileinput-new" data-provides="fileinput">
            										<div class="fileinput-preview upload" data-trigger="fileinput"></div>
            										<input type="file" name="gambar" class="hidden" required>
            									</div>
            									<p class="help-block">Image used have to be in JPG, PNG, and GIF format and the maximum total images size is 50MB also minimal dimension must be 800x600 pixel.</p>
            								</div>
            							</div>
                          <div class="form-group">
            								<label class="col-sm-2 control-label">Photo Mobile<span class="f-merah">*</span></label>
            								<div class="col-sm-10">
            									<div class="fileinput fileinput-new" data-provides="fileinput">
            										<div class="fileinput-preview upload" data-trigger="fileinput"></div>
            										<input type="file" name="mobile" class="hidden" required>
            									</div>
            									<p class="help-block">Image used have to be in JPG, PNG, and GIF format and the maximum total images size is 50MB also minimal dimension must be 800x600 pixel.</p>
            								</div>
            							</div>
                        </div>
                        <!-- panel-body -->
                    </div>
                    <!-- panel -->
                    <div class="panel panel-default">
                        <div class="panel-footer">
                            <button class="btn btn-primary mr5">Simpan</button>
                            <a href="mod-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-warning">Kembali</a>
                        </div>
                        <!-- panel-footer -->
                    </div>
                    <!-- panel -->
                </form>
                <?php

                }else{
                      $edit=mysqli_query($koneksi,"select * from iklan where id_iklan = '$_GET[id]'");
                      $e=mysqli_fetch_array($edit);
                ?>
                <form action="mod/<?php echo"$mod/aksi.php?mod=$mod&ale=3&url=$_GET[url]"; ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" id="signup-daftar">
                    <input type="hidden" name="id" value="<?php echo $e['id_iklan']; ?>">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns">
                                <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                            </div>
                            <!-- panel-btns -->
                            <h4 class="panel-title">Data  <?php echo $pageTitle;?></h4>
                            <p>Silahkan lengkapi data dibawah ini.</p>
                        </div>
                        <div class="panel-body nopadding">
            							<div class="form-group">
            								<label class="col-sm-2 control-label">Advertisement Page :</label>
            								<div class="col-sm-10">
                              <select class="form-control" name="type">
                                <option value="home" <?php if ($e['type']=="home"){ echo "selected";} ?>>Home</option>
                                <option value="restaurant" <?php if ($e['type']=="restaurant"){ echo "selected";} ?>>Restaurant</option>
                                <option value="restaurant_info" <?php if ($e['type']=="restaurant_info"){ echo "selected";} ?>>Restaurant Info</option>
                                <option value="restaurant_menu" <?php if ($e['type']=="restaurant_menu"){ echo "selected";} ?>>Restaurant Menu</option>
                                <option value="restaurant_food" <?php if ($e['type']=="restaurant_food"){ echo "selected";} ?>>Restaurant Food</option>
                                <option value="restaurant_beverage" <?php if ($e['type']=="restaurant_beverage"){ echo "selected";} ?>>Restaurant Beverage</option>
                                <option value="restaurant_photo" <?php if ($e['type']=="restaurant_photo"){ echo "selected";} ?>>Restaurant Photo</option>
                                <option value="restaurant_voter" <?php if ($e['type']=="restaurant_voter"){ echo "selected";} ?>>Restaurant Voter</option>
                                <option value="restaurant_map" <?php if ($e['type']=="restaurant_map"){ echo "selected";} ?>>Restaurant Map</option>
                                <option value="food" <?php if ($e['type']=="food"){ echo "selected";} ?>>Food</option>
                                <option value="food_detail" <?php if ($e['type']=="food_detail"){ echo "selected";} ?>>Food Detail</option>
                                <option value="food_photo" <?php if ($e['type']=="food_photo"){ echo "selected";} ?>>Food Photo</option>
                                <option value="food_voter" <?php if ($e['type']=="food_voter"){ echo "selected";} ?>>Food Voter</option>
                                <option value="food_map" <?php if ($e['type']=="food_map"){ echo "selected";} ?>>Food Map</option>
                                <option value="beverage" <?php if ($e['type']=="beverage"){ echo "selected";} ?>>Beverage</option>
                                <option value="beverage_info" <?php if ($e['type']=="beverage_info"){ echo "selected";} ?>>Beverage Detail</option>
                                <option value="beverage_photo" <?php if ($e['type']=="beverage_photo"){ echo "selected";} ?>>Beverage Photo</option>
                                <option value="beverage_voter" <?php if ($e['type']=="beverage_voter"){ echo "selected";} ?>>Beverage Voter</option>
                                <option value="beverage_map" <?php if ($e['type']=="beverage_map"){ echo "selected";} ?>>Beverage Map</option>
                                <option value="recipe" <?php if ($e['type']=="recipe"){ echo "selected";} ?>>Recipe</option>
                                <option value="recipe_detail" <?php if ($e['type']=="recipe_detail"){ echo "selected";} ?>>Recipe Detail</option>
                                <option value="fgmart" <?php if ($e['type']=="fgmart"){ echo "selected";} ?>>Fgmart</option>
                                <option value="fgmart_detail" <?php if ($e['type']=="fgmart_detail"){ echo "selected";} ?>>Fgmart Detail</option>
                                <option value="coupon" <?php if ($e['type']=="coupon"){ echo "selected";} ?>>Coupon</option>
                                <option value="coupon_detail" <?php if ($e['type']=="coupon_detail"){ echo "selected";} ?>>Coupon Detail</option>
                                <option value="article" <?php if ($e['type']=="article"){ echo "selected";} ?>>Article</option>
                                <option value="article_detail" <?php if ($e['type']=="article_detail"){ echo "selected";} ?>>Article Detail</option>
                                <option value="video" <?php if ($e['type']=="video"){ echo "selected";} ?>>Video</option>
                                <option value="video_detail" <?php if ($e['type']=="video_detail"){ echo "selected";} ?>>Video Detail</option>
                                <option value="all" <?php if ($e['type']=="all"){ echo "selected";} ?>>Advance Search</option>
            									</select>
            								</div>
            							</div>
                          <div class="form-group">
                              <label class="col-sm-2 control-label">Cover Photo <span class="f-merah">*</span></label>
                              <div class="col-sm-10">
                                  <div class="fileinput fileinput-new" data-provides="fileinput">
                                      <div class="fileinput-preview upload" data-trigger="fileinput">
                                          <img src="<?php echo"$base/assets/img/banner/$e[gambar]"; ?>">
                                      </div>
                                      <input type="file" name="gambar" class="hidden">
                                      <input type="hidden" name="gambar_old" value="<?php echo $e['gambar'];?>">
                                  </div>
                                  <p class="help-block">Image used have to be in JPG, PNG, and GIF format and the maximum total images size is 50MB also minimal dimension must be 800x600 pixel.</p>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 control-label">Mobile <span class="f-merah">*</span></label>
                              <div class="col-sm-10">
                                  <div class="fileinput fileinput-new" data-provides="fileinput">
                                      <div class="fileinput-preview upload" data-trigger="fileinput">
                                          <img src="<?php echo"$base/assets/img/banner/$e[mobile]"; ?>">
                                      </div>
                                      <input type="file" name="mobile" class="hidden">
                                      <input type="hidden" name="mobile_old" value="<?php echo $e['mobile'];?>">
                                  </div>
                                  <p class="help-block">Image used have to be in JPG, PNG, and GIF format and the maximum total images size is 50MB also minimal dimension must be 800x600 pixel.</p>
                              </div>
                          </div>
                        </div>
                        <!-- panel-body -->
                    </div>
                    <!-- panel -->
                    <div class="panel panel-default">
                        <div class="panel-footer">
                            <button class="btn btn-primary mr5">Simpan</button>
                            <a href="mod-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-warning">Kembali</a>
                        </div>
                        <!-- panel-footer -->
                    </div>
                    <!-- panel -->
                </form>
                <?php
                    } ?>
            </div>
            <!-- contentpanel -->
        </div>
        <!-- mainpanel -->
    </div>
    <!-- mainwrapper -->
</section>
<style media="screen">
    #country-list {
    margin: 0;
    list-style: none;
    padding: 0px;
    max-height: 155px;
    position: relative;
    overflow: auto;
    }
    .ko {
    border: 1px solid #CCC;
    padding: 5px;
    color: #9C9999;
    border-top: none;
    cursor: pointer;
    transition: 0.7s all 0s ease;
    }
    .ko:hover{
    background: #ffeed0;
    transition: 0.3s all 0s ease;
    }
</style>
<script src="js/jquery-1.11.1.min.js"></script>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$(".pilih").select2();

		tinymce.init({ selector:'textarea' });
	});
</script>
