<section>
    <div class="mainwrapper">
      <?php include "inc/kiri.php"; ?>
      <div class="mainpanel">
        <div class="pageheader">
            <div class="media">
                <div class="pageicon pull-left">
                    <i class="fa fa-dashboard"></i>
                </div>
                <div class="media-body">
                    <ul class="breadcrumb">
                        <li><a href="mod-beranda.htm"><i class="glyphicon glyphicon-dashboard"></i></a></li>
                        <li>Beranda</li>
                    </ul>
                    <h4>Beranda</h4>
                </div>
            </div><!-- media -->
        </div><!-- pageheader -->
        <div class="contentpanel">
          <div class="row row-stat">
            <div class="col-md-4">
              <div class="panel panel-success-alt noborder">
                <div class="panel-heading noborder">
                  <div class="panel-btns" style="display: block; opacity: 0;">
                    <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                  </div><!-- panel-btns -->
                  <div class="panel-icon"><img src="<?php echo"{$base}/adminfg/images/rest.png"; ?>"  width="40" height="40" style="margin: 10px;" /></div>
                  <div class="media-body">
                    <h4 class="nomargin">Restaurant</h4>
                    <h1 class="mt5">
                      <?php $sql=mysqli_num_rows(mysqli_query($koneksi,"select * from restaurant")); echo "$sql";?>
                    </h1>
                  </div><!-- media-body -->
                  <hr>
                  <div class="clearfix mt20">
                      <div class="pull-left">
                        <h5 class="md-title nomargin">This Mounth</h5>
                        <h4 class="nomargin"><?php $sqla=mysqli_fetch_array(mysqli_query($koneksi,"SELECT count(*) as jumlah FROM restaurant WHERE tgl_post > DATE_SUB(NOW(), INTERVAL 1 MONTH)")); echo "$sqla[jumlah]";?></h4>
                      </div>
                      <div class="pull-right">
                        <h5 class="md-title nomargin">This Week</h5>
                        <h4 class="nomargin"><?php $sqla=mysqli_fetch_array(mysqli_query($koneksi,"SELECT count(*) as jumlah FROM restaurant WHERE tgl_post > DATE_SUB(NOW(), INTERVAL 1 WEEK)")); echo "$sqla[jumlah]";?></h4>
                      </div>
                  </div>
                  <div class="clearfix mt20">
                      <div class="pull-right">
                        <a class='btn btn-default' href="detail-restaurant-3.htm">Detail Info Restaurant</a>
                      </div>
                  </div>
                </div><!-- panel-body -->
              </div><!-- panel -->
            </div>

            <div class="col-md-4">
              <div class="panel panel-warning-alt noborder">
                <div class="panel-heading noborder">
                  <div class="panel-btns" style="display: block; opacity: 0;">
                    <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                  </div><!-- panel-btns -->
                  <div class="panel-icon"><img src="<?php echo"{$base}/adminfg/images/ayam.png"; ?>" width="40" height="40" style="margin: 10px;"/></div>
                  <div class="media-body">
                    <h4 class="nomargin">Food</h4>
                    <h1 class="mt5">
                      <?php $sql=mysqli_num_rows(mysqli_query($koneksi,"select * from food")); echo "$sql";?>
                    </h1>
                  </div><!-- media-body -->
                  <hr>
                  <div class="clearfix mt20">
                      <div class="pull-left">
                        <h5 class="md-title nomargin">This Mounth</h5>
                        <h4 class="nomargin"><?php $sqla=mysqli_fetch_array(mysqli_query($koneksi,"SELECT count(*) as jumlah FROM food WHERE tgl_posta > DATE_SUB(NOW(), INTERVAL 1 MONTH)")); echo "$sqla[jumlah]";?></h4>
                      </div>
                      <div class="pull-right">
                        <h5 class="md-title nomargin">This Week</h5>
                        <h4 class="nomargin"><?php $sqla=mysqli_fetch_array(mysqli_query($koneksi,"SELECT count(*) as jumlah FROM food WHERE tgl_posta > DATE_SUB(NOW(), INTERVAL 1 WEEK)")); echo "$sqla[jumlah]";?></h4>
                      </div>
                  </div>
                  <div class="clearfix mt20">
                    <div class="pull-right">
                      <a class='btn btn-default' href="detail-food-6.htm">Detail Info Food</a>
                    </div>
                  </div>
                </div><!-- panel-body -->
              </div><!-- panel -->
            </div>

            <div class="col-md-4">
              <div class="panel panel-primary noborder">
                <div class="panel-heading noborder">
                  <div class="panel-btns" style="display: block; opacity: 0;">
                    <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                  </div><!-- panel-btns -->
                  <div class="panel-icon"><img src="<?php echo"{$base}/adminfg/images/beverage.png"; ?>" width="40" height="40" style="margin: 10px;"/></div>
                  <div class="media-body">
                    <h4 class="nomargin">Beverage</h4>
                    <h1 class="mt5">
                      <?php $sql=mysqli_num_rows(mysqli_query($koneksi,"select * from beverage")); echo "$sql";?>
                    </h1>
                  </div><!-- media-body -->
                  <hr>
                  <div class="clearfix mt20">
                      <div class="pull-left">
                        <h5 class="md-title nomargin">This Mounth</h5>
                        <h4 class="nomargin"><?php $sqla=mysqli_fetch_array(mysqli_query($koneksi,"SELECT count(*) as jumlah FROM beverage WHERE tgl_post > DATE_SUB(NOW(), INTERVAL 1 MONTH)")); echo "$sqla[jumlah]";?></h4>
                      </div>
                      <div class="pull-right">
                        <h5 class="md-title nomargin">This Week</h5>
                        <h4 class="nomargin"><?php $sqla=mysqli_fetch_array(mysqli_query($koneksi,"SELECT count(*) as jumlah FROM beverage WHERE tgl_post > DATE_SUB(NOW(), INTERVAL 1 WEEK)")); echo "$sqla[jumlah]";?></h4>
                      </div>
                  </div>
                  <div class="clearfix mt20">
                    <div class="pull-right">
                      <a class='btn btn-default' href="detail-beverage-8.htm">Detail Info Beverage</a>
                    </div>
                  </div>
                </div><!-- panel-body -->
              </div><!-- panel -->
            </div>

            <div class="col-md-4">
              <div class="panel panel-info-alt noborder">
                <div class="panel-heading noborder">
                  <div class="panel-btns" style="display: block; opacity: 0;">
                    <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                  </div><!-- panel-btns -->
                  <div class="panel-icon"><img src="<?php echo"{$base}/adminfg/images/recipe.png"; ?>" width="40" height="40" style="margin: 10px;"/></div>
                  <div class="media-body">
                    <h4 class="nomargin">Recipe</h4>
                    <h1 class="mt5">
                      <?php $sql=mysqli_num_rows(mysqli_query($koneksi,"select * from recipe")); echo "$sql";?>
                    </h1>
                  </div><!-- media-body -->
                  <hr>
                  <div class="clearfix mt20">
                      <div class="pull-left">
                        <h5 class="md-title nomargin">This Mounth</h5>
                        <h4 class="nomargin"><?php $sqla=mysqli_fetch_array(mysqli_query($koneksi,"SELECT count(*) as jumlah FROM recipe WHERE tgl_post > DATE_SUB(NOW(), INTERVAL 1 MONTH)")); echo "$sqla[jumlah]";?></h4>
                      </div>
                      <div class="pull-right">
                        <h5 class="md-title nomargin">This Week</h5>
                        <h4 class="nomargin"><?php $sqla=mysqli_fetch_array(mysqli_query($koneksi,"SELECT count(*) as jumlah FROM recipe WHERE tgl_post > DATE_SUB(NOW(), INTERVAL 1 WEEK)")); echo "$sqla[jumlah]";?></h4>
                      </div>
                  </div>
                </div><!-- panel-body -->
              </div><!-- panel -->
            </div>

            <div class="col-md-4">
              <div class="panel panel-danger-alt noborder">
                <div class="panel-heading noborder">
                  <div class="panel-btns" style="display: block; opacity: 0;">
                    <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                  </div><!-- panel-btns -->
                  <div class="panel-icon"><img src="<?php echo"{$base}/adminfg/images/cart.png"; ?>" width="40" height="40" style="margin: 10px;" /></div>
                  <div class="media-body">
                    <h4 class="nomargin">FGMART</h4>
                    <h1 class="mt5">
                      <?php $sql=mysqli_num_rows(mysqli_query($koneksi,"select * from fgmart")); echo "$sql";?>
                    </h1>
                  </div><!-- media-body -->
                  <hr>
                  <div class="clearfix mt20">
                      <div class="pull-left">
                        <h5 class="md-title nomargin">This Mounth</h5>
                        <h4 class="nomargin"><?php $sqla=mysqli_fetch_array(mysqli_query($koneksi,"SELECT count(*) as jumlah FROM fgmart WHERE tgl_post > DATE_SUB(NOW(), INTERVAL 1 MONTH)")); echo "$sqla[jumlah]";?></h4>
                      </div>
                      <div class="pull-right">
                        <h5 class="md-title nomargin">This Week</h5>
                        <h4 class="nomargin"><?php $sqla=mysqli_fetch_array(mysqli_query($koneksi,"SELECT count(*) as jumlah FROM fgmart WHERE tgl_post > DATE_SUB(NOW(), INTERVAL 1 WEEK)")); echo "$sqla[jumlah]";?></h4>
                      </div>
                  </div>
                </div><!-- panel-body -->
              </div><!-- panel -->
            </div>

            <div class="col-md-4">
              <div class="panel panel-dark noborder">
                <div class="panel-heading noborder">
                  <div class="panel-btns" style="display: block; opacity: 0;">
                    <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                  </div><!-- panel-btns -->
                  <div class="panel-icon"><img src="<?php echo"{$base}/adminfg/images/discount.png"; ?>" width="40" height="40" style="margin: 10px;" /></div>
                  <div class="media-body">
                    <h4 class="nomargin">Coupon</h4>
                    <h1 class="mt5">
                      <?php $sql=mysqli_num_rows(mysqli_query($koneksi,"select * from coupon")); echo "$sql";?>
                    </h1>
                  </div><!-- media-body -->
                  <hr>
                  <div class="clearfix mt20">
                      <div class="pull-left">
                        <h5 class="md-title nomargin">This Mounth</h5>
                        <h4 class="nomargin"><?php $sqla=mysqli_fetch_array(mysqli_query($koneksi,"SELECT count(*) as jumlah FROM coupon WHERE tgl_post > DATE_SUB(NOW(), INTERVAL 1 MONTH)")); echo "$sqla[jumlah]";?></h4>
                      </div>
                      <div class="pull-right">
                        <h5 class="md-title nomargin">This Week</h5>
                        <h4 class="nomargin"><?php $sqla=mysqli_fetch_array(mysqli_query($koneksi,"SELECT count(*) as jumlah FROM coupon WHERE tgl_post > DATE_SUB(NOW(), INTERVAL 1 WEEK)")); echo "$sqla[jumlah]";?></h4>
                      </div>
                  </div>
                </div><!-- panel-body -->
              </div><!-- panel -->
            </div>

            <div class="col-md-4">
              <div class="panel panel-success-alt noborder">
                <div class="panel-heading noborder">
                  <div class="panel-btns" style="display: block; opacity: 0;">
                    <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                  </div><!-- panel-btns -->
                  <div class="panel-icon"><img src="<?php echo"{$base}/adminfg/images/article.png"; ?>" width="40" height="40" style="margin: 10px;" /></div>
                  <div class="media-body">
                    <h4 class="nomargin">Article</h4>
                    <h1 class="mt5">
                      <?php $sql=mysqli_num_rows(mysqli_query($koneksi,"select * from article")); echo "$sql";?>
                    </h1>
                  </div><!-- media-body -->
                  <hr>
                  <div class="clearfix mt20">
                      <div class="pull-left">
                        <h5 class="md-title nomargin">This Mounth</h5>
                        <h4 class="nomargin"><?php $sqla=mysqli_fetch_array(mysqli_query($koneksi,"SELECT count(*) as jumlah FROM article WHERE tgl_post > DATE_SUB(NOW(), INTERVAL 1 MONTH)")); echo "$sqla[jumlah]";?></h4>
                      </div>
                      <div class="pull-right">
                        <h5 class="md-title nomargin">This Week</h5>
                        <h4 class="nomargin"><?php $sqla=mysqli_fetch_array(mysqli_query($koneksi,"SELECT count(*) as jumlah FROM article WHERE tgl_post > DATE_SUB(NOW(), INTERVAL 1 WEEK)")); echo "$sqla[jumlah]";?></h4>
                      </div>
                  </div>
                </div><!-- panel-body -->
              </div><!-- panel -->
            </div>

            <div class="col-md-4">
              <div class="panel panel-warning-alt noborder">
                <div class="panel-heading noborder">
                  <div class="panel-btns" style="display: block; opacity: 0;">
                    <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                  </div><!-- panel-btns -->
                  <div class="panel-icon"><img src="<?php echo"{$base}/adminfg/images/play-button.png"; ?>" width="40" height="40" style="margin: 10px;" /></div>
                  <div class="media-body">
                    <h4 class="nomargin">Video</h4>
                    <h1 class="mt5">
                      <?php $sql=mysqli_num_rows(mysqli_query($koneksi,"select * from video")); echo "$sql";?>
                    </h1>
                  </div><!-- media-body -->
                  <hr>
                  <div class="clearfix mt20">
                      <div class="pull-left">
                        <h5 class="md-title nomargin">This Mounth</h5>
                        <h4 class="nomargin"><?php $sqla=mysqli_fetch_array(mysqli_query($koneksi,"SELECT count(*) as jumlah FROM video WHERE tgl_post > DATE_SUB(NOW(), INTERVAL 1 MONTH)")); echo "$sqla[jumlah]";?></h4>
                      </div>
                      <div class="pull-right">
                        <h5 class="md-title nomargin">This Week</h5>
                        <h4 class="nomargin"><?php $sqla=mysqli_fetch_array(mysqli_query($koneksi,"SELECT count(*) as jumlah FROM video WHERE tgl_post > DATE_SUB(NOW(), INTERVAL 1 WEEK)")); echo "$sqla[jumlah]";?></h4>
                      </div>
                  </div>
                </div><!-- panel-body -->
              </div><!-- panel -->
            </div>

            <div class="col-md-4">
              <div class="panel panel-primary noborder">
                <div class="panel-heading noborder">
                  <div class="panel-btns" style="display: block; opacity: 0;">
                    <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                  </div><!-- panel-btns -->
                  <div class="panel-icon"><img src="<?php echo"{$base}/adminfg/images/group.png"; ?>" width="40" height="40" style="margin: 10px;" /></div>
                  <div class="media-body">
                    <h4 class="nomargin">Member</h4>
                    <h1 class="mt5">
                      <?php $sql=mysqli_num_rows(mysqli_query($koneksi,"select * from member")); echo "$sql";?>
                    </h1>
                  </div><!-- media-body -->
                  <hr>
                  <div class="clearfix mt20">
                      <div class="pull-left">
                        <h5 class="md-title nomargin">This Mounth</h5>
                        <h4 class="nomargin"><?php $sqla=mysqli_fetch_array(mysqli_query($koneksi,"SELECT count(*) as jumlah FROM member WHERE tgl_join > DATE_SUB(NOW(), INTERVAL 1 MONTH)")); echo "$sqla[jumlah]";?></h4>
                      </div>
                      <div class="pull-right">
                        <h5 class="md-title nomargin">This Week</h5>
                        <h4 class="nomargin"><?php $sqla=mysqli_fetch_array(mysqli_query($koneksi,"SELECT count(*) as jumlah FROM member WHERE tgl_join > DATE_SUB(NOW(), INTERVAL 1 WEEK)")); echo "$sqla[jumlah]";?></h4>
                      </div>
                  </div>
                  <div class="clearfix mt20">
                      <div class="pull-right">
                        <a class='btn btn-default' href="detail-member-2.htm">Detail Info Member</a>
                      </div>
                  </div>
                </div><!-- panel-body -->
              </div><!-- panel -->
            </div>

          </div>
	      </div><!-- contentpanel -->
      </div><!-- mainpanel -->
    </div><!-- mainwrapper -->
</section>

<!-- <script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/pace.min.js"></script>
<script src="js/retina.min.js"></script>
<script src="js/jquery.cookies.js"></script> -->

<!-- <script src="js/custom.js"></script> -->
