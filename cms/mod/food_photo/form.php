<style>
    html, body {
    height: 100%;
    margin: 0;
    padding: 0;
    }
    #map {
    width: 100%;
    height: 400px;
    }
    .controls {
    margin-top: 10px;
    border: 1px solid transparent;
    border-radius: 2px 0 0 2px;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    height: 32px;
    outline: none;
    box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    }
    #searchInput {
    background-color: #fff;
    font-family: Roboto;
    font-size: 15px;
    font-weight: 300;
    margin-left: 12px;
    padding: 0 11px 0 13px;
    text-overflow: ellipsis;
    width: 50%;
    }
    #searchInput:focus {
    border-color: #4d90fe;
    }
</style>
<section>
    <div class="mainwrapper">
        <?php include "inc/kiri.php"; ?>
        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="pageicon pull-left">
                        <i class="fa fa-picture-o"></i>
                    </div>
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href="mod-beranda.htm"><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="mod-beranda.htm">Beranda</a></li>
                            <li>Informasi Food Photo</li>
                        </ul>
                        <h4>Informasi Food Photo</h4>
                    </div>
                </div>
                <!-- media -->
            </div>
            <!-- pageheader -->
            <div class="contentpanel">
                <?php if($_GET['proses']==1){ ?>
					<?php if(isset($_GET['info'])) include "inc/form-alert.php"; ?>
                <form action="mod/<?php echo"$mod/aksi.php?mod=$mod&ale=2&url=$_GET[url]"; ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" id="signup-daftar">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns">
                                <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                            </div>
                            <!-- panel-btns -->
                            <h4 class="panel-title">Data Food Photo</h4>
                            <p>Silahkan lengkapi data dibawah ini.</p>
                        </div>
                        <div class="panel-body nopadding">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Member :</label>
                                <div class="col-sm-10">
                                    <select name="id_member" class="pilih" style="width: 100%;">
                                    <?php
                                        $sql=mysqli_query($koneksi,"select id_member,username from member order by username asc");
                                        while($b=mysqli_fetch_array($sql)){
                                        	echo"<option value='$b[id_member]'>$b[username]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Country :</label>
                                <div class="col-sm-10">
                                    <select name="negara" class="pilih" style="width: 100%;" id="negara" data-action="propinsi" data-target="#propinsi">
                                        <option value="">Select Country</option>
                                        <?php
                                            $sql=mysqli_query($koneksi,"select id_negara,nama_negara from negara order by nama_negara asc");
                                            while($b=mysqli_fetch_array($sql)){
                                            	echo"<option value='$b[id_negara]'>$b[nama_negara]</option>";
                                            }
                                            ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">State/Province :</label>
                                <div class="col-sm-10">
                                    <select name="propinsi" class="pilih" style="width: 100%;" id="propinsi" data-action="kota" data-target="#kota">
                                        <option value="">Select State/ Province</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">City :</label>
                                <div class="col-sm-10">
                                    <select name="kota" class="pilih" style="width: 100%;" id="kota" data-action="resto" data-target="#restaurant">
                                        <option value="">Select City</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Restaurant Name :</label>
                                <div class="col-sm-10">
                                    <select name="restaurant_name" class="pilih" style="width: 100%;" id="restaurant" data-action="food" data-target="#food">
                                        <option value="">Select Restaurant</option>
                                    </select> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Food Name :</label>
                                <div class="col-sm-10">
                                    <select name="nama_food" class="pilih" style="width: 100%;" id="food">
                                        <option value="">Select Restaurant</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Photo Title <span class="f-merah">*</span></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" placeholder="Write Name" name="photo_name" required maxlength="50">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Cover Photo <span class="f-merah">*</span></label>
                                <div class="col-sm-10">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview upload" data-trigger="fileinput"></div>
                                        <input type="file" name="gambar_photo" class="hidden" required>
                                    </div>
                                    <p class="help-block">Image used have to be in JPG, PNG, and GIF format and the maximum total images size is 50MB also minimal dimension must be 800x600 pixel.</p>
                                </div>
                            </div>
                        </div>
                        <!-- panel-body -->
                    </div>
                    <!-- panel -->
                    <div class="panel panel-default">
                        <div class="panel-footer">
                            <button class="btn btn-primary mr5">Simpan</button>
                            <a href="mod-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-warning">Kembali</a>
                        </div>
                        <!-- panel-footer -->
                    </div>
                    <!-- panel -->
                </form>
                <?php
                    }else{
                          $edit=mysqli_query($koneksi,"select * from food_photo where id_food_photo = '$_GET[id]'");
                          $e=mysqli_fetch_array($edit);
                    ?>
                <form action="mod/<?php echo"$mod/aksi.php?mod=$mod&ale=3&url=$_GET[url]"; ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" id="signup-daftar">
                    <input type="hidden" name="id" value="<?php echo $e['id_food_photo']; ?>">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns">
                                <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                            </div>
                            <!-- panel-btns -->
                            <h4 class="panel-title">Data food Photo</h4>
                            <p>Silahkan lengkapi data dibawah ini.</p>
                        </div>
                        <div class="panel-body nopadding">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Member :</label>
                                <div class="col-sm-10">
                                    <select name="id_member" class="pilih" style="width: 100%;">
                                    <?php
                                        $sql=mysqli_query($koneksi,"select id_member,username from member order by username asc");
                                        while($b=mysqli_fetch_array($sql)){
                                        	echo"<option value='$b[id_member]'"; if($b['id_member']==$e['id_member']){ echo "selected";} echo ">$b[username]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Photo Title <span class="f-merah">*</span></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" placeholder="Write Name" name="photo_name" required maxlength="50" value="<?php echo"$e[nama_food_photo]" ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Cover Photo <span class="f-merah">*</span></label>
                                <div class="col-sm-10">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview upload" data-trigger="fileinput">
                                            <img src="<?php echo"$base/assets/img/food/small_$e[gambar_food_photo]"; ?>">
                                        </div>
                                        <input type="file" name="gambar_photo" class="hidden">
                                        <input type="hidden" name="gambar" value="<?php echo $e['gambar_food_photo'];?>">
                                    </div>
                                    <p class="help-block">Image used have to be in JPG, PNG, and GIF format and the maximum total images size is 50MB also minimal dimension must be 800x600 pixel.</p>
                                </div>
                            </div>
                        </div>
                        <!-- panel-body -->
                    </div>
                    <!-- panel -->
                    <div class="panel panel-default">
                        <div class="panel-footer">
                            <button class="btn btn-primary mr5">Simpan</button>
                            <a href="mod-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-warning">Kembali</a>
                        </div>
                        <!-- panel-footer -->
                    </div>
                    <!-- panel -->
                </form>
                <?php
                    } ?>
            </div>
            <!-- contentpanel -->
        </div>
        <!-- mainpanel -->
    </div>
    <!-- mainwrapper -->
</section>
<script src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".pilih").select2();
	});
</script>
<script type="text/javascript">
	$(document).ready(function(){
		var Cite = {
			url: null,
			init: function(){
				$("#negara, #propinsi, #kota, #restaurant").change(Cite.ajax);
			},
			ajax: function(){
				var $this = $(this),
					data = $this.data() || {},
					url ='<?php echo"$base";?>/config/func/ajax_'+ data.action +'.php';
					
				$.post(url, {id: $this.val()}, function( success ) {
					var $target = $(data.target);
	
					$target.html(success);
					$target.fadeIn(2000);
				}, 'html');
			}
		};
		
		Cite.init(); 
    });
</script>
<style media="screen">
    #country-list {
    margin: 0;
    list-style: none;
    padding: 0px;
    max-height: 155px;
    position: relative;
    overflow: auto;
    }
    .ko {
    border: 1px solid #CCC;
    padding: 5px;
    color: #9C9999;
    border-top: none;
    cursor: pointer;
    transition: 0.7s all 0s ease;
    }
    .ko:hover{
    background: #ffeed0;
    transition: 0.3s all 0s ease;
    }
</style>