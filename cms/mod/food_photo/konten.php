<section>
    <div class="mainwrapper">
        <?php include "inc/kiri.php"; ?>
        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="pageicon pull-left">
                        <i class="fa fa-picture-o"></i>
                    </div>
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href="mod-beranda-1.htm"><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="mod-beranda-1.htm">Beranda</a></li>
                            <li>Informasi Food Photo</li>
                        </ul>
                        <h4>Informasi Food Photo</h4>
                    </div>
                </div>
                <!-- media -->
            </div>
            <!-- pageheader -->
            <div class="contentpanel">
                <?php
                    if(isset($_GET['info'])){
                    	if($_GET['info']==1){
                    		echo"<div class='alert alert-success'>
                    			<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
                    			<strong>Sukses!</strong> data berhasil dihapus.
                    		</div>";
                    	}
                    }
                    ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-sm-11">
                                <p><a href="add-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-primary">New Submit</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table id="aledata" class="table table-striped table-bordered responsive">
                            <thead class="">
                                <tr>
                                    <th>No</th>
                                    <th>Image</th>
                                    <th>Title</th>
                                    <th>Food Name</th>
                                    <th>Member</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $no = 0;
									$food=mysqli_query($koneksi,"SELECT * FROM food_photo p LEFT JOIN food r ON p.id_food=r.id_food LEFT JOIN member m ON p.id_member=m.id_member");
                                    while($r=mysqli_fetch_array($food)){
                                    $no++;
                                    echo"<tr>
                                    	<td width='30px'>$no.</td>
                                    	<td><img src='$base/assets/img/food/small_$r[gambar_food_photo]'></td>
                                    	<td>$r[nama_food_photo]</td>
                                    	<td>$r[nama_food]</td>
                                        <td>$r[username]</td>
                                    	<td width='170px'>
                                    			<a href='edit-$mod-$r[id_food_photo]-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>
                                    			<a class='btn btn-danger btn-rounded btn-sm' href='mod/$mod/aksi.php?mod=$mod&id=$r[id_food_photo]&gambarthumb=$r[gambar_food_photo]&ale=1&url=$_GET[url]' onClick=\"return confirm('apakah anda yakin akan menghapus data ini ?')\"><i class='fa fa-trash-o'></i> Hapus</a>
                                    	</td>
                                    </tr>";
                                    }
                                    ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- panel -->
            </div>
            <!-- contentpanel -->
        </div>
        <!-- mainpanel -->
    </div>
    <!-- mainwrapper -->
</section>

