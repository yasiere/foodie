<section>
    <div class="mainwrapper">
        <?php 
			include "inc/kiri.php"; 
			$pageTitle = 'Pages';
			?>
        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="pageicon pull-left">
                        <i class="fa fa-tags"></i>
                    </div>
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href="mod-beranda-1.htm"><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="mod-beranda-1.htm">Beranda</a></li>
                            <li>Informasi <?php echo $pageTitle;?></li>
                        </ul>
                        <h4>Informasi <?php echo $pageTitle;?></h4>
                    </div>
                </div>
                <!-- media -->
            </div>
            <!-- pageheader -->
            <div class="contentpanel">
                <?php if(isset($_GET['info']) && $_GET['info']==1): ?>
					<div class="alert alert-success">
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button
						<strong>Sukses!</strong> data berhasil dihapus.
					</div>
				<?php endif; ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-sm-11">
                                <p><a href="add-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-primary">New Submit</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table id="aledata" class="table table-striped table-bordered responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Page </th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $no = 0;
									$sql=mysqli_query($koneksi,"SELECT id_page, nama_page FROM pages ORDER BY nama_page");
                                    while( $r = mysqli_fetch_array($sql) ){
										$no++;
                                    	echo"<tr>
                                    		<td width='30px'>{$no}.</td>
                                    		<td>{$r['nama_page']}</td>
                                    		<td width='170px'>
												<a href='edit-{$mod}-{$r['id_page']}-{$_GET['url']}.htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>
                                    		</td>
                                    	</tr>";
                                    }
                                    ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- panel -->
            </div>
            <!-- contentpanel -->
        </div>
        <!-- mainpanel -->
    </div>
    <!-- mainwrapper -->
</section>

