<?php
session_start();
if(!empty($_SESSION['ak_id'])){
	include "../../inc/db.php";
	include "../../inc/upload.php";
	include "../../inc/id_masking.php";
	include '../../inc/seo.php';
	include "../../../config/func/upload_gambar_bms.php";

	$mod			= $_GET['mod'];
	$tglhariini		= date("Y-m-d");
	$ukuran_maks	= 552428800;
	$error_cover 	= 0;
	$error_profil 	= 0;

	if($_GET['ale']==1){
		mysqli_query($koneksi,"DELETE FROM beverage WHERE id_beverage='$_GET[id]'");
		unlink("../../../assets/img/beverage/small_".$_GET['gambarthumb']);
		unlink("../../../assets/img/beverage/medium_".$_GET['gambarthumb']);
		unlink("../../../assets/img/beverage/big_".$_GET['gambarthumb']);
		header("Location: ../../mod-beverage-8.htm");
	}
	elseif($_GET['ale']==2){
		$sql=mysqli_query($koneksi,"select id_restaurant from restaurant where id_restaurant='$_POST[restaurant]'");
		$ada=mysqli_num_rows($sql);
		if($ada==0){
			header("Location: ../../add-beverage-8.htm");
			exit();
		}
		
		date_default_timezone_set("Asia/Jakarta");
		$sekarang=date("Y-m-d H:i:s");

		$ukuran_maks	= 552428800;

		$nama_cover			= $_FILES['gambar_photo']['name'];
		$ukuran_cover 		= $_FILES['gambar_photo']['size'];
		$lokasi_cover		= $_FILES['gambar_photo']['tmp_name'];
		list($width_cover, $height_cover) = getimagesize($lokasi_cover);
		if($ukuran_cover > $ukuran_maks or !preg_match("/.(gif|jpg|png)$/i", $nama_cover) or $width_cover < 800 or $height_cover < 600){
			$_SESSION['notif']     = "gambar";
			header("Location: ../../add-beverage-8.htm");
			exit();
		}
		else{
			$making = "";
			if(!empty($_POST['making']))
			{
				$making = implode('+', $_POST['making']);
			}
			
			$r=mysqli_fetch_array($sql);
			$bev = array(
				'id_member' => $_POST['id_member'],
				'id_restaurant' => $r['id_restaurant'],
				'nama_beverage' => $_POST['nama_beverage'],
				'id_beverage_category' => $_POST['category'],
				'id_price_index' => $_POST['price_index'],
				'making_methode' => $making,
				'tgl_post' => $sekarang,
				'id_tag' => $_POST['tag'],
				'type_serving' => $_POST['alcohol'],
			);
			$sql=mysqli_query($koneksi,"INSERT INTO beverage(". implode(',', array_keys($bev)) .") VALUES('". implode("','", array_values($bev)) ."')");
			if($sql){
				$cover=upload_gambar($nama_cover,$lokasi_cover,"../../../assets/img/beverage");
				$id_beverage=mysqli_insert_id($koneksi);
				mysqli_query($koneksi,"INSERT INTO beverage_photo (id_member,id_beverage,nama_beverage_photo,gambar_beverage_photo) VALUES('$_POST[id_member]','$id_beverage','$_POST[nama_beverage]','$cover')");
				mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('beverage','$id_beverage')");
				// if($_POST['feed_submit']==1){
					mysqli_query($koneksi,"INSERT INTO activity (id_member,kat_act,deskripsi,tgl) VALUES('$_POST[id_member]','feed_submit','Submit new beverage review','$sekarang')");
				// }
				if(!empty($_POST['rating']) or !empty($_POST['cleanlines']) or !empty($_POST['flavor']) or !empty($_POST['freshness']) or !empty($_POST['cooking']) or !empty($_POST['aroma']) or !empty($_POST['serving'])){
					if(!empty($_POST['rating'])){
						$r1=$_POST['rating'] * 0.21;
						$r2=$_POST['rating'] * 0.20;
						$r3=$_POST['rating'] * 0.18;
						$r4=$_POST['rating'] * 0.17;
						$r5=$_POST['rating'] * 0.15;
						$r6=$_POST['rating'] * 0.09;
					}
					else{
						$r1=$_POST['cleanlines'] * 0.21;
						$r2=$_POST['flavor'] * 0.20;
						$r3=$_POST['freshness'] * 0.18;
						$r4=$_POST['cooking'] * 0.17;
						$r5=$_POST['aroma'] * 0.15;
						$r6=$_POST['serving'] * 0.09;
					}
					mysqli_query($koneksi,"INSERT INTO beverage_rating (id_beverage,id_member,cleanlines,flavor,freshness,cooking,aroma,serving,tgl_beverage_rating) values('$id_beverage','$_POST[id_member]','$r1','$r2','$r3','$r4','$r5','$r6','$sekarang')");
				}
				$id = id_masking($id_beverage);
				header("Location: ../../mod-beverage-8.htm");
				exit();
			}
		}
	}



	elseif($_GET['ale']==3){
		date_default_timezone_set("Asia/Jakarta");
		$sekarang=date("Y-m-d H:i:s");
		
		$making = "";
		if(!empty($_POST['making']))
		{
			$making = implode('+', $_POST['making']);
		}

		mysqli_query($koneksi,"update beverage set id_member='{$_POST['id_member']}', nama_beverage='$_POST[nama_beverage]',id_beverage_category='$_POST[category]',id_price_index='$_POST[price_index]', making_methode='$making', id_tag='$_POST[tag]', type_serving='$_POST[alcohol]' where id_beverage='$_POST[id]'");
		$_SESSION['notif'] = "sukses";
		header("Location: ../../mod-beverage-8.htm");
		exit();
	}
}
else{
	header('location:not-found.htm');
}
?>
