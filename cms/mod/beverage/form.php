<style>
    html, body {
    height: 100%;
    margin: 0;
    padding: 0;
    }
    #map {
    width: 100%;
    height: 400px;
    }
    .controls {
    margin-top: 10px;
    border: 1px solid transparent;
    border-radius: 2px 0 0 2px;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    height: 32px;
    outline: none;
    box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    }
    #searchInput {
    background-color: #fff;
    font-family: Roboto;
    font-size: 15px;
    font-weight: 300;
    margin-left: 12px;
    padding: 0 11px 0 13px;
    text-overflow: ellipsis;
    width: 50%;
    }
    #searchInput:focus {
    border-color: #4d90fe;
    }
</style>
<section>
    <div class="mainwrapper">
        <?php
			include "inc/kiri.php";
			$pageTitle = 'Beverage';
			?>
        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="pageicon pull-left">
                        <i class="fa fa-picture-o"></i>
                    </div>
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href="mod-beranda.htm"><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="mod-beranda.htm">Beranda</a></li>
                            <li>Informasi <?php echo $pageTitle;?></li>
                        </ul>
                        <h4>Informasi <?php echo $pageTitle;?></h4>
                    </div>
                </div>
                <!-- media -->
            </div>
            <!-- pageheader -->
            <div class="contentpanel">
                <?php if($_GET['proses']==1){ ?>
                <?php if(isset($_GET['info'])) include "inc/form-alert.php"; ?>
                <form action="mod/<?php echo"$mod/aksi.php?mod=$mod&ale=2&url=$_GET[url]"; ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" id="signup-daftar">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns">
                                <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                            </div>
                            <!-- panel-btns -->
                            <h4 class="panel-title">Data <?php echo $pageTitle;?></h4>
                            <p>Silahkan lengkapi data dibawah ini.</p>
                        </div>
                        <div class="panel-body nopadding">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Member :</label>
                                <div class="col-sm-10">
                                    <select name="id_member" class="pilih" style="width: 100%;">
                                    <?php
                                        $sql=mysqli_query($koneksi,"select id_member,username from member order by username asc");
                                        while($b=mysqli_fetch_array($sql)){
                                        	echo"<option value='$b[id_member]'>$b[username]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Country :</label>
                                <div class="col-sm-10">
                                    <select name="negara" class="pilih" style="width: 100%;" data-target="#propinsi" data-cite="propinsi">
                                        <option value="">Select Country</option>
                                        <?php
                                            $sql=mysqli_query($koneksi,"select id_negara,nama_negara from negara order by nama_negara asc");
                                            while($b=mysqli_fetch_array($sql)){
                                            	echo"<option value='$b[id_negara]'>$b[nama_negara]</option>";
                                            }
                                            ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">State/Province :</label>
                                <div class="col-sm-10">
                                    <select name="propinsi" class="pilih" style="width: 100%;" id="propinsi" data-target="#kota" data-cite="kota">
                                        <option value="">Select State/ Province</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">City :</label>
                                <div class="col-sm-10">
                                    <select name="kota" class="pilih" style="width: 100%;" id="kota" data-target="#restaurant" data-cite="resto">
                                        <option value="">Select City</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Restaurant Name :</label>
                                <div class="col-sm-10">
                                    <select name="restaurant" class="pilih" style="width: 100%;" id="restaurant">
                                        <option value="">Select Restaurant</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Beverage Name :</label>
                                <div class="col-sm-10">
				                         <input type="text" name="nama_beverage" placeholder="Write Name" class="form-control">
                                </div>
                            </div>
    							<div class="form-group">
    								<label class="col-sm-2 control-label">Category :</label>
    								<div class="col-sm-10">
    									<select name="category" required class="pilih" style="width: 100%;" id="kat">
    										<option value="">Select Category</option>
    										<?php
    											$sql=mysqli_query($koneksi,"select * from beverage_category order by nama_beverage_category asc");
    											while($a=mysqli_fetch_array($sql)){
    												echo"<option value='$a[id_beverage_category]'>$a[nama_beverage_category]</option>";
    											}
    											?>
    									</select>
    								</div>
    							</div>
                  <div class="form-group">
                      <label class="col-sm-2 control-label">Tag :</label>
                      <div class="col-sm-10">
                        <input type="text" name="tag" placeholder="Write tag (separated by comma)" class="form-control">
                      </div>
                  </div>
    							<div class="form-group">
    								<label class="col-sm-2 control-label">Price Index :</label>
    								<div class="col-sm-10">
    									<select name="price_index" required class="pilih" style="width: 100%;">
    									<?php
    										$sql=mysqli_query($koneksi,"select * from price_index order by nama_price_index asc");
    										while($g=mysqli_fetch_array($sql)){
    											echo"<option value='$g[id_price_index]'>$g[nama_price_index]</option>";
    										}
    										?>
    									</select>
    								</div>
    							</div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Alcohol Serving :</label>
                    <div class="col-sm-10">
                      <div class="radio radio-inline">
                        <label>
                        <input type="radio" name="alcohol" value="No" id="alcohol1">
                          Yes
                        </label>
                      </div>
                      <div class="radio radio-inline radio-danger">
                        <label>
                        <input type="radio" name="alcohol" value="Halal" id="alcohol2" checked="">
                          No
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                      <label class="col-sm-2 control-label">Cover Photo <span class="f-merah">*</span></label>
                      <div class="col-sm-10">
                          <div class="fileinput fileinput-new" data-provides="fileinput">
                              <div class="fileinput-preview upload" data-trigger="fileinput"></div>
                              <input type="file" name="gambar_photo" class="hidden" required>
                          </div>
                          <p class="help-block">Image used have to be in JPG, PNG, and GIF format and the maximum total images size is 50MB also minimal dimension must be 800x600 pixel.</p>
                      </div>
                  </div>
			            <div class="accordion">Making Methode</div>
    							<div class="panels">
    								<div class="form-group">
    									<?php
    										$sql=mysqli_query($koneksi,"select * from making_methode order by nama_making_methode asc");
    										$t=1;
    										while($s=mysqli_fetch_array($sql)){
    											echo"<div class='col-md-3'>
    												<div class='checkbox checkbox-inline checkbox-danger'>
    													<input type='checkbox' name='making[]' value='$s[nama_making_methode]' id='making$t'>
    													<label for='making$t'> $s[nama_making_methode]</label>
    												</div>
    											</div>";
    											$t++;
    										}
    										?>
    								</div>
    							</div>
				          <div class="accordion">Beverage Rate</div>
    							<div class="panels">
    								<div role="tabpanel" id="tab-vote">
    									<!-- Nav tabs -->
    									<ul class="nav nav-tabs" role="tablist">
    										<li role="presentation" class="active"><a href="#feature" aria-controls="feature" role="tab" data-toggle="tab">Feature Vote</a></li>
    										<li role="presentation"><a href="#quick" aria-controls="quick" role="tab" data-toggle="tab">Quick Vote</a></li>
    									</ul>
    									<!-- Tab panes -->
    									<div class="tab-content">
    										<div role="tabpanel" class="tab-pane tab-panel active fade in" id="feature">
    											<table width="100%">
    												<tr>
    													<td>Cleanliness <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall cleanliness of the beverage and plates served, which can include whether the beverage has unwanted insect, plastic, hair or unclean plates, bowls and utensils used."></i></td>
    													<td><input type="hidden" class="rating" id="rat1" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="cleanlines"></td>
    												</tr>
    												<tr>
    													<td>Flavour <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall taste of the beverage which has unique personality."></i></td>
    													<td><input type="hidden" class="rating" id="rat2" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="flavor"></td>
    												</tr>
    												<tr>
    													<td>Freshness <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment on the beverage ingredient's freshness which can include meat, seabeverage, vegetables and sauces."></i></td>
    													<td><input type="hidden" class="rating" id="rat3" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="freshness"></td>
    												</tr>
    												<tr>
    													<td>Cooking <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment of the way the beverage is being cooked wheither it is undercooked or overcooked. However, it also depend on how we like the beverage to be cook."></i></td>
    													<td><input type="hidden" class="rating" id="rat4" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="cooking"></td>
    												</tr>
    												<tr>
    													<td>Presentasion &amp; Aroma <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment on the beverage's presentation and its aroma. A good presentation on beverage will increase our appetite for the beverage. likewise for the aroma too. A fragrance beverage cook meters away can attract our sense of smell. Good beverage makes better with great presentation and aroma."></i></td>
    													<td><input type="hidden" class="rating" id="rat5" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="aroma"></td>
    												</tr>
    												<tr>
    													<td>Serving <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Is the beverage being served having adequate serving? Too much or too little? Unlike Western cuisine which have courses of beverage for a complete meal, Eastern cuisine are more direct and usually treat as the main course. Therefore, the serving is vary."></i></td>
    													<td><input type="hidden" class="rating" id="rat6" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="serving"></td>
    												</tr>
    											</table>
    										</div>
    										<div role="tabpanel" class="tab-pane tab-panel fade" id="quick">
    											<table width="100%">
    												<tr>
    													<td><strong>Give your vote</strong></td>
    													<td>
    														<input type="hidden" class="rating" id="rat6" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" value="" name="rating">
    													</td>
    												</tr>
    											</table>
    										</div>
    									</div>
    									<ul class="list-inline">
    										<li><i class="fa fa-star"></i> Poor</li>
    										<li><i class="fa fa-star"></i><i class="fa fa-star"></i> Below Average</li>
    										<li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Average</li>
    										<li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Very Good</li>
    										<li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Excellent</li>
    									</ul>
    								</div>
    							</div>
                </div>
                        <!-- panel-body -->
                    </div>
                    <!-- panel -->
                    <div class="panel panel-default">
                        <div class="panel-footer">
                            <button class="btn btn-primary mr5">Simpan</button>
                            <a href="mod-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-warning">Kembali</a>
                        </div>
                        <!-- panel-footer -->
                    </div>
                    <!-- panel -->
                </form>
                <?php

                    }








                    elseif ($_GET['proses']==3) {
                      $dat=mysqli_query($koneksi,"select * from member m, negara n where m.id_negara = n.id_negara and m.id_member='$_GET[id]'");
              				$d=mysqli_fetch_array($dat);
                      ?>
                      <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns">
                                <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                            </div><!-- panel-btns -->
                            <h4 class="panel-title">Data Member</h4>
                            <p>Silahkan lengkapi data dibawah ini.</p>
                        </div>
                        <div class="panel-body nopadding">
                          <div class="form-group">
                            <label class="col-sm-1 control-label">Total</label>
                            <div class="col-sm-10">
                              <div class="col-sm-10 control-label"> :
                                <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT id_beverage FROM `beverage`")); echo "$juto"; ?>
                              </div>
                            </div>
                          </div>

            							<div class="form-group">
                            <label class="col-sm-1 control-label">Country</label>
                            <div class="col-sm-10">
                              <div class="col-sm-10 control-label"> :
                                <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT n.nama_negara,n.id_negara, COUNT(*) FROM beverage m JOIN negara n ON m.negara = n.id_negara GROUP BY n.nama_negara")); echo "$juto"; ?>
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-1 control-label"></label>
                            <div class="col-sm-10">
                              <select name="country-list" class="pilih" style="width: 200px" onchange="document.location.href=this.value">
            										<option value="0">Select Country</option>
            										<?php
            											$idnegara = "";
            											if (!empty($_GET['negara'])) {
            												$idnegara = "AND id_negara='$_GET[negara]'";
            											}

            											$sql = mysqli_query($koneksi, "SELECT n.nama_negara,n.id_negara, COUNT(*) FROM beverage m JOIN negara n ON m.negara = n.id_negara GROUP BY n.nama_negara");
            											while ($a = mysqli_fetch_array($sql)) {
            												if ($_GET['negara']==$a['id_negara']) {
            													echo "<option value='detail-beverage-8-country$a[id_negara].htm' selected>$a[nama_negara]</option>";
            												}
            												else {
            													echo "<option value='detail-beverage-8-country$a[id_negara].htm'>$a[nama_negara]</option>";
            												}

            											}
            										?>
            									</select>
                              <table width='100%'>
                                <tr>
                                  <td width='40%'>
                                    <label>Country</label>
                                  </td>
                                  <td>
                                    <label>Total</label>
                                  </td>
                                  <td>
                                    <label>Percentage</label>
                                  </td>
                                </tr>
                                <?php
                                $sql=mysqli_query($koneksi, "SELECT *, COUNT(*) as total,
            										(SELECT COUNT(*) FROM beverage) as total, n.id_negara, COUNT(*) as jumlah ,
            										concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM beverage )) * 100 ),2),'%') AS 'percentage'
            										FROM `beverage` f LEFT JOIN negara n on f.negara=n.id_negara GROUP by negara");
            										$jumsql=mysqli_num_rows($sql);
                                while ($juto=mysqli_fetch_array($sql)) {
            											$total= $juto['total'];
                                  echo"
                                    <tr>
                                      <td width='30%'>
                                        <a href='https://www.foodieguidances.com/pages/beverage/search/?country=$juto[id_negara]'>
                                            <span style='width: 40px;display: inline-block;'>$juto[id_negara]</span> $juto[nama_negara]
                                        </a>
                                      </td>
                                      <td width='30%'>
                                        $juto[jumlah]
                                      </td>
                                      <td width='30%'>
                                        $juto[percentage]
                                      </td>
                                    </tr>
                                  ";
                                };

                                 ?>
            										 <tr style="border-top: 1px solid; padding: 5px 0">
            											 <td> <label>Sum</label>
            											 </td>
            										 	<td>
            										 		<label><?php echo $total; ?></label>
            										 	</td>
            											<td>
            												<label> 100% </label>
            											</td>
            										 </tr>
                               </table>
                            </div>
                          </div>

            							<div class="form-group">
                            <label class="col-sm-1 control-label">Total propinsi</label>
                            <div class="col-sm-10">
                              <?php
            										$idnegara = "";
                                                    $idnegaras = "";
            										if (!empty($_GET['negara'])) {
            											$idnegara = "AND negara='$_GET[negara]'";
            											$idnegaras = "WHERE negara='$_GET[negara]'";
            										}
            									?>
                              :  <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT n.nama_propinsi,n.id_propinsi, COUNT(*) FROM beverage m JOIN propinsi n ON m.propinsi = n.id_propinsi $idnegaras GROUP BY n.id_propinsi")); echo "$juto"; ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-1 control-label"></label>
                            <div class="col-sm-10">
                              <table width='100%'>
                                <tr>
                                  <td width='40%'>
                                    <label>Propinsi</label>
                                  </td>
                                  <td>
                                    <label>Total</label>
                                  </td>
                                  <td>
                                    <label>Percentage</label>
                                  </td>
                                </tr>
                                <?php
                                $sql=mysqli_query($koneksi, "SELECT ne.id_negara, ne.nama_negara, n.nama_propinsi,(SELECT COUNT(*) FROM beverage $idnegaras) as total, 
                                    n.id_propinsi,COUNT(*) as jumlah ,
            										concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM beverage $idnegaras)) * 100 ),2),'%') AS 'percentage' 
                                                    FROM beverage m 
                                                    LEFT JOIN negara ne ON m.negara   =  ne.id_negara
                                                    JOIN propinsi n ON m.propinsi = n.id_propinsi $idnegaras GROUP BY n.nama_propinsi");
                                while ($juto=mysqli_fetch_array($sql)) {
            											$total= $juto['total'];
                                  echo"
                                    <tr>
                                      <td width='40%'>
                                        <a href='https://www.foodieguidances.com/pages/food/search/?country=$juto[id_negara]&state=$juto[id_propinsi]'>
                                            <span style='width: 40px;display: inline-block;'>$juto[id_propinsi]</span> $juto[nama_negara] - $juto[nama_propinsi]
                                        </a>
                                      </td>
                                      <td width='30%'>
                                        $juto[jumlah]
                                      </td>
                                      <td width='30%'>
                                        $juto[percentage]
                                      </td>
                                    </tr>
                                  ";
                                };
                                 ?>
            										 <tr style="border-top: 1px solid; padding: 5px 0">
            											 <td> <label>Sum</label>
            											 </td>
            										 	<td>
            										 		<label><?php echo $total; ?></label>
            										 	</td>
            											<td>
            												<label> 100% </label>
            											</td>
            										 </tr>
                               </table>
                            </div>
                          </div>

            							<div class="form-group">
                            <label class="col-sm-1 control-label">Total City</label>
                            <div class="col-sm-10">
                              :  <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT n.nama_kota,n.id_kota, COUNT(*) FROM beverage m JOIN kota n ON m.kota = n.id_kota $idnegaras GROUP BY n.id_kota")); echo "$juto"; ?>
                            </div>
                          </div>
            							<div class="form-group">
                            <label class="col-sm-1 control-label"></label>
                            <div class="col-sm-10">
                              <table width='100%'>
                                <tr>
                                  <td width='40%'>
                                    <label>City</label>
                                  </td>
                                  <td>
                                    <label>Total</label>
                                  </td>
                                  <td>
                                    <label>Percentage</label>
                                  </td>
                                </tr>
                                <?php
                                $sql=mysqli_query($koneksi, "SELECT pr.id_propinsi, pr.nama_propinsi,  ne.id_negara, ne.nama_negara, n.nama_kota,(SELECT COUNT(*) FROM beverage $idnegaras) as total,n.id_kota,COUNT(*) as jumlah , concat(round(( (COUNT(*) / (SELECT COUNT(*) 
                                                            FROM beverage $idnegaras )) * 100 ),2),'%') AS 'percentage' 
                                                            FROM beverage m 
                                                            LEFT JOIN negara ne ON m.negara   =  ne.id_negara
                                                            LEFT JOIN propinsi pr ON m.propinsi = pr.id_propinsi
                                                            LEFT JOIN kota n ON m.kota = n.id_kota 
                                                            $idnegaras GROUP BY n.nama_kota order by ne.id_negara asc");
                                while ($juto=mysqli_fetch_array($sql)) {
            											$total= $juto['total'];
                                  echo"
                                    <tr>
                                      <td width='40%'>
                                        <a href='https://www.foodieguidances.com/pages/food/search/?country=$juto[id_negara]&state=$juto[id_propinsi]&city=$juto[id_kota]'>
                                            <span style='width: 40px;display: inline-block;'>$juto[id_kota]</span> $juto[nama_negara] - $juto[nama_kota]
                                        </a>
                                      </td>
                                      <td width='30%'>
                                        $juto[jumlah]
                                      </td>
                                      <td width='30%'>
                                        $juto[percentage]
                                      </td>
                                    </tr>
                                  ";
                                };
                                 ?>
            										 <tr style="border-top: 1px solid; padding: 5px 0">
            											 <td> <label>Sum</label>
            											 </td>
            										 	<td>
            										 		<label><?php echo $total; ?></label>
            										 	</td>
            											<td>
            												<label> 100% </label>
            											</td>
            										 </tr>
                               </table>
                            </div>
                          </div>

            							<div class="form-group">
                            <label class="col-sm-1 control-label">Category</label>
                            <div class="col-sm-10">
                              :  <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT  COUNT(*) FROM beverage $idnegaras GROUP BY id_beverage_category")); echo "$juto"; ?>
                            </div>
                          </div>
            							<div class="form-group">
                            <label class="col-sm-1 control-label"></label>
                            <div class="col-sm-10">
                              <table width='100%'>
                                <tr>
                                  <td width='40%'>
                                    <label>Category</label>
                                  </td>
                                  <td>
                                    <label>Total</label>
                                  </td>
                                  <td>
                                    <label>Percentage</label>
                                  </td>
                                </tr>
                                <?php
                                $sql=mysqli_query($koneksi, "SELECT n.id_beverage_category, n.nama_beverage_category,
            											(SELECT COUNT(*) FROM beverage $idnegaras) as total, COUNT(*) as jumlah,
            											concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM beverage $idnegaras )) * 100 ),2),'%') AS 'percentage'
            											FROM beverage m JOIN beverage_category n ON m.id_beverage_category = n.id_beverage_category $idnegaras GROUP BY n.nama_beverage_category");
                                while ($juto=mysqli_fetch_array($sql)) {
            											$total= $juto['total'];
                                  echo"
                                    <tr>
                                      <td width='40%'>
                                        <a href='https://www.foodieguidances.com/pages/beverage/search/?category=$juto[id_beverage_category]'>
                                            $juto[nama_beverage_category]
                                        </a>
                                      </td>
                                      <td width='30%'>
                                        $juto[jumlah]
                                      </td>
                                      <td width='30%'>
                                        $juto[percentage]
                                      </td>
                                    </tr>
                                  ";
                                };
                                 ?>
            										 <tr style="border-top: 1px solid; padding: 5px 0">
            											 <td> <label>Sum</label>
            											 </td>
            										 	<td>
            										 		<label><?php echo $total; ?></label>
            										 	</td>
            											<td>
            												<label> 100% </label>
            											</td>
            										 </tr>
                               </table>
                            </div>
                          </div>

            							<div class="form-group">
                            <label class="col-sm-1 control-label">Price Index</label>
                            <div class="col-sm-10">
                              :  <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT  COUNT(*) FROM beverage $idnegaras GROUP BY id_price_index")); echo "$juto"; ?>
                            </div>
                          </div>
            							<div class="form-group">
                            <label class="col-sm-1 control-label"></label>
                            <div class="col-sm-10">
                              <table width='100%'>
                                <tr>
                                  <td width='40%'>
                                    <label>Price Index</label>
                                  </td>
                                  <td>
                                    <label>Total</label>
                                  </td>
                                  <td>
                                    <label>Percentage</label>
                                  </td>
                                </tr>
                                <?php
                                $sql=mysqli_query($koneksi, "SELECT p.id_price_index, p.nama_price_index,
            											(SELECT COUNT(*) FROM beverage $idnegaras) as total, COUNT(*) as jumlah,
            											concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM beverage $idnegaras )) * 100 ),2),'%') AS 'percentage'
            											FROM `beverage` f LEFT JOIN price_index p on f.id_price_index=p.id_price_index $idnegaras GROUP BY f.id_price_index");
                                while ($juto=mysqli_fetch_array($sql)) {
            											$total= $juto['total'];
                                  echo"
                                    <tr>
                                      <td width='40%'>";
            													if ($juto['nama_price_index'] == '') {
            														echo "Unknown";
            													}
            													echo "
                                        <a href='https://www.foodieguidances.com/pages/beverage/search/?price_index=$juto[id_price_index]'>
                                            $juto[nama_price_index]
                                        </a>
                                      </td>
                                      <td width='30%'>
                                        $juto[jumlah]
                                      </td>
                                      <td width='30%'>
                                        $juto[percentage]
                                      </td>
                                    </tr>
                                  ";
                                };
                                 ?>
            										 <tr style="border-top: 1px solid; padding: 5px 0">
            											 <td> <label>Sum</label>
            											 </td>
            										 	<td>
            										 		<label><?php echo $total; ?></label>
            										 	</td>
            											<td>
            												<label> 100% </label>
            											</td>
            										 </tr>
                               </table>
                            </div>
                          </div>

            							<div class="form-group">
                            <label class="col-sm-1 control-label">Tag</label>
                            <div class="col-sm-10">
                              :
                            </div>
                          </div>
            							<div class="form-group">
                            <label class="col-sm-1 control-label"></label>
                            <div class="col-sm-10">
                              <table width='100%'>
                                <tr>
                                  <td width='40%'>
                                    <label>tag</label>
                                  </td>
                                  <td>
                                    <label>Total</label>
                                  </td>
                                  <td>
                                    <label>Percentage</label>
                                  </td>
                                </tr>
                                <?php
            										$semua = mysqli_fetch_array(mysqli_query($koneksi, "SELECT COUNT(*) as jumlah	FROM `beverage` $idnegaras"));
                                $isi = mysqli_fetch_array(mysqli_query($koneksi, "SELECT COUNT(*) as jumlah	FROM `beverage` where id_tag != '' $idnegara "));
                              	$kosong = mysqli_fetch_array(mysqli_query($koneksi, "SELECT COUNT(*) as jumlah	FROM `beverage` where id_tag = '' $idnegara "));

            											$total= $juto['total'];
                                  echo"
                                    <tr>
                                      <td width='40%'>
                                        Yes
                                      </td>
                                      <td width='30%'>
                                        $isi[jumlah]
                                      </td>
                                      <td width='30%'>
                                        ".round($isi['jumlah']/$semua['jumlah'] * '100',2)."%
                                      </td>
                                    </tr>
            												<tr>
                                      <td width='40%'>
                                        No
                                      </td>
                                      <td width='30%'>
                                        $kosong[jumlah]
                                      </td>
                                      <td width='30%'>
                                        ".round($kosong['jumlah']/$semua['jumlah'] * '100',2)."%
                                      </td>
                                    </tr>
                                  ";
                                ;
                                 ?>
            										 <tr style="border-top: 1px solid; padding: 5px 0">
            											 <td> <label>Sum</label>
            											 </td>
            										 	<td>
            										 		<label><?php echo $semua['jumlah']; ?></label>
            										 	</td>
            											<td>
            												<label> 100% </label>
            											</td>
            										 </tr>
                               </table>
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="col-sm-1 control-label">Alcohol Serving</label>
                            <div class="col-sm-10">
                              :
                            </div>
                          </div>
            							<div class="form-group">
                            <label class="col-sm-1 control-label"></label>
                            <div class="col-sm-10">
                              <table width='100%'>
                                <tr>
                                  <td width='40%'>
                                    <label>Alcohol Serving</label>
                                  </td>
                                  <td>
                                    <label>Total</label>
                                  </td>
                                  <td>
                                    <label>Percentage</label>
                                  </td>
                                </tr>
                                <?php
            										$semua = mysqli_fetch_array(mysqli_query($koneksi, "SELECT COUNT(*) as jumlah	FROM `beverage` $idnegaras"));
                                $isi = mysqli_fetch_array(mysqli_query($koneksi, "SELECT COUNT(*) as jumlah	FROM `beverage` where type_serving = 'no' $idnegara"));
                              	$kosong = mysqli_fetch_array(mysqli_query($koneksi, "SELECT COUNT(*) as jumlah	FROM `beverage` where type_serving = 'halal' $idnegara"));
                                $unk = mysqli_fetch_array(mysqli_query($koneksi, "SELECT COUNT(*) as jumlah	FROM `beverage` where type_serving = '' $idnegara"));


                                  echo"
                                    <tr>
                                      <td width='40%'>
                                        Yes
                                      </td>
                                      <td width='30%'>
                                        $isi[jumlah]
                                      </td>
                                      <td width='30%'>
                                        ".round($isi['jumlah']/$semua['jumlah'] * '100',2)."%
                                      </td>
                                    </tr>
            												<tr>
                                      <td width='40%'>
                                        No
                                      </td>
                                      <td width='30%'>
                                        $kosong[jumlah]
                                      </td>
                                      <td width='30%'>
                                         ".round($kosong['jumlah']/$semua['jumlah'] * '100',2)."%
                                      </td>
                                    </tr>

                                    <tr>
                                      <td width='40%'>
                                        Unknown
                                      </td>
                                      <td width='30%'>
                                        $unk[jumlah]
                                      </td>
                                      <td width='30%'>
                                        ".round($unk['jumlah']/$semua['jumlah'] * '100',2)."%
                                      </td>
                                    </tr>
                                  ";
                                ;
                                 ?>
            										 <tr style="border-top: 1px solid; padding: 5px 0">
            											 <td> <label>Sum</label>
            											 </td>
            										 	<td>
            										 		<label><?php echo $semua['jumlah']; ?></label>
            										 	</td>
            											<td>
            												<label> 100% </label>
            											</td>
            										 </tr>
                               </table>
                            </div>
                          </div>

            							<div class="form-group">
                            <label class="col-sm-1 control-label">Making Methode</label>
                            <div class="col-sm-10">
                              :
                            </div>
                          </div>
            							<div class="form-group">
                            <label class="col-sm-1 control-label"></label>
                            <div class="col-sm-10">
                              <table width='100%'>
                                <tr>
                                  <td width='40%'>
                                    <label>Making Methode</label>
                                  </td>
                                  <td>
                                    <label>Total</label>
                                  </td>
                                  <td>
                                    <label>Percentage</label>
                                  </td>
                                </tr>
            										<?php
                                $sql=mysqli_query($koneksi, "SELECT * FROM making_methode");
                                $sqla=mysqli_query($koneksi, "SELECT * FROM making_methode");
            										$no=1;
                                $totalusa  = mysqli_num_rows(mysqli_query($koneksi, "SELECT id_beverage from beverage"));
                                $totalaus = "";
                                $totalausa = "";
                                while ($j=mysqli_fetch_array($sqla)) {
            											$urutana   = mysqli_num_rows(mysqli_query($koneksi, "SELECT * from beverage where making_methode like '%$j[nama_making_methode]%' $idnegara"));
            											$totalausa = $urutana + $totalausa;
                                }
                                while ($j=mysqli_fetch_array($sql)) {
            											$urutan   = mysqli_num_rows(mysqli_query($koneksi, "SELECT * from beverage where making_methode like '%$j[nama_making_methode]%' $idnegara"));
            											$totalaus = $urutan + $totalaus;
            											if ($urutan <> 0) {
            	                      echo"
            	                        <tr>
            	                          <td width='40%'>";
            														if ($j['nama_making_methode']=='') {
            															echo "Unknown";
            														}echo"
            	                            <a href='https://www.foodieguidances.com/pages/beverage/search/?making_methode[]=$j[nama_making_methode]'>
                                                $j[nama_making_methode]
                                            </a>
            	                          </td>
            	                          <td width='30%'>
            	                            $urutan
            	                          </td>
            	                          <td width='30%'>
            	                            ".round($urutan/$totalausa * '100', 2)."%
            	                          </td>
            	                        </tr>
            	                      ";
            											}

            											$no++;
                                };?>


            										 <tr style="border-top: 1px solid; padding: 5px 0">
            											 <td> <label>Sum</label>
            											 </td>
            										 	<td>
            										 		<label><?php echo $totalausa; ?></label>
            										 	</td>
            											<td>
            												<label> 100% </label>
            											</td>
            										 </tr>
                               </table>
                            </div>
                          </div>

            							<div class="form-group">
                            <label class="col-sm-1 control-label">Beverage Rating</label>
                            <div class="col-sm-10">
                              :
                            </div>
                          </div>
            							<div class="form-group">
                            <label class="col-sm-1 control-label"></label>
                            <div class="col-sm-11">
                              <table width='100%'>
                                <tr>
                                  <td width='40%'>
                                    <label>Stars</label>
                                  </td>
                                  <td>
                                    <label>Total</label>
                                  </td>
                                  <td>
                                    <label>Percentage</label>
                                  </td>
                                </tr>
                                <?php

            										$sqlaaaas = mysqli_query($koneksi, "SELECT *, round(jumlah, 1) as rating, COUNT(*) as jumlah_rating FROM `beverage` $idnegaras GROUP BY rating ORDER BY `rating` DESC");

            										$sqla=mysqli_query($koneksi, "SELECT id_beverage FROM `beverage` $idnegaras");

            											$no= mysqli_num_rows($sqla);
            											$total=$no;
                                  while ($a = mysqli_fetch_array($sqlaaaas)) {
            	                      echo"
            	                        <tr>
            	                          <td width='40%'>
            															$a[rating] Star
            	                          </td>
            	                          <td width='30%'>
            	                            $a[jumlah_rating]
            	                          </td>
            	                          <td width='30%'>
            	                            ".round($a['jumlah_rating']/$total * '100',2)."%
            	                          </td>
            	                        </tr>
            												";
            											}
                                 ?>
            										 <tr style="border-top: 1px solid; padding: 5px 0">
            											 <td> <label>Sum</label>
            											 </td>
            										 	<td>
            										 		<label><?php echo $total; ?></label>
            										 	</td>
            											<td>
            												<label> 100% </label>
            											</td>
            										 </tr>
                               </table>
                            </div>
                          </div>
                        </div>
                      </div>
                    <?php








            				} else{
                          $edit=mysqli_query($koneksi,"select * from beverage where id_beverage = '$_GET[id]'");
                          $e=mysqli_fetch_array($edit);
                    ?>
                <form action="mod/<?php echo"$mod/aksi.php?mod=$mod&ale=3&url=$_GET[url]"; ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" id="signup-daftar">
                    <input type="hidden" name="id" value="<?php echo $e['id_beverage']; ?>">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns">
                                <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                            </div>
                            <!-- panel-btns -->
                            <h4 class="panel-title">Data  <?php echo $pageTitle;?></h4>
                            <p>Silahkan lengkapi data dibawah ini.</p>
                        </div>
                        <div class="panel-body nopadding">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Member :</label>
                                <div class="col-sm-10">
                                    <select name="id_member" class="pilih" style="width: 100%;">
                                    <?php
                                        $sql=mysqli_query($koneksi,"select id_member,username from member order by username asc");
                                        while($b=mysqli_fetch_array($sql)){
											$selected = ($b['id_member'] == $e['id_member'])? "selected": "";
											echo "<option value='{$b['id_member']}' {$selected} >{$b['username']}</option>";
                                        	//echo"<option value='$b[id_member]'"; if($b['id_member']==$e['id_member']){ echo "selected";} echo ">$b[username]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Beverage Name :</label>
                                <div class="col-sm-10">
					                        <input type="text" name="nama_beverage" value="<?php echo $e['nama_beverage'];?>" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Tag :</label>
                                <div class="col-sm-10">
									<input type="text" name="tag" value="<?php echo $e['id_tag'];?>" class="form-control">
                                </div>
                            </div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Category :</label>
								<div class="col-sm-10">
									<select name="category" required class="pilih" style="width: 100%;" id="kat">
										<option value="">Select Category</option>
										<?php
											$sql=mysqli_query($koneksi,"select * from beverage_category order by nama_beverage_category asc");
											while($a=mysqli_fetch_array($sql)){
												$selected = ($a['id_beverage_category'] == $e['id_beverage_category'])? "selected": "";
												echo"<option value='{$a['id_beverage_category']}' {$selected}>{$a['nama_beverage_category']}</option>";
											}
											?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Price Index :</label>
								<div class="col-sm-10">
									<select name="price_index" required class="pilih" style="width: 100%;">
									<?php
										$sql=mysqli_query($koneksi,"select * from price_index order by nama_price_index asc");
										while($g=mysqli_fetch_array($sql)){
											$selected = ($g['id_price_index'] == $e['id_price_index'])? "selected": "";
											echo"<option value='$g[id_price_index]' {$selected}>$g[nama_price_index]</option>";
										}
										?>
									</select>
								</div>
							</div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Alcohol Serving :</label>
                                <div class="col-sm-10">
                                    <div class="radio radio-inline">
										<label>
											<input type="radio" name="alcohol" value="No" <?php if ($e['type_serving']=='No') echo "checked";?>>
											Yes
										</label>
									</div>
									<div class="radio radio-inline radio-danger">
										<label>
											<input type="radio" name="alcohol" value="Halal" <?php if ($e['type_serving']=='Halal') echo "checked";?>>
											No
										</label>
									</div>
                                </div>
                            </div>
							<div class="accordion active">Making Methode</div>
							<div class="panels show">
								<div class="form-group">
									<?php
										$sql=mysqli_query($koneksi,"select * from making_methode order by nama_making_methode asc");
										$t=1;
										$make_arr = explode('+', $e['making_methode']);
										while($s=mysqli_fetch_array($sql)){
											$cheked = in_array($s['nama_making_methode'], $make_arr)? 'checked="checked"':'';
											echo"<div class='col-md-3'>
												<div class='checkbox checkbox-inline checkbox-danger'>
										<input type='checkbox' name='making[]' {$cheked} value='$s[nama_making_methode]' id='making$t'>
													<label for='making$t'> $s[nama_making_methode]</label>
												</div>
											</div>";
											$t++;
										}
										?>
								</div>
							</div>
                        </div>
                        <!-- panel-body -->
                    </div>
                    <!-- panel -->
                    <div class="panel panel-default">
                        <div class="panel-footer">
                            <button class="btn btn-primary mr5">Simpan</button>
                            <a href="mod-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-warning">Kembali</a>
                        </div>
                        <!-- panel-footer -->
                    </div>
                    <!-- panel -->
                </form>
                <?php
                    } ?>
            </div>
            <!-- contentpanel -->
        </div>
        <!-- mainpanel -->
    </div>
    <!-- mainwrapper -->
</section>
<style media="screen">
    #country-list {
    margin: 0;
    list-style: none;
    padding: 0px;
    max-height: 155px;
    position: relative;
    overflow: auto;
    }
    .ko {
    border: 1px solid #CCC;
    padding: 5px;
    color: #9C9999;
    border-top: none;
    cursor: pointer;
    transition: 0.7s all 0s ease;
    }
    .ko:hover{
    background: #ffeed0;
    transition: 0.3s all 0s ease;
    }
</style>
<script src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".pilih").select2();
	});
</script>
<script type="text/javascript">
	$(document).ready(function(){
		var Cite = {
			url: null,
			init: function(){
				$(document).on('change', '[data-cite]', Cite.ajax);
			},
			ajax: function(){
				var $this = $(this),
					data = $this.data() || {},
					url ='<?php echo"$base";?>/config/func/ajax_'+ data.cite +'.php';

				$.post(url, {id: $this.val()}, function( success ) {
					var $target = $(data.target);

					$target.html(success);
					$target.fadeIn(2000);
				}, 'html');
			}
		};

		Cite.init();
    });
</script>
