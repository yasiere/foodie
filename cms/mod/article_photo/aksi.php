<?php
session_start();
if(!empty($_SESSION['ak_id'])){
	include "../../inc/db.php";
	include "../../inc/upload.php";
	include "../../inc/id_masking.php";
	include '../../inc/seo.php';
	include "../../../config/func/upload_gambar_bms.php";

	$mod			= $_GET['mod'];
	$tglhariini		= date("Y-m-d");
	$ukuran_maks	= 552428800;
	$error_cover 	= 0;
	$error_profil 	= 0;

	if($_GET['ale']==1){
		mysqli_query($koneksi,"DELETE FROM article_photo WHERE id_article_photo='$_GET[id]'");
		unlink("../../../assets/img/article/small_".$_GET['gambarthumb']);
		unlink("../../../assets/img/article/medium_".$_GET['gambarthumb']);
		unlink("../../../assets/img/article/big_".$_GET['gambarthumb']);
		header("Location: ../../mod-article_photo-13.htm");
	}
	
	elseif($_GET['ale']==2){
		$sql=mysqli_query($koneksi,"select id_article from article where nama_article='$_POST[nama_article]'");
		$ada=mysqli_num_rows($sql);
		if($ada==0){
			header("Location: ../../add-article_photo-13.htm");
			exit();
		}
		date_default_timezone_set("Asia/Jakarta");
		$sekarang=date("Y-m-d H:i:s");

		$ukuran_maks	= 552428800;

		$nama_cover			= $_FILES['gambar_photo']['name'];
		$ukuran_cover 		= $_FILES['gambar_photo']['size'];
		$lokasi_cover		= $_FILES['gambar_photo']['tmp_name'];
		list($width_cover, $height_cover) = getimagesize($lokasi_cover);
		if($ukuran_cover > $ukuran_maks or !preg_match("/.(gif|jpg|png)$/i", $nama_cover) or $width_cover < 800 or $height_cover < 600){
			$_SESSION['notif']     = "gambar";
			header("Location: ../../mod-article_photo-13.htm");
			exit();
		}
		else{
			$r=mysqli_fetch_array($sql);
			$cover=upload_gambar($nama_cover,$lokasi_cover,"../../../assets/img/article");
			mysqli_query($koneksi,"INSERT INTO article_photo (id_member,id_article,nama_article_photo,gambar_article_photo) VALUES('$_POST[id_member]','$r[id_article]','$_POST[photo_name]','$cover')");
			// if($_POST['feed_submit']==1){
				mysqli_query($koneksi,"INSERT INTO activity (id_member,kat_act,deskripsi,tgl) VALUES('$_POST[id_member]','article-photo','Submit new article photo','$sekarang')");
			// }
			header("Location: ../../mod-article_photo-13.htm");
		}

	}


	elseif($_GET['ale']==3){
		date_default_timezone_set("Asia/Jakarta");
		$sekarang=date("Y-m-d H:i:s");

		$ukuran_maks	= 552428800;
		if(!empty($_FILES['gambar_photo']['tmp_name'])){
			$nama_cover			= $_FILES['gambar_photo']['name'];
			$ukuran_cover 		= $_FILES['gambar_photo']['size'];
			$lokasi_cover		= $_FILES['gambar_photo']['tmp_name'];
			list($width_cover, $height_cover) = getimagesize($lokasi_cover);
			if($ukuran_cover > $ukuran_maks or !preg_match("/.(gif|jpg|png)$/i", $nama_cover) or $width_cover < 800 or $height_cover < 600){
				$_SESSION['notif']     = "gambar";
				header("Location: ../../edit-article_photo-".$_POST['id']."-13.htm");
				exit();
			}
			else{
				unlink("../../../assets/img/article/small_".$_POST['gambar_old']);
				unlink("../../../assets/img/article/medium_".$_POST['gambar_old']);
				unlink("../../../assets/img/article/big_".$_POST['gambar_old']);
				$cover=upload_gambar($nama_cover,$lokasi_cover,"../../../assets/img/article");
				mysqli_query($koneksi,"update article_photo set id_member='{$_POST['id_member']}', nama_article_photo='$_POST[photo_name]',gambar_article_photo='$cover' where id_article_photo='$_POST[id]'");
				$_SESSION['notif']     = "sukses";
				header("Location: ../../edit-article_photo-".$_POST['id']."-13.htm");
				exit();
			}
		}
		else{
			mysqli_query($koneksi,"update article_photo set id_member='{$_POST['id_member']}', nama_article_photo='$_POST[photo_name]' where id_article_photo='$_POST[id]'");
			$_SESSION['notif'] = "sukses";
			header("Location: ../../edit-article_photo-".$_POST['id']."-13.htm");
			exit();
		}
	}
}
else{
	header('location:not-found.htm');
}
?>
