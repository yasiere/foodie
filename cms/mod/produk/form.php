<section>
    <div class="mainwrapper">
        <?php include "inc/kiri.php"; ?>
         <div class="mainpanel">
             <div class="pageheader">
                <div class="media">
                    <div class="pageicon pull-left">
                        <i class="fa fa-home"></i>
                    </div>
                    <div class="media-body">
						<ul class="breadcrumb">
                            <li><a href="mod-beranda.htm"><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="mod-beranda.htm">Beranda</a></li>
                            <li>Informasi Produk</li>
                        </ul>
                        <h4>Informasi Produk</h4>
                    </div>
                </div><!-- media -->
            </div><!-- pageheader -->
            <div class="contentpanel">
				<?php if($_GET['proses']==1){ ?>
				<form action="mod/<?php echo"$mod/aksi.php?mod=$mod&ale=2&url=$_GET[url]"; ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data">
				<?php
				if(isset($_GET['info'])){
					if($_GET['info']==1){
						echo"<div class='alert alert-success'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Sukses!</strong> data berhasil disimpan.
						</div>";
					}
					elseif($_GET['info']==2){
						echo"<div class='alert alert-danger'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Kesalahan!</strong> tipe file tidak cocok, anda harus mengupload file berekstensi *.JPG.
						</div>";
					}
					else{
						echo"<div class='alert alert-danger'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Kesalahan!</strong> ukuran file terlalu besar, ukuran maksimal yang diperbolehkan adalah 300kb.
						</div>";
					}
				}
				?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-btns">
                            <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                        </div><!-- panel-btns -->
						<h4 class="panel-title">Data Produk</h4>
                        <p>Silahkan lengkapi data dibawah ini.</p>
                    </div>
                    <div class="panel-body nopadding">
                    <div class="form-group">
                           <label class="col-sm-2 control-label">Kategori Produk :</label>
                           <div class="col-sm-10">
                               <select name="id_kategori" class="pilih" required style="width: 100%;">
                                	<option value=""> -- Pilih Kategori -- </option>
                                	<? 	$kategori = mysqli_query($koneksi,"select * from kategori order by nama_kategori asc");
										while($kategorix = mysqli_fetch_array($kategori)) {
											echo "<option value='$kategorix[id_kategori]'> $kategorix[nama_kategori] </option>";		
										}
									?>
                                </select>
                           </div>
                      </div>
                      <div class="form-group">
                           <label class="col-sm-2 control-label">Nama Produk :</label>
                           <div class="col-sm-10">
                               <input type="text" name="nama_produk" class="form-control" required>
                           </div>
                       </div>
                       <div class="form-group">
                           <label class="col-sm-2 control-label">Harga :</label>
                           <div class="col-sm-10">
                               <input type="text" name="harga_produk" class="form-control" required>
                           </div>
                       </div>
                       <div class="form-group">
                            <label class="col-sm-2 control-label">Status :</label>
                            <div class="col-sm-10">
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="newitem_produk" >New Item
                                </label>
                                <label class="checkbox-inline">
	                                <input type="checkbox" name="bestseller_produk" >Best Seller
                                </label>
                                <label class="checkbox-inline">
	                                <input type="checkbox" name="musttry_produk" >Must Try
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Bahan :</label>
                            <div class="col-sm-10">
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="sugarfree_produk" >Sugar Free
                                </label>
                                <label class="checkbox-inline">
	                                <input type="checkbox" name="eggfree_produk" >Egg Free
                                </label>
                                <label class="checkbox-inline">
	                                <input type="checkbox" name="dairyfree_produk" >Dairy Free
                                </label>
                            </div>
                        </div>
                        
                       <div class="form-group">
                           <label class="col-sm-2 control-label">Detil :</label>
                           <div class="col-sm-10">
                               <textarea rows="10" class="form-control ckeditor" name="detil_produk"></textarea>
                           </div>
                       </div>
						<div class="form-group">
                            <label class="col-sm-2 control-label">Gambar 1:</label>
                        	<div class="col-sm-10">
							    <input type="file" name="fupload" title="Pilih" class="btn btn-primary"> <br> <input type="text" name="alt_produk" class="form-control" placeholder="alt Gambar 1...">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Gambar 2:</label>
                        	<div class="col-sm-10">
							    <input type="file" name="fupload2" title="Pilih" class="btn btn-primary"> <br> <input type="text" name="alt_produk2" class="form-control" placeholder="alt Gambar 2...">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Gambar 3:</label>
                        	<div class="col-sm-10">
							    <input type="file" name="fupload3" title="Pilih" class="btn btn-primary"> <br> <input type="text" name="alt_produk3" class="form-control" placeholder="alt Gambar 3...">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Gambar 4:</label>
                        	<div class="col-sm-10">
							    <input type="file" name="fupload4" title="Pilih" class="btn btn-primary"> <br> <input type="text" name="alt_produk4" class="form-control" placeholder="alt Gambar 4...">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Gambar 5:</label>
                        	<div class="col-sm-10">
							    <input type="file" name="fupload5" title="Pilih" class="btn btn-primary"> <br> <input type="text" name="alt_produk5" class="form-control" placeholder="alt Gambar 5...">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Gambar 6:</label>
                        	<div class="col-sm-10">
							    <input type="file" name="fupload6" title="Pilih" class="btn btn-primary"> <br> <input type="text" name="alt_produk6" class="form-control" placeholder="alt Gambar 6...">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Gambar 7:</label>
                        	<div class="col-sm-10">
							    <input type="file" name="fupload7" title="Pilih" class="btn btn-primary"> <br> <input type="text" name="alt_produk7" class="form-control" placeholder="alt Gambar 7...">
                            </div>
                        </div>
                        <!-- form-group -->
                    </div><!-- panel-body -->
                </div><!-- panel -->

				<div class="panel panel-default">
					<div class="panel-footer">
                        <button class="btn btn-primary mr5">Simpan</button>
                        <a href="mod-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-warning">Kembali</a>
                     </div><!-- panel-footer -->
                </div><!-- panel -->
				</form>
				<?php
				}else{
				$dat=mysqli_query($koneksi,"select * from produk p, kategori k where p.id_kategori = k.id_kategori and p.id_produk='$_GET[id]'");
				$d=mysqli_fetch_array($dat);
				?>
				<form action="mod/<?php echo"$mod/aksi.php?mod=$mod&ale=3&url=$_GET[url]"; ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php echo"$_GET[id]"; ?>" >
				<?php
				if(isset($_GET['info'])){
					if($_GET['info']==1){
						echo"<div class='alert alert-success'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Sukses!</strong> data berhasil disimpan.
						</div>";
					}
					elseif($_GET['info']==2){
						echo"<div class='alert alert-danger'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Kesalahan!</strong> tipe file tidak cocok, anda harus mengupload file berekstensi *.JPG.
						</div>";
					}
					else{
						echo"<div class='alert alert-danger'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Kesalahan!</strong> ukuran file terlalu besar, ukuran maksimal yang diperbolehkan adalah 300kb.
						</div>";
					}
				}
				?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-btns">
                            <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                        </div><!-- panel-btns -->
						<h4 class="panel-title">Detail Data Produk</h4>
                        <p>Silahkan lengkapi data dibawah ini.</p>
                    </div>
                    <div class="panel-body nopadding">
                   		 <div class="form-group">
                            <label class="col-sm-2 control-label">Nama Produk :</label>
                            <div class="col-sm-10">
                                <input type="text" name="nama_produk" class="form-control" required value="<?php echo"$d[nama_produk]"; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-2 control-label">Kategori Produk :</label>
                           <div class="col-sm-10">
                               <select name="id_kategori" class="pilih" required style="width: 100%;">
                                	<option value="<? echo $d['id_kategori']?>"> <? echo $d['nama_kategori']?> </option>
                                	<? 	$kategori = mysqli_query($koneksi,"select * from kategori where id_kategori <> '$d[id_kategori]' order by nama_kategori asc");
										while($kategorix = mysqli_fetch_array($kategori)) {
											echo "<option value='$kategorix[id_kategori]'> $kategorix[nama_kategori] </option>";		
										}
									?>
                                </select>
                           </div>
                       </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Harga Produk :</label>
                            <div class="col-sm-10">
                                <input type="text" name="harga_produk" class="form-control" value="<?php echo"$d[harga_produk]"; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Status :</label>
                            <div class="col-sm-10">
                                <label class="checkbox-inline">
                                	<? if($d['newitem_produk'] == 'on') { $checked = "checked='checked'"; } else { $checked = ""; } ?>
                                    <input type="checkbox" name="newitem_produk" <? echo $checked ?>>New Item
                                </label>
                                <label class="checkbox-inline">
	                                <? if($d['bestseller_produk'] == 'on') { $checked = "checked='checked'"; } else { $checked = ""; } ?>
    	                            <input type="checkbox" name="bestseller_produk" <? echo $checked ?>>Best Seller
                                </label>
                                <label class="checkbox-inline">
	                                <? if($d['musttry_produk'] == 'on') { $checked = "checked='checked'"; } else { $checked = ""; } ?>
    	                            <input type="checkbox" name="musttry_produk" <? echo $checked ?>>Must Try
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Bahan :</label>
                            <div class="col-sm-10">
                                <label class="checkbox-inline">
	                              <? if($d['sugarfree_produk'] == 'on') { $checked = "checked='checked'"; } else { $checked = ""; } ?>
                                  <input type="checkbox" name="sugarfree_produk" <? echo $checked ?>>Sugar Free
                                </label>
                                <label class="checkbox-inline">
    	                          <? if($d['eggfree_produk'] == 'on') { $checked = "checked='checked'"; } else { $checked = ""; } ?>
                                  <input type="checkbox" name="eggfree_produk" <? echo $checked ?>>Egg Free
                                </label>
                                <label class="checkbox-inline">
        	                      <? if($d['dairyfree_produk'] == 'on') { $checked = "checked='checked'"; } else { $checked = ""; } ?>
                                  <input type="checkbox" name="dairyfree_produk" <? echo $checked ?>>Dairy Free
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Detil Produk :</label>
                            <div class="col-sm-10">
                                <textarea rows="10" class="form-control ckeditor" name="detil_produk"><?php echo"$d[detil_produk]"; ?></textarea>
                            </div>
                        </div>
                      <div class="form-group">
                           <label class="col-sm-2 control-label">Gambar :</label>
                           <div class="col-sm-10">
                             <input type="file" name="fupload" title="Pilih" class="btn btn-primary"> <br> <br> <input type="text" name="alt_produk" class="form-control" value="<? echo $d['alt_produk']?>">
                             <br />
                             <div class="col-sm-3 table-responsive" style="text-align: center">
                             <table class="table">
                             <tr>
                             <?
							 if(!empty($d['gambar_produk'])){
               					echo "<td><img src='$base/produk/".$d['gambar_produk']."' width='300'> ";
								list($width, $height) = getimagesize("$base/produk/".$d['gambar_produk']."");
								echo "<br>";
								echo $width . " x " . $height;
								echo "</td><td><img src='$base/produk/thumb/".$d['gambar_produk']."' width='100'>";
								list($width, $height) = getimagesize("$base/produk/thumb/".$d['gambar_produk']."");
								echo "<br>";
								echo $width . " x " . $height;
								echo "</td>";
								
							 }
                             ?>
                             </tr>
                             </table>
                             </div>
                             <input type="hidden" name="gambar_produk" value="<? echo "$d[gambar_produk]";?>">
                           </div>
                      </div>
                      <div class="form-group">
                           <label class="col-sm-2 control-label">Gambar 2:</label>
                           <div class="col-sm-10">
                             <input type="file" name="fupload2" title="Pilih" class="btn btn-primary"> <br> <br> <input type="text" name="alt_produk2" class="form-control" value="<? echo $d['alt_produk2']?>">
                             <br />
                             <div class="col-sm-3 table-responsive" style="text-align: center">
                             <table class="table">
                             <tr>
                             <?
							 if(!empty($d['gambar_produk2'])){
               					echo "<td><img src='$base/produk/".$d['gambar_produk2']."' width='300'> ";
								list($width, $height) = getimagesize("$base/produk/".$d['gambar_produk2']."");
								echo "<br>";
								echo $width . " x " . $height;
								echo "</td><td><img src='$base/produk/thumb/".$d['gambar_produk2']."' width='100'>";
								list($width, $height) = getimagesize("$base/produk/thumb/".$d['gambar_produk2']."");
								echo "<br>";
								echo $width . " x " . $height;
								echo "</td>";
								
							 }
                             ?>
                             </tr>
                             </table>
                             </div>
                             <input type="hidden" name="gambar_produk2" value="<? echo "$d[gambar_produk2]";?>">
                           </div>
                      </div>
                      <div class="form-group">
                           <label class="col-sm-2 control-label">Gambar 3:</label>
                           <div class="col-sm-10">
                             <input type="file" name="fupload3" title="Pilih" class="btn btn-primary"> <br> <br> <input type="text" name="alt_produk3" class="form-control" value="<? echo $d['alt_produk3']?>">
                             <br />
                             <div class="col-sm-3 table-responsive" style="text-align: center">
                             <table class="table">
                             <tr>
                             <?
							 if(!empty($d['gambar_produk3'])){
               					echo "<td><img src='$base/produk/".$d['gambar_produk3']."' width='300'> ";
								list($width, $height) = getimagesize("$base/produk/".$d['gambar_produk3']."");
								echo "<br>";
								echo $width . " x " . $height;
								echo "</td><td><img src='$base/produk/thumb/".$d['gambar_produk3']."' width='100'>";
								list($width, $height) = getimagesize("$base/produk/thumb/".$d['gambar_produk3']."");
								echo "<br>";
								echo $width . " x " . $height;
								echo "</td>";
								
							 }
                             ?>
                             </tr>
                             </table>
                             </div>
                             <input type="hidden" name="gambar_produk3" value="<? echo "$d[gambar_produk3]";?>">
                           </div>
                      </div>
                      <div class="form-group">
                           <label class="col-sm-2 control-label">Gambar 4:</label>
                           <div class="col-sm-10">
                             <input type="file" name="fupload4" title="Pilih" class="btn btn-primary"> <br> <br> <input type="text" name="alt_produk4" class="form-control" value="<? echo $d['alt_produk4']?>">
                             <br />
                             <div class="col-sm-3 table-responsive" style="text-align: center">
                             <table class="table">
                             <tr>
                             <?
							 if(!empty($d['gambar_produk4'])){
               					echo "<td><img src='$base/produk/".$d['gambar_produk4']."' width='300'> ";
								list($width, $height) = getimagesize("$base/produk/".$d['gambar_produk4']."");
								echo "<br>";
								echo $width . " x " . $height;
								echo "</td><td><img src='$base/produk/thumb/".$d['gambar_produk4']."' width='100'>";
								list($width, $height) = getimagesize("$base/produk/thumb/".$d['gambar_produk4']."");
								echo "<br>";
								echo $width . " x " . $height;
								echo "</td>";
								
							 }
                             ?>
                             </tr>
                             </table>
                             </div>
                             <input type="hidden" name="gambar_produk4" value="<? echo "$d[gambar_produk4]";?>">
                           </div>
                      </div>
                      <div class="form-group">
                           <label class="col-sm-2 control-label">Gambar 5:</label>
                           <div class="col-sm-10">
                             <input type="file" name="fupload5" title="Pilih" class="btn btn-primary"> <br> <br> <input type="text" name="alt_produk5" class="form-control" value="<? echo $d['alt_produk5']?>">
                             <br />
                             <div class="col-sm-3 table-responsive" style="text-align: center">
                             <table class="table">
                             <tr>
                             <?
							 if(!empty($d['gambar_produk5'])){
               					echo "<td><img src='$base/produk/".$d['gambar_produk5']."' width='300'> ";
								list($width, $height) = getimagesize("$base/produk/".$d['gambar_produk5']."");
								echo "<br>";
								echo $width . " x " . $height;
								echo "</td><td><img src='$base/produk/thumb/".$d['gambar_produk5']."' width='100'>";
								list($width, $height) = getimagesize("$base/produk/thumb/".$d['gambar_produk5']."");
								echo "<br>";
								echo $width . " x " . $height;
								echo "</td>";
								
							 }
                             ?>
                             </tr>
                             </table>
                             </div>
                             <input type="hidden" name="gambar_produk5" value="<? echo "$d[gambar_produk5]";?>">
                           </div>
                      </div>
                      <div class="form-group">
                           <label class="col-sm-2 control-label">Gambar 6:</label>
                           <div class="col-sm-10">
                             <input type="file" name="fupload6" title="Pilih" class="btn btn-primary"> <br> <br> <input type="text" name="alt_produk6" class="form-control" value="<? echo $d['alt_produk6']?>">
                             <br />
                             <div class="col-sm-3 table-responsive" style="text-align: center">
                             <table class="table">
                             <tr>
                             <?
							 if(!empty($d['gambar_produk6'])){
               					echo "<td><img src='$base/produk/".$d['gambar_produk6']."' width='300'> ";
								list($width, $height) = getimagesize("$base/produk/".$d['gambar_produk6']."");
								echo "<br>";
								echo $width . " x " . $height;
								echo "</td><td><img src='$base/produk/thumb/".$d['gambar_produk6']."' width='100'>";
								list($width, $height) = getimagesize("$base/produk/thumb/".$d['gambar_produk6']."");
								echo "<br>";
								echo $width . " x " . $height;
								echo "</td>";
								
							 }
                             ?>
                             </tr>
                             </table>
                             </div>
                             <input type="hidden" name="gambar_produk6" value="<? echo "$d[gambar_produk6]";?>">
                           </div>
                      </div>
                      <div class="form-group">
                           <label class="col-sm-2 control-label">Gambar 7:</label>
                           <div class="col-sm-10">
                             <input type="file" name="fupload7" title="Pilih" class="btn btn-primary"> <br> <br> <input type="text" name="alt_produk7" class="form-control" value="<? echo $d['alt_produk7']?>">
                             <br />
                             <div class="col-sm-3 table-responsive" style="text-align: center">
                             <table class="table">
                             <tr>
                             <?
							 if(!empty($d['gambar_produk7'])){
               					echo "<td><img src='$base/produk/".$d['gambar_produk7']."' width='300'> ";
								list($width, $height) = getimagesize("$base/produk/".$d['gambar_produk7']."");
								echo "<br>";
								echo $width . " x " . $height;
								echo "</td><td><img src='$base/produk/thumb/".$d['gambar_produk7']."' width='100'>";
								list($width, $height) = getimagesize("$base/produk/thumb/".$d['gambar_produk7']."");
								echo "<br>";
								echo $width . " x " . $height;
								echo "</td>";
								
							 }
                             ?>
                             </tr>
                             </table>
                             </div>
                             <input type="hidden" name="gambar_produk7" value="<? echo "$d[gambar_produk7]";?>">
                           </div>
                      </div>
                    </div><!-- panel-body -->
                </div><!-- panel -->

				<div class="panel panel-default">
					<div class="panel-footer">
                        <button class="btn btn-primary mr5">Simpan</button>
                        <a href="mod-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-warning">Kembali</a>
                     </div><!-- panel-footer -->
                </div><!-- panel -->
				</form>
				<?php } ?>
			</div><!-- contentpanel -->
        </div><!-- mainpanel -->
    </div><!-- mainwrapper -->
</section>

<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/pace.min.js"></script>
<script src="js/retina.min.js"></script>
<script src="js/jquery.cookies.js"></script>

<script src="js/select2.min.js"></script>
<script src="js/bootstrap-timepicker.min.js"></script>
<script src="js/bootstrap.file-input.js"></script>
<script src="js/custom.js"></script>

<script type="text/javascript">
$(document).ready(function() {
  $(".pilih").select2();
});
</script>

<script>

jQuery(document).ready(function() {
	$('input[type=file]').bootstrapFileInput();
	jQuery('.select-search-hide').select2({
        minimumResultsForSearch: -1
    });
	jQuery('#cekin').timepicker({
		showMeridian: false,
		<?php
		if($s['cek_in']<>""){
			echo"defaultTime:'$s[cek_in]'";
		}
		else{
			echo"defaultTime:'12:00'";
		}
		?>
	});
	jQuery('#cekout').timepicker({
		showMeridian: false,
		<?php
		if($s['cek_out']<>""){
			echo"defaultTime:'$s[cek_out]'";
		}
		else{
			echo"defaultTime:'13:00'";
		}
		?>
	});
	$("#provinsi").change(function(){
		var id = $("#provinsi").val();
		$.ajax({
			type:"POST",
			url: "$base/ajax/provinsi.php",
			data: "id=" + id,
			success: function(data){
				$("#kota").html(data);
				$("#kota").fadeIn(2000);
			}
		});
	});
	$("#kota").change(function(){
		var id = $("#kota").val();
		$.ajax({
			type:"POST",
			url: "$base/ajax/kota.php",
			data: "id=" + id,
			success: function(data){
				$("#kecamatan").html(data);
				$("#kecamatan").fadeIn(2000);
			}
		});
	});
});
</script>
