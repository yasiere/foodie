<?php
session_start();
if(!empty($_SESSION['ak_id'])){
	include "../../inc/db.php";
	include "../../inc/upload.php";
	include "../../inc/id_masking.php";
	include '../../inc/seo.php';
	include "../../../config/func/upload_gambar_bms.php";

	$mod			= $_GET['mod'];
	$tglhariini		= date("Y-m-d");
	$ukuran_maks	= 552428800;
	$error_cover 	= 0;
	$error_profil 	= 0;

	if($_GET['ale']==1){
		mysqli_query($koneksi,"DELETE FROM food WHERE id_food='$_GET[id]'");
		unlink("../../../assets/img/food/small_".$_GET['gambarthumb']);
		unlink("../../../assets/img/food/medium_".$_GET['gambarthumb']);
		unlink("../../../assets/img/food/big_".$_GET['gambarthumb']);
		header("Location: ../../mod-food-6.htm");
	}
	elseif($_GET['ale']==2){

		$sql=mysqli_query($koneksi,"select id_restaurant from restaurant where id_restaurant='$_POST[restaurant]'");
		$ada=mysqli_num_rows($sql);
		if($ada==0){
			header("Location: ".$base_url."/".$_POST['username']."/submit/error-resto");
			exit();
		}
		else{
			date_default_timezone_set("Asia/Jakarta");
			$sekarang=date("Y-m-d H:i:s");

			$ukuran_maks	= 552428800;

			$nama_cover			= $_FILES['gambar_food']['name'];
			$ukuran_cover 		= $_FILES['gambar_food']['size'];
			$lokasi_cover		= $_FILES['gambar_food']['tmp_name'];
			list($width_cover, $height_cover) = getimagesize($lokasi_cover);
			if($ukuran_cover > $ukuran_maks or !preg_match("/.(gif|jpg|png)$/i", $nama_cover) or $width_cover < 800 or $height_cover < 600){
				$_SESSION['notif']     = "gambar";
				header("Location: ".$base_url."/".$username."/my-food/new");
				exit();
			}
			else{
				if(!empty($_POST['cooking'])){
					$cooking_seo = $_POST['cooking'];
					$cooking=implode('+',$cooking_seo);
				}
				else {
					$cooking="";
				}


				if(!empty($_POST['cuisine'])){
					$cu_seo = $_POST['cuisine'];
					$cuisine=implode('+',$cu_seo);
				}
				else {
					$cuisine="";
				}
				$r=mysqli_fetch_array($sql);
				$sql=mysqli_query($koneksi,"INSERT INTO food (id_member,id_restaurant,nama_food,id_food_category,id_price_index,id_cuisine,cooking_methode,id_msg_level,tgl_posta,id_tag,type_serving) VALUES('$_POST[id_member]','$r[id_restaurant]','$_POST[food_name]','$_POST[category]','$_POST[price_index]','$cuisine','$cooking','$_POST[msg_level]','$sekarang','$_POST[tag]','$_POST[pork]')");
				if($sql){
					$cover=upload_gambar($nama_cover,$lokasi_cover,"../../../assets/img/food");
					$id_food=mysqli_insert_id($koneksi);
					mysqli_query($koneksi,"INSERT INTO food_photo (id_member,id_food,nama_food_photo,gambar_food_photo) VALUES('$_POST[id_member]','$id_food','$_POST[food_name]','$cover')");
					mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('Food','$id_food')");
					// if($_POST['feed_submit']==1){
						mysqli_query($koneksi,"INSERT INTO activity (id_member,kat_act,deskripsi,tgl) VALUES('$_POST[id_member]','feed_submit','Submit new food review','$sekarang')");
					// }
					if(!empty($_POST['rating']) or !empty($_POST['cleanlines']) or !empty($_POST['flavor']) or !empty($_POST['freshness']) or !empty($_POST['cooking']) or !empty($_POST['aroma']) or !empty($_POST['serving'])){
						if(!empty($_POST['rating'])){
							$r1=$_POST['rating'] * 0.21;
							$r2=$_POST['rating'] * 0.20;
							$r3=$_POST['rating'] * 0.18;
							$r4=$_POST['rating'] * 0.17;
							$r5=$_POST['rating'] * 0.15;
							$r6=$_POST['rating'] * 0.09;
						}
						else{
							$r1=$_POST['cleanlines'] * 0.21;
							$r2=$_POST['flavor'] * 0.20;
							$r3=$_POST['freshness'] * 0.18;
							$r4=$_POST['cooking'] * 0.17;
							$r5=$_POST['aroma'] * 0.15;
							$r6=$_POST['serving'] * 0.09;
						}
						mysqli_query($koneksi,"INSERT INTO food_rating (id_food,id_member,cleanlines,flavor,freshness,cooking,aroma,serving,tgl_food_rating) values('$id_food','$_POST[id_member]','$r1','$r2','$r3','$r4','$r5','$r6','$sekarang')");
					}
					$id = id_masking($id_food);
					header("Location: ../../mod-food-6.htm");
					exit();
				}
			}
		}

	}



	elseif($_GET['ale']==3){
		date_default_timezone_set("Asia/Jakarta");
		$sekarang=date("Y-m-d H:i:s");

		$cook_seo = $_POST['cooking_methode'];
		if(!empty($cook_seo)){
			$cooking=implode('+',$cook_seo);
		} else {
			# code...
		}

		$cuisine_seo = $_POST['cuisine'];
		if (!empty($cuisine_seo)) {
			$cuisine=implode('+',$cuisine_seo);
		}

		mysqli_query($koneksi,"update food set nama_food='$_POST[food_name]',id_food_category='$_POST[category]',id_price_index='$_POST[price_index]',id_cuisine='$cuisine',cooking_methode='$cooking',id_msg_level='$_POST[msg_level]', id_tag='$_POST[tag]', type_serving='$_POST[pork]' where id_food='$_POST[id]'");
		$_SESSION['notif']     = "sukses";
		$id = id_masking($_POST['id']);
		$slug=seo($_POST['food_name']);
		//header("Location: ".$base_url."/".$u['username']."/my-food/edit/".$_POST['id']);
		header("Location: ../../mod-food-6.htm");
		exit();
	}
}
else{
	header('location:not-found.htm');
}
?>
