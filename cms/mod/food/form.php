<style>
html, body {
	height: 100%;
	margin: 0;
	padding: 0;
}
#map {
	width: 100%;
	height: 400px;
}
.controls {
	margin-top: 10px;
	border: 1px solid transparent;
	border-radius: 2px 0 0 2px;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	height: 32px;
	outline: none;
	box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
}
#searchInput {
	background-color: #fff;
	font-family: Roboto;
	font-size: 15px;
	font-weight: 300;
	margin-left: 12px;
	padding: 0 11px 0 13px;
	text-overflow: ellipsis;
	width: 50%;
}
#searchInput:focus {
 	border-color: #4d90fe;
}

</style>

<section>
    <div class="mainwrapper">
        <?php include "inc/kiri.php"; ?>
         <div class="mainpanel">
             <div class="pageheader">
                <div class="media">
                    <div class="pageicon pull-left">
                        <i class="fa fa-home"></i>
                    </div>
                    <div class="media-body">
						<ul class="breadcrumb">
                            <li><a href="mod-beranda.htm"><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="mod-beranda.htm">Beranda</a></li>
                            <li>Informasi Food</li>
                        </ul>
                        <h4>Informasi Food</h4>
                    </div>
                </div><!-- media -->
            </div><!-- pageheader -->
            <div class="contentpanel">
				<?php if($_GET['proses']==1){ ?>

					<?php
					if(isset($_GET['info'])){
						if($_GET['info']==1){
							echo"<div class='alert alert-success'>
								<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
								<strong>Sukses!</strong> data berhasil disimpan.
							</div>";
						}
						elseif($_GET['info']==2){
							echo"<div class='alert alert-danger'>
								<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
								<strong>Kesalahan!</strong> tipe file tidak cocok, anda harus mengupload file berekstensi *.JPG.
							</div>";
						}
						else{
							echo"<div class='alert alert-danger'>
								<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
								<strong>Kesalahan!</strong> ukuran file terlalu besar, ukuran maksimal yang diperbolehkan adalah 300kb.
							</div>";
						}
					}
					?>
	        <form action="mod/<?php echo"$mod/aksi.php?mod=$mod&ale=2&url=$_GET[url]"; ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" id="signup-daftar">
          <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-btns">
                  <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                </div><!-- panel-btns -->
		            <h4 class="panel-title">Data food</h4>
                <p>Silahkan lengkapi data dibawah ini.</p>
            </div>
            <div class="panel-body nopadding">
              	<div class="form-group">
                  <label class="col-sm-2 control-label">Member :</label>
                  <div class="col-sm-10">
                    <select name="id_member" class="pilih" style="width: 100%;">
                    <?php
                    	$sql=mysqli_query($koneksi,"select id_member,username from member order by username asc");
                    	while($b=mysqli_fetch_array($sql)){
                    		echo"<option value='$b[id_member]'>$b[username]</option>";
                    	}
                    ?>
                    </select>
                  </div>
                </div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Country :</label>
									<div class="col-sm-10">
										<select name="negara" class="pilih" style="width: 100%;" id="negara">
										<option value="">Select Country</option>
											<?php
													$sql=mysqli_query($koneksi,"select id_negara,nama_negara from negara order by nama_negara asc");
													while($b=mysqli_fetch_array($sql)){
														echo"<option value='$b[id_negara]'>$b[nama_negara]</option>";
													}
												?>
										 </select>
									</div>
								</div>
								<div class="form-group">
									 <label class="col-sm-2 control-label">State/Province :</label>
									 <div class="col-sm-10">
											 <select name="propinsi" class="pilih" style="width: 100%;" id="propinsi">
													<option value="">Select State/ Province</option>
												</select>
									 </div>
								</div>
								<div class="form-group">
									 <label class="col-sm-2 control-label">City :</label>
									 <div class="col-sm-10">
											 <select name="kota" class="pilih" style="width: 100%;" id="kota">
													<option value="">Select City</option>
												</select>
									 </div>
								</div>
								<div class="form-group">
									 <label class="col-sm-2 control-label">Restaurant :</label>
									 <div class="col-sm-10">
											 <select name="restaurant" class="pilih" style="width: 100%;" id="search">
													<option value="">Select Restaurant</option>
												</select>
									 </div>
								</div>
                <div class="form-group">
                   <label class="col-sm-2 control-label">Food Name :</label>
                   <div class="col-sm-10">
                       <input type="text" name="food_name" class="form-control" id="username">
                   </div>
                </div>
								<div class="form-group">
									 <label class="col-sm-2 control-label">Category :</label>
									 <div class="col-sm-10">
										 <select name="category" required class="pilih" style="width: 100%;" id="kat">
											<option value="">Select Category</option>
											<?php
												$sql=mysqli_query($koneksi,"select * from food_category order by nama_food_category asc");
												while($a=mysqli_fetch_array($sql)){
													echo"<option value='$a[id_food_category]'>$a[nama_food_category]</option>";
												}
											?>
										</select>
									 </div>
								</div>
								<div class="form-group">
									 <label class="col-sm-2 control-label">Price Index :</label>
									 <div class="col-sm-10">
										<select name="price_index" required class="pilih" style="width: 100%;">
											<option>Select Price Index</option>
											<?php
												$sql=mysqli_query($koneksi,"select * from price_index order by nama_price_index asc");
												while($g=mysqli_fetch_array($sql)){
													echo"<option value='$g[id_price_index]'>$g[nama_price_index]</option>";
												}
											?>
										</select>
									 </div>
								</div>
								<div class="form-group">
									 <label class="col-sm-2 control-label">MSG Level :</label>
									 <div class="col-sm-10">
										<select name="msg_level" class="pilih" style="width: 100%;">
 											<option>Select MSG Level</option>
 											<?php
	 											$sql=mysqli_query($koneksi,"select * from msg_level order by nama_msg_level asc");
	 											while($d=mysqli_fetch_array($sql)){
	 												echo"<option value='$d[id_msg_level]'>$d[nama_msg_level]</option>";
	 											}
 											?>
 										</select>
									 </div>
								</div>
								<div class="form-group">
                   <label class="col-sm-2 control-label">Tag :</label>
                   <div class="col-sm-10">
                      <input type="text" class="form-control" placeholder="Write tag (separated by comma)" name="tag">
                   </div>
                </div>
								<div class="form-group">
										<label class="col-sm-2 control-label">Pork Serving :</label>
										<div class="col-sm-10">
											<div class="radio radio-inline">
												<input type="radio" checked="" name="pork" value="Yes" id="pork1">
												Yes
											</div>
											<div class="radio radio-inline radio-danger">
												<input type="radio" name="pork" value="No" id="pork2">
												No
											</div>
											<div class="radio radio-inline radio-danger">
												<input type="radio" name="pork" value="Halal" id="pork3">
												Halal
											</div>
											<div class="radio radio-inline radio-danger">
												<input type="radio" name="pork" value="vege" id="pork4">
												Vegetable
											</div>
										</div>
								</div>
                <div class="form-group">
                   <label class="col-sm-2 control-label">Cover Photo :</label>
                   <div class="col-sm-10">
                     <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-preview upload" data-trigger="fileinput"></div>
                        <input type="file" name="gambar_food" class="hidden">
                      </div>
                      <p class="help-block">Image used have to be in JPG, PNG, and GIF format and the maximum total images size is 50MB also minimal dimension must be 800x600 pixel.</p>
                   </div>
                </div>
								<div class="accordion">Cuisine</div>
								<div class="panels">
									<div class="form-group">
									 <?php
											$sql=mysqli_query($koneksi,"select * from cuisine order by nama_cuisine asc");
											$t=1;
											while($c=mysqli_fetch_array($sql)){
												echo"<div class='col-md-5'>
													<div class='checkbox checkbox-inline checkbox-danger'>
														<input type='checkbox' name='cuisine[]' value='$c[nama_cuisine]' id='cuisine$t'>
														<label for='cuisine$t'> $c[nama_cuisine]</label>
													</div>
												</div>";
												$t++;
											}
											?>
									</div>
								</div>

                <div class="accordion">Serving</div>
								<div class="panels">
									<div class="form-group">
										<?php
											$sql=mysqli_query($koneksi,"select * from cooking_methode order by nama_cooking_methode asc");
											$t=1;
											while($s=mysqli_fetch_array($sql)){
												echo"<div class='col-md-5'>
													<div class='checkbox checkbox-inline checkbox-danger'>
														<input type='checkbox' name='cooking[]' value='$s[nama_cooking_methode]' id='cooking$t'>
														<label for='cooking$t'> $s[nama_cooking_methode]</label>
													</div>
												</div>";
												$t++;
											}
										?>
									</div>
								</div>
                <div class="accordion">Food Rate</div>
									<div class="panels">
									 <div role="tabpanel" id="tab-vote">
											<!-- Nav tabs -->
											<ul class="nav nav-tabs" role="tablist">
												<li role="presentation" class="active"><a href="#feature" aria-controls="feature" role="tab" data-toggle="tab">Feature Vote</a></li>
												<li role="presentation"><a href="#quick" aria-controls="quick" role="tab" data-toggle="tab">Quick Vote</a></li>
											</ul>
											<!-- Tab panes -->
											<div class="tab-content">
												<div role="tabpanel" class="tab-pane tab-panel active fade in" id="feature">
													<table width="100%">
														<tr>
															<td>Cleanliness <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall cleanliness of the food and plates served, which can include whether the food has unwanted insect, plastic, hair or unclean plates, bowls and utensils used."></i></td>
															<td><input type="hidden" class="rating" id="rat1" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="cleanlines"></td>
														</tr>
														<tr>
															<td>Flavour <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall taste of the food which has unique personality."></i></td>
															<td><input type="hidden" class="rating" id="rat2" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="flavor"></td>
														</tr>
														<tr>
															<td>Freshness <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment on the food ingredient's freshness which can include meat, seafood, vegetables and sauces."></i></td>
															<td><input type="hidden" class="rating" id="rat3" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="freshness"></td>
														</tr>
														<tr>
															<td>Cooking <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment of the way the food is being cooked wheither it is undercooked or overcooked. However, it also depend on how we like the food to be cook."></i></td>
															<td><input type="hidden" class="rating" id="rat4" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="cooking"></td>
														</tr>
														<tr>
															<td>Presentasion &amp; Aroma <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment on the food's presentation and its aroma. A good presentation on food will increase our appetite for the food. likewise for the aroma too. A fragrance food cook meters away can attract our sense of smell. Good food makes better with great presentation and aroma."></i></td>
															<td><input type="hidden" class="rating" id="rat5" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="aroma"></td>
														</tr>
														<tr>
															<td>Serving <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Is the food being served having adequate serving? Too much or too little? Unlike Western cuisine which have courses of food for a complete meal, Eastern cuisine are more direct and usually treat as the main course. Therefore, the serving is vary."></i></td>
															<td><input type="hidden" class="rating" id="rat6" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="serving"></td>
														</tr>
													</table>
												</div>
												<div role="tabpanel" class="tab-pane tab-panel fade" id="quick">
													<table width="100%">
														<tr>
															<td><strong>Give your vote</strong></td>
															<td>
									            <input type="hidden" class="rating" id="rat6" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" value="" name="rating">
									          </td>
														</tr>
													</table>
												</div>
											</div>
									      <ul class="list-inline">
													<li><i class="fa fa-star"></i> Poor</li>
													<li><i class="fa fa-star"></i><i class="fa fa-star"></i> Below Average</li>
													<li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Average</li>
													<li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Very Good</li>
													<li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Excellent</li>
												</ul>
										</div>
									</div>
						</div><!-- panel-body -->
					</div><!-- panel -->
  				<div class="panel panel-default">
  					<div class="panel-footer">
              <button class="btn btn-primary mr5">Simpan</button>
              <a href="mod-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-warning">Kembali</a>
            </div><!-- panel-footer -->
      		</div><!-- panel -->
				</form>















				<?php }
				elseif ($_GET['proses']==3) {
          ?>
          <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-btns">
                    <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                </div><!-- panel-btns -->
                <h4 class="panel-title">Data Member</h4>
                <p>Silahkan lengkapi data dibawah ini.</p>
            </div>
            <div class="panel-body nopadding">
				<div class="form-group">
					<label class="col-sm-1 control-label">Total</label>
					<div class="col-sm-10">
					  <div class="col-sm-10 control-label"> :
					    <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT id_food FROM `food`")); echo "$juto"; ?>
					  </div>
					</div>
				</div>
				<div class="form-group">
	                <label class="col-sm-1 control-label">Country</label>
	                <div class="col-sm-10">
	                  <div class="col-sm-10 control-label"> :
	                    <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT n.nama_negara,n.id_negara, COUNT(*) FROM food m JOIN negara n ON m.negara = n.id_negara GROUP BY n.nama_negara")); echo "$juto"; ?>
	                  </div>
	                </div>
	            </div>

				<div class="form-group">
				<label class="col-sm-1 control-label"></label>
				<div class="col-sm-10">
									<select name="country-list" class="pilih" style="width: 200px" onchange="document.location.href=this.value">
										<option value="0">Select Country</option>
										<?php
											$idnegara = "";
											if (!empty($_GET['negara'])) {
												$idnegara = "AND id_negara='$_GET[negara]'";
											}

											$sql = mysqli_query($koneksi, "SELECT n.nama_negara,n.id_negara, COUNT(*) FROM food m JOIN negara n ON m.negara = n.id_negara GROUP BY n.nama_negara");
											while ($a = mysqli_fetch_array($sql)) {
												if ($_GET['negara']==$a['id_negara']) {
													echo "<option value='detail-food-3-country$a[id_negara].htm' selected>$a[nama_negara]</option>";
												}
												else {
													echo "<option value='detail-food-3-country$a[id_negara].htm'>$a[nama_negara]</option>";
												}

											}
										?>
									</select>

				  <table width='100%'>
				    <tr>
				      <td width='40%'>
				        <label>Country</label>
				      </td>
				      <td>
				        <label>Total</label>
				      </td>
				      <td>
				        <label>Percentage</label>
				      </td>
				    </tr>

				    <?php
				    $sql=mysqli_query($koneksi, "SELECT *, COUNT(*) as total,
										(SELECT COUNT(*) FROM food) as total, n.id_negara, COUNT(*) as jumlah ,
										concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM food )) * 100 ),2),'%') AS 'percentage'
										FROM `food` f LEFT JOIN negara n on f.negara=n.id_negara GROUP by negara");
										$jumsql=mysqli_num_rows($sql);
				    while ($juto=mysqli_fetch_array($sql)) {
											$total= $juto['total'];
				      echo"
				        <tr>
				          <td width='30%'>
				            <span style='width: 40px;display: inline-block;'>$juto[id_negara]</span> 
				            <a href='https://www.foodieguidances.com/pages/food/search/?country=$juto[id_negara]'>
				            	$juto[nama_negara]
				            </a>
				          </td>
				          <td width='30%'>
				            $juto[jumlah]
				          </td>
				          <td width='30%'>
				            $juto[percentage]
				          </td>
				        </tr>
				      ";
				    };

				     ?>
						<tr style="border-top: 1px solid; padding: 5px 0">
							<td> <label>Sum</label>
							</td>
								<td>
									<label><?php echo $total; ?></label>
								</td>
							<td>
								<label> 100% </label>
							</td>
						</tr>
				   </table>
				</div>
				</div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Total propinsi</label>
                <div class="col-sm-10">
									<?php
										$idnegara = "";
										$idnegaras = "";
										if (!empty($_GET['negara'])) {
											$idnegara = "AND m.negara='$_GET[negara]'";
											$idnegaras = "WHERE negara='$_GET[negara]'";
										}
									?>
                  :  <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT n.nama_propinsi,n.id_propinsi, COUNT(*) FROM food m JOIN propinsi n ON m.propinsi = n.id_propinsi $idnegara GROUP BY n.id_propinsi")); echo "$juto"; ?>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Propinsi</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>

                    <?php
                    $sql=mysqli_query($koneksi, "SELECT ne.id_negara, ne.nama_negara, n.nama_propinsi,(SELECT COUNT(*) FROM food $idnegaras) as total, 
                    	n.id_propinsi,COUNT(*) as jumlah, concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM food )) * 100 ),2),'%') AS 'percentage' 
                    	FROM food m 
                    	LEFT JOIN negara ne ON m.negara   =  ne.id_negara
                    	LEFT JOIN propinsi n ON m.propinsi = n.id_propinsi 
                    	$idnegara GROUP BY n.nama_propinsi");
                    while ($juto=mysqli_fetch_array($sql)) {
											$total= $juto['total'];
                      echo"
                        <tr>
                          <td width='40%'>
                            <span style='width: 40px;display: inline-block;'>$juto[id_propinsi]</span> 
                            <a href='https://www.foodieguidances.com/pages/food/search/?country=$juto[id_negara]&state=$juto[id_propinsi]'>
                            	<span style='width: 40px;display: inline-block;'>$juto[id_propinsi]</span> $juto[nama_negara] - $juto[nama_propinsi]
                            </a>
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $total; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Total City</label>
                <div class="col-sm-10">
									<?php
										$idnegara = "";
										$idnegaras = "";
										if (!empty($_GET['negara'])) {
											$idnegara = "AND negara='$_GET[negara]'";
											$idnegaras = "WHERE negara='$_GET[negara]'";
										}
									?>
                  :  <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT 
                  	ne.id_negara, ne.nama_negara,
                  	n.nama_kota,n.id_kota, 
                  COUNT(*) FROM food m 
                  LEFT JOIN negara ne ON m.negara   =  ne.id_negara
                  LEFT JOIN propinsi pr ON m.propinsi = pr.id_propinsi 
                  LEFT JOIN kota n ON m.kota = n.id_kota $idnegara GROUP BY n.id_kota")); echo "$juto"; ?>
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>City</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
                    <?php
                    $sql=mysqli_query($koneksi, "SELECT pr.id_propinsi, pr.nama_propinsi,  ne.id_negara, ne.nama_negara, n.nama_kota,(SELECT COUNT(*) FROM food $idnegaras) as total,n.id_kota,COUNT(*) as jumlah , 
                    	concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM food )) * 100 ),2),'%') AS 'percentage' 
                    	FROM food m
                    	LEFT JOIN negara ne ON m.negara   =  ne.id_negara
                 		LEFT JOIN propinsi pr ON m.propinsi = pr.id_propinsi
                    	LEFT JOIN kota n ON m.kota = n.id_kota 
                    	$idnegara GROUP BY n.nama_kota");
                    while ($juto=mysqli_fetch_array($sql)) {
											$total= $juto['total'];
                      echo"
                        <tr>
                          <td width='40%'>
                            <a href='https://www.foodieguidances.com/pages/food/search/?country=$juto[id_negara]&state=$juto[id_propinsi]&city=$juto[id_kota]'>
                            	<span style='width: 40px;display: inline-block;'>$juto[id_kota]</span> $juto[nama_negara] - $juto[nama_kota]
                            </a>
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $total; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Category</label>
                <div class="col-sm-10">
									<?php
										$idnegara = "";
										$idnegaras = "";
										if (!empty($_GET['negara'])) {
											$idnegara = "AND negara='$_GET[negara]'";
											$idnegaras = "WHERE negara='$_GET[negara]'";
										}
									?>
                  :  <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT  COUNT(*) FROM food $idnegaras GROUP BY id_food_category")); echo "$juto"; ?>
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Category</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
                    <?php
                    $sql=mysqli_query($koneksi, "SELECT n.id_food_category, n.nama_food_category,
											(SELECT COUNT(*) FROM food $idnegaras) as total, COUNT(*) as jumlah,
											concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM food )) * 100 ),2),'%') AS 'percentage'
											FROM food m JOIN food_category n ON m.id_food_category = n.id_food_category $idnegaras GROUP BY n.nama_food_category");
                    while ($juto=mysqli_fetch_array($sql)) {
											$total= $juto['total'];
                      echo"
                        <tr>
                          <td width='40%'>
                          	<a href='https://www.foodieguidances.com/pages/food/search/?food_category=$juto[id_food_category]'>
                            	$juto[nama_food_category]
                            </a>
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $total; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Price Index</label>
                <div class="col-sm-10">
									<?php
										$idnegara = "";
										$idnegaras = "";
										if (!empty($_GET['negara'])) {
											$idnegara = "WHERE negara='$_GET[negara]'";
											$idnegaras = "WHERE f.negara='$_GET[negara]'";
										}
									?>
                  :  <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT  COUNT(*) FROM food $idnegara GROUP BY id_price_index")); echo "$juto"; ?>
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Price Index</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
                    <?php
                    $sql=mysqli_query($koneksi, "SELECT p.id_price_index, p.nama_price_index,
											(SELECT COUNT(*) FROM food $idnegara) as total, COUNT(*) as jumlah,
											concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM food $idnegara)) * 100 ),2),'%') AS 'percentage'
											FROM `food` f LEFT JOIN price_index p on f.id_price_index=p.id_price_index $idnegaras GROUP BY f.id_price_index");
                    while ($juto=mysqli_fetch_array($sql)) {
											$total= $juto['total'];
                      echo"
                        <tr>
                          <td width='40%'>";
													if ($juto['nama_price_index'] == '') {
														echo "Unknown";
													}
													echo "
							<a href='https://www.foodieguidances.com/pages/food/search/?price_index=$juto[id_price_index]'>
                            	$juto[nama_price_index]
                            </a>
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $total; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">MSG Level</label>
                <div class="col-sm-10">
                  :  <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT  COUNT(*) FROM food GROUP BY id_msg_level")); echo "$juto"; ?>
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>MSG Level</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras = "";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE negara='$_GET[negara]'";
												$idnegaras = "WHERE f.negara='$_GET[negara]'";
											}
										?>
                    <?php
                    $sql=mysqli_query($koneksi, "SELECT p.id_msg_level, p.nama_msg_level,
											(SELECT COUNT(*) FROM food $idnegara) as total, COUNT(*) as jumlah,
											concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM food $idnegara)) * 100 ),2),'%') AS 'percentage'
											FROM `food` f LEFT JOIN msg_level p on f.id_msg_level=p.id_msg_level $idnegaras GROUP BY f.id_msg_level");
                    while ($juto=mysqli_fetch_array($sql)) {
											$total= $juto['total'];
                      echo"
                        <tr>
                          <td width='40%'>";
													if ($juto['nama_msg_level'] == '') {
														echo "Unknown";
													}
													echo "
                            <a href='https://www.foodieguidances.com/pages/food/search/?price_index=$juto[id_msg_level]'>
                            	$juto[nama_msg_level]
                            </a>
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $total; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Tag</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>tag</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>

										<?php
											$idnegara = "";
											$idnegaras = "";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE negara='$_GET[negara]'";
												$idnegaras = "AND negara='$_GET[negara]'";
											}
										?>

                    <?php
										$semua = mysqli_fetch_array(mysqli_query($koneksi, "SELECT COUNT(*) as jumlah	FROM `food` $idnegara"));
                    $isi = mysqli_fetch_array(mysqli_query($koneksi, "SELECT COUNT(*) as jumlah	FROM `food` where id_tag != '' $idnegaras"));
                  	$kosong = mysqli_fetch_array(mysqli_query($koneksi, "SELECT COUNT(*) as jumlah	FROM `food` where id_tag = '' $idnegaras"));
											$total= $semua['jumlah'] + $isi['jumlah'] + $kosong['jumlah'];
                      echo"
                        <tr>
                          <td width='40%'>
                            Yes
                          </td>
                          <td width='30%'>
                            $isi[jumlah]
                          </td>
                          <td width='30%'>
                            ".round($isi['jumlah']/$total * '100',2)."%
                          </td>
                        </tr>
												<tr>
                          <td width='40%'>
                            No
                          </td>
                          <td width='30%'>
                            $kosong[jumlah]
                          </td>
                          <td width='30%'>
                            ".round($kosong['jumlah']/$total * '100',2)."%
                          </td>
                        </tr>
                      ";
                    ;
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $semua['jumlah']; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Pork Serving</label>
                <div class="col-sm-10">
                  :  <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT  COUNT(*) FROM food GROUP BY id_msg_level")); echo "$juto"; ?>
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Pork Serving</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras = "";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE negara='$_GET[negara]'";
												$idnegaras = "AND negara='$_GET[negara]'";
											}
										?>
                    <?php
                    $sql=mysqli_query($koneksi, "SELECT type_serving,
											(SELECT COUNT(*) FROM food $idnegara) as total, COUNT(*) as jumlah,
											concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM food $idnegara)) * 100 ),2),'%') AS 'percentage'
											FROM `food` $idnegara GROUP BY type_serving");
                    while ($juto=mysqli_fetch_array($sql)) {
											$total= $juto['total'];
                      echo"
                        <tr>
                          <td width='40%'>
                          ";
													if ($juto['type_serving'] == '') {
														echo "Unknown";
													}
													if ($juto['type_serving'] == 'vege') {
														echo "
														<a href='https://www.foodieguidances.com/pages/food/search/?type_serving=$juto[type_serving]'>
															Vegetarian
														</a>";
													}
													else{
														echo "
														<a href='https://www.foodieguidances.com/pages/food/search/?type_serving=$juto[type_serving]'>
															$juto[type_serving]
														</a>";
													}
													echo"
                          </td>
                          <td width='30%'>
                            $juto[jumlah]
                          </td>
                          <td width='30%'>
                            $juto[percentage]
                          </td>
                        </tr>
                      ";
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $total; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Cuisine</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Cuisine</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras = "";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE negara='$_GET[negara]'";
												$idnegaras = "AND negara='$_GET[negara]'";
											}
										?>
										<?php
                    $sql = mysqli_query($koneksi, "SELECT * FROM cuisine");
										$sqla = mysqli_query($koneksi, "SELECT * FROM cuisine");
										$no=1;
										$totalaua = "";
										$totalau = "";
										while ($j=mysqli_fetch_array($sqla)) {
											$urutana = mysqli_num_rows(mysqli_query($koneksi, "SELECT * from food where id_cuisine like '%$j[nama_cuisine]%' $idnegaras"));
											$totalaua = $urutana + $totalaua;
										}
                    while ($j=mysqli_fetch_array($sql)) {
											$urutan = mysqli_num_rows(mysqli_query($koneksi, "SELECT * from food where id_cuisine like '%$j[nama_cuisine]%' $idnegaras"));
											$totalau = $urutan + $totalau;
											if ($urutan <> 0) {
	                      echo"
	                        <tr>
	                          <td width='40%'>";
														if ($j['nama_cuisine']=='') {
															echo "Unknown";
														}echo"
	                            <a href='https://www.foodieguidances.com/pages/food/search/?cuisine=$juto[nama_cuisine]'>
	                            	$j[nama_cuisine]
	                            </a>
	                          </td>
	                          <td width='30%'>
	                            $urutan
	                          </td>
	                          <td width='30%'>
	                            ".round($urutan/$totalaua * '100',2)."%
	                          </td>
	                        </tr>
	                      ";
											}
											$no++;
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $totalaua; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Cooking Methode</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-10">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Cooking Methode</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras = "";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE negara='$_GET[negara]'";
												$idnegaras = "AND negara='$_GET[negara]'";
											}
										?>
										<?php
                    $sql=mysqli_query($koneksi, "SELECT * FROM cooking_methode");
										$no=1;
										$totalaus ="";
                    while ($j=mysqli_fetch_array($sql)) {
											$urutan = mysqli_num_rows(mysqli_query($koneksi, "SELECT * from food where cooking_methode like '%$j[nama_cooking_methode]%' $idnegaras"));
											$totalu  = mysqli_num_rows(mysqli_query($koneksi, "SELECT id_food from food $idnegara"));
											$totalaus = $urutan + $totalaus;
											if ($urutan <> 0) {
	                      echo"
	                        <tr>
	                          <td width='40%'>";
														if ($j['nama_cooking_methode']=='') {
															echo "Unknown";
														}echo"
	                            <a href='https://www.foodieguidances.com/pages/food/search/?cooking_methode[]=$j[nama_cooking_methode]'>
	                            	$j[nama_cooking_methode]
	                            </a>
	                          </td>
	                          <td width='30%'>
	                            $urutan
	                          </td>
	                          <td width='30%'>
	                            ".round($urutan/$total * '100',2)."%
	                          </td>
	                        </tr>
	                      ";
											}
											$no++;
                    };
                     ?>
										 <tr style="border-top: 1px solid; padding: 5px 0">
											 <td> <label>Sum</label>
											 </td>
										 	<td>
										 		<label><?php echo $totalaus; ?></label>
										 	</td>
											<td>
												<label> 100% </label>
											</td>
										 </tr>
                   </table>
                </div>
              </div>

							<div class="form-group">
                <label class="col-sm-1 control-label">Food Rating</label>
                <div class="col-sm-10">
                  :
                </div>
              </div>
							<div class="form-group">
                <label class="col-sm-1 control-label"></label>
                <div class="col-sm-11">
                  <table width='100%'>
                    <tr>
                      <td width='40%'>
                        <label>Stars</label>
                      </td>
                      <td>
                        <label>Total</label>
                      </td>
                      <td>
                        <label>Percentage</label>
                      </td>
                    </tr>
										<?php
											$idnegara = "";
											$idnegaras = "";
											if (!empty($_GET['negara'])) {
												$idnegara = "WHERE negara='$_GET[negara]'";
												$idnegaras = "AND negara='$_GET[negara]'";
											}
										?>
                    <?php
											$sqlaaaas = mysqli_query($koneksi, "SELECT *, round(jumlah, 1) as rating, COUNT(*) as jumlah_rating FROM `food` $idnegara GROUP BY rating ORDER BY `rating` DESC");
											$sqla=mysqli_query($koneksi, "SELECT id_food FROM `food` $idnegara");

											while ($a = mysqli_fetch_array($sqlaaaas)) {
	                      echo"
	                        <tr>
	                          <td width='40%'>
															$a[rating] Star
	                          </td>
	                          <td width='30%'>
	                            $a[jumlah_rating]
	                          </td>
	                          <td width='30%'>
	                            ".round($a['jumlah_rating']/$total * '100',2)."%
	                          </td>
	                        </tr>
												";
											}
											?>
										<tr style="border-top: 1px solid; padding: 5px 0">
											<td> <label>Sum</label>	</td>
											<td>
												<label><?php echo $total; ?></label>
											</td>
											<td>
												<label> 100% </label>
											</td>
										</tr>
                   </table>
                </div>
              </div>



            </div>
          </div>
        <?php















				} else{
          $edit=mysqli_query($koneksi,"select * from food where id_food = '$_GET[id]'");
          $e=mysqli_fetch_array($edit);
					function tulis_cekbox($field,$koneksi,$judul) {
          	$query ="select * from ".$judul." order by nama_".$judul;
          	$r = mysqli_query($koneksi,$query);
          	$_arrNilai = explode('+', $field);
          	$str = '';
          	$no=1;
          	while ($w = mysqli_fetch_array($r)) {
          		$_ck = (array_search($w[1], $_arrNilai) === false)? '' : 'checked';
          		$str .= "<div class='col-md-6 col-4b'>
          			<div class='checkbox checkbox-inline checkbox-danger'>
          				<input type='checkbox' name='".$judul."[]' value='$w[1]' id='$judul$no' $_ck>
          				<label for='$judul$no'> $w[1]</label>
          			</div>
          		</div>";
          		$no++;
          	}
          	return $str;
          }
				?>
				<form action="mod/<?php echo"$mod/aksi.php?mod=$mod&ale=3&url=$_GET[url]"; ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" id="signup-daftar">
					<input type="hidden" name="id" value="<?php echo "$e[id_food]" ?>">
					<div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-btns">
                  <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                </div><!-- panel-btns -->
		            <h4 class="panel-title">Data food</h4>
                <p>Silahkan lengkapi data dibawah ini.</p>
            </div>
            <div class="panel-body nopadding">
              	<div class="form-group">
                  <label class="col-sm-2 control-label">Member :</label>
                  <div class="col-sm-10">
                    <select name="id_member" class="pilih" style="width: 100%;">
										<?php
                      $sql=mysqli_query($koneksi,"select id_member,username from member order by username asc");
                      while($b=mysqli_fetch_array($sql)){
                        echo"<option value='$b[id_member]'"; if($b['id_member']==$e['id_member']){ echo "selected";} echo ">$b[username]</option>";
                      }
                    ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                   <label class="col-sm-2 control-label">Food Name :</label>
                   <div class="col-sm-10">
                       <input type="text" name="food_name" class="form-control" id="username" value="<?php echo "$e[nama_food]" ?>">
                   </div>
                </div>
								<div class="form-group">
									 <label class="col-sm-2 control-label">Category :</label>
									 <div class="col-sm-10">
										 <select name="category" required class="pilih" style="width: 100%;" id="kat">
											<option value="">Select Category</option>
											<?php
												$sql=mysqli_query($koneksi,"select * from food_category order by nama_food_category asc");
												while($a=mysqli_fetch_array($sql)){
													echo"<option value='$a[id_food_category]'"; if($a['id_food_category']==$e['id_food_category']){ echo "selected";} echo">$a[nama_food_category]</option>";
												}
											?>
										</select>
									 </div>
								</div>
								<div class="form-group">
									 <label class="col-sm-2 control-label">Price Index :</label>
									 <div class="col-sm-10">
										<select name="price_index" required class="pilih" style="width: 100%;">
											<option>Select Price Index</option>
											<?php
												$sql=mysqli_query($koneksi,"select * from price_index order by nama_price_index asc");
												while($g=mysqli_fetch_array($sql)){
													echo"<option value='$g[id_price_index]'"; if($g['id_price_index']==$e['id_price_index']){ echo "selected";} echo">$g[nama_price_index]</option>";
												}
											?>
										</select>
									 </div>
								</div>
								<div class="form-group">
									 <label class="col-sm-2 control-label">MSG Level :</label>
									 <div class="col-sm-10">
										<select name="msg_level" class="pilih" style="width: 100%;">
 											<option>Select MSG Level</option>
 											<?php
	 											$sql=mysqli_query($koneksi,"select * from msg_level order by nama_msg_level asc");
	 											while($d=mysqli_fetch_array($sql)){
	 												echo"<option value='$d[id_msg_level]'"; if($d['id_msg_level']==$e['id_msg_level']){ echo "selected";} echo">$d[nama_msg_level]</option>";
	 											}
 											?>
 										</select>
									 </div>
								</div>
								<div class="form-group">
                   <label class="col-sm-2 control-label">Tag :</label>
                   <div class="col-sm-10">
                      <input type="text" class="form-control" placeholder="Write tag (separated by comma)" name="tag" value="<?php echo "$e[id_tag]" ?>">
                   </div>
                </div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Pork Serving :</label>
									<div class="col-sm-10">
										<div class="radio radio-inline">
											<input type="radio" name="pork" <?php if ($e['type_serving']=='Yes') { echo "checked"; } ?> value="Yes" id="pork1">
											Yes
										</div>
										<div class="radio radio-inline radio-danger">
											<input type="radio" name="pork" <?php if ($e['type_serving']=='No') { echo "checked"; } ?> value="No" id="pork2">
											No
										</div>
										<div class="radio radio-inline radio-danger">
											<input type="radio" name="pork" value="Halal" id="pork3" <?php if ($e['type_serving']=='Halal') { echo "checked"; } ?>>
											Halal
										</div>
										<div class="radio radio-inline radio-danger">
											<input type="radio" name="pork" value="vege" id="pork4" <?php if ($e['type_serving']=='vege') { echo "checked"; } ?>>
											Vegetable
										</div>
									</div>
								</div>
								<div class="accordion">Cuisine</div>
								<div class="panels">
									<div class="form-group">
										<?php
 										 $cuisine=tulis_cekbox($e['id_cuisine'],$koneksi,'cuisine');
 										 echo"$cuisine";
 									 ?>
									</div>
								</div>

                <div class="accordion">Serving</div>
								<div class="panels">
									<div class="form-group">
										<?php
											$serving=tulis_cekbox($e['cooking_methode'],$koneksi,'cooking_methode');
											echo"$serving";
										?>
									</div>
								</div>


									</div><!-- panel-body -->
									</div><!-- panel -->

        				<div class="panel panel-default">
        					<div class="panel-footer">
                    <button class="btn btn-primary mr5">Simpan</button>
                    <a href="mod-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-warning">Kembali</a>
                  </div><!-- panel-footer -->
                </div><!-- panel -->
				</form>

				<?php  } ?>
			</div><!-- contentpanel -->
        </div><!-- mainpanel -->
    </div><!-- mainwrapper -->
</section>


<script src="js/jquery-1.11.1.min.js"></script>


<script type="text/javascript">
$(document).ready(function() {
  $(".pilih").select2();
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('.tip').tooltip();

  var x = 1;
  $(".tambah_menu").click(function(e){ //on add input button click
    e.preventDefault();
    if(x < 30){ //max input box allowed
      x++; //text box increment
      $(".menu_wrap").append('<div class="row mb20"><div class="col-md-12 "><div class="fileinput fileinput-new" data-provides="fileinput"> <div class="fileinput-preview upload" data-trigger="fileinput"></div><input type="file" name="menu[]" class="hidden"></div></div><div class="col-md-6"><input type="text" class="form-control mt30" placeholder="Photo Caption" name="menu_caption[]"></div><div class="col-1"><button type="button" class="hapus_menu btn btn-danger mt30">Remove</button></div></div>'); //add input box
    }
    if(x > 29){
      $(".tambah_menu").hide();
    }
  });

  $(".menu_wrap").on("click",".hapus_menu", function(e){ //user click on remove text
    e.preventDefault(); $(this).parent().parent('.row').remove(); x--;
    if($(".tambah_menu").is(":hidden")){
      $(".tambah_menu").show();
    }
  });


		$("#negara").change(function(){
			var id = $("#negara").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base"; ?>/config/func/ajax_propinsi.php",
				data: "id=" + id,
				success: function(data){
					$("#propinsi").html(data);
					$("#propinsi").fadeIn(2000);
				}
			});
		});
		$("#propinsi").change(function(){
			var id = $("#propinsi").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base"; ?>/config/func/ajax_kota.php",
				data: "id=" + id,
				success: function(data){
					$("#kota").html(data);
					$("#kota").fadeIn(2000);
				}
			});
		});
    $("#kota").change(function(){
			var id = $("#kota").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base"; ?>/config/func/ajax_mall.php",
				data: "id=" + id,
				success: function(data){
					$("#mall").html(data);
					$("#mall").fadeIn(2000);
				}
			});
		});
		$("#kota").change(function(){
			var id = $("#kota").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base"; ?>/config/func/ajax_resto.php",
				data: "id=" + id,
				success: function(data){
					$("#search").html(data);
					$("#search").fadeIn(2000);
				}
			});
		});

});



      $("#duplicate").click(function(){
         var resto_time1 = $("#resto_time1").val();
         var resto_time2 = $("#resto_time2").val();
         var resto_time3 = $("#resto_time3").val();
         var resto_time4 = $("#resto_time4").val();
         var resto_time5 = $("#resto_time5").val();
         var resto_time6 = $("#resto_time6").val();
         var resto_time7 = $("#resto_time7").val();
         var resto_time8 = $("#resto_time8").val();
         var resto_time9 = $("#resto_time9").val();
         var resto_time10 = $("#resto_time10").val();
         var resto_time11 = $("#resto_time11").val();
         var resto_time12 = $("#resto_time12").val();
         var resto_time13 = $("#resto_time13").val();
         var resto_time14 = $("#resto_time14").val();

         var resto_time1a = $("#resto_time1a").val();
         var resto_time2a = $("#resto_time2a").val();
         var resto_time3a = $("#resto_time3a").val();
         var resto_time4a = $("#resto_time4a").val();
         var resto_time5a = $("#resto_time5a").val();
         var resto_time6a = $("#resto_time6a").val();
         var resto_time7a = $("#resto_time7a").val();
         var resto_time8a = $("#resto_time8a").val();
         var resto_time9a = $("#resto_time9a").val();
         var resto_time10a = $("#resto_time10a").val();
         var resto_time11a = $("#resto_time11a").val();
         var resto_time12a = $("#resto_time12a").val();
         var resto_time13a = $("#resto_time13a").val();
         var resto_time14a = $("#resto_time14a").val();

         $("#resto_time3").val(resto_time1);
         $("#resto_time4").val(resto_time2);
         $("#resto_time5").val(resto_time1);
         $("#resto_time6").val(resto_time2);
         $("#resto_time7").val(resto_time1);
         $("#resto_time8").val(resto_time2);
         $("#resto_time9").val(resto_time1);
         $("#resto_time10").val(resto_time2);
         $("#resto_time11").val(resto_time1);
         $("#resto_time12").val(resto_time2);
         $("#resto_time13").val(resto_time1);
         $("#resto_time14").val(resto_time2);

         $("#resto_time3a").val(resto_time1a);
         $("#resto_time4a").val(resto_time2a);
         $("#resto_time5a").val(resto_time1a);
         $("#resto_time6a").val(resto_time2a);
         $("#resto_time7a").val(resto_time1a);
         $("#resto_time8a").val(resto_time2a);
         $("#resto_time9a").val(resto_time1a);
         $("#resto_time10a").val(resto_time2a);
         $("#resto_time11a").val(resto_time1a);
         $("#resto_time12a").val(resto_time2a);
         $("#resto_time13a").val(resto_time1a);
         $("#resto_time14a").val(resto_time2a);
      });

			$("#duplicate2").click(function(){
         var bar_time1 = $("#bar_time1").val();
         var bar_time2 = $("#bar_time2").val();
         var bar_time3 = $("#bar_time3").val();
         var bar_time4 = $("#bar_time4").val();
         var bar_time5 = $("#bar_time5").val();
         var bar_time6 = $("#bar_time6").val();
         var bar_time7 = $("#bar_time7").val();
         var bar_time8 = $("#bar_time8").val();
         var bar_time9 = $("#bar_time9").val();
         var bar_time10 = $("#bar_time10").val();
         var bar_time11 = $("#bar_time11").val();
         var bar_time12 = $("#bar_time12").val();
         var bar_time13 = $("#bar_time13").val();
         var bar_time14 = $("#bar_time14").val();

         var bar_time1a = $("#bar_time1a").val();
         var bar_time2a = $("#bar_time2a").val();
         var bar_time3a = $("#bar_time3a").val();
         var bar_time4a = $("#bar_time4a").val();
         var bar_time5a = $("#bar_time5a").val();
         var bar_time6a = $("#bar_time6a").val();
         var bar_time7a = $("#bar_time7a").val();
         var bar_time8a = $("#bar_time8a").val();
         var bar_time9a = $("#bar_time9a").val();
         var bar_time10a = $("#bar_time10a").val();
         var bar_time11a = $("#bar_time11a").val();
         var bar_time12a = $("#bar_time12a").val();
         var bar_time13a = $("#bar_time13a").val();
         var bar_time14a = $("#bar_time14a").val();

         $("#bar_time3").val(bar_time1);
         $("#bar_time4").val(bar_time2);
         $("#bar_time5").val(bar_time1);
         $("#bar_time6").val(bar_time2);
         $("#bar_time7").val(bar_time1);
         $("#bar_time8").val(bar_time2);
         $("#bar_time9").val(bar_time1);
         $("#bar_time10").val(bar_time2);
         $("#bar_time11").val(bar_time1);
         $("#bar_time12").val(bar_time2);
         $("#bar_time13").val(bar_time1);
         $("#bar_time14").val(bar_time2);

         $("#bar_time3a").val(bar_time1a);
         $("#bar_time4a").val(bar_time2a);
         $("#bar_time5a").val(bar_time1a);
         $("#bar_time6a").val(bar_time2a);
         $("#bar_time7a").val(bar_time1a);
         $("#bar_time8a").val(bar_time2a);
         $("#bar_time9a").val(bar_time1a);
         $("#bar_time10a").val(bar_time2a);
         $("#bar_time11a").val(bar_time1a);
         $("#bar_time12a").val(bar_time2a);
         $("#bar_time13a").val(bar_time1a);
         $("#bar_time14a").val(bar_time2a);
      });

</script>
