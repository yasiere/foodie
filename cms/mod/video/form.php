<style>
    html, body {
    height: 100%;
    margin: 0;
    padding: 0;
    }
    #map {
    width: 100%;
    height: 400px;
    }
    .controls {
    margin-top: 10px;
    border: 1px solid transparent;
    border-radius: 2px 0 0 2px;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    height: 32px;
    outline: none;
    box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    }
    #searchInput {
    background-color: #fff;
    font-family: Roboto;
    font-size: 15px;
    font-weight: 300;
    margin-left: 12px;
    padding: 0 11px 0 13px;
    text-overflow: ellipsis;
    width: 50%;
    }
    #searchInput:focus {
    border-color: #4d90fe;
    }
</style>
<section>
    <div class="mainwrapper">
        <?php
			include "inc/kiri.php";
			$pageTitle = 'Video';
			?>
        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="pageicon pull-left">
                        <i class="fa fa-picture-o"></i>
                    </div>
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href="mod-beranda.htm"><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="mod-beranda.htm">Beranda</a></li>
                            <li>Informasi <?php echo $pageTitle;?></li>
                        </ul>
                        <h4>Informasi <?php echo $pageTitle;?></h4>
                    </div>
                </div>
                <!-- media -->
            </div>
            <!-- pageheader -->
            <div class="contentpanel">
                <?php if($_GET['proses']==1){ ?>
					<?php if(isset($_GET['info'])) include "inc/form-alert.php"; ?>
                <form action="mod/<?php echo"$mod/aksi.php?mod=$mod&ale=2&url=$_GET[url]"; ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" id="signup-daftar">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns">
                                <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                            </div>
                            <!-- panel-btns -->
                            <h4 class="panel-title">Data <?php echo $pageTitle;?></h4>
                            <p>Silahkan lengkapi data dibawah ini.</p>
                        </div>
                        <div class="panel-body nopadding">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Member :</label>
                                <div class="col-sm-10">
                                    <select name="inputs[id_member]" class="pilih" style="width: 100%;">
                                    <?php
                                        $sql=mysqli_query($koneksi,"select id_member,username from member order by username asc");
                                        while($b=mysqli_fetch_array($sql)){
                                        	echo"<option value='$b[id_member]'>$b[username]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Category :</label>
								<div class="col-sm-10">
									<select name="inputs[id_video_category]" required class="pilih" style="width: 100%;" id="kat">
										<option value="">Select Category</option>
										<?php
											$sql=mysqli_query($koneksi,"select * from video_category order by nama_video_category asc");
											while($a=mysqli_fetch_array($sql)){
												echo"<option value='$a[id_video_category]'>$a[nama_video_category]</option>";
											}
											?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Video Title :</label>
								<div class="col-sm-10">
									<input type="text" name="inputs[nama_video]" class="form-control" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Video URL :</label>
								<div class="col-sm-10">
									<input type="text" name="inputs[url]" class="form-control" required>
									<small class="help-block">Example: https://www.youtube.com/watch?v=SEXJxf_s0UA</small>
								</div>
							</div>
                        </div>
                        <!-- panel-body -->
                    </div>
                    <!-- panel -->
                    <div class="panel panel-default">
                        <div class="panel-footer">
                            <button class="btn btn-primary mr5">Simpan</button>
                            <a href="mod-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-warning">Kembali</a>
                        </div>
                        <!-- panel-footer -->
                    </div>
                    <!-- panel -->
                </form>
                <?php
				
                    }else{
                          $edit=mysqli_query($koneksi,"select * from video where id_video = '$_GET[id]'");
                          $e=mysqli_fetch_array($edit);
                    ?>
                <form action="mod/<?php echo"$mod/aksi.php?mod=$mod&ale=3&url=$_GET[url]"; ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" id="signup-daftar">
                    <input type="hidden" name="id" value="<?php echo $e['id_video']; ?>">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns">
                                <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                            </div>
                            <!-- panel-btns -->
                            <h4 class="panel-title">Data  <?php echo $pageTitle;?></h4>
                            <p>Silahkan lengkapi data dibawah ini.</p>
                        </div>
                        <div class="panel-body nopadding">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Member :</label>
                                <div class="col-sm-10">
                                    <select name="inputs[id_member]" class="pilih" style="width: 100%;">
                                    <?php
                                        $sql=mysqli_query($koneksi,"select id_member,username from member order by username asc");
                                        while($b=mysqli_fetch_array($sql)){
											$selected = ($b['id_member'] == $e['id_member'])? "selected": "";
                                        	echo"<option value='$b[id_member]' {$selected}>$b[username]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Category :</label>
								<div class="col-sm-10">
									<select name="inputs[id_video_category]" required class="pilih" style="width: 100%;" id="kat">
										<option value="">Select Category</option>
										<?php
											$sql=mysqli_query($koneksi,"select * from video_category order by nama_video_category asc");
											while($a=mysqli_fetch_array($sql)){
												$selected = ($a['id_video_category'] == $e['id_video_category'])? "selected": "";
												echo"<option value='$a[id_video_category]' {$selected}>$a[nama_video_category]</option>";
											}
											?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Video Title :</label>
								<div class="col-sm-10">
									<input type="text" name="inputs[nama_video]" class="form-control" value="<?php echo $e['nama_video']?>" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Video URL :</label>
								<div class="col-sm-10">
									<input type="text" name="inputs[url]" class="form-control" value="<?php echo $e['url']?>" required>
									<small class="help-block">Example: https://www.youtube.com/watch?v=SEXJxf_s0UA</small>
								</div>
							</div>
                        </div>
                        <!-- panel-body -->
                    </div>
                    <!-- panel -->
                    <div class="panel panel-default">
                        <div class="panel-footer">
                            <button class="btn btn-primary mr5">Simpan</button>
                            <a href="mod-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-warning">Kembali</a>
                        </div>
                        <!-- panel-footer -->
                    </div>
                    <!-- panel -->
                </form>
                <?php
                    } ?>
            </div>
            <!-- contentpanel -->
        </div>
        <!-- mainpanel -->
    </div>
    <!-- mainwrapper -->
</section>
<style media="screen">
    #country-list {
    margin: 0;
    list-style: none;
    padding: 0px;
    max-height: 155px;
    position: relative;
    overflow: auto;
    }
    .ko {
    border: 1px solid #CCC;
    padding: 5px;
    color: #9C9999;
    border-top: none;
    cursor: pointer;
    transition: 0.7s all 0s ease;
    }
    .ko:hover{
    background: #ffeed0;
    transition: 0.3s all 0s ease;
    }
</style>
<script src="js/jquery-1.11.1.min.js"></script>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$(".pilih").select2();
		
		tinymce.init({ selector:'textarea' });
	});
</script>