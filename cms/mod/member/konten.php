<?php 
 error_reporting(0); 
?>
<?php
  // function cutText($text, $length, $mode = 2){
  //   if ($mode != 1) {
  //     $char = $text{$length - 1};
  //     switch($mode)   {
  //       case 2:
  //         while($char != ' ') {
  //           $char = $text{--$length};
  //         }
  //       case 3:
  //         while($char != ' ') {
  //           $char = $text{
  //             ++$num_char;
  //           };
  //         }
  //     }
  //   }
  //   return substr($text, 0, $length);
  // }
?>
<section>
    <div class="mainwrapper">
        <?php include "inc/kiri.php"; ?>
        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="pageicon pull-left">
                        <i class="fa fa-tags"></i>
                    </div>
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href="mod-beranda-1.htm"><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="mod-beranda-1.htm">Beranda</a></li>
                            <li>Informasi Member</li>
                        </ul>
                        <h4>Informasi Member </h4>
                    </div>
                </div><!-- media -->
            </div><!-- pageheader -->

            <div class="contentpanel">
				<?php
				if(isset($_GET['info'])){
					if($_GET['info']==1){
						echo"<div class='alert alert-success'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Sukses!</strong> data berhasil dihapus.
						</div>";
					}
				}
				?>
                <div class="panel panel-default">
                    <div class="panel-heading">
						                <div class="row">
							                <div class="col-sm-11">
								            	<h4 class="panel-title">Daftar Member</h4>
								                <p><a href="add-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-primary">Tambah Data</a></p>
							                </div>
						                </div>
					         </div>
                    <div class="panel-body">
						<table id="aledata" class="table table-striped table-bordered responsive">
							<thead class="">
								<tr>
									<th>No</th>
                    <th>Email </th>
  					        <th>Nama Member </th>
                    <th>Nama Negara </th>
                    <th>Gambar Thumbnail </th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no=1;
								$sql=mysqli_query($koneksi,"select * from member m LEFT JOIN negara n on m.id_negara=n.id_negara  order by m.nama_depan ASC");
								while($r=mysqli_fetch_array($sql)){
									echo"
                    <tr>
  										<td width=''>$no.</td>
  										<td>$r[email]</td>
  										<td width=''>$r[nama_depan] $r[nama_belakang]</td>
  										<td width=''>$r[nama_negara]</td>
  										<td width=''>"; if(!empty($r['gambar_thumb'])) { echo "<img src='$base/assets/img/member/$r[gambar_thumb]' width='100px' class='img-thumbnail'>"; } echo "</td>
  										<td width='150px'>
  												<a href='edit-$mod-$r[id_member]-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>
  												<a class='btn btn-danger btn-rounded btn-sm' href='mod/$mod/aksi.php?mod=$mod&id=$r[id_member]&gambarthumb=$r[gambar_thumb]&gambarlandscape=$r[gambar_landscape]&ale=1&url=$_GET[url]' onClick=\"return confirm('apakah anda yakin akan menghapus data ini ?')\"><i class='fa fa-trash-o'></i> Hapus</a>
  										</td>
  									</tr>";
                  $no++;
								}
								?>
							</tbody>
						</table>
					</div>
                </div><!-- panel -->
            </div><!-- contentpanel -->
        </div><!-- mainpanel -->
    </div><!-- mainwrapper -->
</section>
