<section>

    <div class="mainwrapper">

        <?php include "inc/kiri.php"; ?>

         <div class="mainpanel">

             <div class="pageheader">

                <div class="media">

                    <div class="pageicon pull-left">

                        <i class="fa fa-home"></i>

                    </div>

                    <div class="media-body">

						<ul class="breadcrumb">

                            <li><a href="mod-beranda.htm"><i class="glyphicon glyphicon-home"></i></a></li>

                            <li><a href="mod-beranda.htm">Beranda</a></li>

                            <li>Informasi Member</li>

                        </ul>

                        <h4>Informasi Member</h4>

                    </div>

                </div><!-- media -->

            </div><!-- pageheader -->

            <div class="contentpanel">

				<?php if($_GET['proses']==1){ ?>

				<form action="mod/<?php echo"$mod/aksi.php?mod=$mod&ale=2&url=$_GET[url]"; ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" id="signup-daftar">

				<?php

				if(isset($_GET['info'])){

					if($_GET['info']==1){

						echo"<div class='alert alert-success'>

							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button

							<strong>Sukses!</strong> data berhasil disimpan.

						</div>";

					}

					elseif($_GET['info']==2){

						echo"<div class='alert alert-danger'>

							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button

							<strong>Kesalahan!</strong> tipe file tidak cocok, anda harus mengupload file berekstensi *.JPG.

						</div>";

					}

					else{

						echo"<div class='alert alert-danger'>

							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button

							<strong>Kesalahan!</strong> ukuran file terlalu besar, ukuran maksimal yang diperbolehkan adalah 300kb.

						</div>";

					}

				}

				?>

                <div class="panel panel-default">

                    <div class="panel-heading">

                        <div class="panel-btns">

                            <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>

                        </div><!-- panel-btns -->

						<h4 class="panel-title">Data Member</h4>

                        <p>Silahkan lengkapi data dibawah ini.</p>

                    </div>

                    <div class="panel-body nopadding">

                      <div class="form-group">

                           <label class="col-sm-2 control-label">Username:</label>

                           <div class="col-sm-10">

                               <b>foodieguidances.com/</b><input type="text" name="m_username" class="form-control" id="username">

                           </div>

                       </div>

                       <div class="form-group">

                           <label class="col-sm-2 control-label">Email :</label>

                           <div class="col-sm-10">

                               <input type="text" name="m_surat" class="form-control" id="email">

                           </div>

                       </div>

                       <div class="form-group">

                           <label class="col-sm-2 control-label">Password :</label>

                           <div class="col-sm-10">

                               <input type="text" name="m_kode" class="form-control" id="password">

                           </div>

                       </div>

                       <div class="form-group">

                           <label class="col-sm-2 control-label">Confirm Password :</label>

                           <div class="col-sm-10">

                               <input type="text" name="m_kode2" class="form-control">

                           </div>

                       </div>

                       <div class="form-group">

                           <label class="col-sm-2 control-label">Country :</label>

                           <div class="col-sm-10">

                               <select name="m_negara" class="pilih" style="width: 100%;" id="negara">

                                	<?php

										$sql=mysqli_query($koneksi,"select * from negara order by nama_negara asc");

										while($b=mysqli_fetch_array($sql)){

											echo"<option value='$b[id_negara]'>$b[nama_negara]</option>";

										}

									?>

                                </select>

                           </div>

                       </div>

                       <div class="form-group">

                           <label class="col-sm-2 control-label">Gender :</label>

                           <div class="col-sm-10">

                               	<div class="radio radio-danger radio-inline">

								<input type="radio" id="inlineRadio1" value="Male" name="m_gender" checked>

								<label for="inlineRadio1"> Male </label>

                                </div>

                                <div class="radio radio-inline">

                                    <input type="radio" id="inlineRadio2" value="Female" name="m_gender">

                                    <label for="inlineRadio2"> Female </label>

                                </div>

                           </div>

                       </div>

                       <div class="form-group">

                           <label class="col-sm-2 control-label">Nama Depan :</label>

                           <div class="col-sm-10">

                               	<input type="text" name="m_depan" class="form-control">

                           </div>

                       </div>

                       <div class="form-group">

                           <label class="col-sm-2 control-label">Nama Belakang :</label>

                           <div class="col-sm-10">

                               	<input type="text" name="m_belakang" class="form-control">

                           </div>

                       </div>

                       <div class="form-group">

                           <label class="col-sm-2 control-label">Deskripsi :</label>

                           <div class="col-sm-10">

                               	<textarea type="text" name="m_deskripsi" class="form-control" rows="4"></textarea>

                           </div>

                       </div>

                       <div class="form-group">

                           <label class="col-sm-2 control-label">Facebook :</label>

                           <div class="col-sm-10">

                               	<input type="text" name="m_facebook" class="form-control">

                           </div>

                       </div>

                       <div class="form-group">

                           <label class="col-sm-2 control-label">Twitter :</label>

                           <div class="col-sm-10">

                               	<input type="text" name="m_twitter" class="form-control">

                           </div>

                       </div>

                       <div class="form-group">

                           <label class="col-sm-2 control-label">Pinterest :</label>

                           <div class="col-sm-10">

                               	<input type="text" name="m_pinterest" class="form-control">

                           </div>

                       </div>

                       <div class="form-group">

                           <label class="col-sm-2 control-label">Google+ :</label>

                           <div class="col-sm-10">

                               	<input type="text" name="m_google" class="form-control">

                           </div>

                       </div>

                       <div class="form-group">

                           <label class="col-sm-2 control-label">Instagram :</label>

                           <div class="col-sm-10">

                               	<input type="text" name="m_instagram" class="form-control">

                           </div>

                       </div>

                       <div class="form-group">

                       		<label class="col-sm-2 control-label">Thumbnail</label>

                            <div class="col-sm-10">

                                <input type="file" name="profil" title="Pilih" class="btn btn-primary">

                            </div>

                       </div>

                       <div class="form-group">

                       		<label class="col-sm-2 control-label">Cover</label>

                            <div class="col-sm-10">

                                <input type="file" name="cover" title="Pilih" class="btn btn-primary">

                            </div>

                       </div>



						<!--<div class="form-group">

                            <label class="col-sm-2 control-label">Gambar 1:</label>

                        	<div class="col-sm-10">

							    <input type="file" name="fupload" title="Pilih" class="btn btn-primary"> <br> <input type="text" name="alt_produk" class="form-control" placeholder="alt Gambar 1...">

                            </div>

                        </div>-->



                        <!-- form-group -->

                    </div><!-- panel-body -->

                </div><!-- panel -->



				<div class="panel panel-default">

					<div class="panel-footer">

                        <button class="btn btn-primary mr5">Simpan</button>

                        <a href="mod-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-warning">Kembali</a>

                     </div><!-- panel-footer -->

                </div><!-- panel -->

				</form>

				<?php

				}









        elseif ($_GET['proses']==3) {

          $dat=mysqli_query($koneksi,"select * from member m, negara n where m.id_negara = n.id_negara and m.id_member='$_GET[id]'");

  				$d=mysqli_fetch_array($dat);

          ?>

          <div class="panel panel-default">

            <div class="panel-heading">

                <div class="panel-btns">

                    <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>

                </div><!-- panel-btns -->

                <h4 class="panel-title">Data Member</h4>

                <p>Silahkan lengkapi data dibawah ini.</p>

            </div>

            <div class="panel-body nopadding">

              <div class="form-group">

                <label class="col-sm-1 control-label">Total</label>

                <div class="col-sm-10">

                  <div class="col-sm-10 control-label"> :

                    <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT username, COUNT(*) as user FROM `member` GROUP by username")); echo "$juto"; ?>

                  </div>

                </div>

              </div>

              <div class="form-group">

                <label class="col-sm-1 control-label">Country</label>

                <div class="col-sm-10">

                  <div class="col-sm-10 control-label"> :

                    <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT n.nama_negara,n.id_negara, COUNT(*) FROM member m JOIN negara n ON m.id_negara = n.id_negara GROUP BY n.nama_negara")); echo "$juto"; ?>

                  </div>

                </div>

              </div>

              <div class="form-group">

                <label class="col-sm-1 control-label"></label>

                <div class="col-sm-10">

                  <table width='100%'>

                    <tr>

                      <td width='40%'>

                        <label>Country</label>

                      </td>

                      <td>

                        <label>Total</label>

                      </td>

                      <td>

                        <label>Percentage</label>

                      </td>

                    </tr>

                    <?php

                    $sql=mysqli_query($koneksi, "SELECT n.nama_negara,n.id_negara,COUNT(*) as jumlah , concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM member )) * 100 ),2),'%') AS 'percentage' FROM member m JOIN negara n ON m.id_negara = n.id_negara GROUP BY n.nama_negara");

                    while ($juto=mysqli_fetch_array($sql)) {

                      echo"

                        <tr>

                          <td width='30%'>

                            $juto[nama_negara]

                          </td>

                          <td width='30%'>

                            $juto[jumlah]

                          </td>

                          <td width='30%'>

                            $juto[percentage]

                          </td>

                        </tr>

                      ";

                    };

                     ?>

                   </table>

                </div>

              </div>

              <div class="form-group">

                <label class="col-sm-1 control-label">Gender</label>

                <div class="col-sm-10">

                  : 3

                </div>

              </div>

              <div class="form-group">

                <label class="col-sm-1 control-label"></label>

                <div class="col-sm-10">

                  <table width='100%'>

                    <tr>

                      <td width='40%'>

                        <label>Gender</label>

                      </td>

                      <td>

                        <label>Total</label>

                      </td>

                      <td>

                        <label>Percentage</label>

                      </td>

                    </tr>

                    <?php

                    $sql=mysqli_query($koneksi, "SELECT gender ,COUNT(*) as jumlah , concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM member )) * 100 ),2),'%') AS 'percentage' FROM member  GROUP BY gender");

                    while ($juto=mysqli_fetch_array($sql)) {

                      echo"

                        <tr>

                          <td width='40%'>

                            $juto[gender]";

                            if ($juto['gender']=='') {

                              echo "Unknown";

                            }echo"

                          </td>

                          <td width='30%'>

                            $juto[jumlah]

                          </td>

                          <td width='30%'>

                            $juto[percentage]

                          </td>

                        </tr>

                      ";

                    };

                     ?>

                   </table>

                </div>

              </div>

            </div>

          </div>

        <?php }











        else{

				$dat=mysqli_query($koneksi,"select * from member m, negara n where m.id_negara = n.id_negara and m.id_member='$_GET[id]'");

				$d=mysqli_fetch_array($dat);

				?>

				<form action="mod/<?php echo"$mod/aksi.php?mod=$mod&ale=3&url=$_GET[url]"; ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" id="edit-daftar">

				<input type="hidden" name="id" value="<?php echo"$_GET[id]"; ?>" >

				<?php

				if(isset($_GET['info'])){

					if($_GET['info']==1){

						echo"<div class='alert alert-success'>

							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button

							<strong>Sukses!</strong> data berhasil disimpan.

						</div>";

					}

					elseif($_GET['info']==2){

						echo"<div class='alert alert-danger'>

							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button

							<strong>Kesalahan!</strong> tipe file tidak cocok, anda harus mengupload file berekstensi *.JPG.

						</div>";

					}

					else{

						echo"<div class='alert alert-danger'>

							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button

							<strong>Kesalahan!</strong> ukuran file terlalu besar, ukuran maksimal yang diperbolehkan adalah 300kb.

						</div>";

					}

				}

				?>

                <div class="panel panel-default">

                    <div class="panel-heading">

                        <div class="panel-btns">

                            <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>

                        </div><!-- panel-btns -->

						<h4 class="panel-title">Detail Data Produk</h4>

                        <p>Silahkan lengkapi data dibawah ini.</p>

                    </div>

                    <div class="panel-body nopadding">

                      <div class="form-group">

                           <label class="col-sm-2 control-label">Username:</label>

                           <div class="col-sm-10">

                               <b>foodieguidances.com/</b><input type="text" name="m_username" class="form-control" id="username" value="<?php echo $d['username'] ?>" readonly="readonly">

                           </div>

                       </div>

                       <div class="form-group">

                           <label class="col-sm-2 control-label">Email :</label>

                           <div class="col-sm-10">

                               <input type="text" name="m_surat" class="form-control" id="email" value="<?php echo $d['email'] ?>">

                           </div>

                       </div>

                       <div class="form-group">

                           <label class="col-sm-2 control-label">Password :</label>

                           <div class="col-sm-10">

                               <input type="password" name="m_kode" class="form-control" id="password" value="Password123#">

                           </div>

                       </div>

                       <div class="form-group">

                           <label class="col-sm-2 control-label">Confirm Password :</label>

                           <div class="col-sm-10">

                               <input type="password" name="m_kode2" class="form-control" value="Password123#">

                           </div>

                       </div>

                       <div class="form-group">

                           <label class="col-sm-2 control-label">Country :</label>

                           <div class="col-sm-10">

                               <select name="m_negara" class="pilih" style="width: 100%;" id="negara">

                                	<?php

										$sql=mysqli_query($koneksi,"select * from negara order by nama_negara asc");

										while($b=mysqli_fetch_array($sql)){

											if($d['id_negara'] == $b['id_negara'])

											{

												$pilih = "selected";

											}

											else

											{

												$pilih = "";

											}

											echo"<option $pilih value='$b[id_negara]'>$b[nama_negara]</option>";

										}

									?>

                                </select>

                           </div>

                       </div>

                       <div class="form-group">

                           <label class="col-sm-2 control-label">Gender :</label>

                           <div class="col-sm-10">

                           		<?php

									if($d['gender'] == 'Male')

									{

										$cekmale = "checked";

										$cekfemale = "";

									}

									else

									{

										$cekmale = "";

										$cekfemale = "checked";

									}

								?>

                               	<div class="radio radio-danger radio-inline">

								<input type="radio" id="inlineRadio1" value="Male" name="m_gender" <?php echo $cekmale ?>>

								<label for="inlineRadio1"> Male </label>

                                </div>

                                <div class="radio radio-inline">

                                    <input type="radio" id="inlineRadio2" value="Female" name="m_gender" <?php echo $cekfemale ?>>

                                    <label for="inlineRadio2"> Female </label>

                                </div>

                           </div>

                       </div>

                       <div class="form-group">

                           <label class="col-sm-2 control-label">Nama Depan :</label>

                           <div class="col-sm-10">

                               	<input type="text" name="m_depan" class="form-control" value="<?php echo $d['nama_depan'] ?>">

                           </div>

                       </div>

                       <div class="form-group">

                           <label class="col-sm-2 control-label">Nama Belakang :</label>

                           <div class="col-sm-10">

                               	<input type="text" name="m_belakang" class="form-control" value="<?php echo $d['nama_belakang'] ?>">

                           </div>

                       </div>

                       <div class="form-group">

                           <label class="col-sm-2 control-label">Deskripsi :</label>

                           <div class="col-sm-10">

                               	<textarea type="text" name="m_deskripsi" class="form-control" rows="4"><?php echo $d['deskripsi'] ?></textarea>

                           </div>

                       </div>

                       <div class="form-group">

                           <label class="col-sm-2 control-label">Facebook :</label>

                           <div class="col-sm-10">

                               	<input type="text" name="m_facebook" class="form-control" value="<?php echo $d['social_fb'] ?>">

                           </div>

                       </div>

                       <div class="form-group">

                           <label class="col-sm-2 control-label">Twitter :</label>

                           <div class="col-sm-10">

                               	<input type="text" name="m_twitter" class="form-control" value="<?php echo $d['social_twitter'] ?>">

                           </div>

                       </div>

                       <div class="form-group">

                           <label class="col-sm-2 control-label">Pinterest :</label>

                           <div class="col-sm-10">

                               	<input type="text" name="m_pinterest" class="form-control" value="<?php echo $d['social_google'] ?>">

                           </div>

                       </div>

                       <div class="form-group">

                           <label class="col-sm-2 control-label">Google+ :</label>

                           <div class="col-sm-10">

                               	<input type="text" name="m_google" class="form-control" value="<?php echo $d['social_pint'] ?>">

                           </div>

                       </div>

                       <div class="form-group">

                           <label class="col-sm-2 control-label">Instagram :</label>

                           <div class="col-sm-10">

                               	<input type="text" name="m_instagram" class="form-control" value="<?php echo $d['social_insta'] ?>">

                           </div>

                       </div>

                       <div class="form-group">

                       		<label class="col-sm-2 control-label">Thumbnail</label>

                            <div class="col-sm-10">

                                <input type="file" name="profil" title="Pilih" class="btn btn-primary">

                                <br>

                                <img src="<?php echo $base ?>/assets/img/member/<?php echo $d['gambar_thumb'] ?>" width="100px" class="img-thumbnail">

                            </div>

                       </div>

                       <div class="form-group">

                       		<label class="col-sm-2 control-label">Cover</label>

                            <div class="col-sm-10">

                                <input type="file" name="cover" title="Pilih" class="btn btn-primary">

                                <br>

                                <img src="<?php echo $base ?>/assets/img/member/<?php echo $d['gambar_landscape'] ?>" width="400px" class="img-thumbnail">

                            </div>

                       </div>



						<!--<div class="form-group">

                            <label class="col-sm-2 control-label">Gambar 1:</label>

                        	<div class="col-sm-10">

							    <input type="file" name="fupload" title="Pilih" class="btn btn-primary"> <br> <input type="text" name="alt_produk" class="form-control" placeholder="alt Gambar 1...">

                            </div>

                        </div>-->



                        <!-- form-group -->

                    </div><!-- panel-body -->

                </div><!-- panel -->



				<div class="panel panel-default">

					<div class="panel-footer">

                        <button class="btn btn-primary mr5">Simpan</button>

                        <a href="mod-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-warning">Kembali</a>

                     </div><!-- panel-footer -->

                </div><!-- panel -->

				</form>

				<?php } ?>

			</div><!-- contentpanel -->

        </div><!-- mainpanel -->

    </div><!-- mainwrapper -->

</section>







<script type="text/javascript">

$(document).ready(function() {

  $(".pilih").select2();

});

</script>







	<script type="text/javascript">

	$(document).ready(function () {

		$.validator.addMethod("usertheme",function(value,element){

         return this.optional(element) || /^[a-z][a-z0-9_-]{3,12}$/i.test(value);

      });

		// $.validator.addMethod("passkuat",function(value,element){

      //    return this.optional(element) || /^[a-z0-9_-]{7,20}$/i.test(value);

      // });

		// $.validator.addMethod("passkuat",function(value,element){

      //  	return this.optional(element) || /^(?=.*[A-Z])(?=.*[!@#$&%^*~?])(?=.*[0-9]).{7,20}$/i.test(value);

      // });

		$.validator.addMethod("passkuat",function(value,element){

            return this.optional(element) || /^(?=.*[A-Z])(?=.*[!@#$&%^*~?])(?=.*[0-9]).{7,20}$/i.test(value);

        });

		$.validator.addMethod("accept_char",function(value,element){

         return this.optional(element) || /^([a-zA-Z0-9,\./<>\?;':""[\]\\{}\|`~!@#\$%\^&\*()-_=\+]*)$/i.test(value);

      });



		$("#cuisine18").click(function(){

			$(".as").toggle();

			$(".ad").toggle();



		});



		$('#signup-daftar').validate({

			rules: {

				username: {

					required: true,

					minlength: 4,

					maxlength: 20,

					usertheme:true,

					remote: {

						url: "../config/func/check-username.php",

						type: "post",

						data: {

						  username: function() {

							return $( "#username" ).val();

						  }

						}

					}

				},

				m_surat: {

					required: true,

					email: true,

					remote: {

						url: "../config/func/check-email.php",

						type: "post",

						data: {

						  m_surat: function() {

							return $( "#email" ).val();

						  }

						}

					}

				},

				m_kode: {

					required: true,

					minlength: 8,

					maxlength: 20,

					passkuat:true,

					accept_char:true

				},

				m_kode2: {

					equalTo : "#password"

				},

				negara: {

					required: true

				}

			},

			messages: {

				username: {

					required: "Username cant be blank.",

					minlength: "Username must be between 4 - 20 characters",

					maxlength: "Username must be between 4 - 20 characters",

					usertheme: "Username first character must be letter (a-z) follow by letter, number, underscore or a hyphen",

					remote: "username is already in use"

				},

				m_surat: {

					required: "Email cant be blank",

					email: "Email not valid",

					remote: "Email is already registered"

				},

				m_kode: {

					required: "Password cant be blank.",

					minlength: "Password must be between 8 - 20 characters",

					maxlength: "Password must be between 8 - 20 characters",

					passkuat: "Your Password is weak! at least one uppercase letter, one number and one special character",

					accept_char: "Only keyboard character allowed (!,@,#,$,%,^,&,*,~,?)"

				},

				m_kode2: {

					equalTo : "Your passwords do not match. Please try again"

				},

				negara: {

					required: "Country cant be blank."

				}

			}

		});



		$('#edit-daftar').validate({

			rules: {

				username: {

					required: true,

					minlength: 4,

					maxlength: 20,

					usertheme:true,

					remote: {

						url: "../config/func/check-username.php",

						type: "post",

						data: {

						  username: function() {

							return $( "#username" ).val();

						  }

						}

					}

				},

				m_surat: {

					required: true,

					email: true,

				},

				m_kode: {

					required: true,

					minlength: 8,

					maxlength: 20,

					passkuat:true,

					accept_char:true

				},

				m_kode2: {

					equalTo : "#password"

				},

				negara: {

					required: true

				}

			},

			messages: {

				username: {

					required: "Username cant be blank.",

					minlength: "Username must be between 4 - 20 characters",

					maxlength: "Username must be between 4 - 20 characters",

					usertheme: "Username first character must be letter (a-z) follow by letter, number, underscore or a hyphen",

					remote: "username is already in use"

				},

				m_surat: {

					required: "Email cant be blank",

					email: "Email not valid",

				},

				m_kode: {

					required: "Password cant be blank.",

					minlength: "Password must be between 8 - 20 characters",

					maxlength: "Password must be between 8 - 20 characters",

					passkuat: "Your Password is weak! at least one uppercase letter, one number and one special character",

					accept_char: "Only keyboard character allowed (!,@,#,$,%,^,&,*,~,?)"

				},

				m_kode2: {

					equalTo : "Your passwords do not match. Please try again"

				},

				negara: {

					required: "Country cant be blank."

				}

			}

		});

	});

	</script>

