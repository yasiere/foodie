<style>
    html, body {
    height: 100%;
    margin: 0;
    padding: 0;
    }
    #map {
    width: 100%;
    height: 400px;
    }
    .controls {
    margin-top: 10px;
    border: 1px solid transparent;
    border-radius: 2px 0 0 2px;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    height: 32px;
    outline: none;
    box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    }
    #searchInput {
    background-color: #fff;
    font-family: Roboto;
    font-size: 15px;
    font-weight: 300;
    margin-left: 12px;
    padding: 0 11px 0 13px;
    text-overflow: ellipsis;
    width: 50%;
    }
    #searchInput:focus {
    border-color: #4d90fe;
    }
</style>
<section>
    <div class="mainwrapper">
        <?php
			include "inc/kiri.php";
			$pageTitle = 'Recipe';
			?>
        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="pageicon pull-left">
                        <i class="fa fa-picture-o"></i>
                    </div>
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href="mod-beranda.htm"><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="mod-beranda.htm">Beranda</a></li>
                            <li>Informasi <?php echo $pageTitle;?></li>
                        </ul>
                        <h4>Informasi <?php echo $pageTitle;?></h4>
                    </div>
                </div>
                <!-- media -->
            </div>
            <!-- pageheader -->
            <div class="contentpanel">
                <?php if($_GET['proses']==1){ ?>
					<?php if(isset($_GET['info'])) include "inc/form-alert.php"; ?>
                <form action="mod/<?php echo"$mod/aksi.php?mod=$mod&ale=2&url=$_GET[url]"; ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" id="signup-daftar">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns">
                                <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                            </div>
                            <!-- panel-btns -->
                            <h4 class="panel-title">Data <?php echo $pageTitle;?></h4>
                            <p>Silahkan lengkapi data dibawah ini.</p>
                        </div>
                        <div class="panel-body nopadding">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Member :</label>
                                <div class="col-sm-10">
                                    <select name="inputs[id_member]" class="pilih" style="width: 100%;">
                                    <?php
                                        $sql=mysqli_query($koneksi,"select id_member,username from member order by username asc");
                                        while($b=mysqli_fetch_array($sql)){
                                        	echo"<option value='$b[id_member]'>$b[username]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Recipe Title :</label>
								<div class="col-sm-10">
									<input type="text" name="inputs[nama_recipe]" class="form-control" placeholder="Write Name" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Cover Photo <span class="f-merah">*</span></label>
								<div class="col-sm-10">
									<div class="fileinput fileinput-new" data-provides="fileinput">
										<div class="fileinput-preview upload" data-trigger="fileinput"></div>
										<input type="file" name="gambar_photo" class="hidden" required>
									</div>
									<p class="help-block">Image used have to be in JPG, PNG, and GIF format and the maximum total images size is 50MB also minimal dimension must be 800x600 pixel.</p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Category :</label>
								<div class="col-sm-10">
									<select name="inputs[id_recipe_category]" required class="pilih" style="width: 100%;">
										<option value="">Select Category</option>
										<?php
											$sql=mysqli_query($koneksi,"select * from recipe_category order by nama_recipe_category asc");
											while($a=mysqli_fetch_array($sql)){
												echo"<option value='$a[id_recipe_category]'>$a[nama_recipe_category]</option>";
											}
											?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Cuisine :</label>
								<div class="col-sm-10">
									<select name="inputs[id_cuisine]" required class="pilih" style="width: 100%;">
										<option value="">Select Cuisine</option>
										<?php
											$sql=mysqli_query($koneksi,"select * from cuisine order by nama_cuisine asc");
											while($f=mysqli_fetch_array($sql)){
												echo"<option value='$f[id_cuisine]'>$f[nama_cuisine]</option>";
											}
											?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Serving Pax :</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" placeholder="ex: 1 pax - 2 pax" name="inputs[serving_pax]">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Dish :</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" placeholder="Write Dish" name="inputs[dish]">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">MSG Level :</label>
								<div class="col-sm-10">
									<select name="inputs[id_msg_level]" class="pilih" style="width: 100%;">
										<option>Select MSG Level</option>
										<?php
											$sql=mysqli_query($koneksi,"select * from msg_level order by nama_msg_level asc");
											while($m=mysqli_fetch_array($sql)){
												echo"<option value='$m[id_msg_level]'>$m[nama_msg_level]</option>";
											}
											?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Difficulty :</label>
								<div class="col-sm-10">
									<select name="inputs[id_difficulty]" class="pilih" style="width: 100%;">
									<?php
										$sql=mysqli_query($koneksi,"select * from difficulty order by nama_difficulty asc");
										while($d=mysqli_fetch_array($sql)){
											echo"<option value='$d[id_difficulty]'>$d[nama_difficulty]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Cooking Methode :</label>
								<div class="col-sm-10">
									<select name="inputs[id_cooking_methode]" class="pilih" style="width: 100%;">
										<option>Select Cooking Methode</option>
										<?php
											$sql=mysqli_query($koneksi,"select * from cooking_methode order by nama_cooking_methode asc");
											while($e=mysqli_fetch_array($sql)){
												echo"<option value='$e[id_cooking_methode]'>$e[nama_cooking_methode]</option>";
											}
											?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Duration :</label>
								<div class="col-sm-10">
									<select name="inputs[id_duration]" class="pilih" style="width: 100%;">
									<?php
										$sql=mysqli_query($koneksi,"select * from duration order by nama_duration asc");
										while($g=mysqli_fetch_array($sql)){
											echo"<option value='$g[id_duration]'>$g[nama_duration]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Pork Serving :</label>
								<div class="col-sm-10">
									<?php foreach( array('Yes'=>'Yes', 'No'=>'No', 'Halal'=>'Halal', 'Vegetarian'=>'Vegetarian') as $label => $value ):?>
										<div class="radio radio-inline">
											<label> <input type="radio" <?php if ($value=='Yes') echo "checked";?> name="inputs[pork_serving]" value="<?php echo $value;?>"> <?php echo $label;?> </label>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Ingredients :</label>
								<div class="col-sm-10">
									<textarea class="form-control mb5" name="inputs[ingredient]" placeholder="Write Ingredients and Amount"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Seasoning :</label>
								<div class="col-sm-10">
									<textarea class="form-control mb5" name="inputs[seasoning]" placeholder="Write Seasoning"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Way of Cooking :</label>
								<div class="col-sm-10">
									<textarea class="form-control mb5" name="inputs[way_of_cooking]" placeholder="Write Way of Cooking"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Smart Tips :</label>
								<div class="col-sm-10">
									<textarea class="form-control mb5" name="inputs[smart_tips]" placeholder="Write Smart Tips"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Reference :</label>
								<div class="col-sm-10">
									<textarea class="form-control mb5" name="inputs[reference]" placeholder="Write Reference"></textarea>
								</div>
							</div>
                        </div>
                        <!-- panel-body -->
                    </div>
                    <!-- panel -->
                    <div class="panel panel-default">
                        <div class="panel-footer">
                            <button class="btn btn-primary mr5">Simpan</button>
                            <a href="mod-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-warning">Kembali</a>
                        </div>
                        <!-- panel-footer -->
                    </div>
                    <!-- panel -->
                </form>
                <?php

                    }







                    elseif ($_GET['proses']==3) {
                      $dat=mysqli_query($koneksi,"select * from member m, negara n where m.id_negara = n.id_negara and m.id_member='$_GET[id]'");
              				$d=mysqli_fetch_array($dat);
                      ?>
                      <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns">
                                <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                            </div><!-- panel-btns -->
                            <h4 class="panel-title">Data Member</h4>
                            <p>Silahkan lengkapi data dibawah ini.</p>
                        </div>
                        <div class="panel-body nopadding">
                          <div class="form-group">
                            <label class="col-sm-1 control-label">Total</label>
                            <div class="col-sm-10">
                              <div class="col-sm-10 control-label"> :
                                <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT id_recipe FROM `recipe`")); echo "$juto"; ?>
                              </div>
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="col-sm-1 control-label">Type Serving</label>
                            <div class="col-sm-10">
                              :  <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT  COUNT(*) FROM recipe GROUP BY pork_serving")); echo "$juto"; ?>
                            </div>
                          </div>
            							<div class="form-group">
                            <label class="col-sm-1 control-label"></label>
                            <div class="col-sm-10">
                              <table width='100%'>
                                <tr>
                                  <td width='40%'>
                                    <label>Category</label>
                                  </td>
                                  <td>
                                    <label>Total</label>
                                  </td>
                                  <td>
                                    <label>Percentage</label>
                                  </td>
                                </tr>
                                <?php
                                $sql=mysqli_query($koneksi, "SELECT n.nama_recipe_category,
            											(SELECT COUNT(*) FROM recipe) as total, COUNT(*) as jumlah,
            											concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM recipe )) * 100 ),2),'%') AS 'percentage'
            											FROM recipe m JOIN recipe_category n ON m.id_recipe_category = n.id_recipe_category GROUP BY n.nama_recipe_category");
                                while ($juto=mysqli_fetch_array($sql)) {
            											$total= $juto['total'];
                                  echo"
                                    <tr>
                                      <td width='40%'>
                                        $juto[nama_recipe_category]
                                      </td>
                                      <td width='30%'>
                                        $juto[jumlah]
                                      </td>
                                      <td width='30%'>
                                        $juto[percentage]
                                      </td>
                                    </tr>
                                  ";
                                };
                                 ?>
            										 <tr style="border-top: 1px solid; padding: 5px 0">
            											 <td> <label>Sum</label>
            											 </td>
            										 	<td>
            										 		<label><?php echo $total; ?></label>
            										 	</td>
            											<td>
            												<label> 100% </label>
            											</td>
            										 </tr>
                               </table>
                            </div>
                          </div>


            							<div class="form-group">
                            <label class="col-sm-1 control-label">Category</label>
                            <div class="col-sm-10">
                              :  <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT  COUNT(*) FROM recipe GROUP BY id_recipe_category")); echo "$juto"; ?>
                            </div>
                          </div>
            							<div class="form-group">
                            <label class="col-sm-1 control-label"></label>
                            <div class="col-sm-10">
                              <table width='100%'>
                                <tr>
                                  <td width='40%'>
                                    <label>Category</label>
                                  </td>
                                  <td>
                                    <label>Total</label>
                                  </td>
                                  <td>
                                    <label>Percentage</label>
                                  </td>
                                </tr>
                                <?php
                                $sql=mysqli_query($koneksi, "SELECT n.nama_recipe_category,
            											(SELECT COUNT(*) FROM recipe) as total, COUNT(*) as jumlah,
            											concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM recipe )) * 100 ),2),'%') AS 'percentage'
            											FROM recipe m JOIN recipe_category n ON m.id_recipe_category = n.id_recipe_category GROUP BY n.nama_recipe_category");
                                while ($juto=mysqli_fetch_array($sql)) {
            											$total= $juto['total'];
                                  echo"
                                    <tr>
                                      <td width='40%'>
                                        $juto[nama_recipe_category]
                                      </td>
                                      <td width='30%'>
                                        $juto[jumlah]
                                      </td>
                                      <td width='30%'>
                                        $juto[percentage]
                                      </td>
                                    </tr>
                                  ";
                                };
                                 ?>
            										 <tr style="border-top: 1px solid; padding: 5px 0">
            											 <td> <label>Sum</label>
            											 </td>
            										 	<td>
            										 		<label><?php echo $total; ?></label>
            										 	</td>
            											<td>
            												<label> 100% </label>
            											</td>
            										 </tr>
                               </table>
                            </div>
                          </div>

            							<div class="form-group">
                            <label class="col-sm-1 control-label">Msg Level</label>
                            <div class="col-sm-10">
                              :  <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT  COUNT(*) FROM recipe GROUP BY id_msg_level")); echo "$juto"; ?>
                            </div>
                          </div>
            							<div class="form-group">
                            <label class="col-sm-1 control-label"></label>
                            <div class="col-sm-10">
                              <table width='100%'>
                                <tr>
                                  <td width='40%'>
                                    <label>Price Index</label>
                                  </td>
                                  <td>
                                    <label>Total</label>
                                  </td>
                                  <td>
                                    <label>Percentage</label>
                                  </td>
                                </tr>
                                <?php
                                $sql=mysqli_query($koneksi, "SELECT p.nama_msg_level,
            											(SELECT COUNT(*) FROM recipe) as total, COUNT(*) as jumlah,
            											concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM recipe )) * 100 ),2),'%') AS 'percentage'
            											FROM `recipe` f LEFT JOIN msg_level p on f.id_msg_level=p.id_msg_level GROUP BY f.id_msg_level");
                                while ($juto=mysqli_fetch_array($sql)) {
            											$total= $juto['total'];
                                  echo"
                                    <tr>
                                      <td width='40%'>";
            													if ($juto['nama_msg_level'] == '') {
            														echo "Unknown";
            													}
            													echo "
                                        $juto[nama_msg_level]
                                      </td>
                                      <td width='30%'>
                                        $juto[jumlah]
                                      </td>
                                      <td width='30%'>
                                        $juto[percentage]
                                      </td>
                                    </tr>
                                  ";
                                };
                                 ?>
            										 <tr style="border-top: 1px solid; padding: 5px 0">
            											 <td> <label>Sum</label>
            											 </td>
            										 	<td>
            										 		<label><?php echo $total; ?></label>
            										 	</td>
            											<td>
            												<label> 100% </label>
            											</td>
            										 </tr>
                               </table>
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="col-sm-1 control-label">Difficulty</label>
                            <div class="col-sm-10">
                              :  <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT  COUNT(*) FROM recipe GROUP BY id_difficulty")); echo "$juto"; ?>
                            </div>
                          </div>
            							<div class="form-group">
                            <label class="col-sm-1 control-label"></label>
                            <div class="col-sm-10">
                              <table width='100%'>
                                <tr>
                                  <td width='40%'>
                                    <label>Price Index</label>
                                  </td>
                                  <td>
                                    <label>Total</label>
                                  </td>
                                  <td>
                                    <label>Percentage</label>
                                  </td>
                                </tr>
                                <?php
                                $sql=mysqli_query($koneksi, "SELECT p.nama_difficulty,
            											(SELECT COUNT(*) FROM recipe) as total, COUNT(*) as jumlah,
            											concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM recipe )) * 100 ),2),'%') AS 'percentage'
            											FROM `recipe` f LEFT JOIN difficulty p on f.id_difficulty=p.id_difficulty GROUP BY f.id_difficulty");
                                while ($juto=mysqli_fetch_array($sql)) {
            											$total= $juto['total'];
                                  echo"
                                    <tr>
                                      <td width='40%'>";
            													if ($juto['nama_difficulty'] == '') {
            														echo "Unknown";
            													}
            													echo "
                                        $juto[nama_difficulty]
                                      </td>
                                      <td width='30%'>
                                        $juto[jumlah]
                                      </td>
                                      <td width='30%'>
                                        $juto[percentage]
                                      </td>
                                    </tr>
                                  ";
                                };
                                 ?>
            										 <tr style="border-top: 1px solid; padding: 5px 0">
            											 <td> <label>Sum</label>
            											 </td>
            										 	<td>
            										 		<label><?php echo $total; ?></label>
            										 	</td>
            											<td>
            												<label> 100% </label>
            											</td>
            										 </tr>
                               </table>
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="col-sm-1 control-label">Cooking Methode</label>
                            <div class="col-sm-10">
                              :  <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT  COUNT(*) FROM recipe GROUP BY id_cooking_methode")); echo "$juto"; ?>
                            </div>
                          </div>
            							<div class="form-group">
                            <label class="col-sm-1 control-label"></label>
                            <div class="col-sm-10">
                              <table width='100%'>
                                <tr>
                                  <td width='40%'>
                                    <label>Cooking Methode</label>
                                  </td>
                                  <td>
                                    <label>Total</label>
                                  </td>
                                  <td>
                                    <label>Percentage</label>
                                  </td>
                                </tr>
                                <?php
                                $sql=mysqli_query($koneksi, "SELECT p.nama_cooking_methode,
            											(SELECT COUNT(*) FROM recipe) as total, COUNT(*) as jumlah,
            											concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM recipe )) * 100 ),2),'%') AS 'percentage'
            											FROM `recipe` f LEFT JOIN cooking_methode p on f.id_cooking_methode=p.id_cooking_methode GROUP BY f.id_cooking_methode");
                                while ($juto=mysqli_fetch_array($sql)) {
            											$total= $juto['total'];
                                  echo"
                                    <tr>
                                      <td width='40%'>";
            													if ($juto['nama_cooking_methode'] == '') {
            														echo "Unknown";
            													}
            													echo "
                                        $juto[nama_cooking_methode]
                                      </td>
                                      <td width='30%'>
                                        $juto[jumlah]
                                      </td>
                                      <td width='30%'>
                                        $juto[percentage]
                                      </td>
                                    </tr>
                                  ";
                                };
                                 ?>
            										 <tr style="border-top: 1px solid; padding: 5px 0">
            											 <td> <label>Sum</label>
            											 </td>
            										 	<td>
            										 		<label><?php echo $total; ?></label>
            										 	</td>
            											<td>
            												<label> 100% </label>
            											</td>
            										 </tr>
                               </table>
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="col-sm-1 control-label">Duration</label>
                            <div class="col-sm-10">
                              :  <?php $juto=mysqli_num_rows(mysqli_query($koneksi, "SELECT  COUNT(*) FROM recipe GROUP BY id_duration")); echo "$juto"; ?>
                            </div>
                          </div>
            							<div class="form-group">
                            <label class="col-sm-1 control-label"></label>
                            <div class="col-sm-10">
                              <table width='100%'>
                                <tr>
                                  <td width='40%'>
                                    <label>Duration</label>
                                  </td>
                                  <td>
                                    <label>Total</label>
                                  </td>
                                  <td>
                                    <label>Percentage</label>
                                  </td>
                                </tr>
                                <?php
                                $sql=mysqli_query($koneksi, "SELECT p.nama_duration,
            											(SELECT COUNT(*) FROM recipe) as total, COUNT(*) as jumlah,
            											concat(round(( (COUNT(*) / (SELECT COUNT(*) FROM recipe )) * 100 ),2),'%') AS 'percentage'
            											FROM `recipe` f LEFT JOIN duration p on f.id_duration=p.id_duration GROUP BY f.id_duration");
                                while ($juto=mysqli_fetch_array($sql)) {
            											$total= $juto['total'];
                                  echo"
                                    <tr>
                                      <td width='40%'>";
            													if ($juto['nama_duration'] == '') {
            														echo "Unknown";
            													}
            													echo "
                                        $juto[nama_duration]
                                      </td>
                                      <td width='30%'>
                                        $juto[jumlah]
                                      </td>
                                      <td width='30%'>
                                        $juto[percentage]
                                      </td>
                                    </tr>
                                  ";
                                };
                                 ?>
            										 <tr style="border-top: 1px solid; padding: 5px 0">
            											 <td> <label>Sum</label>
            											 </td>
            										 	<td>
            										 		<label><?php echo $total; ?></label>
            										 	</td>
            											<td>
            												<label> 100% </label>
            											</td>
            										 </tr>
                               </table>
                            </div>
                          </div>
                        </div>
                      </div>
                    <?php









                  } else{
                          $edit=mysqli_query($koneksi,"select * from recipe where id_recipe = '$_GET[id]'");
                          $e=mysqli_fetch_array($edit);
                    ?>
                <form action="mod/<?php echo"$mod/aksi.php?mod=$mod&ale=3&url=$_GET[url]"; ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" id="signup-daftar">
                    <input type="hidden" name="id" value="<?php echo $e['id_recipe']; ?>">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns">
                                <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                            </div>
                            <!-- panel-btns -->
                            <h4 class="panel-title">Data  <?php echo $pageTitle;?></h4>
                            <p>Silahkan lengkapi data dibawah ini.</p>
                        </div>
                        <div class="panel-body nopadding">

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Member :</label>
                                <div class="col-sm-10">
                                    <select name="inputs[id_member]" class="pilih" style="width: 100%;">
                                    <?php
                                        $sql=mysqli_query($koneksi,"select id_member,username from member order by username asc");
                                        while($b=mysqli_fetch_array($sql)){
											$selected = ($b['id_member'] == $e['id_member'])? "selected": "";
                                        	echo"<option value='$b[id_member]' {$selected}>$b[username]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Recipe Title :</label>
								<div class="col-sm-10">
									<input type="text" name="inputs[nama_recipe]" class="form-control" value="<?php echo $e['nama_recipe']?>" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Category :</label>
								<div class="col-sm-10">
									<select name="inputs[id_recipe_category]" required class="pilih" style="width: 100%;">
										<option value="">Select Category</option>
										<?php
											$sql=mysqli_query($koneksi,"select * from recipe_category order by nama_recipe_category asc");
											while($a=mysqli_fetch_array($sql)){
												$selected = ($a['id_recipe_category'] == $e['id_recipe_category'])? "selected": "";
												echo"<option value='$a[id_recipe_category]' {$selected}>$a[nama_recipe_category]</option>";
											}
											?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Cuisine :</label>
								<div class="col-sm-10">
									<select name="inputs[id_cuisine]" required class="pilih" style="width: 100%;">
										<option value="">Select Cuisine</option>
										<?php
											$sql=mysqli_query($koneksi,"select * from cuisine order by nama_cuisine asc");
											while($f=mysqli_fetch_array($sql)){
												$selected = ($f['id_cuisine'] == $e['id_cuisine'])? "selected": "";
												echo"<option value='$f[id_cuisine]' {$selected}>$f[nama_cuisine]</option>";
											}
											?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Serving Pax :</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" value="<?php echo $e['serving_pax']?>" name="inputs[serving_pax]">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Dish :</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" value="<?php echo $e['dish']?>" name="inputs[dish]">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">MSG Level :</label>
								<div class="col-sm-10">
									<select name="inputs[id_msg_level]" class="pilih" style="width: 100%;">
										<option>Select MSG Level</option>
										<?php
											$sql=mysqli_query($koneksi,"select * from msg_level order by nama_msg_level asc");
											while($m=mysqli_fetch_array($sql)){
												$selected = ($m['id_msg_level'] == $e['id_msg_level'])? "selected": "";
												echo"<option value='$m[id_msg_level]' {$selected}>$m[nama_msg_level]</option>";
											}
											?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Difficulty :</label>
								<div class="col-sm-10">
									<select name="inputs[id_difficulty]" class="pilih" style="width: 100%;">
									<?php
										$sql=mysqli_query($koneksi,"select * from difficulty order by nama_difficulty asc");
										while($d=mysqli_fetch_array($sql)){
											$selected = ($d['id_difficulty'] == $e['id_difficulty'])? "selected": "";
											echo"<option value='$d[id_difficulty]' {$selected}>$d[nama_difficulty]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Cooking Methode :</label>
								<div class="col-sm-10">
									<select name="inputs[id_cooking_methode]" class="pilih" style="width: 100%;">
										<option>Select Cooking Methode</option>
										<?php
											$sql=mysqli_query($koneksi,"select * from cooking_methode order by nama_cooking_methode asc");
											while($k=mysqli_fetch_array($sql)){
												$selected = ($k['id_cooking_methode'] == $e['id_cooking_methode'])? "selected": "";
												echo"<option value='$k[id_cooking_methode]' {$selected}>$k[nama_cooking_methode]</option>";
											}
											?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Duration :</label>
								<div class="col-sm-10">
									<select name="inputs[id_duration]" class="pilih" style="width: 100%;">
									<?php
										$sql=mysqli_query($koneksi,"select * from duration order by nama_duration asc");
										while($g=mysqli_fetch_array($sql)){
											$selected = ($g['id_duration'] == $e['id_duration'])? "selected": "";
											echo"<option value='$g[id_duration]' {$selected}>$g[nama_duration]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Pork Serving :</label>
								<div class="col-sm-10">
									<?php foreach( array('Yes'=>'Yes', 'No'=>'No', 'Halal'=>'Halal', 'Vegetarian'=>'Vegetarian') as $label => $value ):?>
										<div class="radio radio-inline">
											<label>
												<input type="radio" <?php if ($e['pork_serving'] == $value){  echo "checked";}?> name="inputs[pork_serving]" value="<?php echo $value;?>">
												<?php echo $label;?>
											</label>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Ingredients :</label>
								<div class="col-sm-10">
									<textarea class="form-control mb5" name="inputs[ingredient]" placeholder="Write Ingredients and Amount"><?php echo $e['ingredient']?></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Seasoning :</label>
								<div class="col-sm-10">
									<textarea class="form-control mb5" name="inputs[seasoning]" placeholder="Write Seasoning"><?php echo $e['seasoning']?></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Way of Cooking :</label>
								<div class="col-sm-10">
									<textarea class="form-control mb5" name="inputs[way_of_cooking]" placeholder="Write Way of Cooking"><?php echo $e['way_of_cooking']?></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Smart Tips :</label>
								<div class="col-sm-10">
									<textarea class="form-control mb5" name="inputs[smart_tips]" placeholder="Write Smart Tips"><?php echo $e['smart_tips']?></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Reference :</label>
								<div class="col-sm-10">
									<textarea class="form-control mb5" name="inputs[reference]" placeholder="Write Reference"><?php echo $e['reference']?></textarea>
								</div>
							</div>
































                        </div>
                        <!-- panel-body -->
                    </div>
                    <!-- panel -->
                    <div class="panel panel-default">
                        <div class="panel-footer">
                            <button class="btn btn-primary mr5">Simpan</button>
                            <a href="mod-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-warning">Kembali</a>
                        </div>
                        <!-- panel-footer -->
                    </div>
                    <!-- panel -->
                </form>
                <?php
                    } ?>
            </div>
            <!-- contentpanel -->
        </div>
        <!-- mainpanel -->
    </div>
    <!-- mainwrapper -->
</section>
<style media="screen">
    #country-list {
    margin: 0;
    list-style: none;
    padding: 0px;
    max-height: 155px;
    position: relative;
    overflow: auto;
    }
    .ko {
    border: 1px solid #CCC;
    padding: 5px;
    color: #9C9999;
    border-top: none;
    cursor: pointer;
    transition: 0.7s all 0s ease;
    }
    .ko:hover{
    background: #ffeed0;
    transition: 0.3s all 0s ease;
    }
</style>
<script src="js/jquery-1.11.1.min.js"></script>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$(".pilih").select2();

		tinymce.init({ selector:'textarea' });
	});
</script>
