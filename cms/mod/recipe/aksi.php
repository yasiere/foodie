<?php
session_start();
if(!empty($_SESSION['ak_id'])){
	include "../../inc/db.php";
	include "../../inc/upload.php";
	include "../../inc/id_masking.php";
	include '../../inc/seo.php';
	include "../../../config/func/upload_gambar_bms.php";

	$mod			= $_GET['mod'];
	$tglhariini		= date("Y-m-d");
	$ukuran_maks	= 552428800;
	$error_cover 	= 0;
	$error_profil 	= 0;

	if($_GET['ale']==1){
		mysqli_query($koneksi,"DELETE FROM recipe WHERE id_recipe='$_GET[id]'");
		mysqli_query($koneksi,"DELETE FROM recipe_photo WHERE id_recipe='$_GET[id]'");
		unlink("../../../assets/img/recipe/small_".$_GET['gambarthumb']);
		unlink("../../../assets/img/recipe/medium_".$_GET['gambarthumb']);
		unlink("../../../assets/img/recipe/big_".$_GET['gambarthumb']);
		header("Location: ../../mod-recipe-10.htm");
	}
	elseif($_GET['ale']==2){ 
		date_default_timezone_set("Asia/Jakarta");
		$sekarang=date("Y-m-d H:i:s");

		$ukuran_maks	= 552428800;

		$nama_cover			= $_FILES['gambar_photo']['name'];
		$ukuran_cover 		= $_FILES['gambar_photo']['size'];
		$lokasi_cover		= $_FILES['gambar_photo']['tmp_name'];
		list($width_cover, $height_cover) = getimagesize($lokasi_cover);
		if($ukuran_cover > $ukuran_maks or !preg_match("/.(gif|jpg|png)$/i", $nama_cover) or $width_cover < 800 or $height_cover < 600){
			$_SESSION['notif']     = "gambar";
			header("Location: ../../add-recipe-10.htm");
			exit();
		}
		else{
			$post = $_POST['inputs'];
			$sql=mysqli_query($koneksi,"INSERT INTO recipe(". implode(',', array_keys($post)) .", tgl_post) VALUES('". implode("','", array_values($post)) ."','{$sekarang}')");
			if($sql){
				$post = $_POST['inputs'];
				$cover=upload_gambar($nama_cover,$lokasi_cover,"../../../assets/img/recipe");
				$id_recipe=mysqli_insert_id($koneksi);
				mysqli_query($koneksi,"INSERT INTO recipe_photo (id_member,id_recipe,nama_recipe_photo,gambar_recipe_photo) VALUES('$post[id_member]','$id_recipe','$post[nama_recipe]','$cover')");
				mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('recipe','$id_recipe')");
				// if($_POST['feed_submit']==1){
					mysqli_query($koneksi,"INSERT INTO activity (id_member,kat_act,deskripsi,tgl) VALUES('$post[id_member]','feed_submit','Submit new recipe review','$sekarang')");
				// }
				$id = id_masking($id_recipe);
				header("Location: ../../mod-recipe-10.htm");
				exit();
			}
		}
	}



	elseif($_GET['ale']==3){
		date_default_timezone_set("Asia/Jakarta");
		$sekarang=date("Y-m-d H:i:s");
				
		$upadate = array();
		foreach($_POST['inputs'] as $name => $val)
		{
			$upadate[] = $name ."='". $val ."'";
		}

		mysqli_query($koneksi,"update recipe set ". implode(',', $upadate) ." where id_recipe='$_POST[id]'");
		$_SESSION['notif'] = "sukses";
		header("Location: ../../mod-recipe-10.htm");
		exit();
	}
}
else{
	header('location:not-found.htm');
}
?>
