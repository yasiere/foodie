<section>
    <div class="mainwrapper">
        <?php include "inc/kiri.php"; ?>                
        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="pageicon pull-left">
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href="mod-beranda.htm"><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="mod-beranda.htm">Beranda</a></li>
                            <li>Akun</li>
                        </ul>
                        <h4>Akun Info</h4>
                    </div>
                </div><!-- media -->
            </div><!-- pageheader -->
                    
            <div class="contentpanel">
				<?php
				if(isset($_GET['info'])){
					if($_GET['info']==1){
						echo"<div class='alert alert-success'>
							<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
							<strong>Sukses!</strong> data berhasil dihapus.
						</div>";
					}
				}
				?>
                <div class="panel panel-default">
                    <div class="panel-heading">
						<div class="row">
							<div class="col-sm-11">
								<h4 class="panel-title">Akun Info</h4>
								<p>Pada sesi ini anda dapat melihat informasi akun. Anda dapat membuat, mengedit dan menghapus data apabila status anda adalah Administrator.</p>
							</div>
							
						</div>
					</div>
                    <div class="panel-body">        
						<table id="aledata" class="table table-striped table-bordered responsive">
							<thead class="">
								<tr>
									<th>No</th>
									<th>Nama</th>
									<th>Email</th>
									<th>Status</th>
									<th>No Telpon</th>
									<th>Alamat</th>
									<th>Tgl Registrasi</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no=1;
								$sql=mysqli_query($koneksi,"select * from users order by nama asc");
								if($u['aktifasi']==2){
									$sql=mysqli_query($koneksi,"select * from users where id_users ='$u[id_users]' or aktifasi > 2 order by nama asc");
								}
								elseif($u['aktifasi'] > 2){
									$sql=mysqli_query($koneksi,"select * from users where id_users ='$u[id_users]'");
								}
								while($r=mysqli_fetch_array($sql)){
									$tgl=tgl_indo($r['tgl_daftar']);
									$status="Super Admin";
									if($r['aktifasi']==2){
										$status="Admin";
									}
									elseif($r['aktifasi']==3){
										$status="Approver";
									}
									elseif($r['aktifasi']==4){
										$status="Reservasi";
									}
									elseif($r['aktifasi']==5){
										$status="Marketing";
									}
									echo"<tr>
										<td width='30px'>$no.</td>
										<td>$r[nama]</td>
										<td>$r[email]</td>
										<td>$status</td>
										<td>$r[no_telpon]</td>
										<td>$r[alamat]</td>
										<td>$tgl</td>
										<td width='170px' class='text-center'>
											<a href='edit-$mod-$r[id_users]-$_GET[url].htm' class='btn btn-success btn-rounded btn-sm'><i class='fa fa-pencil'></i> Edit</a>";
											if($u['aktifasi']<>3 and $r['id_users']<>$u['id_users']){
												echo"<a class='btn btn-danger btn-rounded btn-sm' href='mod/$mod/aksi.php?mod=$mod&id=$r[id_users]&ale=1&url=$_GET[url]' onClick=\"return confirm('apakah anda yakin akan menghapus data ini ?')\"><i class='fa fa-trash-o'></i> Hapus</a>";
											}
										echo"</td>
									</tr>";
									$no++;
								}
								?>
							</tbody>
						</table>
					</div>
                </div><!-- panel -->
            </div><!-- contentpanel -->
        </div><!-- mainpanel -->
    </div><!-- mainwrapper -->
</section>

<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/pace.min.js"></script>
<script src="js/retina.min.js"></script>
<script src="js/jquery.cookies.js"></script>

<script src="js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/plug-ins/725b2a2115b/integration/bootstrap/3/dataTables.bootstrap.js"></script>
<script src="//cdn.datatables.net/responsive/1.0.1/js/dataTables.responsive.js"></script>      
<script src="js/custom.js"></script>
<script>
jQuery(document).ready(function(){
	jQuery('#aledata').dataTable({
		"sPaginationType": "full_numbers",
		"iDisplayLength": 25,
		"aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, "Semua"]]
    });
});
</script>