<section>
    <div class="mainwrapper">
        <?php include "inc/kiri.php"; ?>                
         <div class="mainpanel">
             <div class="pageheader">
                <div class="media">
                    <div class="pageicon pull-left">
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href="mod-beranda.htm"><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="mod-beranda.htm">Beranda</a></li>
                            <li>Akun</li>
                        </ul>
                        <h4>Akun Info</h4>
                    </div>
                </div><!-- media -->
            </div><!-- pageheader -->
            <div class="contentpanel">
				<?php if($_GET['proses']==1){ ?>
				<form action="mod/<?php echo"$mod/aksi.php?mod=$mod&ale=2&url=$_GET[url]"; ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data">
				<input type="hidden" name="url" <?php echo"value='$_GET[url]'"; ?>>
				<?php
				if(isset($_GET['info'])){
					echo"<div class='alert alert-success'>
						<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
						<strong>Sukses!</strong> data berhasil disimpan.
					</div>";
				}
				?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-btns">
                            <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                        </div><!-- panel-btns -->
						<h4 class="panel-title">Detail Akun</h4>
                        <p>Silahkan lengkapi form dibawah ini.</p>
                    </div>
                    <div class="panel-body nopadding">
						<div class="form-group">
                            <label class="col-sm-2 control-label">Nama Akun <span class="asterisk">*</span></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control"  name="nama" required>
                            </div>
                        </div><!-- form-group -->
						<div class="form-group">
                            <label class="col-sm-2 control-label">Email<span class="asterisk">*</span></label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control"  name="email" required autocomplete="off">
                            </div>
                        </div><!-- form-group -->
						<div class="form-group">
                            <label class="col-sm-2 control-label">Password <span class="asterisk">*</span></label>
                            <div class="col-sm-1">
                                <input type="password" class="form-control"  name="pass" required title="Disarankan terdiri dari angka dan huruf, maksimal 10 karakter." autocomplete="off" maxlength="10">
                            </div>
                        </div><!-- form-group -->
						<div class="form-group">
                            <label class="col-sm-2 control-label">Alamat</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control"  name="alamat">
                            </div>
                        </div><!-- form-group -->
						<div class="form-group">
                            <label class="col-sm-2 control-label">No Telpon</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control"  name="no_telpon">
                            </div>
                        </div><!-- form-group -->
						<?php
						if($u['aktifasi']==1){
							echo"<div class='form-group'>
                            <label class='col-sm-2 control-label'>Status Akun <span class='asterisk'>*</span></label>
                            <div class='col-sm-1'>
								<select name='status' class='form-control' required>
									<option value='2'>Admin</option>
									<option value='3'>Approver</option>
									<option value='4'>Reservasi</option>
									<option value='5'>Marketing</option>
								</select>
                            </div>
                        </div>";
						}
						elseif($u['aktifasi']==2){
							echo"<div class='form-group'>
                            <label class='col-sm-2 control-label'>Status Akun <span class='asterisk'>*</span></label>
                            <div class='col-sm-1'>
								<select name='status' class='form-control' required>
									<option value='3'>Approver</option>
									<option value='4'>Reservasi</option>
									<option value='5'>Marketing</option>
								</select>
                            </div>
                        </div>";
						}
						?>
                    </div><!-- panel-body -->
					<div class="panel-footer">
                        <button class="btn btn-primary mr5">Simpan</button>
                        <a href="mod-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-warning">Kembali</a>
                     </div><!-- panel-footer -->
                </div><!-- panel -->
				</form>
				<?php 
				}else{ 
				$dat=mysqli_query($koneksi,"select * from users where id_users='$_GET[id]'");
				$d=mysqli_fetch_array($dat);
				?>
				<form action="mod/<?php echo"$mod/aksi.php?mod=$mod&ale=3&url=$_GET[url]"; ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data">
				<input type="hidden" name="id" <?php echo"value='$d[id_users]'"; ?>>
				<input type="hidden" name="url" <?php echo"value='$_GET[url]'"; ?>>
				<?php
				if(isset($_GET['info'])){
					echo"<div class='alert alert-success'>
						<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
						<strong>Sukses!</strong> data berhasil disimpan.
					</div>";
				}
				?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-btns">
                            <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                        </div><!-- panel-btns -->
						<h4 class="panel-title">Detail Akun</h4>
                        <p>Silahkan lengkapi form dibawah ini.</p>
                    </div>
                    <div class="panel-body nopadding">
						<div class="form-group">
                            <label class="col-sm-2 control-label">Nama Akun <span class="asterisk">*</span></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control"  name="nama" required value="<?php echo"$d[nama]"; ?>">
                            </div>
                        </div><!-- form-group -->
						<div class="form-group">
                            <label class="col-sm-2 control-label">Email<span class="asterisk">*</span></label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control"  name="email" required autocomplete="off" value="<?php echo"$d[email]"; ?>">
                            </div>
                        </div><!-- form-group -->
						<div class="form-group">
                            <label class="col-sm-2 control-label">Password <span class="asterisk">*</span></label>
                            <div class="col-sm-1">
                                <input type="password" class="form-control"  name="pass" required title="Disarankan terdiri dari angka dan huruf, maksimal 10 karakter." value="<?php echo"$d[password_asli]"; ?>" autocomplete="off" maxlength="10">
                            </div>
                        </div><!-- form-group -->
						<div class="form-group">
                            <label class="col-sm-2 control-label">Alamat</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control"  name="alamat" value="<?php echo"$d[alamat]"; ?>">
                            </div>
                        </div><!-- form-group -->
						<div class="form-group">
                            <label class="col-sm-2 control-label">No Telpon</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control"  name="no_telpon" value="<?php echo"$d[no_telpon]"; ?>">
                            </div>
                        </div><!-- form-group -->
                    </div><!-- panel-body -->
					<div class="panel-footer">
                        <button class="btn btn-primary mr5">Simpan</button>
                        <a href="mod-<?php echo"$mod-$_GET[url]"; ?>.htm" class="btn btn-warning">Kembali</a>
                     </div><!-- panel-footer -->
                </div><!-- panel -->
				</form>
				<?php } ?>
			</div><!-- contentpanel -->
        </div><!-- mainpanel -->
    </div><!-- mainwrapper -->
</section>

<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/jquery-ui-1.10.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/pace.min.js"></script>
<script src="js/retina.min.js"></script>
<script src="js/jquery.cookies.js"></script>

<script src="js/custom.js"></script>