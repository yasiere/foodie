<header>
	<div class="headerwrapper">
		<div class="header-left">
			<a href="mod-beranda-1.htm" class="logo"><img src="<?php echo $base ?>/assets/img/theme/logo.jpg" alt="" style="width: 90px;"/></a>
			<div class="pull-right">
				<a href="" class="menu-collapse"><i class="fa fa-bars"></i></a>
			</div>
		</div>
	<!-- header-left -->
	<div class="header-right">
		<div class="pull-right">
			<div class="btn-group btn-group-option">
				<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
				<i class="fa fa-caret-down"></i>
				</button>
				<ul class="dropdown-menu pull-right" role="menu">
					<li><a href="mod-akun-22.htm"><i class="glyphicon glyphicon-cog"></i> Info Akun</a></li>
					<li class="divider"></li>
					<li><a href="logout.htm"><i class="glyphicon glyphicon-log-out"></i>Sign Out</a></li>
				</ul>
			</div>
			<!-- btn-group -->
		</div>
		<!-- pull-right -->
	</div>
	<!-- header-right -->
</div>
<!-- headerwrapper -->
</header>
