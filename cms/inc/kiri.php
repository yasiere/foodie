<div class="leftpanel">
	<div class="media profile-left">
		<a class="pull-left profile-thumb" href="profile.html">
		<img class="img-circle" src="images/photos/profile.png" alt="">
		</a>
		<div class="media-body">
			<h4 class="media-heading"><?php echo"$u[nama]"; ?></h4>
			<small class="text-muted"><?php echo"$u[email]"; ?></small>
		</div>
	</div>
	<!-- media -->
	<h5 class="leftpanel-title">Navigasi</h5>
	<ul class="nav nav-pills nav-stacked">
		<li <?php if ($_GET['url']==1){echo "class='active'";} ?>><a href="mod-beranda-1.htm"><i class="fa fa-dashboard"></i><span>Home</span></a></li>

        <?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
		<li class="<?php if ($_GET['url']==2){echo "active";} ?>"><a href="mod-member-2.htm"><i class="fa fa-users"></i> <span>Member</span></a></li>
		<?php }
		?>

		<?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
		<li class="<?php if ($_GET['url']==19){echo "active";} ?>"><a href="mod-data-19.htm"><i class="fa fa-bars"></i> <span>Data</span></a></li>
		<?php } ?>

    <?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
			<li class="parent <?php if (($_GET['url']==3) || ($_GET['url']==4) || ($_GET['url']==5)){echo "active";} ?>"><a href="mod-restaurant-3.htm"><i class="fa fa-home"></i> <span>Restaurant</span></a>
				<ul class="children">
					<?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
						<li class="<?php if ($_GET['url']==3){echo "active";} ?>"><a href="mod-restaurant-3.htm"><span>Restaurant</span></a></li>
					<?php }	?>

					<?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
						<li class="<?php if ($_GET['url']==4){echo "active";} ?>"><a href="mod-restaurant_photo-4.htm"><span>Restaurant Photo</span></a></li>
					<?php }	?>

					<?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
						<li class="<?php if ($_GET['url']==5){echo "active";} ?>"><a href="mod-restaurant_menu-5.htm"><span>Restaurant Menu</span></a></li>
					<?php }	?>
        </ul>
			</li>
		<?php }	?>




		<?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
			<li class="parent <?php if (($_GET['url']==6) || ($_GET['url']==7)){echo "active";} ?>"><a href="mod-food-6.htm"><i class="fa fa-cutlery "></i> <span>Food</span></a>
				<ul class="children">
					<?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
						<li class="<?php if ($_GET['url']==6){echo "active";} ?>"><a href="mod-food-6.htm"><span>Food</span></a></li>
					<?php }	?>

					<?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
						<li class="<?php if ($_GET['url']==7){echo "active";} ?>"><a href="mod-food_photo-7.htm"><span>Food Photo</span></a></li>
					<?php }	?>
				</ul>
			</li>
		<?php }	?>




		<?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
			<li class="parent <?php if (($_GET['url']==8) || ($_GET['url']==9)){echo "active";} ?>"><a href="mod-beverage-8.htm"><i class="fa fa-glass"></i> <span>Beverage</span></a>
				<ul class="children">
					<?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
						<li class="<?php if ($_GET['url']==8){echo "active";} ?>"><a href="mod-beverage-8.htm"><span>Beverage</span></a></li>
					<?php }	?>

					<?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
						<li class="<?php if ($_GET['url']==9){echo "active";} ?>"><a href="mod-beverage_photo-9.htm"><span>Beverage Photo</span></a></li>
					<?php }	?>
				</ul>
			</li>
		<?php }	?>





		<?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
			<li class="parent <?php if (($_GET['url']==10) || ($_GET['url']==11)){echo "active";} ?>"><a href="mod-recipe-10.htm"><i class="fa fa-book"></i> <span>Recipe</span></a>
				<ul class="children">
					<?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
						<li class="<?php if ($_GET['url']==10){echo "active";} ?>"><a href="mod-recipe-10.htm"><span>Recipe</span></a></li>
					<?php }	?>

					<?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
						<li class="<?php if ($_GET['url']==11){echo "active";} ?>"><a href="mod-recipe_photo-11.htm"><span>Recipe Photo</span></a></li>
					<?php }	?>
				</ul>
			</li>
		<?php }	?>



		<?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
			<li class="parent <?php if (($_GET['url']==12) || ($_GET['url']==13)){echo "active";} ?>"><a href="mod-article-12.htm"><i class="fa fa-file-text"></i> <span>Article</span></a>
				<ul class="children">
					<?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
						<li class="<?php if ($_GET['url']==12){echo "active";} ?>"><a href="mod-article-12.htm"><span>Article</span></a></li>
					<?php }	?>

					<?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
						<li class="<?php if ($_GET['url']==13){echo "active";} ?>"><a href="mod-article_photo-13.htm"><span>Article Photo</span></a></li>
					<?php }	?>
				</ul>
			</li>
		<?php }	?>



		<?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
			<li class="<?php if ($_GET['url']==14){echo "active";} ?>"><a href="mod-video-14.htm"><i class="fa fa-youtube-square"></i> <span>Video</span></a></li>
		<?php }	?>

		<?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
			<li class="<?php if ($_GET['url']==15){echo "active";} ?>"><a href="mod-fgmart-15.htm"><i class="fa fa-picture-o"></i> <span>FGMart</span></a></li>
		<?php }	?>

		<?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
			<li class="<?php if ($_GET['url']==16){echo "active";} ?>"><a href="mod-news-16.htm"><i class="fa fa-newspaper-o"></i> <span>News</span></a></li>
		<?php }	?>

		<?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
			<li class="<?php if ($_GET['url']==17){echo "active";} ?>"><a href="mod-pages-17.htm"><i class="fa fa-file-text"></i> <span>Pages</span></a></li>
		<?php }	?>

		<?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
			<li class="<?php if ($_GET['url']==18){echo "active";} ?>"><a href="mod-adv-18.htm"><i class="fa fa-file-text"></i> <span>Advertisement</span></a></li>
		<?php }	?>

		<?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
		<li class="<?php if ($_GET['url']==20){echo "active";} ?>"><a href="mod-slider-20.htm"><i class="fa fa-picture-o"></i> <span>Slider</span></a></li>
		<?php } ?>

		<?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
		<li class="<?php if ($_GET['url']==21){echo "active";} ?>"><a href="mod-coupon-21.htm"><i class="fa fa-picture-o"></i> <span>Coupon</span></a></li>
		<?php } ?>

        <?php /*?><?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
		<li class="<?php if ($_GET['url']==2){echo "active";} ?>"><a href="mod-aboutstatis-2.htm"><i class="fa fa-globe"></i> <span>Halaman About</span></a></li>
		<?php }
		?>

        <?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
		<li class="<?php if ($_GET['url']==5){echo "active";} ?>"><a href="mod-indexstatis-5.htm"><i class="fa fa-home"></i> <span>Halaman Index</span></a></li>
		<?php }
		?>

        <?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
		<li class="<?php if ($_GET['url']==3){echo "active";} ?>"><a href="mod-blog-3.htm"><i class="fa fa-pencil-square-o"></i> <span>Blog</span></a></li>
		<?php }
		?>

        <?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
		<li class="<?php if ($_GET['url']==4){echo "active";} ?>"><a href="mod-slider-4.htm"><i class="fa fa-picture-o"></i> <span>Slider</span></a></li>
		<?php }
		?><?php */?>

        <?php /*?><?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
		<li class="<?php if ($_GET['url']==6){echo "active";} ?>"><a href="mod-kategori-6.htm"><i class="fa fa-th-large"></i> <span>Kategori</span></a></li>
		<?php }
		?><?php */?>

        <?php /*?><?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
		<li class="<?php if ($_GET['url']==7){echo "active";} ?>"><a href="mod-kategori-7.htm"><i class="fa fa-th"></i> <span>Kategori</span></a></li>
		<?php }
		?>

		<?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
		<li class="<?php if ($_GET['url']==8){echo "active";} ?>"><a href="mod-produk-8.htm"><i class="fa fa-archive"></i> <span>Produk</span></a></li>
		<?php }
		?> <?php */?>


		<?php /*?><?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
		<li class="<?php if ($_GET['url']==6){echo "active";} ?>"><a href="mod-warehouseplan-6.htm"><i class="fa fa-home"></i> <span>Warehouse Plan</span></a></li>
		<?php }
		?>

		<?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
		<li class="<?php if ($_GET['url']==4){echo "active";} ?>"><a href="mod-newsevent-4.htm"><i class="fa fa-newspaper-o"></i> <span>News and Event</span></a></li>
		<?php }
		?>
		<?php if($u['aktifasi']==1 or $u['aktifasi']==2){ ?>
		<li class="<?php if ($_GET['url']==5){echo "active";} ?>"><a href="mod-slider-5.htm"><i class="fa fa-image"></i> <span>Slider</span></a></li>
		<?php }
		?><?php */?>
	</ul>
</div>

<!-- leftpanel -->
