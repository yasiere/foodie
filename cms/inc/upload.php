<?php
function upload($nama_file,$folder,$lokasi_file,$lebar,$tinggi){
	$nama_folder = "../../../$folder/";
	list($lebar_asli, $tinggi_asli, $source_type) = getimagesize($lokasi_file);
	$gambar_asli = imagecreatefromjpeg($lokasi_file);
	$ukuran_asli = $lebar_asli / $tinggi_asli;
	$ukuran = $lebar / $tinggi;
	if ($ukuran_asli > $ukuran) {
    	$tinggi_sementara = $tinggi;
    	$lebar_sementara = ( int ) ($tinggi * $ukuran_asli);
	} else {
    	/*jika gambar sama atau lebih tinggi*/
    	$lebar_sementara = $lebar;
    	$tinggi_sementara = ( int ) ($lebar / $ukuran_asli);
	}
	/*rubah ukuran gambar ke ukuran sementara*/
	$gambar_sementara = imagecreatetruecolor($lebar_sementara, $tinggi_sementara);
	imagecopyresampled($gambar_sementara,$gambar_asli,0, 0,0, 0,$lebar_sementara, $tinggi_sementara,$lebar_asli, $tinggi_asli);
	/*Copy cropped region from temporary image into the desired GD image*/
	$x_absis = ($lebar_sementara - $lebar) / 2;
	$y_absis = ($tinggi_sementara - $tinggi) / 2;
	$gambar_akhir = imagecreatetruecolor($lebar, $tinggi);
	imagecopy($gambar_akhir,$gambar_sementara,0, 0,$x_absis, $y_absis,$lebar, $tinggi);
	imagejpeg($gambar_akhir,$nama_folder.$nama_file);
	imagedestroy($gambar_akhir);
}

function uploadstatisbanner($uploadName){
    $direktori          = "../../../assets/img/";
    $direktoriThumb     = "../../../assets/img/";
    $file               = $direktori.$uploadName;
   
    //simpan gambar ukuran sebenernya
    $realImagesName     = $_FILES['fupload']['tmp_name'];
    $realImagesType     = $_FILES['fupload']['type'];
    $acak           	= rand(0000,9999);
    move_uploaded_file($realImagesName, $file);
   
    //simpan ukuran thumbs
    $thumbWidth     = 450;
    $thumbHeight    = 390;
	
    //identitas file gambar

    if ($realImagesType=='image/jpeg') {
        $realImages             = imagecreatefromjpeg($file);
        $width                  = imageSX($realImages);
        $height                 = imageSY($realImages);
		
		//mengubah ukuran gambar
		$thumbImage = imagecreatetruecolor($thumbWidth, $thumbHeight);
		imagecopyresampled($thumbImage, $realImages, 0,0,0,0, $thumbWidth, $thumbHeight, $width, $height);
		
		//simpan gambar thumbnail
		imagejpeg($thumbImage,$direktoriThumb.$uploadName);
		
    } else {
        $realImages             = imagecreatefrompng($file);
        $width                  = imageSX($realImages);
        $height                 = imageSY($realImages);
		
		//mengubah ukuran gambar
		$thumbImage = imagecreatetruecolor($thumbWidth, $thumbHeight);
		imagealphablending($thumbImage, false);
		imagesavealpha($thumbImage,true);
		$transparent = imagecolorallocatealpha($thumbImage, 255, 255, 255, 127);
		imagefilledrectangle($thumbImage, 0, 0, $nWidth, $nHeight, $transparent);
	    imagecopyresampled($thumbImage, $realImages, 0,0,0,0, $thumbWidth, $thumbHeight, $width, $height);
		
		//simpan gambar thumbnail
		imagepng($thumbImage,$direktoriThumb.$uploadName);
	 
    } 
   
    //hapus objek gambar dalam memori
    imagedestroy($realImages);
    imagedestroy($thumbImage);
}

function uploadstatis($uploadName){
    $direktori          = "../../../assets/img/";
    $direktoriThumb     = "../../../assets/img/";
    $file               = $direktori.$uploadName;
   
    //simpan gambar ukuran sebenernya
    $realImagesName     = $_FILES['fupload']['tmp_name'];
    $realImagesType     = $_FILES['fupload']['type'];
    $acak           	= rand(0000,9999);
    move_uploaded_file($realImagesName, $file);
   
    //simpan ukuran thumbs
    $thumbWidth     = 470;
    $thumbHeight    = 470;
	
    //identitas file gambar

    if ($realImagesType=='image/jpeg') {
        $realImages             = imagecreatefromjpeg($file);
        $width                  = imageSX($realImages);
        $height                 = imageSY($realImages);
		
		//mengubah ukuran gambar
		$thumbImage = imagecreatetruecolor($thumbWidth, $thumbHeight);
		imagecopyresampled($thumbImage, $realImages, 0,0,0,0, $thumbWidth, $thumbHeight, $width, $height);
		
		//simpan gambar thumbnail
		imagejpeg($thumbImage,$direktoriThumb.$uploadName);
		
    } else {
        $realImages             = imagecreatefrompng($file);
        $width                  = imageSX($realImages);
        $height                 = imageSY($realImages);
		
		//mengubah ukuran gambar
		$thumbImage = imagecreatetruecolor($thumbWidth, $thumbHeight);
		imagealphablending($thumbImage, false);
		imagesavealpha($thumbImage,true);
		$transparent = imagecolorallocatealpha($thumbImage, 255, 255, 255, 127);
		imagefilledrectangle($thumbImage, 0, 0, $nWidth, $nHeight, $transparent);
	    imagecopyresampled($thumbImage, $realImages, 0,0,0,0, $thumbWidth, $thumbHeight, $width, $height);
		
		//simpan gambar thumbnail
		imagepng($thumbImage,$direktoriThumb.$uploadName);
	 
    } 
   
    //hapus objek gambar dalam memori
    imagedestroy($realImages);
    imagedestroy($thumbImage);
}

function uploadslider($uploadName){
    $direktori          = "../../../assets/img/";
    $direktoriThumb     = "../../../assets/img/";
    $file               = $direktori.$uploadName;
   
    //simpan gambar ukuran sebenernya
    $realImagesName     = $_FILES['fupload']['tmp_name'];
    $realImagesType     = $_FILES['fupload']['type'];
    $acak           	= rand(0000,9999);
    move_uploaded_file($realImagesName, $file);
   
    //simpan ukuran thumbs
    $thumbWidth     = 2048;
    $thumbHeight    = 834;
	
    //identitas file gambar

    if ($realImagesType=='image/jpeg') {
        $realImages             = imagecreatefromjpeg($file);
        $width                  = imageSX($realImages);
        $height                 = imageSY($realImages);
		
		//mengubah ukuran gambar
		$thumbImage = imagecreatetruecolor($thumbWidth, $thumbHeight);
		imagecopyresampled($thumbImage, $realImages, 0,0,0,0, $thumbWidth, $thumbHeight, $width, $height);
		
		//simpan gambar thumbnail
		imagejpeg($thumbImage,$direktoriThumb.$uploadName);
		
    } else {
        $realImages             = imagecreatefrompng($file);
        $width                  = imageSX($realImages);
        $height                 = imageSY($realImages);
		
		//mengubah ukuran gambar
		$thumbImage = imagecreatetruecolor($thumbWidth, $thumbHeight);
		imagealphablending($thumbImage, false);
		imagesavealpha($thumbImage,true);
		$transparent = imagecolorallocatealpha($thumbImage, 255, 255, 255, 127);
		imagefilledrectangle($thumbImage, 0, 0, $nWidth, $nHeight, $transparent);
	    imagecopyresampled($thumbImage, $realImages, 0,0,0,0, $thumbWidth, $thumbHeight, $width, $height);
		
		//simpan gambar thumbnail
		imagepng($thumbImage,$direktoriThumb.$uploadName);
	 
    } 
   
    //hapus objek gambar dalam memori
    imagedestroy($realImages);
    imagedestroy($thumbImage);
}

function uploadkategori($uploadName){
    $direktori          = "../../../assets/img/";
    $direktoriThumb     = "../../../assets/img/";
    $file               = $direktori.$uploadName;
   
    //simpan gambar ukuran sebenernya
    $realImagesName     = $_FILES['fupload']['tmp_name'];
    $realImagesType     = $_FILES['fupload']['type'];
    $acak           	= rand(0000,9999);
    move_uploaded_file($realImagesName, $file);
   
    //simpan ukuran thumbs
    $thumbWidth     = 450;
    $thumbHeight    = 390;
	
    //identitas file gambar

    if ($realImagesType=='image/jpeg') {
        $realImages             = imagecreatefromjpeg($file);
        $width                  = imageSX($realImages);
        $height                 = imageSY($realImages);
		
		//mengubah ukuran gambar
		$thumbImage = imagecreatetruecolor($thumbWidth, $thumbHeight);
		imagecopyresampled($thumbImage, $realImages, 0,0,0,0, $thumbWidth, $thumbHeight, $width, $height);
		
		//simpan gambar thumbnail
		imagejpeg($thumbImage,$direktoriThumb.$uploadName);
		
    } else {
        $realImages             = imagecreatefrompng($file);
        $width                  = imageSX($realImages);
        $height                 = imageSY($realImages);
		
		//mengubah ukuran gambar
		$thumbImage = imagecreatetruecolor($thumbWidth, $thumbHeight);
		imagealphablending($thumbImage, false);
		imagesavealpha($thumbImage,true);
		$transparent = imagecolorallocatealpha($thumbImage, 255, 255, 255, 127);
		imagefilledrectangle($thumbImage, 0, 0, $nWidth, $nHeight, $transparent);
	    imagecopyresampled($thumbImage, $realImages, 0,0,0,0, $thumbWidth, $thumbHeight, $width, $height);
		
		//simpan gambar thumbnail
		imagepng($thumbImage,$direktoriThumb.$uploadName);
	 
    } 
   
    //hapus objek gambar dalam memori
    imagedestroy($realImages);
    imagedestroy($thumbImage);
}

function upload1($uploadName){
    $direktori          = "../../../produk/";
    $direktoriThumb     = "../../../produk/thumb/";
    $file               = $direktori.$uploadName;
   
    //simpan gambar ukuran sebenernya
    $realImagesName     = $_FILES['fupload']['tmp_name'];
    $realImagesType     = $_FILES['fupload']['type'];
    $acak           	= rand(0000,9999);
    move_uploaded_file($realImagesName, $file);
   
   	//simpan ukuran thumbs
    $thumbWidth     = 240;
    $thumbHeight    = 240;
	
    //identitas file gambar

    if ($realImagesType=='image/jpeg') {
        $realImages             = imagecreatefromjpeg($file);
        $width                  = imageSX($realImages);
        $height                 = imageSY($realImages);
		
		//mengubah ukuran gambar
		$thumbImage = imagecreatetruecolor($thumbWidth, $thumbHeight);
		imagecopyresampled($thumbImage, $realImages, 0,0,0,0, $thumbWidth, $thumbHeight, $width, $height);
		
		//simpan gambar thumbnail
		imagejpeg($thumbImage,$direktoriThumb.$uploadName);
		
    } else {
        $realImages             = imagecreatefrompng($file);
        $width                  = imageSX($realImages);
        $height                 = imageSY($realImages);
		
		//mengubah ukuran gambar
		$thumbImage = imagecreatetruecolor($thumbWidth, $thumbHeight);
		imagealphablending($thumbImage, false);
		imagesavealpha($thumbImage,true);
		$transparent = imagecolorallocatealpha($thumbImage, 255, 255, 255, 127);
		imagefilledrectangle($thumbImage, 0, 0, $nWidth, $nHeight, $transparent);
	    imagecopyresampled($thumbImage, $realImages, 0,0,0,0, $thumbWidth, $thumbHeight, $width, $height);
		
		//simpan gambar thumbnail
		imagepng($thumbImage,$direktoriThumb.$uploadName);
	 
    } 
   
    //hapus objek gambar dalam memori
    imagedestroy($realImages);
    imagedestroy($thumbImage);
	
					  
}

function upload2($uploadName){
    $direktori          = "../../../produk/";
    $direktoriThumb     = "../../../produk/thumb/";
    $file               = $direktori.$uploadName;
   
    //simpan gambar ukuran sebenernya
    $realImagesName     = $_FILES['fupload2']['tmp_name'];
    $realImagesType     = $_FILES['fupload2']['type'];
    $acak           	= rand(0000,9999);
    move_uploaded_file($realImagesName, $file);
   
    //simpan ukuran thumbs
    $thumbWidth     = 240;
    $thumbHeight    = 240;
	
    //identitas file gambar

    if ($realImagesType=='image/jpeg') {
        $realImages             = imagecreatefromjpeg($file);
        $width                  = imageSX($realImages);
        $height                 = imageSY($realImages);
		
		//mengubah ukuran gambar
		$thumbImage = imagecreatetruecolor($thumbWidth, $thumbHeight);
		imagecopyresampled($thumbImage, $realImages, 0,0,0,0, $thumbWidth, $thumbHeight, $width, $height);
		
		//simpan gambar thumbnail
		imagejpeg($thumbImage,$direktoriThumb.$uploadName);
		
    } else {
        $realImages             = imagecreatefrompng($file);
        $width                  = imageSX($realImages);
        $height                 = imageSY($realImages);
		
		//mengubah ukuran gambar
		$thumbImage = imagecreatetruecolor($thumbWidth, $thumbHeight);
		imagealphablending($thumbImage, false);
		imagesavealpha($thumbImage,true);
		$transparent = imagecolorallocatealpha($thumbImage, 255, 255, 255, 127);
		imagefilledrectangle($thumbImage, 0, 0, $nWidth, $nHeight, $transparent);
	    imagecopyresampled($thumbImage, $realImages, 0,0,0,0, $thumbWidth, $thumbHeight, $width, $height);
		
		//simpan gambar thumbnail
		imagepng($thumbImage,$direktoriThumb.$uploadName);
	 
    } 
   
    //hapus objek gambar dalam memori
    imagedestroy($realImages);
    imagedestroy($thumbImage);
}

function upload3($uploadName){
    $direktori          = "../../../produk/";
    $direktoriThumb     = "../../../produk/thumb/";
    $file               = $direktori.$uploadName;
   
    //simpan gambar ukuran sebenernya
    $realImagesName     = $_FILES['fupload3']['tmp_name'];
    $realImagesType     = $_FILES['fupload3']['type'];
    $acak           	= rand(0000,9999);
    move_uploaded_file($realImagesName, $file);
   
    //simpan ukuran thumbs
    $thumbWidth     = 240;
    $thumbHeight    = 240;
	
    //identitas file gambar

    if ($realImagesType=='image/jpeg') {
        $realImages             = imagecreatefromjpeg($file);
        $width                  = imageSX($realImages);
        $height                 = imageSY($realImages);
		
		//mengubah ukuran gambar
		$thumbImage = imagecreatetruecolor($thumbWidth, $thumbHeight);
		imagecopyresampled($thumbImage, $realImages, 0,0,0,0, $thumbWidth, $thumbHeight, $width, $height);
		
		//simpan gambar thumbnail
		imagejpeg($thumbImage,$direktoriThumb.$uploadName);
		
    } else {
        $realImages             = imagecreatefrompng($file);
        $width                  = imageSX($realImages);
        $height                 = imageSY($realImages);
		
		//mengubah ukuran gambar
		$thumbImage = imagecreatetruecolor($thumbWidth, $thumbHeight);
		imagealphablending($thumbImage, false);
		imagesavealpha($thumbImage,true);
		$transparent = imagecolorallocatealpha($thumbImage, 255, 255, 255, 127);
		imagefilledrectangle($thumbImage, 0, 0, $nWidth, $nHeight, $transparent);
	    imagecopyresampled($thumbImage, $realImages, 0,0,0,0, $thumbWidth, $thumbHeight, $width, $height);
		
		//simpan gambar thumbnail
		imagepng($thumbImage,$direktoriThumb.$uploadName);
	 
    } 
   
    //hapus objek gambar dalam memori
    imagedestroy($realImages);
    imagedestroy($thumbImage);
}

function upload4($uploadName){
    $direktori          = "../../../produk/";
    $direktoriThumb     = "../../../produk/thumb/";
    $file               = $direktori.$uploadName;
   
    //simpan gambar ukuran sebenernya
    $realImagesName     = $_FILES['fupload4']['tmp_name'];
    $realImagesType     = $_FILES['fupload4']['type'];
    $acak           	= rand(0000,9999);
    move_uploaded_file($realImagesName, $file);
   
    //simpan ukuran thumbs
    $thumbWidth     = 240;
    $thumbHeight    = 240;
	
    //identitas file gambar

    if ($realImagesType=='image/jpeg') {
        $realImages             = imagecreatefromjpeg($file);
        $width                  = imageSX($realImages);
        $height                 = imageSY($realImages);
		
		//mengubah ukuran gambar
		$thumbImage = imagecreatetruecolor($thumbWidth, $thumbHeight);
		imagecopyresampled($thumbImage, $realImages, 0,0,0,0, $thumbWidth, $thumbHeight, $width, $height);
		
		//simpan gambar thumbnail
		imagejpeg($thumbImage,$direktoriThumb.$uploadName);
		
    } else {
        $realImages             = imagecreatefrompng($file);
        $width                  = imageSX($realImages);
        $height                 = imageSY($realImages);
		
		//mengubah ukuran gambar
		$thumbImage = imagecreatetruecolor($thumbWidth, $thumbHeight);
		imagealphablending($thumbImage, false);
		imagesavealpha($thumbImage,true);
		$transparent = imagecolorallocatealpha($thumbImage, 255, 255, 255, 127);
		imagefilledrectangle($thumbImage, 0, 0, $nWidth, $nHeight, $transparent);
	    imagecopyresampled($thumbImage, $realImages, 0,0,0,0, $thumbWidth, $thumbHeight, $width, $height);
		
		//simpan gambar thumbnail
		imagepng($thumbImage,$direktoriThumb.$uploadName);
	 
    } 
   
    //hapus objek gambar dalam memori
    imagedestroy($realImages);
    imagedestroy($thumbImage);
}

function upload5($uploadName){
    $direktori          = "../../../produk/";
    $direktoriThumb     = "../../../produk/thumb/";
    $file               = $direktori.$uploadName;
   
    //simpan gambar ukuran sebenernya
    $realImagesName     = $_FILES['fupload5']['tmp_name'];
    $realImagesType     = $_FILES['fupload5']['type'];
    $acak           	= rand(0000,9999);
    move_uploaded_file($realImagesName, $file);
   
    //simpan ukuran thumbs
    $thumbWidth     = 240;
    $thumbHeight    = 240;
	
    //identitas file gambar

    if ($realImagesType=='image/jpeg') {
        $realImages             = imagecreatefromjpeg($file);
        $width                  = imageSX($realImages);
        $height                 = imageSY($realImages);
		
		//mengubah ukuran gambar
		$thumbImage = imagecreatetruecolor($thumbWidth, $thumbHeight);
		imagecopyresampled($thumbImage, $realImages, 0,0,0,0, $thumbWidth, $thumbHeight, $width, $height);
		
		//simpan gambar thumbnail
		imagejpeg($thumbImage,$direktoriThumb.$uploadName);
		
    } else {
        $realImages             = imagecreatefrompng($file);
        $width                  = imageSX($realImages);
        $height                 = imageSY($realImages);
		
		//mengubah ukuran gambar
		$thumbImage = imagecreatetruecolor($thumbWidth, $thumbHeight);
		imagealphablending($thumbImage, false);
		imagesavealpha($thumbImage,true);
		$transparent = imagecolorallocatealpha($thumbImage, 255, 255, 255, 127);
		imagefilledrectangle($thumbImage, 0, 0, $nWidth, $nHeight, $transparent);
	    imagecopyresampled($thumbImage, $realImages, 0,0,0,0, $thumbWidth, $thumbHeight, $width, $height);
		
		//simpan gambar thumbnail
		imagepng($thumbImage,$direktoriThumb.$uploadName);
	 
    } 
   
    //hapus objek gambar dalam memori
    imagedestroy($realImages);
    imagedestroy($thumbImage);
}

function upload6($uploadName){
    $direktori          = "../../../produk/";
    $direktoriThumb     = "../../../produk/thumb/";
    $file               = $direktori.$uploadName;
   
    //simpan gambar ukuran sebenernya
    $realImagesName     = $_FILES['fupload6']['tmp_name'];
    $realImagesType     = $_FILES['fupload6']['type'];
    $acak           	= rand(0000,9999);
    move_uploaded_file($realImagesName, $file);
   
    //simpan ukuran thumbs
    $thumbWidth     = 240;
    $thumbHeight    = 240;
	
    //identitas file gambar

    if ($realImagesType=='image/jpeg') {
        $realImages             = imagecreatefromjpeg($file);
        $width                  = imageSX($realImages);
        $height                 = imageSY($realImages);
		
		//mengubah ukuran gambar
		$thumbImage = imagecreatetruecolor($thumbWidth, $thumbHeight);
		imagecopyresampled($thumbImage, $realImages, 0,0,0,0, $thumbWidth, $thumbHeight, $width, $height);
		
		//simpan gambar thumbnail
		imagejpeg($thumbImage,$direktoriThumb.$uploadName);
		
    } else {
        $realImages             = imagecreatefrompng($file);
        $width                  = imageSX($realImages);
        $height                 = imageSY($realImages);
		
		//mengubah ukuran gambar
		$thumbImage = imagecreatetruecolor($thumbWidth, $thumbHeight);
		imagealphablending($thumbImage, false);
		imagesavealpha($thumbImage,true);
		$transparent = imagecolorallocatealpha($thumbImage, 255, 255, 255, 127);
		imagefilledrectangle($thumbImage, 0, 0, $nWidth, $nHeight, $transparent);
	    imagecopyresampled($thumbImage, $realImages, 0,0,0,0, $thumbWidth, $thumbHeight, $width, $height);
		
		//simpan gambar thumbnail
		imagepng($thumbImage,$direktoriThumb.$uploadName);
	 
    } 
   
    //hapus objek gambar dalam memori
    imagedestroy($realImages);
    imagedestroy($thumbImage);
}

function upload7($uploadName){
    $direktori          = "../../../produk/";
    $direktoriThumb     = "../../../produk/thumb/";
    $file               = $direktori.$uploadName;
   
    //simpan gambar ukuran sebenernya
    $realImagesName     = $_FILES['fupload7']['tmp_name'];
    $realImagesType     = $_FILES['fupload7']['type'];
    $acak           	= rand(0000,9999);
    move_uploaded_file($realImagesName, $file);
   
   //simpan ukuran thumbs
    $thumbWidth     = 240;
    $thumbHeight    = 240;
	
    //identitas file gambar

    if ($realImagesType=='image/jpeg') {
        $realImages             = imagecreatefromjpeg($file);
        $width                  = imageSX($realImages);
        $height                 = imageSY($realImages);
		
		//mengubah ukuran gambar
		$thumbImage = imagecreatetruecolor($thumbWidth, $thumbHeight);
		imagecopyresampled($thumbImage, $realImages, 0,0,0,0, $thumbWidth, $thumbHeight, $width, $height);
		
		//simpan gambar thumbnail
		imagejpeg($thumbImage,$direktoriThumb.$uploadName);
		
    } else {
        $realImages             = imagecreatefrompng($file);
        $width                  = imageSX($realImages);
        $height                 = imageSY($realImages);
		
		//mengubah ukuran gambar
		$thumbImage = imagecreatetruecolor($thumbWidth, $thumbHeight);
		imagealphablending($thumbImage, false);
		imagesavealpha($thumbImage,true);
		$transparent = imagecolorallocatealpha($thumbImage, 255, 255, 255, 127);
		imagefilledrectangle($thumbImage, 0, 0, $nWidth, $nHeight, $transparent);
	    imagecopyresampled($thumbImage, $realImages, 0,0,0,0, $thumbWidth, $thumbHeight, $width, $height);
		
		//simpan gambar thumbnail
		imagepng($thumbImage,$direktoriThumb.$uploadName);
	 
    } 
   
    //hapus objek gambar dalam memori
    imagedestroy($realImages);
    imagedestroy($thumbImage);
}

?>
