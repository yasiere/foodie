<?php return array (
  'codeToName' => 
  array (
    32 => 'space',
    33 => 'exclam',
    34 => 'quotedbl',
    35 => 'numbersign',
    36 => 'dollar',
    37 => 'percent',
    38 => 'ampersand',
    39 => 'quotesingle',
    40 => 'parenleft',
    41 => 'parenright',
    42 => 'asterisk',
    43 => 'plus',
    44 => 'comma',
    45 => 'hyphen',
    46 => 'period',
    47 => 'slash',
    48 => 'zero',
    49 => 'one',
    50 => 'two',
    51 => 'three',
    52 => 'four',
    53 => 'five',
    54 => 'six',
    55 => 'seven',
    56 => 'eight',
    57 => 'nine',
    58 => 'colon',
    59 => 'semicolon',
    60 => 'less',
    61 => 'equal',
    62 => 'greater',
    63 => 'question',
    64 => 'at',
    65 => 'A',
    66 => 'B',
    67 => 'C',
    68 => 'D',
    69 => 'E',
    70 => 'F',
    71 => 'G',
    72 => 'H',
    73 => 'I',
    74 => 'J',
    75 => 'K',
    76 => 'L',
    77 => 'M',
    78 => 'N',
    79 => 'O',
    80 => 'P',
    81 => 'Q',
    82 => 'R',
    83 => 'S',
    84 => 'T',
    85 => 'U',
    86 => 'V',
    87 => 'W',
    88 => 'X',
    89 => 'Y',
    90 => 'Z',
    91 => 'bracketleft',
    92 => 'backslash',
    93 => 'bracketright',
    94 => 'asciicircum',
    95 => 'underscore',
    96 => 'grave',
    97 => 'a',
    98 => 'b',
    99 => 'c',
    100 => 'd',
    101 => 'e',
    102 => 'f',
    103 => 'g',
    104 => 'h',
    105 => 'i',
    106 => 'j',
    107 => 'k',
    108 => 'l',
    109 => 'm',
    110 => 'n',
    111 => 'o',
    112 => 'p',
    113 => 'q',
    114 => 'r',
    115 => 's',
    116 => 't',
    117 => 'u',
    118 => 'v',
    119 => 'w',
    120 => 'x',
    121 => 'y',
    122 => 'z',
    123 => 'braceleft',
    124 => 'bar',
    125 => 'braceright',
    126 => 'asciitilde',
    161 => 'exclamdown',
    162 => 'cent',
    163 => 'sterling',
    165 => 'yen',
    166 => 'brokenbar',
    167 => 'section',
    168 => 'dieresis',
    169 => 'copyright',
    170 => 'ordfeminine',
    171 => 'guillemotleft',
    174 => 'registered',
    175 => 'macron',
    176 => 'degree',
    180 => 'acute',
    182 => 'paragraph',
    183 => 'periodcentered',
    184 => 'cedilla',
    186 => 'ordmasculine',
    187 => 'guillemotright',
    188 => 'onequarter',
    189 => 'onehalf',
    190 => 'threequarters',
    191 => 'questiondown',
    192 => 'Agrave',
    193 => 'Aacute',
    194 => 'Acircumflex',
    195 => 'Atilde',
    196 => 'Adieresis',
    197 => 'Aring',
    198 => 'AE',
    199 => 'Ccedilla',
    200 => 'Egrave',
    201 => 'Eacute',
    202 => 'Ecircumflex',
    203 => 'Edieresis',
    204 => 'Igrave',
    205 => 'Iacute',
    206 => 'Icircumflex',
    207 => 'Idieresis',
    208 => 'Eth',
    209 => 'Ntilde',
    210 => 'Ograve',
    211 => 'Oacute',
    212 => 'Ocircumflex',
    213 => 'Otilde',
    214 => 'Odieresis',
    215 => 'multiply',
    216 => 'Oslash',
    217 => 'Ugrave',
    218 => 'Uacute',
    219 => 'Ucircumflex',
    220 => 'Udieresis',
    221 => 'Yacute',
    222 => 'Thorn',
    223 => 'germandbls',
    224 => 'agrave',
    225 => 'aacute',
    226 => 'acircumflex',
    227 => 'atilde',
    228 => 'adieresis',
    229 => 'aring',
    230 => 'ae',
    231 => 'ccedilla',
    232 => 'egrave',
    233 => 'eacute',
    234 => 'ecircumflex',
    235 => 'edieresis',
    236 => 'igrave',
    237 => 'iacute',
    238 => 'icircumflex',
    239 => 'idieresis',
    240 => 'eth',
    241 => 'ntilde',
    242 => 'ograve',
    243 => 'oacute',
    244 => 'ocircumflex',
    245 => 'otilde',
    246 => 'odieresis',
    247 => 'divide',
    248 => 'oslash',
    249 => 'ugrave',
    250 => 'uacute',
    251 => 'ucircumflex',
    252 => 'udieresis',
    253 => 'yacute',
    254 => 'thorn',
    255 => 'ydieresis',
    256 => 'Amacron',
    257 => 'amacron',
    258 => 'Abreve',
    259 => 'abreve',
    260 => 'Aogonek',
    261 => 'aogonek',
    262 => 'Cacute',
    263 => 'cacute',
    264 => 'Ccircumflex',
    265 => 'ccircumflex',
    266 => 'Cdotaccent',
    267 => 'cdotaccent',
    268 => 'Ccaron',
    269 => 'ccaron',
    270 => 'Dcaron',
    271 => 'dcaron',
    272 => 'Dcroat',
    273 => 'dmacron',
    274 => 'Emacron',
    275 => 'emacron',
    276 => 'Ebreve',
    277 => 'ebreve',
    278 => 'Edotaccent',
    279 => 'edotaccent',
    280 => 'Eogonek',
    281 => 'eogonek',
    282 => 'Ecaron',
    283 => 'ecaron',
    284 => 'Gcircumflex',
    285 => 'gcircumflex',
    286 => 'Gbreve',
    287 => 'gbreve',
    288 => 'Gdotaccent',
    289 => 'gdotaccent',
    290 => 'Gcommaaccent',
    291 => 'gcommaaccent',
    292 => 'Hcircumflex',
    293 => 'hcircumflex',
    294 => 'Hbar',
    295 => 'hbar',
    296 => 'Itilde',
    297 => 'itilde',
    298 => 'Imacron',
    299 => 'imacron',
    300 => 'Ibreve',
    301 => 'ibreve',
    302 => 'Iogonek',
    303 => 'iogonek',
    304 => 'Idot',
    305 => 'dotlessi',
    306 => 'IJ',
    307 => 'ij',
    308 => 'Jcircumflex',
    309 => 'jcircumflex',
    310 => 'Kcommaaccent',
    311 => 'kcommaaccent',
    313 => 'Lacute',
    314 => 'lacute',
    315 => 'Lcommaaccent',
    316 => 'lcommaaccent',
    317 => 'Lcaron',
    318 => 'lcaron',
    319 => 'Ldot',
    320 => 'ldot',
    321 => 'Lslash',
    322 => 'lslash',
    323 => 'Nacute',
    324 => 'nacute',
    325 => 'Ncommaaccent',
    326 => 'ncommaaccent',
    327 => 'Ncaron',
    328 => 'ncaron',
    329 => 'napostrophe',
    330 => 'Eng',
    331 => 'eng',
    332 => 'Omacron',
    333 => 'omacron',
    334 => 'Obreve',
    335 => 'obreve',
    336 => 'Ohungarumlaut',
    337 => 'ohungarumlaut',
    338 => 'OE',
    339 => 'oe',
    340 => 'Racute',
    341 => 'racute',
    342 => 'Rcommaaccent',
    343 => 'rcommaaccent',
    344 => 'Rcaron',
    345 => 'rcaron',
    346 => 'Sacute',
    347 => 'sacute',
    348 => 'Scircumflex',
    349 => 'scircumflex',
    350 => 'Scedilla',
    351 => 'scedilla',
    352 => 'Scaron',
    353 => 'scaron',
    354 => 'Tcommaaccent',
    355 => 'tcommaaccent',
    356 => 'Tcaron',
    357 => 'tcaron',
    358 => 'Tbar',
    359 => 'tbar',
    360 => 'Utilde',
    361 => 'utilde',
    362 => 'Umacron',
    363 => 'umacron',
    364 => 'Ubreve',
    365 => 'ubreve',
    366 => 'Uring',
    367 => 'uring',
    368 => 'Uhungarumlaut',
    369 => 'uhungarumlaut',
    370 => 'Uogonek',
    371 => 'uogonek',
    372 => 'Wcircumflex',
    373 => 'wcircumflex',
    374 => 'Ycircumflex',
    375 => 'ycircumflex',
    376 => 'Ydieresis',
    377 => 'Zacute',
    378 => 'zacute',
    379 => 'Zdotaccent',
    380 => 'zdotaccent',
    381 => 'Zcaron',
    382 => 'zcaron',
    402 => 'florin',
    506 => 'Aringacute',
    507 => 'aringacute',
    508 => 'AEacute',
    509 => 'aeacute',
    510 => 'Oslashacute',
    511 => 'oslashacute',
    536 => 'Scommaaccent',
    537 => 'scommaaccent',
    710 => 'circumflex',
    711 => 'caron',
    728 => 'breve',
    729 => 'dotaccent',
    730 => 'ring',
    731 => 'ogonek',
    732 => 'tilde',
    733 => 'hungarumlaut',
    7808 => 'Wgrave',
    7809 => 'wgrave',
    7810 => 'Wacute',
    7811 => 'wacute',
    7812 => 'Wdieresis',
    7813 => 'wdieresis',
    7922 => 'Ygrave',
    7923 => 'ygrave',
    8211 => 'endash',
    8212 => 'emdash',
    8216 => 'quoteleft',
    8217 => 'quoteright',
    8218 => 'quotesinglbase',
    8220 => 'quotedblleft',
    8221 => 'quotedblright',
    8222 => 'quotedblbase',
    8224 => 'dagger',
    8225 => 'daggerdbl',
    8226 => 'bullet',
    8230 => 'ellipsis',
    8240 => 'perthousand',
    8249 => 'guilsinglleft',
    8250 => 'guilsinglright',
    8260 => 'fraction',
    8364 => 'Euro',
    8482 => 'trademark',
    8531 => 'onethird',
    8532 => 'twothirds',
    8539 => 'oneeighth',
    8540 => 'threeeighths',
    8541 => 'fiveeighths',
    8542 => 'seveneighths',
    8722 => 'minus',
    64256 => 'f_f',
    64257 => 'f_i',
    64258 => 'f_l',
    64259 => 'f_f_i',
    64260 => 'f_f_l',
  ),
  'isUnicode' => true,
  'EncodingScheme' => 'FontSpecific',
  'FontName' => 'Guardian Egyp Regular',
  'FullName' => 'GuardianEgyp-Regular',
  'Version' => 'Version 1.002;PS 001.002;hotconv 1.0.57;makeotf.lib2.0.21895',
  'PostScriptName' => 'GuardianEgyp-Regular',
  'Weight' => 'Medium',
  'ItalicAngle' => '0',
  'IsFixedPitch' => 'false',
  'UnderlineThickness' => '50',
  'UnderlinePosition' => '-125',
  'FontHeightOffset' => '200',
  'Ascender' => '790',
  'Descender' => '-210',
  'FontBBox' => 
  array (
    0 => '-170',
    1 => '-213',
    2 => '1185',
    3 => '1033',
  ),
  'StartCharMetrics' => '516',
  'C' => 
  array (
    32 => 190,
    33 => 249,
    34 => 373,
    35 => 504,
    36 => 503,
    37 => 826,
    38 => 678,
    39 => 214,
    40 => 311,
    41 => 311,
    42 => 364,
    43 => 526,
    44 => 221,
    45 => 302,
    46 => 221,
    47 => 334,
    48 => 626,
    49 => 378,
    50 => 514,
    51 => 493,
    52 => 570,
    53 => 504,
    54 => 564,
    55 => 488,
    56 => 565,
    57 => 564,
    58 => 228,
    59 => 228,
    60 => 506,
    61 => 516,
    62 => 506,
    63 => 408,
    64 => 886,
    65 => 651,
    66 => 605,
    67 => 603,
    68 => 671,
    69 => 585,
    70 => 580,
    71 => 659,
    72 => 724,
    73 => 315,
    74 => 403,
    75 => 651,
    76 => 554,
    77 => 864,
    78 => 707,
    79 => 682,
    80 => 553,
    81 => 682,
    82 => 636,
    83 => 519,
    84 => 593,
    85 => 675,
    86 => 642,
    87 => 936,
    88 => 671,
    89 => 608,
    90 => 530,
    91 => 314,
    92 => 334,
    93 => 314,
    94 => 394,
    95 => 310,
    96 => 488,
    97 => 473,
    98 => 541,
    99 => 452,
    100 => 566,
    101 => 493,
    102 => 311,
    103 => 500,
    104 => 580,
    105 => 269,
    106 => 261,
    107 => 529,
    108 => 269,
    109 => 878,
    110 => 582,
    111 => 541,
    112 => 565,
    113 => 552,
    114 => 350,
    115 => 418,
    116 => 342,
    117 => 582,
    118 => 529,
    119 => 773,
    120 => 529,
    121 => 526,
    122 => 472,
    123 => 368,
    124 => 214,
    125 => 368,
    126 => 400,
    160 => 190,
    161 => 249,
    162 => 452,
    163 => 534,
    165 => 580,
    166 => 214,
    167 => 493,
    168 => 488,
    169 => 776,
    170 => 455,
    171 => 412,
    174 => 433,
    175 => 488,
    176 => 336,
    180 => 488,
    182 => 567,
    183 => 218,
    184 => 488,
    186 => 498,
    187 => 412,
    188 => 806,
    189 => 826,
    190 => 828,
    191 => 408,
    192 => 651,
    193 => 651,
    194 => 651,
    195 => 651,
    196 => 651,
    197 => 651,
    198 => 916,
    199 => 603,
    200 => 585,
    201 => 585,
    202 => 585,
    203 => 585,
    204 => 315,
    205 => 315,
    206 => 315,
    207 => 315,
    208 => 671,
    209 => 707,
    210 => 682,
    211 => 682,
    212 => 682,
    213 => 682,
    214 => 682,
    215 => 526,
    216 => 682,
    217 => 675,
    218 => 675,
    219 => 675,
    220 => 675,
    221 => 608,
    222 => 569,
    223 => 597,
    224 => 473,
    225 => 473,
    226 => 473,
    227 => 473,
    228 => 473,
    229 => 473,
    230 => 760,
    231 => 452,
    232 => 493,
    233 => 493,
    234 => 493,
    235 => 493,
    236 => 269,
    237 => 269,
    238 => 269,
    239 => 269,
    240 => 539,
    241 => 582,
    242 => 541,
    243 => 541,
    244 => 541,
    245 => 541,
    246 => 541,
    247 => 526,
    248 => 541,
    249 => 582,
    250 => 582,
    251 => 582,
    252 => 582,
    253 => 526,
    254 => 550,
    255 => 526,
    256 => 651,
    257 => 473,
    258 => 651,
    259 => 473,
    260 => 651,
    261 => 473,
    262 => 603,
    263 => 452,
    264 => 603,
    265 => 452,
    266 => 603,
    267 => 452,
    268 => 603,
    269 => 452,
    270 => 671,
    271 => 566,
    272 => 671,
    273 => 566,
    274 => 585,
    275 => 493,
    276 => 585,
    277 => 493,
    278 => 585,
    279 => 493,
    280 => 585,
    281 => 493,
    282 => 585,
    283 => 493,
    284 => 659,
    285 => 500,
    286 => 659,
    287 => 500,
    288 => 659,
    289 => 500,
    290 => 659,
    291 => 500,
    292 => 724,
    293 => 580,
    294 => 724,
    295 => 580,
    296 => 315,
    297 => 269,
    298 => 315,
    299 => 269,
    300 => 315,
    301 => 269,
    302 => 315,
    303 => 269,
    304 => 315,
    305 => 269,
    306 => 708,
    307 => 530,
    308 => 403,
    309 => 261,
    310 => 651,
    311 => 529,
    313 => 554,
    314 => 269,
    315 => 554,
    316 => 269,
    317 => 554,
    318 => 269,
    319 => 554,
    320 => 294,
    321 => 554,
    322 => 269,
    323 => 707,
    324 => 582,
    325 => 707,
    326 => 582,
    327 => 707,
    328 => 582,
    329 => 612,
    330 => 707,
    331 => 565,
    332 => 682,
    333 => 541,
    334 => 682,
    335 => 541,
    336 => 682,
    337 => 541,
    338 => 910,
    339 => 888,
    340 => 636,
    341 => 350,
    342 => 636,
    343 => 350,
    344 => 636,
    345 => 350,
    346 => 519,
    347 => 418,
    348 => 519,
    349 => 418,
    350 => 519,
    351 => 418,
    352 => 519,
    353 => 418,
    354 => 593,
    355 => 342,
    356 => 593,
    357 => 342,
    358 => 593,
    359 => 342,
    360 => 675,
    361 => 582,
    362 => 675,
    363 => 582,
    364 => 675,
    365 => 582,
    366 => 675,
    367 => 582,
    368 => 675,
    369 => 582,
    370 => 675,
    371 => 582,
    372 => 936,
    373 => 773,
    374 => 608,
    375 => 526,
    376 => 608,
    377 => 530,
    378 => 472,
    379 => 530,
    380 => 472,
    381 => 530,
    382 => 472,
    402 => 391,
    506 => 651,
    507 => 473,
    508 => 916,
    509 => 760,
    510 => 682,
    511 => 541,
    536 => 519,
    537 => 418,
    567 => 261,
    710 => 488,
    711 => 488,
    728 => 488,
    729 => 488,
    730 => 488,
    731 => 488,
    732 => 488,
    733 => 488,
    7808 => 936,
    7809 => 773,
    7810 => 936,
    7811 => 773,
    7812 => 936,
    7813 => 773,
    7922 => 608,
    7923 => 526,
    8201 => 90,
    8211 => 392,
    8212 => 672,
    8216 => 214,
    8217 => 214,
    8218 => 214,
    8220 => 373,
    8221 => 373,
    8222 => 373,
    8224 => 404,
    8225 => 416,
    8226 => 353,
    8230 => 689,
    8240 => 1212,
    8242 => 214,
    8243 => 373,
    8249 => 250,
    8250 => 250,
    8260 => 217,
    8364 => 606,
    8482 => 713,
    8531 => 817,
    8532 => 873,
    8539 => 851,
    8540 => 872,
    8541 => 854,
    8542 => 823,
    8722 => 526,
    9413 => 788,
    64256 => 609,
    64257 => 572,
    64258 => 572,
    64259 => 870,
    64260 => 870,
  ),
  'CIDtoGID_Compressed' => true,
  'CIDtoGID' => 'eJzt22XQUEUUBuD3WKiI2CghYoBN2IgJtiJ2K1KiAgZ2d3d3d3d3d3d3d7d+OvzQGWYMhE+c55nZ2d27Z2ffnXv/3mQEjZmxMnbGSZOMm/EyfppmgjTLhGmeiTJxJsmkmSyTZ4q0yJSZKi3TKq3TJlOnbaZJu0yb6TJ9Zkj7dMiMmSkzZ5bMmtkyezqmUzqnS+bInJkrc2eezJv50jXzp1sWyIJZKAtnkSya7umRxbJ4lsiSWSpLZ5ksm+XSM8unV1bIilkpK2eVrJrVsnrWyJpZK2tnnayb9dI764/o5Rv0Sd/0S/+G0YBskIHZMBtl4wz6bW1whmSTYXWbNrTNsnmGNvRbZMtsla2zTbbNdtk+O2TH7JSds0t2zW7ZPXtkz+yVvbNP9s1+2T8H5MAclINzSA7NYTk8R+TIHJWjc0yOzXE5PifkxJyUk3NKTs1pOT1n5MyclbNzTs7NeTk/F+TCXJSLc0kuzWW5PFfkylyVq3NNrs11uT435MbclJtzS27Nbbk9d+TO3JW7c0/uzX25Pw/kwTyUh/NIHs1jeTxP5Mk8lafzTJ7Nc3k+L+TFvJSX80pezWt5PW/kzbyVt/NO3s17eT8f5MN8lI/zST5tuP1n+Txf5Mt8la/zTb7Nd/k+P+TH/JSfK1U1Ro1ZY9XYNU41qXFrvBq/mtYE1awmrOY1UU1ck9SkNVlNXlNUi5qypqqW1apaV5uautrWNNWupq3pavqaodpXh5qxZqqZa5aatWar2atjdarO1aXmqDlrrpq75vlrr7jm/Re+k/+Qmq+61vzVrRaoBf/23oVq4RE4eZF/vvf/rxat7n9a06MWq8VriVqylhoVmQAAAAAAAAAAAAAAAACgMdTStUwtW8tVz1q+sbOMLNWrVmjsDIwuasXhPFupVh42WqVWrdUa+tVrjVqzoV+r1q51hq2tO5yd6zW03vW7P+2rT/UdTl2/EU/+X1L9GzvBqFMDRvoJG9TAP8w3rI1q4xo0ss/lVzW4sRMA/J/VkMZOAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADA6qE1q09qsNq+hjZ0EAIBR4ReYAZoA',
  '_version_' => 6,
);