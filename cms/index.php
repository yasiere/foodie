<?php include "inc/db.php"; ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>CMS</title>
        <link href="css/style.default.css" rel="stylesheet">
        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo $base ?>/assets/img/favicon/apple-touch-icon-57x57.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $base ?>/assets/img/favicon/apple-touch-icon-114x114.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $base ?>/assets/img/favicon/apple-touch-icon-72x72.png" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $base ?>/assets/img/favicon/apple-touch-icon-144x144.png" />
        <link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo $base ?>/assets/img/favicon/apple-touch-icon-60x60.png" />
        <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo $base ?>/assets/img/favicon/apple-touch-icon-120x120.png" />
        <link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo $base ?>/assets/img/favicon/apple-touch-icon-76x76.png" />
        <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo $base ?>/assets/img/favicon/apple-touch-icon-152x152.png" />
        <link rel="icon" type="image/png" href="<?php echo $base ?>/assets/img/favicon/favicon-196x196.png" sizes="196x196" />
        <link rel="icon" type="image/png" href="<?php echo $base ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96" />
        <link rel="icon" type="image/png" href="<?php echo $base ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32" />
        <link rel="icon" type="image/png" href="<?php echo $base ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16" />
        <link rel="icon" type="image/png" href="<?php echo $base ?>/assets/img/favicon/favicon-128.png" sizes="128x128" />
        <link rel="shortcut icon" href="<?php echo $base ?>/assets/img/favicon/favicon.ico" type="image/x-icon" />
        <meta name="application-name" content="&nbsp;" />
        <meta name="msapplication-TileColor" content="#FFFFFF" />
        <meta name="msapplication-TileImage" content="<?php echo $base ?>/assets/img/favicon/mstile-144x144.png" />
        <meta name="msapplication-square70x70logo" content="<?php echo $base ?>/assets/img/favicon/mstile-70x70.png" />
        <meta name="msapplication-square150x150logo" content="<?php echo $base ?>/assets/img/favicon/mstile-150x150.png" />
        <meta name="msapplication-wide310x150logo" content="<?php echo $base ?>/assets/img/favicon/mstile-310x150.png" />
        <meta name="msapplication-square310x310logo" content="<?php echo $base ?>/assets/img/favicon/mstile-310x310.png" />
    </head>
    <body class="signin"style="background: #FF6E6E">
        <section>
            <div class="panel panel-signin">
                <div class="panel-body">
                    <div class="logo text-center" style="padding-bottom: 30px; padding-top: 30px;">
                        <img src="<?php echo $base ?>/assets/img/theme/logo.jpg" style="width: 50%;">
                    </div>
					<?php
					if(isset($_GET['info'])){
						if($_GET['info']==1){
							$teks="<strong>Gagal!</strong> email dan password tidak cocok dengan database kami.";
							echo"<div class='alert alert-danger'>
								<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
								$teks
							</div>";
						}
						else{
							$teks="<strong>Sukses!</strong> anda telah berhasil keluar dari sistem.";
							echo"<div class='alert alert-success'>
								<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
								$teks
							</div>";
						}
					}
					?>
                    <div class="mb30"></div>
                    <form action="proses-login.htm" method="post">
                        <div class="input-group mb15">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                            <input type="email" class="form-control" placeholder="Email" name="ak_email" required autocomplete="off" maxlength="50" pattern="^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$">
                        </div><!-- input-group -->
                        <div class="input-group mb15">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                            <input type="password" class="form-control" placeholder="Password" name="ak_pass" required autocomplete="off" pattern=".{6,}" maxlength="10">
                        </div><!-- input-group -->
                        <div class="clearfix">
                            <div class="pull-left">
                                <div class="ckbox ckbox-primary mt10">
                                    <input type="checkbox" id="rememberMe" value="1">
                                    <label for="rememberMe">Ingat saya</label>
                                </div>
                            </div>
                            <div class="pull-right">
                                <button type="submit" class="btn btn-success">Masuk <i class="fa fa-angle-right ml5"></i></button>
                            </div>
                        </div>
                    </form>
                </div><!-- panel-body -->
                <div class="panel-footer text-center">

                </div><!-- panel-footer -->
            </div><!-- panel -->
        </section>
        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/jquery-migrate-1.2.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/modernizr.min.js"></script>
        <script src="js/pace.min.js"></script>
        <script src="js/retina.min.js"></script>
        <script src="js/jquery.cookies.js"></script>
        <script src="js/custom.js"></script>
    </body>
</html>
