<?php
session_start();
include "config/func/base_url.php";
$active = "";
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
    <title>Notification - Foodie Guidances</title>
	<meta name="keywords" content="">
    <meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
		<link rel="stylesheet" href="<?php echo"$base_url"; ?>/assets/css/example.wink.css">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/superslides.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>
	<link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
 <body style="position: initial;" class="drawer drawer--right">
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>
    <div class="container hook">
		<div class="row">
			<div class="col-16">
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li class="active">Notification</li>
				</ol>
				<h3 class="f-merah mb10">Notification</h3>
				<p>
				<?php
				if($_GET['info']=="signup_berhasil"){
					echo"We have sent confirmation email to your email account, please check and click confirmation button to activation your Foodie Guidances account.";
				}
				elseif($_GET['info']=="aktifasi_error"){
					echo"Email is already registered";
				}
				elseif($_GET['info']=="aktifasi_berhasil"){
					echo"Your account has been successfully activated, you can login now.";
				}
				elseif($_GET['info']=="login_error"){
					echo"Your email and password not match, please make sure that account was activated before login.";
					?>
					<form class="border-form form-horizontal" method="post" action="<?php echo"$base_url"; ?>/config/func/process_signin.php" id="signup-daftar">
						<input type="hidden" name="page" value="<?php echo str_replace('_', '/', $_GET['page']); ?>">
						<div class="row mb20">
							<div class="col-4 col-4c"><label class="control-label">Email <span class="f-merah">*</span></label></div>
							<div class="col-6 col-4b">
								<input type="email" class="form-control" placeholder="Write Email" name="m_surat" value="<?php echo"$ts[email]" ?>" autocomplete="off" id="email">
							</div>
						</div>
						<div class="row mb20">
							<div class="col-4 col-4c"><label class="control-label">Password <span class="f-merah">*</span></label></div>
							<div class="col-6 col-4b">
								<div class="input-group" style="width: 100%;">
							      <input type="password" id="password-1" class="form-control" placeholder="Write Password" name="m_kode">

							   </div>
							</div>
						</div>
						<div class="row mb20">
							<div class="col-4 col-4c mene-atas"></div>
							<div class=" col-6 col-4b checkbox checkbox-inline checkbox-danger" style="margin-left: 10px;">
								<input name="remember" value="1" id="remember" type="checkbox">
								<label for="remember" style="margin-top: -3px; margin-right: 3px;"> Remember Me</label>
							</div>
						</div>
						<div class="row mb20">
							<div class="col-4 col-4c mene-atas"></div>
							<div class="col-6 col-4b f-12">
								<a href="<?php echo"$base_url/forgot-password"; ?>" class="f-merah">Forgot your password?</a>
							</div>
						</div>
						<button type="submit" class="btn btn-danger">Sign In</button> <button type="reset" class="btn btn-danger">Reset</button>
					</form>
					<?php
				}
				elseif($_GET['info']=="login_area"){
					echo "You have not login yet. To continue function, you need to login at bellow.";
					?>
					<form class="border-form form-horizontal" method="post" action="<?php echo"$base_url"; ?>/config/func/process_signin.php" id="signup-daftar">
						<input type="hidden" name="page" value="<?php echo str_replace('_', '/', $_GET['page']); ?>">
						<div class="row mb20">
							<div class="col-4 col-4c"><label class="control-label">Email <span class="f-merah">*</span></label></div>
							<div class="col-6 col-4b">
								<input type="email" class="form-control" placeholder="Write Email" name="m_surat" value="<?php echo"$ts[email]" ?>" autocomplete="off" id="email">
							</div>
						</div>
						<div class="row mb20">
							<div class="col-4 col-4c"><label class="control-label">Password <span class="f-merah">*</span></label></div>
							<div class="col-6 col-4b">
								<div class="input-group" style="width: 100%;">
							      <input type="password" id="password-1" class="form-control" placeholder="Write Password" name="m_kode">

							   </div>
							</div>
						</div>
						<div class="row mb20">
							<div class="col-4 col-4c mene-atas"></div>
							<div class=" col-6 col-4b checkbox checkbox-inline checkbox-danger" style="margin-left: 10px;">
								<input name="remember" value="1" id="remember" type="checkbox">
								<label for="remember" style="margin-top: -3px; margin-right: 3px;"> Remember Me</label>
							</div>
						</div>
						<div class="row mb20">
							<div class="col-4 col-4c mene-atas"></div>
							<div class="col-6 col-4b f-12">
								<a href="<?php echo"$base_url/forgot-password"; ?>" class="f-merah">Forgot your password?</a>
							</div>
						</div>
						<button type="submit" class="btn btn-danger">Sign In</button> <button type="reset" class="btn btn-danger">Reset</button>
					</form>
					<?php
				}
				elseif($_GET['info']=="login-first"){
					// echo"You need to signin in order to continue to submit page. Please signin in <a href='https://www.foodieguidances.com/sign-in' class='f-merah'>here</a>. ";
					echo "You have not login yet. To continue function, you need to login at bellow.";
					?>
					<form class="border-form form-horizontal" method="post" action="<?php echo"$base_url"; ?>/config/func/process_signin.php" id="signup-daftar">
						<input type="hidden" name="page" value="<?php echo str_replace('_', '/', $_GET['page']); ?>">
						<div class="row mb20">
							<div class="col-4 col-4c"><label class="control-label">Email <span class="f-merah">*</span></label></div>
							<div class="col-6 col-4b">
								<input type="email" class="form-control" placeholder="Write Email" name="m_surat" value="<?php echo"$ts[email]" ?>" autocomplete="off" id="email">
							</div>
						</div>
						<div class="row mb20">
							<div class="col-4 col-4c"><label class="control-label">Password <span class="f-merah">*</span></label></div>
							<div class="col-6 col-4b">
								<div class="input-group" style="width: 100%;">
							      <input type="password" id="password-1" class="form-control" placeholder="Write Password" name="m_kode">

							   </div>
							</div>
						</div>
						<div class="row mb20">
							<div class="col-4 col-4c mene-atas"></div>
							<div class=" col-6 col-4b checkbox checkbox-inline checkbox-danger" style="margin-left: 10px;">
								<input name="remember" value="1" id="remember" type="checkbox">
								<label for="remember" style="margin-top: -3px; margin-right: 3px;"> Remember Me</label>
							</div>
						</div>
						<div class="row mb20">
							<div class="col-4 col-4c mene-atas"></div>
							<div class="col-6 col-4b f-12">
								<a href="<?php echo"$base_url/forgot-password"; ?>" class="f-merah">Forgot your password?</a>
							</div>
						</div>
						<button type="submit" class="btn btn-danger">Sign In</button> <button type="reset" class="btn btn-danger">Reset</button>
					</form>
					<?php
				}
				elseif($_GET['info']=="delete_account"){
					echo"You are automated logout. By deleting your account it will delete all your personal info in our system. There will be 30 days transitional period, your account will be automatic restore if you login on transitional period.";
				}
				elseif($_GET['info']=="login_coupon"){
					echo"Oops! You need to <a href='$base_url/sign-in' class='f-merah'>Log in</a> or <a href='$base_url/sign-up'  class='f-merah'>Sign up</a> tog get to access the coupon details as it is exclusively for members only.";
				}
				elseif($_GET['info']=="forgot"){
					echo"We have sent you reset password link to your email, Please check your email.";
				}
				?>
				</p>
			</div>
		</div>
    </div>
    <?php include"config/inc/footer.php"; ?>
</body>
<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
<script src="<?php echo"$base_url"; ?>/assets/js/hideShowPassword.min.js"></script>
<script>
$('#password-1').hidePassword(true);
</script>
<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php echo"$base_url"; ?>/assets/js/jquery.validate.min.js"></script>
<script src="<?php echo"$base_url"; ?>/assets/js/theme.js"></script>
<script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
 <script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
 <script>
		$(document).ready(function() {
			$('.drawer').drawer();

		$('.tutup').click(function(){
			$('.iklanan').toggleClass('hilang', 500);
		});

		$('.cart').click(function(e){
			e.stopPropagation();
			$('.dis-cart').toggle();
			$('.dis-plus').hide();
			$('.dis-share').hide();
		});
		$('.plus').click(function(e){
			e.stopPropagation();
			$('.dis-plus').toggle();
			$('.dis-cart').hide();
			$('.dis-share').hide();
		});
		$('.share').click(function(e){
			e.stopPropagation();
			$('.dis-share').toggle();
			$('.dis-plus').hide();
			$('.dis-cart').hide();
		});
		$('.drawer-toggle').click(function(){
			$('.dis-cart').hide();
			$('.dis-plus').hide();
			$('.dis-share').hide();
		});
		$(document).click(function () {
			 var $el = $(".dis-share");
			 if ($el.is(":visible")) {
					$el.fadeOut(200);
			 }
			 var $ela = $(".dis-plus");
			 if ($ela.is(":visible")) {
					$ela.fadeOut(200);
			 }
			 var $elu = $(".dis-cart");
			 if ($elu.is(":visible")) {
					$elu.fadeOut(200);
			 }
		 });

		});
 </script>
<script type="text/javascript">
$(document).ready(function () {
	$(".alert").fadeTo(5000, 500).fadeOut(500, function(){
		$(".alert").alert('close');
	});

	$('#signup-daftar').validate({
});
});
</script>
</html>
