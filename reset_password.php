<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
$active = "signup";
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
    <title>Reset Password - Foodie Guidances</title>
	<meta name="keywords" content="">
    <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>
  <link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body style="position: initial;" class="drawer drawer--right">
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>
    <div class="container hook">
		<div class="row">
			<div class="col-16">
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li class="active">Reset Password</li>
				</ol>
				<h3 class="f-merah mb10">Reset Password</h3>
				<p>*required field</p>
				<form class="border-form form-horizontal" method="post" action="<?php echo"$base_url"; ?>/config/func/process_password.php" id="signup-daftar">
					<input type="hidden" name="id" value="<?php echo"$_GET[id]"; ?>">
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Password <span class="f-merah">*</span></label></div>
						<div class="col-6 col-4b">
							<input type="password" id="password" class="form-control" placeholder="Write Password" name="m_kode">
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Confirm Password <span class="f-merah">*</span></label></div>
						<div class="col-6 col-4b">
							<input type="password" class="form-control" placeholder="Re-Write Password" name="m_kode2">
						</div>
					</div>
					<button type="submit" class="btn btn-danger">Save</button>
				</form>
			</div>
		</div>
    </div>
    <?php include"config/inc/footer.php"; ?>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.validate.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/theme.js"></script>
	<script type="text/javascript">
	$(document).ready(function () {
		$.validator.addMethod("usertheme",function(value,element){
            return this.optional(element) || /^[a-z][a-z0-9_-]{3,12}$/i.test(value);
        });
		$.validator.addMethod("passkuat",function(value,element){
            return this.optional(element) || /^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9]).{7,20}$/i.test(value);
        });
		$.validator.addMethod("accept_char",function(value,element){
            return this.optional(element) || /^([a-zA-Z0-9,\./<>\?;':""[\]\\{}\|`~!@#\$%\^&\*()-_=\+]*)$/i.test(value);
        });

		$('#signup-daftar').validate({
			rules: {
				username: {
					required: true,
					minlength: 4,
					maxlength: 20,
					usertheme:true,
					remote: {
						url: "config/func/check-username.php",
						type: "post",
						data: {
						  username: function() {
							return $( "#username" ).val();
						  }
						}
					}
				},
				m_surat: {
					required: true,
					email: true,
					remote: {
						url: "config/func/check-email.php",
						type: "post",
						data: {
						  m_surat: function() {
							return $( "#email" ).val();
						  }
						}
					}
				},
				m_kode: {
					required: true,
					minlength: 8,
					maxlength: 20,
					passkuat:true,
					accept_char:true
				},
				m_kode2: {
					equalTo : "#password"
				},
				negara: {
					required: true
				}
			},
			messages: {
				username: {
					required: "Username cant be blank.",
					minlength: "Username must be between 4 - 20 characters",
					maxlength: "Username must be between 4 - 20 characters",
					usertheme: "Username first character must be letter (a-z) follow by letter, number, underscore or a hyphen",
					remote: "username is already in use"
				},
				m_surat: {
					required: "Email cant be blank",
					email: "Email not valid",
					remote: "Email is already registered"
				},
				m_kode: {
					required: "Password cant be blank.",
					minlength: "Password must be between 8 - 20 characters",
					maxlength: "Password must be between 8 - 20 characters",
					passkuat: "Your Password is weak! at least one uppercase letter, one number and one special character",
					accept_char: "Only keyboard character allowed (!,@,#,$,%,^,&,*,~,?)"
				},
				m_kode2: {
					equalTo : "Your passwords do not match. Please try again"
				},
				negara: {
					required: "Country cant be blank."
				}
			}
		});
	});
	</script>
  <script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
	<script>
		 $(document).ready(function() {
			 $('.drawer').drawer();

		 $('.tutup').click(function(){
			 $('.iklanan').toggleClass('hilang', 500);
		 });

		 $('.cart').click(function(e){
			 e.stopPropagation();
			 $('.dis-cart').toggle();
			 $('.dis-plus').hide();
			 $('.dis-share').hide();
		 });
		 $('.plus').click(function(e){
			 e.stopPropagation();
			 $('.dis-plus').toggle();
			 $('.dis-cart').hide();
			 $('.dis-share').hide();
		 });
		 $('.share').click(function(e){
			 e.stopPropagation();
			 $('.dis-share').toggle();
			 $('.dis-plus').hide();
			 $('.dis-cart').hide();
		 });
		 $('.drawer-toggle').click(function(){
			 $('.dis-cart').hide();
			 $('.dis-plus').hide();
			 $('.dis-share').hide();
		 });
		 $('.btn-caris').click(function(){
			 $('.search_kiri').toggleClass('search_kiri_tam', 500);
		 });
		 $(document).click(function () {
				var $el = $(".dis-share");
				if ($el.is(":visible")) {
					 $el.fadeOut(200);
				}
				var $ela = $(".dis-plus");
				if ($ela.is(":visible")) {
					 $ela.fadeOut(200);
				}
				var $elu = $(".dis-cart");
				if ($elu.is(":visible")) {
					 $elu.fadeOut(200);
				}
			});

		 });
	</script>
</body>
</html>
