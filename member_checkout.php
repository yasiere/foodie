<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";	
if(isset($_SESSION['food_member'])){
	$sql=mysqli_query($koneksi,"select * from pesan_sementara p, fgmart_item f, fgmart g where p.id_member='$_SESSION[food_member]' and p.id_fgmart_item=f.id_fgmart_item and f.id_fgmart=g.id_fgmart");
	$ada=mysqli_num_rows($sql);
	if($ada<>0){
		function isi_keranjang($koneksi,$id_member){
			$isikeranjang = array();
			$idm=$id_member;
			$sql = mysqli_query($koneksi,"SELECT * FROM pesan_sementara WHERE id_member='$idm'");
			while ($r=mysqli_fetch_array($sql)) {
				$isikeranjang[] = $r;
			}
			return $isikeranjang;
		}
		include "config/func/member_data.php";
		date_default_timezone_set('Asia/Jakarta');
		$tgl = date("Y-m-d");
		mysqli_query($koneksi,"insert into pesanan (id_member,status,tgl_pesanan) values('$_SESSION[food_member]','Pending','$tgl')");
		$id_pesanan=mysqli_insert_id($koneksi);
		
		$isikeranjang = isi_keranjang($koneksi,$_SESSION['food_member']);
		$jml          = count($isikeranjang);
		// simpan data detail pemesanan  
		for ($i = 0; $i < $jml; $i++){
			mysqli_query($koneksi,"INSERT INTO pesan(id_pesanan, id_fgmart_item) VALUES('$id_pesanan',{$isikeranjang[$i]['id_fgmart_item']})");
		}  
		// setelah data pemesanan tersimpan, hapus data pemesanan di tabel pemesanan sementara (pesan_sementara)
		for ($i = 0; $i < $jml; $i++) {
			mysqli_query($koneksi,"DELETE FROM pesan_sementara WHERE id_pesan_sementara = {$isikeranjang[$i]['id_pesan_sementara']}");
		}	
		
		echo"<form id='bayar' action='https://www.sandbox.paypal.com/cgi-bin/webscr' method='post'>
			<input type='hidden' name='cmd' value='_cart'>
			<input type='hidden' name='upload' value='1'>			
			<input type='hidden' name='no_note' value='0'>						
			<input type='hidden' name='bn' value='PP-BuyNowBF'>					
			<input type='hidden' name='tax' value='0'>			
			<input type='hidden' name='rm' value='2'>
			<input type='hidden' name='business' value='alejualpaypal@ale.com'>
			<input type='hidden' name='handling_cart' value='0'>
			<input type='hidden' name='currency_code' value='USD'>
			<input type='hidden' name='lc' value='MY' />
			<input type='hidden' name='return' value='$base_url/$u[username]/order-notification/$id_pesanan'>			
			<input type='hidden' name='cbt' value='Confirm Payment' />
			<input type='hidden' name='cancel_return' value='$base_url/$u[username]/order-cancel/$id_pesanan'>			
			<input type='hidden' name='custom' value='' />";
			$no=1;
			while($g=mysqli_fetch_array($sql)){
				echo"<input type='hidden' name='item_name_$no' value='$g[nama_fgmart] - $g[jenis]'>
				<input type='hidden' name='quantity_$no' value='1' />
				<input type='hidden' name='amount_$no' value='$g[harga]' />
				<input type='hidden' name='shipping_$no' value='0' />";
				$no++;
			}
		echo"</form>
		<script>
			document.getElementById('bayar').submit();
		</script>";
	}
	else{
		header("Location: ".$base_url."/".$u['username']."/stock-cart");
	}
}
else{
	header("Location: ".$base_url."/login-area");
}
?>