<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";
include "config/func/jumlah_data.php";
include "config/func/id_masking.php";

$auto_logout=1800000;
if(!empty($_SESSION['food_member'])){
	if (time()-$_SESSION['timestamp']>$auto_logout){
		session_destroy();
		session_unset();
		header("Location: ".$base_url."/auto-logout");
		exit();
	}else{
		$_SESSION['timestamp']=time();
	}
	include "config/func/member_data.php";
}
$id = id_masking($_GET['id']);
$active = "food";
$type = "food_detail";
// mysqli_query($koneksi,"update food set dilihat=dilihat + 1 where id_food='$id'");
$resto=mysqli_query($koneksi,"select *,r.id_negara as kode_negara,f.dilihat,f.id_price_index,(SELECT COUNT(h.id_member) FROM food_like h WHERE h.id_food=f.id_food) AS dilike,(SELECT COUNT(h.id_member) FROM food_rekomendasi h WHERE h.id_food=f.id_food) AS direkomendasi,(SELECT COUNT(r.id_member) FROM food_rating r WHERE r.id_food=f.id_food) AS jumlah_vote,(SELECT SUM(r.cleanlines)/ COUNT(r.id_member) FROM food_rating r WHERE r.id_food=f.id_food) AS t_cleanlines,(SELECT SUM(r.flavor)/ COUNT(r.id_member) FROM food_rating r WHERE r.id_food=f.id_food) AS t_flavor,(SELECT SUM(r.freshness)/ COUNT(r.id_member) FROM food_rating r WHERE r.id_food=f.id_food) AS t_freshness,(SELECT SUM(r.cooking)/ COUNT(r.id_member) FROM food_rating r WHERE r.id_food=f.id_food) AS t_cooking,(SELECT SUM(r.aroma)/ COUNT(r.id_member) FROM food_rating r WHERE r.id_food=f.id_food) AS t_aroma,(SELECT SUM(r.serving)/ COUNT(r.id_member) FROM food_rating r WHERE r.id_food=f.id_food) AS t_serving,(SELECT (SUM(r.cleanlines) + SUM(r.flavor) + SUM(r.freshness) + SUM(r.cooking) + SUM(r.aroma) + SUM(r.serving)) / COUNT(r.id_member) FROM food_rating r WHERE r.id_food =f.id_food) AS total_rate FROM food f LEFT JOIN food_category k ON f.id_food_category=k.id_food_category LEFT JOIN price_index p ON f.id_price_index=p.id_price_index  LEFT JOIN msg_level l ON f.id_msg_level=l.id_msg_level LEFT JOIN restaurant r ON f.id_restaurant=r.id_restaurant LEFT JOIN type_of_business a ON r.id_type_of_business = a.id_type_of_business LEFT JOIN negara b ON r.id_negara = b.id_negara LEFT JOIN propinsi d ON r.id_propinsi = d.id_propinsi LEFT JOIN kota e ON r.id_kota = e.id_kota LEFT JOIN landmark g ON r.id_landmark = g.id_landmark LEFT JOIN member n ON f.id_member = n.id_member where f.id_food='$id'");
$r=mysqli_fetch_array($resto);
$id_resto = id_masking($r['id_restaurant']);
$seo_nama = seo($r['restaurant_name']);
$slug=seo($r['nama_food']);
$post=date("jS M, Y", strtotime($r['tgl_post']));
$total_cleanlines=number_format((float)$r['t_cleanlines'], 2, '.', '');
$img_cleanlines=$total_cleanlines / 0.21;
$total_flavor=number_format((float)$r['t_flavor'], 2, '.', '');
$img_flavor=$total_flavor / 0.20;
$total_freshness=number_format((float)$r['t_freshness'], 2, '.', '');
$img_freshness=$total_freshness / 0.18;
$total_cooking=number_format((float)$r['t_cooking'], 2, '.', '');
$img_cooking=$total_cooking / 0.17;
$total_aroma=number_format((float)$r['t_aroma'], 2, '.', '');
$img_aroma=$total_aroma / 0.15;
$total_serving=number_format((float)$r['t_serving'], 2, '.', '');
$img_serving=$total_serving / 0.09;
$total_rating=number_format((float)$r['total_rate'], 2, '.', '');
$gsql=mysqli_query($koneksi,"select p.gambar_food_photo,p.nama_food_photo, (select count(l.id_food_photo_like) from food_photo_like l where l.id_food_photo=p.id_food_photo) as total_like from food_photo p where p.id_food='$id' order by total_like desc,p.id_food_photo limit 1");
$g=mysqli_fetch_array($gsql);
$gambar=$g['gambar_food_photo'];

?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
  <title><?php echo"$r[nama_food]"; ?> - Foodie Guidances</title>

	<meta name="keywords" content="<?php echo"$r[nama_food]"; ?>, Rating : <?php echo $total_rating;?>">
  <meta name="description" content="<?php echo"$r[nama_food]"; ?>, Rating : <?php echo $total_rating;?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<meta property="og:title" content="<?php echo"$r[nama_food]"; ?>" />
	<meta property="og:url" content="<?php echo"$base_url/pages/food/info/$_GET[id]/$slug"; ?>" />
	<meta property="og:image" content="<?php echo"$base_url/assets/img/food/big_$gambar"; ?>" />
	<meta property="og:description" content="<?php echo"$r[nama_food], $r[street_address], Rating: $total_rating "; ?>" />
	<meta property="og:description" content="<?php echo"$r[nama_food], $r[street_address], Rating: $total_rating "; ?>" />
	<meta property="og:site_name" content="Foodie Guidances" />
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/slick.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/lightbox.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/owl.carousel.css" rel="stylesheet">
  <link href="<?php echo"$base_url"; ?>/assets/css/owl.theme.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>
	<link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">
</head>
 <body style="position: initial;" class="drawer drawer--right">
	<?php include"config/inc/header.php"; ?>
    <!-- Fixed navbar -->
    <div class="container hook">
		<div class="row">
			<div class="col-12 col-8a">
				<div class="preview" style="">
					<img src="<?php echo" $base_url/assets/img/review.png"; ?>"  width="100%">
				</div>
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="<?php echo"$base_url/pages/food/search/"; ?>">Food</a></li>
					<li class="active"><?php echo"$r[nama_food]"; ?></li>
				</ol>
				<?php
					if(isset($_SESSION['resto_notif'])){
						if($_SESSION['resto_notif']=="suka"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully like this food.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="suka_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was liked this food before. You can only do one time in 24 hours.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="rekomendasi"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully recommend this food.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="rekomendasi_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was recommeded this food before. You can only do one time in 24 hours.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="bookmark"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully bookmark this food.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="bookmark_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was bookmark this food before. You can only do one time.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="disini"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully added the data to taste it at this food.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="disini_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was added the data at this food before. You can only do one time.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="lapor"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully report this food.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="lapor_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was reported this food before. You can only do one time.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="rating"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully rating this food.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="rating_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was rated this food before. You can only do one time in 24 hours.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="rating_pilih"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> Please select star rating value before vote.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="login_dulu"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Must Login!</strong> You must login or register before doing this action.
							</div>";
						}
						unset($_SESSION['resto_notif']);
					}
				?>

				<h1 class="judul">
					<?php
					echo"$r[nama_food] <br>";
					if($total_rating<>0){
					?>
						<a href = "javascript:void(0)" style="text-decoration: none;" class='klik-bintang'>
							<?php echo"<input type='hidden' class='rating' data-filled='fa fa-star' data-empty='fa fa-star-o' readonly='readonly' value='$total_rating'>";?>
						</a>
						<?php echo"
							<span style='font-size: 14px;'>Rating : $total_rating </span><em class='f-12'>from $r[jumlah_vote] votes</em>";
					}
					?>
				</h1>
				<div class="mb10 f-12"><em>Category</em> <?php echo"$r[nama_food_category]"; ?> <span class="bullet">&#8226;</span>
					<a href="<?php echo"$base_url/$r[username]"; ?>"><em>by</em> <?php if(!empty($r['nama_depan'])){echo"$r[nama_depan] $r[nama_belakang]";}else{echo"Admin";} ?></a>
					<span class="bullet">&#8226;</span> <em>post</em> <?php echo"$post"; ?>
				</div>
				<div class="box-info-1">

					<?php
						include 'config/func/kiri_food.php';
					?>


					<div class="pull-right">
					  <div class="detail-menu">
					    <div class="" style="float: left; width: 20%; padding:5px; padding-left:0; padding-top:0; padding-bottom:10px; margin-top: -20px; text-align: center">
					      <img src="<?php echo"$base_url";?>/assets/images/globe2.png" width="100%">
					      <?php
					        $pr=mysqli_fetch_array(mysqli_query($koneksi, "SELECT *
											FROM (
											  SELECT @curRank := @curRank + 1 AS rank
											       , b.id_food
											       , b.nama_food
					                   , b.jumlah
					                   , b.rank_vote
					                   , b.rank_like
											  FROM food b
											  CROSS JOIN (SELECT @curRank := 0) vars
											  ORDER BY jumlah desc, rank_vote DESC, rank_like DESC, nama_food ASC
											) p WHERE p.id_food=$id"));
					          echo "#".$pr['rank'];
					          $rank_global=$pr['rank'];
					       ?>
					    </div>
					    <div class="" style="float: left; width: 20%; padding:5px; padding-left:0; padding-top:0; padding-bottom:10px; margin-top: -20px; text-align: center">
					      <img src="<?php echo"$base_url";?>/assets/images/flag.png" width="100%">
					      <?php
					        $pr=mysqli_fetch_array(mysqli_query($koneksi, "SELECT *
											FROM
											  (SELECT
					                            @curRank := @curRank + 1 AS rank
					                            , b.id_food
					                            , b.id_restaurant
					                            , b.nama_food
					                            , b.jumlah
					                            , b.rank_vote
					                            , b.rank_like
					                            , b.negara
											  FROM food b
											  CROSS JOIN (SELECT @curRank := 0) vars  WHERE negara=$r[kode_negara]
											  ORDER BY jumlah desc, rank_vote DESC, rank_like DESC, nama_food ASC
											) p WHERE id_food=$id ORDER BY rank asc
					        "));
					          echo "#".$pr['rank'];
					          $rank_negara=$pr['rank'];
					       ?>
					    </div>
					    <div class="" style="float: left; width: 20%; padding:5px; padding-left:0; padding-top:0; padding-bottom:10px; margin-top: -20px; text-align: center">
					      <img src="<?php echo"$base_url";?>/assets/images/prov.png" width="100%">
					      <?php
					        $pr=mysqli_fetch_array(mysqli_query($koneksi, "SELECT *
											FROM
											  (SELECT
					                            @curRank := @curRank + 1 AS rank
					                            , b.id_food
					                            , b.id_restaurant
					                            , b.nama_food
					                            , b.jumlah
					                            , b.rank_vote
					                            , b.rank_like
					                            , b.negara
											  FROM food b
											  CROSS JOIN (SELECT @curRank := 0) vars  WHERE propinsi=$r[id_propinsi]
											  ORDER BY jumlah desc, rank_vote DESC, rank_like DESC, nama_food ASC
											) p WHERE id_food=$id ORDER BY rank asc
					        "));
					          echo "#".$pr['rank'];
					          $rank_state=$pr['rank'];
					      ?>
					    </div>
					    <div class="" style="float: left; width: 20%; padding:5px; padding-left:0; padding-top:0; padding-bottom:10px; margin-top: -20px; text-align: center">
					      <img src="<?php echo"$base_url";?>/assets/images/city.png" width="100%">
					      <?php
					        $pr=mysqli_fetch_array(mysqli_query($koneksi, "SELECT *
											FROM
											  (SELECT
					                            @curRank := @curRank + 1 AS rank
					                            , b.id_food
					                            , b.id_restaurant
					                            , b.nama_food
					                            , b.jumlah
					                            , b.rank_vote
					                            , b.rank_like
					                            , b.negara
											  FROM food b
											  CROSS JOIN (SELECT @curRank := 0) vars  WHERE kota=$r[id_kota]
											  ORDER BY jumlah desc, rank_vote DESC, rank_like DESC, nama_food ASC
											) p WHERE id_food=$id ORDER BY rank asc"));
					          echo "#".$pr['rank'];
					          $rank_city=$pr['rank'];
					      ?>
					    </div>
					    <div class="" style="float: left; width: 20%; padding:5px; padding-left:0; padding-top:0; padding-bottom:10px; margin-top: -20px; text-align: center">
					      <img src="<?php echo"$base_url";?>/assets/images/mall.png" width="100%">
					      <?php
					      if ($r['id_mall']<>0) {
					        $pr=mysqli_fetch_array(mysqli_query($koneksi, "SELECT *
											FROM
											  (SELECT
					                            @curRank := @curRank + 1 AS rank
					                            , b.id_food
					                            , b.id_restaurant
					                            , b.nama_food
					                            , b.jumlah
					                            , b.rank_vote
					                            , b.rank_like
					                            , b.negara
											  FROM food b
											  CROSS JOIN (SELECT @curRank := 0) vars  WHERE mall=$r[id_mall]
											  ORDER BY jumlah desc, rank_vote DESC, rank_like DESC, nama_food ASC
											) p WHERE id_food=$id ORDER BY rank asc"));
					          echo "#".$pr['rank'];
					          $rank_mall=$pr['rank'];
					      }
					        else {
					          echo "#N.A.";
					          $rank_mall="N.A.";
					        }
					      ?>
					    </div>
					    <hr>
					    <div style="width: 25%; float: left">
					      <div class="signature">
					        <?php
					        if($r['type_serving']=="Halal"){
					          echo"<img src='$base_url/assets/img/theme/logo-halal.png'>";
					        }
					        elseif($r['type_serving']=="Yes"){
					          echo"<img src='$base_url/assets/img/theme/icon_pork.png'>";
					        }
					        elseif($r['type_serving']=="No"){
					          echo"<img src='$base_url/assets/img/theme/icon_halal.png'>";
					        }
					        elseif($r['type_serving']=="vege"){
					          echo"<img src='$base_url/assets/img/theme/icon_vegetarian.png'>";
					        }
					        else{
					          echo"<img src='$base_url/assets/img/theme/kos.png'>";
					        }
					        ?>
					      </div>
					    </div>
					    <div style="width: 75%; float: left; text-align: left">
					      <?php error_reporting(0);
					        $me=mysqli_query($koneksi,"select tgl_like from food_like where id_member='$id_member' and id_food='$id' order by id_food_like desc limit 1");
					        $ada_melike=mysqli_num_rows($me);
					        if($ada_melike<>0){
					          $q=mysqli_fetch_array($me);
					          echo'<a href="#" class="f-merah" title="Like"><i class="fa fa-heart-o"></i></a>';
					        }
					        else {
					          echo '<a href="'.$base_url.'/pages/food/event/info/'.$_GET['id'].'/1" title="Like"><i class="fa fa-heart-o"></i></a>';
					        }
					       ?>
					       <?php
					        $me=mysqli_query($koneksi,"select tgl_rekomendasi from food_rekomendasi where id_member='$id_member' and id_food='$id' order by id_food_rekomendasi desc limit 1");
					        $ada_melike=mysqli_num_rows($me);
					        if($ada_melike<>0){
					          $q=mysqli_fetch_array($me);
					          echo'<a href="#" title="Recommendation" class="f-merah"><i class="fa fa-thumbs-o-up"></i></a>';
					        }
					        else {
					          echo '<a href="'.$base_url.'/pages/food/event/info/'.$_GET["id"].'/2" title="Recommendation"><i class="fa fa-thumbs-o-up"></i></a>';
					        }
					       ?>
					       <?php
					        $me=mysqli_query($koneksi,"select id_bookmark from bookmark where id_member='$id_member' and id='$id' order by id_bookmark desc limit 1");
					        $ada_melike=mysqli_num_rows($me);
					        if($ada_melike<>0){
					          $q=mysqli_fetch_array($me);
					          echo'<a href="#" title="Bookmark" class="f-merah"><i class="fa fa-bookmark-o"></i></a>';
					        }
					        else {
					          echo '<a href="'.$base_url.'/pages/food/event/info/'.$_GET["id"].'/3" title="Bookmark"><i class="fa fa-bookmark-o"></i></a>';
					        }
					       ?>
					       <?php
					        $me=mysqli_query($koneksi,"select tgl_here from food_here where id_member='$id_member' and id_food='$id' order by id_food_here desc limit 1");
					        $ada_melike=mysqli_num_rows($me);
					        if($ada_melike<>0){
					          $q=mysqli_fetch_array($me);
					          echo'<a href="#" title="Taste it" class="f-merah"><i class="fa fa-cutlery"></i></a>';
					        }
					        else {
					          echo '<a href="'.$base_url.'/pages/food/event/info/'.$_GET["id"].'/4" title="Taste it"><i class="fa fa-cutlery"></i></a>';
					        }
					       ?>
					      <a href="">
					        <span class="social-share"><i class="fa fa-share-alt"></i>
					          <div class="social-box">
					            <span class='st_facebook_large' displayText='Facebook'></span>
					            <span class='st_linkedin_large' displayText='LinkedIn'></span>
					            <span class='st_pinterest_large' displayText='Pinterest'></span>
					            <span class='st_googleplus_large' displayText='Google +'></span>
					            <span class='st_twitter_large' displayText='Tweet'></span>
					            <span class='st_sharethis_large' displayText='ShareThis'></span>
					          </div>
					        </span>
					      </a>
					      <a href="<?php echo"$base_url/pages/food/event/info/$_GET[id]/5"; ?>" title="Report"><i class="fa fa-flag-o"></i></a>
					      <br>
					    </div>
					  </div>
					  <div style="width:100%; float: left; margin-bottom: 18px"></div>
					  <div style="width: 25%; float: left">
					    <a href = "javascript:void(0)" onclick = "initMap();document.getElementById('map-pop').style.display='block';document.getElementById('fade-map').style.display='block'">
					      <img src="https://www.foodieguidances.com/assets/images/maps.png" width='100%' style="padding: 0px; width: 50px">
					    </a>
					  </div>
					  <div style="width: 75%; float: left">
					    <div class="statistik">
					      <em>
					        <i class="fa fa-heart"></i>
					        <a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">
					          <?php
					            if ($r['dilike'] > 1) {
					              echo"$r[dilike] likes";
					            }
					            else {
					              echo"$r[dilike] like";
					            }
					          ?>
					        </a>
					      </em>
					      <em><i class="fa fa-eye"></i> <?php
					      if ($r['dilihat'] > 1) {
					        echo"$r[dilihat] Views";
					      }
					      else {
					         echo"$r[dilihat] View";
					      }
					    ?></em>
					      <em><i class="fa fa-thumbs-up"></i>
					        <a href = "javascript:void(0)" onclick = "document.getElementById('lighta').style.display='block';document.getElementById('fadea').style.display='block'">
					          <?php
					            if ($r['direkomendasi'] > 1) {
					              echo"$r[direkomendasi] recommendations";
					            }
					            else {
					              echo"$r[direkomendasi] recommendation";
					            }
					           ?>
					        </a>
					        </em>
					    </div>
					  </div>
					  <div class="row">
					    <div class="col-16" style="margin-top:13px">
					      <a href="#" class="btn btn-success btn-block" style="width:48%">
					        Add Food
					      </a>
					      <a href="#" class="btn btn-success btn-block" style="width: 50%; float: right; margin-top: -34px;">
					        Add Beverage
					      </a>
					      <a href="#" class="btn btn-danger btn-block" style="width: 32%; font-size: 10px; float: left; margin-right: 1.9%; padding: 10px 5px;">New Restaurant</a>
					      <a href="#" class="btn btn-danger btn-block"  style="width: 32%; font-size: 10px; float: left; margin-right: 1.9%; padding: 10px 5px;">
					        New Food
					      </a>
					      <a href="#" class="btn btn-danger btn-block"  style="width: 32%; font-size: 10px; float: left; padding: 10px 5px;">
					        New Beverage
					      </a>
					      <a class="btn btn-danger btn-block" href="#tab-vote" id="go-to-vote" style="float:left">Vote</a>
					    </div>
					  </div>
					</div>



					<div class="clearfix"></div>
					<div class="mt20 tag">Tagged under
						<?php
						$tag  = explode(",",$r['id_tag']);
						foreach ( $tag as $item_tag ) {
							echo"<a href='$base_url/pages/food/search/?tag=$item_tag'>#$item_tag</a> ";
						}
						?>
					</div>
				</div>
				<div class="box-info-2">
					<div class="box-head">
						Basic Information <?php if(!empty($_SESSION['food_member'])){ echo"<a href='$base_url/$u[username]/my-restaurant/edit/$r[id_restaurant]'>Edit Restaurant Information</a>";}?>
					</div>
					<div class="box-panel">
						<dl class="dl-horizontal">
							<?php
							echo"<dt>Restaurant</dt><dd><a class='f-merah' href='$base_url/pages/restaurant/info/$id_resto/$seo_nama'>$r[restaurant_name]</a></dd>";
							if(!empty($r['street_address'])){
								echo"<dt>Address</dt><dd>$r[street_address]</dd>";
							}
							echo"<dt>City</dt><dd>$r[nama_kota]</dd>
							<dt>State/Province</dt><dd>$r[nama_propinsi]</dd>
							<dt>Country</dt><dd>$r[nama_negara]</dd>";
							if(!empty($r['postal_code'])){
								echo"<dt>Postal Code</dt><dd>$r[postal_code]</dd>";
							}
							echo"<dt>Type of Business</dt><dd>$r[nama_type_of_business]</dd>";
							if($r['id_landmark']<>0){
								echo"<dt>Landmark</dt><dd>$r[nama_landmark]</dd>";
							}
							if(!empty($r['telephone'])){
								echo"<dt>Telephone</dt><dd>$r[telephone]</dd>";
							}
							if(!empty($r['fax'])){
								echo"<dt>Faxcimile</dt><dd>$r[fax]</dd>";
							}
							if(!empty($r['reservation_phone'])){
								echo"<dt>Reservation Phone</dt><dd>$r[reservation_phone]</dd>";
							}
							if(!empty($r['email_resto'])){
								echo"<dt>Email</dt><dd>$r[email_resto]</dd>";
							}
							if(!empty($r['web'])){
								echo"<dt>Website</dt><dd><a href='http://$r[web]' class='f-merah'>$r[web]</a></dd>";
							}
							if(!empty($r['facebook'])){
								echo"<dt>Facebook Page</dt><dd><a href='http://www.facebook.com/$r[facebook]' class='f-merah'>$r[facebook]</a></dd>";
							}
							if(!empty($r['twitter'])){
								echo"<dt>Twitter Page</dt><dd><a href='http://www.twitter.com/$r[twitter]' class='f-merah'>$r[twitter]</a></dd>";
							}
							if(!empty($r['instagram'])){
								echo"<dt>Instagram Page</dt><dd><a href='http://www.instagram.com/$r[instagram]' class='f-merah'>$r[instagram]</a></dd>";
							}
							if(!empty($r['pinterest'])){
								echo"<dt>Pinterest Page</dt><dd><a href='http://www.pinterest.com/$r[pinterest]' class='f-merah'>".$r[pinterest]."</a></dd>";
							}
							?>
						</dl>
					</div>
				</div>
				<div role="tabpanel">
					<ul class="nav nav-tabs" role="tablist">
						<li class="active"><a href="#">Info</a></li>
						<li><a href="#">Other Photo</a></li>
						<li><a href="#">Voter</a></li>
						<li><a href="#">Restaurant Map</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-heading">
							<?php if(!empty($_SESSION['food_member'])){ echo"<a href='$base_url/$u[username]/my-food/edit/$r[id_food]'>Edit Food Information</a>";}?>
							<div class="clearfix"></div>
						</div>
						<div class="tab-panel">
							<ul class="list-unstyled">
								<?php
								echo"<li><em>Category</em><span><i class='fa fa-check'></i> $r[nama_food_category]</span></li>
								<li><em>Cuisine</em>
									<div class='row'>";
									$cuisine  = explode("+",$r['id_cuisine']);
									foreach ( $cuisine as $item_cuisine ) {
										echo"<div class='mb5'><i class='fa fa-check'></i> $item_cuisine</div>";
									}
								echo"</div></li>";
								if($r['cooking_methode']<>""){
									echo"<li><em>Cooking Methode</em>
									<div class='row'>";
									$cooking  = explode("+",$r['cooking_methode']);
									foreach ( $cooking as $item_cooking ) {
										echo"<div class='col-8 mb5'><i class='fa fa-check'></i> $item_cooking</div>";
									}
									echo"</div></li>";
								}
								if($r['id_price_index']<>0){
									echo"<li><em>Price Index</em><span><i class='fa fa-check'></i> $r[nama_price_index]</span></li>";
								}
								if($r['id_msg_level']<>0){
									echo"<li><em>Monosodium Glutamate Level</em><span><i class='fa fa-check'></i> $r[nama_msg_level]</span></li>";
								}
								?>
							</ul>
						</div>
					</div>
				</div>
				<div role="tabpanel" id="tab-vote">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#result" aria-controls="result" role="tab" data-toggle="tab">Result</a></li>
						<li role="presentation"><a href="#feature" aria-controls="feature" role="tab" data-toggle="tab">Feature Vote</a></li>
						<li role="presentation"><a href="#quick" aria-controls="quick" role="tab" data-toggle="tab">Quick Vote</a></li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane tab-panel active fade in" id="result">
							<table class="tablebintang">
								<tr>
									<td>Cleanliness <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall cleanliness of the food and plates served, which can include whether the food has unwanted insect, plastic, hair or unclean plates, bowls and utensils used."></i></td>
									<td><input type="hidden" class="rating" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" readonly="readonly" value="<?php echo"$img_cleanlines"; ?>"></td>
									<td rowspan="5" class="mene-atas">
										<span><?php echo"$total_rating"; ?></span>
										<em>from <?php echo"$r[jumlah_vote]"; ?> vote</em>
									</td>
								</tr>
								<tr>
									<td>Flavour <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall taste of the food which has unique personality."></i></td>
									<td><input type="hidden" class="rating" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" readonly="readonly" value="<?php echo"$img_flavor"; ?>"></td>
								</tr>
								<tr>
									<td>Freshness <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment on the food ingredient's freshness which can include meat, seafood, vegetables and sauces."></i></td>
									<td><input type="hidden" class="rating" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" readonly="readonly" value="<?php echo"$img_freshness"; ?>"></td>
								</tr>
								<tr>
									<td>Cooking <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment of the way the food is being cooked wheither it is undercooked or overcooked. However, it also depend on how we like the food to be cook."></i></td>
									<td><input type="hidden" class="rating" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" readonly="readonly" value="<?php echo"$img_cooking"; ?>"></td>
								</tr>
								<tr>
									<td>Presentasion &amp; Aroma <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment on the food's presentation and its aroma. A good presentation on food will increase our appetite for the food. likewise for the aroma too. A fragrance food cook meters away can attract our sense of smell. Good food makes better with great presentation and aroma."></i></td>
									<td><input type="hidden" class="rating" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" readonly="readonly" value="<?php echo"$img_aroma"; ?>"></td>
								</tr>
								<tr>
									<td>Serving <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Is the food being served having adequate serving? Too much or too little? Unlike Western cuisine which have courses of food for a complete meal, Eastern cuisine are more direct and usually treat as the main course. Therefore, the serving is vary."></i></td>
									<td><input type="hidden" class="rating" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" readonly="readonly" value="<?php echo"$img_serving"; ?>"></td>
								</tr>
								<tr class="total-vote">
									<td colspan="2">
										<span><?php echo"$total_rating"; ?></span>
										<em>from <?php echo"$r[jumlah_vote]"; ?> vote</em>
									</td>
								</tr>
							</table>
						</div>
						<div role="tabpanel" class="tab-pane tab-panel fade" id="feature">
							<form method="get" action="#">
								<input type="hidden" value="6" name="fungsi">
								<input type="hidden" value="info" name="page">
								<input type="hidden" value="<?php echo"$_GET[id]"; ?>" name="id">
								<table class="tablebintang">
									<tr>
										<td>Cleanliness <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall cleanliness of the food and plates served, which can include whether the food has unwanted insect, plastic, hair or unclean plates, bowls and utensils used."></i></td>
										<td><input type="hidden" class="rating" id="rat1" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="cleanlines"></td>
										<td rowspan="5" class="mene-atas"><button class="btn btn-danger" type="reset">Vote</button>
											<div class='alert-danger alert-dismissible as' role='alert'>
						                  <strong>Please submit minimum one star for each rating categories in other to be rated</strong>
						               </div>
										</td>
									</tr>
									<tr>
										<td>Flavour <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall taste of the food which has unique personality."></i></td>
										<td><input type="hidden" class="rating" id="rat2" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="flavor"></td>
									</tr>
									<tr>
										<td>Freshness <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment on the food ingredient's freshness which can include meat, seafood, vegetables and sauces."></i></td>
										<td><input type="hidden" class="rating" id="rat3" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="freshness"></td>
									</tr>
									<tr>
										<td>Cooking <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment of the way the food is being cooked wheither it is undercooked or overcooked. However, it also depend on how we like the food to be cook."></i></td>
										<td><input type="hidden" class="rating" id="rat4" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="cooking"></td>
									</tr>
									<tr>
										<td>Presentation &amp; Aroma <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment on the food's presentation and its aroma. A good presentation on food will increase our appetite for the food. likewise for the aroma too. A fragrance food cook meters away can attract our sense of smell. Good food makes better with great presentation and aroma."></i></td>
										<td><input type="hidden" class="rating" id="rat5" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="aroma"></td>
									</tr>
									<tr>
										<td>Serving <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Is the food being served having adequate serving? Too much or too little? Unlike Western cuisine which have courses of food for a complete meal, Eastern cuisine are more direct and usually treat as the main course. Therefore, the serving is vary."></i></td>
										<td><input type="hidden" class="rating" id="rat6" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="serving"></td>
									</tr>
									<tr class="total-vote">
										<td colspan="2">
											<button class="btn btn-danger" type="reset">Vote</button>
											<div class='alert-danger alert-dismissible as' role='alert'>
						                  <strong>Please submit minimum one star for each rating categories in other to be rated</strong>
						               </div>
										</td>
									</tr>
								</table>
							</form>
						</div>
						<div role="tabpanel" class="tab-pane tab-panel fade" id="quick">
							<form method="get" action="#">
								<input type="hidden" value="6" name="fungsi">
								<input type="hidden" value="info" name="page">
								<input type="hidden" value="<?php echo"$_GET[id]"; ?>" name="id">
								<table class="tablebintang">
									<tr>
										<td><strong>Give your vote</strong></td>
										<td><input type="hidden" class="rating" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="rating"></td>
									</tr>
									<tr class="total-vote">
										<td colspan="2"><button class="btn btn-danger" type="reset">Vote</button></td>
									</tr>
								</table>
							</form>
						</div>
					</div>
					<ul class="list-inline">
						<li><i class="fa fa-star"></i> Poor</li>
						<li><i class="fa fa-star"></i><i class="fa fa-star"></i> Below Average</li>
						<li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Average</li>
						<li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Very Good</li>
						<li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Excellent</li>
					</ul>
				</div>
				<?php
				$here=mysqli_query($koneksi,"select * from food_here left join member on food_here.id_member=member.id_member where id_food='$id'");
				$jumlah_here=mysqli_num_rows($here);
				?>
				<hr>
				<?php $idu = id_masking($_GET['id']); ?>
				<a href="<?php echo "$base_url/$username/my-food/delete/$id/$gambar"; ?>" class="btn btn-danger">Cancel</a>
				<a href="<?php echo "$base_url/$username/my-food/edit/$idu"; ?>" class="btn btn-warning">Edit</a>
				<a href="<?php echo "$base_url/$username/submit/food/$id"; ?>" class="btn btn-success">Save</a>
			</div>
			<div class="col-4 mene-atas">
				<?php include"config/inc/search.php"; ?>
				<?php include"config/inc/iklan.php"; ?>
				<div class="fb-like-box" data-href="https://www.facebook.com/foodieguidances#_=_" data-width="220" data-height="350" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
			</div>
		</div>
    </div>
    <?php include"config/inc/footer.php"; ?>
	 <?php
	 $idu = id_masking($_GET['id']);
		 $pb=mysqli_query($koneksi, "select * from food where id_food < $idu ORDER BY id_food desc limit 1");
	 $jumpb=mysqli_num_rows($pb);
	 $tpb=mysqli_fetch_array($pb);
	 if ($jumpb<>0) {
		 $ba = id_masking($tpb['id_food']);
		 $slug=seo($tpb['nama_food']);
		 $next="$base_url/pages/food/info/$ba/$slug";
	 } else {
		 $pba=mysqli_query($koneksi, "select * from food ORDER BY id_food desc limit 1");
		 $jumpba=mysqli_num_rows($pba);
		 $tpba=mysqli_fetch_array($pba);
		 $baa = id_masking($tpba['id_food']);
		 $sluga=seo($tpba['nama_food']);
		 $next="$base_url/pages/food/info/$baa/$sluga";
	 }

	 $pn=mysqli_query($koneksi, "select * from food where id_food > $idu ORDER BY id_food limit 1");
	 $jumpn=mysqli_num_rows($pn);
	 $tpn=mysqli_fetch_array($pn);
	 if ($jumpn<>0) {
		 $na = id_masking($tpn['id_food']);
		 $slug=seo($tpn['nama_food']);
		 $back="$base_url/pages/food/info/$na/$slug";
	 } else {
		 $pna=mysqli_query($koneksi, "select * from food ORDER BY id_food limit 1");
		 $jumpna=mysqli_num_rows($pna);
		 $tpna=mysqli_fetch_array($pna);
		 $naa = id_masking($tpna['id_food']);
		 $sluga=seo($tpna['nama_food']);
		 $back="$base_url/pages/food/info/$naa/$sluga";
	 }
 ?>
 <a class="scrollToNext" href="<?php echo $back ?>" style="bottom: 320px;"><img src="<?php echo"$base_url/assets/img/theme/kiri.png" ?>"></a>
 <a class="scrollToNext" href="<?php echo $next ?>" style="bottom: 273px;"><img src="<?php echo"$base_url/assets/img/theme/kanan.png" ?>"></a>
 <a href="#" class="scrollToTop"><img src="<?php echo"$base_url/assets/img/theme/atas.png" ?>"></a>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap-rating.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/lightbox.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/slick.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.lazyload.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/theme.js"></script>
	<script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>
	<script type="text/javascript">stLight.options({publisher: "7b97d330-d7b9-49c3-b5ee-b3aaa89bbe66", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

	<script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
   <script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
   <script>
      $(document).ready(function() {
	      $('.drawer').drawer();

			$('.tutup').click(function(){
				$('.iklanan').toggleClass('hilang', 500);
			});

			$('.cart').click(function(e){
				e.stopPropagation();
				$('.dis-cart').toggle();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$('.plus').click(function(e){
				e.stopPropagation();
				$('.dis-plus').toggle();
				$('.dis-cart').hide();
				$('.dis-share').hide();
			});
			$('.share').click(function(e){
				e.stopPropagation();
				$('.dis-share').toggle();
				$('.dis-plus').hide();
				$('.dis-cart').hide();
			});
			$('.drawer-toggle').click(function(){
				$('.dis-cart').hide();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$(document).click(function () {
				 var $el = $(".dis-share");
				 if ($el.is(":visible")) {
					  $el.fadeOut(200);
				 }
				 var $ela = $(".dis-plus");
				 if ($ela.is(":visible")) {
					  $ela.fadeOut(200);
				 }
				 var $elu = $(".dis-cart");
				 if ($elu.is(":visible")) {
					  $elu.fadeOut(200);
				 }
		   });

      });
   </script>

	<script type="text/javascript">
	$(document).ready(function() {
		$(window).scroll(function(){
	 		  if ($(this).scrollTop() > 100) {
	 			  $('.scrollToTop').fadeIn();
	 		  } else {
	 			  $('.scrollToTop').fadeOut();
	 		  }
	 	  });

	 	  $(window).scroll(function(){
	 		 if ($(this).scrollTop() > 50) {
	 			 $('.scrollToNext').fadeIn();
	 		 } else {
	 			 $('.scrollToNext').fadeOut();
	 		 }
	 	  });

			$('#go-to-vote').click(function() {
				$('html, body').animate({
					scrollTop: $( $(this).attr('href') ).offset().top
				}, 500);
				$('#tab-vote a[href="#feature"]').tab('show');
				return false;
			});
	});
	</script>

	<script src="<?php echo"$base_url"; ?>/assets/js/owl.carousel.js"></script>
	<link href="<?php echo"$base_url"; ?>/assets/css/owl.carousel.css" rel="stylesheet">
   <link href="<?php echo"$base_url"; ?>/assets/css/owl.theme.css" rel="stylesheet">
	<style>
	#owl-demo .item{
		 margin: 3px;
	}
	#owl-demo .item img{
		 display: block;
		 width: 100%;
		 height: auto;
	}
	</style>
	<script>
		$(document).ready(function() {
		  $("#owl-demo").owlCarousel({
			 items : 4,
			 lazyLoad : true,
			 navigation : true
		  });
		});
	</script>

	<div id="fb-root"></div>
	<style media="screen">
		#de{position: relative;float: left;}
		#next{color: rgb(0, 0, 0);
position: relative;
float: left;}
#back{color: rgb(0, 0, 0);
position: relative;
float: left;}
	</style>
	<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=348272391978609&version=v2.0";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
	<script type="text/javascript">
		$(document).ready(function () {
			$(".as").hide();
	      var rat1 = $("#rat1").val();
	      var rat2 = $("#rat2").val();
	      var rat3 = $("#rat3").val();
	      var rat4 = $("#rat4").val();
	      var rat5 = $("#rat5").val();
	      var rat6 = $("#rat6").val();
	      $("#rat1,#rat2,#rat3,#rat4,#rat5,#rat6").change(function(){
	         if ((rat1 < 1) && (rat2 < 1) && (rat3 < 1) && (rat4 < 1) && (rat5 < 1) && (rat6 < 1) ) {
	            $(".btn-danger").hide();
	            $(".as").show();
	         }
	      });

	      $("#rat1,#rat2,#rat3,#rat4,#rat5,#rat6").change(function(){
	         var rat1 = $("#rat1").val();
	         var rat2 = $("#rat2").val();
	         var rat3 = $("#rat3").val();
	         var rat4 = $("#rat4").val();
	         var rat5 = $("#rat5").val();
	         var rat5 = $("#rat5").val();
	         var rat6 = $("#rat6").val();
	         if ((rat1 > 0) && (rat2 > 0) && (rat3 > 0) && (rat4 > 0) && (rat5 > 0) && (rat6 > 0)) {
	            $(".btn-danger").show();
	            $(".as").hide();
	         }
	      });

			$("img.lazy").lazyload({
				effect : "fadeIn"
			});
			$(".alert").fadeTo(5000, 500).fadeOut(500, function(){
				$(".alert").alert('close');
			});
			$('.tip').tooltip();
			$('.rating').rating();
			$('.member-carousel').slick({
				infinite: true,
				slidesToShow: 14,
				slidesToScroll: 14,
				arrows:false,
				autoplay:true,
				autoplaySpeed: 10000
			});
			$('#go-to-vote').click(function() {
				$('html, body').animate({
					scrollTop: $( $(this).attr('href') ).offset().top
				}, 500);
				$('#tab-vote a[href="#feature"]').tab('show');
				return false;
			});
			$('.social-share').click( function(event){
				event.stopPropagation();
				$( ".social-box" ).toggle(1);
			});
			$('.carousel').carousel({
				interval: 10000
			});
			$(document).click( function(){
				$('.social-box').hide();
			});
		});
		/* * * CONFIGURATION VARIABLES * * */
		var disqus_shortname = 'foodieguidancescom';

		/* * * DON'T EDIT BELOW THIS LINE * * */
		(function() {
			var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
			dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
			(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
		})();
	</script>
	<?php
		include "config/func/foot_food.php";
	?>
	<style media="screen">

	</style>
	<script type="text/javascript"></script>
</body>
</html>
