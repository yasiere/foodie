<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";
include "config/func/jumlah_data.php";
include "config/func/id_masking.php";
$auto_logout=1800000;
if(!empty($_SESSION['food_member'])){
	if (time()-$_SESSION['timestamp']>$auto_logout){
		session_destroy();
		session_unset();
		header("Location: ".$base_url."/auto-logout");
		exit();
	}else{
		$_SESSION['timestamp']=time();
	}
	include "config/func/member_data.php";
}
$id = id_masking($_GET['id']);
$active="food";
$type="food_map";
$resto=mysqli_query($koneksi,"select *,r.id_negara as kode_negara ,f.dilihat,f.id_price_index,(SELECT COUNT(h.id_member) FROM food_like h WHERE h.id_food=f.id_food) AS dilike,(SELECT COUNT(h.id_member) FROM food_rekomendasi h WHERE h.id_food=f.id_food) AS direkomendasi,(SELECT COUNT(r.id_member) FROM food_rating r WHERE r.id_food=f.id_food) AS jumlah_vote,(SELECT SUM(r.cleanlines)/ COUNT(r.id_member) FROM food_rating r WHERE r.id_food=f.id_food) AS t_cleanlines,(SELECT SUM(r.flavor)/ COUNT(r.id_member) FROM food_rating r WHERE r.id_food=f.id_food) AS t_flavor,(SELECT SUM(r.freshness)/ COUNT(r.id_member) FROM food_rating r WHERE r.id_food=f.id_food) AS t_freshness,(SELECT SUM(r.cooking)/ COUNT(r.id_member) FROM food_rating r WHERE r.id_food=f.id_food) AS t_cooking,(SELECT SUM(r.aroma)/ COUNT(r.id_member) FROM food_rating r WHERE r.id_food=f.id_food) AS t_aroma,(SELECT SUM(r.serving)/ COUNT(r.id_member) FROM food_rating r WHERE r.id_food=f.id_food) AS t_serving,(SELECT (SUM(r.cleanlines) + SUM(r.flavor) + SUM(r.freshness) + SUM(r.cooking) + SUM(r.aroma) + SUM(r.serving)) / COUNT(r.id_member) FROM food_rating r WHERE r.id_food =f.id_food) AS total_rate FROM food f LEFT JOIN food_category k ON f.id_food_category=k.id_food_category LEFT JOIN price_index p ON f.id_price_index=p.id_price_index  LEFT JOIN msg_level l ON f.id_msg_level=l.id_msg_level LEFT JOIN restaurant r ON f.id_restaurant=r.id_restaurant LEFT JOIN type_of_business a ON r.id_type_of_business = a.id_type_of_business LEFT JOIN negara b ON r.id_negara = b.id_negara LEFT JOIN propinsi d ON r.id_propinsi = d.id_propinsi LEFT JOIN kota e ON r.id_kota = e.id_kota LEFT JOIN landmark g ON r.id_landmark = g.id_landmark LEFT JOIN member n ON f.id_member = n.id_member where f.id_food='$id'");
$r=mysqli_fetch_array($resto);
$slug=seo($r['nama_food']);
$post=date("jS M, Y", strtotime($r['tgl_post']));
$total_cleanlines=number_format((float)$r['t_cleanlines'], 2, '.', '');
$img_cleanlines=$total_cleanlines / 0.21;
$total_flavor=number_format((float)$r['t_flavor'], 2, '.', '');
$img_flavor=$total_flavor / 0.20;
$total_freshness=number_format((float)$r['t_freshness'], 2, '.', '');
$img_freshness=$total_freshness / 0.18;
$total_cooking=number_format((float)$r['t_cooking'], 2, '.', '');
$img_cooking=$total_cooking / 0.17;
$total_aroma=number_format((float)$r['t_aroma'], 2, '.', '');
$img_aroma=$total_aroma / 0.15;
$total_serving=number_format((float)$r['t_serving'], 2, '.', '');
$img_serving=$total_serving / 0.09;
$total_rating=number_format((float)$r['total_rate'], 2, '.', '');
$gsql=mysqli_query($koneksi,"select p.gambar_food_photo,p.nama_food_photo,(select count(l.id_food_photo_like) from food_photo_like l where l.id_food_photo=p.id_food_photo) as total_like from food_photo p where p.id_food='$id' order by total_like desc,p.id_food_photo limit 1");
$g=mysqli_fetch_array($gsql);
$gambar=$g['gambar_food_photo'];
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
  <title><?php echo"$r[nama_food]"; ?> - Foodie Guidances</title>
	<meta name="keywords" content="">
  <meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<meta property="og:title" content="<?php echo"$r[nama_food]"; ?>" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="<?php echo"$base_url/pages/food/info/$_GET[id]/$slug"; ?>" />
	<meta property="og:image" content="<?php echo"$base_url/assets/img/food/medium_$gambar"; ?>" />
	<meta property="og:description" content="<?php echo"$r[nama_food]"; ?>" />
	<meta property="og:site_name" content="Foodie Guidances" />
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/slick.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/lightbox.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>

	<link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
 <body style="position: initial;" class="drawer drawer--right">
	<?php include"config/inc/header.php"; ?>
    <!-- Fixed navbar -->
    <div class="container hook">
		<div class="row">
			<div class="col-12 col-8a">
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="<?php echo"$base_url/pages/food/search/"; ?>">Food</a></li>
					<li class="active"><?php echo"$r[nama_food]"; ?></li>
				</ol>
				<?php
					if(isset($_SESSION['resto_notif'])){
						if($_SESSION['resto_notif']=="suka"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully like this food.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="suka_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was liked this food before. You can only do one time in 24 hours.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="rekomendasi"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully recommend this food.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="rekomendasi_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was recommeded this food before. You can only do one time in 24 hours.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="bookmark"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully bookmark this food.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="bookmark_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was bookmark this food before. You can only do one time.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="disini"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully added the data to taste it at this food.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="disini_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was added the data at this food before. You can only do one time.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="lapor"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully report this food.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="lapor_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was reported this food before. You can only do one time.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="rating"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully rating this food.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="rating_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was rated this food before. You can only do one time in 24 hours.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="rating_pilih"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> Please select star rating value before vote.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="login_dulu"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Must Login!</strong> You must login or register before doing this action.
							</div>";
						}
						unset($_SESSION['resto_notif']);
					}
				?>
				<h1 class="judul">
					<?php
					echo"$r[nama_food] <br>";
					if($total_rating<>0){
						echo"";?>
						<a href = "javascript:void(0)" style="text-decoration: none;" class='klik-bintang'>
							<?php echo"<input type='hidden' class='rating' data-filled='fa fa-star' data-empty='fa fa-star-o' readonly='readonly' value='$total_rating'>";?>
						</a>
						<?php echo"
							<span style='font-size: 14px;'>Rating : $total_rating </span><em class='f-12'>from $r[jumlah_vote] votes</em>";
					}
					?>
				</h1>
				<div class="mb10 f-12"><em>Category</em> <?php echo"$r[nama_food_category]"; ?> <span class="bullet">&#8226;</span>
					<a href="<?php echo"$base_url/$r[username]"; ?>"><em>by</em> <?php if(!empty($r['nama_depan'])){echo"$r[nama_depan] $r[nama_belakang]";}else{echo"Admin";} ?></a>
					<span class="bullet">&#8226;</span> <em>post</em> <?php echo"$post"; ?>
				</div>
				<div class="box-info-1">

					<?php
						include 'config/func/kiri_food.php';
					?>
					<?php
						include 'config/func/kanan_food.php';
					?>

					<div class="clearfix"></div>
				</div>
				<div role="tabpanel">
					<ul class="nav nav-tabs" role="tablist">
						<li><a href="<?php echo"$base_url/pages/food/info/$_GET[id]/$slug"; ?>">Info</a></li>
						<li><a href="<?php echo"$base_url/pages/food/photo/$_GET[id]/$slug"; ?>">Other Photo</a></li>
						<li><a href="<?php echo"$base_url/pages/food/voter/$_GET[id]/$slug"; ?>">Voter</a></li>
						<li class="active"><a href="#">Restaurant Map</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-panel">
							<div id="mapa" style="height:500px"></div>
							<div class="no-geolocation">
								<span class="text-danger">Your browser does not support Geolocation.</span>
							</div>
							<a href="#" class="btn btn-danger btn-block get-directions" target="_blank">Get Direction to My location Now</a>
						</div>
					</div>
				</div>
				<div role="tabpanel" id="tab-vote">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#result" aria-controls="result" role="tab" data-toggle="tab">Result</a></li>
						<li role="presentation"><a href="#feature" aria-controls="feature" role="tab" data-toggle="tab">Feature Vote</a></li>
						<li role="presentation"><a href="#quick" aria-controls="quick" role="tab" data-toggle="tab">Quick Vote</a></li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane tab-panel active fade in" id="result">
							<table class="tablebintang">
								<tr>
									<td>Cleanliness <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall cleanliness of the food and plates served, which can include whether the food has unwanted insect, plastic, hair or unclean plates, bowls and utensils used."></i></td>
									<td><input type="hidden" class="rating" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" readonly="readonly" value="<?php echo"$img_cleanlines"; ?>"></td>
									<td rowspan="5" class="mene-atas">
										<span><?php echo"$total_rating"; ?></span>
										<em>from <?php echo"$r[jumlah_vote]"; ?> vote</em>
									</td>
								</tr>
								<tr>
									<td>Flavour <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall taste of the food which has unique personality."></i></td>
									<td><input type="hidden" class="rating" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" readonly="readonly" value="<?php echo"$img_flavor"; ?>"></td>
								</tr>
								<tr>
									<td>Freshness <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment on the food ingredient's freshness which can include meat, seafood, vegetables and sauces."></i></td>
									<td><input type="hidden" class="rating" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" readonly="readonly" value="<?php echo"$img_freshness"; ?>"></td>
								</tr>
								<tr>
									<td>Cooking <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment of the way the food is being cooked wheither it is undercooked or overcooked. However, it also depend on how we like the food to be cook."></i></td>
									<td><input type="hidden" class="rating" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" readonly="readonly" value="<?php echo"$img_cooking"; ?>"></td>
								</tr>
								<tr>
									<td>Presentasion &amp; Aroma <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment on the food's presentation and its aroma. A good presentation on food will increase our appetite for the food. likewise for the aroma too. A fragrance food cook meters away can attract our sense of smell. Good food makes better with great presentation and aroma."></i></td>
									<td><input type="hidden" class="rating" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" readonly="readonly" value="<?php echo"$img_aroma"; ?>"></td>
								</tr>
								<tr>
									<td>Serving <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Is the food being served having adequate serving? Too much or too little? Unlike Western cuisine which have courses of food for a complete meal, Eastern cuisine are more direct and usually treat as the main course. Therefore, the serving is vary."></i></td>
									<td><input type="hidden" class="rating" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" readonly="readonly" value="<?php echo"$img_serving"; ?>"></td>
								</tr>
								<tr class="total-vote">
									<td colspan="2">
										<span><?php echo"$total_rating"; ?></span>
										<em>from <?php echo"$r[jumlah_vote]"; ?> vote</em>
									</td>
								</tr>
							</table>
						</div>
						<div role="tabpanel" class="tab-pane tab-panel fade" id="feature">
							<form method="get" action="<?php echo"$base_url/food_aksi.php"; ?>">
								<input type="hidden" value="6" name="fungsi">
								<input type="hidden" value="info" name="page">
								<input type="hidden" value="<?php echo"$_GET[id]"; ?>" name="id">
								<table class="tablebintang">
									<tr>
										<td>Cleanliness <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall cleanliness of the food and plates served, which can include whether the food has unwanted insect, plastic, hair or unclean plates, bowls and utensils used."></i></td>
										<td><input type="hidden" class="rating" id="rat1" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="cleanlines"></td>
										<td rowspan="5" class="mene-atas"><button class="btn btn-danger" type="submit">Vote</button>
											<div class='alert-danger alert-dismissible as' role='alert'>
						                  <strong>Please submit minimum one star for each rating categories in other to be rated</strong>
						               </div>
										</td>
									</tr>
									<tr>
										<td>Flavour <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall taste of the food which has unique personality."></i></td>
										<td><input type="hidden" class="rating" id="rat2" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="flavor"></td>
									</tr>
									<tr>
										<td>Freshness <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment on the food ingredient's freshness which can include meat, seafood, vegetables and sauces."></i></td>
										<td><input type="hidden" class="rating" id="rat3" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="freshness"></td>
									</tr>
									<tr>
										<td>Cooking <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment of the way the food is being cooked wheither it is undercooked or overcooked. However, it also depend on how we like the food to be cook."></i></td>
										<td><input type="hidden" class="rating" id="rat4" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="cooking"></td>
									</tr>
									<tr>
										<td>Presentation &amp; Aroma <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment on the food's presentation and its aroma. A good presentation on food will increase our appetite for the food. likewise for the aroma too. A fragrance food cook meters away can attract our sense of smell. Good food makes better with great presentation and aroma."></i></td>
										<td><input type="hidden" class="rating" id="rat5" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="aroma"></td>
									</tr>
									<tr>
										<td>Serving <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Is the food being served having adequate serving? Too much or too little? Unlike Western cuisine which have courses of food for a complete meal, Eastern cuisine are more direct and usually treat as the main course. Therefore, the serving is vary."></i></td>
										<td><input type="hidden" class="rating" id="rat6" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="serving"></td>
									</tr>
									<tr class="total-vote">
										<td colspan="2">
											<button class="btn btn-danger" type="submit">Vote</button>
											<div class='alert-danger alert-dismissible as' role='alert'>
						                  <strong>Please submit minimum one star for each rating categories in other to be rated</strong>
						               </div>
										</td>
									</tr>
								</table>
							</form>
						</div>
						<div role="tabpanel" class="tab-pane tab-panel fade" id="quick">
							<form method="get" action="<?php echo"$base_url/food_aksi.php"; ?>">
								<input type="hidden" value="6" name="fungsi">
								<input type="hidden" value="info" name="page">
								<input type="hidden" value="<?php echo"$_GET[id]"; ?>" name="id">
								<table class="tablebintang">
									<tr>
										<td><strong>Give your vote</strong></td>
										<td><input type="hidden" class="rating" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="rating"></td>
										<td rowspan="2" class="mene-atas"><button class="btn btn-danger" type="submit">Vote</button>
											<div class='alert-danger alert-dismissible as' role='alert'>
						            <strong>Please submit minimum one star for each rating categories in other to be rated</strong>
						          </div>
										</td>
									</tr>
									<tr class="total-vote">
										<td colspan="2">
											<button class="btn btn-danger" type="submit">Vote</button>
										</td>
									</tr>
								</table>
							</form>
						</div>
					</div>
					<ul class="list-inline">
						<li><i class="fa fa-star"></i> Poor</li>
						<li><i class="fa fa-star"></i><i class="fa fa-star"></i> Below Average</li>
						<li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Average</li>
						<li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Very Good</li>
						<li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Excellent</li>
					</ul>
				</div>
				<?php
				$here=mysqli_query($koneksi,"select * from food_here left join member on food_here.id_member=member.id_member where id_food='$id'");
				$jumlah_here=mysqli_num_rows($here);
				?>
				<h5 class="f-merah"><?php echo"$jumlah_here"; ?> Members Have Tasted it</h5>
				<?php
				if($jumlah_here<>0){
					echo"<div class='member-carousel'>";
					while($h=mysqli_fetch_array($here)){
						echo"<div><img data-original='$base_url/assets/img/member/$h[gambar_thumb]' class='lazy' width='40' height='40'></div>";
					}
					echo"</div>";
				}
				else{
					echo"<p>There is no member was tasted it.</p>";
				}
				?>
				<h5 class="f-merah">Similar Food</h5>
				<div class="media" style="overflow: unset">
					<div class="row">
						<div id="owl-demo" class="owl-carousel">
							<?php
							$resto=mysqli_query($koneksi,"select r.id_food, r.nama_food, r.tgl_posta,(select count(h.id_food) from food_here h where h.id_food=r.id_food) as jumlah,(select p.gambar_food_photo from food_photo p where p.id_food=r.id_food order by p.id_food_photo desc limit 1) as gambar,(SELECT COUNT(h.id_member) FROM food_like h WHERE h.id_food=r.id_food) AS dilike from food r where r.id_food<>'$r[id_food]' and r.id_food_category='$r[id_food_category]' order by r.tgl_posta DESC");
							$ada_resto=mysqli_num_rows($resto);
							if($ada_resto<>0){
								while($s=mysqli_fetch_array($resto)){
									$slug=seo($s['nama_food']);
									$post=date("jS M, Y", strtotime($s['tgl_posta']));
									$id = id_masking($s['id_food']);
									echo"<div class=' item'>
										<div class='thumb'>
											<a href='$base_url/pages/food/info/$id/$slug'><img data-original='$base_url/assets/img/food/small_$s[gambar]' class='lazy' width='160' height='120'></a>
										</div>
										<div class='info'>
											<em>posted on $post</em>
											<h3 class='sembunyi'><a href='$base_url/pages/food/info/$id/$slug'>$s[nama_food]</a></h3>
											<ul class='list-unstyled f-12'>
												<li>$s[dilike] Like <span class='bullet'>&#8226;</span> $s[jumlah] Taste it</li>
											</ul>
										</div>
									</div>";
								}
							}
							else{
								echo"<div class='col-12'>There is no food with same category right now.</div>";
							}
							?>
						</div>
					</div>
				</div>
				<hr>
				<div id="disqus_thread"></div>
			</div>
			<div class="col-4 mene-atas">
				<?php include"config/inc/search.php"; ?>
				<?php include"config/inc/iklan.php"; ?>
				<div class="fb-like-box" data-href="https://www.facebook.com/foodieguidances#_=_" data-width="220" data-height="350" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
			</div>
		</div>
    </div>
    <?php include"config/inc/footer.php"; ?>
	 <?php
 	$idu = id_masking($_GET['id']);
 		$pb=mysqli_query($koneksi, "select * from food where id_food < $idu ORDER BY id_food desc limit 1");
 	$jumpb=mysqli_num_rows($pb);
 	$tpb=mysqli_fetch_array($pb);
 	if ($jumpb<>0) {
 		$ba = id_masking($tpb['id_food']);
 		$slug=seo($tpb['nama_food']);
 		$next="$base_url/pages/food/info/$ba/$slug";
 	} else {
 		$pba=mysqli_query($koneksi, "select * from food ORDER BY id_food desc limit 1");
 		$jumpba=mysqli_num_rows($pba);
 		$tpba=mysqli_fetch_array($pba);
 		$baa = id_masking($tpba['id_food']);
 		$sluga=seo($tpba['nama_food']);
 		$next="$base_url/pages/food/info/$baa/$sluga";
 	}

 	$pn=mysqli_query($koneksi, "select * from food where id_food > $idu ORDER BY id_food limit 1");
 	$jumpn=mysqli_num_rows($pn);
 	$tpn=mysqli_fetch_array($pn);
 	if ($jumpn<>0) {
 		$na = id_masking($tpn['id_food']);
 		$slug=seo($tpn['nama_food']);
 		$back="$base_url/pages/food/info/$na/$slug";
 	} else {
 		$pna=mysqli_query($koneksi, "select * from food ORDER BY id_food limit 1");
 		$jumpna=mysqli_num_rows($pna);
 		$tpna=mysqli_fetch_array($pna);
 		$naa = id_masking($tpna['id_food']);
 		$sluga=seo($tpna['nama_food']);
 		$back="$base_url/pages/food/info/$naa/$sluga";
 	}
 ?>
 <a class="scrollToNext" href="<?php echo $back ?>" style="bottom: 320px;"><img src="<?php echo"$base_url/assets/img/theme/kiri.png" ?>"></a>
 <a class="scrollToNext" href="<?php echo $next ?>" style="bottom: 273px;"><img src="<?php echo"$base_url/assets/img/theme/kanan.png" ?>"></a>
 <a href="#" class="scrollToTop"><img src="<?php echo"$base_url/assets/img/theme/atas.png" ?>"></a>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap-rating.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/lightbox.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/slick.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.lazyload.min.js"></script>
	<script  src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.ui.map.full.min.js"></script>

	<script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
   <script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
   <script>
      $(document).ready(function() {
	      $('.drawer').drawer();

			$('.tutup').click(function(){
				$('.iklanan').toggleClass('hilang', 500);
			});

			$('.cart').click(function(e){
				e.stopPropagation();
				$('.dis-cart').toggle();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$('.plus').click(function(e){
				e.stopPropagation();
				$('.dis-plus').toggle();
				$('.dis-cart').hide();
				$('.dis-share').hide();
			});
			$('.share').click(function(e){
				e.stopPropagation();
				$('.dis-share').toggle();
				$('.dis-plus').hide();
				$('.dis-cart').hide();
			});
			$('.drawer-toggle').click(function(){
				$('.dis-cart').hide();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$(document).click(function () {
				 var $el = $(".dis-share");
				 if ($el.is(":visible")) {
					  $el.fadeOut(200);
				 }
				 var $ela = $(".dis-plus");
				 if ($ela.is(":visible")) {
					  $ela.fadeOut(200);
				 }
				 var $elu = $(".dis-cart");
				 if ($elu.is(":visible")) {
					  $elu.fadeOut(200);
				 }
		   });
      });
   </script>

	<?php if(!empty($_SESSION['food_member'])){ ?>
		<script src="<?php echo"$base_url"; ?>/assets/js/theme.js"></script>
	<?php } ?>
	<script type="text/javascript">
		$(document).ready(function () {
			$(document).on("contextmenu",function(e){
				if(e.target.nodeName != "INPUT" && e.target.nodeName != "TEXTAREA")
				e.preventDefault();
			});
			$.fn.disableTextSelect = function() {
				return this.each(function() {
					$(this).css({
						'MozUserSelect':'none',
						'webkitUserSelect':'none'
					}).attr('unselectable','on').bind('selectstart', function() {
						return false;
					});
				});
			};
			$('body').disableTextSelect();
		});
	</script>

	<script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>
	<script type="text/javascript">stLight.options({publisher: "7b97d330-d7b9-49c3-b5ee-b3aaa89bbe66", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
	<div id="fb-root"></div>

	<script type="text/javascript">
	$(document).ready(function() {
		$(window).scroll(function(){
	 		  if ($(this).scrollTop() > 100) {
	 			  $('.scrollToTop').fadeIn();
	 		  } else {
	 			  $('.scrollToTop').fadeOut();
	 		  }
	 	  });

	 	  $(window).scroll(function(){
	 		 if ($(this).scrollTop() > 50) {
	 			 $('.scrollToNext').fadeIn();
	 		 } else {
	 			 $('.scrollToNext').fadeOut();
	 		 }
	 	  });

			$('#go-to-vote').click(function() {
				$('html, body').animate({
					scrollTop: $( $(this).attr('href') ).offset().top
				}, 500);
				$('#tab-vote a[href="#feature"]').tab('show');
				return false;
			});
	});
	</script>

	<script src="<?php echo"$base_url"; ?>/assets/js/owl.carousel.js"></script>
	<link href="<?php echo"$base_url"; ?>/assets/css/owl.carousel.css" rel="stylesheet">
   <link href="<?php echo"$base_url"; ?>/assets/css/owl.theme.css" rel="stylesheet">
	<style>
	#owl-demo .item{
		 margin: 3px;
	}
	#owl-demo .item img{
		 display: block;
		 width: 100%;
		 height: auto;
	}
	</style>
	<script>
		$(document).ready(function() {
		  $("#owl-demo").owlCarousel({
			 items : 4,
			 lazyLoad : true,
			 navigation : true
		  });
		});
	</script>

	<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=348272391978609&version=v2.0";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
	<script type="text/javascript">
		$(document).ready(function () {
			$(".as").hide();
	      var rat1 = $("#rat1").val();
	      var rat2 = $("#rat2").val();
	      var rat3 = $("#rat3").val();
	      var rat4 = $("#rat4").val();
	      var rat5 = $("#rat5").val();
	      var rat6 = $("#rat6").val();
	      $("#rat1,#rat2,#rat3,#rat4,#rat5,#rat6").change(function(){
	         if ((rat1 < 1) && (rat2 < 1) && (rat3 < 1) && (rat4 < 1) && (rat5 < 1) && (rat6 < 1) ) {
	            $(".btn-danger").hide();
	            $(".as").show();
	         }
	      });

	      $("#rat1,#rat2,#rat3,#rat4,#rat5,#rat6").change(function(){
	         var rat1 = $("#rat1").val();
	         var rat2 = $("#rat2").val();
	         var rat3 = $("#rat3").val();
	         var rat4 = $("#rat4").val();
	         var rat5 = $("#rat5").val();
	         var rat5 = $("#rat5").val();
	         var rat6 = $("#rat6").val();
	         if ((rat1 > 0) && (rat2 > 0) && (rat3 > 0) && (rat4 > 0) && (rat5 > 0) && (rat6 > 0)) {
	            $(".btn-danger").show();
	            $(".as").hide();
	         }
	      });

			$("img.lazy").lazyload({
				effect : "fadeIn"
			});
			$(".alert").fadeTo(5000, 500).fadeOut(500, function(){
				$(".alert").alert('close');
			});
			$('.tip').tooltip();
			$('.rating').rating();
			$('.member-carousel').slick({
				infinite: true,
				slidesToShow: 14,
				slidesToScroll: 14,
				arrows:false,
				autoplay:true,
				autoplaySpeed: 10000
			});
			$('#go-to-vote').click(function() {
				$('html, body').animate({
					scrollTop: $( $(this).attr('href') ).offset().top
				}, 500);
				$('#tab-vote a[href="#feature"]').tab('show');
				return false;
			});
			var startingLocation;

			var destination = "<?php echo"$r[latitude],$r[longitude]"; ?>"; // replace this with any destination

			$('a.get-directions').click(function (e) {
				e.preventDefault();

				// check if browser supports geolocation
				if (navigator.geolocation) {

					// get user's current position
					navigator.geolocation.getCurrentPosition(function (position) {

						// get latitude and longitude
						var latitude = position.coords.latitude;
						var longitude = position.coords.longitude;
						startingLocation = latitude + "," + longitude;

						// send starting location and destination to goToGoogleMaps function
						goToGoogleMaps(startingLocation, destination);

					});
				}

				// fallback for browsers without geolocation
				else {

					// get manually entered postcode
					startingLocation = $('.manual-location').val();

					// if user has entered a starting location, send starting location and destination to goToGoogleMaps function
					if (startingLocation != '') {
						goToGoogleMaps(startingLocation, destination);
					}
					// else fade in the manual postcode field
					else {
						$('.no-geolocation').fadeIn();
					}

				}

				// go to Google Maps function - takes a starting location and destination and sends the query to Google Maps
				function goToGoogleMaps(startingLocation, destination) {
					window.location = "https://www.google.com/maps/dir/" + startingLocation + "/" + destination;
				}

			});


			$('.social-share').click( function(event){
				event.stopPropagation();
				$( ".social-box" ).toggle(1);
			});
			$('.carousel').carousel({
				interval: 10000
			});
			$(document).click( function(){
				$('.social-box').hide();
			});
		});
		/* * * CONFIGURATION VARIABLES * * */
		var disqus_shortname = 'foodieguidancescom';

		/* * * DON'T EDIT BELOW THIS LINE * * */
		(function() {
			var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
			dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
			(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
		})();
	</script>
	<script type="text/javascript">
	  $(document).ready(function () {
	    $('.klik-bintang').click(function(){
	      $('#lightp').show();
	      $('#fadep').show();

	      $('.fitin div').css('font-size', '30px');
	      while( $('.fitin div').height() > $('.fitin').height() ) {
	          $('.fitin div').css('font-size', (parseInt($('.fitin div').css('font-size')) - 1) + "px" );
	      };

	      $('.restos div').css('font-size', '16px');
	      while( $('.restos div').height() > $('.restos').height() ) {
	          $('.restos div').css('font-size', (parseInt($('.restos div').css('font-size')) - 1) + "px" );
	      };
	    });
	  });
	</script>
	<style media="screen">
	.black_overlay{
		display: none;
		position: absolute;
		top: 0%;
		left: 0%;
		width: 100%;
		height: 100%;
		background-color: black;
		z-index:1001;
		-moz-opacity: 0.8;
		opacity:.80;
		filter: alpha(opacity=80);
	}
	.white_content {
		display: none;
		position: fixed;
		top: 20%;
		left: 40%;
		width: 20%;
		height: 56%;
		padding: 24px;
		border: 5px solid #C00606;
		background-color: #FFF;
		z-index: 1002;
		overflow-y: auto;
		overflow-x: hidden;
	}
	</style>
	<div id="light" class="white_content">
		<h3 style="font-size: 20px; margin: 0px 0px 15px;">Like</h3>
		<table class="table">
			<?php
				$idu=id_masking($_GET['id']);
				$me=mysqli_query($koneksi,"SELECT * FROM food_like r left join member m on m.id_member=r.id_member where r.id_food='$idu'");
				$no=1;
				$juma=mysqli_num_rows($me);
				if (!empty($juma)) {
					while ($tame=mysqli_fetch_array($me)) {
						$like_date=date("jS M, Y", strtotime($tame['tgl_like']));
							echo "<tr><td>$no.</td>
										<td><img src='$base_url/assets/img/member/$tame[gambar_thumb]' class='foto_profil' width='30px'></td> <td>$tame[username]</td> <td>$like_date</td></tr>";
						$no++;
					}
				}
				else {
					echo "Empty";
				}
			?>
		</table>
		<a style="position: absolute; border: 6px solid rgb(255, 255, 255); right: -6px; top: -6px; z-index: 999999999; background: #C00606; color: rgb(255, 255, 255); padding: 6px 10px;" href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'"><i class="fa fa-close"></i></a>
	</div>
	<div id="lighta" class="white_content">
		<h3 style="font-size: 20px; margin: 0px 0px 15px;">Recommendation</h3>
		<table class="table">
			<?php
				$idu=id_masking($_GET['id']);
				$me=mysqli_query($koneksi,"SELECT * FROM food_rekomendasi r left join member m on m.id_member=r.id_member where r.id_food='$idu'");
				$no=1;
				$juma=mysqli_num_rows($me);
				if (!empty($juma)) {
					while ($tame=mysqli_fetch_array($me)) {
						$like_date=date("jS M, Y", strtotime($tame['tgl_rekomendasi']));
							echo "<tr><td>$no.</td>
										<td><img src='$base_url/assets/img/member/$tame[gambar_thumb]' class='foto_profil' width='30px'></td> <td>$tame[username]</td> <td>$like_date</td></tr>";
						$no++;
					}
				}
				else {
					echo "Empty";
				}
			?>
		</table>
		<a style="position: absolute; border: 6px solid rgb(255, 255, 255); right: -6px; top: -6px; z-index: 999999999; background: #C00606; color: rgb(255, 255, 255); padding: 6px 10px;" href = "javascript:void(0)" onclick = "document.getElementById('lighta').style.display='none';document.getElementById('fadea').style.display='none'"><i class="fa fa-close"></i></a>
	</div>

	<div id="lightp" class="white_content" style="width: 450px; height: 675px;  overflow: hidden;">
	  <h3 style="font-size: 20px; margin: 0px 0px 15px;">Certificate</h3>
	  <div class="piagam-isi-food">

	    <div class="fitin">
	      <div class="h1a"><?php echo "$r[nama_food]"; ?></div>
	    </div>
	    <div class="of">Of</div>
	    <div class="restos">
	      <div class="h1"><?php echo "$r[restaurant_name]"; ?></div>
	    </div>
	    <div class="restos">
	      <div class="alamat"><?php echo "$r[street_address]"; ?></div>
	    </div>

	    <div class="negara"><?php echo $r['nama_negara']; ?></div>
	    <div class="bintang"><?php echo "<input type='hidden' class='rating' data-filled='fa fa-star' data-empty='fa fa-star-o' readonly='readonly' value='$total_rating'>"; ?></div>
	    <div class="rating"><?php echo "$total_rating / <em class='f-12'> $r[jumlah_vote] votes</em>"; ?></div>
	    <span class="world"># <span style="  color: #ed1c24;"><?php echo "$rank_global"; ?></span></span>
	    <span class="ranknegara"># <span style="  color: #ed1c24;"><?php echo "$rank_negara"; ?></span></span>
	    <span class="rankstate"># <span style="  color: #ed1c24;"><?php echo "$rank_state"; ?></span></span>
	    <span class="rankcity"># <span style="  color: #ed1c24;"><?php echo "$rank_city"; ?></span></span>
	    <span class="rankmall"># <span style="  color: #ed1c24;"><?php echo "$rank_mall"; ?></span></span>
	  </div>
	  <img src="<?php echo"$base_url";?>/assets/img/theme/certificate-food.jpg" alt="" width="100%">

	  <a style="position: absolute; border: 6px solid rgb(255, 255, 255); right: -6px; top: -6px; z-index: 999999999; background: #C00606; color: rgb(255, 255, 255); padding: 6px 10px;" href = "javascript:void(0)" onclick = "document.getElementById('lightp').style.display='none';document.getElementById('fadep').style.display='none'"><i class="fa fa-close"></i></a>
	</div>
	<div id="fadep" class="black_overlay"  onclick = "document.getElementById('lightp').style.display='none';document.getElementById('fadep').style.display='none'"></div>

	<div id="fade" class="black_overlay"  onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'"></div>
	<div id="fadea" class="black_overlay"  onclick = "document.getElementById('lighta').style.display='none';document.getElementById('fadea').style.display='none'"></div>

	<div id="map-pop" class="white_content-map">
		<h3 style="font-size: 20px; margin: 0px 0px 15px;">Map</h3>
		<table class="table" style="margin-bottom: 0">
			<div id="map" style="height:90%"></div><br>
	    <a href="#" class="btn btn-danger btn-block get-directions" target="_blank" target="_blank">Get Direction to My location Now</a>

			<?php
				$idu=id_masking($_GET['id']);
				$me=mysqli_query($koneksi,"SELECT * FROM food b left join restaurant r on b.id_restaurant=r.id_restaurant where b.id_food='$idu'");
				$no=1;
				$juma=mysqli_num_rows($me);
				if (!empty($juma)) {
					$r=mysqli_fetch_array($me); ?>
					<script>
						function initMap() {
							var lati = <?php echo $r[latitude]; ?>;
							var lngi = <?php echo $r[longitude]; ?>;

							var myLatLng = {lat: lati, lng: lngi};
							var myLatLnga = {lat: lati, lng: lngi};

							var map = new google.maps.Map(document.getElementById('map'), {
								zoom: 18,
								center: myLatLng,
							});

							var mapa = new google.maps.Map(document.getElementById('mapa'), {
								zoom: 18,
								center: myLatLng,
							});

							var image = 'https://www.foodieguidances.com/assets/images/foodie-point.png';
							var marker = new google.maps.Marker({
								position: myLatLng,
								map: map,
								icon:  image
							});

							var images = 'https://www.foodieguidances.com/assets/images/foodie-point.png';
							var marker2 = new google.maps.Marker({
								position: myLatLnga,
								map: mapa,
								icon:  images
							});


							google.maps.event.addListenerOnce(map, 'idle', function() {
									var center = map.getCenter()
									google.maps.event.trigger(map, "resize")
									map.setCenter(center)
							});

							google.maps.event.addListener(mapa, 'click', function() {
									var center = map.getCenter()
									google.maps.event.trigger(mapa, "resize")
									map.setCenter(center)
							});


						}
						window.onload = function() {
							initMap();
						};

				</script>
				<script async defer
					src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCwYMs0LtJBgwkV937PkNKKR3dbEiD0zA">
				</script>
				<?php
				}
				else {
					echo "Empty";
				}
			?>
		</table>
		<a style="position: absolute; border: 6px solid rgb(255, 255, 255); right: -6px; top: -6px; z-index: 999999999; background: #C00606; color: rgb(255, 255, 255); padding: 6px 10px;" href = "javascript:void(0)" onclick = "document.getElementById('map-pop').style.display='none';document.getElementById('fade-map').style.display='none'"><i class="fa fa-close"></i></a>
	</div>
	<div id="fade-map" class="black_overlay"  onclick = "document.getElementById('map-pop').style.display='none';document.getElementById('fade-map').style.display='none'"></div>

</body>
</html>
