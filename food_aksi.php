<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";
include "config/func/id_masking.php";
date_default_timezone_set("Asia/Jakarta");
$sekarang=date("Y-m-d");
$id = id_masking($_GET['id']);
$r=mysqli_fetch_array(mysqli_query($koneksi,"select id_food, nama_food from food where id_food='$id'"));
$seo=seo($r['nama_food']);
if(isset($_SESSION['food_member'])){
	if($_GET['fungsi']=="1"){
		$ada=mysqli_num_rows(mysqli_query($koneksi,"select id_food_like from food_like where id_food='$id' and id_member='$_SESSION[food_member]' and tgl_like='$sekarang'"));
		if($ada==0){
			mysqli_query($koneksi,"insert into food_like (id_member,id_food,tgl_like) values('$_SESSION[food_member]','$id','$sekarang')");
			$_SESSION['resto_notif']     = "suka";
		}
		else{
			$_SESSION['resto_notif']     = "suka_gagal";
		}
	}
	elseif($_GET['fungsi']=="2"){
		$ada=mysqli_num_rows(mysqli_query($koneksi,"select id_food_rekomendasi from food_rekomendasi where id_food='$id' and id_member='$_SESSION[food_member]' and tgl_rekomendasi='$sekarang'"));
		if($ada==0){
			mysqli_query($koneksi,"insert into food_rekomendasi (id_member,id_food,tgl_rekomendasi) values('$_SESSION[food_member]','$id','$sekarang')");
			$_SESSION['resto_notif']     = "rekomendasi";
		}
		else{
			$_SESSION['resto_notif']     = "rekomendasi_gagal";
		}
	}
	elseif($_GET['fungsi']=="3"){
		$ada_bookmark=mysqli_num_rows(mysqli_query($koneksi,"select id_bookmark from bookmark where id='$id' and jenis='food' and id_member='$_SESSION[food_member]'"));
		if($ada_bookmark==0){
			mysqli_query($koneksi,"insert into bookmark (id_member,jenis,id,tgl_bookmark) values('$_SESSION[food_member]','food','$id','$sekarang')");
			$_SESSION['resto_notif']     = "bookmark";
		}
		else{
			$_SESSION['resto_notif']     = "bookmark_gagal";
		}
	}
	elseif($_GET['fungsi']=="4"){
		$ada_here=mysqli_num_rows(mysqli_query($koneksi,"select id_food_here from food_here where id_food='$id' and id_member='$_SESSION[food_member]'"));
		if($ada_here==0){
			mysqli_query($koneksi,"insert into food_here (id_member,id_food,tgl_here) values('$_SESSION[food_member]','$id','$sekarang')");
			$_SESSION['resto_notif']     = "disini";
		}
		else{
			$_SESSION['resto_notif']     = "disini_gagal";
		}
	}
	elseif($_GET['fungsi']=="5"){
		$ada_report=mysqli_num_rows(mysqli_query($koneksi,"select id_food_report from food_report where id_food='$id' and id_member='$_SESSION[food_member]'"));
		if($ada_report==0){
			mysqli_query($koneksi,"insert into food_report (id_member,id_food,tgl_report) values('$_SESSION[food_member]','$id','$sekarang')");
			$_SESSION['resto_notif']     = "lapor";
		}
		else{
			$_SESSION['resto_notif']     = "lapor_gagal";
		}
	}
	elseif($_GET['fungsi']=="6"){
		$ada_report=mysqli_num_rows(mysqli_query($koneksi,"select id_food_rating from food_rating where id_food='$id' and id_member='$_SESSION[food_member]' and tgl_food_rating='$sekarang'"));
		if($ada_report==0){
			if(!empty($_GET['rating']) ){
				$r1=$_GET['rating'] * 0.21;
				$r2=$_GET['rating'] * 0.20;
				$r3=$_GET['rating'] * 0.18;
				$r4=$_GET['rating'] * 0.17;
				$r5=$_GET['rating'] * 0.15;
				$r6=$_GET['rating'] * 0.09;
				mysqli_query($koneksi,"INSERT INTO food_rating (id_food,id_member,cleanlines,flavor,freshness,cooking,aroma,serving,tgl_food_rating) values('$id','$_SESSION[food_member]','$r1','$r2','$r3','$r4','$r5','$r6','$sekarang')");
				mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('food','$id')");
				$_SESSION['resto_notif']     = "rating";

				$global=mysqli_query($koneksi,"SELECT id_food, id_negara, id_propinsi, id_kota, id_mall FROM food f LEFT JOIN restaurant r on f.id_restaurant = r.id_restaurant");
				while ($b=mysqli_fetch_array($global)) {
					$rank=mysqli_fetch_array(mysqli_query($koneksi, "SELECT
						((SUM(f.cleanlines) + SUM(f.flavor) + SUM(f.freshness) + SUM(f.cooking) + SUM(f.aroma) + SUM(f.serving)) / COUNT(f.id_member)) as rank,
						(select count(id_food_rating) as jumlah from food_rating where food_rating.id_food=$b[id_food]) as jumlah_vote,
						(SELECT COUNT(h.id_member) FROM food_like h WHERE $b[id_food]=h.id_food) AS dilike
						 FROM	food_rating f WHERE $b[id_food] =f.id_food"));

					mysqli_query($koneksi,"UPDATE food set jumlah='$rank[rank]', rank_vote='$rank[jumlah_vote]', rank_like='$rank[dilike]', negara='$b[id_negara]', propinsi='$b[id_propinsi]', kota='$b[id_kota]', mall='$b[id_mall]' where id_food='$b[id_food]'");
				}
				
			}
			else{
				if(!empty($_GET['cleanlines']) and !empty($_GET['flavor']) and !empty($_GET['aroma']) and !empty($_GET['freshness']) and !empty($_GET['cooking']) and !empty($_GET['serving'])){
					$r1=$_GET['cleanlines'] * 0.21;
					$r2=$_GET['flavor'] * 0.20;
					$r3=$_GET['freshness'] * 0.18;
					$r4=$_GET['cooking'] * 0.17;
					$r5=$_GET['aroma'] * 0.15;
					$r6=$_GET['serving'] * 0.09;
					mysqli_query($koneksi,"INSERT INTO food_rating (id_food,id_member,cleanlines,flavor,freshness,cooking,aroma,serving,tgl_food_rating) values('$id','$_SESSION[food_member]','$r1','$r2','$r3','$r4','$r5','$r6','$sekarang')");
					mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('food','$id')");
					$_SESSION['resto_notif']     = "rating";

					$global=mysqli_query($koneksi,"SELECT id_food, id_negara, id_propinsi, id_kota, id_mall FROM food f LEFT JOIN restaurant r on f.id_restaurant = r.id_restaurant");
					while ($b=mysqli_fetch_array($global)) {
						$rank=mysqli_fetch_array(mysqli_query($koneksi, "SELECT
							((SUM(f.cleanlines) + SUM(f.flavor) + SUM(f.freshness) + SUM(f.cooking) + SUM(f.aroma) + SUM(f.serving)) / COUNT(f.id_member)) as rank,
							(select count(id_food_rating) as jumlah from food_rating where food_rating.id_food=$b[id_food]) as jumlah_vote,
							(SELECT COUNT(h.id_member) FROM food_like h WHERE $b[id_food]=h.id_food) AS dilike
							 FROM	food_rating f WHERE $b[id_food] =f.id_food"));

						mysqli_query($koneksi,"UPDATE food set jumlah='$rank[rank]', rank_vote='$rank[jumlah_vote]', rank_like='$rank[dilike]', negara='$b[id_negara]', propinsi='$b[id_propinsi]', kota='$b[id_kota]', mall='$b[id_mall]' where id_food='$b[id_food]'");
					}

				}
				else{
					$_SESSION['resto_notif']     = "rating_pilih";
					include "config/func/rank_food.php";
				}
			}
		}
		else{
			$_SESSION['resto_notif']     = "rating_gagal";
			include "config/func/rank_food.php";
		}
	}
	if($_GET['fungsi']=="7"){
		$ada=mysqli_num_rows(mysqli_query($koneksi,"select id_food_photo_like from food_photo_like where id_food_photo_like='$_GET[idp]' and id_member='$_SESSION[food_member]' and tgl_photo_like='$sekarang'"));
		if($ada==0){
			mysqli_query($koneksi,"insert into food_photo_like (id_member,id_food_photo,tgl_photo_like) values('$_SESSION[food_member]','$_GET[idp]','$sekarang')");
			$_SESSION['resto_notif']     = "suka_photo";
		}
		else{
			$_SESSION['resto_notif']     = "suka_photo_gagal";
		}
	}
}
else{
	$_SESSION['resto_notif']     = "login_dulu";
}
header("Location: ".$base_url."/pages/food/".$_GET['page']."/".$_GET['id']."/".$seo);
?>
