<?php 
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";
include "config/func/jumlah_data.php";
include "config/func/id_masking.php";
//include "config/func/rank_beverage.php";
$auto_logout=1800000;
if(!empty($_SESSION['food_member'])){
	if (time()-$_SESSION['timestamp']>$auto_logout){
		session_destroy();
		session_unset();
		header("Location: ".$base_url."/auto-logout");
		exit();
	}else{
		$_SESSION['timestamp']=time();
	}
	include "config/func/member_data.php";
}
$id = id_masking($_GET['id']);

$active="events";
$type="process_info";

 mysqli_query($koneksi,"update events_food set dilihat=dilihat + 1 where id_events_food='$id'"); 


$resto=mysqli_query($koneksi,"SELECT *,
	ef.dilihat as dilihat,
	
	(SELECT COUNT(h.id_member) FROM events_food_like h WHERE h.id_events_food=ef.id_events_food) AS dilike,
	(select count(ekr.id_events_food) from events_food_rekomendasi ekr where ekr.id_events_food=ef.id_events_food) as direkomendasi,
	(SELECT SUM(efr.cleanliness)  / COUNT(efr.id_member) FROM events_food_rating efr WHERE ef.id_events_food=efr.id_food) AS t_cleanlines,
	(SELECT SUM(efr.flavor)       / COUNT(efr.id_member) FROM events_food_rating efr WHERE ef.id_events_food=efr.id_food) AS t_flavor,
	(SELECT SUM(efr.freshness)    / COUNT(efr.id_member) FROM events_food_rating efr WHERE ef.id_events_food=efr.id_food) AS t_freshness,
	(SELECT SUM(efr.cooking)      / COUNT(efr.id_member) FROM events_food_rating efr WHERE ef.id_events_food=efr.id_food) AS t_cooking,
	(SELECT SUM(efr.pna)          / COUNT(efr.id_member) FROM events_food_rating efr WHERE ef.id_events_food=efr.id_food) AS t_pna,
	(SELECT SUM(efr.serving)      / COUNT(efr.id_member) FROM events_food_rating efr WHERE ef.id_events_food=efr.id_food) AS t_serving,
	(SELECT COUNT(efr.id_member) FROM events_food_rating efr WHERE ef.id_events_food=efr.id_food) AS jumlah_vote,

	(SELECT (SUM(efr.cleanliness) + SUM(efr.flavor) + SUM(efr.freshness) + SUM(efr.cooking) + SUM(efr.pna) + SUM(efr.serving)) / COUNT(efr.id_member) 
														 FROM events_food_rating efr WHERE ef.id_events_food=efr.id_food) as total_rate

	FROM `events_food` ef 

	LEFT JOIN events_kiosk ek   on ef.id_kiosk=ek.id_events_kiosk 
    LEFT Join events ev 	    on ek.id_events=ev.id_events
	LEFT join events_category c on ev.id_category=c.id_events_category

	LEFT join negara n          on ev.id_negara=n.id_negara
	LEFT join propinsi p        on ev.id_propinsi=p.id_propinsi 
	LEFT join kota k            on ev.id_kota=k.id_kota  

	WHERE id_events_food = '$id'");


$r=mysqli_fetch_array($resto);

$slug=seo($r['nama_events_food']);
$post=date("jS M, Y", strtotime($r['tgl_post']));

$total_cleanlines=number_format((float)$r['t_cleanlines'], 2, '.', '');
$img_cleanlines=$total_cleanlines / 0.18;

$total_flavor=number_format((float)$r['t_flavor'], 2, '.', '');
$img_flavor=$total_flavor / 0.18;

$total_freshness=number_format((float)$r['t_freshness'], 2, '.', '');
$img_freshness=$total_freshness / 0.18;

$total_cooking=number_format((float)$r['t_cooking'], 2, '.', '');
$img_cooking=$total_cooking / 0.18;

$total_pna=number_format((float)$r['t_pna'], 2, '.', '');
$img_pna=$total_pna / 0.14;

$total_serving=number_format((float)$r['t_serving'], 2, '.', '');
$img_serving=$total_serving / 0.14;

$total_rating=number_format((float)$r['total_rate'], 2, '.', '');
$gambar=$r['gambar_food'];
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
  <title><?php echo"$r[nama_events_food]"; ?> - Foodie Guidances</title>
	<meta name="keywords" content="">
  <meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<meta property="og:title" content="<?php echo"$r[nama_events_food]"; ?>" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="<?php echo"$base_url/pages/process/info/$_GET[id]/$slug"; ?>" />
	<meta property="og:image" content="<?php echo"$base_url/assets/img/process/medium_$gambar"; ?>" />
	<meta property="og:description" content="<?php echo"$r[nama_events_food]"; ?>" />
	<meta property="og:site_name" content="Foodie Guidances" />
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/slick.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/lightbox.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>

	<link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
 <body style="position: initial;" class="drawer drawer--right">
	<?php include"config/inc/header.php"; ?>
    <!-- Fixed navbar -->
    <div class="container hook">
		<div class="row">
			<div class="col-12 col-8a">
				<ul class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="<?php echo"$base_url/pages/process/search/"; ?>">Events</a></li>
					<li class="active"><?php echo"$r[nama_events_food]"; ?></li>
				</ul>
				<?php
					if(isset($_SESSION['resto_notif'])){
						if($_SESSION['resto_notif']=="suka"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully like this process.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="suka_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was liked this process before. You can only do one time in 24 hours.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="rekomendasi"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully recommend this process.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="rekomendasi_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was recommeded this process before. You can only do one time in 24 hours.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="bookmark"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully bookmark this process.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="bookmark_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was bookmark this process before. You can only do one time.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="disini"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully added the data to taste it at this process.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="disini_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was added the data at this process before. You can only do one time.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="lapor"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully report this process.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="lapor_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was reported this process before. You can only do one time.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="rating"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully rating this process.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="rating_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was rated this process before. You can only do one time in 24 hours.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="rating_pilih"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> Please select star rating value before vote.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="login_dulu"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Must Login!</strong> You must login or register before doing this action.
							</div>";
						}
						unset($_SESSION['resto_notif']);
					}
				?>
				<h1 class="judul">
					<?php
					echo"$r[nama_events_food]<br>";
					if($total_rating<>0){
						echo"";?>
						<a href = "javascript:void(0)" style="text-decoration: none;" class='klik-bintang'>
							<?php echo"<input type='hidden' class='rating' data-filled='fa fa-star' data-empty='fa fa-star-o' readonly='readonly' value='$total_rating'>";?>
						</a>
						<?php echo"
							<span style='font-size: 14px;'>Rating : $total_rating </span><em class='f-12'>from $r[jumlah_vote] votes</em>";
					}
					?>
				</h1>
				<div class="mb10 f-12"><em>Category</em> <?php echo"$r[nama_events_category]"; ?> <span class="bullet">&#8226;</span>
					<a href="<?php echo"$base_url/$r[username]"; ?>"><em>by</em> <?php if(!empty($r['nama_depan'])){echo"$r[nama_depan] $r[nama_belakang]";}else{echo"Admin";} ?></a>
					<span class="bullet">&#8226;</span> <em>post</em> <?php echo"$post"; ?>
				</div>



				<div class="box-info-1">
					<div class="pull-left">
					  <?php echo"<a href='$base_url/assets/img/events/food/big_$gambar' data-lightbox='$r[nama_events_food]' data-title='$r[nama_events_food]'>
					  <img data-original='$base_url/assets/img/events/food/big_$gambar' class='lazy' width='100%'></a>"; ?>
					</div>
					<div class="pull-right">
						<div class="detail-menu">
							<a href="<?php echo"$base_url/pages/foodie/event/info/$_GET[id]/1"; ?>" title="Like"><i class="fa fa-heart-o"></i></a>
							<a href="<?php echo"$base_url/pages/foodie/event/info/$_GET[id]/2"; ?>" title="Recommendation"><i class="fa fa-thumbs-o-up"></i></a>
							<a href="<?php echo"$base_url/pages/foodie/event/info/$_GET[id]/3"; ?>" title="Bookmark"><i class="fa fa-bookmark-o"></i></a>
							<span class="social-share"><i class="fa fa-share-alt"></i>
								<div class="social-box">
									<span class='st_facebook_large' displayText='Facebook'></span>
									<span class='st_linkedin_large' displayText='LinkedIn'></span>
									<span class='st_pinterest_large' displayText='Pinterest'></span>
									<span class='st_googleplus_large' displayText='Google +'></span>
									<span class='st_twitter_large' displayText='Tweet'></span>
									<span class='st_sharethis_large' displayText='ShareThis'></span>
								</div>
							</span>
							<a href="<?php echo"$base_url/pages/foodie/event/info/$_GET[id]/5"; ?>" title="Report"><i class="fa fa-flag-o"></i></a>
						</div>

						<div class="statistik">
							<em>
								<i class="fa fa-heart"></i>
								<a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">
									<?php	echo"$r[dilike] likes"; ?>
								</a>
								<?php
								if(!empty($_SESSION['food_member'])){
									$me=mysqli_query($koneksi,"select tgl_like from events_food_like where id_member='$id_member' and id_events_food='$id' order by id_events_food desc limit 1");
									$ada_melike=mysqli_num_rows($me);
									if($ada_melike<>0){
										$q=mysqli_fetch_array($me);
										$like_date=date("jS M, Y", strtotime($q['tgl_like']));
										echo" (you has liked on $like_date)";
									}
								}
								?>
							</em>
							<em><i class="fa fa-eye"></i> <?php echo"$r[dilihat]"; ?> views</em>
							<em><i class="fa fa-thumbs-up"></i>
								<a href = "javascript:void(0)" onclick = "document.getElementById('lighta').style.display='block';document.getElementById('fadea').style.display='block'">
								<?php echo"$r[direkomendasi]"; ?> recommendations</a>
								</em>
						</div>
						
					</div>
					<div class="clearfix"></div>
					<div class="mt20 tag">Tagged under
						<?php
						$tag  = explode(",",$r['tag']);
						foreach ( $tag as $item_tag ) {
							echo"<a href='$base_url/pages/events/search/?tag=$item_tag'>#$item_tag</a> ";
						}
						?>
					</div>


				</div>
				<div class="box-info-2">
					<div class="box-head">
						Basic Information
					</div>
					<div class="box-panel">
						<dl class="dl-horizontal">
							<?php
							$dov=date("jS M, Y", strtotime($r['periode']));
							$doe=date("jS M, Y", strtotime($r['date_of_expired']));
							
							echo"
							<dt>Country of Origin</dt><dd>$r[nama_negara]</dd>
							<dt>Province</dt><dd>$r[nama_propinsi]</dd>
							<dt>City</dt><dd>$r[nama_kota]</dd>
							<dt>Address</dt><dd>$r[address_events]</dd>
							<dt>Categories</dt><dd>$r[nama_events_category]</dd>
							<dt>Event Periode</dt><dd>$dov</dd>
							<dt>Current Status</dt><dd>";
								if (strtotime($r['periode']) <= time() ) {
									echo "Closed";
								}
								else{
									echo "Ongoing";
								}
							echo"</dd>
							
							";
							
							
							?>
						</dl>
					</div>
				</div>


				<div role="tabpanel" id="tab-vote">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#result" aria-controls="result" role="tab" data-toggle="tab">Result</a></li>
						<li role="presentation"><a href="#feature" aria-controls="feature" role="tab" data-toggle="tab">Feature Vote</a></li>
						<li role="presentation"><a href="#quick" aria-controls="quick" role="tab" data-toggle="tab">Quick Vote</a></li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane tab-panel active fade in" id="result">
							<table class="tablebintang pertama">
								<tr>
									<td>Cleanliness <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall cleanliness of the beverage and glasses served, which included whether the beverage has unwanted insect, plastic, hair or unclean glasses and utensils used."></i></td>
									<td><input type="hidden" class="rating" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="cleanliness" value="<?php echo $img_cleanlines;?>" readonly></td>
									<td rowspan="5" class="mene-atas">
										<span><?php echo"$total_rating"; ?></span>
										<em>from <?php echo"$r[jumlah_vote]"; ?> vote</em>
									</td>
								</tr>
								<tr>
									<td>Flavor <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assesment of the taste of the beverage which has unique personality."></i></td>
									<td><input type="hidden" class="rating"  data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="flavor" value="<?php echo $img_flavor;?>" readonly></td>
								</tr>
								<tr>
									<td>Freshness <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment on the beverage ingredient's freshness which can include fresh fruits used for the fruit juices beverage or fresh tea and coffe beans."></i></td>
									<td><input type="hidden" class="rating"  data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="freshness" value="<?php echo $img_freshness;?>" readonly></td>
								</tr>
								<tr>
									<td>Cooking <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment on the beverage ingredient's freshness which can include fresh fruits used for the fruit juices beverage or fresh tea and coffe beans."></i></td>
									<td><input type="hidden" class="rating"  data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="cooking" value="<?php echo $img_cooking;?>" readonly></td>
								</tr>
								<tr>
									<td>Presentation & Aroma <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment on the beverage ingredient's freshness which can include fresh fruits used for the fruit juices beverage or fresh tea and coffe beans."></i></td>
									<td><input type="hidden" class="rating"  data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="pna" value="<?php echo $img_pna;?>" readonly></td>
								</tr>
								<tr>
									<td>Serving <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment on the beverage ingredient's freshness which can include fresh fruits used for the fruit juices beverage or fresh tea and coffe beans."></i></td>
									<td><input type="hidden" class="rating"  data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="serving" value="<?php echo $img_serving;?>" readonly></td>
								</tr>
								<tr class="total-vote">
									<td colspan="2">
										<span><?php echo"$total_rating"; ?></span>
										<em>from <?php echo"$r[jumlah_vote]"; ?> vote</em>
									</td>
								</tr>
							</table>			
						</div>
						<div role="tabpanel" class="tab-pane tab-panel fade" id="feature">
							<form method="get" action="<?php echo"$base_url/events_food_aksi.php"; ?>">
								<input type="hidden" value="6" name="fungsi">
								<input type="hidden" value="info" name="page">
								<input type="hidden" value="<?php echo"$_GET[id]"; ?>" name="id">
								<table class="tablebintang pertama">
									<tr>
										<td>Cleanliness <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall cleanliness of the food and plates served, which can include whether the food has unwanted insect, plastic, hair or unclean plates, bowls and utensils used."></i></td>
										<td><input type="hidden" class="rating" id="rat1" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="cleanliness"></td>
										<td rowspan="5" class="mene-atas"><button class="btn btn-danger" type="submit">Vote</button>
											<div class='alert-danger alert-dismissible as' role='alert' >
									           <strong>Please submit minimum one star for each rating categories in other to be rated</strong>
									         </div>
										</td>
									</tr>
									<tr>
										<td>Flavor <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assesment of the taste of the beverage which has unique personality."></i></td>
										<td><input type="hidden" class="rating" id="rat2" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="flavor"></td>
									</tr>
									<tr>
										<td>Freshness <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment on the beverage ingredient's freshness which can include fresh fruits used for the fruit juices beverage or fresh tea and coffe beans."></i></td>
										<td><input type="hidden" class="rating" id="rat3" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="freshness"></td>
									</tr>
									<tr>
										<td>Cooking <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment on the beverage ingredient's freshness which can include fresh fruits used for the fruit juices beverage or fresh tea and coffe beans."></i></td>
										<td><input type="hidden" class="rating" id="rat4" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="cooking"></td>
									</tr>
									<tr>
										<td>Presentation & Aroma <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment on the beverage ingredient's freshness which can include fresh fruits used for the fruit juices beverage or fresh tea and coffe beans."></i></td>
										<td><input type="hidden" class="rating" id="rat5" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="pna"></td>
									</tr>
									<tr>
										<td>Serving <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment on the beverage ingredient's freshness which can include fresh fruits used for the fruit juices beverage or fresh tea and coffe beans."></i></td>
										<td><input type="hidden" class="rating" id="rat6" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="serving"></td>
									</tr>
									<tr class="total-vote">
										<td colspan="2">
											<button class="btn btn-danger" type="submit">Vote</button>
											<div class='alert-danger alert-dismissible as' role='alert'>
								                <strong>Please submit minimum one star for each rating categories in other to be rated</strong>
								            </div>
										</td>
									</tr>
								</table>								
							</form>
						</div>
						<div role="tabpanel" class="tab-pane tab-panel fade" id="quick">
							<form method="get" action="<?php echo"$base_url/events_food_aksi.php"; ?>">
								<input type="hidden" value="6" name="fungsi">
								<input type="hidden" value="info" name="page">
								<input type="hidden" value="<?php echo"$_GET[id]"; ?>" name="id">
								<table class="tablebintang">
									<tr>
										<td><strong>Give your vote</strong></td>
										<td><input type="hidden" class="rating" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="rating"></td>
										<td rowspan="2" class="mene-atas"><button class="btn btn-danger" type="submit">Vote</button>
											<div class='alert-danger alert-dismissible as' role='alert'>
									            <strong>Please submit minimum one star for each rating categories in other to be rated</strong>
									          </div>
										</td>
									</tr>
									<tr class="total-vote">
										<td colspan="2">
											<button class="btn btn-danger" type="submit">Vote</button>
										</td>
									</tr>
								</table>
							</form>
						</div>
					</div>
					<ul class="list-inline">
						<li><i class="fa fa-star"></i> Poor</li>
						<li><i class="fa fa-star"></i><i class="fa fa-star"></i> Below Average</li>
						<li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Average</li>
						<li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Very Good</li>
						<li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Excellent</li>
					</ul>
				</div>
				
				<hr>
				<div id="disqus_thread"></div>
			</div>
			<div class="col-4 mene-atas">
				<?php include"config/inc/search.php"; ?>
				<?php include"config/inc/iklan.php"; ?>
				<div class="fb-like-box" data-href="https://www.facebook.com/foodieguidances#_=_" data-width="220" data-height="350" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
			</div>
		</div>
    </div>
    <?php include"config/inc/footer.php"; ?>
	 <?php
 	$idu = id_masking($_GET['id']);
 		$pb=mysqli_query($koneksi, "select * from beverage where id_beverage < $idu ORDER BY id_beverage desc limit 1");
 	$jumpb=mysqli_num_rows($pb);
 	$tpb=mysqli_fetch_array($pb);
 	if ($jumpb<>0) {
 		$ba = id_masking($tpb['id_beverage']);
 		$slug=seo($tpb['nama_beverage']);
 		$next="$base_url/pages/beverage/info/$ba/$slug";
 	} else {
 		$pba=mysqli_query($koneksi, "select * from beverage ORDER BY id_beverage desc limit 1");
 		$jumpba=mysqli_num_rows($pba);
 		$tpba=mysqli_fetch_array($pba);
 		$baa = id_masking($tpba['id_beverage']);
 		$sluga=seo($tpba['nama_beverage']);
 		$next="$base_url/pages/beverage/info/$baa/$sluga";
 	}

 	$pn=mysqli_query($koneksi, "select * from beverage where id_beverage > $idu ORDER BY id_beverage limit 1");
 	$jumpn=mysqli_num_rows($pn);
 	$tpn=mysqli_fetch_array($pn);
 	if ($jumpn<>0) {
 		$na = id_masking($tpn['id_beverage']);
 		$slug=seo($tpn['nama_beverage']);
 		$back="$base_url/pages/beverage/info/$na/$slug";
 	} else {
 		$pna=mysqli_query($koneksi, "select * from beverage ORDER BY id_beverage limit 1");
 		$jumpna=mysqli_num_rows($pna);
 		$tpna=mysqli_fetch_array($pna);
 		$naa = id_masking($tpna['id_beverage']);
 		$sluga=seo($tpna['nama_beverage']);
 		$back="$base_url/pages/beverage/info/$naa/$sluga";
 	}
 ?>
 <a class="scrollToNext" href="<?php echo $back ?>" style="bottom: 320px;"><img src="<?php echo"$base_url/assets/img/theme/kiri.png" ?>"></a>
 <a class="scrollToNext" href="<?php echo $next ?>" style="bottom: 273px;"><img src="<?php echo"$base_url/assets/img/theme/kanan.png" ?>"></a>
 <a href="#" class="scrollToTop"><img src="<?php echo"$base_url/assets/img/theme/atas.png" ?>"></a>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap-rating.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/lightbox.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/slick.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.lazyload.min.js"></script>

	<script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
  <script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
  <script>
      $(document).ready(function() {
      	$(".as").hide();

	      



	      $("#rat1,#rat2,#rat3,#rat4,#rat5,#rat6").change(function(){
	      	var rata1 = $("#rat1").val();
	      var rata2 = $("#rat2").val();
	      var rata3 = $("#rat3").val();
	      var rata4 = $("#rat4").val();
	      var rata5 = $("#rat5").val();
	      var rata6 = $("#rat6").val();
	      	
	         if ((rata1 > 0) && (rata2 > 0) && (rata3 > 0) && (rata4 > 0) && (rata5 > 0) && (rata6 > 0)) {
	            $(".btn-danger").show();
	            $(".as").hide();
	         }
	         else {
	            $(".btn-danger").hide();
	            $(".as").show();
	         }
	      });

	      // $("#rat1,#rat2,#rat3,#rat4").change(function(){
	      //    if ((rat1 > 0) && (rat2 > 0) && (rat3 > 0) && (rat4 > 0)) {
	      //       $(".btn-danger").show();
	      //       $(".as").hide();
	      //    }
	      // });

				$('.drawer').drawer({
           iscroll: {
             scrollbars: true,
             mouseWheel: true,
             interactiveScrollbars: true,
             shrinkScrollbars: 'scale',
             fadeScrollbars: true,
             click: true
           },
           showOverlay: true
        });

			$('.tutup').click(function(){
				$('.iklanan').toggleClass('hilang', 500);
			});

			$('.cart').click(function(e){
				e.stopPropagation();
				$('.dis-cart').toggle();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$('.plus').click(function(e){
				e.stopPropagation();
				$('.dis-plus').toggle();
				$('.dis-cart').hide();
				$('.dis-share').hide();
			});
			$('.share').click(function(e){
				e.stopPropagation();
				$('.dis-share').toggle();
				$('.dis-plus').hide();
				$('.dis-cart').hide();
			});
			$('.drawer-toggle').click(function(){
				$('.dis-cart').hide();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$(document).click(function () {
				 var $el = $(".dis-share");
				 if ($el.is(":visible")) {
					  $el.fadeOut(200);
				 }
				 var $ela = $(".dis-plus");
				 if ($ela.is(":visible")) {
					  $ela.fadeOut(200);
				 }
				 var $elu = $(".dis-cart");
				 if ($elu.is(":visible")) {
					  $elu.fadeOut(200);
				 }
		   });

      });
  </script>

	<?php if(!empty($_SESSION['food_member'])){ ?>
		<script src="<?php echo"$base_url"; ?>/assets/js/theme.js"></script>
	<?php } ?>
	<script type="text/javascript">
		$(document).ready(function () {
			$(document).on("contextmenu",function(e){
				if(e.target.nodeName != "INPUT" && e.target.nodeName != "TEXTAREA")
				e.preventDefault();
			});
			$.fn.disableTextSelect = function() {
				return this.each(function() {
					$(this).css({
						'MozUserSelect':'none',
						'webkitUserSelect':'none'
					}).attr('unselectable','on').bind('selectstart', function() {
						return false;
					});
				});
			};
			$('body').disableTextSelect();
		});
	</script>

	<script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>
	<script type="text/javascript">stLight.options({publisher: "7b97d330-d7b9-49c3-b5ee-b3aaa89bbe66", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
	<div id="fb-root"></div>

	<script type="text/javascript">
	$(document).ready(function() {
		$(window).scroll(function(){
	 		  if ($(this).scrollTop() > 100) {
	 			  $('.scrollToTop').fadeIn();
	 		  } else {
	 			  $('.scrollToTop').fadeOut();
	 		  }
	 	  });

	 	  $(window).scroll(function(){
	 		 if ($(this).scrollTop() > 50) {
	 			 $('.scrollToNext').fadeIn();
	 		 } else {
	 			 $('.scrollToNext').fadeOut();
	 		 }
	 	  });

			$('#go-to-vote').click(function() {
				$('html, body').animate({
					scrollTop: $( $(this).attr('href') ).offset().top
				}, 500);
				$('#tab-vote a[href="#feature"]').tab('show');
				return false;
			});
	});
	</script>

	<script src="<?php echo"$base_url"; ?>/assets/js/owl.carousel.js"></script>
	<link href="<?php echo"$base_url"; ?>/assets/css/owl.carousel.css" rel="stylesheet">
   <link href="<?php echo"$base_url"; ?>/assets/css/owl.theme.css" rel="stylesheet">
	<style>
	#owl-demo .item{
		 margin: 3px;
	}
	#owl-demo .item img{
		 display: block;
		 width: 100%;
		 height: auto;
	}
	</style>
	<script>
	$(document).ready(function() {
	  $("#owl-demo").owlCarousel({
		 items : 4,
		 navigation : true
	  });
	});
	</script>

	<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=348272391978609&version=v2.0";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
	<script type="text/javascript">
		$(document).ready(function () {


			


			$("img.lazy").lazyload({
				effect : "fadeIn"
			});
			$(".alert").fadeTo(5000, 500).fadeOut(500, function(){
				$(".alert").alert('close');
			});
			$('.tip').tooltip();
			$('.rating').rating();
			$('.member-carousel').slick({
				infinite: true,
				slidesToShow: 14,
				slidesToScroll: 14,
				arrows:false,
				autoplay:true,
				autoplaySpeed: 10000
			});
			$('#go-to-vote').click(function() {
				$('html, body').animate({
					scrollTop: $( $(this).attr('href') ).offset().top
				}, 500);
				$('#tab-vote a[href="#feature"]').tab('show');
				return false;
			});
			$('.social-share').click( function(event){
				event.stopPropagation();
				$( ".social-box" ).toggle(1);
			});
			$('.carousel').carousel({
				interval: 10000
			});
			$(document).click( function(){
				$('.social-box').hide();
			});
		});
		/* * * CONFIGURATION VARIABLES * * */
		var disqus_shortname = 'foodieguidancescom';

		/* * * DON'T EDIT BELOW THIS LINE * * */
		(function() {
			var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
			dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
			(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
		})();
	</script>

	<div id="lighta" class="white_content">
		<h3 style="font-size: 20px; margin: 0px 0px 15px;">Recommendation</h3>
		<table class="table">
			<?php
			$idu=id_masking($_GET['id']);
			$me=mysqli_query($koneksi,"SELECT * FROM events_food_rekomendasi r left join member m on m.id_member=r.id_member where r.id_events_food='$idu'");
			$no=1;
			$juma=mysqli_num_rows($me);
			if (!empty($juma)) {
				while ($tame=mysqli_fetch_array($me)) {
				$like_date=date("jS M, Y", strtotime($tame['tgl_rekomendasi']));
					echo "<tr><td>$no.</td>
						<td><img src='$base_url/assets/img/member/$tame[gambar_thumb]' class='foto_profil' width='30px'></td> <td>$tame[username]</td> <td>$like_date</td></tr>";
				$no++;
				}
			}
			else {
				echo "Empty";
			}
			?>
		</table>
		<a style="position: absolute; border: 6px solid rgb(255, 255, 255); right: -6px; top: -6px; z-index: 999999999; background: #C00606; color: rgb(255, 255, 255); padding: 6px 10px;" href = "javascript:void(0)" onclick = "document.getElementById('lighta').style.display='none';document.getElementById('fadea').style.display='none'"><i class="fa fa-close"></i></a>
	</div>
	<div id="fadea" class="black_overlay"  onclick = "document.getElementById('lighta').style.display='none';document.getElementById('fadea').style.display='none'"></div>

	<div id="light" class="white_content">
		<h3 style="font-size: 20px; margin: 0px 0px 15px;">Like</h3>
		<table class="table">
			<?php
			$idu=id_masking($_GET['id']);
			$me=mysqli_query($koneksi,"SELECT * FROM events_food_like r left join member m on m.id_member=r.id_member where r.id_events_food='$idu'");
			$no=1;
			$juma=mysqli_num_rows($me);
			if (!empty($juma)) {
				while ($tame=mysqli_fetch_array($me)) {
				$like_date=date("jS M, Y", strtotime($tame['tgl_like']));
					echo "<tr><td>$no.</td>
						<td><img src='$base_url/assets/img/member/$tame[gambar_thumb]' class='foto_profil' width='30px'></td> <td>$tame[username]</td> <td>$like_date</td></tr>";
				$no++;
				}
			}
			else {
				echo "Empty";
			}
			?>
		</table>
		<a style="position: absolute; border: 6px solid rgb(255, 255, 255); right: -6px; top: -6px; z-index: 999999999; background: #C00606; color: rgb(255, 255, 255); padding: 6px 10px;" href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'"><i class="fa fa-close"></i></a>
	</div>
	<div id="fade" class="black_overlay"  onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'"></div>


</body>
</html>
