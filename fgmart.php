<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";
include "config/func/jumlah_data.php";
include "config/func/id_masking.php";
$auto_logout=1800000;
if(!empty($_SESSION['food_member'])){
	if (time()-$_SESSION['timestamp']>$auto_logout){
		session_destroy();
		session_unset();
		header("Location: ".$base_url."/auto-logout");
		exit();
	}else{
		$_SESSION['timestamp']=time();
	}
	include "config/func/member_data.php";
}
$active = "fgmart";
$type="fgmart";
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
  <title>FGMART - Foodie Guidances</title>
	<meta name="keywords" content="">
  <meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/jquery-ui.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/superslides.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>

	<link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">

	<link rel="prev" title="Page 2" href="<?php echo "$base_url/pages/recipe" ?>" />
	<link rel="next" title="Page 1" href="<?php echo  "$base_url/pages/coupon" ?>" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
 <body style="position: initial;" class="drawer drawer--right">
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>
	<div class="pencarian">
      <div class="pencari">
			<form method="get" action="<?php echo"$base_url/pages/fgmart/search/"; ?>" id="ale-form" onsubmit="myFunction()">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Search based on Name, Category, Country, State, City..." id="search" name="keyword">
					<span class="input-group-btn">
						<button class="btn btn-danger" type="submit"><i class="fa fa-search"></i></button>
					</span>
				</div><!-- /input-group -->
			</form>
		</div>
		<div id="slides">
			<div class="slides-container">
				<?php
				$sql=mysqli_query($koneksi,"SELECT * FROM slideshow ORDER BY id_slideshow ASC");
				while($w=mysqli_fetch_array($sql)){
					echo"<img src='$base_url/assets/img/slideshow/$w[gambar]' alt='foodie guidances'>";
				}
				?>
			</div>
		</div>
		<div class="wrap-pencarian mene-atas">
			<!-- <h1>FGMART, FoodieGuidances.com</h1> -->
			<div class="box-pencarian">
				<div class="col-16 al">
					<form method="get" action="<?php echo"$base_url/pages/fgmart/search/"; ?>">
						<strong>Search</strong>
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Search based on Name, Category.." id="search" name="keyword">
							<span class="input-group-btn">
								<button class="btn btn-danger" type="submit"><i class="fa fa-search"></i></button>
							</span>
						</div><!-- /input-group -->
					</form>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
    <div class="container no-padding">
		<h4 class="f-merah no-mb mt20 ml10-j"><a href="<?php echo"$base_url/pages/fgmart/search/?sort=r.id_fgmart+DESC"; ?>">Latest FGMART Stock</a></h4>
		<div class="media">
			<div class="row">
				<?php
				$resto=mysqli_query($koneksi,"select *,(select count(h.id_fgmart) from fgmart_like h where h.id_fgmart=f.id_fgmart) as dilike from fgmart f order by f.id_fgmart desc  limit 4");
				while($s=mysqli_fetch_array($resto)){
					$slug=seo($s['nama_fgmart']);
					$post=date("jS M, Y", strtotime($s['tgl_post']));
					$id = id_masking($s['id_fgmart']);
					echo"<div class='col-4 col-4a no-padding' style='margin-right:0%'>
						<div class='thumb'>
							<a href='$base_url/pages/fgmart/$id/$slug'><img data-original='$base_url/assets/img/fgmart/big_$s[gambar_fgmart]' class='lazy' width='220' height='165'></a>
						</div>
						<div class='info ml10-j'>
							<em>posted on $post</em>
							<h3 class='sembunyi'><a href='$base_url/pages/fgmart/$id/$slug'>$s[nama_fgmart]</a></h3>
							<ul class='list-unstyled f-12'>
								<li>$s[dilike] Like <span class='bullet'>&#8226;</span> $s[dilihat] View</li>
							</ul>
							<br>
						</div>
					</div>";
				}
				?>
			</div>
		</div>
		<h4 class="f-merah no-mb ml10-j"><a href="<?php echo"$base_url/pages/fgmart/search/?sort=r.dilihat+DESC"; ?>">Popular FGMART Stock</a></h4>
		<div class="media">
			<div class="row">
				<?php
				$resto=mysqli_query($koneksi,"select *,(select count(h.id_fgmart) from fgmart_like h where h.id_fgmart=f.id_fgmart) as dilike from fgmart f order by f.dilihat desc  limit 4");
				while($h=mysqli_fetch_array($resto)){
					$slug=seo($h['nama_fgmart']);
					$post=date("jS M, Y", strtotime($h['tgl_post']));
					$id = id_masking($h['id_fgmart']);
					echo"<div class='col-4 col-4a no-padding' style='margin-right:0%'>
						<div class='thumb'>
							<a href='$base_url/pages/fgmart/$id/$slug'><img data-original='$base_url/assets/img/fgmart/big_$h[gambar_fgmart]' class='lazy' width='220' height='165'></a>
						</div>
						<div class='info ml10-j'>
							<em>posted on $post</em>
							<h3 class='sembunyi'><a href='$base_url/pages/fgmart/$id/$slug'>$h[nama_fgmart]</a></h3>
							<ul class='list-unstyled f-12'>
								<li>$h[dilike] Like <span class='bullet'>&#8226;</span> $h[dilihat] View</li>
							</ul>
							<br>
						</div>
					</div>";
				}
				?>
			</div>
		</div>
		<h4 class="f-merah no-mb ml10-j"><a href="<?php echo"$base_url/pages/fgmart/search/?sort=dilike+DESC"; ?>">The Most Favored People</a></h4>
		<div class="media">
			<div class="row">
				<?php
				$resto=mysqli_query($koneksi,"select *,(select count(h.id_fgmart) from fgmart_like h where h.id_fgmart=f.id_fgmart) as dilike from fgmart f order by dilike desc  limit 4");
				while($b=mysqli_fetch_array($resto)){
					$slug=seo($b['nama_fgmart']);
					$post=date("jS M, Y", strtotime($b['tgl_post']));
					$id = id_masking($b['id_fgmart']);
					echo"<div class='col-4 col-4a no-padding' style='margin-right:0%'>
						<div class='thumb'>
							<a href='$base_url/pages/fgmart/$id/$slug'><img data-original='$base_url/assets/img/fgmart/big_$b[gambar_fgmart]' class='lazy' width='220' height='165'></a>
						</div>
						<div class='info ml10-j'>
							<em>posted on $post</em>
							<h3 class='sembunyi'><a href='$base_url/pages/fgmart/$id/$slug'>$b[nama_fgmart]</a></h3>
							<ul class='list-unstyled f-12'>
								<li>$b[dilike] Like <span class='bullet'>&#8226;</span> $b[dilihat] View</li>
							</ul>
							<br>
						</div>
					</div>";
				}
				?>
			</div>
		</div>
    </div>
    <?php include"config/inc/footer.php"; ?>
	 <a class="scrollToNext" href="<?php echo "$base_url/pages/recipe" ?>" style="bottom: 320px;"><img src="<?php echo"$base_url/assets/img/theme/kiri.png" ?>"></a>
	 <a class="scrollToNext" href="<?php echo  "$base_url/pages/coupon" ?>" style="bottom: 273px;"><img src="<?php echo"$base_url/assets/img/theme/kanan.png" ?>"></a>
	 <a href="#" class="scrollToTop"><img src="<?php echo"$base_url/assets/img/theme/atas.png" ?>"></a>

	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery-ui.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.superslides.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.lazyload.min.js"></script>

   <script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
   <script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
   <script>
      $(document).ready(function() {
	      $('.drawer').drawer();

			$('.tutup').click(function(){
				$('.iklanan').toggleClass('hilang', 500);
			});

			$('.cart').click(function(e){
				e.stopPropagation();
				$('.dis-cart').toggle();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$('.plus').click(function(e){
				e.stopPropagation();
				$('.dis-plus').toggle();
				$('.dis-cart').hide();
				$('.dis-share').hide();
			});
			$('.share').click(function(e){
				e.stopPropagation();
				$('.dis-share').toggle();
				$('.dis-plus').hide();
				$('.dis-cart').hide();
			});
			$('.drawer-toggle').click(function(){
				$('.dis-cart').hide();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$(document).click(function () {
				 var $el = $(".dis-share");
				 if ($el.is(":visible")) {
					  $el.fadeOut(200);
				 }
				 var $ela = $(".dis-plus");
				 if ($ela.is(":visible")) {
					  $ela.fadeOut(200);
				 }
				 var $elu = $(".dis-cart");
				 if ($elu.is(":visible")) {
					  $elu.fadeOut(200);
				 }
		   });
      });
   </script>

	<?php if(!empty($_SESSION['food_member'])){ ?>
		<script src="<?php echo"$base_url"; ?>/assets/js/theme.js"></script>
	<?php } ?>
	<script type="text/javascript">
		$(document).ready(function () {
			$(document).on("contextmenu",function(e){
				if(e.target.nodeName != "INPUT" && e.target.nodeName != "TEXTAREA")
				e.preventDefault();
			});
			$.fn.disableTextSelect = function() {
				return this.each(function() {
					$(this).css({
						'MozUserSelect':'none',
						'webkitUserSelect':'none'
					}).attr('unselectable','on').bind('selectstart', function() {
						return false;
					});
				});
			};
			$('body').disableTextSelect();
		});
	</script>
	<script type="text/javascript">
	$(document).ready(function() {
	$(window).scroll(function(){
		 if ($(this).scrollTop() > 100) {
			 $('.scrollToTop').fadeIn();
		 } else {
			 $('.scrollToTop').fadeOut();
		 }
	});

	$(window).scroll(function(){
		if ($(this).scrollTop() > 50) {
			$('.scrollToNext').fadeIn();
		} else {
			$('.scrollToNext').fadeOut();
		}
	});

	  $('#go-to-vote').click(function() {
		  $('html, body').animate({
			  scrollTop: $( $(this).attr('href') ).offset().top
		  }, 500);
		  $('#tab-vote a[href="#feature"]').tab('show');
		  return false;
	  });
	});
	</script>
	<div id="fb-root"></div>
	<script>
	$(document).ready(function () {
		$("img.lazy").lazyload({
			effect : "fadeIn"
		});
		$('#slides').superslides({
			animation: 'slide',
			inherit_height_from: '.pencarian',
			play: 8000,
			pagination: true
		});

		$('#search').autocomplete({
			source: "<?php echo "$base_url/config/func/ajax_fgmart_search.php"; ?>",
			minLength: 3
		});
	});
	/*=====For developer to config ====*/
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=348272391978609&version=v2.0";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script>
</body>
</html>
