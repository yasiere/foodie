<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";	
include "config/func/jumlah_data.php";	
$auto_logout=600;
if(!empty($_SESSION['food_member'])){
	if (time()-$_SESSION['timestamp']>$auto_logout){
		session_destroy();
		session_unset();
		header("Location: ".$base_url."/auto-logout");
		exit();
	}else{
		$_SESSION['timestamp']=time();
	}			
	include "config/func/member_data.php";
}
$active = "search";
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
    <title>Quick Search - Foodie Guidances</title>
	<meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/jquery-ui.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/superslides.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
 <body>
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>
	<div class="pencarian">
		<div id="slides">
			<div class="slides-container">
				<?php
				$sql=mysqli_query($koneksi,"SELECT * FROM slideshow ORDER BY id_slideshow ASC");
				while($w=mysqli_fetch_array($sql)){
					echo"<img src='$base_url/assets/img/slideshow/$w[gambar]' alt='foodie guidances'>";
				}
				?>
			</div>
		</div>
		<div class="wrap-pencarian">
			<h1>FoodieGuidances.com</h1>
			<div class="box-pencarian">
				<div class="pull-left">
					<strong>Search</strong>
					<form method="post" action="<?php echo"$base_url/pages/quick-search"; ?>">
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Search based on Name.." id="search" name="keyword" required>
							<span class="input-group-btn">
								<button class="btn btn-danger" type="submit"><i class="fa fa-search"></i></button>
							</span>
						</div><!-- /input-group -->
					</form>
				</div>
				<div class="pull-right">
					<strong class="blk">Submit</strong>
					<div class="btn-group">
						<button type="button" class="btn btn-danger btn-block dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-check"></i> New Submit</button>
						<ul class="dropdown-menu" role="menu">
							<?php
							if(!empty($_SESSION['food_member'])){
								echo"<li><a href='$base_url/$u[username]/my-restaurant/new'>Restaurant</a></li>
								<li><a href='$base_url/$u[username]/my-food/new'>Food</a></li>
								<li><a href='$base_url/$u[username]/my-beverage/new'>Beverage</a></li>
								<li><a href='$base_url/$u[username]/my-recipe/new'>Recipe</a></li>
								<li><a href='$base_url/$u[username]/my-article/new'>Article</a></li>
								<li><a href='$base_url/$u[username]/my-video/new'>Video</a></li>";
							}
							else{
								echo"<li><a href='$base_url/login-area/my-restaurant/new'>Restaurant</a></li>
								<li><a href='$base_url/login-area'>Food</a></li>
								<li><a href='$base_url/login-area'>Beverage</a></li>
								<li><a href='$base_url/login-area'>Recipe</a></li>
								<li><a href='$base_url/login-area'>Article</a></li>
								<li><a href='$base_url/login-area'>Video</a></li>";
							}
							?>
						</ul>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
    <div class="container">
		<h4 class="f-merah no-mb mt20">Restaurant</h4>
		<div class="media">
			<div class="row">
				<?php
				$sql=mysqli_query($koneksi,"select *,(select count(h.id_restaurant) from restaurant_like h where h.id_restaurant=r.id_restaurant) as dilike from restaurant r where r.restaurant_name like '%$_POST[keyword]%' order by r.id_restaurant desc");
				$ada_resto=mysqli_num_rows($sql);
				if($ada_resto<>0){
					while($a=mysqli_fetch_array($sql)){
						$slug=seo($a['restaurant_name']);
						$post=date("jS M, Y", strtotime($a['tgl_post']));
						echo"<div class='col-4'>
							<div class='thumb'>
								<a href='$base_url/pages/restaurant/info/$a[id_restaurant]/$slug'><img src='$base_url/assets/img/restaurant/small_$a[cover_photo]'></a>
							</div>
							<div class='info'>
								<em>posted on $post</em>
								<h3 class='sembunyi'><a href='$base_url/pages/restaurant/info/$a[id_restaurant]/$slug'>$a[restaurant_name]</a></h3>
								<ul class='list-unstyled f-12'>
									<li>$a[dilike] Like <span class='bullet'>&#8226;</span> $a[dilihat] View</li>
								</ul>
							</div>
						</div>";
					}
				}
				else{
					echo "<meta http-equiv=\"refresh\" content=\"0;URL=/pages/restaurant/search\">";
				}
				?>
			</div>
		</div>
    </div>
    <?php include"config/inc/footer.php"; ?>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery-ui.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.superslides.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/theme.js"></script>
	<script>
	$(document).ready(function () {
		$('#slides').superslides({
			animation: 'slide',
			inherit_height_from: '.pencarian',
			play: 8000,
			pagination: true
		});
		
		$('#search').autocomplete({
			source: "<?php echo "$base_url/config/func/ajax_restaurant_search.php"; ?>",
			minLength: 3
		});
	});
	</script>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=348272391978609&version=v2.0";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
</body>
</html>