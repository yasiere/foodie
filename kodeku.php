Detail Tugas

 Adapun ruang lingkup Job Deskripsi, akan saya jelaskan dalam keterangan dibawah ini :

 1. Brand identity.
 Akan menyediakan Logo / Brand identiy untuk DoBuy, Brand akan disesuaikan
 dengan citra bisnis DoBuy berdasarkan hasil interview dari pertemuan pertama
 dan kedua serta akan di kombinasikan dengan perkembangan informasi yang akan
 di peroleh seiring berjalannya proyek Brand Identiy. Saya akan dengan intens
 melakukan interaksi dengan klien guna menghasilkan tujuan atau hasil kreatifitas
 dalam hal Brand identity DoBuy yang sesuai dengan bisnis DoBuy.

 1.3. Illustrator.
 Akan menyediakan semua kebutuhan Materi UI & UX Desain, yaitu meliputi :
 	1.3.1. Custom Icon.
 	Grak icon yang akan dibuat secara khusus yang mana akan disesuaikan atau
 	diserasikan dengan Brand Identity sehingga mencipatakan suasana
 	keseragaman bagi user saat meng akses atau melihat tur - tur bisnis Online
 	DoBuy.
 	1.3.2. Custom Digital Banner.
 	Baner Digital DoBuy akan dibuat dengan mengindahkan segala aspek Brand
 	Identity DoBuy mulai dari warna, penggunaan Font Type dan setting layout
 	yang telah di standarisasi mengikuti Brand Identity DoBuy


1. Slicing Website
	Akan menerjemahkan tampilan yang dibuat oleh seorang desainer kedalam bentuk code
	menggunakan beberapa bahasa yaitu HTML, CSS dan JS untuk keperluan mempercantik 
	tampilan dan akan disesuaikan dengan BackEnd Dev.

2. Responsive Website
	Akan menyelaraskan tampilan yang telah dibuat berdasarkan Desiner UI&UX kedalam 
	beberapa device bebeda sesuai arahan dari desainer UI/UX, Meliputi device :

	Desktop - Tablet/Ipad - Mobile Smartphone

3. Menganimasikan Website
	Akan mepercantik tampilan sebuah website menggunakan kode JS, Meliputi tampilan :

	Animasi komponen - Slider - Popup/Lightbox - Transition Page

4. Mengoptimalkan kode
	Kode yang telah selesai dibuat akan masuk dalam proses pengoptimalan kode yang 
	bertujuan untuk memperingan kode dan mempersingkat waktu loading page.

5. Penghubung BackEnd
	Akan membantu menjelaskan backend untuk memahami tata letak file, fungsi-fungsi JS 
	yang telah dibuat.

	