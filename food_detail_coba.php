<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";
include "config/func/jumlah_data.php";
include "config/func/id_masking.php";

$auto_logout=1800000;
if(!empty($_SESSION['food_member'])){
	if (time()-$_SESSION['timestamp']>$auto_logout){
		session_destroy();
		session_unset();
		header("Location: ".$base_url."/auto-logout");
		exit();
	}else{
		$_SESSION['timestamp']=time();
	}
	include "config/func/member_data.php";
}
$id = id_masking($_GET['id']);
$active = "food";
$type = "food_detail";
mysqli_query($koneksi,"update food set dilihat=dilihat + 1 where id_food='$id'");
$resto=mysqli_query($koneksi,"select *,r.id_negara as kode_negara,f.dilihat,f.id_price_index,(SELECT COUNT(h.id_member) FROM food_like h WHERE h.id_food=f.id_food) AS dilike,(SELECT COUNT(h.id_member) FROM food_rekomendasi h WHERE h.id_food=f.id_food) AS direkomendasi,(SELECT COUNT(r.id_member) FROM food_rating r WHERE r.id_food=f.id_food) AS jumlah_vote,(SELECT SUM(r.cleanlines)/ COUNT(r.id_member) FROM food_rating r WHERE r.id_food=f.id_food) AS t_cleanlines,(SELECT SUM(r.flavor)/ COUNT(r.id_member) FROM food_rating r WHERE r.id_food=f.id_food) AS t_flavor,(SELECT SUM(r.freshness)/ COUNT(r.id_member) FROM food_rating r WHERE r.id_food=f.id_food) AS t_freshness,(SELECT SUM(r.cooking)/ COUNT(r.id_member) FROM food_rating r WHERE r.id_food=f.id_food) AS t_cooking,(SELECT SUM(r.aroma)/ COUNT(r.id_member) FROM food_rating r WHERE r.id_food=f.id_food) AS t_aroma,(SELECT SUM(r.serving)/ COUNT(r.id_member) FROM food_rating r WHERE r.id_food=f.id_food) AS t_serving,(SELECT (SUM(r.cleanlines) + SUM(r.flavor) + SUM(r.freshness) + SUM(r.cooking) + SUM(r.aroma) + SUM(r.serving)) / COUNT(r.id_member) FROM food_rating r WHERE r.id_food =f.id_food) AS total_rate FROM food f LEFT JOIN food_category k ON f.id_food_category=k.id_food_category LEFT JOIN price_index p ON f.id_price_index=p.id_price_index  LEFT JOIN msg_level l ON f.id_msg_level=l.id_msg_level LEFT JOIN restaurant r ON f.id_restaurant=r.id_restaurant LEFT JOIN type_of_business a ON r.id_type_of_business = a.id_type_of_business LEFT JOIN negara b ON r.id_negara = b.id_negara LEFT JOIN propinsi d ON r.id_propinsi = d.id_propinsi LEFT JOIN kota e ON r.id_kota = e.id_kota LEFT JOIN landmark g ON r.id_landmark = g.id_landmark LEFT JOIN member n ON f.id_member = n.id_member where f.id_food='$id'");
$r=mysqli_fetch_array($resto);
$id_resto = id_masking($r['id_restaurant']);
$seo_nama = seo($r['restaurant_name']);
$slug=seo($r['nama_food']);
$post=date("jS M, Y", strtotime($r['tgl_post']));
$total_cleanlines=number_format((float)$r['t_cleanlines'], 2, '.', '');
$img_cleanlines=$total_cleanlines / 0.21;
$total_flavor=number_format((float)$r['t_flavor'], 2, '.', '');
$img_flavor=$total_flavor / 0.20;
$total_freshness=number_format((float)$r['t_freshness'], 2, '.', '');
$img_freshness=$total_freshness / 0.18;
$total_cooking=number_format((float)$r['t_cooking'], 2, '.', '');
$img_cooking=$total_cooking / 0.17;
$total_aroma=number_format((float)$r['t_aroma'], 2, '.', '');
$img_aroma=$total_aroma / 0.15;
$total_serving=number_format((float)$r['t_serving'], 2, '.', '');
$img_serving=$total_serving / 0.09;
$total_rating=number_format((float)$r['total_rate'], 2, '.', '');
$gsql=mysqli_query($koneksi,"select p.gambar_food_photo,p.nama_food_photo, (select count(l.id_food_photo_like) from food_photo_like l where l.id_food_photo=p.id_food_photo) as total_like from food_photo p where p.id_food='$id' order by total_like desc,p.id_food_photo limit 1");
$g=mysqli_fetch_array($gsql);
$gambar=$g['gambar_food_photo'];

?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
  <title><?php echo"$r[nama_food]"; ?> - Foodie Guidances</title>

	<meta name="keywords" content="<?php echo"$r[nama_food]"; ?>, Rating : <?php echo $total_rating;?>">
  <meta name="description" content="<?php echo"$r[nama_food]"; ?>, Rating : <?php echo $total_rating;?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<meta property="og:title" content="<?php echo"$r[nama_food]"; ?>" />
	<meta property="og:url" content="<?php echo"$base_url/pages/food/info/$_GET[id]/$slug"; ?>" />
	<meta property="og:image" content="<?php echo"$base_url/assets/img/food/big_$gambar"; ?>" />
	<meta property="og:description" content="<?php echo"$r[nama_food], $r[street_address], Rating: $total_rating "; ?>" />
	<meta property="og:description" content="<?php echo"$r[nama_food], $r[street_address], Rating: $total_rating "; ?>" />
	<meta property="og:site_name" content="Foodie Guidances" />
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/slick.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/lightbox.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/owl.carousel.css" rel="stylesheet">
  <link href="<?php echo"$base_url"; ?>/assets/css/owl.theme.css" rel="stylesheet">

	<link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">
</head>
 <body style="position: initial;" class="drawer drawer--right">
	<?php include"config/inc/header.php"; ?>
    <!-- Fixed navbar -->
    <div class="container hook">
		</div>
    <?php include"config/inc/footer.php"; ?>
	 <?php
	 $idu = id_masking($_GET['id']);
		 $pb=mysqli_query($koneksi, "select * from food where id_food < $idu ORDER BY id_food desc limit 1");
	 $jumpb=mysqli_num_rows($pb);
	 $tpb=mysqli_fetch_array($pb);
	 if ($jumpb<>0) {
		 $ba = id_masking($tpb['id_food']);
		 $slug=seo($tpb['nama_food']);
		 $next="$base_url/pages/food/info/$ba/$slug";
	 } else {
		 $pba=mysqli_query($koneksi, "select * from food ORDER BY id_food desc limit 1");
		 $jumpba=mysqli_num_rows($pba);
		 $tpba=mysqli_fetch_array($pba);
		 $baa = id_masking($tpba['id_food']);
		 $sluga=seo($tpba['nama_food']);
		 $next="$base_url/pages/food/info/$baa/$sluga";
	 }

	 $pn=mysqli_query($koneksi, "select * from food where id_food > $idu ORDER BY id_food limit 1");
	 $jumpn=mysqli_num_rows($pn);
	 $tpn=mysqli_fetch_array($pn);
	 if ($jumpn<>0) {
		 $na = id_masking($tpn['id_food']);
		 $slug=seo($tpn['nama_food']);
		 $back="$base_url/pages/food/info/$na/$slug";
	 } else {
		 $pna=mysqli_query($koneksi, "select * from food ORDER BY id_food limit 1");
		 $jumpna=mysqli_num_rows($pna);
		 $tpna=mysqli_fetch_array($pna);
		 $naa = id_masking($tpna['id_food']);
		 $sluga=seo($tpna['nama_food']);
		 $back="$base_url/pages/food/info/$naa/$sluga";
	 }
 ?>
 <a class="scrollToNext" href="<?php echo $back ?>" style="bottom: 320px;"><img src="<?php echo"$base_url/assets/img/theme/kiri.png" ?>"></a>
 <a class="scrollToNext" href="<?php echo $next ?>" style="bottom: 273px;"><img src="<?php echo"$base_url/assets/img/theme/kanan.png" ?>"></a>
 <a href="#" class="scrollToTop"><img src="<?php echo"$base_url/assets/img/theme/atas.png" ?>"></a>






	<link href="<?php echo"$base_url"; ?>/assets/css/owl.carousel.css" rel="stylesheet">
   <link href="<?php echo"$base_url"; ?>/assets/css/owl.theme.css" rel="stylesheet">
	<style>
	#owl-demo .item{
		 margin: 3px;
	}
	#owl-demo .item img{
		 display: block;
		 width: 100%;
		 height: auto;
	}
	</style>


	<div id="fb-root"></div>
	<style media="screen">
		#de{position: relative;float: left;}
		#next{color: rgb(0, 0, 0);
position: relative;
float: left;}
#back{color: rgb(0, 0, 0);
position: relative;
float: left;}
	</style>

	<?php
		include "config/func/foot_food.php";
	?>
	<style media="screen">

	</style>
	<script type="text/javascript"></script>
</body>
</html>
