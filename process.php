<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";
include "config/func/jumlah_data.php";
include "config/func/id_masking.php";
$auto_logout=1800000;
if(!empty($_SESSION['food_member'])){
	if (time()-$_SESSION['timestamp']>$auto_logout){
		session_destroy();
		session_unset();
		header("Location: ".$base_url."/auto-logout");
		exit();
	}else{
		$_SESSION['timestamp']=time();
	}
	include "config/func/member_data.php";
}
$active = "process";
$type = "process";
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
  <title>Process F&B - Foodie Guidances</title>
	<meta name="keywords" content="">
  <meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/jquery-ui.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/superslides.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>

	<link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">

	<link rel="prev" title="Page 2" href="<?php echo "$base_url/pages/food" ?>" />
	<link rel="next" title="Page 1" href="<?php echo  "$base_url/pages/recipe" ?>" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
 <body style="position: initial;" class="drawer drawer--right">
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>
	<div class="pencarian">
		<div class="pencari">
			<form method="get" action="<?php echo"$base_url/pages/process/search/"; ?>" id="ale-form" onsubmit="myFunction()">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Search based on Name, Category, Country, State, City..." id="search" name="keyword">
					<span class="input-group-btn">
						<button class="btn btn-danger" type="submit"><i class="fa fa-search"></i></button>
					</span>
				</div><!-- /input-group -->
			</form>
		</div>
		<div id="slides">
			<div class="slides-container">
				<?php
				$sql=mysqli_query($koneksi,"SELECT * FROM slideshow ORDER BY id_slideshow ASC");
				while($w=mysqli_fetch_array($sql)){
					echo"<img src='$base_url/assets/img/slideshow/$w[gambar]' alt='foodie guidances'>";
				}
				?>
			</div>
		</div>
		<?php include "config/inc/pencarian.php"; ?>
	</div>
    <div class="container">
		<div class="row">
			<div class="col-8 col-8a no-padding">

				<div class="col-8" style="width:50%">
					<h4 class="f-merah no-mb mt20"><a href="<?php echo"$base_url/pages/process/search/?sort=r.id_beverage+DESC"; ?>">Latest Process F&B</a></h4>
					<span class="f-abu f-12 blk mb20 mb-j20">Worldwide data collection</span>
				</div>
				<div class="col-8" style="width:50%">
					<h4 class="f-merah no-mb mt20">
						<a href="<?php echo"$base_url/pages/process/search/?keyword="; ?>"><i class="fa fa-search" style="margin-right:5px"></i> Advance Search</a></h4>
					 <span class="f-abu f-12 blk mb20 mb-j20">All Beverage</span>
				</div>
				<div style="clear: both;"></div>

				<div class="media">
					<?php
					// select *,
					// (select p.gambar_process_photo from process_photo p where p.id_process=f.id_process order by p.id_process_photo desc limit 1) as gambar,
					// (SELECT COUNT(h.id_member) FROM beverage_like h WHERE h.id_beverage=f.id_beverage) AS dilike,
					// (select count(id_beverage_rating) as jumlah from beverage_rating where beverage_rating.id_beverage=f.id_beverage) as jumlah_vote,
					// (SELECT (SUM(r.expired) + SUM(r.taste) + SUM(r.freshness) + SUM(r.serving)) / COUNT(r.id_member) FROM process_rating r WHERE r.id_process =f.id_process) AS total 
					// from process f,process_category k, price_index p 
					// where f.categories=k.id_process_category and f.id_price_index=p.id_price_index order by f.id_process desc limit 2

					$sql=mysqli_query($koneksi,"select *, 
					(select p.gambar_process_photo from process_photo p where p.id_process=f.id_process order by p.id_process_photo desc limit 1) as gambar,
					(SELECT COUNT(h.id_member) FROM process_like h WHERE h.id_process=f.id_process) AS dilike, 
					(select count(h.id_process) from process_rekomendasi h where h.id_process=f.id_process) as direkomendasi,
					(select count(id_process_rating) as jumlah from process_rating where process_rating.id_process=f.id_process) as jumlah_vote, 
					(SELECT (SUM(r.expired) + SUM(r.taste) + SUM(r.freshness) + SUM(r.serving)) / COUNT(r.id_member) FROM process_rating r WHERE r.id_process =f.id_process) AS total 
					from process f,process_category k, price_index p where f.categories=k.id_process_category and f.id_price_index=p.id_price_index order by f.id_process desc limit 10");
					while ($b=mysqli_fetch_array($sql)) {
						$gsql=mysqli_query($koneksi,"select p.gambar_process_photo, (select count(l.id_process_photo_like) from process_photo_like l where l.id_process_photo=p.id_process_photo) as total_like from process_photo p where p.id_process='$b[id_process]' order by total_like desc,p.id_process_photo limit 1");
						$g=mysqli_fetch_array($gsql);
						$gambar=$g['gambar_process_photo'];
					$slug=seo($b['nama_process']);
					$post=date("jS M, Y", strtotime($b['date_of_viewed']));
					$total_rating=number_format((float)$b['total'], 2, '.', '');
					$id = id_masking($b['id_process']);
					echo"<div class='row col-4a'>
						<div class='col-8 thumb col-8a no-padding'>
							<a href='$base_url/pages/process/info/$id/$slug'>
								<img data-original='$base_url/assets/img/process/big_$gambar' class='lazy' width='220' height='165'>
							</a>
							<a href='$base_url/pages/process'><span>Process F&B</span></a>
						</div>
						<div class='col-8 info col-8a'>
							<em>posted on $post</em>
							<h3 class='sembunyi'><a href='$base_url/pages/process/info/$id/$slug'>$b[nama_process]</a></h3>
							<ul class='list-unstyled f-12'>
								<li><em>category</em> <a href='$base_url/pages/process/search/?category=$b[id_process_category]'>$b[nama_process_category]</a></li>
								<li><em>cost</em> <a href='$base_url/pages/process/search/?price_index=$b[id_price_index]'>$b[nama_price_index]</a></li>
								<li>$b[dilike] Like <span class='bullet'>&#8226;</span> $b[dilihat] Views </li>
								<li>$b[direkomendasi] Recomendations</li>
							</ul>";
							if($total_rating<>0){
								echo"<div><input type='hidden' class='rating' data-filled='fa fa-star' data-empty='fa fa-star-o' readonly='readonly' value='$total_rating'>&nbsp;&nbsp;&nbsp;&nbsp;<strong class='f-18'>$total_rating</strong>&nbsp;&nbsp;<em>from $b[jumlah_vote] vote</em></div>";
							}
						echo"</div>
					</div>";
						}
					?>
				</div>				
				kita pernah bicara bisnis:
				1. website
				2. Design
				3. Ternak Lele, bebek, ayam Dll
				4. Import
				5. Produksi kripik pisan DLL
				6. Olshop Jaknot, BL, toped
				7. 
			</div>
			<div class="menu-bawah">
				<a href="<?php echo"$base_url/pages/process/search/?sort=total+DESC"; ?>">World Top Process F&B</a>
			</div>
			<div class="col-4 mene-atas">
				<h4 class="f-merah no-mb mt20"><a href="<?php echo"$base_url/pages/process/search/?sort=total+DESC"; ?>">World Top Process Food</a></h4>
				<span class="f-abu f-12 blk mb10">Worldwide data collection</span>
				<ol class="list-number">
					<?php
					$top=mysqli_query($koneksi,"select *, (select p.gambar_process_photo from process_photo p where p.id_process=f.id_process order by p.id_process_photo desc limit 1) as gambar,(SELECT COUNT(h.id_member) FROM process_like h WHERE h.id_process=f.id_process) AS dilike, (select count(id_process_rating) as jumlah from process_rating where process_rating.id_process=f.id_process) as jumlah_vote, (SELECT (SUM(r.expired) + SUM(r.taste) + SUM(r.freshness) + SUM(r.serving)) / COUNT(r.id_member) FROM process_rating r WHERE r.id_process =f.id_process) AS total from process f,process_category k, price_index p where f.categories=k.id_process_category and f.id_price_index=p.id_price_index and f.type_process='1' ORDER BY `total`  DESC limit 10");
					while($t=mysqli_fetch_array($top)){
						if($t['total']<>0){
							$slug=seo($t['nama_process']);
							$id = id_masking($t['id_process']);
							echo"<li><h4><a href='$base_url/pages/process/info/$id/$slug'>$t[nama_process]</a></h4><a href='$base_url/pages/process/search/?category=$t[id_process_category]' class='f-12 f-merah'>$t[nama_process_category]</a></li>";
						}
					}
					?>
				</ol>

				<h4 class="f-merah no-mb mt20"><a href="<?php echo"$base_url/pages/process/search/?sort=total+DESC"; ?>">World Top Process Beverage</a></h4>
				<span class="f-abu f-12 blk mb10">Worldwide data collection</span>
				<ol class="list-number">
					<?php
					$top=mysqli_query($koneksi,"select *, (select p.gambar_process_photo from process_photo p where p.id_process=f.id_process order by p.id_process_photo desc limit 1) as gambar,(SELECT COUNT(h.id_member) FROM process_like h WHERE h.id_process=f.id_process) AS dilike, (select count(id_process_rating) as jumlah from process_rating where process_rating.id_process=f.id_process) as jumlah_vote, (SELECT (SUM(r.expired) + SUM(r.taste) + SUM(r.freshness) + SUM(r.serving)) / COUNT(r.id_member) FROM process_rating r WHERE r.id_process =f.id_process) AS total from process f,process_category k, price_index p where f.categories=k.id_process_category and f.id_price_index=p.id_price_index and f.type_process='2' ORDER BY `total`  DESC limit 10");
					while($t=mysqli_fetch_array($top)){
						if($t['total']<>0){
							$slug=seo($t['nama_process']);
							$id = id_masking($t['id_process']);
							echo"<li><h4><a href='$base_url/pages/process/info/$id/$slug'>$t[nama_process]</a></h4><a href='$base_url/pages/process/search/?category=$t[id_process_category]' class='f-12 f-merah'>$t[nama_process_category]</a></li>";
						}
					}
					?>
				</ol>


				<em class="blk mb10 f-12">The ranking are obtain from our members reviews based on the average rating out of 5 stars</em>
				<h4 class="f-merah no-mb mt30">Foodie's Video</h4>
				<span class="f-abu f-12 blk mb10">Popular Foodie's Video</span>
				<?php
				$sql=mysqli_query($koneksi,"select * from video order by RAND() desc limit 1");
				while($v=mysqli_fetch_array($sql)){
					$url = $v['url'];
					preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
					$id = $matches[1];
					echo "<iframe class='mb20' type='text/html' width='220' height='123' src='https://www.youtube.com/embed/$id?rel=0&showinfo=0&color=white&iv_load_policy=3' frameborder='0' allowfullscreen></iframe>";
				}
				?>

			
			</div>
			<div class="col-4 mene-atas">
				<?php include"config/inc/search.php"; ?>
				<?php include"config/inc/iklan.php"; ?>
				<div class="fb-like-box" data-href="https://www.facebook.com/foodieguidances#_=_" data-width="220" data-height="350" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
			</div>
		</div>
    </div>
    <?php include"config/inc/footer.php"; ?>
	 <a class="scrollToNext" href="<?php echo "$base_url/pages/food" ?>" style="bottom: 320px;"><img src="<?php echo"$base_url/assets/img/theme/kiri.png" ?>"></a>
	 <a class="scrollToNext" href="<?php echo  "$base_url/pages/recipe" ?>" style="bottom: 273px;"><img src="<?php echo"$base_url/assets/img/theme/kanan.png" ?>"></a>
	 <a href="#" class="scrollToTop"><img src="<?php echo"$base_url/assets/img/theme/atas.png" ?>"></a>

	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery-ui.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap-rating.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.superslides.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.lazyload.min.js"></script>

	<script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
   <script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
   <script>
      $(document).ready(function() {
	      $('.drawer').drawer();

			$('.tutup').click(function(){
				$('.iklanan').toggleClass('hilang', 500);
			});

			$('.cart').click(function(e){
				e.stopPropagation();
				$('.dis-cart').toggle();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$('.plus').click(function(e){
				e.stopPropagation();
				$('.dis-plus').toggle();
				$('.dis-cart').hide();
				$('.dis-share').hide();
			});
			$('.share').click(function(e){
				e.stopPropagation();
				$('.dis-share').toggle();
				$('.dis-plus').hide();
				$('.dis-cart').hide();
			});
			$('.drawer-toggle').click(function(){
				$('.dis-cart').hide();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$(document).click(function () {
				 var $el = $(".dis-share");
				 if ($el.is(":visible")) {
					  $el.fadeOut(200);
				 }
				 var $ela = $(".dis-plus");
				 if ($ela.is(":visible")) {
					  $ela.fadeOut(200);
				 }
				 var $elu = $(".dis-cart");
				 if ($elu.is(":visible")) {
					  $elu.fadeOut(200);
				 }
		   });

      });
   </script>

	<?php if(!empty($_SESSION['food_member'])){ ?>
		<script src="<?php echo"$base_url"; ?>/assets/js/theme.js"></script>
	<?php } ?>
	<script type="text/javascript">
		$(document).ready(function () {
			$(document).on("contextmenu",function(e){
				if(e.target.nodeName != "INPUT" && e.target.nodeName != "TEXTAREA")
				e.preventDefault();
			});
			$.fn.disableTextSelect = function() {
				return this.each(function() {
					$(this).css({
						'MozUserSelect':'none',
						'webkitUserSelect':'none'
					}).attr('unselectable','on').bind('selectstart', function() {
						return false;
					});
				});
			};
			$('body').disableTextSelect();
		});
	</script>

	<script type="text/javascript">
	$(document).ready(function() {
	$(window).scroll(function(){
		 if ($(this).scrollTop() > 100) {
			 $('.scrollToTop').fadeIn();
		 } else {
			 $('.scrollToTop').fadeOut();
		 }
	});

	$(window).scroll(function(){
		if ($(this).scrollTop() > 50) {
			$('.scrollToNext').fadeIn();
		} else {
			$('.scrollToNext').fadeOut();
		}
	});

	  $('#go-to-vote').click(function() {
		  $('html, body').animate({
			  scrollTop: $( $(this).attr('href') ).offset().top
		  }, 500);
		  $('#tab-vote a[href="#feature"]').tab('show');
		  return false;
	  });
	});
	</script>
	<div id="fb-root"></div>
	<script>
	$(document).ready(function () {
		$("img.lazy").lazyload({
			effect : "fadeIn"
		});
		$('#slides').superslides({
			animation: 'slide',
			inherit_height_from: '.pencarian',
			play: 8000,
			pagination: true
		});
		$('.rating').rating();
		$('.carousel').carousel({
			interval: 10000
		});
		$('#search').autocomplete({
			source: "<?php echo "$base_url/config/func/ajax_beverage_search.php"; ?>",
			minLength: 3
		});
	});
	/*=====For developer to config ====*/
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=348272391978609&version=v2.0";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script>
</body>
</html>
