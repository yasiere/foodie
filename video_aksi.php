<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";	
include "config/func/id_masking.php";
date_default_timezone_set("Asia/Jakarta");
$sekarang=date("Y-m-d");
$id = id_masking($_GET['id']);
$r=mysqli_fetch_array(mysqli_query($koneksi,"select id_video, nama_video from video where id_video='$id'"));
$seo=seo($r['nama_video']);
if(isset($_SESSION['food_member'])){
	if($_GET['fungsi']=="1"){
		$ada=mysqli_num_rows(mysqli_query($koneksi,"select id_video_like from video_like where id_video='$id' and id_member='$_SESSION[food_member]' and tgl_like='$sekarang'"));
		if($ada==0){
			mysqli_query($koneksi,"insert into video_like (id_member,id_video,tgl_like) values('$_SESSION[food_member]','$id','$sekarang')");
			$_SESSION['resto_notif']     = "suka";
		}
		else{
			$_SESSION['resto_notif']     = "suka_gagal";
		}
	}
	elseif($_GET['fungsi']=="3"){
		$ada_bookmark=mysqli_num_rows(mysqli_query($koneksi,"select id_bookmark from bookmark where id='$id' and jenis='video' and id_member='$_SESSION[food_member]'"));
		if($ada_bookmark==0){
			mysqli_query($koneksi,"insert into bookmark (id_member,jenis,id,tgl_bookmark) values('$_SESSION[food_member]','video','$id','$sekarang')");
			$_SESSION['resto_notif']     = "bookmark";
		}	
		else{
			$_SESSION['resto_notif']     = "bookmark_gagal";
		}
	}
	elseif($_GET['fungsi']=="5"){
		$ada_report=mysqli_num_rows(mysqli_query($koneksi,"select id_video_report from video_report where id_video='$id' and id_member='$_SESSION[food_member]'"));
		if($ada_report==0){
			mysqli_query($koneksi,"insert into video_report (id_member,id_video,tgl_report) values('$_SESSION[food_member]','$id','$sekarang')");
			$_SESSION['resto_notif']     = "lapor";
		}
		else{
			$_SESSION['resto_notif']     = "lapor_gagal";
		}
	}
}
else{
	$_SESSION['resto_notif']     = "login_dulu";
}
header("Location: ".$base_url."/pages/video/".$_GET['id']."/".$seo);
?>