<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";
include "config/func/jumlah_data.php";
include "config/func/id_masking.php";
$auto_logout=1800000;
if(!empty($_SESSION['food_member'])){
	if (time()-$_SESSION['timestamp']>$auto_logout){
		session_destroy();
		session_unset();
		header("Location: ".$base_url."/auto-logout");
		exit();
	}else{
		$_SESSION['timestamp']=time();
	}
	include "config/func/member_data.php";
}
$id = id_masking($_GET['id']);
$active="video";
$type="video_detail";
mysqli_query($koneksi,"update video set dilihat=dilihat + 1 where id_video='$id'");
$resto=mysqli_query($koneksi,"select *,(select count(h.id_video) from video_like h where h.id_video=f.id_video) as dilike from video f LEFT JOIN member m ON f.id_member=m.id_member WHERE f.id_video='$id'");
$r=mysqli_fetch_array($resto);
$slug=seo($r['nama_video']);
$post=date("jS M, Y", strtotime($r['tgl_post']));
$url = $r['url'];
preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
$id_video = $matches[1];
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
    <title>Video Detail - Foodie Guidances</title>
	<meta name="keywords" content="">
    <meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<meta property="og:title" content="<?php echo"$r[nama_video]"; ?>" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="<?php echo"$base_url/pages/video/info/$_GET[id]/$slug"; ?>" />
	<meta property="og:image" content="<?php echo"http://img.youtube.com/vi/$id_video/default.jpg"; ?>" />
	<meta property="og:description" content="<?php echo"$r[nama_video]"; ?>" />
	<meta property="og:site_name" content="Foodie Guidances" />
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/lightbox.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/slick.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>
	<link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
 <body style="position: initial;" class="drawer drawer--right">
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>
    <div class="container hook">
		<div class="row">
			<div class="col-12 col-8a">
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Video</a></li>
					<li class="active"><?php echo"$r[nama_video]"; ?></li>
				</ol>
				<?php
					if(isset($_SESSION['resto_notif'])){
						if($_SESSION['resto_notif']=="suka"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully like this video.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="suka_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was liked this video before. You can only do one time in 24 hours.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="bookmark"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully bookmark this video.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="bookmark_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was bookmark this video before. You can only do one time.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="lapor"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully report this video.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="lapor_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was reported this video before. You can only do one time.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="login_dulu"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Must Login!</strong> You must login or register before doing this action.
							</div>";
						}
						unset($_SESSION['resto_notif']);
					}
				?>
				<h1 class="judul"><?php echo"$r[nama_video]"; ?></h1>
				<div class="mb10 f-12">
					<a href="<?php echo"$base_url/$r[username]"; ?>"><em>by</em> <?php if(!empty($r['nama_depan'])){echo"$r[nama_depan] $r[nama_belakang]";}else{echo"Admin";} ?></a>
					<span class="bullet">&#8226;</span> <em>post</em> <?php echo"$post"; ?></div>
				<div class="box-info-1">
					<div class="pull-left">
						<?php
						echo "<iframe class='mb20' type='text/html' width='100%' height='300px' src='https://www.youtube.com/embed/$id_video?rel=0&showinfo=0&color=white&iv_load_policy=3' frameborder='0' allowfullscreen></iframe>";
						?>
					</div>
					<div class="pull-right">
						<div class="detail-menu">
							<a href="<?php echo"$base_url/pages/video/event/$_GET[id]/1"; ?>" title="Like"><i class="fa fa-heart-o"></i></a>
							<a href="<?php echo"$base_url/pages/video/event/$_GET[id]/3"; ?>" title="Bookmark"><i class="fa fa-bookmark-o"></i></a>
							<span class="social-share"><i class="fa fa-share-alt"></i>
								<div class="social-box">
									<span class='st_facebook_large' displayText='Facebook'></span>
									<span class='st_linkedin_large' displayText='LinkedIn'></span>
									<span class='st_pinterest_large' displayText='Pinterest'></span>
									<span class='st_googleplus_large' displayText='Google +'></span>
									<span class='st_twitter_large' displayText='Tweet'></span>
									<span class='st_sharethis_large' displayText='ShareThis'></span>
								</div>
							</span>
							<a href="<?php echo"$base_url/pages/video/event/$_GET[id]/5"; ?>" title="Report"><i class="fa fa-flag-o"></i></a>
						</div>
						<div class="statistik">
							<em>
								<i class="fa fa-heart"></i>
								<a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">
									<?php	echo"$r[dilike] likes"; ?>
								</a>
								<?php
								if(!empty($_SESSION['food_member'])){
									$me=mysqli_query($koneksi,"select tgl_like from video_like where id_member='$id_member' and id_video='$id' order by id_video_like desc limit 1");
									$ada_melike=mysqli_num_rows($me);
									if($ada_melike<>0){
										$q=mysqli_fetch_array($me);
										$like_date=date("jS M, Y", strtotime($q['tgl_like']));
										echo" (you has liked on $like_date)";
									}
								}
								?>
							</em>
							<em><i class="fa fa-eye"></i> <?php echo"$r[dilihat]"; ?> views</em>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<hr>
				<h5 class="f-merah">Other Video</h5>
				<div class="media">
					<div class="row">
						<?php
						$sql=mysqli_query($koneksi,"select *,(SELECT COUNT(h.id_member) FROM video_like h WHERE h.id_video=f.id_video) AS dilike from video f left join video_category v on f.id_video_category=v.id_video_category where f.id_video_category='$r[id_video_category]' and f.id_video<>'$id' order by f.dilihat desc limit 4");
						$ada_resto=mysqli_num_rows($sql);
						if($ada_resto<>0){
							while($s=mysqli_fetch_array($sql)){
								$url = $s['url'];
								preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
								$id_video = $matches[1];
								$post=date("jS M, Y", strtotime($s['tgl_post']));
								$slug=seo($s['nama_video']);
								$id = id_masking($s['id_video']);
								echo"<div class='col-4'>
									<div class='thumb'>
										<a href='$base_url/pages/video/$id/$slug'><img data-original='http://img.youtube.com/vi/$id_video/default.jpg' class='lazy' width='160' height='120'></a>
									</div>
									<div class='info'>
										<em>posted on $post</em>
										<h3 class='sembunyi'><a href='$base_url/pages/video/$id/$slug'>$s[nama_video]</a></h3>
										<ul class='list-unstyled f-12'>
											<li>$s[dilike] Like <span class='bullet'>&#8226;</span> $s[dilihat] View</li>
										</ul>
									</div>
								</div>";
							}
						}
						else{
							echo"<div class='col-12'>There is no other video right now.</div>";
						}
						?>
					</div>
				</div>
				<hr>
				<div id="disqus_thread"></div>
			</div>
			<div class="col-4 mene-atas">
				<?php include"config/inc/search.php"; ?>
				<?php include"config/inc/iklan.php"; ?>
				<div class="fb-like-box" data-href="https://www.facebook.com/foodieguidances#_=_" data-width="220" data-height="350" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
			</div>
		</div>
    </div>
    <?php include"config/inc/footer.php"; ?>
	 <?php
 		  $idu = id_masking($_GET['id']);
 			  $pb=mysqli_query($koneksi, "select * from video where id_video < $idu ORDER BY id_video desc limit 1");
 		  $jumpb=mysqli_num_rows($pb);
 		  $tpb=mysqli_fetch_array($pb);
 		  if ($jumpb<>0) {
 			  $ba = id_masking($tpb['id_video']);
 			  $slug=seo($tpb['nama_video']);
 			  $next="$base_url/pages/video/$ba/$slug";
 		  } else {
 			  $pba=mysqli_query($koneksi, "select * from video ORDER BY id_video desc limit 1");
 			  $jumpba=mysqli_num_rows($pba);
 			  $tpba=mysqli_fetch_array($pba);
 			  $baa = id_masking($tpba['id_video']);
 			  $sluga=seo($tpba['nama_video']);
 			  $next="$base_url/pages/video/$baa/$sluga";
 		  }

 		  $pn=mysqli_query($koneksi, "select * from video where id_video > $idu ORDER BY id_video limit 1");
 		  $jumpn=mysqli_num_rows($pn);
 		  $tpn=mysqli_fetch_array($pn);
 		  if ($jumpn<>0) {
 			  $na = id_masking($tpn['id_video']);
 			  $slug=seo($tpn['nama_video']);
 			  $back="$base_url/pages/video/$na/$slug";
 		  } else {
 			  $pna=mysqli_query($koneksi, "select * from video ORDER BY id_video limit 1");
 			  $jumpna=mysqli_num_rows($pna);
 			  $tpna=mysqli_fetch_array($pna);
 			  $naa = id_masking($tpna['id_video']);
 			  $sluga=seo($tpna['nama_video']);
 			  $back="$base_url/pages/video/$naa/$sluga";
 		  }
 		?>
 		<a class="scrollToNext" href="<?php echo $back ?>" style="bottom: 320px;"><img src="<?php echo"$base_url/assets/img/theme/kiri.png" ?>"></a>
 		<a class="scrollToNext" href="<?php echo $next ?>" style="bottom: 273px;"><img src="<?php echo"$base_url/assets/img/theme/kanan.png" ?>"></a>
 		<a href="#" class="scrollToTop"><img src="<?php echo"$base_url/assets/img/theme/atas.png" ?>"></a>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/lightbox.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.flexslider-min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.lazyload.min.js"></script>
	<?php if(!empty($_SESSION['food_member'])){ ?>
		<script src="<?php echo"$base_url"; ?>/assets/js/theme.js"></script>
	<?php } ?>
	<script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
   <script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
   <script>
      $(document).ready(function() {
	      $('.drawer').drawer();

			$('.tutup').click(function(){
				$('.iklanan').toggleClass('hilang', 500);
			});

			$('.cart').click(function(e){
				e.stopPropagation();
				$('.dis-cart').toggle();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$('.plus').click(function(e){
				e.stopPropagation();
				$('.dis-plus').toggle();
				$('.dis-cart').hide();
				$('.dis-share').hide();
			});
			$('.share').click(function(e){
				e.stopPropagation();
				$('.dis-share').toggle();
				$('.dis-plus').hide();
				$('.dis-cart').hide();
			});
			$('.drawer-toggle').click(function(){
				$('.dis-cart').hide();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$(document).click(function () {
				 var $el = $(".dis-share");
				 if ($el.is(":visible")) {
					  $el.fadeOut(200);
				 }
				 var $ela = $(".dis-plus");
				 if ($ela.is(":visible")) {
					  $ela.fadeOut(200);
				 }
				 var $elu = $(".dis-cart");
				 if ($elu.is(":visible")) {
					  $elu.fadeOut(200);
				 }
		   });
      });
   </script>
	<script type="text/javascript">
		$(document).ready(function () {
			$(document).on("contextmenu",function(e){
				if(e.target.nodeName != "INPUT" && e.target.nodeName != "TEXTAREA")
				e.preventDefault();
			});
			$.fn.disableTextSelect = function() {
				return this.each(function() {
					$(this).css({
						'MozUserSelect':'none',
						'webkitUserSelect':'none'
					}).attr('unselectable','on').bind('selectstart', function() {
						return false;
					});
				});
			};
			$('body').disableTextSelect();
		});
	</script>
	<script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
	$(window).scroll(function(){
		 if ($(this).scrollTop() > 100) {
			 $('.scrollToTop').fadeIn();
		 } else {
			 $('.scrollToTop').fadeOut();
		 }
	});

	$(window).scroll(function(){
		if ($(this).scrollTop() > 50) {
			$('.scrollToNext').fadeIn();
		} else {
			$('.scrollToNext').fadeOut();
		}
	});

	  $('#go-to-vote').click(function() {
		  $('html, body').animate({
			  scrollTop: $( $(this).attr('href') ).offset().top
		  }, 500);
		  $('#tab-vote a[href="#feature"]').tab('show');
		  return false;
	  });
	});
	</script>
	<script type="text/javascript">stLight.options({publisher: "7b97d330-d7b9-49c3-b5ee-b3aaa89bbe66", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=348272391978609&version=v2.0";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
	<script type="text/javascript">
		$(document).ready(function () {
			$("img.lazy").lazyload({
				effect : "fadeIn"
			});
			$(".alert").fadeTo(5000, 500).fadeOut(500, function(){
				$(".alert").alert('close');
			});
			$('.social-share').click( function(event){
				event.stopPropagation();
				$( ".social-box" ).toggle(1);
			});
			$('.carousel').carousel({
				interval: 10000
			});
			$(document).click( function(){
				$('.social-box').hide();
			});
			/* * * CONFIGURATION VARIABLES * * */
			var disqus_shortname = 'foodieguidancescom';

			/* * * DON'T EDIT BELOW THIS LINE * * */
			(function() {
				var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
				dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
				(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
			})();
		});
	</script>
	<style media="screen">
	.black_overlay{
		display: none;
		position: absolute;
		top: 0%;
		left: 0%;
		width: 100%;
		height: 100%;
		background-color: black;
		z-index:1001;
		-moz-opacity: 0.8;
		opacity:.80;
		filter: alpha(opacity=80);
	}
	.white_content {
		display: none;
		position: fixed;
		top: 20%;
		left: 40%;
		width: 20%;
		height: 56%;
		padding: 24px;
		border: 5px solid #C00606;
		background-color: #FFF;
		z-index: 1002;
		overflow-y: auto;
		overflow-x: hidden;
	}
	</style>
	<div id="light" class="white_content">
		<h3 style="font-size: 20px; margin: 0px 0px 15px;">Like</h3>
		<table class="table">
			<?php
				$idu=id_masking($_GET['id']);
				$me=mysqli_query($koneksi,"SELECT * FROM video_like r left join member m on m.id_member=r.id_member where r.id_video='$idu'");
				$no=1;
				$juma=mysqli_num_rows($me);
				if (!empty($juma)) {
					while ($tame=mysqli_fetch_array($me)) {
						$like_date=date("jS M, Y", strtotime($tame['tgl_like']));
							echo "<tr><td>$no.</td>
										<td><img src='$base_url/assets/img/member/$tame[gambar_thumb]' class='foto_profil' width='30px'></td> <td>$tame[username]</td> <td>$like_date</td></tr>";
						$no++;
					}
				}
				else {
					echo "Empty";
				}
			?>
		</table>
		<a style="position: absolute; border: 6px solid rgb(255, 255, 255); right: -6px; top: -6px; z-index: 999999999; background: #C00606; color: rgb(255, 255, 255); padding: 6px 10px;" href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'"><i class="fa fa-close"></i></a>
	</div>
	<div id="lighta" class="white_content">
		<h3 style="font-size: 20px; margin: 0px 0px 15px;">Recommendation</h3>
		<table class="table">
			<?php
				$idu=id_masking($_GET['id']);
				$me=mysqli_query($koneksi,"SELECT * FROM recipe_rekomendasi r left join member m on m.id_member=r.id_member where r.id_recipe='2'");
				$no=1;
				$juma=mysqli_num_rows($me);
				if (!empty($juma)) {
					while ($tame=mysqli_fetch_array($me)) {
						$like_date=date("jS M, Y", strtotime($tame['tgl_rekomendasi']));
							echo "<tr><td>$no.</td>
										<td><img src='$base_url/assets/img/member/$tame[gambar_thumb]' class='foto_profil' width='30px'></td> <td>$tame[username]</td> <td>$like_date</td></tr>";
						$no++;
					}
				}
				else {
					echo "Empty";
				}
			?>
		</table>
		<a style="position: absolute; border: 6px solid rgb(255, 255, 255); right: -6px; top: -6px; z-index: 999999999; background: #C00606; color: rgb(255, 255, 255); padding: 6px 10px;" href = "javascript:void(0)" onclick = "document.getElementById('lighta').style.display='none';document.getElementById('fadea').style.display='none'"><i class="fa fa-close"></i></a>
	</div>
	<div id="fade" class="black_overlay"  onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'"></div>
	<div id="fadea" class="black_overlay"  onclick = "document.getElementById('lighta').style.display='none';document.getElementById('fadea').style.display='none'"></div>
</body>
</html>
