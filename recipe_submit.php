<?php
session_start();
$auto_logout=1800000;
include "config/func/base_url.php";
if(!empty($_SESSION['food_member'])){
if (time()-$_SESSION['timestamp']>$auto_logout){
    session_destroy();
    session_unset();
	header("Location: ".$base_url."/auto-logout");
	exit();
}else{
    $_SESSION['timestamp']=time();
}
include "config/database/db.php";
include "config/func/member_data.php";
$active = "member";
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
    <title>Submit Recipe - Foodie Guidances</title>
	<meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/select2.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/awesome-bootstrap-checkbox.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>
   <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
  <script>tinymce.init({ selector:'textarea' });</script>
  <link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
 <body style="position: initial;" class="drawer drawer--right">
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>
   <div class="loadi">
      <img src="<?php echo "$base/assets/img/theme/load.gif"?>">
   </div>
    <div class="container hook">
		<div class="row">
			<div class="col-16">
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Account</a></li>
					<li class="active">Submit Recipe</li>
				</ol>
				<?php
				if(isset($_SESSION['notif'])){
					if($_SESSION['notif']=="gambar"){
						echo"<div class='alert alert-danger alert-dismissible' role='alert'>
							<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
							<strong>Failed!</strong> Image used have to be in JPG, PNG, and GIF format and the maximum total images size is 50MB also minimal dimension must be 800x600 pixel.
						</div>";
					}
					unset($_SESSION['notif']);
				}
				?>
				<h3 class="f-merah mb10">Submit Recipe</h3>
				<p>*required field</p>
				<form class="border-form form-horizontal" method="post" action="<?php echo"$base_url"; ?>/config/func/save_recipe.php" enctype="multipart/form-data">
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Recipe Title <span class="f-merah">*</span></label></div>
						<div class="col-6 col-4b">
							<input type="text" class="form-control" placeholder="Write Name" id="recipe" name="recipe_name" required maxlength="50">
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Cover Photo <span class="f-merah">*</span></label></div>
						<div class="col-6 col-4b">
							<div class="fileinput fileinput-new" data-provides="fileinput">
							  <div class="fileinput-preview upload" data-trigger="fileinput"></div>
							  <input type="file" name="gambar" class="hidden" id="gam">
							</div>
							<p class="help-block">Image used have to be in JPG, PNG, and GIF format and the maximum total images size is 50MB also minimal dimension must be 800x600 pixel.</p>
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Category <span class="f-merah">*</span></label></div>
						<div class="col-6 col-4b">
							<select name="category" required class="form-control" id="kat">
								<option value="">Select Category</option>
								<?php
								$sql=mysqli_query($koneksi,"select * from recipe_category order by nama_recipe_category asc");
								while($a=mysqli_fetch_array($sql)){
									echo"<option value='$a[id_recipe_category]'>$a[nama_recipe_category]</option>";
								}
								?>
							</select>
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Cuisine <span class="f-merah">*</span></label></div>
						<div class="col-6 col-4b">
							<select name="cuisine" required class="form-control" id="cuisine">
								<option value="">Select Cuisine</option>
								<?php
								$sql=mysqli_query($koneksi,"select * from cuisine order by nama_cuisine asc");
								while($f=mysqli_fetch_array($sql)){
									echo"<option value='$f[id_cuisine]'>$f[nama_cuisine]</option>";
								}
								?>
							</select>
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Serving Pax</label></div>
						<div class="col-6 col-4b">
							<input type="text" class="form-control" placeholder="ex: 1 pax - 2 pax" name="serving_pax">
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Dish</label></div>
						<div class="col-6 col-4b">
							<input type="text" class="form-control" placeholder="Write Dish" name="dish">
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">MSG Level</label></div>
						<div class="col-6 col-4b">
							<select name="msg_level" class="form-control">
								<option>Select MSG Level</option>
								<?php
								$sql=mysqli_query($koneksi,"select * from msg_level order by nama_msg_level asc");
								while($m=mysqli_fetch_array($sql)){
									echo"<option value='$m[id_msg_level]'>$m[nama_msg_level]</option>";
								}
								?>
							</select>
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Difficulty</label></div>
						<div class="col-6 col-4b">
							<select name="difficulty" class="form-control">
								<?php
								$sql=mysqli_query($koneksi,"select * from difficulty order by nama_difficulty asc");
								while($d=mysqli_fetch_array($sql)){
									echo"<option value='$d[id_difficulty]'>$d[nama_difficulty]</option>";
								}
								?>
							</select>
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Cooking Methode</label></div>
						<div class="col-6 col-4b">
							<select name="cooking_methode" class="form-control">
								<option>Select Cooking Methode</option>
								<?php
								$sql=mysqli_query($koneksi,"select * from cooking_methode order by nama_cooking_methode asc");
								while($e=mysqli_fetch_array($sql)){
									echo"<option value='$e[id_cooking_methode]'>$e[nama_cooking_methode]</option>";
								}
								?>
							</select>
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Duration</label></div>
						<div class="col-6 col-4b">
							<select name="duration" class="form-control">
								<?php
								$sql=mysqli_query($koneksi,"select * from duration order by nama_duration asc");
								while($g=mysqli_fetch_array($sql)){
									echo"<option value='$g[id_duration]'>$g[nama_duration]</option>";
								}
								?>
							</select>
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Pork Serving</label></div>
						<div class="col-12 col-4b">
							<div class="radio radio-inline radio-danger">
								<input type="radio" checked="" name="pork" value="Yes" id="pork1">
								<label for="pork1"> Yes </label>
							</div>
							<div class="radio radio-inline radio-danger">
								<input type="radio" name="pork" value="No" id="pork2">
								<label for="pork2"> No </label>
							</div>
							<div class="radio radio-inline radio-danger">
								<input type="radio" name="pork" value="Halal" id="pork3">
								<label for="pork3"> Halal </label>
							</div>
							<div class="radio radio-inline radio-danger">
								<input type="radio" name="pork" value="Vegetarian" id="pork4">
								<label for="pork4"> Vegetarian </label>
							</div>
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Ingredients <span class="f-merah">*</span></label></div>
						<div class="col-12 col-8a">
							<textarea class="form-control mb5" name="ingredient" rows="6" maxlength="10000" placeholder="Write Ingredients and Amount" required id="word1"></textarea>
							<p class="text-right help-block">You have <span id="counter1"></span> characters left.</p>
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Seasoning</label></div>
						<div class="col-12 col-8a">
							<textarea class="form-control mb5" name="seasoning" rows="6" maxlength="10000" placeholder="Write Seasoning" id="word2"></textarea>
							<p class="text-right help-block">You have <span id="counter2"></span> characters left.</p>
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Way of Cooking</label></div>
						<div class="col-12 col-8a">
							<textarea class="form-control mb5" name="way_cooking" rows="6" placeholder="Write Way of Cooking" id="word3"></textarea>
							<p class="text-right help-block">You have <span id="counter3"></span> characters left.</p>
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Smart Tips</label></div>
						<div class="col-12 col-8a">
							<textarea class="form-control mb5" name="smart_tips" rows="6" maxlength="10000" placeholder="Write Smart Tips" id="word4"></textarea>
							<p class="text-right help-block">You have <span id="counter4"></span> characters left.</p>
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Reference</label></div>
						<div class="col-12 col-8a">
							<textarea class="form-control mb5" name="reference" rows="6" maxlength="10000" placeholder="Write Reference" id="word5"></textarea>
							<p class="text-right help-block">You have <span id="counter5"></span> characters left.</p>
						</div>
					</div>
					<button type="submit" class="btn btn-danger">Submit</button> <button type="reset" class="btn btn-danger">Reset</button>
				</form>
			</div>
		</div>
    </div>
    <?php include"config/inc/footer.php"; ?>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jasny-bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/select2.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.simplyCountable.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/theme.js"></script>
  <script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
	<script>
		 $(document).ready(function() {
			 $('.drawer').drawer();

		 $('.tutup').click(function(){
			 $('.iklanan').toggleClass('hilang', 500);
		 });

		 $('.cart').click(function(e){
			 e.stopPropagation();
			 $('.dis-cart').toggle();
			 $('.dis-plus').hide();
			 $('.dis-share').hide();
		 });
		 $('.plus').click(function(e){
			 e.stopPropagation();
			 $('.dis-plus').toggle();
			 $('.dis-cart').hide();
			 $('.dis-share').hide();
		 });
		 $('.share').click(function(e){
			 e.stopPropagation();
			 $('.dis-share').toggle();
			 $('.dis-plus').hide();
			 $('.dis-cart').hide();
		 });
		 $('.drawer-toggle').click(function(){
			 $('.dis-cart').hide();
			 $('.dis-plus').hide();
			 $('.dis-share').hide();
		 });
		 $('.btn-cari').click(function(){
			 $('.search_kiri').toggleClass('search_kiri_tam', 500);
		 });
		 $(document).click(function () {
				var $el = $(".dis-share");
				if ($el.is(":visible")) {
					 $el.fadeOut(200);
				}
				var $ela = $(".dis-plus");
				if ($ela.is(":visible")) {
					 $ela.fadeOut(200);
				}
				var $elu = $(".dis-cart");
				if ($elu.is(":visible")) {
					 $elu.fadeOut(200);
				}
			});

		 });
	</script>
	<script type="text/javascript">
	$(document).ready(function () {
		$("select").select2();
		$(".alert").fadeTo(5000, 500).fadeOut(500, function(){
			$(".alert").alert('close');
		});
		$('#word1').simplyCountable({
		    counter: '#counter1',
			maxCount: 10000,
			strictMax: true
		});
		$('#word2').simplyCountable({
		    counter: '#counter2',
			maxCount: 10000,
			strictMax: true
		});
		$('#word3').simplyCountable({
		    counter: '#counter3',
			maxCount: 10000,
			strictMax: true
		});
		$('#word4').simplyCountable({
		    counter: '#counter4',
			maxCount: 10000,
			strictMax: true
		});
		$('#word5').simplyCountable({
		    counter: '#counter5',
			maxCount: 10000,
			strictMax: true
		});
	});
	</script>
   <script type="text/javascript">
      $("form").submit(function( event ) {
      	var recipe = $("#recipe").val();
         var kat = $("#kat").val();
         var gam = $("#gam").val();
      	var cuisine= $("#cuisine").val();
      	var word1= $("#word1").val();

         if (!(recipe === "") && !(cuisine === "") && !(word1 === "") && !(kat === "") && !(gam === "")) {
            $(".loadi").show();
         }
         else {

         }
      });
	</script>
</body>
</html>
<?php
}
else{
	header("Location: ".$base_url."/login-area");
}
?>
