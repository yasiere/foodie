<?php
session_start();
$auto_logout=1800000;

if(!empty($_SESSION['food_member'])){
if (time()-$_SESSION['timestamp']>$auto_logout){
    session_destroy();
    session_unset();
	header("Location: ".$base_url."/auto-logout");
	exit();
}else{
    $_SESSION['timestamp']=time();
}
include "config/database/db.php";
include "config/func/member_data.php";

$active = "member";

?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
    <title>Notification - Foodie Guidances</title>
	<meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<?php
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";
include "config/func/jumlah_data.php";
include "config/func/id_masking.php"; ?>
 <body style="position: initial;" class="drawer drawer--right">
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>
    <div class="container hook">
		<div class="row">
			<div class="col-16">
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Account</a></li>
					<li class="active">Notification</li>
				</ol>
				<?php
				if(isset($_GET['info'])){
					if($_GET['info']=="restaurant"){
                  $id = id_masking($_GET['id']);
                  $resto=mysqli_query($koneksi,"SELECT * FROM restaurant where id_restaurant='$_GET[id]'");
                  $r=mysqli_fetch_array($resto);
                  $slug=seo($r['restaurant_name']);
						echo"<div class='alert alert-success' role='alert'>Success! New Restaurant review has been saved.</div>";
                  echo "
                     <h3 class='f-merah mb10'>What will you do next?</h3>
                     <div class='row mb10'>
                        <div class='col-4 col-4b'><a href='#' onclick='goBack()' class='btn btn-block btn-success'>Back</a></div>
                        <div class='col-4 col-4b'></div>
                        <div class='col-4 col-4b'></div>
                        <div class='col-4 col-4b'><a href='$base_url/pages/restaurant/info/$id/$slug' class='btn btn-block btn-success'>View Last Submit</a></div>
                     </div>
                     <div class='row mb10'>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-food/new/$_GET[id]' class='btn btn-block btn-success'>Add Food Review</a></div>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-beverage/new/$_GET[id]' class='btn btn-block btn-success'>Add Beverage Review</a></div>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant-photo/new/$_GET[id]' class='btn btn-block btn-success'>Add Restaurant Photo</a></div>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant-menu/new/$_GET[id]' class='btn btn-block btn-success'>Add Restaurant Menu</a></div>
                        <br><br><br><br>
                     </div>
                     <div class='row mb10'>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant/new' class='btn btn-block btn-danger'>Add New Restaurant Review</a></div>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-food/new' class='btn btn-block btn-danger'>Add New Food Review</a></div>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-beverage/new' class='btn btn-block btn-danger'>Add New Beverage Review</a></div>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-recipe/new' class='btn btn-block btn-danger'>Add New Recipe Review</a></div>
                     </div>
                     <div class='row mb10'>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-article/new' class='btn btn-block btn-danger'>Add New Article Review</a></div>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-video/new' class='btn btn-block btn-danger'>Add New Video</a></div>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant-photo/new' class='btn btn-block btn-danger'>Add New Restaurant Photo</a></div>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-menu/new' class='btn btn-block btn-danger'>Add New Restaurant Menu</a></div>
                     </div>
                  ";
					}
					elseif($_GET['info']=="food"){
                  $id = id_masking($_GET['id']);
                  $resto=mysqli_query($koneksi,"SELECT * FROM food where id_food='$_GET[id]'");
                  $r=mysqli_fetch_array($resto);
                  $slug=seo($r['nama_food']);
						echo"<div class='alert alert-success' role='alert'>Success! New Food review has been saved.</div>";
                  echo "
                     <h3 class='f-merah mb10'>What will you do next?</h3>
                     <div class='row mb10'>
                        <div class='col-4 col-4b'><a href='#' onclick='goBack()' class='btn btn-block btn-success'>Back</a></div>
                        <div class='col-4 col-4b'></div>
                        <div class='col-4 col-4b'></div>
                        <div class='col-4 col-4b'><a href='$base_url/pages/food/info/$id/$slug' class='btn btn-block btn-success'>View Last Submit</a></div>
                     </div>
                     <div class='row mb10'>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant/edit/$r[id_restaurant]' class='btn btn-block btn-success'>Edit Restaurant Review</a></div>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-food/new/$r[id_restaurant]' class='btn btn-block btn-success'>Add Food Review</a></div>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-beverage/new/$r[id_restaurant]' class='btn btn-block btn-success'>Add Beverage Review</a></div>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant-photo/new/$r[id_restaurant]' class='btn btn-block btn-success'>Add Restaurant Photo</a></div>
                     </div>
                     <div class='row mb10'>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant-menu/new/$r[id_restaurant]' class='btn btn-block btn-success'>Add Restaurant Menu</a></div>
                        <br><br><br><br>
                     </div>
                     <div class='row mb10'>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant/new' class='btn btn-block btn-danger'>Add New Restaurant Review</a></div>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-food/new' class='btn btn-block btn-danger'>Add New Food Review</a></div>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-beverage/new' class='btn btn-block btn-danger'>Add New Beverage Review</a></div>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-recipe/new' class='btn btn-block btn-danger'>Add New Recipe Review</a></div>
                     </div>
                     <div class='row mb10'>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-article/new' class='btn btn-block btn-danger'>Add New Article Review</a></div>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-video/new' class='btn btn-block btn-danger'>Add New Video</a></div>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant-photo/new' class='btn btn-block btn-danger'>Add New Restaurant Photo</a></div>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-menu/new' class='btn btn-block btn-danger'>Add New Restaurant Menu</a></div>
                     </div>
                  ";
					}
					elseif($_GET['info']=="beverage"){
                  $id = id_masking($_GET['id']);
                  $re=mysqli_query($koneksi,"SELECT * FROM beverage where id_beverage='$_GET[id]'");
                  $r=mysqli_fetch_array($re);
                  $slug=seo($r['nama_beverage']);
                        // $ress=mysqli_query($koneksi,"SELECT * FROM restaurant where id_restaurant='$r[id_restaurant]'");
                        // $ra=mysqli_fetch_array($ress);
                        // $slug=seo($r['nama_beverage']);
						echo"<div class='alert alert-success' role='alert'>Success! New Beverage review has been saved.</div>";
                  echo "
                     <h3 class='f-merah mb10'>What will you do next?</h3>
                     <div class='row mb10'>
                        <div class='col-4 col-4b'><a href='#' onclick='goBack()' class='btn btn-block btn-success'>Back</a></div>
                        <div class='col-4 col-4b'></div>
                        <div class='col-4 col-4b'></div>
                        <div class='col-4 col-4b'><a href='$base_url/pages/beverage/info/$id/$slug' class='btn btn-block btn-success'>View Last Submit</a></div>
                     </div>
                     <div class='row mb10'>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant/edit/$r[id_restaurant]' class='btn btn-block btn-success'>Edit Restaurant Review</a></div>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-beverage/new/$_GET[id_resto]' class='btn btn-block btn-success'>Add Beverage Review</a></div>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-food/new/$_GET[id_resto]' class='btn btn-block btn-success'>Add Food Review</a></div>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant-photo/new/$_GET[id_resto]' class='btn btn-block btn-success'>Add Restaurant Photo</a></div>
                     </div>
                     <div class='row mb10'>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant-menu/new/$r[id_restaurant]' class='btn btn-block btn-success'>Add Restaurant Menu</a></div>
                        <br><br><br><br>
                     </div>
                     <div class='row mb10'>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant/new' class='btn btn-block btn-danger'>Add New Restaurant Review</a></div>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-food/new' class='btn btn-block btn-danger'>Add New Food Review</a></div>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-beverage/new' class='btn btn-block btn-danger'>Add New Beverage Review</a></div>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-recipe/new' class='btn btn-block btn-danger'>Add New Recipe Review</a></div>
                     </div>
                     <div class='row mb10'>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-article/new' class='btn btn-block btn-danger'>Add New Article Review</a></div>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-video/new' class='btn btn-block btn-danger'>Add New Video</a></div>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant-photo/new' class='btn btn-block btn-danger'>Add New Restaurant Photo</a></div>
                        <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-menu/new' class='btn btn-block btn-danger'>Add New Restaurant Menu</a></div>
                     </div>
                  ";
					}
					elseif($_GET['info']=="recipe"){
						echo"<div class='alert alert-success' role='alert'>Success! New Recipe review has been saved.</div>";
                  echo "<div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant/new' class='btn btn-block btn-danger'>Add New Restaurant Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-food/new' class='btn btn-block btn-danger'>Add New Food Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-beverage/new' class='btn btn-block btn-danger'>Add New Beverage Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-recipe/new' class='btn btn-block btn-danger'>Add New Recipe Review</a></div>
                        </div>
                        <div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-article/new' class='btn btn-block btn-danger'>Add New Article Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-video/new' class='btn btn-block btn-danger'>Add New Video</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant-photo/new' class='btn btn-block btn-danger'>Add New Restaurant Photo</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-menu/new' class='btn btn-block btn-danger'>Add New Restaurant Menu</a></div>
                        </div>";
					}
					elseif($_GET['info']=="article"){
						echo"<div class='alert alert-success' role='alert'>Success! New Article has been saved.</div>";
                  echo "<div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant/new' class='btn btn-block btn-danger'>Add New Restaurant Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-food/new' class='btn btn-block btn-danger'>Add New Food Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-beverage/new' class='btn btn-block btn-danger'>Add New Beverage Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-recipe/new' class='btn btn-block btn-danger'>Add New Recipe Review</a></div>
                        </div>
                        <div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-article/new' class='btn btn-block btn-danger'>Add New Article Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-video/new' class='btn btn-block btn-danger'>Add New Video</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant-photo/new' class='btn btn-block btn-danger'>Add New Restaurant Photo</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-menu/new' class='btn btn-block btn-danger'>Add New Restaurant Menu</a></div>
                        </div>";
					}
					elseif($_GET['info']=="restaurant-photo"){
						echo"<div class='alert alert-success' role='alert'>Success! New Restaurant photo has been saved.</div>";
                  echo "<div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant/new' class='btn btn-block btn-danger'>Add New Restaurant Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-food/new' class='btn btn-block btn-danger'>Add New Food Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-beverage/new' class='btn btn-block btn-danger'>Add New Beverage Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-recipe/new' class='btn btn-block btn-danger'>Add New Recipe Review</a></div>
                        </div>
                        <div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-article/new' class='btn btn-block btn-danger'>Add New Article Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-video/new' class='btn btn-block btn-danger'>Add New Video</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant-photo/new' class='btn btn-block btn-danger'>Add New Restaurant Photo</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-menu/new' class='btn btn-block btn-danger'>Add New Restaurant Menu</a></div>
                        </div>";
					}
					elseif($_GET['info']=="food-photo"){
						echo"<div class='alert alert-success' role='alert'>Success! New Food photo has been saved.</div>";
                  echo "<div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant/new' class='btn btn-block btn-danger'>Add New Restaurant Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-food/new' class='btn btn-block btn-danger'>Add New Food Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-beverage/new' class='btn btn-block btn-danger'>Add New Beverage Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-recipe/new' class='btn btn-block btn-danger'>Add New Recipe Review</a></div>
                        </div>
                        <div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-article/new' class='btn btn-block btn-danger'>Add New Article Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-video/new' class='btn btn-block btn-danger'>Add New Video</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant-photo/new' class='btn btn-block btn-danger'>Add New Restaurant Photo</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-menu/new' class='btn btn-block btn-danger'>Add New Restaurant Menu</a></div>
                        </div>";
					}
					elseif($_GET['info']=="beverage-photo"){
						echo"<div class='alert alert-success' role='alert'>Success! New Beverage photo has been saved.</div>";
                  echo "<div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant/new' class='btn btn-block btn-danger'>Add New Restaurant Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-food/new' class='btn btn-block btn-danger'>Add New Food Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-beverage/new' class='btn btn-block btn-danger'>Add New Beverage Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-recipe/new' class='btn btn-block btn-danger'>Add New Recipe Review</a></div>
                        </div>
                        <div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-article/new' class='btn btn-block btn-danger'>Add New Article Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-video/new' class='btn btn-block btn-danger'>Add New Video</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant-photo/new' class='btn btn-block btn-danger'>Add New Restaurant Photo</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-menu/new' class='btn btn-block btn-danger'>Add New Restaurant Menu</a></div>
                        </div>";
					}
					elseif($_GET['info']=="recipe-photo"){
						echo"<div class='alert alert-success' role='alert'>Success! New Recipe photo has been saved.</div>";
                  echo "<div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant/new' class='btn btn-block btn-danger'>Add New Restaurant Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-food/new' class='btn btn-block btn-danger'>Add New Food Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-beverage/new' class='btn btn-block btn-danger'>Add New Beverage Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-recipe/new' class='btn btn-block btn-danger'>Add New Recipe Review</a></div>
                        </div>
                        <div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-article/new' class='btn btn-block btn-danger'>Add New Article Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-video/new' class='btn btn-block btn-danger'>Add New Video</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant-photo/new' class='btn btn-block btn-danger'>Add New Restaurant Photo</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-menu/new' class='btn btn-block btn-danger'>Add New Restaurant Menu</a></div>
                        </div>";
					}
					elseif($_GET['info']=="article-photo"){
						echo"<div class='alert alert-success' role='alert'>Success! New Article photo has been saved.</div>";
                  echo "<div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant/new' class='btn btn-block btn-danger'>Add New Restaurant Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-food/new' class='btn btn-block btn-danger'>Add New Food Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-beverage/new' class='btn btn-block btn-danger'>Add New Beverage Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-recipe/new' class='btn btn-block btn-danger'>Add New Recipe Review</a></div>
                        </div>
                        <div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-article/new' class='btn btn-block btn-danger'>Add New Article Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-video/new' class='btn btn-block btn-danger'>Add New Video</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant-photo/new' class='btn btn-block btn-danger'>Add New Restaurant Photo</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-menu/new' class='btn btn-block btn-danger'>Add New Restaurant Menu</a></div>
                        </div>";
					}
					elseif($_GET['info']=="menu"){
						echo"<div class='alert alert-success' role='alert'>Success! New Menu has been saved.</div>";
                  echo "<div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant/new' class='btn btn-block btn-danger'>Add New Restaurant Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-food/new' class='btn btn-block btn-danger'>Add New Food Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-beverage/new' class='btn btn-block btn-danger'>Add New Beverage Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-recipe/new' class='btn btn-block btn-danger'>Add New Recipe Review</a></div>
                        </div>
                        <div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-article/new' class='btn btn-block btn-danger'>Add New Article Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-video/new' class='btn btn-block btn-danger'>Add New Video</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant-photo/new' class='btn btn-block btn-danger'>Add New Restaurant Photo</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-menu/new' class='btn btn-block btn-danger'>Add New Restaurant Menu</a></div>
                        </div>";
					}
					elseif($_GET['info']=="video"){
						echo"<div class='alert alert-success' role='alert'>Success! New Video has been saved.</div>";
                  echo "<div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant/new' class='btn btn-block btn-danger'>Add New Restaurant Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-food/new' class='btn btn-block btn-danger'>Add New Food Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-beverage/new' class='btn btn-block btn-danger'>Add New Beverage Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-recipe/new' class='btn btn-block btn-danger'>Add New Recipe Review</a></div>
                        </div>
                        <div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-article/new' class='btn btn-block btn-danger'>Add New Article Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-video/new' class='btn btn-block btn-danger'>Add New Video</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant-photo/new' class='btn btn-block btn-danger'>Add New Restaurant Photo</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-menu/new' class='btn btn-block btn-danger'>Add New Restaurant Menu</a></div>
                        </div>";
					}
					elseif($_GET['info']=="error-resto"){
						echo"<div class='alert alert-success' role='alert'>Failed! No restaurant that matches the name of the restaurant that you input. Please try again and make sure the name of the restaurant is available.</div>";
                  echo "<div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant/new' class='btn btn-block btn-danger'>Add New Restaurant Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-food/new' class='btn btn-block btn-danger'>Add New Food Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-beverage/new' class='btn btn-block btn-danger'>Add New Beverage Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-recipe/new' class='btn btn-block btn-danger'>Add New Recipe Review</a></div>
                        </div>
                        <div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-article/new' class='btn btn-block btn-danger'>Add New Article Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-video/new' class='btn btn-block btn-danger'>Add New Video</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant-photo/new' class='btn btn-block btn-danger'>Add New Restaurant Photo</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-menu/new' class='btn btn-block btn-danger'>Add New Restaurant Menu</a></div>
                        </div>";
					}
					elseif($_GET['info']=="error-food"){
						echo"<div class='alert alert-success' role='alert'>Failed! No food that matches the name of the food that you input. Please try again and make sure the name of the food is available.</div>";
                  echo "<div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant/new' class='btn btn-block btn-danger'>Add New Restaurant Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-food/new' class='btn btn-block btn-danger'>Add New Food Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-beverage/new' class='btn btn-block btn-danger'>Add New Beverage Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-recipe/new' class='btn btn-block btn-danger'>Add New Recipe Review</a></div>
                        </div>
                        <div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-article/new' class='btn btn-block btn-danger'>Add New Article Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-video/new' class='btn btn-block btn-danger'>Add New Video</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant-photo/new' class='btn btn-block btn-danger'>Add New Restaurant Photo</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-menu/new' class='btn btn-block btn-danger'>Add New Restaurant Menu</a></div>
                        </div>";
					}
					elseif($_GET['info']=="error-beverage"){
						echo"<div class='alert alert-success' role='alert'>Failed! No beverage that matches the name of the beverage that you input. Please try again and make sure the name of the beverage is available.</div>";
                  echo "<div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant/new' class='btn btn-block btn-danger'>Add New Restaurant Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-food/new' class='btn btn-block btn-danger'>Add New Food Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-beverage/new' class='btn btn-block btn-danger'>Add New Beverage Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-recipe/new' class='btn btn-block btn-danger'>Add New Recipe Review</a></div>
                        </div>
                        <div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-article/new' class='btn btn-block btn-danger'>Add New Article Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-video/new' class='btn btn-block btn-danger'>Add New Video</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant-photo/new' class='btn btn-block btn-danger'>Add New Restaurant Photo</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-menu/new' class='btn btn-block btn-danger'>Add New Restaurant Menu</a></div>
                        </div>";
					}
					elseif($_GET['info']=="error-recipe"){
						echo"<div class='alert alert-success' role='alert'>Failed! No recipe that matches the name of the recipe that you input. Please try again and make sure the name of the recipe is available.</div>";
                  echo "<div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant/new' class='btn btn-block btn-danger'>Add New Restaurant Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-food/new' class='btn btn-block btn-danger'>Add New Food Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-beverage/new' class='btn btn-block btn-danger'>Add New Beverage Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-recipe/new' class='btn btn-block btn-danger'>Add New Recipe Review</a></div>
                        </div>
                        <div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-article/new' class='btn btn-block btn-danger'>Add New Article Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-video/new' class='btn btn-block btn-danger'>Add New Video</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant-photo/new' class='btn btn-block btn-danger'>Add New Restaurant Photo</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-menu/new' class='btn btn-block btn-danger'>Add New Restaurant Menu</a></div>
                        </div>";
					}
					elseif($_GET['info']=="error-article"){
						echo"<div class='alert alert-success' role='alert'>Failed! No article that matches the name of the article that you input. Please try again and make sure the name of the article is available.</div>";
                  echo "<div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant/new' class='btn btn-block btn-danger'>Add New Restaurant Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-food/new' class='btn btn-block btn-danger'>Add New Food Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-beverage/new' class='btn btn-block btn-danger'>Add New Beverage Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-recipe/new' class='btn btn-block btn-danger'>Add New Recipe Review</a></div>
                        </div>
                        <div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-article/new' class='btn btn-block btn-danger'>Add New Article Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-video/new' class='btn btn-block btn-danger'>Add New Video</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant-photo/new' class='btn btn-block btn-danger'>Add New Restaurant Photo</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-menu/new' class='btn btn-block btn-danger'>Add New Restaurant Menu</a></div>
                        </div>";
					}
					else{
						echo"<div class='alert alert-danger' role='alert'>What do you want baby..</div>";
                  echo "<div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant/new' class='btn btn-block btn-danger'>Add New Restaurant Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-food/new' class='btn btn-block btn-danger'>Add New Food Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-beverage/new' class='btn btn-block btn-danger'>Add New Beverage Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-recipe/new' class='btn btn-block btn-danger'>Add New Recipe Review</a></div>
                        </div>
                        <div class='row mb10'>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-article/new' class='btn btn-block btn-danger'>Add New Article Review</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-video/new' class='btn btn-block btn-danger'>Add New Video</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-restaurant-photo/new' class='btn btn-block btn-danger'>Add New Restaurant Photo</a></div>
                           <div class='col-4 col-4b'><a href='$base_url/$u[username]/my-menu/new' class='btn btn-block btn-danger'>Add New Restaurant Menu</a></div>
                        </div>";
					}
				}
				?>

			</div>
		</div>
    </div>
    <?php include"config/inc/footer.php"; ?>
    <script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
    <script src="<?php echo"$base_url"; ?>/assets/js/hideShowPassword.min.js"></script>
    <script>
    $('#password-1').hidePassword(true);
    </script>
    <script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
    <script src="<?php echo"$base_url"; ?>/assets/js/jquery.validate.min.js"></script>
    <script src="<?php echo"$base_url"; ?>/assets/js/theme.js"></script>
    <script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
     <script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
     <script>
    		$(document).ready(function() {
    			$('.drawer').drawer();

    		$('.tutup').click(function(){
    			$('.iklanan').toggleClass('hilang', 500);
    		});

    		$('.cart').click(function(e){
    			e.stopPropagation();
    			$('.dis-cart').toggle();
    			$('.dis-plus').hide();
    			$('.dis-share').hide();
    		});
    		$('.plus').click(function(e){
    			e.stopPropagation();
    			$('.dis-plus').toggle();
    			$('.dis-cart').hide();
    			$('.dis-share').hide();
    		});
    		$('.share').click(function(e){
    			e.stopPropagation();
    			$('.dis-share').toggle();
    			$('.dis-plus').hide();
    			$('.dis-cart').hide();
    		});
    		$('.drawer-toggle').click(function(){
    			$('.dis-cart').hide();
    			$('.dis-plus').hide();
    			$('.dis-share').hide();
    		});
    		$(document).click(function () {
    			 var $el = $(".dis-share");
    			 if ($el.is(":visible")) {
    					$el.fadeOut(200);
    			 }
    			 var $ela = $(".dis-plus");
    			 if ($ela.is(":visible")) {
    					$ela.fadeOut(200);
    			 }
    			 var $elu = $(".dis-cart");
    			 if ($elu.is(":visible")) {
    					$elu.fadeOut(200);
    			 }
    		 });

    		});
     </script>
    <script type="text/javascript">
    $(document).ready(function () {
    	$(".alert").fadeTo(5000, 500).fadeOut(500, function(){
    		$(".alert").alert('close');
    	});

    	$('#signup-daftar').validate({
    });
    });
    </script>
</body>
</html>
<?php
}
else{
	header("Location: ".$base_url."/login-area");
}
?>
<script>
function goBack() {
    window.history.go(-2);
}
</script>
