<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";	
include "config/func/id_masking.php";
date_default_timezone_set("Asia/Jakarta");
$sekarang=date("Y-m-d");
$id = id_masking($_GET['id']);
$r=mysqli_fetch_array(mysqli_query($koneksi,"select id_recipe, nama_recipe from recipe where id_recipe='$id'"));
$seo=seo($r['nama_recipe']);
if(isset($_SESSION['food_member'])){
	if($_GET['fungsi']=="1"){
		$ada=mysqli_num_rows(mysqli_query($koneksi,"select id_recipe_like from recipe_like where id_recipe='$id' and id_member='$_SESSION[food_member]' and tgl_like='$sekarang'"));
		if($ada==0){
			mysqli_query($koneksi,"insert into recipe_like (id_member,id_recipe,tgl_like) values('$_SESSION[food_member]','$id','$sekarang')");
			$_SESSION['resto_notif']     = "suka";
		}
		else{
			$_SESSION['resto_notif']     = "suka_gagal";
		}
	}
	elseif($_GET['fungsi']=="2"){
		$ada=mysqli_num_rows(mysqli_query($koneksi,"select id_recipe_rekomendasi from recipe_rekomendasi where id_recipe='$id' and id_member='$_SESSION[food_member]' and tgl_rekomendasi='$sekarang'"));
		if($ada==0){
			mysqli_query($koneksi,"insert into recipe_rekomendasi (id_member,id_recipe,tgl_rekomendasi) values('$_SESSION[food_member]','$id','$sekarang')");
			$_SESSION['resto_notif']     = "rekomendasi";
		}
		else{
			$_SESSION['resto_notif']     = "rekomendasi_gagal";
		}
	}
	elseif($_GET['fungsi']=="3"){
		$ada_bookmark=mysqli_num_rows(mysqli_query($koneksi,"select id_bookmark from bookmark where id='$id' and jenis='recipe' and id_member='$_SESSION[food_member]'"));
		if($ada_bookmark==0){
			mysqli_query($koneksi,"insert into bookmark (id_member,jenis,id,tgl_bookmark) values('$_SESSION[food_member]','recipe','$id','$sekarang')");
			$_SESSION['resto_notif']     = "bookmark";
		}	
		else{
			$_SESSION['resto_notif']     = "bookmark_gagal";
		}
	}
	elseif($_GET['fungsi']=="4"){
		$ada_here=mysqli_num_rows(mysqli_query($koneksi,"select id_recipe_here from recipe_here where id_recipe='$id' and id_member='$_SESSION[food_member]'"));
		if($ada_here==0){
			mysqli_query($koneksi,"insert into recipe_here (id_member,id_recipe,tgl_here) values('$_SESSION[food_member]','$id','$sekarang')");
			$_SESSION['resto_notif']     = "disini";
		}
		else{
			$_SESSION['resto_notif']     = "disini_gagal";
		}
	}
	elseif($_GET['fungsi']=="5"){
		$ada_report=mysqli_num_rows(mysqli_query($koneksi,"select id_recipe_report from recipe_report where id_recipe='$id' and id_member='$_SESSION[food_member]'"));
		if($ada_report==0){
			mysqli_query($koneksi,"insert into recipe_report (id_member,id_recipe,tgl_report) values('$_SESSION[food_member]','$id','$sekarang')");
			$_SESSION['resto_notif']     = "lapor";
		}
		else{
			$_SESSION['resto_notif']     = "lapor_gagal";
		}
	}
	elseif($_GET['fungsi']=="6"){
		$ada_report=mysqli_num_rows(mysqli_query($koneksi,"select id_recipe_rating from recipe_rating where id_recipe='$id' and id_member='$_SESSION[food_member]' and tgl_recipe_rating='$sekarang'"));
		if($ada_report==0){
			if(!empty($_GET['rating'])){
				$r1=$_GET['rating'] * 0.21;
				$r2=$_GET['rating'] * 0.20;
				$r3=$_GET['rating'] * 0.18;
				$r4=$_GET['rating'] * 0.17;
				$r5=$_GET['rating'] * 0.15;
				$r6=$_GET['rating'] * 0.09;
			}
			else{
				$r1=$_GET['cleanlines'] * 0.21;
				$r2=$_GET['flavor'] * 0.20;
				$r3=$_GET['freshness'] * 0.18;
				$r4=$_GET['cooking'] * 0.17;
				$r5=$_GET['aroma'] * 0.15;
				$r6=$_GET['serving'] * 0.09;
			}
			mysqli_query($koneksi,"INSERT INTO recipe_rating (id_recipe,id_member,cleanlines,flavor,freshness,cooking,aroma,serving,tgl_recipe_rating) values('$id','$_SESSION[food_member]','$r1','$r2','$r3','$r4','$r5','$r6','$sekarang')");
			$_SESSION['resto_notif']     = "rating";
		}
		else{
			$_SESSION['resto_notif']     = "rating_gagal";
		}
	}
}
else{
	$_SESSION['resto_notif']     = "login_dulu";
}
header("Location: ".$base_url."/pages/recipe/".$_GET['page']."/".$_GET['id']."/".$seo);
?>