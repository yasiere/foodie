<?php
session_start();
$auto_logout=600;
include "config/func/base_url.php";
if(!empty($_SESSION['food_member'])){
if (time()-$_SESSION['timestamp']>$auto_logout){
    session_destroy();
    session_unset();
	header("Location: ".$base_url."/auto-logout");
	exit();
}else{
    $_SESSION['timestamp']=time();
}
include "config/database/db.php";
include "config/func/member_data.php";
$active = "member";
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
    <title>Social Media - Foodie Guidances</title>
	<meta name="keywords" content="">
    <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/awesome-bootstrap-checkbox.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>
  <link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body style="position: initial;" class="drawer drawer--right">
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>
    <div class="container hook">
		<div class="row">
      <div class="btn-caris">
        <a href="#">
          <img src="<?php echo"$base_url/assets/img/member/$u[gambar_thumb]"; ?>" class="foto_profil">
        </a>
      </div>
			<div class="col-4 search_kiri">
				<ul class="breadcrumb">
					<li><a href="#">Account</a></li>
					<li class="active">Social Media</li>
				</ul>
				<div>
					<img src="<?php echo"$base_url/assets/img/member/$u[gambar_thumb]"; ?>" class="foto_profil">
					<div class="detail">
						<?php echo"<h6>$u[nama_depan] $u[nama_belakang]</h6>"; ?>
					</div>
				</div>
				<ul class="list-unstyled member-social">
					<?php
					if($u['social_fb']==""){
						echo"<li><img src='$base_url/assets/img/theme/fb_29_off.png'> Facebook</li>";
					}
					else{
						echo"<li><img src='$base_url/assets/img/theme/fb_29.png'> <a href='$u[social_fb]' target='_blank'>Facebook</a></li>";
					}
					if($u['social_twitter']==""){
						echo"<li><img src='$base_url/assets/img/theme/tw_29_off.png'> Twitter</li>";
					}
					else{
						echo"<li><img src='$base_url/assets/img/theme/tw_29.png'> <a href='$u[social_twitter]' target='_blank'>Twitter</a></li>";
					}
					if($u['social_pint']==""){
						echo"<li><img src='$base_url/assets/img/theme/pin_29_off.png'> Pinterest</li>";
					}
					else{
						echo"<li><img src='$base_url/assets/img/theme/pin_29.png'> <a href='$u[social_pint]' target='_blank'>Pinterest</a></li>";
					}
					if($u['social_google']==""){
						echo"<li><img src='$base_url/assets/img/theme/gog_29_off.png'> Google+</li>";
					}
					else{
						echo"<li><img src='$base_url/assets/img/theme/gog_29.png'> <a href='$u[social_google]' target='_blank'>Google+</a></li>";
					}
					if($u['social_insta']==""){
						echo"<li><img src='$base_url/assets/img/theme/insta_29_off.png'> Instagram</li>";
					}
					else{
						echo"<li><img src='$base_url/assets/img/theme/insta_29.png'> <a href='$u[social_insta]' target='_blank'>Instagram</a></li>";
					}
					?>
				</ul>
				<hr class="mtb10">
				<ul class="list-unstyled">
					<li><a href="<?php echo"$base_url/$u[username]/activity-feed"; ?>">Activity Feed</a></li>
					<li><span class="buka">
						Account <div class="pull-right">+</div>
						<ul class="pl50 sub-menu">
							<li><a href="<?php echo"$base_url/$u[username]/edit-profile"; ?>">Edit Profile</a></li>
							<li><a href="<?php echo"$base_url/$u[username]/change-password"; ?>">Change Password</a></li>
							<li><a href="<?php echo"$base_url/$u[username]/activity-setting"; ?>">Activity Feed Setting</a></li>
							<li><a href="<?php echo"$base_url/$u[username]/social-media"; ?>" class="f-merah">Social Media</a></li>
							<li><a href="<?php echo"$base_url/$u[username]/delete-account"; ?>">Delete Account</a></li>
						</ul>
						</span>
					</li>
					<li><span class="buka2">
						Submit <div class="pull-right">+</div>
						<ul class="pl50 sub-menu">
							<li><a href="<?php echo"$base_url/$u[username]/my-restaurant/new"; ?>">Restaurant</a></li>
							<li><a href="<?php echo"$base_url/$u[username]/my-food/new"; ?>">Food</a></li>
							<li><a href="<?php echo"$base_url/$u[username]/my-beverage/new"; ?>">Beverage</a></li>
							<li><a href="<?php echo"$base_url/$u[username]/my-process/new"; ?>">My Process F&B</a></li>
							<li><a href="<?php echo"$base_url/$u[username]/my-recipe/new"; ?>">Recipe</a></li>
							<li><a href="<?php echo"$base_url/$u[username]/my-article/new"; ?>">Article</a></li>
							<li><a href="<?php echo"$base_url/$u[username]/my-video/new"; ?>">Video</a></li>
						</ul>
						</span>
					</li>
					<li><a href="<?php echo"$base_url/$u[username]/my-restaurant"; ?>">My Restaurant</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-restaurant-photo"; ?>">My Restaurant Photo</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-restaurant-menu"; ?>">My Restaurant Menu</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-food"; ?>">My Food</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-food-photo"; ?>">My Food Photo</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-beverage"; ?>">My Beverage</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-beverage-photo"; ?>">My Beverage Photo</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-process"; ?>">My Process F&B</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-events"; ?>">My Events</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-recipe"; ?>">My Recipe</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-recipe-photo"; ?>">My Recipe Photo</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-article"; ?>">My Article</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-article-photo"; ?>">My Article Photo</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-video"; ?>">My Video</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-bookmark"; ?>">Bookmark</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-vote"; ?>">Give Vote to</a></li>
				</ul>
				<hr class="mtb10">
				<strong>FGMart</strong>
				<ul class="list-unstyled">
					<li><a href="<?php echo"$base_url/$u[username]/stock-cart"; ?>">Stock Cart</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/download-link"; ?>">Download Link</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/order-history"; ?>">Order History</a></li>
				</ul>
			</div>
			<div class="col-12 col-8a">
				<?php
				if(isset($_SESSION['notif'])){
					if($_SESSION['notif']=="sukses"){
						echo"<div class='alert alert-success alert-dismissible' role='alert'>
							<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
							<strong>Great!</strong> Your social media network information are now updated.
						</div>";
					}
					unset($_SESSION['notif']);
				}
				?>
				<h4 class="f-merah no-mb mt20 mb10">Social Media</h4>
				<form class="border-form" method="post" action="<?php echo"$base_url"; ?>/config/func/save_social.php" id="signup-daftar">
					<div class="row mb10">
						<div class="col-4 col-4b">
							<label><img src="<?php echo"$base_url"; ?>/assets/img/theme/icon_fb.png"> Facebook</label>
						</div>
						<div class="col-8 col-4b">
							<input type="text" name="fb" class="form-control" placeholder="Paste your facebook profile url here.. (http://)"
                     <?php if($u['social_fb']<>"#"){echo"value='$u[social_fb]'";}?>>
						</div>
					</div>
					<div class="row mb10">
						<div class="col-4 col-4b">
							<label><img src="<?php echo"$base_url"; ?>/assets/img/theme/icon_tw.png"> Twitter</label>
						</div>
						<div class="col-8 col-4b">
							<input type="text" name="tw" class="form-control" placeholder="Paste your twitter profile url here..(http://)"
                     <?php if($u['social_twitter']<>"#"){echo"value='$u[social_twitter]'";}?>>
						</div>
					</div>
					<div class="row mb10">
						<div class="col-4 col-4b">
							<label><img src="<?php echo"$base_url"; ?>/assets/img/theme/icon_pinterest.png"> Pinterest</label>
						</div>
						<div class="col-8 col-4b">
							<input type="text" name="pinterest" class="form-control" placeholder="Paste your pinterest profile url here..(http://)"
                  <?php if($u['social_pint']<>"#"){echo"value='$u[social_pint]'";}?>>
						</div>
					</div>
					<div class="row mb10">
						<div class="col-4 col-4b">
							<label><img src="<?php echo"$base_url"; ?>/assets/img/theme/icon_google.png"> Google+</label>
						</div>
						<div class="col-8 col-4b">
							<input type="text" name="google" class="form-control" placeholder="Paste your google+ profile url here..(http://)" <?php if($u['social_google']<>"#"){echo"value='$u[social_google]'";}?>>
						</div>
					</div>
					<div class="row mb10">
						<div class="col-4 col-4b">
							<label><img src="<?php echo"$base_url"; ?>/assets/img/theme/icon_insta.png"> Instagram</label>
						</div>
						<div class="col-8 col-4b">
							<input type="text" name="instagram" class="form-control" placeholder="Paste your instagram profile url here..(http://)" <?php if($u['social_insta']<>"#"){echo"value='$u[social_insta]'";}?>>
						</div>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-danger">Save</button>
					</div>
				</form>
			</div>
		</div>
    </div>
    <?php include"config/inc/footer.php"; ?>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.validate.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/theme.js"></script>
  <script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
	<script>
		 $(document).ready(function() {
			 $('.drawer').drawer();

		 $('.tutup').click(function(){
			 $('.iklanan').toggleClass('hilang', 500);
		 });

		 $('.cart').click(function(e){
			 e.stopPropagation();
			 $('.dis-cart').toggle();
			 $('.dis-plus').hide();
			 $('.dis-share').hide();
		 });
		 $('.plus').click(function(e){
			 e.stopPropagation();
			 $('.dis-plus').toggle();
			 $('.dis-cart').hide();
			 $('.dis-share').hide();
		 });
		 $('.share').click(function(e){
			 e.stopPropagation();
			 $('.dis-share').toggle();
			 $('.dis-plus').hide();
			 $('.dis-cart').hide();
		 });
		 $('.drawer-toggle').click(function(){
			 $('.dis-cart').hide();
			 $('.dis-plus').hide();
			 $('.dis-share').hide();
		 });
		 $('.btn-caris').click(function(){
			 $('.search_kiri').toggleClass('search_kiri_tam', 500);
		 });
		 $(document).click(function () {
				var $el = $(".dis-share");
				if ($el.is(":visible")) {
					 $el.fadeOut(200);
				}
				var $ela = $(".dis-plus");
				if ($ela.is(":visible")) {
					 $ela.fadeOut(200);
				}
				var $elu = $(".dis-cart");
				if ($elu.is(":visible")) {
					 $elu.fadeOut(200);
				}
			});

		 });
	</script>
	<script type="text/javascript">
	$(document).ready(function () {
		$(".alert").fadeTo(5000, 500).fadeOut(500, function(){
			$(".alert").alert('close');
		});
		$.validator.addMethod("passkuat",function(value,element){
            return this.optional(element) || /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \+.-]*)*\/?$/i.test(value);
        });
		$('#signup-daftar').validate({
			rules: {
				fb: {
					minlength: 8,
					maxlength: 50,
					passkuat:true
				},
				tw: {
					minlength: 8,
					maxlength: 50,
					passkuat:true
				},
				pinterest: {
					minlength: 8,
					maxlength: 50,
					passkuat:true
				},
				google: {
					minlength: 8,
					maxlength: 50,
					passkuat:true
				},
				instagram: {
					minlength: 8,
					maxlength: 50,
					passkuat:true
				}
			},
			messages: {
				fb: {
					minlength: "Url must be between 8 - 50 characters.",
					maxlength: "Url must be between 8 - 50 characters.",
					passkuat: "Please write the correct URL"
				},
				tw: {
					minlength: "Url must be between 8 - 50 characters.",
					maxlength: "Url must be between 8 - 50 characters.",
					passkuat: "Please write the correct URL"
				},
				pinterest: {
					minlength: "Url must be between 8 - 50 characters.",
					maxlength: "Url must be between 8 - 50 characters.",
					passkuat: "Please write the correct URL"
				},
				google: {
					minlength: "Url must be between 8 - 50 characters.",
					maxlength: "Url must be between 8 - 50 characters.",
					passkuat: "Please write the correct URL"
				},
				instagram: {
					minlength: "Url must be between 8 - 50 characters.",
					maxlength: "Url must be between 8 - 50 characters.",
					passkuat: "Please write the correct URL"
				}
			}
		});

		$('.buka2 .sub-menu').hide(); //Hide children by default
		$('.buka,.buka2').click(function(event){
			$(this).children('.sub-menu').slideToggle('slow');
		});
	});
	</script>
</body>
</html>
<?php
}
else{
	header("Location: ".$base_url."/login-area");
}
?>
