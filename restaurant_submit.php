<?php
session_start();
// $auto_logout=18000000000000000000000;
include "config/func/base_url.php";
// if(!empty($_SESSION['food_member'])){
// if (time()-$_SESSION['timestamp']>$auto_logout){
//     session_destroy();
//     session_unset();
// 	header("Location: ".$base_url."/auto-logout");
// 	exit();
// }else{
//     $_SESSION['timestamp']=time();
// }
include "config/database/db.php";
include "config/func/member_data.php";
$active = "member";
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
  <title>Submit Restaurant - Foodie Guidances</title>
	<meta name="keywords" content="">
  <meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap-timepicker.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/select2.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/awesome-bootstrap-checkbox.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>
	<link href="<?php echo"$base_url"; ?>/assets/css/jquery-ui.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">
	<style>
		#map {
			width: 100%;
			height: 400px;
		}
		.controls {
			margin-top: 10px;
			border: 1px solid transparent;
			border-radius: 2px 0 0 2px;
			box-sizing: border-box;
			-moz-box-sizing: border-box;
			height: 32px;
			outline: none;
			box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
		}
		#searchInput {
			background-color: #fff;
			font-family: Roboto;
			font-size: 15px;
			font-weight: 300;
			margin-left: 12px;
			padding: 0 11px 0 13px;
			text-overflow: ellipsis;
			width: 50%;
		}
		#searchInput:focus {
		 	border-color: #4d90fe;
		}
	</style>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
 <body style="position: initial;" class="drawer drawer--right">
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>
   <div class="loadi">
   <img src="<?php echo "$base/assets/img/theme/load.gif"?>">
   </div>
    <div class="container hook">
		<div class="row">
			<div class="col-16">
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Account</a></li>
					<li class="active">Submit Restaurant</li>
				</ol>
				<?php
				if(isset($_SESSION['notif'])){
					if($_SESSION['notif']=="gambar"){
						echo"<div class='alert alert-danger alert-dismissible' role='alert'>
							<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
							<strong>Failed!</strong> Image used have to be in JPG, PNG, and GIF format and the maximum total images size is 50MB also minimal dimension must be 800x600 pixel.
						</div>";
					}
					unset($_SESSION['notif']);
				}
				?>
				<h3 class="f-merah mb10">Submit Restaurant Review</h3>

				<form class="border-form form-horizontal" method="post" action="<?php echo"$base_url"; ?>/config/func/save_restaurant.php" enctype="multipart/form-data">
               <div class="row mb20">
                  <div class="col-4 col-4b"><label class="control-label">Country <span class="f-merah">*</span></label></div>
                  <div class="col-6 col-4b">
                     <select name="negara" required class="form-control" id="negara">
                        <option value="">Select Country</option>
                        <?php
                        $sql=mysqli_query($koneksi,"select * from negara order by nama_negara asc");
                        while($b=mysqli_fetch_array($sql)){
                           echo"<option value='$b[id_negara]'>$b[nama_negara]</option>";
                        }
                        ?>
                     </select>
                  </div>
               </div>
               <div class="row mb20">
                  <div class="col-4 col-4b"><label class="control-label">State/ Province <span class="f-merah">*</span></label></div>
                  <div class="col-6 col-4b">
                     <select name="propinsi" required class="form-control" id="propinsi">
                        <option value="">Select State/ Province</option>
                     </select>
                  </div>
               </div>
               <div class="row mb20">
                  <div class="col-4 col-4b"><label class="control-label">City <span class="f-merah">*</span></label></div>
                  <div class="col-6 col-4b">
                     <select name="kota" required class="form-control" id="kota">
                        <option value="">Select City</option>
                     </select>
                  </div>
               </div>
               <div class="row mb20">
									<div class="col-4 col-4b"><label class="control-label">Restaurant Name <span class="f-merah">*</span></label></div>
										<div class="col-6 col-4b">
			                 <input type="text" class="form-control" placeholder="Write Name" name="restaurant_name" autocomplete="off" required id="search-box">
			                 <div id="suggesstion-box"></div>
								  	</div>
								</div>
								<div class="row mb20">
									<div class="col-4 col-4b"><label class="control-label">Business Type <span class="f-merah">*</span></label></div>
									<div class="col-6 col-4b">
										<select name="type_of_business" required class="form-control" id="business_type">
											<option value="">Select Business Type</option>
											<?php
											$sql=mysqli_query($koneksi,"select * from type_of_business order by nama_type_of_business asc");
											while($a=mysqli_fetch_array($sql)){
												echo"<option value='$a[id_type_of_business]'>$a[nama_type_of_business]</option>";
											}
											?>
										</select>
									</div>
								</div>
								<div class="row mb20">
									<div class="col-4 col-4b"><label class="control-label">Cover Photo</label></div>
									<div class="col-6 col-4b">
										<div class="fileinput fileinput-new" data-provides="fileinput">
										  <div class="fileinput-preview upload" data-trigger="fileinput"></div>
										  <input type="file" name="cover_photo" class="hidden">
										</div>
										<p class="help-block">Image used have to be in JPG, PNG, and GIF format and the maximum total images size is 50MB also minimal dimension must be 800x600 pixel.</p>
									</div>
								</div>
								<div class="row mb20">
									<div class="col-4 col-4b"><label class="control-label">Tag <span class="f-merah">*</span></label></div>
									<div class="col-6 col-4b">
										<input type="text" id="tag" class="form-control" placeholder="Write tag (separated by comma)" autocomplete="off" name="tag" required>
									</div>
								</div>
					<div class="box-info-2">
						<div class="box-head">
							Basic Information<i class="fa fa-angle-up klik"></i>
						</div>
						<div class="box-panel">
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Street Address</label></div>
								<div class="col-6 col-4b">
									<input type="text" class="form-control mb10" placeholder="Street Address" autocomplete="off" name="street_address" maxlength="100">
								</div>
							</div>
                     <div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Shopping Mall</label></div>
								<div class="col-6 col-4b">
									<select name="mall" class="form-control" id="mall">
										<option>Select Shopping Mall</option>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Landmark</label></div>
								<div class="col-6 col-4b">
									<select name="landmark" class="form-control">
										<option>Select Landmark</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from landmark order by nama_landmark asc");
										while($d=mysqli_fetch_array($sql)){
											echo"<option value='$d[id_landmark]'>$d[nama_landmark]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Telephone</label></div>
								<div class="col-6 col-4b">
									<input type="text" class="form-control" placeholder="Write Phone Number" autocomplete="off" name="telephone">
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Facsimile</label></div>
								<div class="col-6 col-4b">
									<input type="text" class="form-control" placeholder="Write Facsimile" autocomplete="off" name="fax">
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Postal Code</label></div>
								<div class="col-6 col-4b">
									<input type="text" class="form-control" placeholder="Write Postal Code" autocomplete="off" name="postal_code">
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Reservation</label></div>
								<div class="col-6 col-4b">
									<input type="text" class="form-control" placeholder="Write Phone Number" autocomplete="off" name="reservation_phone">
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Email</label></div>
								<div class="col-6 col-4b">
									<input type="email" class="form-control" placeholder="Write Email" autocomplete="off" name="email_resto">
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Website</label></div>
								<div class="col-6 col-4b">
									<input type="text" class="form-control" placeholder="Write Website Url" autocomplete="off" name="web">
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Social Media</label></div>
								<div class="col-6 col-4b">
									<input type="text" class="form-control mb10" placeholder="Write Facebook Url" autocomplete="off" name="facebook">
									<input type="text" class="form-control mb10" placeholder="Write Twitter Url" autocomplete="off" name="twitter">
									<input type="text" class="form-control mb10" placeholder="Write Instagram Url" autocomplete="off" name="instagram">
									<input type="text" class="form-control" placeholder="Write Pinterest Url" autocomplete="off" name="pinterest">
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Branch</label></div>
								<div class="col-6 col-4b">
									<textarea class="form-control" name="branch_name" placeholder="Write Branch" rows="2"></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="box-info-2">
						<div class="box-head">
							Main Information<i class="fa fa-angle-up klik"></i>
						</div>
						<div class="box-panel">
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Pork Serving</label></div>
								<div class="col-8 col-4b">
									<div class="radio col-8a radio-inline radio-danger">
										<input type="radio" checked="" name="pork" value="Yes" id="pork1">
										<label for="pork1"> Yes </label>
									</div>
									<div class="radio col-8a radio-inline radio-danger">
										<input type="radio" name="pork" value="No" id="pork2">
										<label for="pork2"> No </label>
									</div>
									<div class="radio col-8a radio-inline radio-danger">
										<input type="radio" name="pork" value="Halal" id="pork3">
										<label for="pork3"> Halal </label>
									</div>
                    <div class="radio col-8a radio-inline radio-danger">
										<input type="radio" name="pork" value="vege" id="pork4">
										<label for="pork4"> Vegetarian </label>
									</div>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Alcohol Serving</label></div>
								<div class="col-8 col-4b">
									<div class="radio radio-inline radio-danger">
										<input type="radio" checked="" name="alcohol" value="Yes" id="alcohol1">
										<label for="alcohol1"> Yes </label>
									</div>
									<div class="radio radio-inline radio-danger">
										<input type="radio" name="alcohol" value="No" id="alcohol2">
										<label for="alcohol2"> No </label>
									</div>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Price Index <span class="f-merah">*</span></label></div>
								<div class="col-6 col-4b">
									<select name="price_index" required class="form-control" id="price_index">
										<option value="">Select Price Index</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from price_index");
										while($g=mysqli_fetch_array($sql)){
											echo"<option value='$g[id_price_index]'>$g[nama_price_index]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Suitable For <span class="f-merah">*</span></label></div>
								<div class="col-6 col-4b">
									<select name="suitable_for" required class="form-control" id="suitable_for">
										<option value="">Select Suitable For </option>
										<?php
										$sql=mysqli_query($koneksi,"select * from suitable_for order by nama_suitable_for asc");
										while($h=mysqli_fetch_array($sql)){
											echo"<option value='$h[id_suitable_for]'>$h[nama_suitable_for]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Wi-Fi <span class="f-merah">*</span></label></div>
								<div class="col-6 col-4b">
									<select name="wifi" required class="form-control" id="wifi">
										<option value="">Select Wi-Fi</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from wifi order by nama_wifi asc");
										while($n=mysqli_fetch_array($sql)){
											echo"<option value='$n[id_wifi]'>$n[nama_wifi]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Payment <span class="f-merah">*</span></label></div>
								<div class="col-6 col-4b">
									<select name="payment" required class="form-control" id="payment">
										<option value="">Select Payment</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from term_of_payment order by nama_term_of_payment asc");
										while($o=mysqli_fetch_array($sql)){
											echo"<option value='$o[id_term_of_payment]'>$o[nama_term_of_payment]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Serving Time</label></div>
								<div class="col-6 col-4b">
									<select name="serving_time" class="form-control">
										<option value="0">Select Serving Time</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from serving_time");
										while($k=mysqli_fetch_array($sql)){
											echo"<option value='$k[id_serving_time]'>$k[nama_serving_time]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Type of Service</label></div>
								<div class="col-6 col-4b">
									<select name="type_of_service" required class="form-control">
										<option value="0">Select Type of Service</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from type_of_service order by nama_type_of_service asc");
										while($l=mysqli_fetch_array($sql)){
											echo"<option value='$l[id_type_of_service]'>$l[nama_type_of_service]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Air Conditioning</label></div>
								<div class="col-6 col-4b">
									<select name="air_conditioning" required class="form-control">
										<option value="0">Select Air Conditioning</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from air_conditioning order by nama_air_conditioning asc");
										while($l1=mysqli_fetch_array($sql)){
											echo"<option value='$l1[id_air_conditioning]'>$l1[nama_air_conditioning]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Heating System</label></div>
								<div class="col-6 col-4b">
									<select name="heating_system" required class="form-control">
										<option value="0">Select Heating Conditioning</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from heating_system order by nama_heating_system asc");
										while($l2=mysqli_fetch_array($sql)){
											echo"<option value='$l2[id_heating_system]'>$l2[nama_heating_system]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Premise's Security</label></div>
								<div class="col-6 col-4b">
									<select name="premise_security" class="form-control">
										<option value="0">Select premise's Security</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from premise_security order by nama_premise_security asc");
										while($p=mysqli_fetch_array($sql)){
											echo"<option value='$p[id_premise_security]'>$p[nama_premise_security]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Parking Spaces</label></div>
								<div class="col-6 col-4b">
									<select name="parking_spaces" class="form-control">
										<option value="0">Select Parking Spaces</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from parking_spaces order by nama_parking_spaces asc");
										while($q=mysqli_fetch_array($sql)){
											echo"<option value='$q[id_parking_spaces]'>$q[nama_parking_spaces]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Premise's Fire Safety</label></div>
								<div class="col-6 col-4b">
									<select name="premise_fire_safety" class="form-control">
										<option value="0">Select Premise's Fire Safety</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from premise_fire_safety order by nama_premise_fire_safety asc");
										while($r=mysqli_fetch_array($sql)){
											echo"<option value='$r[id_premise_fire_safety]'>$r[nama_premise_fire_safety]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Premise's Hygiene</label></div>
								<div class="col-6 col-4b">
									<select name="premise_hygiene" class="form-control">
										<option value="0">Select Premise's Hygiene</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from premise_hygiene order by nama_premise_hygiene asc");
										while($r1=mysqli_fetch_array($sql)){
											echo"<option value='$r1[id_premise_hygiene]'>$r1[nama_premise_hygiene]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Premise's maintenance</label></div>
								<div class="col-6 col-4b">
									<select name="premise_maintenance" class="form-control">
										<option value="0">Select Premise Maintenance</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from premise_maintenance order by nama_premise_maintenance asc");
										while($s=mysqli_fetch_array($sql)){
											echo"<option value='$s[id_premise_maintenance]'>$s[nama_premise_maintenance]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Ambience</label></div>
								<div class="col-6 col-4b">
									<select name="ambience" class="form-control">
										<option value="0">Select Ambience</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from ambience order by nama_ambience asc");
										while($t=mysqli_fetch_array($sql)){
											echo"<option value='$t[id_ambience]'>$t[nama_ambience]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Attire</label></div>
								<div class="col-6 col-4b">
									<select name="attire" class="form-control">
										<option value="0">Select Attire </option>
										<?php
										$sql=mysqli_query($koneksi,"select * from attire order by nama_attire asc");
										while($u=mysqli_fetch_array($sql)){
											echo"<option value='$u[id_attire]'>$u[nama_attire]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Clean Washroom</label></div>
								<div class="col-6 col-4b">
									<select name="clean_washroom" class="form-control">
										<option value="0">Select Clean Washroom</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from clean_washroom order by nama_clean_washroom asc");
										while($v=mysqli_fetch_array($sql)){
											echo"<option value='$v[id_clean_washroom]'>$v[nama_clean_washroom]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Tables Availability</label></div>
								<div class="col-6 col-4b">
									<select name="tables_availability" class="form-control">
										<option value="0">Select Tables Availability</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from tables_availability order by nama_tables_availability asc");
										while($w=mysqli_fetch_array($sql)){
											echo"<option value='$w[id_tables_availability]'>$w[nama_tables_availability]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Noise Level</label></div>
								<div class="col-6 col-4b">
									<select name="noise_level" class="form-control">
										<option value="0">Select Noise Level</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from noise_level");
										while($x=mysqli_fetch_array($sql)){
											echo"<option value='$x[id_noise_level]'>$x[nama_noise_level]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Waiter Tipping</label></div>
								<div class="col-6 col-4b">
									<select name="waiter_tipping" class="form-control">
										<option value="0">Select Waiter Tipping</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from waiter_tipping order by nama_waiter_tipping asc");
										while($y=mysqli_fetch_array($sql)){
											echo"<option value='$y[id_waiter_tipping]'>$y[nama_waiter_tipping]</option>";
										}
										?>
									</select>
								</div>
							</div>
						</div>
					</div>
               <div class="box-info-2">
						<div class="box-head">
							Business Hours<i class="fa fa-angle-down klik"></i>
						</div>
						<div class="box-panel dn">
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Operational</label></div>
								<div class="col-6 col-4b">
									<select name="operation_hour" class="form-control">
										<option value='0'>No Information</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from operation_hour order by nama_operation_hour asc");
										while($e1=mysqli_fetch_array($sql)){
											$selected="";
											if($e1['id_operation_hour']==$e['id_operation_hour']){
												$selected="selected";
											}
											echo"<option value='$e1[id_operation_hour]' $selected>$e1[nama_operation_hour]</option>";
										}
										?>
									</select>
								</div>
							</div>

							<div class="row mb20">
								<div class="col-16 res-table">
									<table class="table">
										<tr>
											<td></td>
											<th colspan="2">Restaurant Hour 1</th>
											<th colspan="2">Restaurant Hour 2</th>
                                 <th colspan="2">Option</th>
										</tr>

										<tr>
											<th class="vc">Monday</th>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="resto_time3" name="resto_time3"></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="resto_time4" name="resto_time4"></td>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="resto_time3a" name="resto_time3a"></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="resto_time4a" name="resto_time4a"></td>
                                 <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                           				<input name="resto_monday" value="Off Day" id="resto_monday" type="checkbox">
                           				<label for="resto_monday"> Off Day</label>
                           			</div>
                                 </td>
                                 <td><div class="checkbox checkbox-inline checkbox-danger">
															 <div class="btn btn-danger" id="duplicate">
																		Apply All Days
																		</div>
														 </div></td>
										</tr>
										<tr>
											<th class="vc">Tuesday</th>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="resto_time5" name="resto_time5"></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="resto_time6" name="resto_time6"></td>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="resto_time5a" name="resto_time5a"></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="resto_time6a" name="resto_time6a"></td>
                                 <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                           				<input name="resto_tuesday" value="Off Day" id="resto_tuesday" type="checkbox">
                           				<label for="resto_tuesday"> Off Day</label>
                           			</div>
                                 </td>
                                 <td></td>
										</tr>
										<tr>
											<th class="vc">Wednesday</th>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="resto_time7" name="resto_time7"></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="resto_time8" name="resto_time8"></td>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="resto_time7a" name="resto_time7a"></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="resto_time8a" name="resto_time8a"></td>
                                 <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                           				<input name="resto_wednesday" value="Off Day" id="resto_wednesday" type="checkbox">
                           				<label for="resto_wednesday"> Off Day</label>
                           			</div>
                                 </td>
                                 <td></td>
										</tr>
										<tr>
											<th class="vc">Thursday</th>
											<td><input type="text" class="form-control time" placeholder="Open Time"  id="resto_time9" name="resto_time9"></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="resto_time10" name="resto_time10"></td>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="resto_time9a" name="resto_time9a"></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="resto_time10a" name="resto_time10a"></td>
                                 <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                           				<input name="resto_thursday" value="Off Day" id="resto_thursday" type="checkbox">
                           				<label for="resto_thursday"> Off Day</label>
                           			</div>
                                 </td>
                                 <td></td>
										</tr>
										<tr>
											<th class="vc">Friday</th>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="resto_time11" name="resto_time11"></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="resto_time12" name="resto_time12"></td>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="resto_time11a" name="resto_time11a"></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="resto_time12a" name="resto_time12a"></td>
                                 <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                           				<input name="resto_friday" value="Off Day" id="resto_friday" type="checkbox">
                           				<label for="resto_friday"> Off Day</label>
                           			</div>
                                 </td>
                                 <td></td>
										</tr>
										<tr>
											<th class="vc">Saturday</th>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="resto_time13" name="resto_time13"></td>
											<td><input type="text" class="form-control time" placeholder="Select Time" id="resto_time14" name="resto_time14"></td>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="resto_time13a" name="resto_time13a"></td>
											<td><input type="text" class="form-control time" placeholder="Select Time" id="resto_time14a" name="resto_time14a"></td>
                                 <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                           				<input name="resto_saturday" value="Off Day" id="resto_saturday" type="checkbox">
                           				<label for="resto_saturday"> Off Day</label>
                           			</div>
                                 </td>
                                 <td></td>
										</tr>
										<tr>
											<th class="vc">Sunday</th>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="resto_time1" name="resto_time1"></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="resto_time2" name="resto_time2"></td>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="resto_time1a" name="resto_time1a"></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="resto_time2a" name="resto_time2a"></td>
                                 <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                           				<input name="resto_sunday" value="Off Day" id="resto_sunday" type="checkbox">
                           				<label for="resto_sunday"> Off Day</label>
                           			</div>
                                 </td>
                                 <td>

                                 </td>
										</tr>
									</table>
								</div>
							</div>

                     <div class="row mb20">
								<div class="col-16 res-table">
									<table class="table">
										<tr>
											<td></td>
											<th colspan="2">Bar Hour 1</th>
											<th colspan="2">Bar Hour 2</th>
                                 <th colspan="2">Option</th>
										</tr>

										<tr>
											<th class="vc">Monday</th>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="bar_time3" name="bar_time3"></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="bar_time4" name="bar_time4"></td>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="bar_time3a" name="bar_time3a"></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="bar_time4a" name="bar_time4a"></td>
                                 <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                           				<input name="resto_monday" value="Off Day" id="resto_monday" type="checkbox">
                           				<label for="resto_monday"> Off Day</label>
                           			</div>
                                 </td>
                                 <td>
																	 <div class="checkbox checkbox-inline checkbox-danger">
																 <div class="btn btn-danger" id="duplicate2">
																			Apply All Days
																			</div>
															 </div></td>
										</tr>
										<tr>
											<th class="vc">Tuesday</th>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="bar_time5" name="bar_time5"></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="bar_time6" name="bar_time6"></td>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="bar_time5a" name="bar_time5a"></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="bar_time6a" name="bar_time6a"></td>
                                 <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                           				<input name="resto_tuesday" value="Off Day" id="resto_tuesday" type="checkbox">
                           				<label for="resto_tuesday"> Off Day</label>
                           			</div>
                                 </td>
                                 <td></td>
										</tr>
										<tr>
											<th class="vc">Wednesday</th>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="bar_time7" name="bar_time7"></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="bar_time8" name="bar_time8"></td>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="bar_time7a" name="bar_time7a"></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="bar_time8a" name="bar_time8a"></td>
                                 <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                           				<input name="resto_wednesday" value="Off Day" id="resto_wednesday" type="checkbox">
                           				<label for="resto_wednesday"> Off Day</label>
                           			</div>
                                 </td>
                                 <td></td>
										</tr>
										<tr>
											<th class="vc">Thursday</th>
											<td><input type="text" class="form-control time" placeholder="Open Time"  id="bar_time9" name="bar_time9"></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="bar_time10" name="bar_time10"></td>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="bar_time9a" name="bar_time9a"></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="bar_time10a" name="bar_time10a"></td>
                                 <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                           				<input name="resto_thursday" value="Off Day" id="resto_thursday" type="checkbox">
                           				<label for="resto_thursday"> Off Day</label>
                           			</div>
                                 </td>
                                 <td></td>
										</tr>
										<tr>
											<th class="vc">Friday</th>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="bar_time11" name="bar_time11"></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="bar_time12" name="bar_time12"></td>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="bar_time11a" name="bar_time11a"></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="bar_time12a" name="bar_time12a"></td>
                                 <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                           				<input name="resto_friday" value="Off Day" id="resto_friday" type="checkbox">
                           				<label for="resto_friday"> Off Day</label>
                           			</div>
                                 </td>
                                 <td></td>
										</tr>
										<tr>
											<th class="vc">Saturday</th>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="bar_time13" name="bar_time13"></td>
											<td><input type="text" class="form-control time" placeholder="Select Time" id="bar_time14" name="bar_time14"></td>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="bar_time13a" name="bar_time13a"></td>
											<td><input type="text" class="form-control time" placeholder="Select Time" id="bar_time14a" name="bar_time14a"></td>
                                 <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                           				<input name="resto_saturday" value="Off Day" id="resto_saturday" type="checkbox">
                           				<label for="resto_saturday"> Off Day</label>
                           			</div>
                                 </td>
                                 <td></td>
										</tr>
										<tr>
											<th class="vc">Sunday</th>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="bar_time1" name="bar_time1"></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="bar_time2" name="bar_time2"></td>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="bar_time1a" name="bar_time1a"></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="bar_time2a" name="bar_time2a"></td>
                                 <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                           				<input name="resto_sunday" value="Off Day" id="resto_sunday" type="checkbox">
                           				<label for="resto_sunday"> Off Day</label>
                           			</div>
                                 </td>
                                 <td>

                                 </td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="box-info-2">
						<div class="box-head">
							Map<i class="fa fa-angle-up klik"></i>
						</div>
						<div class="box-panel">
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Input Address</label></div>
                <div class="col-6 col-4b">
                  <input id="search_address" class="controls form-control" type="text" value="<?php echo"$e[restaurant_name]";?>" placeholder="Enter a location" /><br>
                  <div id="geoloc" class="btn btn-danger">Your Location</div>
                  <br>
								</div>
              </div>
							<div class="row">
								<div class="col-16"><br>
                  <div id="map-canvas" style="height:500px"></div>
									<ul id="geoData">
                    <input id="lat" name="lat" value="<?php echo"$e[latitude]";?>" type="hidden" />
                    <input id="long" name="lng" value="<?php echo"$e[longitude]";?>"  type="hidden" />
									</ul>
                  <?php
                  $sqls=mysqli_query($koneksi,"select * from negara where id_negara='$e[id_negara]' order by nama_negara asc");
                  $bs=mysqli_fetch_array($sqls);

                  if (empty($e['latitude'])) {
                    $lat = $bs['latitude_negara'];  $lng = $bs['longitude_negara']; $nama= $bs['nama_negara'];
                  }
                  else {
                    $lat = $e['latitude'];  $lng = $e['longitude']; $nama= $e['restaurant_name'];
                  }
                   ?>
                </div><!-- col-sm-6 -->
							</div>
						</div>
					</div>
					<div class="box-info-2">
						<div class="box-head">
							Restaurant Description<i class="fa fa-angle-down klik"></i>
						</div>
						<div class="box-panel dn">
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Description</label></div>
								<div class="col-8 col-4b">
									<textarea class="form-control" name="description" placeholder="Write Restaurant Description" rows="4"></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="box-info-2">
						<div class="box-head">
							Business Status<i class="fa fa-angle-down klik"></i>
						</div>
						<div class="box-panel dn">
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Status</label></div>
								<div class="col-8 col-4b">
									<div class="radio radio-inline radio-danger">
										<input type="radio" checked="" name="business_status" value="Ongoing" id="status1">
										<label for="status1"> Ongoing </label>
									</div>
									<div class="radio radio-inline radio-danger">
										<input type="radio" name="business_status" value="Renovation" id="status2">
										<label for="status2"> Renovation </label>
									</div>
									<div class="radio radio-inline radio-danger">
										<input type="radio" name="business_status" value="Relocate" id="status3">
										<label for="status3"> Relocate </label>
									</div>
									<div class="radio radio-inline radio-danger">
										<input type="radio" name="business_status" value="Ceased Operation" id="status4">
										<label for="status4"> Ceased Operation </label>
									</div>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Description</label></div>
								<div class="col-8 col-4b">
									<textarea class="form-control" name="business_description" placeholder="Write further information if applicable" rows="4"></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="box-info-2">
						<div class="box-head">
							Features and Facility<i class="fa fa-angle-down klik"></i>
						</div>
						<div class="box-panel dn">
							<div class="row mb20">
								<?php
								$sql=mysqli_query($koneksi,"select * from facility order by nama_facility asc");
								$t=1;
								while($s=mysqli_fetch_array($sql)){
									echo"<div class='col-5'>
										<div class='checkbox checkbox-inline checkbox-danger'>
											<input type='checkbox' name='facility[]' value='$s[nama_facility]' id='facility$t'>
											<label for='facility$t'> $s[nama_facility]</label>
										</div>
									</div>";
									$t++;
								}
								?>
							</div>
						</div>
					</div>
					<div class="box-info-2">
						<div class="box-head">
							Cuisine<i class="fa fa-angle-down klik"></i>
						</div>
						<div class="box-panel dn">
							<div class="row mb20">
								<?php
								$sql=mysqli_query($koneksi,"select * from cuisine order by nama_cuisine asc");
								while($c=mysqli_fetch_array($sql)){
									echo"<div class='col-5'>
										<div class='checkbox checkbox-inline checkbox-danger'>
											<input type='checkbox' name='cuisine[]' value='$c[nama_cuisine]' id='cuisine$t'>
											<label for='cuisine$t'> $c[nama_cuisine]</label>
										</div>
									</div>";
									$t++;
								}
								?>
							</div>
						</div>
					</div>
					<div class="box-info-2">
						<div class="box-head">
							Serving<i class="fa fa-angle-down klik"></i>
						</div>
						<div class="box-panel dn">
							<div class="row mb20">
								<?php
								$sql=mysqli_query($koneksi,"select * from serving order by nama_serving asc");
								while($i=mysqli_fetch_array($sql)){
									echo"<div class='col-5'>
										<div class='checkbox checkbox-inline checkbox-danger'>
											<input type='checkbox' name='serving[]' value='$i[nama_serving]' id='serving$t'>
											<label for='serving$t'> $i[nama_serving]</label>
										</div>
									</div>";
									$t++;
								}
								?>
							</div>
						</div>
					</div>
					<div class="box-info-2">
						<div class="box-head">
							Type of Serving<i class="fa fa-angle-down klik"></i>
						</div>
						<div class="box-panel dn">
							<div class="row mb20">
								<?php
								$sql=mysqli_query($koneksi,"select * from type_of_serving order by nama_type_of_serving asc");
								while($j=mysqli_fetch_array($sql)){
									echo"<div class='col-5'>
										<div class='checkbox checkbox-inline checkbox-danger'>
											<input type='checkbox' name='type_of_serving[]' value='$j[nama_type_of_serving]' id='type_of_serving$t'>
											<label for='type_of_serving$t'> $j[nama_type_of_serving]</label>
										</div>
									</div>";
									$t++;
								}
								?>
							</div>
						</div>
					</div>
					<div class="box-info-2">
						<div class="box-head">
							Menu Photo<i class="fa fa-angle-down klik"></i>
						</div>
						<div class="box-panel dn">
							<div class="menu_wrap">
								<div class="row mb20">
									<div class="col-17">
										<div class="fileinput fileinput-new" data-provides="fileinput">
										  <div class="fileinput-preview upload" data-trigger="fileinput"></div>
										  <input type="file" name="menu[]" class="hidden">
										</div>
									</div>
									<div class="col-4"><input type="text"  autocomplete="off" class="form-control mt30" placeholder="Photo Caption" name="menu_caption[]"></div>
								</div>
							</div>
							<button type="button" class="tambah_menu btn btn-success">Add More Photo</button>
						</div>
					</div>
					<div class="box-info-2">
						<div class="box-head">
							Restaurant Rate<i class="fa fa-angle-up klik"></i>
						</div>
						<div class="box-panel">
							<div role="tabpanel" id="tab-vote">
								<!-- Nav tabs -->
								<ul class="nav nav-tabs" role="tablist">
									<li role="presentation" class="active"><a href="#feature" aria-controls="feature" role="tab" data-toggle="tab">Feature Vote</a></li>
									<li role="presentation"><a href="#quick" aria-controls="quick" role="tab" data-toggle="tab">Quick Vote</a></li>
								</ul>
								<!-- Tab panes -->
								<div class="tab-content">
									<div role="tabpanel" class="tab-pane tab-panel active fade in" id="feature">
										<table class="tablebintang">
											<tr>
												<td>Cleanliness <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall cleanliness of the restaurant, including the washroom, dining area, food & beverage, kitchens, cooks and the servers."></i></td>
												<td><input type="hidden" class="rating" id="rat1" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="cleanlines"></td>

											</tr>
											<tr>
												<td>Customer Service <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall customer services of the restaurant including the recepcionist, the phone operators, the food & beverage servers, manages and cashiers."></i></td>
												<td><input type="hidden" class="rating" id="rat2" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="customer_services"></td>
											</tr>
											<tr>
												<td>Food &amp; Beverage <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall food & beverage assessment based on cleanliness, flavor, ingredient freshness, cooking or making, presentation & aroma and value for money."></i></td>
												<td><input type="hidden" class="rating" id="rat3" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="food_beverage"></td>
											</tr>
											<tr>
												<td>Comfort <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment of the restaurant's comfortless on the sitting arrangement whether the table is too close to each other or the light is too direct to your eyes to be uncomforted or the coziness environment which make you feel home and comfort, etc."></i></td>
												<td><input type="hidden" class="rating" id="rat4" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="comfort"></td>
											</tr>
											<tr>
												<td>Value for Money <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment on the restaurant's food & beverage, comfortless, and services they provides to us a whole compare to the price we paid."></i></td>
												<td><input type="hidden" class="rating" id="rat5" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="value_money"></td>
											</tr>
										</table>
									</div>
									<div role="tabpanel" class="tab-pane tab-panel fade" id="quick">
										<table class="tablebintang">
											<tr>
												<td><strong>Give your vote</strong></td>
												<td>

                                       <input type="hidden" class="rating" id="rat6" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" value="" name="rating"></td>
											</tr>
										</table>
									</div>
								</div>
                        <ul class="list-inline">
         						<li><i class="fa fa-star"></i> Poor</li>
         						<li><i class="fa fa-star"></i><i class="fa fa-star"></i> Below Average</li>
         						<li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Average</li>
         						<li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Very Good</li>
         						<li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Excellent</li>
         					</ul>
							</div>
						</div>
					</div>
					<button type="submit" class="btn btn-danger" id="sum">Submit</button> <button type="reset" class="btn btn-danger">Reset</button>
               <div class="btn btn-danger as btn-as">Submit</div> <div class="btn btn-danger as btn-as">Reset</div><br><br>
               <div class='alert-danger alert-dismissible as' role='alert' style="display:none; padding: 15px">
                  <strong>Please submit minimum one star for each rating categories in other to be rated</strong>
               </div>
				</form>
			</div>
		</div>
    </div>
    <?php include"config/inc/footer.php"; ?>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jasny-bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap-rating.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap-timepicker.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/select2.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.file-input.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
	 <script>
			$(document).ready(function() {
				$('.drawer').drawer();

			$('.tutup').click(function(){
				$('.iklanan').toggleClass('hilang', 500);
			});

			$('.cart').click(function(e){
				e.stopPropagation();
				$('.dis-cart').toggle();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$('.plus').click(function(e){
				e.stopPropagation();
				$('.dis-plus').toggle();
				$('.dis-cart').hide();
				$('.dis-share').hide();
			});
			$('.share').click(function(e){
				e.stopPropagation();
				$('.dis-share').toggle();
				$('.dis-plus').hide();
				$('.dis-cart').hide();
			});
			$('.drawer-toggle').click(function(){
				$('.dis-cart').hide();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$('.btn-cari').click(function(){
				$('.search_kiri').toggleClass('search_kiri_tam', 500);
			});
			$(document).click(function () {
				 var $el = $(".dis-share");
				 if ($el.is(":visible")) {
						$el.fadeOut(200);
				 }
				 var $ela = $(".dis-plus");
				 if ($ela.is(":visible")) {
						$ela.fadeOut(200);
				 }
				 var $elu = $(".dis-cart");
				 if ($elu.is(":visible")) {
						$elu.fadeOut(200);
				 }
			 });

			});
	 </script>
	<script type="text/javascript">
		$(document).ready(function () {
			$(document).on("contextmenu",function(e){
				if(e.target.nodeName != "INPUT" && e.target.nodeName != "TEXTAREA")
				e.preventDefault();
			});
			$.fn.disableTextSelect = function() {
				return this.each(function() {
					$(this).css({
						'MozUserSelect':'none',
						'webkitUserSelect':'none'
					}).attr('unselectable','on').bind('selectstart', function() {
						return false;
					});
				});
			};
			$('body').disableTextSelect();
		});
	</script>

   <script type="text/javascript">
            $("form").submit(function( event ) {
               var negara = $("#negara").val();
      			var propinsi = $("#propinsi").val();
      			var kota = $("#kota").val();
      			var search_box = $("#search-box").val();
      			var business_type = $("#business_type").val();
      			var tag = $("#tag").val();
      			var price_index = $("#price_index").val();
      			var suitable_for = $("#suitable_for").val();
      			var wifi = $("#wifi").val();
      			var payment = $("#payment").val();
               if (!(kota === "") && !(search_box === "") && !(business_type === "") && !(tag === "") && !(price_index === "") && !(suitable_for === "") && !(wifi === "") && !(payment === "")) {
                  $(".loadi").show();
               }
               else {

               }
            });
	</script>


   <style media="screen">
      .ko{
         border: 1px solid #CCC;
    padding: 5px;
    color: #9C9999;
    border-top: none;
      }
      .ko:hover{
         background: #F5F5F5;
      }
      #country-list{
         margin: 0;
         list-style: none;
         padding: 0px;
         max-height: 62px;
         position: relative;
         overflow: auto;
      }
   </style>

   <script>
   $(document).ready(function(){
      $(".as").hide();
      var rat1 = $("#rat1").val();
      var rat2 = $("#rat2").val();
      var rat3 = $("#rat3").val();
      var rat4 = $("#rat4").val();
      var rat5 = $("#rat5").val();
      var rat5 = $("#rat5").val();
      $("#rat1,#rat2,#rat3,#rat4,#rat5").change(function(){
         if ((rat1 < 1) && (rat2 < 1) && (rat3 < 1) && (rat4 < 1) && (rat5 < 1) ) {
            $(".btn-danger").hide();
            $(".as").show();
         }
      });

      $("#rat1,#rat2,#rat3,#rat4,#rat5").change(function(){
         var rat1 = $("#rat1").val();
         var rat2 = $("#rat2").val();
         var rat3 = $("#rat3").val();
         var rat4 = $("#rat4").val();
         var rat5 = $("#rat5").val();
         if ((rat1 > 0) && (rat2 > 0) && (rat3 > 0) && (rat4 > 0) && (rat5 > 0)) {
            $(".btn-danger").show();
            $(".as").hide();
         }
      });

      $("#res").click(function(){
          $("#rat1").val();
          $("#rat2").val();
          $("#rat3").val();
          $("#rat4").val();
          $("#rat5").val();
      });

   	$("#search-box").keyup(function(){
         var id = $("#kota").val();
   		$.ajax({
   		type: "POST",
   		url: "<?php echo"$base_url"; ?>/config/func/resto.php",
   		data:'keyword='+$(this).val() + '&id='+id,
   		success: function(data){
   			$("#suggesstion-box").show();
   			$("#suggesstion-box").html(data);
   			$("#search-box").css("background","#FFF");
   		}
   		});
   	});

      $("#duplicate").click(function(){
         var resto_time1 = $("#resto_time1").val();
         var resto_time2 = $("#resto_time2").val();
         var resto_time3 = $("#resto_time3").val();
         var resto_time4 = $("#resto_time4").val();
         var resto_time5 = $("#resto_time5").val();
         var resto_time6 = $("#resto_time6").val();
         var resto_time7 = $("#resto_time7").val();
         var resto_time8 = $("#resto_time8").val();
         var resto_time9 = $("#resto_time9").val();
         var resto_time10 = $("#resto_time10").val();
         var resto_time11 = $("#resto_time11").val();
         var resto_time12 = $("#resto_time12").val();
         var resto_time13 = $("#resto_time13").val();
         var resto_time14 = $("#resto_time14").val();

         var resto_time1a = $("#resto_time1a").val();
         var resto_time2a = $("#resto_time2a").val();
         var resto_time3a = $("#resto_time3a").val();
         var resto_time4a = $("#resto_time4a").val();
         var resto_time5a = $("#resto_time5a").val();
         var resto_time6a = $("#resto_time6a").val();
         var resto_time7a = $("#resto_time7a").val();
         var resto_time8a = $("#resto_time8a").val();
         var resto_time9a = $("#resto_time9a").val();
         var resto_time10a = $("#resto_time10a").val();
         var resto_time11a = $("#resto_time11a").val();
         var resto_time12a = $("#resto_time12a").val();
         var resto_time13a = $("#resto_time13a").val();
         var resto_time14a = $("#resto_time14a").val();

         $("#resto_time1").val(resto_time3);
         $("#resto_time2").val(resto_time4);
         $("#resto_time5").val(resto_time3);
         $("#resto_time6").val(resto_time4);
         $("#resto_time7").val(resto_time3);
         $("#resto_time8").val(resto_time4);
         $("#resto_time9").val(resto_time3);
         $("#resto_time10").val(resto_time4);
         $("#resto_time11").val(resto_time3);
         $("#resto_time12").val(resto_time4);
         $("#resto_time13").val(resto_time3);
         $("#resto_time14").val(resto_time4);

         $("#resto_time1a").val(resto_time3a);
         $("#resto_time2a").val(resto_time4a);
         $("#resto_time5a").val(resto_time3a);
         $("#resto_time6a").val(resto_time4a);
         $("#resto_time7a").val(resto_time3a);
         $("#resto_time8a").val(resto_time4a);
         $("#resto_time9a").val(resto_time3a);
         $("#resto_time10a").val(resto_time4a);
         $("#resto_time11a").val(resto_time3a);
         $("#resto_time12a").val(resto_time4a);
         $("#resto_time13a").val(resto_time3a);
         $("#resto_time14a").val(resto_time4a);
      });

      $("#duplicate2").click(function(){
         var bar_time1 = $("#bar_time1").val();
         var bar_time2 = $("#bar_time2").val();
         var bar_time3 = $("#bar_time3").val();
         var bar_time4 = $("#bar_time4").val();
         var bar_time5 = $("#bar_time5").val();
         var bar_time6 = $("#bar_time6").val();
         var bar_time7 = $("#bar_time7").val();
         var bar_time8 = $("#bar_time8").val();
         var bar_time9 = $("#bar_time9").val();
         var bar_time10 = $("#bar_time10").val();
         var bar_time11 = $("#bar_time11").val();
         var bar_time12 = $("#bar_time12").val();
         var bar_time13 = $("#bar_time13").val();
         var bar_time14 = $("#bar_time14").val();

         var bar_time1a = $("#bar_time1a").val();
         var bar_time2a = $("#bar_time2a").val();
         var bar_time3a = $("#bar_time3a").val();
         var bar_time4a = $("#bar_time4a").val();
         var bar_time5a = $("#bar_time5a").val();
         var bar_time6a = $("#bar_time6a").val();
         var bar_time7a = $("#bar_time7a").val();
         var bar_time8a = $("#bar_time8a").val();
         var bar_time9a = $("#bar_time9a").val();
         var bar_time10a = $("#bar_time10a").val();
         var bar_time11a = $("#bar_time11a").val();
         var bar_time12a = $("#bar_time12a").val();
         var bar_time13a = $("#bar_time13a").val();
         var bar_time14a = $("#bar_time14a").val();

         $("#bar_time3").val(bar_time3);
         $("#bar_time4").val(bar_time4);
         $("#bar_time5").val(bar_time3);
         $("#bar_time6").val(bar_time4);
         $("#bar_time7").val(bar_time3);
         $("#bar_time8").val(bar_time4);
         $("#bar_time9").val(bar_time3);
         $("#bar_time10").val(bar_tim42);
         $("#bar_time11").val(bar_time3);
         $("#bar_time12").val(bar_time4);
         $("#bar_time13").val(bar_time3);
         $("#bar_time14").val(bar_time4);

         $("#bar_time3a").val(bar_time3a);
         $("#bar_time4a").val(bar_time4a);
         $("#bar_time5a").val(bar_time3a);
         $("#bar_time6a").val(bar_time4a);
         $("#bar_time7a").val(bar_time3a);
         $("#bar_time8a").val(bar_time4a);
         $("#bar_time9a").val(bar_time3a);
         $("#bar_time10a").val(bar_tim42a);
         $("#bar_time11a").val(bar_time3a);
         $("#bar_time12a").val(bar_time4a);
         $("#bar_time13a").val(bar_time3a);
         $("#bar_time14a").val(bar_time4a);
      });
   });

   function selectCountry(val) {
   $("#search-box").val(val);
   $("#suggesstion-box").hide();
   }
   </script>

	<script type="text/javascript">
	$(document).ready(function () {
		$(".dn").hide();
		$("select").select2();

		$(".alert").fadeTo(5000, 500).fadeOut(500, function(){
			$(".alert").alert('close');
		});
		$('.tip').tooltip();
		$("#negara").change(function(){
			var id = $("#negara").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_propinsi.php",
				data: "id=" + id,
				success: function(data){
					$("#propinsi").html(data);
					$("#propinsi").fadeIn(2000);
				}
			});
		});
		$("#propinsi").change(function(){
			var id = $("#propinsi").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_kota.php",
				data: "id=" + id,
				success: function(data){
					$("#kota").html(data);
					$("#kota").fadeIn(2000);
				}
			});
		});
      $("#kota").change(function(){
			var id = $("#kota").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_mall.php",
				data: "id=" + id,
				success: function(data){
					$("#mall").html(data);
					$("#mall").fadeIn(2000);
				}
			});
		});



		$('.rating').rating();



      $(".box-head").click(function(){
         var targeta = $(this).children(".klik");
         $(targeta).toggleClass("fa-angle-up fa-angle-down");
			var target = $(this).parent().children(".box-panel");
			$(target).slideToggle();
		});

      $('.time').timepicker({
			defaultTime:''
		});

		var x = 1;
		$(".tambah_menu").click(function(e){ //on add input button click
			e.preventDefault();
			if(x < 30){ //max input box allowed
				x++; //text box increment
				$(".menu_wrap").append('<div class="row mb20"><div class="col-17"><div class="fileinput fileinput-new" data-provides="fileinput"> <div class="fileinput-preview upload" data-trigger="fileinput"></div><input type="file" name="menu[]" class="hidden"></div></div><div class="col-4"><input type="text" class="form-control mt15" placeholder="Photo Caption" name="menu_caption[]"></div><div class="col-1"><button type="button" class="hapus_menu btn btn-danger mt15">Remove</button></div></div>'); //add input box
			}
			if(x > 29){
				$(".tambah_menu").hide();
			}
		});

		$(".menu_wrap").on("click",".hapus_menu", function(e){ //user click on remove text
			e.preventDefault(); $(this).parent().parent('.row').remove(); x--;
			if($(".tambah_menu").is(":hidden")){
				$(".tambah_menu").show();
			}
		});








	});
	</script>

	<script type="text/javascript">
	$('#search_address').on('keypress', function(e) {
		return e.which !== 13;
	});

	$('.form-control').on('keypress', function (e) {
			var ingnore_key_codes = [34, 39];
			if ($.inArray(e.which, ingnore_key_codes) >= 0) {
					e.preventDefault();
					alert("\'\" is not allowed");
			} else {}
	});

		function initialize() {
		var marker;
		var latlng = new google.maps.LatLng(0.13183582116662096, 106.67724609375);
		var geocoder = new google.maps.Geocoder();
		var mapOptions = {
			center: latlng,
			zoom: 6,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			draggableCursor: "pointer",
			streetViewControl: false
		};

		var imageicon = 'https://www.foodieguidances.com/assets/images/foodie-point.png';

		var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
					var markers = new google.maps.Marker({
						position: latlng,
						map: map,
						icon : imageicon
					});

		google.maps.event.addListener(map, "click", function (location) {
			//map.setCenter(location.latLng);
			setLatLong(location.latLng.lat(), location.latLng.lng());

			marker.setMap(null);
			markers.setMap(null);

			marker = new google.maps.Marker({
				position: location.latLng,
				map: map,
				icon : imageicon
			});
			marker.setPosition(location.latLng);
			setGeoCoder(location.latLng);
		});

		var input = (document.getElementById('search_address'));

		var autocomplete = new google.maps.places.Autocomplete(input);
		autocomplete.bindTo('bounds', map);
		autocomplete.setTypes([]);

		var infowindow = new google.maps.InfoWindow();
		marker = new google.maps.Marker({
			map: map,
			anchorPoint: new google.maps.Point(0, -29),
			icon : imageicon
		});

		google.maps.event.addListener(autocomplete, 'place_changed', function() {
			//infowindow.close();
			marker.setVisible(true);
			var place = autocomplete.getPlace();

			if (!place.geometry) return;

			// If the place has a geometry, then present it on a map.
			if (place.geometry.viewport) {
				map.fitBounds(place.geometry.viewport);
			} else {
				map.setCenter(place.geometry.location);
				map.setZoom(17);  // Why 17? Because it looks good.
			}
			marker.setIcon(imageicon);
			map.setCenter(20);
			marker.setPosition(place.geometry.location);
			marker.setVisible(true);
			setLatLong(place.geometry.location.lat(), place.geometry.location.lng());

		});

		document.getElementById('geoloc').onclick=function(){
			if(navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function(position) {
					var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
					markers.setMap(null);
					map.setCenter(pos);
					marker.setPosition(pos);

					setGeoCoder(pos);
					setLatLong(position.coords.latitude, position.coords.longitude);

				}, function() {
					handleNoGeolocation(true);
				});
			} else {
				// Browser doesn't support Geolocation
				handleNoGeolocation(false);
			}
		};


		function setLatLong(lat, long) {
			document.getElementById('lat').value=lat;
			document.getElementById('long').value=long;
		}

		function setGeoCoder(pos) {
			geocoder.geocode({'location': pos}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					if (results[0]) {
						document.getElementById('search_address').value=results[0].formatted_address;
					} else {
						document.getElementById('search_address').value='';
					}
				} else {
					document.getElementById('search_address').value='';
				}
			});
		}
		}


		google.maps.event.addDomListener(window, 'load', initialize);

	</script>
	<script async defer
	  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCwYMs0LtJBgwkV937PkNKKR3dbEiD0zA&callback=initialize&libraries=places">
	</script>
</body>
</html>
<?php


?>
