<?php
function upload($nama_file,$folder,$lokasi_file,$lebar,$tinggi){
	$nama_folder = "../../$folder/";
	list($lebar_asli, $tinggi_asli, $source_type) = getimagesize($lokasi_file);
	$gambar_asli = imagecreatefromjpeg($lokasi_file);
	$ukuran_asli = $lebar_asli / $tinggi_asli;
	$ukuran = $lebar / $tinggi;
	if ($ukuran_asli > $ukuran) {
    	$tinggi_sementara = $tinggi;
    	$lebar_sementara = ( int ) ($tinggi * $ukuran_asli);
	} else {
    	/*jika gambar sama atau lebih tinggi*/
    	$lebar_sementara = $lebar;
    	$tinggi_sementara = ( int ) ($lebar / $ukuran_asli);
	}
	/*rubah ukuran gambar ke ukuran sementara*/
	$gambar_sementara = imagecreatetruecolor($lebar_sementara, $tinggi_sementara);
	imagecopyresampled($gambar_sementara,$gambar_asli,0, 0,0, 0,$lebar_sementara, $tinggi_sementara,$lebar_asli, $tinggi_asli);
	/*Copy cropped region from temporary image into the desired GD image*/
	$x_absis = ($lebar_sementara - $lebar) / 2;
	$y_absis = ($tinggi_sementara - $tinggi) / 2;
	$gambar_akhir = imagecreatetruecolor($lebar, $tinggi);
	imagecopy($gambar_akhir,$gambar_sementara,0, 0,$x_absis, $y_absis,$lebar, $tinggi);
	imagejpeg($gambar_akhir,$nama_folder.$nama_file);
	imagedestroy($gambar_akhir);
}
?>