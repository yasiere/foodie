<?php
session_start();
include "base_url.php";
if(!empty($_SESSION['food_member'])){
	include "../database/db.php";
	include "upload_gambar_bms.php";
	include "member_data.php";
	$sql=mysqli_query($koneksi,"select id_food from food where nama_food='$_POST[food]'");
	$ada=mysqli_num_rows($sql);
	if($ada==0){
		header("Location: ".$base_url."/".$username."/submit/error-food");
		exit();
	}
	date_default_timezone_set("Asia/Jakarta");
	$sekarang=date("Y-m-d H:i:s");

	$ukuran_maks	= 552428800;

	$nama_cover			= $_FILES['gambar']['name'];
	$ukuran_cover 		= $_FILES['gambar']['size'];
	$lokasi_cover		= $_FILES['gambar']['tmp_name'];
	list($width_cover, $height_cover) = getimagesize($lokasi_cover);
	if($ukuran_cover > $ukuran_maks or !preg_match("/.(gif|jpg|png)$/i", $nama_cover) or $width_cover < 800 or $height_cover < 600){
		$_SESSION['notif']     = "gambar";
		header("Location: ".$base_url."/".$username."/my-food-photo/new");
		exit();
	}
	else{
		$r=mysqli_fetch_array($sql);
		$cover=upload_gambar($nama_cover,$lokasi_cover,"../../assets/img/food");
		mysqli_query($koneksi,"INSERT INTO food_photo (id_member,id_food,nama_food_photo,gambar_food_photo) VALUES('$u[id_member]','$r[id_food]','$_POST[nama]','$cover')");
		// if($u['feed_submit']==1){
			mysqli_query($koneksi,"INSERT INTO activity (id_member,kat_act,deskripsi,tgl) VALUES('$id_member','feed_submit','Submit new food photo','$sekarang')");
		// }
		mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('food','$r[id_food]')");
		header("Location: ".$base_url."/".$username."/submit/food-photo");
	}
}
else{
	header("Location: ".$base_url."/login-area");
}
?>
