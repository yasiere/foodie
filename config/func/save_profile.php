<?php
session_start();
include "base_url.php";
if(!empty($_SESSION['food_member'])){
	include "../database/db.php";
	include "upload_gambar.php";
	include "member_data.php";

	$ukuran_maks	= 552428800;
	$error_cover = 0;
	$error_profil = 0;

	if(!empty($_FILES['cover']['tmp_name'])){
		$nama_cover1		= $_FILES['cover']['name'];
		$ukuran_cover1 		= $_FILES['cover']['size'];
		$lokasi_cover1		= $_FILES['cover']['tmp_name'];
		list($width_cover1, $height_cover1) = getimagesize($lokasi_cover1);
		if($ukuran_cover1 > $ukuran_maks or !preg_match("/.(gif|jpg|png)$/i", $nama_cover1) or $width_cover1 < 965 or $height_cover1 < 357){
			$error_cover=1;
		}
		else{
			if($u['gambar_landscape'] <> "landscape.jpg"){
				unlink("../../assets/img/member/$u[gambar_landscape]");
			}
			$landscape=upload_gambar($nama_cover1,$lokasi_cover1,"../../assets/img/member",965,357);
			mysqli_query($koneksi,"update member set gambar_landscape='$landscape' WHERE id_member = '$u[id_member]'");
		}
	}
	if(!empty($_FILES['profil']['tmp_name'])){
		$nama_cover2		= $_FILES['profil']['name'];
		$ukuran_cover2 		= $_FILES['profil']['size'];
		$lokasi_cover2		= $_FILES['profil']['tmp_name'];
		list($width_cover2, $height_cover2) = getimagesize($lokasi_cover2);
		if($ukuran_cover2 > $ukuran_maks or !preg_match("/.(gif|jpg|png)$/i", $nama_cover2) or $width_cover2 < 140 or $height_cover2 < 140){
			$error_profil=1;
		}
		else{
			if($u['gambar_thumb'] <> "thumb.jpg"){
				unlink("../../assets/img/member/$u[gambar_thumb]");
			}
			$profil=upload_gambar($nama_cover2,$lokasi_cover2,"../../assets/img/member",140,140);
			mysqli_query($koneksi,"update member set gambar_thumb='$profil' WHERE id_member = '$u[id_member]'");
		}
	}
	if($error_cover == 1 or $error_profil == 1){
		$_SESSION['notif']     = "gambar";
		header("Location: ".$base_url."/".$u['username']."/edit-profile");
		exit();
	}
	else{
		if(isset($_POST['nama_depan'])){
			mysqli_query($koneksi,"UPDATE member SET nama_depan = '$_POST[nama_depan]', nama_belakang = '$_POST[nama_belakang]', deskripsi = '$_POST[deskripsi]', id_negara = '$_POST[negara]' WHERE id_member   = '$u[id_member]'");
		}
		else{
			mysqli_query($koneksi,"UPDATE member SET deskripsi = '$_POST[deskripsi]', negara = '$_POST[negara]' WHERE id_member   = '$u[id_member]'");
		}
		$_SESSION['notif']     = "sukses";
		header("Location: ".$base_url."/".$u['username']."/edit-profile");
		exit();
	}
}
else{
	header("Location: ".$base_url."/login-area");
}
?>
