<?php
session_start();
include "base_url.php";
if(!empty($_SESSION['food_member'])){
	include "../database/db.php";
	include "upload_gambar_bms.php";
	include "member_data.php";
	include "id_masking.php";

	
		date_default_timezone_set("Asia/Jakarta");

		$sekarang=date("Y-m-d H:i:s");

		$ukuran_maks	= 552428800;

		$nama_cover			= $_FILES['cover_photo']['name'];
		$ukuran_cover 		= $_FILES['cover_photo']['size'];
		$lokasi_cover		= $_FILES['cover_photo']['tmp_name'];
		list($width_cover, $height_cover) = getimagesize($lokasi_cover);

		if($ukuran_cover > $ukuran_maks or !preg_match("/.(gif|jpg|png)$/i", $nama_cover) or $width_cover < 800 or $height_cover < 600){
			$_SESSION['notif']     = "gambar";
			echo var_dump($_POST);
			echo "asas";
			//header("Location: ".$base_url."/".$u['username']."/my-events/new");
			exit();
		}
		else{

			function strstr_after($haystack, $needle, $case_insensitive = false) {
			    $strpos = ($case_insensitive) ? 'stripos' : 'strpos';
			    $pos = $strpos($haystack, $needle);
			    if (is_int($pos)) {
			        return substr($haystack, $pos + strlen($needle));
			    }
			    // Most likely false or null
			    return $pos;
			}

			// $r=mysqli_fetch_array($sql);
			$categories = strstr_after($_POST['categories'], '##');
			//echo $categorie;
			$sql=mysqli_query($koneksi,"INSERT INTO `events`(`id_member`, `id_negara`, `id_propinsi`, `id_kota`, `nama_events`, address_events, id_category, `periode`, tag, id_price_index, `time1`, `time2`, `tgl_post`) 
										VALUES ('$u[id_member]', '$_POST[negara]', '$_POST[propinsi]', '$_POST[kota]', '$_POST[nama_events]', '$_POST[address_events]', '$_POST[categories]', '$_POST[periode]', '$_POST[tag]','$_POST[id_price_index]', '$_POST[events_time1]', '$_POST[events_time2]', '$sekarang')");
			
			$id_events=mysqli_insert_id($koneksi);


			if(!empty($_POST['rating'])){
				$r1=$_POST['rating'] * 0.30;
				$r2=$_POST['rating'] * 0.27;
				$r3=$_POST['rating'] * 0.23;
				$r4=$_POST['rating'] * 0.20;
			}
			else{
				$r1=$_POST['security'] * 0.30;
				$r2=$_POST['clean']    * 0.27;
				$r3=$_POST['comfort']  * 0.23;
				$r4=$_POST['organise'] * 0.20;
			}
			mysqli_query($koneksi,"INSERT INTO `events_rating`(`id_events`, `id_member`,`security`, `clean`, `comfort`, `organise`, `tgl_rating`) 
									VALUES ('$id_events','$_SESSION[food_member]','$r1','$r2','$r3','$r4','$sekarang')");

			
			// foreach($_POST['kiosk'] as $key){
			// 	mysqli_query($koneksi,"INSERT INTO events_kiosk(id_events, nama_events_kiosk) VALUES('$id_events','$key')");			
			// }

			// foreach($_POST['food'] as $key){
			// 	mysqli_query($koneksi,"INSERT INTO events_food(id_events, nama_events_food) VALUES('$id_events','$key')");			
			// }

			// foreach($_POST['beverage'] as $key){
			// 	mysqli_query($koneksi,"INSERT INTO events_beverage(id_events, nama_events_beverage) VALUES('$id_events','$key')");			
			// }

			if($sql == true){
				$cover=upload_gambar($nama_cover,$lokasi_cover,"../../assets/img/events");
				
				foreach($_FILES['menu']['name'] as $key1 => $menu){
					if ($_FILES['menu']['size'][$key1] <> 0){
						$nama_menu = $_FILES['menu']['name'][$key1];
						$lokasi_menu = $_FILES['menu']['tmp_name'][$key1];
						$caption_menu = $_POST['menu_caption'][$key1];
						$foto_menu=upload_gambar($nama_menu,$lokasi_menu,"../../assets/img/events");
						mysqli_query($koneksi,"INSERT INTO events_photo(id_member,id_events,gambar_events_photo,events_caption,tgl_post) 
																	VALUES('$u[id_member]','$id_events','$foto_menu','$caption_menu','$sekarang')");
					}
				}

				mysqli_query($koneksi,"UPDATE `events` SET `gambar`= '$cover' WHERE id_events = '$id_events'");
				mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('Events','$id_events')");
				mysqli_query($koneksi,"INSERT INTO activity (id_member,kat_act,deskripsi,tgl) VALUES('$u[id_member]','events_submit','Submit Events','$sekarang')");				

				
			}


			header('Location: '.$base_url.'/'.$u['username'].'/my-events/kiosk/'.id_masking($id_events));
			

		}
	
}
else{
	header("Location: ".$base_url."/login-area");
}
?>
