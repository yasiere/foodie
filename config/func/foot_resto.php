<style media="screen">
.black_overlay{
  display: none;
  position: absolute;
  top: 0%;
  left: 0%;
  width: 100%;
  height: 100%;
  background-color: black;
  z-index:1001;
  -moz-opacity: 0.8;
  opacity:.80;
  filter: alpha(opacity=80);
}
.white_content {
  display: none;
  position: fixed;
  top: 20%;
  left: 40%;
  width: 20%;
  height: 56%;
  padding: 24px;
  border: 5px solid #C00606;
  background-color: #FFF;
  z-index: 1002;
  overflow-y: auto;
  overflow-x: hidden;
}
</style>
<div id="light" class="white_content">
  <h3 style="font-size: 20px; margin: 0px 0px 15px;">Like</h3>
  <table class="table">
    <?php
      $idu=id_masking($_GET['id']);
      $me=mysqli_query($koneksi,"SELECT * FROM restaurant_like r left join member m on m.id_member=r.id_member where r.id_restaurant='$idu'");
      $no=1;
      $juma=mysqli_num_rows($me);
      if (!empty($juma)) {
        while ($tame=mysqli_fetch_array($me)) {
          $like_date=date("jS M, Y", strtotime($tame['tgl_like']));
            echo "<tr><td>$no.</td>
                  <td><img src='$base_url/assets/img/member/$tame[gambar_thumb]' class='foto_profil' width='30px'></td> <td>$tame[username]</td> <td>$like_date</td></tr>";
          $no++;
        }
      }
      else {
        echo "Empty";
      }
    ?>
  </table>
  <a style="position: absolute; border: 6px solid rgb(255, 255, 255); right: -6px; top: -6px; z-index: 999999999; background: #C00606; color: rgb(255, 255, 255); padding: 6px 10px;" href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'"><i class="fa fa-close"></i></a>
</div>
<div id="lighta" class="white_content">
  <h3 style="font-size: 20px; margin: 0px 0px 15px;">Recommendation</h3>
  <table class="table">
    <?php
      $idu=id_masking($_GET['id']);
      $me=mysqli_query($koneksi,"SELECT * FROM restaurant_rekomendasi r left join member m on m.id_member=r.id_member where r.id_restaurant='$idu'");
      $no=1;
      $juma=mysqli_num_rows($me);
      if (!empty($juma)) {
        while ($tame=mysqli_fetch_array($me)) {
          $like_date=date("jS M, Y", strtotime($tame['tgl_rekomendasi']));
            echo "<tr><td>$no.</td>
                  <td><img src='$base_url/assets/img/member/$tame[gambar_thumb]' class='foto_profil' width='30px'></td> <td>$tame[username]</td> <td>$like_date</td></tr>";
          $no++;
        }
      }
      else {
        echo "Empty";
      }
    ?>
  </table>
  <a style="position: absolute; border: 6px solid rgb(255, 255, 255); right: -6px; top: -6px; z-index: 999999999; background: #C00606; color: rgb(255, 255, 255); padding: 6px 10px;" href = "javascript:void(0)" onclick = "document.getElementById('lighta').style.display='none';document.getElementById('fadea').style.display='none'"><i class="fa fa-close"></i></a>
</div>

<div id="lightp" class="white_content" style="width: 450px; height: 675px;     overflow: hidden;">
  <select id="tahun-pi" name="">
    <?php
      $ar=mysqli_query($koneksi,"select YEAR(dates) as tahun from rank_food GROUP by dates limit 5");
      while ($arry=mysqli_fetch_array($ar)) {
      echo "<option value='$arry[tahun]'>$arry[tahun]</option>";
      };
     ?>
     <option value="<?php echo date('Y');?>" selected><?php echo date('Y');?></option>
  </select>
  <script type="text/javascript">
    $("#tahun-pi").change(function(){
      var tahun = $(this).val();
      var id = <?php echo $idu; ?>;
      $.ajax({
        type:"POST",
        url: "<?php echo"$base_url"; ?>/ajax_certificate_resto.php",
        data: "tahun=" + tahun + "&id=" + id,
        success: function(data){

          $('.fitin div').css('font-size', '30px');
          while( $('.fitin div').height() > $('.fitin').height() ) {
              $('.fitin div').css('font-size', (parseInt($('.fitin div').css('font-size')) - 1) + "px" );
          };
          $('.restos div').css('font-size', '16px');
          while( $('.restos div').height() > $('.restos').height() ) {
              $('.restos div').css('font-size', (parseInt($('.restos div').css('font-size')) - 1) + "px" );
          };
          $(".tahun-bawah").remove();
          $("#tahun-atas").html(data);
          $("#tahun-atas").fadeIn(2000);
        }
      });
    });
  </script>

    <div id="tahun-atas">
      <div class="tahun-bawah">
        <div class="piagam-isi ta2017">
          <div class="fitin">
            <div class="h1"><?php echo "$r[restaurant_name]"; ?></div>
          </div>
          <div class="restos">
            <div class="alamat"><?php echo "$r[street_address]"; ?></div>
          </div>
          <div class="negara"><?php echo $r['nama_negara']; ?></div>
          <div class="bintang"><?php echo "<input type='hidden' class='rating' data-filled='fa fa-star' data-empty='fa fa-star-o' readonly='readonly' value='$total_rating'>"; ?></div>
          <div class="rating"><?php echo "$total_rating / <em class='f-12'> $r[jumlah_vote] votes</em>"; ?></div>
          <span class="world"># <span style="  color: #ed1c24;"><?php echo "$rank_global"; ?></span></span>
          <span class="ranknegara"># <span style="  color: #ed1c24;"><?php echo "$rank_negara"; ?></span></span>
          <span class="rankstate"># <span style="  color: #ed1c24;"><?php echo "$rank_state"; ?></span></span>
          <span class="rankcity"># <span style="  color: #ed1c24;"><?php echo "$rank_city"; ?></span></span>
          <span class="rankmall"># <span style="  color: #ed1c24;"><?php echo "$rank_mall"; ?></span></span>
          <span class="tahun"><span><?php echo date("Y"); ?></span></span>
          <span class="tahuna"><span><?php echo date("Y"); ?></span></span>
        </div>
        <img src="<?php echo"$base_url";?>/assets/img/theme/certificate.jpg" alt="" width="100%">
      </div>
    </div>

  <a style="position: absolute; border: 6px solid rgb(255, 255, 255); right: -6px; top: -6px; z-index: 999999999; background: #C00606; color: rgb(255, 255, 255); padding: 6px 10px;" href = "javascript:void(0)" onclick = "document.getElementById('lightp').style.display='none';document.getElementById('fadep').style.display='none'"><i class="fa fa-close"></i></a>
</div>

<div id="fade" class="black_overlay"  onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'"></div>
<div id="fadea" class="black_overlay"  onclick = "document.getElementById('lighta').style.display='none';document.getElementById('fadea').style.display='none'"></div>
<div id="fadep" class="black_overlay"  onclick = "document.getElementById('lightp').style.display='none';document.getElementById('fadep').style.display='none'"></div>


<div id="map-pop" class="white_content-map">
<h3 style="font-size: 20px; margin: 0px 0px 15px;">Map</h3>
<table class="table" style="margin-bottom: 0">
  <div id="map" style="height:90%"></div><br>
  <a href="#" class="btn btn-danger btn-block get-directions" target="_blank" target="_blank">Get Direction to My location Now</a>

  <?php
    $idu=id_masking($_GET['id']);
    $me=mysqli_query($koneksi,"SELECT * FROM restaurant where id_restaurant='$idu' and latitude!=''");
    $no=1;
    $juma=mysqli_num_rows($me);
    if (!empty($juma)) {
      $r=mysqli_fetch_array($me); ?>
      <script>
        function initMap() {
          var lati = <?php echo $r[latitude]; ?>;
          var lngi = <?php echo $r[longitude]; ?>;

          var myLatLng = {lat: lati, lng: lngi};
          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 18,
            center: myLatLng,
          });
          var image = 'https://www.foodieguidances.com/assets/images/foodie-point.png';
          var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            icon:  image
          });

          google.maps.event.addListenerOnce(map, 'idle', function() {
              var center = map.getCenter()
              google.maps.event.trigger(map, "resize")
              map.setCenter(center)
          });

        }
        initMap();
    </script>
    <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCwYMs0LtJBgwkV937PkNKKR3dbEiD0zA">
    </script>
    <?php
    }
    else {
      echo "Empty";
      ?>
      <script>
      function initMap() {

      }
      </script>
      <?php
    }
  ?>
</table>
  <a style="position: absolute; border: 6px solid rgb(255, 255, 255); right: -6px; top: -6px; z-index: 999999999; background: #C00606; color: rgb(255, 255, 255); padding: 6px 10px;" href = "javascript:void(0)" onclick = "document.getElementById('lightp').style.display='none';document.getElementById('fadep').style.display='none'"><i class="fa fa-close"></i></a>
</div>
<div id="fade-map" class="black_overlay"  onclick = "document.getElementById('map-pop').style.display='none';document.getElementById('fade-map').style.display='none'"></div>


<script type="text/javascript">
  $(document).ready(function () {
    $('.klik-bintang').click(function(){
      $('#lightp').show();
      $('#fadep').show();

      $('.fitin div').css('font-size', '30px');
      while( $('.fitin div').height() > $('.fitin').height() ) {
          $('.fitin div').css('font-size', (parseInt($('.fitin div').css('font-size')) - 1) + "px" );
      };

      $('.restos div').css('font-size', '16px');
      while( $('.restos div').height() > $('.restos').height() ) {
          $('.restos div').css('font-size', (parseInt($('.restos div').css('font-size')) - 1) + "px" );
      };
    });

  });
</script>

<script type="text/javascript">
      var startingLocation;
      var destination = "<?php echo"$r[latitude],$r[longitude]"; ?>"; // replace this with any destination
      $('a.get-directions').click(function (e) {
        e.preventDefault();

        // check if browser supports geolocation
        if (navigator.geolocation) {

          // get user's current position
          navigator.geolocation.getCurrentPosition(function (position) {

            // get latitude and longitude
            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;
            startingLocation = latitude + "," + longitude;
            // send starting location and destination to goToGoogleMaps function
            goToGoogleMaps(startingLocation, destination);
          });
        }
        // fallback for browsers without geolocation
        else {
          // get manually entered postcode
          startingLocation = $('.manual-location').val();
          // if user has entered a starting location, send starting location and destination to goToGoogleMaps function
          if (startingLocation != '') {
            goToGoogleMaps(startingLocation, destination);
          }
          // else fade in the manual postcode field
          else {
            $('.no-geolocation').fadeIn();
          }
        }
        // go to Google Maps function - takes a starting location and destination and sends the query to Google Maps
        function goToGoogleMaps(startingLocation, destination) {
          window.location = "https://www.google.com/maps/dir/"+startingLocation + "/" + destination;
        }
      });
</script>
