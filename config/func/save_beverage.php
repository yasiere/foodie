<?php
session_start(); error_reporting(0);
include "base_url.php";
if(!empty($_SESSION['food_member'])){
	include "../database/db.php";
	include "upload_gambar_bms.php";
	include "member_data.php";
	include "id_masking.php";

	$sql=mysqli_query($koneksi,"select id_restaurant from restaurant where id_restaurant='$_POST[restaurant]'");
	$ada=mysqli_num_rows($sql);
	if($ada==0){
		header("Location: ".$base_url."/".$u['username']."/submit/error-resto");
		exit();
	}
	else{
		date_default_timezone_set("Asia/Jakarta");
		$sekarang=date("Y-m-d H:i:s");

		$ukuran_maks	= 552428800;

		$nama_cover			= $_FILES['gambar_beverage']['name'];
		$ukuran_cover 		= $_FILES['gambar_beverage']['size'];
		$lokasi_cover		= $_FILES['gambar_beverage']['tmp_name'];
		list($width_cover, $height_cover) = getimagesize($lokasi_cover);
		if($ukuran_cover > $ukuran_maks or !preg_match("/.(gif|jpg|png)$/i", $nama_cover) or $width_cover < 800 or $height_cover < 600){
			$_SESSION['notif']     = "gambar";
			header("Location: ".$base_url."/".$u['username']."/my-beverage/new");
			exit();
		}
		else{
			if(!empty($_POST['making'])){
				$making_seo = $_POST['making'];
				$making=implode('+',$making_seo);
			}
			else {
				$making="";
			}
			$r=mysqli_fetch_array($sql);
			$sql=mysqli_query($koneksi,"INSERT INTO beverage (id_member,id_restaurant,nama_beverage,id_beverage_category,id_price_index,making_methode,tgl_post, id_tag, type_serving) VALUES('$u[id_member]','$r[id_restaurant]','$_POST[beverage_name]','$_POST[category]','$_POST[price_index]','$making','$sekarang','$_POST[tag]', '$_POST[pork]')");
			if($sql){
				$cover=upload_gambar($nama_cover,$lokasi_cover,"../../assets/img/beverage");
				$id_beverage=mysqli_insert_id($koneksi);
				mysqli_query($koneksi,"INSERT INTO beverage_photo (id_member,id_beverage,nama_beverage_photo,gambar_beverage_photo) VALUES('$id_member','$id_beverage','$_POST[beverage_name]','$cover')");
				mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('Beverage','$id_beverage')");
				// if($u['feed_submit']==1){
					mysqli_query($koneksi,"INSERT INTO activity (id_member,kat_act,deskripsi,tgl) VALUES('$u[id_member]','feed_submit','Submit new beverage review','$sekarang')");
				// }
				if(!empty($_POST['rating']) or !empty($_POST['cleanlines']) or !empty($_POST['flavor']) or !empty($_POST['freshness']) or !empty($_POST['cooking']) or !empty($_POST['aroma']) or !empty($_POST['serving'])){
					if(!empty($_POST['rating'])){
						$r1=$_POST['rating'] * 0.21;
						$r2=$_POST['rating'] * 0.20;
						$r3=$_POST['rating'] * 0.18;
						$r4=$_POST['rating'] * 0.17;
						$r5=$_POST['rating'] * 0.15;
						$r6=$_POST['rating'] * 0.09;
					}
					else{
						$r1=$_POST['cleanlines'] * 0.21;
						$r2=$_POST['flavor'] * 0.20;
						$r3=$_POST['freshness'] * 0.18;
						$r4=$_POST['cooking'] * 0.17;
						$r5=$_POST['aroma'] * 0.15;
						$r6=$_POST['serving'] * 0.09;
					}
					mysqli_query($koneksi,"INSERT INTO beverage_rating (id_beverage,id_member,cleanlines,flavor,freshness,cooking,aroma,serving,tgl_beverage_rating) values('$id_beverage','$u[id_member]','$r1','$r2','$r3','$r4','$r5','$r6','$sekarang')");
				}
				$id = id_masking($id_beverage);
				$global=mysqli_query($koneksi,"SELECT id_beverage, id_negara, id_propinsi, id_kota, id_mall FROM beverage b left JOIN restaurant r on b.id_restaurant=r.id_restaurant");
				while ($b=mysqli_fetch_array($global)) {
					$rank=mysqli_fetch_array(mysqli_query($koneksi, "SELECT
						((SUM(f.cleanlines) + SUM(f.flavor) + SUM(f.freshness) + SUM(f.cooking) + SUM(f.aroma) + SUM(f.serving)) / COUNT(f.id_member)) as rank,
						(SELECT COUNT(h.id_member) FROM beverage_like h WHERE h.id_beverage=$b[id_beverage]) AS dilike,
						(select count(id_beverage_rating) as jumlah from beverage_rating where beverage_rating.id_beverage=$b[id_beverage]) as jumlah_vote
						 FROM	beverage_rating f WHERE $b[id_beverage] =f.id_beverage"));
						 mysqli_query($koneksi,"UPDATE beverage set jumlah='$rank[rank]', rank_vote='$rank[jumlah_vote]', rank_like='$rank[dilike]', negara='$b[id_negara]', propinsi='$b[id_propinsi]', kota='$b[id_kota]', mall='$b[id_mall]' where id_beverage='$b[id_beverage]'");
				}
				header("Location: ".$base_url."/pages/beverage/review/$id/".$_POST['beverage_name']."");
				exit();
			}
		}
	}
}
else{
	header("Location: ".$base_url."/login-area");
}
?>
