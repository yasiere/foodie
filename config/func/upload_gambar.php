<?php
function upload_gambar($nama_asli_file,$lokasi_asli_file,$lokasi_folder,$lebar,$tinggi){
	date_default_timezone_set("Asia/Jakarta");
	$sekarang=date("Y-m-d");
	$acak	= rand(0000,9999);
	$kaboom = explode(".", $nama_asli_file);
	$jenis_file = end($kaboom);
	$nama_baru = "foodieguidances.com_".$acak."_".$sekarang.".".$jenis_file;

	$upload_keserver = move_uploaded_file($lokasi_asli_file, "$lokasi_folder/$nama_asli_file");
	$file_asli = $lokasi_folder."/".$nama_asli_file;

    $jenis_file = strtolower($jenis_file);
    if ($jenis_file == "gif"){
		$img = imagecreatefromgif($file_asli);
    }
	 elseif($jenis_file =="png"){
		$img = imagecreatefrompng($file_asli);
    }
	 elseif($jenis_file =="jpeg"){
		$img = imagecreatefromjpeg($file_asli);
    }
	 else {
		$img = imagecreatefromjpeg($file_asli);
    }

	list($lebar_asli, $tinggi_asli) = getimagesize($file_asli);
    $ukuran_asli = $lebar_asli / $tinggi_asli;
	$ukuran = $lebar / $tinggi;
	if ($ukuran_asli > $ukuran) {
    	$tinggi_set = $tinggi;
    	$lebar_set = ( int ) ($tinggi * $ukuran_asli);
	} else {
    	$lebar_set = $lebar;
    	$tinggi_set = ( int ) ($lebar / $ukuran_asli);
	}
	$img_sementara = imagecreatetruecolor($lebar_set, $tinggi_set);
	imagecopyresampled($img_sementara,$img,0, 0,0, 0,$lebar_set, $tinggi_set, $lebar_asli, $tinggi_asli);
	$x0 = ($lebar_set - $lebar) / 2;
	$y0 = ($tinggi_set - $tinggi) / 2;
	$img_akhir = imagecreatetruecolor($lebar, $tinggi);
	imagecopy($img_akhir,$img_sementara,0, 0,$x0, $y0,$lebar, $tinggi);
	imagejpeg($img_akhir,$lokasi_folder."/".$nama_baru);
	imagedestroy($img_sementara);
	imagedestroy($img);
	unlink("$lokasi_folder/$nama_asli_file");
	return $nama_baru;
}
?>
