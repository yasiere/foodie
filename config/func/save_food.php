<?php
error_reporting(0);
session_start();

include "base_url.php";
if(!empty($_SESSION['food_member'])){
	include "../database/db.php";
	include "upload_gambar_bms.php";
	include "member_data.php";
	include "id_masking.php";

	$sql=mysqli_query($koneksi,"select id_restaurant from restaurant where id_restaurant='$_POST[restaurant]'");
	$ada=mysqli_num_rows($sql);
	if($ada==0){
		header("Location: ".$base_url."/".$u['username']."/submit/error-resto");
		exit();
	}
	else{
		date_default_timezone_set("Asia/Jakarta");
		$sekarang=date("Y-m-d H:i:s");

		$ukuran_maks	= 552428800;

		$nama_cover			= $_FILES['gambar_food']['name'];
		$ukuran_cover 		= $_FILES['gambar_food']['size'];
		$lokasi_cover		= $_FILES['gambar_food']['tmp_name'];
		list($width_cover, $height_cover) = getimagesize($lokasi_cover);
		if($ukuran_cover > $ukuran_maks or !preg_match("/.(gif|jpg|png)$/i", $nama_cover) or $width_cover < 800 or $height_cover < 600){
			$_SESSION['notif']     = "gambar";
			header("Location: ".$base_url."/".$username."/my-food/new");
			exit();
		}
		else{

			// $aDoor = $_POST['cooking'];
			//   if(!empty($aDoor))
			//   {
			//     $N = count($aDoor);
			//     for($i=0; $i < $N; $i++)
			//     {
			// 		$cooking=implode('+',$aDoor[$i]);
			//     }
			//   }

			if(!empty($_POST['cooking'])){
				$cooking_seo = $_POST['cooking'];
				$cooking=implode('+',$cooking_seo);
			}
			else {
				$cooking="";
			}


			if(!empty($_POST['cuisine'])){
				$cu_seo = $_POST['cuisine'];
				$cuisine=implode('+',$cu_seo);
			}
			else {
				$cuisine="";
			}
			$r=mysqli_fetch_array($sql);
			$sql=mysqli_query($koneksi,"INSERT INTO food (id_member,id_restaurant,nama_food,id_food_category,id_price_index,id_cuisine,cooking_methode,id_msg_level,tgl_posta,id_tag,type_serving) VALUES('$u[id_member]','$r[id_restaurant]','$_POST[food_name]','$_POST[category]','$_POST[price_index]','$cuisine','$cooking','$_POST[msg_level]','$sekarang','$_POST[tag]','$_POST[pork]')");
			if($sql){
				$cover=upload_gambar($nama_cover,$lokasi_cover,"../../assets/img/food");
				$id_food=mysqli_insert_id($koneksi);
				mysqli_query($koneksi,"INSERT INTO food_photo (id_member,id_food,nama_food_photo,gambar_food_photo) VALUES('$id_member','$id_food','$_POST[food_name]','$cover')");
				mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('Food','$id_food')");
				// if($u['feed_submit']==1){
					mysqli_query($koneksi,"INSERT INTO activity (id_member,kat_act,deskripsi,tgl) VALUES('$id_member','feed_submit','Submit new food review','$sekarang')");
				// }
				if(!empty($_POST['rating']) or !empty($_POST['cleanlines']) or !empty($_POST['flavor']) or !empty($_POST['freshness']) or !empty($_POST['cooking']) or !empty($_POST['aroma']) or !empty($_POST['serving'])){
					if(!empty($_POST['rating'])){
						$r1=$_POST['rating'] * 0.21;
						$r2=$_POST['rating'] * 0.20;
						$r3=$_POST['rating'] * 0.18;
						$r4=$_POST['rating'] * 0.17;
						$r5=$_POST['rating'] * 0.15;
						$r6=$_POST['rating'] * 0.09;
					}
					else{
						$r1=$_POST['cleanlines'] * 0.21;
						$r2=$_POST['flavor'] * 0.20;
						$r3=$_POST['freshness'] * 0.18;
						$r4=$_POST['cooking'] * 0.17;
						$r5=$_POST['aroma'] * 0.15;
						$r6=$_POST['serving'] * 0.09;
					}
					mysqli_query($koneksi,"INSERT INTO food_rating (id_food,id_member,cleanlines,flavor,freshness,cooking,aroma,serving,tgl_food_rating) values('$id_food','$u[id_member]','$r1','$r2','$r3','$r4','$r5','$r6','$sekarang')");
				}
				$id = id_masking($id_food);
				$global=mysqli_query($koneksi,"SELECT id_food, id_negara, id_propinsi, id_kota, id_mall FROM food f LEFT JOIN restaurant r on f.id_restaurant = r.id_restaurant");
				while ($b=mysqli_fetch_array($global)) {
					$rank=mysqli_fetch_array(mysqli_query($koneksi, "SELECT
						((SUM(f.cleanlines) + SUM(f.flavor) + SUM(f.freshness) + SUM(f.cooking) + SUM(f.aroma) + SUM(f.serving)) / COUNT(f.id_member)) as rank,
						(select count(id_food_rating) as jumlah from food_rating where food_rating.id_food=$b[id_food]) as jumlah_vote,
						(SELECT COUNT(h.id_member) FROM food_like h WHERE $b[id_food]=h.id_food) AS dilike
						 FROM	food_rating f WHERE $b[id_food] =f.id_food"));

					mysqli_query($koneksi,"UPDATE food set jumlah='$rank[rank]', rank_vote='$rank[jumlah_vote]', rank_like='$rank[dilike]', negara='$b[id_negara]', propinsi='$b[id_propinsi]', kota='$b[id_kota]', mall='$b[id_mall]' where id_food='$b[id_food]'");
				}
				header("Location: ".$base_url."/pages/food/review/$id/".$_POST['food_name']."");
				exit();
			}
		}
	}
}
else{
	header("Location: ".$base_url."/login-area");
}
?>
