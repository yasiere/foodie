<?php
function upload_gambar($nama_asli_file,$lokasi_asli_file,$lokasi_folder){
	$acak	= rand(0000,9999);
	$kaboom = explode(".", $nama_asli_file);
	$jenis_file = end($kaboom);
	$nama_baru = str_replace(' ', '_', $nama_asli_file);
	$nama_baru = "foodieguidances.com".$acak.$nama_asli_file;
	
	$upload_keserver = move_uploaded_file($lokasi_asli_file, "$lokasi_folder/$nama_asli_file");
	$file_asli = $lokasi_folder."/".$nama_asli_file;
	
    $jenis_file = strtolower($jenis_file);
    if ($jenis_file == "gif"){ 
		$img = imagecreatefromgif($file_asli);
    } else if($jenis_file =="png"){ 
		$img = imagecreatefrompng($file_asli);
    } else { 
		$img = imagecreatefromjpeg($file_asli);
    }
	
	list($lebar_asli, $tinggi_asli) = getimagesize($file_asli);
    $ukuran_asli = $lebar_asli / $tinggi_asli;
	
	$lokasi_watermark = "../../assets/img/mark-big.png";
	$watermark = imagecreatefrompng($lokasi_watermark);
	$ukuran = 800 / 600;
	if ($ukuran_asli > $ukuran) {
    	$tinggi = 600;
    	$lebar = ( int ) (600 * $ukuran_asli);
	} else {
    	$lebar = 800;
    	$tinggi = ( int ) (800 / $ukuran_asli);
	}
    $img_sementara = imagecreatetruecolor($lebar, $tinggi);
	imagecopyresampled($img_sementara,$img,0, 0,0, 0,$lebar, $tinggi, $lebar_asli, $tinggi_asli);
	$x0 = ($lebar - 800) / 2;
	$y0 = ($tinggi - 600) / 2;
	$img_akhir = imagecreatetruecolor(800, 600);
	imagecopy($img_akhir,$img_sementara,0, 0,$x0, $y0,800, 600);
	imagecopy($img_akhir, $watermark, 614, 502, 0, 0, 186, 98);
    imagejpeg($img_akhir,$lokasi_folder."/big_".$nama_baru);
	imagedestroy($img_sementara);
	imagedestroy($watermark);
	
	$lokasi_watermark = "../../assets/img/mark-medium.png";
	$watermark = imagecreatefrompng($lokasi_watermark);
	$ukuran = 400 / 300;
	if ($ukuran_asli > $ukuran) {
    	$tinggi = 300;
    	$lebar = ( int ) (300 * $ukuran_asli);
	} else {
    	$lebar = 400;
    	$tinggi = ( int ) (400 / $ukuran_asli);
	}
    $img_sementara = imagecreatetruecolor($lebar, $tinggi);
	imagecopyresampled($img_sementara,$img,0, 0,0, 0,$lebar, $tinggi, $lebar_asli, $tinggi_asli);
	$x0 = ($lebar - 400) / 2;
	$y0 = ($tinggi - 300) / 2;
	$img_akhir = imagecreatetruecolor(400, 300);
	imagecopy($img_akhir,$img_sementara,0, 0,$x0, $y0,400, 300);
	imagecopy($img_akhir, $watermark, 307, 251, 0, 0, 93, 49);
    imagejpeg($img_akhir,$lokasi_folder."/medium_".$nama_baru);
	imagedestroy($img_sementara);
	imagedestroy($watermark);
	
	$lokasi_watermark = "../../assets/img/mark-small.png";
	$watermark = imagecreatefrompng($lokasi_watermark);
	$ukuran = 220 / 165;
	if ($ukuran_asli > $ukuran) {
    	$tinggi = 165;
    	$lebar = ( int ) (165 * $ukuran_asli);
	} else {
    	$lebar = 220;
    	$tinggi = ( int ) (220 / $ukuran_asli);
	}
    $img_sementara = imagecreatetruecolor($lebar, $tinggi);
	imagecopyresampled($img_sementara,$img,0, 0,0, 0,$lebar, $tinggi, $lebar_asli, $tinggi_asli);
	$x0 = ($lebar - 220) / 2;
	$y0 = ($tinggi - 165) / 2;
	$img_akhir = imagecreatetruecolor(220, 165);
	imagecopy($img_akhir,$img_sementara,0, 0,$x0, $y0,220, 165);
	imagecopy($img_akhir, $watermark, 168, 139, 0, 0, 52, 26);
    imagejpeg($img_akhir,$lokasi_folder."/small_".$nama_baru);
	imagedestroy($img_sementara);
	imagedestroy($watermark);
	
	imagedestroy($img);
	unlink("$lokasi_folder/$nama_asli_file");
	return $nama_baru;
}
$cover=upload_gambar($_FILES['fupload']['name'],$_FILES['fupload']['tmp_name'],"../../assets/img/test","mark-all.png");
?>