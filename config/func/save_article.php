<?php
session_start();
include "base_url.php";
if(!empty($_SESSION['food_member'])){
	include "../database/db.php";
	include "upload_gambar_bms.php";
	include "member_data.php";

	date_default_timezone_set("Asia/Jakarta");
	$sekarang=date("Y-m-d H:i:s");

	$ukuran_maks	= 552428800;
	$error_gambar   = 0;
	foreach($_FILES['gambar']['name'] as $key1 => $gambar){
		if ($_FILES['gambar']['size'][$key1] <> 0){
			$nama		= $_FILES['gambar']['name'][$key1];
			$ukuran 	= $_FILES['gambar']['size'][$key1];
			$lokasi 	= $_FILES['gambar']['tmp_name'][$key1];
			list($width_gambar, $height_gambar) = getimagesize($lokasi);
			if($ukuran > $ukuran_maks or !preg_match("/.(gif|jpg|png)$/i", $nama) or $width_gambar < 800 or $height_gambar < 600){
				$error_gambar=1;
			}
		}
	}
	if($error_gambar == 1){
		$_SESSION['notif']     = "gambar";
		header("Location: ".$base_url."/".$u['username']."/my-article/new");
		exit();
	}
	else{
		$sql=mysqli_query($koneksi,"INSERT INTO article (id_member,nama_article,konten_article,tgl_post) VALUES('$u[id_member]','$_POST[nama]','$_POST[konten]','$sekarang')");
		if($sql){
			$id=mysqli_insert_id($koneksi);
			foreach($_FILES['gambar']['name'] as $key1 => $gambar){
				if ($_FILES['gambar']['size'][$key1] <> 0){
					$nama_gambar = $_FILES['gambar']['name'][$key1];
					$lokasi_gambar = $_FILES['gambar']['tmp_name'][$key1];
					$foto_gambar=upload_gambar($nama_gambar,$lokasi_gambar,"../../assets/img/article");
					mysqli_query($koneksi,"INSERT INTO article_photo(id_member,id_article,nama_article_photo,gambar_article_photo) VALUES('$id_member','$id','$_POST[nama]','$foto_gambar')");
				}
			}
			mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('Article','$id')");
			// if($u['feed_submit']==1){ 
				mysqli_query($koneksi,"INSERT INTO activity (id_member,kat_act,deskripsi,tgl) VALUES('$u[id_member]','feed_submit','Submit new article review','$sekarang')");
			// }
			header("Location: ".$base_url."/".$u['username']."/submit/article");
		}
	}
}
else{
	header("Location: ".$base_url."/login-area");
}
?>
