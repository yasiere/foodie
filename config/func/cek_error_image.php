<?php
function cek_error_image($filename,$ukuran_maks) {
	foreach($filename['tmp_name'] as $key1 => $value1){
		$nama		= $filename['name'][$key1];
		$ukuran 	= $filename['size'][$key1];
		if($ukuran > $ukuran_maks or !preg_match("/.(gif|jpg|png)$/i", $nama)){
			$error=1;
		}
		else{
			$error=0;
		}
	}
	return $error;
}
?>