<?php
session_start();
include "base_url.php";
if(!empty($_SESSION['food_member'])){
	include "../database/db.php";
	include "upload_gambar_bms.php";
	include "member_data.php";
	include "id_masking.php";
	include "seo.php";
	// include "$base_url/config/func/rank_beverage.php";
	date_default_timezone_set("Asia/Jakarta");
	$sekarang=date("Y-m-d H:i:s");

	$ukuran_maks	= 552428800;

	function strstr_after($haystack, $needle, $case_insensitive = false) {
	    $strpos = ($case_insensitive) ? 'stripos' : 'strpos';
	    $pos = $strpos($haystack, $needle);
	    if (is_int($pos)) {
	        return substr($haystack, $pos + strlen($needle));
	    }
	    // Most likely false or null
	    return $pos;
	}
	$categories = strstr_after($_POST['categories'], '##');
	mysqli_query($koneksi,"UPDATE `process` SET id_member = '$u[id_member]', type_process = '$_POST[process_type]', `nama_process`= '$_POST[nama_process]',`manufactured_by`= '$_POST[manufactured_by]',
		`id_negara`= '$_POST[negara]',`reviewed_country`= '$_POST[reviewed_country]',`month_of_manufactured`= '$_POST[month_of_manufactured]',
		`date_of_viewed`= '$_POST[date_of_viewed]',`date_of_expired`= '$_POST[date_of_expired]',`expired_periode`= '$_POST[expired_periode]',
		`id_price_index`= '$_POST[id_price_index]',`food_chemical_used`= '$_POST[food_chemical_used]',`categories`= '$categories',`tag`= '$_POST[tag]' where id_process='$_POST[id]'");
	
	if(!empty($_FILES['gambar_process']['tmp_name'])){
		$nama_cover			= $_FILES['gambar_process']['name'];
		$ukuran_cover 		= $_FILES['gambar_process']['size'];
		$lokasi_cover		= $_FILES['gambar_process']['tmp_name'];
		list($width_cover, $height_cover) = getimagesize($lokasi_cover);
		if($ukuran_cover > $ukuran_maks or !preg_match("/.(gif|jpg|png)$/i", $nama_cover) or $width_cover < 800 or $height_cover < 600){
			$_SESSION['notif']     = "gambar";
			header("Location: ".$base_url."/".$u['username']."/my-process/edit/".$_POST['id']);
			
		}
		else{
			
			$cover=upload_gambar($nama_cover,$lokasi_cover,"../../assets/img/process");
			mysqli_query($koneksi,"update process_photo set gambar_process_photo='$cover' where id_process='$_POST[id]'");
			unlink("../../assets/img/process/small_".$_POST['gambar_processku']); 
			unlink("../../assets/img/process/medium_".$_POST['gambar_processku']); 
			unlink("../../assets/img/process/big_".$_POST['gambar_processku']); 
			
		}
	}

	$_SESSION['notif']     = "sukses";
	// header("Location: ".$base_url."/".$u['username']."/my-beverage/edit/".$_POST['id']);
	$id = id_masking($_POST['id']);

	$slug=seo($_POST['nama_process']);
	mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('process','$_POST[id]')");
	$date=date("Y-m-d");

	mysqli_query($koneksi, "INSERT INTO `edit` (`id`, `jenis`, `tanggal`, `admin`) VALUES ('$_POST[id]', 'process', '$date', '$_SESSION[food_member]')");

	// header("Location: ".$base_url."/pages/process/info/".$id."/".$slug);
	header("Location: ".$base_url."/".$u['username']."/my-process/edit/".id_masking($_POST['id']));
	exit();
}
else{
	header("Location: ".$base_url."/login-area");
}
?>
