<?php $batas='25'; ?>
<div class="panel-group mt20 mb10" id="accordionsss" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default head-pan">
    <div class="panel-heading jud-pan" role="tab" id="resto1" data-toggle="collapse" data-parent="#accordionsss" href="#collapsesss1" aria-expanded="true" aria-controls="collapsesss1">
      <h4 class="panel-title">
        ❯ Restaurant
      </h4>
    </div>
    <div id="collapsesss1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="resto1">
      <div class="panel-body">
        <form method="get" action="<?php echo"$base_url/pages/restaurant/search/"; ?>" class="border-form" id="ale-form" onsubmit="myFunction()">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading1">
              <h4 class="panel-title">
                <a href="<?php echo"$base_url" ?>/pages/restaurant/search/?sort=total_vote+DESC&amp;batas=<?php echo $batas; ?>">Top Ranking</a>
              </h4>
            </div>
            <div class="panel-heading" role="tab" id="heading1">
              <h4 class="panel-title">
                <a href="<?php echo"$base_url" ?>/pages/restaurant/search/?sort=r.dilihat+DESC&amp;batas=<?php echo $batas; ?>">Popular</a>
              </h4>
            </div>
            <div class="panel-heading" role="tab" id="heading1">
              <h4 class="panel-title">
                <a href="<?php echo"$base_url" ?>/pages/restaurant/search/?sort=direkomendasi+DESC&amp;batas=<?php echo $batas; ?>">Recomended</a>
              </h4>
            </div>
            <div class="panel-heading" role="tab" id="heading14">
              <h4 class="panel-title">
                <a data-parent="#accordion" href="#collapse14" aria-expanded="true" aria-controls="collapse1">Opening Status</a>
              </h4>
            </div>
            <div id="collapse14" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading14">
              <div class="panel-body">
                <div class="form-group">
                  <select class="form-group" name="opening">
                    <option value="">Select Opening Status</option>
                    <option value="open" <?php if($_GET['opening']=='open'){ echo "selected";} ?>>Open</option>
                    <option value="close" <?php if($_GET['opening']=='close'){ echo "selected";} ?>>Close</option>
                    <option value="no" <?php if($_GET['opening']=='no'){ echo "selected";} ?>>No Information</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="panel-heading" role="tab" id="heading1">
              <h4 class="panel-title">
                <a data-toggle="collapse"  data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">Location Filter</a>
              </h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
              <div class="panel-body">
                <div class="form-group">
                  <label>Country</label>
                  <select class="form-control" name="country" id="negara">
                    <option value="" selected>Select Country</option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from negara order by nama_negara asc");
                    while($b=mysqli_fetch_array($sql)){
                      if($b['id_negara']==$_GET['country']){
                        echo"<option value='$b[id_negara]' selected>$b[nama_negara]</option>";
                      }
                      else{
                        echo"<option value='$b[id_negara]'>$b[nama_negara]</option>";
                      }
                    }
                    ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>States/Province</label>
                  <select class="form-control" name="state" id="propinsi">
                    <option value="" selected>Select States/ Province</option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from propinsi where id_negara='$_GET[country]' order by nama_propinsi asc");
                    while($b=mysqli_fetch_array($sql)){
                      if($b['id_propinsi']==$_GET['state']){
                        echo"<option value='$b[id_propinsi]' selected>$b[nama_propinsi]</option>";
                      }
                      else{
                        echo"<option value='$b[id_propinsi]'>$b[nama_propinsi]</option>";
                      }
                    }?>
                  </select>
                </div>
                <div class="form-group">
                  <label>City</label>
                  <select class="form-control" name="city" id="kota">
                    <option value="" selected>Select City</option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from kota where id_propinsi='$_GET[state]' order by nama_kota asc");
                    while($b=mysqli_fetch_array($sql)){
                      if($b['id_kota']==$_GET['city']){
                        echo"<option value='$b[id_kota]' selected>$b[nama_kota]</option>";
                      }
                      else{
                        echo"<option value='$b[id_kota]'>$b[nama_kota]</option>";
                      }
                    }?>
                  </select>
                </div>

              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading8lan">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse8lan" aria-expanded="true" aria-controls="collapse8lan">Landmark</a>
              </h4>
            </div>
            <div id="collapse8lan" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8lan">
              <div class="panel-body">
                <div class="form-group">
                  <select class="form-control" name="landmark">
                    <option value="" selected>Select Landmark </option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from landmark order by nama_landmark asc");
                    while($d=mysqli_fetch_array($sql)){
                      if($d['id_landmark']==$_GET['landmark']){
                        echo"<option value='$d[id_landmark]' selected>$d[nama_landmark]</option>";
                      }
                      else{
                        echo"<option value='$d[id_landmark]'>$d[nama_landmark]</option>";
                      }
                    }
                    ?>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading8sho">
              <h4 class="panel-title">
                <a data-toggle="collapse"  data-parent="#accordion" href="#collapse8sho" aria-expanded="true" aria-controls="collapse8sho">Shopping Mall</a>
              </h4>
            </div>
            <div id="collapse8sho" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8sho">
              <div class="panel-body">
                <div class="form-group">
                  <select name="mall" class="form-control" id="mall">
                    <option value="">Select Shopping Mall (You must select a city first)</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading8bus">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse8bus" aria-expanded="true" aria-controls="collapse8bus">Business Type</a>
              </h4>
            </div>
            <div id="collapse8bus"  class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8bus">
              <div class="panel-body">
                <div class="form-group">
                  <select class="form-control" name="business_type">
                    <option value="" selected>Select Business Type</option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from type_of_business order by nama_type_of_business asc");
                    while($a=mysqli_fetch_array($sql)){
                      if($a['id_type_of_business']==$_GET['business_type']){
                        echo"<option value='$a[id_type_of_business]' selected>$a[nama_type_of_business]</option>";
                      }
                      else{
                        echo"<option value='$a[id_type_of_business]'>$a[nama_type_of_business]</option>";
                      }
                    }
                    ?>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading5">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="true" aria-controls="collapse5">Type of Cuisine</a>
              </h4>
            </div>
            <div id="collapse5"  class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
              <div class="panel-body">
                <?php
                  $curr='';
                  $cuisine=tulis_cekboxxer($curr,$koneksi,'cuisine');
                  echo"$cuisine";
                ?>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading2">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">Restaurant Rating</a>
              </h4>
            </div>
            <div id="collapse2"  class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
              <div class="panel-body">
                <div class="form-group">
                  <label>Overall</label>
                  <select class="form-control" name="overall">
                    <option value="" selected>Select Overate Rate</option>
                    <option value="1" <?php if($_GET['overall']==1){ echo "selected";} ?>>1 Star</option>
                    <option value="2" <?php if($_GET['overall']==2){ echo "selected";}?>>2 Star</option>
                    <option value="3" <?php if($_GET['overall']==3){ echo "selected";}?>>3 Star</option>
                    <option value="4" <?php if($_GET['overall']==4){ echo "selected";}?>>4 Star</option>
                    <option value="5" <?php if($_GET['overall']==5){ echo "selected";}?>>5 Star</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Cleanliness</label>
                  <select class="form-control select" name="clean">
                    <option value="" selected>Select Rate of Cleanliness</option>
                    <option value="0.26" <?php if($_GET['clean']=='0.26'){ echo "selected";}?>>1 Star</option>
                    <option value="0.52" <?php if($_GET['clean']=='0.52'){ echo "selected";}?>>2 Star</option>
                    <option value="0.78" <?php if($_GET['clean']=='0.78'){ echo "selected";}?>>3 Star</option>
                    <option value="1.04" <?php if($_GET['clean']=='1.04'){ echo "selected";}?>>4 Star</option>
                    <option value="1.3" <?php if($_GET['clean']=='1.3'){ echo "selected";}?>>5 Star</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Customer Services</label>
                  <select class="form-control select" name="services">
                    <option value="" selected>Select Rate of Customer Services</option>
                    <option value="0.24" <?php if($_GET['services']=='0.24'){ echo "selected";}?>>1 Star</option>
                    <option value="0.48" <?php if($_GET['services']=='0.48'){ echo "selected";}?>>2 Star</option>
                    <option value="0.72" <?php if($_GET['services']=='0.72'){ echo "selected";}?>>3 Star</option>
                    <option value="0.96" <?php if($_GET['services']=='0.96'){ echo "selected";}?>>4 Star</option>
                    <option value="1.2" <?php if($_GET['services']=='1.2'){ echo "selected";}?>>5 Star</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Food &amp; Beverages</label>
                  <select class="form-control select" name="food">
                    <option value="" selected>Select Rate of Food &amp; Beverages</option>
                    <option value="0.23" <?php if($_GET['food']=='0.23'){ echo "selected";}?>>1 Star</option>
                    <option value="0.46" <?php if($_GET['food']=='0.46'){ echo "selected";}?>>2 Star</option>
                    <option value="0.69" <?php if($_GET['food']=='0.69'){ echo "selected";}?>>3 Star</option>
                    <option value="0.92" <?php if($_GET['food']=='0.96'){ echo "selected";}?>>4 Star</option>
                    <option value="1.15" <?php if($_GET['food']=='1.15'){ echo "selected";}?>>5 Star</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Comfort</label>
                  <select class="form-control select" name="comfort">
                    <option value="" selected>Select Rate of Comfort</option>
                    <option value="0.14" <?php if($_GET['comfort']=='0.14'){ echo "selected";}?>>1 Star</option>
                    <option value="0.28" <?php if($_GET['comfort']=='0.28'){ echo "selected";}?>>2 Star</option>
                    <option value="0.42" <?php if($_GET['comfort']=='0.42'){ echo "selected";}?>>3 Star</option>
                    <option value="0.56" <?php if($_GET['comfort']=='0.56'){ echo "selected";}?>>4 Star</option>
                    <option value="0.7" <?php if($_GET['comfort']=='0.7'){ echo "selected";}?>>5 Star</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Value for Money</label>
                  <select class="form-control select" name="money">
                    <option value="" selected>Select Rate of Value for Money</option>
                    <option value="0.13" <?php if($_GET['money']=='0.13'){ echo "selected";}?>>1 Star</option>
                    <option value="0.26" <?php if($_GET['money']=='0.26'){ echo "selected";}?>>2 Star</option>
                    <option value="0.39" <?php if($_GET['money']=='0.39'){ echo "selected";}?>>3 Star</option>
                    <option value="0.52" <?php if($_GET['money']=='0.52'){ echo "selected";}?>>4 Star</option>
                    <option value="0.65" <?php if($_GET['money']=='0.65'){ echo "selected";}?>>5 Star</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading3">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">Popular Filter</a>
              </h4>
            </div>
            <div id="collapse3"  class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
              <div class="panel-body">
                <div class="radio radio-danger mb10">
                  <input type="radio" name="alcohol" <?php if($_GET[alcohol]=='Yes'){echo "checked";}; ?> value="Yes" id="opt1">
                  <label for="opt1">Alcohol</label>
                </div>
                <div class="radio radio-danger mb10">
                  <input type="radio" name="alcohol" <?php if($_GET[alcohol]=='No'){echo "checked";}; ?> value="No" id="opt2">
                  <label for="opt2">No Alcohol</label>
                </div>
                <div class="radio radio-danger mb10">
                  <input type="radio" name="pork" <?php if($_GET[pork]=='Yes'){echo "checked";}; ?> value="Yes" id="opt3">
                  <label for="opt3">Pork</label>
                </div>
                <div class="radio radio-danger mb10">
                  <input type="radio" name="pork" <?php if($_GET[pork]=='No'){echo "checked";}; ?> value="No" id="opt4">
                  <label for="opt4">No Pork</label>
                </div>
                <div class="radio radio-danger mb10">
                  <input type="radio" name="pork" <?php if($_GET[pork]=='Halal'){echo "checked";}; ?> value="Halal" id="opt5">
                  <label for="opt5">Halal</label>
                </div>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading4">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="true" aria-controls="collapse4">Other Filter</a>
              </h4>
            </div>
            <div id="collapse4"  class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
              <div class="panel-body">
                <div class="form-group">
                  <label>Price Index</label>
                  <select class="form-control" name="price_index">
                    <option value="" selected>Select Price Index</option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from price_index order by nama_price_index asc");
                    while($g=mysqli_fetch_array($sql)){
                      if($g['id_price_index']==$_GET['price_index']){
                        echo"<option value='$g[id_price_index]' selected>$g[nama_price_index]</option>";
                      }
                      else{
                        echo"<option value='$g[id_price_index]'>$g[nama_price_index]</option>";
                      }
                    }
                    ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Suitable For</label>
                  <select class="form-control" name="suitable_for">
                    <option value="" selected>Select Suitable For</option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from suitable_for order by nama_suitable_for asc");
                    while($h=mysqli_fetch_array($sql)){
                      if($h['id_suitable_for']==$_GET['suitable_for']){
                        echo"<option value='$h[id_suitable_for]' selected>$h[nama_suitable_for]</option>";
                      }
                      else{
                        echo"<option value='$h[id_suitable_for]'>$h[nama_suitable_for]</option>";
                      }
                    }
                    ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Serving Time</label>
                  <select class="form-control" name="serving_time">
                    <option value="" selected>Select Serving Time</option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from serving_time order by nama_serving_time asc");
                    while($k=mysqli_fetch_array($sql)){
                      if($k['id_serving_time']==$_GET['serving_time']){
                        echo"<option value='$k[id_serving_time]' selected>$k[nama_serving_time]</option>";
                      }
                      else{
                        echo"<option value='$k[id_serving_time]'>$k[nama_serving_time]</option>";
                      }
                    }
                    ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Type of Service</label>
                  <select class="form-control" name="type_of_service">
                    <option value="" selected>Select Type of Service</option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from type_of_service order by nama_type_of_service asc");
                    while($l=mysqli_fetch_array($sql)){
                      if($l['id_type_of_service']==$_GET['type_of_service']){
                        echo"<option value='$l[id_type_of_service]' selected>$l[nama_type_of_service]</option>";
                      }
                      else{
                        echo"<option value='$l[id_type_of_service]'>$l[nama_type_of_service]</option>";
                      }
                    }
                    ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Air Conditioning</label>
                  <select class="form-control" name="air_conditioning">
                    <option value="" selected>Air Conditioning</option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from air_conditioning order by nama_air_conditioning asc");
                    while($l1=mysqli_fetch_array($sql)){
                      if($l1['id_air_conditioning']==$_GET['air_conditioning']){
                        echo"<option value='$l1[id_air_conditioning]' selected>$l1[nama_air_conditioning]</option>";
                      }
                      else{
                        echo"<option value='$l1[id_air_conditioning]'>$l1[nama_air_conditioning]</option>";
                      }
                    }
                    ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Heating System</label>
                  <select class="form-control" name="heating_system">
                    <option value="" selected>Heating System</option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from heating_system order by nama_heating_system asc");
                    while($l2=mysqli_fetch_array($sql)){
                      if($l2['id_heating_system']==$_GET['heating_system']){
                        echo"<option value='$l2[id_heating_system]' selected>$l2[nama_heating_system]</option>";
                      }
                      else{
                        echo"<option value='$l2[id_heating_system]'>$l2[nama_heating_system]</option>";
                      }
                    }
                    ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Wi-Fi</label>
                  <select class="form-control" name="wifi">
                    <option value="" selected>Select Wi-Fi</option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from wifi order by nama_wifi asc");
                    while($n=mysqli_fetch_array($sql)){
                      if($n['id_wifi']==$_GET['wifi']){
                        echo"<option value='$n[id_wifi]' selected>$n[nama_wifi]</option>";
                      }
                      else{
                        echo"<option value='$n[id_wifi]'>$n[nama_wifi]</option>";
                      }
                    }
                    ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Payments</label>
                  <select class="form-control" name="term_of_payment">
                    <option value="" selected>Select Payments</option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from term_of_payment order by nama_term_of_payment asc");
                    while($o=mysqli_fetch_array($sql)){
                      if($o['id_term_of_payment']==$_GET['term_of_payment']){
                        echo"<option value='$o[id_term_of_payment]' selected>$o[nama_term_of_payment]</option>";
                      }
                      else{
                        echo"<option value='$o[id_term_of_payment]'>$o[nama_term_of_payment]</option>";
                      }
                    }
                    ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Premise's Security</label>
                  <select class="form-control" name="premise_security">
                    <option value="" selected>Select Premise Security</option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from premise_security order by nama_premise_security asc");
                    while($p=mysqli_fetch_array($sql)){
                      if($p['id_premise_security']==$_GET['premise_security']){
                        echo"<option value='$p[id_premise_security]' selected>$p[nama_premise_security]</option>";
                      }
                      else{
                        echo"<option value='$p[id_premise_security]'>$p[nama_premise_security]</option>";
                      }
                    }
                    ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Parking Spaces</label>
                  <select class="form-control" name="parking_spaces">
                    <option value="" selected>Select Parking Spaces</option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from parking_spaces order by nama_parking_spaces asc");
                    while($q=mysqli_fetch_array($sql)){
                      if($q['id_parking_spaces']==$_GET['parking_spaces']){
                        echo"<option value='$q[id_parking_spaces]' selected>$q[nama_parking_spaces]</option>";
                      }
                      else{
                        echo"<option value='$q[id_parking_spaces]'>$q[nama_parking_spaces]</option>";
                      }
                    }
                    ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Premise's Fire Safety </label>
                  <select class="form-control" name="premise_fire_safety">
                    <option value="" selected>Select Premise's Fire Safety</option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from premise_fire_safety order by nama_premise_fire_safety asc");
                    while($r=mysqli_fetch_array($sql)){
                      if($r['id_premise_fire_safety']==$_GET['premise_fire_safety']){
                        echo"<option value='$r[id_premise_fire_safety]' selected>$r[nama_premise_fire_safety]</option>";
                      }
                      else{
                        echo"<option value='$r[id_premise_fire_safety]'>$r[nama_premise_fire_safety]</option>";
                      }
                    }
                    ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Premise's Hygiene</label>
                  <select class="form-control" name="premise_hygiene">
                    <option value="" selected>Select Premise Hygiene</option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from premise_hygiene order by nama_premise_hygiene asc");
                    while($r1=mysqli_fetch_array($sql)){
                      if($r1['id_premise_hygiene']==$_GET['premise_hygiene']){
                        echo"<option value='$r1[id_premise_hygiene]' selected>$r1[nama_premise_hygiene]</option>";
                      }
                      else{
                        echo"<option value='$r1[id_premise_hygiene]'>$r1[nama_premise_hygiene]</option>";
                      }
                    }
                    ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Premise's Maintenance</label>
                  <select class="form-control" name="premise_maintenance">
                    <option value="" selected>Select Premise Maintenance</option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from premise_maintenance order by nama_premise_maintenance asc");
                    while($s=mysqli_fetch_array($sql)){
                      if($s['id_premise_maintenance']==$_GET['premise_maintenance']){
                        echo"<option value='$s[id_premise_maintenance]' selected>$s[nama_premise_maintenance]</option>";
                      }
                      else{
                        echo"<option value='$s[id_premise_maintenance]'>$s[nama_premise_maintenance]</option>";
                      }
                    }
                    ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Ambience</label>
                  <select class="form-control" name="ambience">
                    <option value="" selected>Select Ambience</option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from ambience order by nama_ambience asc");
                    while($t=mysqli_fetch_array($sql)){
                      if($t['id_ambience']==$_GET['ambience']){
                        echo"<option value='$t[id_ambience]' selected>$t[nama_ambience]</option>";
                      }
                      else{
                        echo"<option value='$t[id_ambience]'>$t[nama_ambience]</option>";
                      }
                    }
                    ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Attire</label>
                  <select class="form-control" name="attire">
                    <option value="" selected>Select Attire</option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from attire order by nama_attire asc");
                    while($u=mysqli_fetch_array($sql)){
                      if($u['id_attire']==$_GET['attire']){
                        echo"<option value='$u[id_attire]' selected>$u[nama_attire]</option>";
                      }
                      else{
                        echo"<option value='$u[id_attire]'>$u[nama_attire]</option>";
                      }
                    }
                    ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Clean Washroom</label>
                  <select class="form-control" name="clean_washroom">
                    <option value="" selected>Select Clean Washroom</option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from clean_washroom order by nama_clean_washroom asc");
                    while($v=mysqli_fetch_array($sql)){
                      if($v['id_clean_washroom']==$_GET['clean_washroom']){
                        echo"<option value='$v[id_clean_washroom]' selected>$v[nama_clean_washroom]</option>";
                      }
                      else{
                        echo"<option value='$v[id_clean_washroom]'>$v[nama_clean_washroom]</option>";
                      }
                    }
                    ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Tables Availability</label>
                  <select class="form-control" name="tables_availability">
                    <option value="" selected>Select Tables Availability</option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from tables_availability order by nama_tables_availability asc");
                    while($w=mysqli_fetch_array($sql)){
                      if($w['id_tables_availability']==$_GET['tables_availability']){
                        echo"<option value='$w[id_tables_availability]' selected>$w[nama_tables_availability]</option>";
                      }
                      else{
                        echo"<option value='$w[id_tables_availability]'>$w[nama_tables_availability]</option>";
                      }
                    }
                    ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Noise Level</label>
                  <select class="form-control" name="noise_level">
                    <option value="" selected>Select Noise Level</option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from noise_level order by nama_noise_level asc");
                    while($x=mysqli_fetch_array($sql)){

                      if($x['id_noise_level']==$_GET['noise_level']){
                        echo"<option value='$x[id_noise_level]' selected>$x[nama_noise_level]</option>";
                      }
                      else{
                        echo"<option value='$x[id_noise_level]'>$x[nama_noise_level]</option>";
                      }
                    }
                    ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Waiter Tipping</label>
                  <select class="form-control" name="waiter_tipping">
                    <option value="" selected>Select Waiter Tipping</option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from waiter_tipping order by nama_waiter_tipping asc");
                    while($y=mysqli_fetch_array($sql)){
                      if($y['id_waiter_tipping']==$_GET['waiter_tipping']){
                        echo"<option value='$y[id_waiter_tipping]' selected>$y[nama_waiter_tipping]</option>";
                      }
                      else{
                        echo"<option value='$y[id_waiter_tipping]'>$y[nama_waiter_tipping]</option>";
                      }
                    }
                    ?>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading6">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="true" aria-controls="collapse6">Features and Facility</a>
              </h4>
            </div>
            <div id="collapse6"  class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
              <div class="panel-body">
                <?php
                  $fac='';
                  foreach($_GET['facility'] as $facility){
                    $fac.=$facility."+";
                  }
                  $facility=tulis_cekboxxer($fac,$koneksi,'facility');
                  echo"$facility";
                ?>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading7">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="true" aria-controls="collapse7">Serving</a>
              </h4>
            </div>
            <div id="collapse7"  class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
              <div class="panel-body">
                <?php
                  $ser='';
                  foreach($_GET['serving'] as $serving){
                    $ser.=$serving."+";
                  }
                  $serving=tulis_cekboxxer($ser,$koneksi,'serving');
                  echo"$serving";
                ?>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading8">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse8" aria-expanded="true" aria-controls="collapse8">Type of Serving</a>
              </h4>
            </div>
            <div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8">
              <div class="panel-body">
                <?php
                $$toss='';
                foreach($_GET['type_of_serving'] as $tos){
                  $toss.=$tos."+";
                }
                $type_of_serving=tulis_cekboxxer($toss,$koneksi,'type_of_serving');
                echo"$type_of_serving";
                ?>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading8tag">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse8tag" aria-expanded="true" aria-controls="collapse8tag">Tag</a>
              </h4>
            </div>
            <div id="collapse8tag" class='panel-collapse collapse' role="tabpanel" aria-labelledby="heading8tag">
              <div class="panel-body">
                <div class="form-group">
                  <div class="input-group">
                      <input class="form-control tag" name="tag" value="<?php echo"$_GET[tag]";?>">
                      <div class="input-group-addon">x</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <button type="submit" class="btn btn-success btn-block">Search</button>
          <a href="https://www.foodieguidances.com/pages/restaurant/search/" class="btn btn-warning btn-block">Reset</a>
        </form>
      </div>
    </div>
  </div>
</div>
