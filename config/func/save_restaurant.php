<?php
error_reporting(0);
session_start();

include "base_url.php";
if(!empty($_SESSION['food_member'])){
	include "../database/db.php";
	include "upload_gambar_bms.php";
	include "member_data.php";
	include "id_masking.php";
	include "$base_url/config/func/rank_restaurant.php";

	date_default_timezone_set("Asia/Jakarta");
	$sekarang=date("Y-m-d H:i:s");
	$ukuran_maks	= 552428800;

	$error_cover = 0;
	$error_menu = 0;
	$facility="";
	$cuisine="";
	$serving="";
	$type_of_serving="";
	if(!empty($_FILES['cover_photo']['tmp_name'])){
		$nama_cover			= $_FILES['cover_photo']['name'];
		$ukuran_cover 		= $_FILES['cover_photo']['size'];
		$lokasi_cover		= $_FILES['cover_photo']['tmp_name'];
		list($width_cover, $height_cover) = getimagesize($lokasi_cover);
		if($ukuran_cover > $ukuran_maks or !preg_match("/.(gif|jpg|png|jpeg)$/i", $nama_cover) or $width_cover < 600 or $height_cover < 600){
			$error_cover=1;
		}
	}
	foreach($_FILES['menu']['name'] as $key1 => $menu){
		if ($_FILES['menu']['size'][$key1] <> 0){
			$nama		= $_FILES['menu']['name'][$key1];
			$ukuran 	= $_FILES['menu']['size'][$key1];
			$lokasi 	= $_FILES['menu']['tmp_name'][$key1];
			list($width_menu, $height_menu) = getimagesize($lokasi);
			if($ukuran > $ukuran_maks or !preg_match("/.(gif|jpg|png|jpeg)$/i", $nama) or $width_menu < 600 or $height_menu < 600){
				$error_menu=1;
			}
		}
	}
	if($error_cover == 1 or $error_menu == 1){
		$_SESSION['notif']     = "gambar";
		header("Location: ".$base_url."/".$u['username']."/my-restaurant/new");
		exit();
	}
	else{
		if(!empty($_FILES['cover_photo']['tmp_name'])){
			$cover=upload_gambar($nama_cover,$lokasi_cover,"../../assets/img/restaurant");
		}
		if(!empty($_POST['facility'])){
			$fat_seo = $_POST['facility'];
			$facility=implode('+',$fat_seo);
		}
		if(!empty($_POST['cuisine'])){
			$cuisine_seo = $_POST['cuisine'];
			$cuisine=implode('+',$cuisine_seo);
		}
		if(!empty($_POST['serving'])){
			$serving_seo = $_POST['serving'];
			$serving=implode('+',$serving_seo);
		}
		if(!empty($_POST['type_of_serving'])){
			$type_of_serving_seo = $_POST['type_of_serving'];
			$type_of_serving=implode('+',$type_of_serving_seo);
		}

		if (empty($_POST['resto_sunday'])) {
			$resto_time1=$_POST['resto_time1'];
			$resto_time2=$_POST['resto_time2'];
			$resto_time1a=$_POST['resto_time1a'];
			$resto_time2a=$_POST['resto_time2a'];
		} else {
			$resto_time1="";
			$resto_time2="";
			$resto_time1a="";
			$resto_time2a="";
		}
		if (empty($_POST['resto_monday'])) {
			$resto_time3=$_POST['resto_time3'];
			$resto_time4=$_POST['resto_time4'];
			$resto_time3a=$_POST['resto_time3a'];
			$resto_time4a=$_POST['resto_time4a'];
		} else {
			$resto_time3="";
			$resto_time4="";
			$resto_time3a="";
			$resto_time4a="";
		}
		if (empty($_POST['resto_tuesday'])) {
			$resto_time5=$_POST['resto_time5'];
			$resto_time6=$_POST['resto_time6'];
			$resto_time5a=$_POST['resto_time5a'];
			$resto_time6a=$_POST['resto_time6a'];
		} else {
			$resto_time5="";
			$resto_time6="";
			$resto_time5a="";
			$resto_time6a="";
		}
		if (empty($_POST['resto_wednesday'])) {
			$resto_time7=$_POST['resto_time7'];
			$resto_time8=$_POST['resto_time8'];
			$resto_time7a=$_POST['resto_time7a'];
			$resto_time8a=$_POST['resto_time8a'];
		} else {
			$resto_time7="";
			$resto_time8="";
			$resto_time7a="";
			$resto_time8a="";
		}
		if (empty($_POST['resto_thursday'])) {
			$resto_time9=$_POST['resto_time9'];
			$resto_time10=$_POST['resto_time10'];
			$resto_time9a=$_POST['resto_time9a'];
			$resto_time10a=$_POST['resto_time10a'];
		} else {
			$resto_time9="";
			$resto_time10="";
			$resto_time9a="";
			$resto_time10a="";
		}
		if (empty($_POST['resto_friday'])) {
			$resto_time11=$_POST['resto_time11'];
			$resto_time12=$_POST['resto_time12'];
			$resto_time11a=$_POST['resto_time11a'];
			$resto_time12a=$_POST['resto_time12a'];
		} else {
			$resto_time11="";
			$resto_time12="";
			$resto_time11a="";
			$resto_time12a="";
		}
		if (empty($_POST['resto_saturday'])) {
			$resto_time13=$_POST['resto_time13'];
			$resto_time14=$_POST['resto_time14'];
			$resto_time13a=$_POST['resto_time13a'];
			$resto_time14a=$_POST['resto_time14a'];
		} else {
			$resto_time13="";
			$resto_time14="";
			$resto_time13a="";
			$resto_time14a="";
		}


		if (empty($_POST['bar_sunday'])) {
			$bar_time1=$_POST['bar_time1'];
			$bar_time2=$_POST['bar_time2'];
			$bar_time1a=$_POST['bar_time1a'];
			$bar_time2a=$_POST['bar_time2a'];
		} else {
			$bar_time1="";
			$bar_time2="";
			$bar_time1a="";
			$bar_time2a="";
		}
		if (empty($_POST['bar_monday'])) {
			$bar_time3=$_POST['bar_time3'];
			$bar_time4=$_POST['bar_time4'];
			$bar_time3a=$_POST['bar_time3a'];
			$bar_time4a=$_POST['bar_time4a'];
		} else {
			$bar_time3="";
			$bar_time4="";
			$bar_time3a="";
			$bar_time4a="";
		}
		if (empty($_POST['bar_tuesday'])) {
			$bar_time5=$_POST['bar_time5'];
			$bar_time6=$_POST['bar_time6'];
			$bar_time5a=$_POST['bar_time5a'];
			$bar_time6a=$_POST['bar_time6a'];
		} else {
			$bar_time5="";
			$bar_time6="";
			$bar_time5a="";
			$bar_time6a="";
		}
		if (empty($_POST['bar_wednesday'])) {
			$bar_time7=$_POST['bar_time7'];
			$bar_time8=$_POST['bar_time8'];
			$bar_time7a=$_POST['bar_time7a'];
			$bar_time8a=$_POST['bar_time8a'];
		} else {
			$bar_time7="";
			$bar_time8="";
			$bar_time7a="";
			$bar_time8a="";
		}
		if (empty($_POST['bar_thursday'])) {
			$bar_time9=$_POST['bar_time9'];
			$bar_time10=$_POST['bar_time10'];
			$bar_time9a=$_POST['bar_time9a'];
			$bar_time10a=$_POST['bar_time10a'];
		} else {
			$bar_time9="";
			$bar_time10="";
			$bar_time9a="";
			$bar_time10a="";
		}
		if (empty($_POST['bar_friday'])) {
			$bar_time11=$_POST['bar_time11'];
			$bar_time12=$_POST['bar_time12'];
			$bar_time11a=$_POST['bar_time11a'];
			$bar_time12a=$_POST['bar_time12a'];
		} else {
			$bar_time11="";
			$bar_time12="";
			$bar_time11a="";
			$bar_time12a="";
		}
		if (empty($_POST['bar_saturday'])) {
			$bar_time13=$_POST['bar_time13'];
			$bar_time14=$_POST['bar_time14'];
			$bar_time13a=$_POST['bar_time13a'];
			$bar_time14a=$_POST['bar_time14a'];
		} else {
			$bar_time13="";
			$bar_time14="";
			$bar_time13a="";
			$bar_time14a="";
		}

		$sql=mysqli_query($koneksi,"INSERT INTO restaurant(id_member,restaurant_name,id_type_of_business,tag,street_address,id_negara,id_propinsi,
		id_kota,postal_code,id_landmark,id_mall,telephone,fax,reservation_phone,email_resto,web,facebook,twitter,instagram,
		pinterest,branch_name,restaurant_description,business_status,business_status_description,id_operation_hour,
		resto_time1,resto_time1a,resto_time2,resto_time2a,resto_time3,resto_time3a,resto_time4,resto_time4a,resto_time5,resto_time5a,resto_time6,resto_time6a,resto_time7,resto_time7a,resto_time8,resto_time8a,resto_time9,resto_time9a,resto_time10,resto_time10a,resto_time11,resto_time11a,resto_time12,resto_time12a,resto_time13,resto_time13a,resto_time14,resto_time14a,

		bar_time1,bar_time1a,bar_time2,bar_time2a,bar_time3,bar_time3a,bar_time4,bar_time4a,bar_time5,bar_time5a,bar_time6,bar_time6a,bar_time7,bar_time7a,bar_time8,bar_time8a,bar_time9,bar_time9a,bar_time10,bar_time10a,bar_time11,bar_time11a,bar_time12,bar_time12a,bar_time13,bar_time13a,bar_time14,bar_time14a,
		pork_serving,alcohol_serving,cuisine,id_price_index,id_suitable_for,serving,type_of_serving,id_serving_time,id_type_of_service,
		id_air_conditioning,id_heating_system,facility,id_wifi,id_term_of_payment,id_premise_security,id_premise_fire_safety,id_premise_hygiene,
		id_premise_maintenance,id_parking_spaces,id_ambience,id_attire,id_clean_washroom,id_tables_availability,id_noise_level,id_waiter_tipping,
		latitude,longitude,tgl_post)
		VALUES ('$u[id_member]','$_POST[restaurant_name]','$_POST[type_of_business]','$_POST[tag]','$_POST[street_address]','$_POST[negara]','$_POST[propinsi]',
		'$_POST[kota]','$_POST[postal_code]','$_POST[landmark]','$_POST[mall]','$_POST[telephone]','$_POST[fax]','$_POST[reservation_phone]','$_POST[email_resto]','$_POST[web]','$_POST[facebook]','$_POST[twitter]','$_POST[instagram]',
		'$_POST[pinterest]','$_POST[branch_name]','$_POST[description]','$_POST[business_status]','$_POST[business_description]','$_POST[operation_hour]',
		'$resto_time1','$resto_time1a','$resto_time2','$resto_time2a','$resto_time3','$resto_time3a','$resto_time4','$resto_time4a','$resto_time5','$resto_time5a','$resto_time6','$resto_time6a','$resto_time7','$resto_time7a','$resto_time8','$resto_time8a','$resto_time9','$resto_time9a','$resto_time10','$resto_time10a','$resto_time11','$resto_time11a','$resto_time12','$resto_time12a','$resto_time13','$resto_time13a','$resto_time14','$resto_time14a',

		'$bar_time1','$bar_time1a','$bar_time2','$bar_time2a','$bar_time3','$bar_time3a','$bar_time4','$bar_time4a','$bar_time5','$bar_time5a','$bar_time6','$bar_time6a','$bar_time7','$bar_time7a','$bar_time8','$bar_time8a','$bar_time9','$bar_time9a','$bar_time10','$bar_time10a','$bar_time11','$bar_time11a','$bar_time12','$bar_time12a','$bar_time13','$bar_time13a','$bar_time14','$bar_time14a',
		'$_POST[pork]','$_POST[alcohol]','$cuisine','$_POST[price_index]','$_POST[suitable_for]','$serving','$type_of_serving','$_POST[serving_time]','$_POST[type_of_service]',
		'$_POST[air_conditioning]','$_POST[heating_system]','$facility','$_POST[wifi]','$_POST[payment]','$_POST[premise_security]','$_POST[premise_fire_safety]','$_POST[premise_hygiene]',
		'$_POST[premise_maintenance]','$_POST[parking_spaces]','$_POST[ambience]','$_POST[attire]','$_POST[clean_washroom]','$_POST[tables_availability]','$_POST[noise_level]','$_POST[waiter_tipping]',
		'$_POST[lat]','$_POST[lng]','$sekarang')");
		if($sql){
			$id_restaurant=mysqli_insert_id($koneksi);
			if(!empty($_FILES['cover_photo']['tmp_name'])){
				mysqli_query($koneksi,"INSERT INTO restaurant_photo (id_member,id_restaurant,nama_restaurant_photo,gambar_restaurant_photo) VALUES('$u[id_member]','$id_restaurant','$_POST[restaurant_name]','$cover')");
			}
			mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('Restaurant','$id_restaurant')");
			// if($u['feed_submit']==1){
				mysqli_query($koneksi,"INSERT INTO activity (id_member,kat_act,deskripsi,tgl) VALUES('$u[id_member]','restaurant','Submit new restaurant review','$sekarang')");
			// }
			foreach($_FILES['menu']['name'] as $key1 => $menu){
				if ($_FILES['menu']['size'][$key1] <> 0){
					$nama_menu = $_FILES['menu']['name'][$key1];
					$lokasi_menu = $_FILES['menu']['tmp_name'][$key1];
					$caption_menu = $_POST['menu_caption'][$key1];
					$foto_menu=upload_gambar($nama_menu,$lokasi_menu,"../../assets/img/menu");
					mysqli_query($koneksi,"INSERT INTO menu(id_member,id_restaurant,nama_menu,gambar_menu,tgl_post) VALUES('$u[id_member]','$id_restaurant','$caption_menu','$foto_menu','$sekarang')");
				}
			}
			if(!empty($_POST['rating']) or !empty($_POST['cleanlines']) or !empty($_POST['customer_services']) or !empty($_POST['food_beverage']) or !empty($_POST['comfort']) or !empty($_POST['value_money'])){
				if(!empty($_POST['rating'])){
					$r1=$_POST['rating'] * 0.26;
					$r2=$_POST['rating'] * 0.24;
					$r3=$_POST['rating'] * 0.23;
					$r4=$_POST['rating'] * 0.14;
					$r5=$_POST['rating'] * 0.13;
				}
				else{
					$r1=$_POST['cleanlines'] * 0.26;
					$r2=$_POST['customer_services'] * 0.24;
					$r3=$_POST['food_beverage'] * 0.23;
					$r4=$_POST['comfort'] * 0.14;
					$r5=$_POST['value_money'] * 0.13;
				}
				mysqli_query($koneksi,"INSERT INTO restaurant_rating (id_restaurant,id_member,cleanlines,customer_services,food_beverage,comfort,value_money,tgl_restaurant_rating)
				values('$id_restaurant','$u[id_member]','$r1','$r2','$r3','$r4','$r5','$sekarang')");
			}
			$id = id_masking($id_restaurant);
			header("Location: ".$base_url."/pages/restaurant/review/$id/$restaurant_name");
			exit();
		}
	}
}
else{
	header("Location: ".$base_url."/login-area");
	exit();
}
?>
