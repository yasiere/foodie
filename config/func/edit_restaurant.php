<?php
// echo "$_POST[lat]";
session_start();
include "base_url.php";
if(!empty($_SESSION['food_member'])){

	include "../database/db.php";
	include "upload_gambar_bms.php";
	include "member_data.php";
	include "id_masking.php";
	include "seo.php";
	// include "$base_url/config/func/rank_restaurant.php";
	date_default_timezone_set("Asia/Jakarta");
	$sekarang=date("Y-m-d H:i:s");
	$date=date("Y-m-d");
	$fat_seo = $_POST['facility'];
	if (!empty($fat_seo)) {
		$facility=implode('+',$fat_seo);
	}

	$cuisine_seo = $_POST['cuisine'];
	if (!empty($cuisine_seo)) {
		$cuisine=implode('+',$cuisine_seo);
	}

	if (!empty($_POST['serving'])) {
		$serving_seo = $_POST['serving'];
		if (!empty($serving_seo)) {
			$serving=implode('+',$serving_seo);
		}
	}
	else {
		$serving="";
	}

	if (!empty($_POST['type_of_serving'])) {
		$type_of_serving_seo = $_POST['type_of_serving'];
		if (!empty($type_of_serving_seo)) {
			$type_of_serving=implode('+',$type_of_serving_seo);
		}
	}
	else {
		$type_of_serving="";
	}

	if (empty($_POST['resto_sunday'])) {
		$resto_time1=$_POST['resto_time1'];
		$resto_time2=$_POST['resto_time2'];
		$resto_time1a=$_POST['resto_time1a'];
		$resto_time2a=$_POST['resto_time2a'];
	} else {
		$resto_time1="";
		$resto_time2="";
		$resto_time1a="";
		$resto_time2a="";
	}
	if (empty($_POST['resto_monday'])) {
		$resto_time3=$_POST['resto_time3'];
		$resto_time4=$_POST['resto_time4'];
		$resto_time3a=$_POST['resto_time3a'];
		$resto_time4a=$_POST['resto_time4a'];
	} else {
		$resto_time3="";
		$resto_time4="";
		$resto_time3a="";
		$resto_time4a="";
	}
	if (empty($_POST['resto_tuesday'])) {
		$resto_time5=$_POST['resto_time5'];
		$resto_time6=$_POST['resto_time6'];
		$resto_time5a=$_POST['resto_time5a'];
		$resto_time6a=$_POST['resto_time6a'];
	} else {
		$resto_time5="";
		$resto_time6="";
		$resto_time5a="";
		$resto_time6a="";
	}
	if (empty($_POST['resto_wednesday'])) {
		$resto_time7=$_POST['resto_time7'];
		$resto_time8=$_POST['resto_time8'];
		$resto_time7a=$_POST['resto_time7a'];
		$resto_time8a=$_POST['resto_time8a'];
	} else {
		$resto_time7="";
		$resto_time8="";
		$resto_time7a="";
		$resto_time8a="";
	}
	if (empty($_POST['resto_thursday'])) {
		$resto_time9=$_POST['resto_time9'];
		$resto_time10=$_POST['resto_time10'];
		$resto_time9a=$_POST['resto_time9a'];
		$resto_time10a=$_POST['resto_time10a'];
	} else {
		$resto_time9="";
		$resto_time10="";
		$resto_time9a="";
		$resto_time10a="";
	}
	if (empty($_POST['resto_friday'])) {
		$resto_time11=$_POST['resto_time11'];
		$resto_time12=$_POST['resto_time12'];
		$resto_time11a=$_POST['resto_time11a'];
		$resto_time12a=$_POST['resto_time12a'];
	} else {
		$resto_time11="";
		$resto_time12="";
		$resto_time11a="";
		$resto_time12a="";
	}
	if (empty($_POST['resto_saturday'])) {
		$resto_time13=$_POST['resto_time13'];
		$resto_time14=$_POST['resto_time14'];
		$resto_time13a=$_POST['resto_time13a'];
		$resto_time14a=$_POST['resto_time14a'];
	} else {
		$resto_time13="";
		$resto_time14="";
		$resto_time13a="";
		$resto_time14a="";
	}


	if (empty($_POST['bar_sunday'])) {
		$bar_time1=$_POST['bar_time1'];
		$bar_time2=$_POST['bar_time2'];
		$bar_time1a=$_POST['bar_time1a'];
		$bar_time2a=$_POST['bar_time2a'];
	} else {
		$bar_time1="";
		$bar_time2="";
		$bar_time1a="";
		$bar_time2a="";
	}
	if (empty($_POST['bar_monday'])) {
		$bar_time3=$_POST['bar_time3'];
		$bar_time4=$_POST['bar_time4'];
		$bar_time3a=$_POST['bar_time3a'];
		$bar_time4a=$_POST['bar_time4a'];
	} else {
		$bar_time3="";
		$bar_time4="";
		$bar_time3a="";
		$bar_time4a="";
	}
	if (empty($_POST['bar_tuesday'])) {
		$bar_time5=$_POST['bar_time5'];
		$bar_time6=$_POST['bar_time6'];
		$bar_time5a=$_POST['bar_time5a'];
		$bar_time6a=$_POST['bar_time6a'];
	} else {
		$bar_time5="";
		$bar_time6="";
		$bar_time5a="";
		$bar_time6a="";
	}
	if (empty($_POST['bar_wednesday'])) {
		$bar_time7=$_POST['bar_time7'];
		$bar_time8=$_POST['bar_time8'];
		$bar_time7a=$_POST['bar_time7a'];
		$bar_time8a=$_POST['bar_time8a'];
	} else {
		$bar_time7="";
		$bar_time8="";
		$bar_time7a="";
		$bar_time8a="";
	}
	if (empty($_POST['bar_thursday'])) {
		$bar_time9=$_POST['bar_time9'];
		$bar_time10=$_POST['bar_time10'];
		$bar_time9a=$_POST['bar_time9a'];
		$bar_time10a=$_POST['bar_time10a'];
	} else {
		$bar_time9="";
		$bar_time10="";
		$bar_time9a="";
		$bar_time10a="";
	}
	if (empty($_POST['bar_friday'])) {
		$bar_time11=$_POST['bar_time11'];
		$bar_time12=$_POST['bar_time12'];
		$bar_time11a=$_POST['bar_time11a'];
		$bar_time12a=$_POST['bar_time12a'];
	} else {
		$bar_time11="";
		$bar_time12="";
		$bar_time11a="";
		$bar_time12a="";
	}
	if (empty($_POST['bar_saturday'])) {
		$bar_time13=$_POST['bar_time13'];
		$bar_time14=$_POST['bar_time14'];
		$bar_time13a=$_POST['bar_time13a'];
		$bar_time14a=$_POST['bar_time14a'];
	} else {
		$bar_time13="";
		$bar_time14="";
		$bar_time13a="";
		$bar_time14a="";
	}


	$sql=mysqli_query($koneksi,"UPDATE restaurant SET restaurant_name='$_POST[restaurant_name]',id_type_of_business='$_POST[type_of_business]',tag='$_POST[tag]',street_address='$_POST[street_address]',
	id_negara='$_POST[negara]',id_propinsi='$_POST[propinsi]',id_kota='$_POST[kota]',postal_code='$_POST[postal_code]',id_landmark='$_POST[landmark]',id_mall='$_POST[mall]',telephone='$_POST[telephone]',fax='$_POST[facsimile]',
	reservation_phone='$_POST[reservation_phone]',email_resto='$_POST[email_resto]',web='$_POST[web]',facebook='$_POST[facebook]',twitter='$_POST[twitter]',instagram='$_POST[instagram]',
	pinterest='$_POST[pinterest]',branch_name='$_POST[branch_name]',restaurant_description='$_POST[description]',business_status='$_POST[business_status]',
	business_status_description='$_POST[business_description]',id_operation_hour='$_POST[operation_hour]',
resto_time1='$resto_time1',resto_time1a='$resto_time1a',
resto_time2='$resto_time2',resto_time2a='$resto_time2a',
resto_time3='$resto_time3',resto_time3a='$resto_time3a',
resto_time4='$resto_time4',resto_time4a='$resto_time4a',
resto_time5='$resto_time5',resto_time5a='$resto_time5a',
resto_time6='$resto_time6',resto_time6a='$resto_time6a',
resto_time7='$resto_time7',resto_time7a='$resto_time7a',
resto_time8='$resto_time8',resto_time8a='$resto_time8a',
resto_time9='$resto_time9',resto_time9a='$resto_time9a',
resto_time10='$resto_time10',resto_time10a='$resto_time10a',
resto_time11='$resto_time11',resto_time11a='$resto_time11a',
resto_time12='$resto_time12',resto_time12a='$resto_time12a',
resto_time13='$resto_time13',resto_time13a='$resto_time13a',
resto_time14='$resto_time14',resto_time14a='$resto_time14a',
bar_time1='$_POST[bar_time1]',bar_time1a='$_POST[bar_time1a]',
bar_time2='$_POST[bar_time2]',bar_time2a='$_POST[bar_time2a]',
bar_time3='$_POST[bar_time3]',bar_time3a='$_POST[bar_time3a]',
bar_time4='$_POST[bar_time4]',bar_time4a='$_POST[bar_time4a]',
bar_time5='$_POST[bar_time5]',bar_time5a='$_POST[bar_time5a]',
bar_time6='$_POST[bar_time6]',bar_time6a='$_POST[bar_time6a]',
bar_time7='$_POST[bar_time7]',bar_time7a='$_POST[bar_time7a]',
bar_time8='$_POST[bar_time8]',bar_time8a='$_POST[bar_time8a]',
bar_time9='$_POST[bar_time9]',bar_time9a='$_POST[bar_time9a]',
bar_time10='$_POST[bar_time10]',bar_time10a='$_POST[bar_time10a]',
bar_time11='$_POST[bar_time11]',bar_time11a='$_POST[bar_time11a]',
bar_time12='$_POST[bar_time12]',bar_time12a='$_POST[bar_time12a]',
bar_time13='$_POST[bar_time13]',bar_time13a='$_POST[bar_time13a]',
bar_time14='$_POST[bar_time14]',bar_time14a='$_POST[bar_time14a]',
pork_serving='$_POST[pork]',alcohol_serving='$_POST[alcohol]',
	cuisine='$cuisine',
	id_price_index='$_POST[price_index]',
	id_suitable_for='$_POST[suitable_for]',
	serving='$serving',
	type_of_serving='$type_of_serving',
	id_serving_time='$_POST[serving_time]',
	id_type_of_service='$_POST[type_of_service]',id_air_conditioning='$_POST[air_conditioning]',id_heating_system='$_POST[heating_system]',facility='$facility',id_wifi='$_POST[wifi]',
	id_term_of_payment='$_POST[payment]',id_premise_security='$_POST[premise_security]',id_premise_fire_safety='$_POST[premise_fire_safety]',id_premise_hygiene='$_POST[premise_hygiene]',
	id_premise_maintenance='$_POST[premise_maintenance]',id_parking_spaces='$_POST[parking_spaces]',id_ambience='$_POST[ambience]',id_attire='$_POST[attire]',id_clean_washroom='$_POST[clean_washroom]',
	id_tables_availability='$_POST[tables_availability]',id_noise_level='$_POST[noise_level]',id_waiter_tipping='$_POST[waiter_tipping]',latitude='$_POST[lat]',longitude='$_POST[lng]',
	tgl_edit='$sekarang' where id_restaurant='$_POST[id]'");

	mysqli_query($koneksi,"INSERT INTO activity (id_member,deskripsi,tgl) VALUES('$u[id_member]','Edit $_POST[restaurant_name]','$sekarang')");
	mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('restaurant','$_POST[id]')");
	mysqli_query($koneksi, "INSERT INTO `edit` (`id_edit`, `id`, `jenis`, `tanggal`, `admin`) VALUES (NULL, '$_POST[id]', 'restaurant', '$date', '$_SESSION[food_member]')");

		$_SESSION['notif']     = "sukses";
		//header("Location: ".$base_url."/".$u['username']."/my-restaurant/edit/".$_POST['id']);
		$id = id_masking($_POST['id']);
		$slug=seo($_POST['restaurant_name']);


		$global=mysqli_query($koneksi,"SELECT id_restaurant FROM restaurant");
		while ($b=mysqli_fetch_array($global)) {
			$rank=mysqli_fetch_array(mysqli_query($koneksi, "SELECT
				((SUM(cleanlines) + SUM(customer_services) + SUM(food_beverage) + SUM(comfort) + SUM(value_money)) / COUNT(id_member)) as rank,
				(SELECT COUNT(h.id_member) FROM restaurant_like h WHERE $b[id_restaurant]=h.id_restaurant) AS jumlah_like,
				(SELECT COUNT(id_restaurant_rating) AS jumlah from restaurant_rating WHERE $b[id_restaurant] = restaurant_rating.id_restaurant) AS jumlah_vote
				FROM
			restaurant_rating f WHERE $b[id_restaurant] = f.id_restaurant"));
				mysqli_query($koneksi,"UPDATE restaurant set jumlah='$rank[rank]', rank_vote='$rank[jumlah_vote]', rank_like='$rank[jumlah_like]'  where id_restaurant='$b[id_restaurant]'");
		}



		header("Location: ".$base_url."/pages/restaurant/info/".$id."/".$slug);
		exit();

}
else{
	header("Location: ".$base_url."/login-area");
}
?>
