<?php
date_default_timezone_set('Asia/Jakarta');
$storeSchedule = [
    'Sun' => [$r['resto_time1'] => $r['resto_time2'], $r['resto_time1a'] => $r['resto_time2a']],
    'Mon' => [$r['resto_time3'] => $r['resto_time4'], $r['resto_time3a'] => $r['resto_time4a']],
    'Tue' => [$r['resto_time5'] => $r['resto_time6'], $r['resto_time5a'] => $r['resto_time6a']],
    'Wed' => [$r['resto_time7'] => $r['resto_time8'], $r['resto_time7a'] => $r['resto_time8a']],
    'Thu' => [$r['resto_time9'] => $r['resto_time10'], $r['resto_time9a'] => $r['resto_time10a']],
    'Fri' => [$r['resto_time11'] => $r['resto_time12'], $r['resto_time11a'] => $r['resto_time12a']],
    'Sat' => [$r['resto_time13'] => $r['resto_time14'], $r['resto_time13a'] => $r['resto_time14a']]
]; 

  // current OR user supplied UNIX timestamp
$timestamp = time();

// default status
$status = 'closed';

// get current time object
$currentTime = (new DateTime())->setTimestamp($timestamp);

// loop through time ranges for current day
foreach ($storeSchedule[date('D', $timestamp)] as $startTime => $endTime) {

    // create time objects from start/end times
    $startTime = DateTime::createFromFormat('h:i A', $startTime);
    $endTime   = DateTime::createFromFormat('h:i A', $endTime);

    // check if current time is within a range
    if (($startTime < $currentTime) && ($currentTime < $endTime)) {
        $status = 'open';
        break;
    }
}
?>


<div class="pull-left">
  <?php echo"<a href='$base_url/assets/img/food/big_$gambar' data-lightbox='$r[nama_food]' data-title='$r[nama_food]'>
    <img data-original='$base_url/assets/img/food/big_$gambar' class='lazy' width='100%'></a>"; ?>

    <?php
      if ($r['id_operation_hour']==4) {
        echo '
            <div style="background: #39b54a">
              <div class="pull-left" style="background: #39b54a">
                <strong>Status :</strong>
                <span class="open">Always Open</span>
              </div>
              <div class="pull-right"></div>
              <div class="clearfix"></div>
            </div>
        ';
      }
      elseif(empty($st) ){
        echo '
        <div style="background: #FFEED0">
          <div class="pull-left" style="background: #FFEED0">
            <strong style="color:#333">Status :</strong>
            <span class="open" style="color:#333">No Information</span>
          </div>
          <div class="pull-right"></div>
          <div class="clearfix"></div>
        </div>
        ';
      }
      elseif ($status == 'open') {
         echo '
            <div style="background: #39b54a">
              <div class="pull-left" style="background: #39b54a">
                <strong>Status :</strong>
                <span class="open">Open</span>
              </div>
              <div class="pull-right"></div>
              <div class="clearfix"></div>
            </div>
        ';
      }
      elseif ($status == 'closed') {
         echo '
            <div style="background: #ee1c25">
              <div class="pull-left" style="background: #ee1c25">
                <strong>Status :</strong>
                <span class="open">Close</span>
              </div>
              <div class="pull-right"></div>
              <div class="clearfix"></div>
            </div>
        ';
      }

     ?>

</div>
