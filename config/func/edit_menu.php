<?php
session_start();
include "base_url.php";
if(!empty($_SESSION['food_member'])){
	include "../database/db.php";
	include "upload_gambar_bms.php";
	include "member_data.php";

	date_default_timezone_set("Asia/Jakarta");
	$sekarang=date("Y-m-d H:i:s");
	
	$ukuran_maks	= 552428800;
	if(!empty($_FILES['gambar_menu']['tmp_name'])){
		$nama_cover			= $_FILES['gambar_menu']['name'];
		$ukuran_cover 		= $_FILES['gambar_menu']['size'];
		$lokasi_cover		= $_FILES['gambar_menu']['tmp_name'];
		list($width_cover, $height_cover) = getimagesize($lokasi_cover);
		if($ukuran_cover > $ukuran_maks or !preg_match("/.(gif|jpg|png)$/i", $nama_cover) or $width_cover < 800 or $height_cover < 600){
			$_SESSION['notif']     = "gambar";
			header("Location: ".$base_url."/".$u['username']."/my-restaurant-menu/edit/".$_POST['id']);
			exit();
		}
		else{
			unlink("../../assets/img/menu/small_".$_POST['gambar']); 
			unlink("../../assets/img/menu/medium_".$_POST['gambar']); 
			unlink("../../assets/img/menu/big_".$_POST['gambar']);
			$cover=upload_gambar($nama_cover,$lokasi_cover,"../../assets/img/menu");
			mysqli_query($koneksi,"update menu set nama_menu='$_POST[menu_name]',gambar_menu='$cover' where id_menu='$_POST[id]'");
			$_SESSION['notif']     = "sukses";
			header("Location: ".$base_url."/".$u['username']."/my-restaurant-menu/edit/".$_POST['id']);
			exit();
		}
	}
	else{
		mysqli_query($koneksi,"update menu set nama_menu='$_POST[menu_name]' where id_menu='$_POST[id]'");
		$_SESSION['notif']     = "sukses";
		header("Location: ".$base_url."/".$u['username']."/my-restaurant-menu/edit/".$_POST['id']);
		exit();
	}
	
}
else{
	header("Location: ".$base_url."/login-area");
}
?>