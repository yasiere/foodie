<div class="panel-groupmb10 mb10" id="accordionsssa" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default head-pan">
    <div class="panel-heading jud-pan" role="tab" id="food1" data-toggle="collapse" data-parent="#accordionsssa" href="#collapsesssa1" aria-expanded="true" aria-controls="collapsesssa1">
      <h4 class="panel-title">
        ❯ Food
      </h4>
    </div>
    <?php error_reporting(0);
      if(!empty($_GET['batas'])){
        $batas=$_GET['batas'];
      }
      else {
        $batas="25";
      }
    ?>
    <div id="collapsesssa1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="food1">
      <div class="panel-body">
        <form method="get" action="<?php echo"$base_url/pages/food/search/"; ?>" class="border-form" id="ale-form" onsubmit="myFunction()">
          <input type="hidden" name="sort" value="<?php echo"$_GET[sort]";?>">
          <input type="hidden" name="batas" value="<?php echo"$_GET[batas]";?>">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading1">
              <h4 class="panel-title">
                <a href="<?php echo"$base_url" ?>/pages/food/search/?sort=total+DESC&batas=<?php echo"$batas" ?>">Top Ranking</a>
              </h4>
            </div>
            <div class="panel-heading" role="tab" id="heading1">
              <h4 class="panel-title">
                <a href="<?php echo"$base_url" ?>/pages/food/search/?sort=r.dilihat+DESC&batas=<?php echo"$batas" ?>">Popular</a>
              </h4>
            </div>
            <div class="panel-heading" role="tab" id="heading1">
              <h4 class="panel-title">
                <a href="<?php echo"$base_url" ?>/pages/food/search/?sort=direkomendasi+DESC&batas=<?php echo"$batas" ?>">Recomended</a>
              </h4>
            </div>
            <div class="panel-heading" role="tab" id="headingfood1">
              <h4 class="panel-title">
                <a data-toggle="collapse"  data-parent="#accordionfood1" href="#collapsefood1" aria-expanded="true" aria-controls="collapsefood1">Location Filter</a>
              </h4>
            </div>
            <div id="collapsefood1" class='panel-collapse collapse' role="tabpanel" aria-labelledby="headingfood1">
              <div class="panel-body">

                <div class="form-group">
                  <label>Country</label>
                  <select class="form-control" name="country" id="negara2">
                    <option value="" selected>Select Country</option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from negara order by nama_negara asc");
                    while($b=mysqli_fetch_array($sql)){
                      if($b['id_negara']==$_GET['country']){
                        echo"<option value='$b[id_negara]' selected>$b[nama_negara]</option>";
                      }
                      else{
                        echo"<option value='$b[id_negara]'>$b[nama_negara]</option>";
                      }
                    }
                    ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>States/Province</label>
                  <select class="form-control" name="state" id="propinsi2">
                    <option value="" selected>Select States/ Province</option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from propinsi where id_negara='$_GET[country]' order by nama_propinsi asc");
                    while($b=mysqli_fetch_array($sql)){
                      if($b['id_propinsi']==$_GET['state']){
                        echo"<option value='$b[id_propinsi]' selected>$b[nama_propinsi]</option>";
                      }
                      else{
                        echo"<option value='$b[id_propinsi]'>$b[nama_propinsi]</option>";
                      }
                    }?>
                  </select>
                </div>
                <div class="form-group">
                  <label>City</label>
                  <select class="form-control" name="city" id="kota2">
                    <option value="" selected>Select City</option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from kota where id_propinsi='$_GET[state]' order by nama_kota asc");
                    while($b=mysqli_fetch_array($sql)){
                      if($b['id_kota']==$_GET['city']){
                        echo"<option value='$b[id_kota]' selected>$b[nama_kota]</option>";
                      }
                      else{
                        echo"<option value='$b[id_kota]'>$b[nama_kota]</option>";
                      }
                    }?>
                  </select>
                </div>
              </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingfood2cat">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordionfood" href="#collapsefood2cat" aria-expanded="true" aria-controls="collapsefood2cat">Category</a>
              </h4>
            </div>
            <div id="collapsefood2cat" class='panel-collapse collapse' role="tabpanel" aria-labelledby="headingfood2cat">
              <div class="panel-body">
                <div class="form-group">
                  <select class="form-control" name="category">
                    <option value="" selected>Select Category</option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from food_category order by nama_food_category asc");
                    while($b=mysqli_fetch_array($sql)){

                      if($b['id_food_category']==$_GET['category']){
                        echo"<option value='$b[id_food_category]' selected>$b[nama_food_category]</option>";
                      }
                      else{
                        echo"<option value='$b[id_food_category]'>$b[nama_food_category]</option>";
                      }
                    }
                    ?>
                  </select>
                </div>
              </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingfood2cat">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordionfood" href="#collapsefood2pri" aria-expanded="true" aria-controls="collapsefood2pri">Price Index</a>
              </h4>
            </div>
            <div id="collapsefood2pri" class='panel-collapse collapse' role="tabpanel" aria-labelledby="headingfood2pri">
              <div class="panel-body">
                <div class="form-group">
                  <select class="form-control" name="price_index">
                    <option value="" selected>Select Price Index</option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from price_index order by nama_price_index asc");
                    while($d=mysqli_fetch_array($sql)){
                      if($d['id_price_index']==$_GET['price_index']){
                        echo"<option value='$d[id_price_index]' selected>$d[nama_price_index]</option>";
                      }
                      else{
                        echo"<option value='$d[id_price_index]'>$d[nama_price_index]</option>";
                      }
                    }
                    ?>
                  </select>
                </div>
              </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingfood2cui">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordionfood" href="#collapsefood2cui" aria-expanded="true" aria-controls="collapsefood2cui">Cuisine</a>
              </h4>
            </div>
            <div id="collapsefood2cui" class='panel-collapse collapse' role="tabpanel" aria-labelledby="headingfood2cui">
              <div class="panel-body">
                <?php
                  $cuisiness=tulis_cekboxxer($cooss,$koneksi,'cuisine','cui');
                  echo"$cuisiness";
                ?>
              </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingfood2msg">
              <h4 class="panel-title">
                <a data-toggle="collapse"  data-parent="#accordionfood" href="#collapsefood2msg" aria-expanded="true" aria-controls="collapsefood2msg">MSG Level</a>
              </h4>
            </div>
            <div id="collapsefood2msg" class='panel-collapse collapse' role="tabpanel" aria-labelledby="headingfood2msg">
              <div class="panel-body">
                <div class="form-group">
                  <select class="form-control" name="msg_level">
                    <option value="" selected>Select MSG Level</option>
                    <?php
                    $sql=mysqli_query($koneksi,"select * from msg_level order by nama_msg_level asc");
                    while($e=mysqli_fetch_array($sql)){
                      if($e['id_msg_level']==$_GET['msg_level']){
                        echo"<option value='$e[id_msg_level]' selected>$e[nama_msg_level]</option>";
                      }
                      else{
                        echo"<option value='$e[id_msg_level]'>$e[nama_msg_level]</option>";
                      }
                    }
                    ?>
                  </select>
                </div>
              </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingfood2">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordionfood" href="#collapsefood2" aria-expanded="true" aria-controls="collapsefood2">Cooking Methode</a>
              </h4>
            </div>
            <div id="collapsefood2" class='panel-collapse collapse' role="tabpanel" aria-labelledby="headingfood2">
              <div class="panel-body">
                <?php

                  $cooking_methode=tulis_cekboxxer($coo,$koneksi,'cooking_methode','cooking_me');
                  echo"$cooking_methode";
                ?>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingfood3">
              <h4 class="panel-title">
                <a data-toggle="collapse"  data-parent="#accordionfood" href="#collapsefood3" aria-expanded="true" aria-controls="collapsefood3">Food Rating</a>
              </h4>
            </div>
            <div id="collapsefood3" class='panel-collapse collapse' role="tabpanel" aria-labelledby="headingfood3">
                <div class="panel-body">
                  <div class="form-group">
                    <label>Overall</label>
                    <select class="form-control" name="overall">
                      <option value="">Select Overate Rate</option>
                      <option value="1" <?php if($_GET['overall']=='1'){ echo "selected";}?>>1 Star</option>
                      <option value="2" <?php if($_GET['overall']=='2'){ echo "selected";}?>>2 Star</option>
                      <option value="3" <?php if($_GET['overall']=='3'){ echo "selected";}?>>3 Star</option>
                      <option value="4" <?php if($_GET['overall']=='4'){ echo "selected";}?>>4 Star</option>
                      <option value="5" <?php if($_GET['overall']=='5'){ echo "selected";}?>>5 Star</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Cleanliness</label>
                    <select class="form-control select" name="clean">
                      <option value="">Select Rate of Cleanliness</option>
                      <option value="0.21" <?php if($_GET['clean']=='0.21'){ echo "selected";} ?>>1 Star</option>
                      <option value="0.42" <?php if($_GET['clean']=='0.42'){ echo "selected";} ?>>2 Star</option>
                      <option value="0.63" <?php if($_GET['clean']=='0.63'){ echo "selected";} ?>>3 Star</option>
                      <option value="0.84" <?php if($_GET['clean']=='0.84'){ echo "selected";} ?>>4 Star</option>
                      <option value="1.05" <?php if($_GET['clean']=='1.05'){ echo "selected";} ?>>5 Star</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Flavor</label>
                    <select class="form-control select" name="flavor">
                      <option value="">Select Rate of Flavor</option>
                      <option value="0.20" <?php if($_GET['flavor']=='0.20'){ echo "selected";} ?>>1 Star</option>
                      <option value="0.40" <?php if($_GET['flavor']=='0.40'){ echo "selected";} ?>>2 Star</option>
                      <option value="0.60" <?php if($_GET['flavor']=='0.60'){ echo "selected";} ?>>3 Star</option>
                      <option value="0.80" <?php if($_GET['flavor']=='0.80'){ echo "selected";} ?>>4 Star</option>
                      <option value="1.00" <?php if($_GET['flavor']=='1.00'){ echo "selected";} ?>>5 Star</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Freshness</label>
                    <select class="form-control select" name="freshness">
                      <option value="">Select Rate of Freshness</option>
                      <option value="0.18" <?php if($_GET['freshness']=='0.18'){ echo "selected";} ?>>1 Star</option>
                      <option value="0.36" <?php if($_GET['freshness']=='0.36'){ echo "selected";} ?>>2 Star</option>
                      <option value="0.54" <?php if($_GET['freshness']=='0.54'){ echo "selected";} ?>>3 Star</option>
                      <option value="0.72" <?php if($_GET['freshness']=='0.72'){ echo "selected";} ?>>4 Star</option>
                      <option value="0.90" <?php if($_GET['freshness']=='0.90'){ echo "selected";} ?>>5 Star</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Cooking</label>
                    <select class="form-control select" name="cooking">
                      <option value="" >Select Rate of Cooking</option>
                      <option value="0.17" <?php if($_GET['cooking']=='0.17'){ echo "selected";} ?>>1 Star</option>
                      <option value="0.34" <?php if($_GET['cooking']=='0.34'){ echo "selected";} ?>>2 Star</option>
                      <option value="0.51" <?php if($_GET['cooking']=='0.51'){ echo "selected";} ?>>3 Star</option>
                      <option value="0.68" <?php if($_GET['cooking']=='0.68'){ echo "selected";} ?>>4 Star</option>
                      <option value="0.85" <?php if($_GET['cooking']=='0.85'){ echo "selected";} ?>>5 Star</option>
                    </select>
                  </div>
                  <div class="form-group">

                    <label>Presentasion &amp; Aroma</label>
                    <select class="form-control select" name="aroma">
                      <option value="" >Select Rate of Presentasion &amp; Aroma</option>
                      <option value="0.15" <?php if($_GET['aroma']=='0.15'){ echo "selected";} ?>>1 Star</option>
                      <option value="0.30" <?php if($_GET['aroma']=='0.30'){ echo "selected";} ?>>2 Star</option>
                      <option value="0.45" <?php if($_GET['aroma']=='0.45'){ echo "selected";} ?>>3 Star</option>
                      <option value="0.60" <?php if($_GET['aroma']=='0.60'){ echo "selected";} ?>>4 Star</option>
                      <option value="0.75" <?php if($_GET['aroma']=='0.75'){ echo "selected";} ?>>5 Star</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Serving</label>
                    <select class="form-control select" name="serving">
                      <option value="" >Select Rate of Serving</option>
                      <option value="0.09" <?php if($_GET['serving']=='0.09'){ echo "selected";} ?>>1 Star</option>
                      <option value="0.18" <?php if($_GET['serving']=='0.18'){ echo "selected";} ?>>2 Star</option>
                      <option value="0.27" <?php if($_GET['serving']=='0.27'){ echo "selected";} ?>>3 Star</option>
                      <option value="0.36" <?php if($_GET['serving']=='0.36'){ echo "selected";} ?>>4 Star</option>
                      <option value="0.45" <?php if($_GET['serving']=='0.45'){ echo "selected";} ?>>5 Star</option>
                    </select>
                  </div>
                </div>

            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingfood2tag">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordionfood" href="#collapsefood2tag" aria-expanded="true" aria-controls="collapsefood2tag">Tag</a>
              </h4>
            </div>
            <div id="collapsefood2tag" class='panel-collapse collapse' role="tabpanel" aria-labelledby="headingfood2tag">
              <div class="panel-body">
                <div class="form-group">
                  <div class="input-group">
                      <input class="form-control tag" name="tag" value="<?php echo"$_GET[tag]";?>">
                      <div class="input-group-addon">x</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <button type="submit" class="btn btn-success btn-block">Search</button>
          <a href="https://www.foodieguidances.com/pages/food/search/" class="btn btn-warning btn-block">Reset</a>
        </form>
      </div>
    </div>
  </div>
</div>
