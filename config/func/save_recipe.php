<?php
session_start(); error_reporting(0);
include "base_url.php";
if(!empty($_SESSION['food_member'])){
	include "../database/db.php";
	include "upload_gambar_bms.php";
	include "member_data.php";

	date_default_timezone_set("Asia/Jakarta");
	$sekarang=date("Y-m-d H:i:s");

	$ukuran_maks	= 552428800;

	$nama_cover			= $_FILES['gambar']['name'];
	$ukuran_cover 		= $_FILES['gambar']['size'];
	$lokasi_cover		= $_FILES['gambar']['tmp_name'];
	list($width_cover, $height_cover) = getimagesize($lokasi_cover);
	if($ukuran_cover > $ukuran_maks or !preg_match("/.(gif|jpg|png)$/i", $nama_cover) or $width_cover < 800 or $height_cover < 600){
		$_SESSION['notif']     = "gambar";
		header("Location: ".$base_url."/".$username."/my-recipe/new");
		exit();
	}
	else{
		$sql=mysqli_query($koneksi,"INSERT INTO recipe (id_member,nama_recipe,pork_serving,id_cuisine,id_recipe_category,serving_pax,dish,id_msg_level,id_difficulty,id_cooking_methode,id_duration,ingredient,seasoning,way_of_cooking,smart_tips,reference,tgl_post) VALUES('$u[id_member]','$_POST[recipe_name]','$_POST[pork]','$_POST[cuisine]','$_POST[category]','$_POST[serving_pax]','$_POST[dish]','$_POST[msg_level]','$_POST[difficulty]','$_POST[cooking_methode]','$_POST[duration]','$_POST[ingredient]','$_POST[seasoning]','$_POST[way_cooking]','$_POST[smart_tips]','$_POST[reference]','$sekarang')");
		if($sql){
			$cover=upload_gambar($nama_cover,$lokasi_cover,"../../assets/img/recipe");
			$id_recipe=mysqli_insert_id($koneksi);
			mysqli_query($koneksi,"INSERT INTO recipe_photo (id_member,id_recipe,nama_recipe_photo,gambar_recipe_photo) VALUES('$id_member','$id_recipe','$_POST[recipe_name]','$cover')");
			mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('Recipe','$id_recipe')");
			// if($u['feed_submit']==1){
				mysqli_query($koneksi,"INSERT INTO activity (id_member,kat_act,deskripsi,tgl) VALUES('$id_member','recipe','Submit new recipe review','$sekarang')");
			// }
			header("Location: ".$base_url."/".$username."/submit/recipe");
			exit();
		}
	}
}
else{
	header("Location: ".$base_url."/login-area");
}
?>
