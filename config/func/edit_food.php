<?php
session_start();
include "base_url.php";
if(!empty($_SESSION['food_member'])){
	include "../database/db.php";
	include "upload_gambar_bms.php";
	include "member_data.php";
	include "id_masking.php";
	include "seo.php";
	// include "$base_url/config/func/rank_food.php";
	date_default_timezone_set("Asia/Jakarta");
	$sekarang=date("Y-m-d H:i:s");

	$cook_seo = $_POST['cooking_methode'];
	if(!empty($cook_seo)){
		$cooking=implode('+',$cook_seo);
	} else {
		# code...
	}

	$cuisine_seo = $_POST['cuisine'];
	if (!empty($cuisine_seo)) {
		$cuisine=implode('+',$cuisine_seo);
	}

	mysqli_query($koneksi,"update food set nama_food='$_POST[food_name]',id_food_category='$_POST[category]',id_price_index='$_POST[price_index]',id_cuisine='$cuisine',
	cooking_methode='$cooking',id_msg_level='$_POST[msg_level]', id_tag='$_POST[tag]', type_serving='$_POST[pork]' where id_food='$_POST[id]'");
	$_SESSION['notif']     = "sukses";
	$id = id_masking($_POST['id']);
	$slug=seo($_POST['food_name']);
	//header("Location: ".$base_url."/".$u['username']."/my-food/edit/".$_POST['id']);
	mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('food','$_POST[id]')");
	$date=date("Y-m-d");
	mysqli_query($koneksi, "INSERT INTO `edit` (`id`, `jenis`, `tanggal`, `admin`) VALUES ('$_POST[id]', 'food', '$date', '$_SESSION[food_member]')");
	include "rank_food.php";
	header("Location: ".$base_url."/pages/food/info/".$id."/".$slug);
	exit();
}
else{
	header("Location: ".$base_url."/login-area");
}
?>
