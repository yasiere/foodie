<?php
class Paging{
	// Fungsi untuk mencek halaman dan posisi data
	function cariPosisi($batas){
		if(empty($_GET['page'])){
			$posisi=0;
			$_GET['page']=1;
		}
		else{
			$posisi = ($_GET['page']-1) * $batas;
		}
		return $posisi;
	}
	// Fungsi untuk menghitung total halaman
	function jumlahHalaman($jmldata, $batas){
		$jmlhalaman = ceil($jmldata/$batas);
		return $jmlhalaman;
	}
	// Fungsi untuk link halaman 1,2,3
	function navHalaman($link,$halaman_aktif, $jmlhalaman){
		$link_halaman = "";
		// Link ke halaman pertama (first) dan sebelumnya (prev)
		if($halaman_aktif > 1){
			$prev = $halaman_aktif-1;
			$link_halaman .= "<li><a href='$link&page=1'>&laquo;</a></li><li><a href='$link&page=$prev'>&lsaquo;</a></li>";
		}
		else{
			$link_halaman .= "<li class='disabled'><a>&laquo;</a></li><li class='disabled'><a>&lsaquo;</a></li>";
		}
		// Link halaman 1,2,3, �
		$angka = ($halaman_aktif > 3 ? "<li class='disabled'><a>...</a></li>" : " ");
		for ($i=$halaman_aktif-2; $i<$halaman_aktif; $i++){
			if ($i < 1)
			continue;
			$angka .= "<li><a href='$link&page=$i'>$i</a></li>";
		}
		$angka .= " <li class='active'><a>$halaman_aktif</a></li>";
		for($i=$halaman_aktif+1; $i<($halaman_aktif+3); $i++){
			if($i > $jmlhalaman)
			break;
			$angka .= "<li><a href='$link&page=$i'>$i</a></li>";
		}
		$angka .= ($halaman_aktif+2<$jmlhalaman ? "<li class='disabled'><a>...</a></li><li><a href='$link&page=$jmlhalaman'>$jmlhalaman</a></li>" : " ");
		$link_halaman .= "$angka";
		// Link ke halaman berikutnya (Next) dan terakhir (Last)
		if($halaman_aktif < $jmlhalaman){
			$next = $halaman_aktif+1;
			$link_halaman .= "<li><a href='$link&page=$next'>&rsaquo;</a></li><li><a href='$link&page=$jmlhalaman'>&raquo;</a></li>";
		}
		else{
			$link_halaman .= "<li class='disabled'><a href='#'>&rsaquo;</a></li><li class='disabled'><a href='#'>&raquo;</a></li>";
		}
		return $link_halaman;
	}
}
?>