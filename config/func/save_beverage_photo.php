<?php
session_start();
include "base_url.php";
if(!empty($_SESSION['food_member'])){
	include "../database/db.php";
	include "upload_gambar_bms.php";
	include "member_data.php";
	$sql=mysqli_query($koneksi,"select id_beverage from beverage where nama_beverage='$_POST[beverage]'");
	$ada=mysqli_num_rows($sql);
	if($ada==0){
		header("Location: ".$base_url."/".$username."/submit/error-beverage");
		exit();
	}
	date_default_timezone_set("Asia/Jakarta");
	$sekarang=date("Y-m-d H:i:s");

	$ukuran_maks	= 552428800;

	$nama_cover			= $_FILES['gambar']['name'];
	$ukuran_cover 		= $_FILES['gambar']['size'];
	$lokasi_cover		= $_FILES['gambar']['tmp_name'];
	list($width_cover, $height_cover) = getimagesize($lokasi_cover);
	if($ukuran_cover > $ukuran_maks or !preg_match("/.(gif|jpg|png)$/i", $nama_cover) or $width_cover < 800 or $height_cover < 600){
		$_SESSION['notif']     = "gambar";
		header("Location: ".$base_url."/".$username."/my-beverage-photo/new");
		exit();
	}
	else{
		$r=mysqli_fetch_array($sql);
		$cover=upload_gambar($nama_cover,$lokasi_cover,"../../assets/img/beverage");
		mysqli_query($koneksi,"INSERT INTO beverage_photo (id_member,id_beverage,nama_beverage_photo,gambar_beverage_photo) VALUES('$id_member','$r[id_beverage]','$_POST[nama]','$cover')");
		mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('beverage','$r[id_beverage]')");
		// if($u['feed_submit']==1){
			mysqli_query($koneksi,"INSERT INTO activity (id_member,kat_act,deskripsi,tgl) VALUES('$id_member','feed_submit','Submit new beverage photo','$sekarang')");
		// }
		header("Location: ".$base_url."/".$username."/submit/beverage-photo");
	}
}
else{
	header("Location: ".$base_url."/login-area");
}
?>
