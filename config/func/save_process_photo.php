<?php
session_start();
include "base_url.php";
if(!empty($_SESSION['food_member'])){
	include "../database/db.php";
	include "upload_gambar_bms.php";
    include "member_data.php";
    include "id_masking.php";
    include "seo.php";
	// $sql=mysqli_query($koneksi,"select * from process where id_process='$_POST[process]'");
	// $ada=mysqli_num_rows($sql);
	// if($ada==0){
	// 	echo "select * from process where id_process='$_POST[process]'";
	// 	//header("Location: ".$base_url."/".$username."/submit/error-process");
	// 	exit();
	// }

	date_default_timezone_set("Asia/Jakarta");
	$sekarang=date("Y-m-d H:i:s");

	$ukuran_maks	= 552428800;

	$nama_cover			= $_FILES['gambar']['name'];
	$ukuran_cover 		= $_FILES['gambar']['size'];
	$lokasi_cover		= $_FILES['gambar']['tmp_name'];

	list($width_cover, $height_cover) = getimagesize($lokasi_cover);
	if($ukuran_cover > $ukuran_maks or !preg_match("/.(gif|jpg|png)$/i", $nama_cover) or $width_cover < 800 or $height_cover < 600){
		$_SESSION['notif']     = "gambar";
		header("Location: ".$base_url."/".$username."/my-process-photo/new");
		exit();
	}
	else{
		
		
		if(isset($_POST['gambar'])){
			unlink("../../assets/img/process/small_".$_POST['gambar']); 
			unlink("../../assets/img/process/medium_".$_POST['gambar']); 
			unlink("../../assets/img/process/big_".$_POST['gambar']); 
			$cover=upload_gambar($nama_cover,$lokasi_cover,"../../assets/img/process");

			mysqli_query($koneksi,"UPDATE process_photo set process_caption='$_POST[nama]', gambar_process_photo='$cover' where id_process_photo = '$_POST[process]' ");

			mysqli_query($koneksi,"INSERT INTO activity (id_member,kat_act,deskripsi,tgl) VALUES('$id_member','feed_submit','Submit new process photo','$sekarang')");
			
			header("Location: ".$base_url."/".$u['username']."/my-process-photo");
		}
		else{
			$sql=mysqli_query($koneksi,"select * from process where id_process='$_POST[process]'");
			$r=mysqli_fetch_array($sql);
			$cover=upload_gambar($nama_cover,$lokasi_cover,"../../assets/img/process");
			mysqli_query($koneksi,"INSERT INTO process_photo (id_member,id_process,process_caption,gambar_process_photo) VALUES('$id_member','$r[id_process]','$_POST[nama]','$cover')");
			mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('process','$r[id_process]')");
			// if($u['feed_submit']==1){
				mysqli_query($koneksi,"INSERT INTO activity (id_member,kat_act,deskripsi,tgl) VALUES('$id_member','feed_submit','Submit new process photo','$sekarang')");
			// }
			header("Location: ".$base_url."/pages/process/info/".id_masking($r['id_process'])."/".seo($r['nama_process'])."");
		}
	}


}
else{
	header("Location: ".$base_url."/login-area");
}
?>
