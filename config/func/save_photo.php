<?php
session_start();
include "base_url.php";
if(!empty($_SESSION['food_member'])){
	include "../database/db.php";
	include "upload_gambar_bms.php";
	include "member_data.php";
	$sql=mysqli_query($koneksi,"select id_restaurant from restaurant where restaurant_name='$_POST[restaurant]'");
	$ada=mysqli_num_rows($sql);
	if($ada==0){
		header("Location: ".$base_url."/".$username."/submit/error-resto");
		exit();
	}
	date_default_timezone_set("Asia/Jakarta");
	$sekarang=date("Y-m-d H:i:s");

	$ukuran_maks	= 552428800;

	// $nama_cover			= $_FILES['gambar_photo']['name'];
	// $ukuran_cover 		= $_FILES['gambar_photo']['size'];
	// $lokasi_cover		= $_FILES['gambar_photo']['tmp_name'];
	// list($width_cover, $height_cover) = getimagesize($lokasi_cover);
	foreach($_FILES['gambar']['name'] as $key1 => $gambar){
		if ($_FILES['gambar']['size'][$key1] <> 0){
			$nama		= $_FILES['gambar']['name'][$key1];
			$ukuran 	= $_FILES['gambar']['size'][$key1];
			$lokasi 	= $_FILES['gambar']['tmp_name'][$key1];
			list($width_gambar, $height_gambar) = getimagesize($lokasi);
			if($ukuran > $ukuran_maks or !preg_match("/.(gif|jpg|png)$/i", $nama) or $width_gambar < 800 or $height_gambar < 600){
				$error_gambar=1;
			}
		}
	}
	// if($ukuran_cover > $ukuran_maks or !preg_match("/.(gif|jpg|png)$/i", $nama_cover) or $width_cover < 800 or $height_cover < 600){
	// 	$_SESSION['notif']     = "gambar";
	// 	header("Location: ".$base_url."/".$username."/my-restaurant-photo/new");
	// 	exit();
	// }
	if($error_gambar == 1){
		$_SESSION['notif']     = "gambar";
		header("Location: ".$base_url."/".$u['username']."/my-article/new");
		exit();
	}
	else{
		$r=mysqli_fetch_array($sql);
		// $cover=upload_gambar($nama_cover,$lokasi_cover,"../../assets/img/restaurant");
		//$sql=mysqli_query($koneksi,"INSERT INTO restaurant_photo (id_member,id_restaurant,nama_restaurant_photo) VALUES('$u[id_member]','$r[id_restaurant]','$_POST[photo_name]')");

			$id=mysqli_insert_id($koneksi);
			foreach($_FILES['gambar']['name'] as $key1 => $gambar){
				if ($_FILES['gambar']['size'][$key1] <> 0){
					$nama_gambar = $_FILES['gambar']['name'][$key1];
					$lokasi_gambar = $_FILES['gambar']['tmp_name'][$key1];
					$foto_gambar=upload_gambar($nama_gambar,$lokasi_gambar,"../../assets/img/restaurant");
					mysqli_query($koneksi,"INSERT INTO restaurant_photo(id_member,id_restaurant,nama_restaurant_photo,gambar_restaurant_photo) VALUES('$u[id_member]','$r[id_restaurant]','$_POST[photo_name]','$foto_gambar')");
				}
			}
			mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('Restaurant','$id')");
			// if($u['feed_submit']==1){
			mysqli_query($koneksi,"INSERT INTO activity (id_member,kat_act,deskripsi,tgl) VALUES('$u[id_member]','restaurant-photo','Submit new restaurant photo','$sekarang')");
			// }
			header("Location: ".$base_url."/".$username."/submit/restaurant-photo");




	}
}
else{
	header("Location: ".$base_url."/login-area");
}
?>
