<?php
session_start();
include "base_url.php";
if(!empty($_SESSION['food_member'])){
	include "../database/db.php";
	include "upload_gambar_bms.php";
    include "member_data.php";
    include "id_masking.php";
    include "seo.php";
	// $sql=mysqli_query($koneksi,"select * from events where id_events='$_POST[events]'");
	// $ada=mysqli_num_rows($sql);
	// if($ada==0){
	// 	echo "select * from events where id_events='$_POST[events]'";
	// 	//header("Location: ".$base_url."/".$username."/submit/error-events");
	// 	exit();
	// }

	date_default_timezone_set("Asia/Jakarta");
	$sekarang=date("Y-m-d H:i:s");
	$ukuran_maks	= 552428800;
	$nama_cover			= $_FILES['gambar']['name'];
	$ukuran_cover 		= $_FILES['gambar']['size'];
	$lokasi_cover		= $_FILES['gambar']['tmp_name'];

	list($width_cover, $height_cover) = getimagesize($lokasi_cover);
	if($ukuran_cover > $ukuran_maks or !preg_match("/.(gif|jpg|png)$/i", $nama_cover) or $width_cover < 800 or $height_cover < 600){
        $_SESSION['notif']     = "gambar";
        if(!empty($_POST['events'])){
            header("Location: ".$base_url."/".$username."/my-events-photo/new/".$_POST['events']);
        }
		header("Location: ".$base_url."/".$username."/my-events-photo/new");
		exit();
	}
	else{
		if(isset($_POST['gambar'])){
			unlink("../../assets/img/events/small_".$_POST['gambar']); 
			unlink("../../assets/img/events/medium_".$_POST['gambar']); 
			unlink("../../assets/img/events/big_".$_POST['gambar']); 
			$cover=upload_gambar($nama_cover,$lokasi_cover,"../../assets/img/events");

			mysqli_query($koneksi,"UPDATE events_photo set events_caption='$_POST[nama]', gambar_events_photo='$cover' where id_events_photo = '$_POST[events]' ");

			mysqli_query($koneksi,"INSERT INTO activity (id_member,kat_act,deskripsi,tgl) VALUES('$id_member','feed_submit','Submit new events photo','$sekarang')");
			
			header("Location: ".$base_url."/".$u['username']."/my-events-photo");
		}
		else{
			$sql=mysqli_query($koneksi,"select * from events where id_events='$_POST[events]'");
			$r=mysqli_fetch_array($sql);
			$cover=upload_gambar($nama_cover,$lokasi_cover,"../../assets/img/events");
			mysqli_query($koneksi,"INSERT INTO events_photo (id_member,id_events,events_caption,gambar_events_photo) VALUES('$id_member','$r[id_events]','$_POST[nama]','$cover')");
			mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('events','$r[id_events]')");
			// if($u['feed_submit']==1){
				mysqli_query($koneksi,"INSERT INTO activity (id_member,kat_act,deskripsi,tgl) VALUES('$id_member','feed_submit','Submit new events photo','$sekarang')");
			// }
			header("Location: ".$base_url."/pages/events/info/".id_masking($r['id_events'])."/".seo($r['nama_events'])."");
		}
	}


}
else{
	header("Location: ".$base_url."/login-area");
}
?>
