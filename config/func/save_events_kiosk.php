<?php
session_start();
include "base_url.php";
if(!empty($_SESSION['food_member'])){
	include "../database/db.php";
	include "upload_gambar_bms.php";
	include "member_data.php";
	include "id_masking.php";

	
		date_default_timezone_set("Asia/Jakarta");

		$sekarang=date("Y-m-d H:i:s");

		$ukuran_maks	= 552428800;

		$nama_cover			= $_FILES['cover_photo']['name'];
		$ukuran_cover 		= $_FILES['cover_photo']['size'];
		$lokasi_cover		= $_FILES['cover_photo']['tmp_name'];

		if(!empty($nama_cover)){

			list($width_cover, $height_cover) = getimagesize($lokasi_cover);
		}

		

			$id_events=mysqli_insert_id($koneksi);
			if ($_POST['act'] == 'add') {

				if($ukuran_cover > $ukuran_maks or !preg_match("/.(gif|jpg|png)$/i", $nama_cover) or $width_cover < 800 or $height_cover < 600){
					$_SESSION['notif']     = "gambar";
					// echo var_dump($_POST);
					// echo "asas";
					header('Location: ' . $_SERVER['HTTP_REFERER']);

					exit();
				}
				else{
					$sql=mysqli_query($koneksi,"INSERT INTO `events_kiosk`(`id_events`, `nama_events_kiosk`, tag, `tgl_post`) VALUES ('$_POST[idevents]', '$_POST[name_kiosk]','$_POST[tag]', '$sekarang')");
					$id_events=mysqli_insert_id($koneksi);
					if($sql == true){
						$cover=upload_gambar($nama_cover,$lokasi_cover,"../../assets/img/events/kiosk");
						if(!empty($_POST['rating'])){
							$r1=$_POST['rating'] * 0.25;
							$r2=$_POST['rating'] * 0.25;
							$r3=$_POST['rating'] * 0.25;
							$r4=$_POST['rating'] * 0.25;
						}
						else{
							$r1=$_POST['cleanliness'] * 0.2;
							$r2=$_POST['cusser'] * 0.2;
							$r3=$_POST['fnb'] * 0.4;
							$r4=$_POST['vfm'] * 0.2;
						}
						mysqli_query($koneksi,"INSERT INTO `events_kiosk_rating`(`id_kiosk`, `id_member`, `cleanliness`, `cusser`, `fnb`, `vfm`, `tgl_rating`) 
							VALUES ('$id_events','$u[id_member]', '$r1', '$r2', '$r3', '$r4', '$sekarang')");
						mysqli_query($koneksi,"UPDATE `events_kiosk` SET `gambar_kiosk`= '$cover' WHERE id_events_kiosk = '$id_events'");
						mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('Events Kiosk','$id_events')");
						mysqli_query($koneksi,"INSERT INTO activity (id_member,kat_act,deskripsi,tgl) VALUES('$u[id_member]','events_submit','Submit Events','$sekarang')");				
						
					}
					
					header('Location: '.$base_url.'/'.$u['username'].'/my-events/kiosk/'.id_masking($_POST['idevents']));	
				}
				
			}
			else{
				$sql=mysqli_query($koneksi,"UPDATE `events_kiosk` SET `nama_events_kiosk`= '$_POST[name_kiosk]', tag='$_POST[tag]' WHERE id_events_kiosk = '$_POST[id]'");

				echo id_masking($_POST['idevents']);
				if(!empty($nama_cover)){
					$cover=upload_gambar($nama_cover,$lokasi_cover,"../../assets/img/events/kiosk");
		
					mysqli_query($koneksi,"UPDATE `events_kiosk` SET `gambar_kiosk`= '$cover' WHERE id_events_kiosk = '$_POST[id]'");
					unlink("../../assets/img/events/kiosk/small_".$_POST['gambar']);
					unlink("../../assets/img/events/kiosk/medium_".$_POST['gambar']); 
					unlink("../../assets/img/events/kiosk/big_".$_POST['gambar']);  
				}
				mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('Events Kiosk','$id_events')");
				mysqli_query($koneksi,"INSERT INTO activity (id_member,kat_act,deskripsi,tgl) VALUES('$u[id_member]','events_submit','Submit Events Kiosk','$sekarang')");				
				
				header('Location: '.$base_url.'/'.$u['username'].'/my-events/kiosk/'.id_masking($_POST['idevents']));	
				// header('Location: '.$base_url.'/'.$u['username'].'/my-events/kiosk/'.id_masking($_POST['id']));	
			}
			
			exit();

		
	
}
else{
	header("Location: ".$base_url."/login-area");
}
?>
