<?php
session_start(); error_reporting(0);
include "base_url.php";
if(!empty($_SESSION['food_member'])){
	include "../database/db.php";
	include "upload_gambar_bms.php";
	include "member_data.php";
	include "id_masking.php";

	
		date_default_timezone_set("Asia/Jakarta");

		$sekarang=date("Y-m-d H:i:s");

		$ukuran_maks	= 552428800;		
		
		$nama_cover			= $_FILES['gambar_process']['name'];
		$ukuran_cover 		= $_FILES['gambar_process']['size'];
		$lokasi_cover		= $_FILES['gambar_process']['tmp_name'];
		list($width_cover, $height_cover) = getimagesize($lokasi_cover);

		if($ukuran_cover > $ukuran_maks or !preg_match("/.(gif|jpg|png)$/i", $nama_cover) or $width_cover < 800 or $height_cover < 600){
			$_SESSION['notif']     = "gambar";
			header("Location: ".$base_url."/".$u['username']."/my-process/new");
			exit();
		}

		else{			

			function strstr_after($haystack, $needle, $case_insensitive = false) {
			    $strpos = ($case_insensitive) ? 'stripos' : 'strpos';
			    $pos = $strpos($haystack, $needle);
			    if (is_int($pos)) {
			        return substr($haystack, $pos + strlen($needle));
			    }
			    // Most likely false or null
			    return $pos;
			}

			$r=mysqli_fetch_array($sql);
			$categories = strstr_after($_POST['categories'], '##');
			//echo $categorie;
			$sql=mysqli_query($koneksi,"INSERT INTO `process`(`id_member`, `nama_process`,type_process, `manufactured_by`, `id_negara`, `reviewed_country`, `month_of_manufactured`, `date_of_viewed`, `date_of_expired`, `expired_periode`, `id_price_index`, `food_chemical_used`, `categories`, `tag`) 
										VALUES ('$u[id_member]', '$_POST[nama_process]','$_POST[process_type]', '$_POST[manufactured_by]', '$_POST[negara]', '$_POST[reviewed_country]', '$_POST[month_of_manufactured]', '$_POST[date_of_viewed]', '$_POST[date_of_expired]', '$_POST[expired_periode]', '$_POST[id_price_index]', '$_POST[food_chemical_used]', '$categories', '$_POST[tag]')");
			if($sql == true){
				$cover=upload_gambar($nama_cover,$lokasi_cover,"../../assets/img/process");
				$id_process=mysqli_insert_id($koneksi);
					
				foreach($_FILES['menu']['name'] as $key1 => $menu){
					if ($_FILES['menu']['size'][$key1] <> 0){
						$nama_menu = $_FILES['menu']['name'][$key1];
						$lokasi_menu = $_FILES['menu']['tmp_name'][$key1];
						$caption_menu = $_POST['menu_caption'][$key1];
						$foto_menu=upload_gambar($nama_menu,$lokasi_menu,"../../assets/img/process");
						mysqli_query($koneksi,"INSERT INTO process_photo(id_member,id_process,gambar_process_photo,process_caption,tgl_post) 
																	VALUES('$u[id_member]','$id_process','$foto_menu','$caption_menu','$sekarang')");
					}
				}
				
				mysqli_query($koneksi,"INSERT INTO `process_photo`(`id_process`, `gambar_process_photo`) VALUES ('$id_process', '$cover')");
				mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('Process','$id_process')");
				mysqli_query($koneksi,"INSERT INTO activity (id_member,kat_act,deskripsi,tgl) VALUES('$u[id_member]','feed_submit','Submit new Process F&B','$sekarang')");
				
				if(!empty($_POST['rating']) or !empty($_POST['exp']) or !empty($_POST['taste']) or !empty($_POST['freshness']) or !empty($_POST['serving'])){
					$katku = strstr_after($_POST['categories'], '##');

					if ($katku == '1') {
						if(!empty($_POST['rating'])){
							$r1=$_POST['rating'] * 0.25;
							$r2=$_POST['rating'] * 0.25;
							$r3=$_POST['rating'] * 0.25;
							$r4=$_POST['rating'] * 0.25;			
						}
						else{
							$r1=$_POST['exp'] * 0.3;
							$r2=$_POST['taste'] * 0.3;
							$r3=$_POST['freshness'] * 0.3;
							$r4=$_POST['serving'] * 0.1;
						}
					}
					else{
						if(!empty($_POST['rating'])){
							$r1=$_POST['rating'] * 0.33333333;
							$r2=$_POST['rating'] * 0.33333333;
							$r4=$_POST['rating'] * 0.33333333;			
						}
						else{
							$r1=$_POST['exp'] * 0.35;
							$r2=$_POST['taste'] * 0.35;
							$r4=$_POST['serving'] * 0.3;
						}
					}
					mysqli_query($koneksi,"INSERT INTO process_rating(`id_process`, `id_member`, `expired`, `taste`, `freshness`, `serving`, tgl_process_rating) 
						values('$id_process','$u[id_member]','$r1','$r2','$r3','$r4','$sekarang')");
				}
				

				$id = id_masking($id_process);
				// $global=mysqli_query($koneksi,"SELECT id_beverage, id_negara, id_propinsi, id_kota, id_mall FROM beverage b left JOIN restaurant r on b.id_restaurant=r.id_restaurant");
				// while ($b=mysqli_fetch_array($global)) {
				// 	$rank=mysqli_fetch_array(mysqli_query($koneksi, "SELECT
				// 		((SUM(f.cleanlines) + SUM(f.flavor) + SUM(f.freshness) + SUM(f.cooking) + SUM(f.aroma) + SUM(f.serving)) / COUNT(f.id_member)) as rank,
				// 		(SELECT COUNT(h.id_member) FROM beverage_like h WHERE h.id_beverage=$b[id_beverage]) AS dilike,
				// 		(select count(id_beverage_rating) as jumlah from beverage_rating where beverage_rating.id_beverage=$b[id_beverage]) as jumlah_vote
				// 		 FROM	beverage_rating f WHERE $b[id_beverage] =f.id_beverage"));
				// 		 mysqli_query($koneksi,"UPDATE beverage set jumlah='$rank[rank]', rank_vote='$rank[jumlah_vote]', rank_like='$rank[dilike]', negara='$b[id_negara]', propinsi='$b[id_propinsi]', kota='$b[id_kota]', mall='$b[id_mall]' where id_beverage='$b[id_beverage]'");
				// }
				header("Location: ".$base_url."/".$u['username']."/my-process");
				exit();
			}
		}
	
}
else{
	header("Location: ".$base_url."/login-area");
}
?>
