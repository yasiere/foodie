<?php
session_start();
include "base_url.php";
if(!empty($_SESSION['food_member'])){
	include "../database/db.php";
	include "upload_gambar_bms.php";
	include "member_data.php";
	include "id_masking.php";

	
		date_default_timezone_set("Asia/Jakarta");

		$sekarang=date("Y-m-d H:i:s");
		$sekarangku=date("Y-m-d");


		$ukuran_maks	= 552428800;

		$nama_cover			= $_FILES['cover_photo']['name'];
		$ukuran_cover 		= $_FILES['cover_photo']['size'];
		$lokasi_cover		= $_FILES['cover_photo']['tmp_name'];

		if(!empty($nama_cover)){
			list($width_cover, $height_cover) = getimagesize($lokasi_cover);
		}

			$id_events=mysqli_insert_id($koneksi);
			if ($_POST['act'] == 'add') {

				if($ukuran_cover > $ukuran_maks or !preg_match("/.(gif|jpg|png|jpeg)$/i", $nama_cover) or $width_cover < 800 or $height_cover < 600){
					$_SESSION['notif']     = "gambar";
					// echo var_dump($_POST);
					// echo "asas";
					header('Location: ' . $_SERVER['HTTP_REFERER']);

					// exit();
				}
				else{
					$sql=mysqli_query($koneksi,"INSERT INTO `events_beverage`(`id_kiosk`, `nama_events_beverage`, tag, tgl_post) VALUES ('$_POST[idevents]', '$_POST[name_beverage]', '$_POST[tag]', '$sekarangku')");
					// echo "INSERT INTO `events_beverage`(`id_kiosk`, `nama_events_beverage`, 'tgl_post') VALUES ('$_POST[idevents]', '$_POST[name_beverage]', '$sekarangku')";
					$id_events=mysqli_insert_id($koneksi);
					if($sql == true){
						$cover=upload_gambar($nama_cover,$lokasi_cover,"../../assets/img/events/beverage");
						if(!empty($_POST['rating'])){
							$r1=$_POST['rating'] * 0.166;
							$r2=$_POST['rating'] * 0.166;
							$r3=$_POST['rating'] * 0.166;
							$r4=$_POST['rating'] * 0.166;
							$r5=$_POST['rating'] * 0.168;
							$r6=$_POST['rating'] * 0.168;
						}
						else{
							$r1=$_POST['cleanliness'] * 0.18;
							$r2=$_POST['flavor'] * 0.18;
							$r3=$_POST['freshness'] * 0.18;
							$r4=$_POST['cooking'] * 0.18;
							$r5=$_POST['pna'] * 0.14;
							$r6=$_POST['serving'] * 0.14;
						}
						mysqli_query($koneksi,"INSERT INTO `events_beverage_rating`(`id_beverage`, `id_member`, `cleanliness`, `flavor`, `freshness`, `cooking`, `pna`, `serving`, `tgl_rating`) 
							VALUES ('$id_events','$u[id_member]', '$r1', '$r2', '$r3', '$r4', '$r5', '$r6', '$sekarang')");
						// echo"INSERT INTO `events_beverage_rating`(`id_beverage`, `id_member`, `cleanliness`, `cusser`, `fnb`, `vfm`, `tgl_rating`) 
						// 	VALUES ('$id_events','$u[id_member]', '$r1', '$r2', '$r3', '$r4', '$sekarang')";
						mysqli_query($koneksi,"UPDATE `events_beverage` SET `gambar_beverage`= '$cover' WHERE id_events_beverage = '$id_events'");
						mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('Events beverage','$id_events')");
						mysqli_query($koneksi,"INSERT INTO activity (id_member,kat_act,deskripsi,tgl) VALUES('$u[id_member]','events_submit','Submit Events','$sekarang')");				
						
					}
					
					header('Location: '.$base_url.'/'.$u['username'].'/my-events/beverage/'.id_masking($_POST['idevents']));	
				}
				
			}
			else{
				$sql=mysqli_query($koneksi,"UPDATE `events_beverage` SET `nama_events_beverage`= '$_POST[name_beverage]', tag = '$_POST[tag]' WHERE id_events_beverage = '$_POST[id]'");
				
				if(!empty($nama_cover)){
					$cover=upload_gambar($nama_cover,$lokasi_cover,"../../assets/img/events/beverage");
		
					mysqli_query($koneksi,"UPDATE `events_beverage` SET `gambar_beverage`= '$cover' WHERE id_events_beverage = '$_POST[id]'");
					unlink("../../assets/img/events/beverage/small_".$_POST['gambar']);
					unlink("../../assets/img/events/beverage/medium_".$_POST['gambar']); 
					unlink("../../assets/img/events/beverage/big_".$_POST['gambar']);  
				}
				mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('Events beverage','$id_events')");
				mysqli_query($koneksi,"INSERT INTO activity (id_member,kat_act,deskripsi,tgl) VALUES('$u[id_member]','events_submit','Submit Events beverage','$sekarang')");				
				
				header('Location: '.$base_url.'/'.$u['username'].'/my-events/beverage/'.id_masking($_POST['idevents']));	
			}
			
			exit();

		
	
}
else{
	header("Location: ".$base_url."/login-area");
}
?>
