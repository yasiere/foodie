
<?php
$pages = array();
$pages["restaurant"] = "Restaurant";
$pages["food"] = "Food";
$pages["beverage"] = "Beverage";
$pages["process"] = "Process F&B";
$pages["events"] = "Events";
$pages["recipe"] = "Recipe";
// $pages["fgmart"] = "FGMART";
$pages["coupon"] = "Coupon";
$pages["article"] = "Article";
$pages["video"] = "Video";
$pages["all/search"] = "Search";
?>
<header class="navbar navbar-default">
	<div class="container">
		<div class="row">
			<div class="col-16">
				<nav class="pull-right">
					<ul class="menu-top mene-atas">
							<?php
								if(isset($_SESSION['food_member'])){
									$sql=mysqli_query($koneksi,"select * from pesan_sementara p where id_member='$_SESSION[food_member]'");
									$ada=mysqli_num_rows($sql);
									$us=ucwords($u['username']);
									echo"<li>Hello, $us</li>
									<li><a href='$base_url/$u[username]'>Profile</a></li>
									
									<li><a href='$base_url/logout'>Log Out</a></li>";
								}
								else{
									echo"<li><a href='$base_url/sign-in' class='signin'>Sign In</a></li>
									<li><a href='$base_url/sign-up' class='signout'>Sign Up</a></li>";
								}
							?>
							<!-- <li><a href='$base_url/$u[username]/stock-cart'>CART &nbsp;$ada item</a></li> -->
							<li class="over">
								<a href ="#" class="transs">
									Translate
								</a>
							</li>
							<!-- <li>
								<a href="<?php echo"$base_url"; ?>/" class="tab"></a>
								<a href="<?php echo"$base_url"; ?>/" class="laptop"></a>
								<div class="clearfix"></div>
							</li> -->

					</ul>



					<button type="button" class="share men-side">
						<img alt="menu" src="<?php echo"$base_url";?>/assets/images/share.png">
					</button>
					<button type="button" class="plus men-side">
						<img alt="menu" src="<?php echo"$base_url";?>/assets/images/plus.png">
					</button>
					<!-- <button type="button" class="cart men-side">
						<img alt="menu" src="<?php echo"$base_url";?>/assets/images/cart.png">
					</button> -->
					<button type="button" class="globe men-side transs">
						<img alt="menu" src="<?php echo"$base_url";?>/assets/images/globe.png">
					</button>

					<button type="button" class="drawer-toggle men-side">
						<img src="<?php echo"$base_url";?>/assets/images/menu32@64.png">
					</button>
				</nav>
			</div>
			<div class="col-16 mene-atas">
				<nav class="pull-right">
					<ul class="menu-main">
						<?php foreach($pages as $url=>$title):?>
						<li>
							<a <?php if($url === $active):?>class="aktif"<?php endif;?> href="<?php echo"$base_url/pages/$url";?>"><?php echo "$title";?></a>
						</li>
						<?php endforeach;?>
					</ul>
					<div class="clearfix"></div>
				</nav>
			</div>
		</div>
		<a class="logo" href="<?php echo"$base_url"; ?>/home"><img src="<?php echo"$base_url"; ?>/assets/img/theme/logo.jpg"></a>
		<a class="logo2" href="<?php echo"$base_url"; ?>/home"><img src="<?php echo"$base_url"; ?>/assets/images/logo2.png" width='100%'></a>

	</div>
</header>

<!-- <div class="dis-cart">
	<div class="head">
		Your cart
	</div>
	<?php
		$sql=mysqli_query($koneksi,"select * from pesan_sementara p, fgmart_item f, fgmart g where p.id_member='$_SESSION[food_member]' and p.id_fgmart_item=f.id_fgmart_item and f.id_fgmart=g.id_fgmart");
		$ada=mysqli_num_rows($sql);
		if($ada<>0){
			$no=1;
			$total=0;
			while($g=mysqli_fetch_array($sql)){
				echo"
					<div class='col-cart'>
						<img src='$base_url/assets/img/fgmart/small_$g[gambar_fgmart]' width='100%'>
						<br>
						<strong>Foto Martabak</strong><br>
						<div class='font'>$g[jenis]  •  $g[dimensi]</div>
						<div class='fonta'>$ $g[harga]</div>
						<a href='$base_url/pages/fgmart/delete-cart/$g[id_pesan_sementara]'><span class='fa fa-trash-o fa-2x'></span></a>
					</div>
				";
				$total=$total + $g['harga'];
				$no++;
			}
		}
		else{
			echo"No item on cart. Please select item first.";
		}

	 ?>


</div> -->

<div class="dis-plus">
	<div class="head">
		Submit Yours Pages Below
	</div>
	<div class="body" style="padding:0">
		<?php
		if(!empty($_SESSION['food_member'])){
			echo"
			<a href='$base_url/$u[username]/my-restaurant/new' class='s'><img src='$base_url/assets/img/restaurant.png' width='100%'></a>
			<a href='$base_url/$u[username]/my-food/new' class='s'><img src='$base_url/assets/img/food.jpg' width='100%'></a>
			<a href='$base_url/$u[username]/my-beverage/new' class='s'><img src='$base_url/assets/img/beverage.jpg' width='100%'></a>
			<a href='$base_url/$u[username]/my-process/new' class='s'><img src='$base_url/assets/img/process.jpg' width='100%'></a>
			<a href='$base_url/$u[username]/my-events/new' class='s'><img src='$base_url/assets/img/events.jpg' width='100%'></a>
			<a href='$base_url/$u[username]/my-recipe/new' class='s'><img src='$base_url/assets/img/recipe.jpg' width='100%'></a>
			<a href='$base_url/$u[username]/my-article/new' class='s'><img src='$base_url/assets/img/article.jpg' width='100%'></a>
			<a href='$base_url/$u[username]/my-video/new' class='s'><img src='$base_url/assets/img/video.jpg' width='100%'></a>";
		}
		else{
			echo"
			<a href='$base_url/login-area' class='s'><img src='$base_url/assets/img/restaurant.png' width='100%'></a>
			<a href='$base_url/login-area' class='s'><img src='$base_url/assets/img/food.jpg' width='100%'></a>
			<a href='$base_url/login-area' class='s'><img src='$base_url/assets/img/beverage.jpg' width='100%'></a>
			<a href='$base_url/login-area' class='s'><img src='$base_url/assets/img/process.jpg' width='100%'></a>
			<a href='$base_url/login-area' class='s'><img src='$base_url/assets/img/events.jpg' width='100%'></a>
			<a href='$base_url/login-area' class='s'><img src='$base_url/assets/img/recipe.jpg' width='100%'></a>
			<a href='$base_url/login-area' class='s'><img src='$base_url/assets/img/article.jpg' width='100%'></a>
			<a href='$base_url/login-area' class='s'><img src='$base_url/assets/img/video.jpg' width='100%'></a>";
		}
		?>

	</div>
</div>

<div class="dis-share">
	<div class="head">
		Share
	</div>
	<div class="body" style="padding:0">
		<div class="" style="width:50%; margin: 20px auto; text-align: center">
			<img src="<?php echo"$base_url";?>/assets/images/logo2.png" width="40%"><br><br>
			<font size="5px">Share the deliciousness to your friends</font>
		</div>
		<div class="text-center">
			<span class='st_facebook_large' displayText='Facebook'></span>
			<span class='st_twitter_large' displayText='Tweet'></span>
			<span class='st_googleplus_large' displayText='Google +'></span>
			<span class='st_pinterest_large' displayText='Pinterest'></span>
			<span class='st_linkedin_large' displayText='LinkedIn'></span>
			<span class='st_sharethis_large' displayText='ShareThis'></span>
			<span class='st_email_large' displayText='Email'></span>
		</div>
	</div>
</div>

<div class="dis-globe">
	<div class="head">
		Translate
	</div>

</div>


<nav class="drawer-nav" role="navigation">
	<div class="">
		<div class="clos drawer-toggle">
			<img src="<?php echo"$base_url";?>/assets/images/keluar.png" width="100%" alt="" />
		</div>

	<div class='judul-akun' style="margin-top: 0px;">
		 <!-- <div class='text-center'>
			  <img src="<?php echo"$base_url";?>/assets/img/img-akun.jpg"><br>
			  <div class='gesb text-red padding-6'>
					Tamara Ivanovic
					<div class='ger abu'>Female • Indonesia</div>
			  </div>
			  <img src="<?php echo"$base_url";?>/assets/img/fb-abu.png">
			  <img src="<?php echo"$base_url";?>/assets/img/tw-abu.png">
			  <img src="<?php echo"$base_url";?>/assets/img/pin-abu.png">
			  <img src="<?php echo"$base_url";?>/assets/img/g+-abu.png">
			  <img src="<?php echo"$base_url";?>/assets/img/in-abu.png">
		 </div> -->
		 <?php
 			if(isset($_SESSION['food_member'])){
 				$sql=mysqli_query($koneksi,"select * from pesan_sementara p where id_member='$_SESSION[food_member]'");
 				$ada=mysqli_num_rows($sql);
 				$us=ucwords($u['username']);?>
				<div class="text-center">
                <div style=" margin-top: 35px;"></div>
					<img src="<?php echo"$base_url/assets/img/member/$u[gambar_thumb]"; ?>" class="foto_profil"><br>
					<div class='gesb text-red padding-6'>
					<?php echo "Hello, $us"; ?>
					<div class='ger abu'><?php echo"$u[gender]";?> • <?php echo"$u[nama_negara]";?></div>
					</div>
					<a href="<?php echo"$u[social_fb]"; ?>"><img src="<?php echo"$base_url";?>/assets/images/fb-abu.png" width="40px"></a>
					<a href="<?php echo"$u[social_twitter]"; ?>"><img src="<?php echo"$base_url";?>/assets/images/tw-abu.png" width="40px"></a>
					<a href="<?php echo"$u[social_pint]"; ?>"><img src="<?php echo"$base_url";?>/assets/images/pin-abu.png" width="40px"></a>
					<a href="<?php echo"$u[social_google]"; ?>"><img src="<?php echo"$base_url";?>/assets/images/g+-abu.png" width="40px"></a>
					<a href="<?php echo"$u[social_insta]"; ?>"><img src="<?php echo"$base_url";?>/assets/images/in-abu.png" width="40px"></a><br><br>

					<a class='text-red' href='<?php echo"$base_url/$u[username]";?>'>Profile</a><br>
					<a href='<?php echo"$base_url/$u[username]/stock-cart";?>'>CART &nbsp;<?php echo"$ada";?> item</a><br /><br />
					<a href='<?php echo"$base_url/logout";?>' style="padding: 5px 7px; background: #fff;     border-radius: 5px;">Log Out</a>
				</div>

 			<?php }
 			else{
 				echo"<div class='text-center'><a href='$base_url/sign-in' class='signin'>Sign In</a>  <a class='signout' href='$base_url/sign-up'>Sign Up</a></div>";
 			}
 		?>
		</div>

	<ul class="drawer-menu">
		<?php foreach($pages as $url=>$title):?>
		<li>
			<a <?php if ($url === $active) { echo "class='drawer-menu-item'";} else {echo "class='drawer-menu-item aktif'";	} ?> href="<?php echo"$base_url/pages/$url";?>"><?php echo "$title";?></a>
		</li>
		<?php endforeach;?>

		<li class="foot padding-20">
			<font class="gert" style="margin-bottom:5px">FOLLOW US ON</font><br>
			<img src="<?php echo"$base_url";?>/assets/images/fb.png">
			<img src="<?php echo"$base_url";?>/assets/images/tw.png">
			<img src="<?php echo"$base_url";?>/assets/images/g+.png">
			<img src="<?php echo"$base_url";?>/assets/images/pin.png">
			<img src="<?php echo"$base_url";?>/assets/images/ins.png">
			<img src="<?php echo"$base_url";?>/assets/images/rss.png">
		</li>
	</ul>
	<span >


	</span>

</div>
</nav>
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "f4e223ef-536c-443b-80c3-7274d2da1e42", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

<?php error_reporting(0);
	// $hari=date("D");
	// if ($hari=='Sun') {
	// 	$sunrise = $r['resto_time1'];
	// 	$sunset = $r['resto_time2'];
	// 	$rise = $r['resto_time1a'];
	// 	$set = $r['resto_time2a'];
	// }
	// elseif ($hari=='Mon') {
	// 	$sunrise = $r['resto_time3'];
	// 	$sunset = $r['resto_time4'];
	// 	$rise = $r['resto_time3a'];
	// 	$set = $r['resto_time4a'];
	// }
	// elseif ($hari=='Tue') {
	// 	$sunrise = $r['resto_time5'];
	// 	$sunset = $r['resto_time6'];
	// 	$rise = $r['resto_time5a'];
	// 	$set = $r['resto_time6a'];
	// }
	// elseif ($hari=='Wed') {
	// 	$sunrise = $r['resto_time7'];
	// 	$sunset = $r['resto_time8'];
	// 	$rise = $r['resto_time7a'];
	// 	$set = $r['resto_time8a'];
	// }
	// elseif ($hari=='Thu') {
	// 	$sunrise = $r['resto_time9'];
	// 	$sunset = $r['resto_time10'];
	// 	$rise = $r['resto_time9a'];
	// 	$set = $r['resto_time10a'];
	// }
	// elseif ($hari=='Fri') {
	// 	$sunrise = $r['resto_time11'];
	// 	$sunset = $r['resto_time12'];
	// 	$rise = $r['resto_time11a'];
	// 	$set = $r['resto_time12a'];
	// }
	// elseif ($hari=='Sat') {
	// 	$sunrise = $r['resto_time13'];
	// 	$sunset = $r['resto_time14'];
	// 	$rise = $r['resto_time13a'];
	// 	$set = $r['resto_time14a'];
	// }
	// else{
	// 	$sunrise = $r['resto_time1'];
	// 	$sunset = $r['resto_time2'];
	// 	$rise = $r['resto_time1a'];
	// 	$set = $r['resto_time2a'];
	// }

date_default_timezone_set('Asia/Jakarta');

  $hari= date('D');
  if ($hari=='Sun') {
	// if(!empty($r['resto_time1a']) && !empty($r['resto_time2a'])){
	//   $storeSchedule = array('Sun' => array($r['resto_time1'], $r['resto_time2'], $r['resto_time1a'], $r['resto_time2a']));
	// }
	// else{
	//  $storeSchedule = array('Sun' => array($r['resto_time1'], $r['resto_time2']));
	// }
	$st=$r['resto_time1'].$r['resto_time2'];
  }

  if ($hari=='Mon') {
	// if(!empty($r['resto_time3a']) && !empty($r['resto_time4a'])){
	//   $storeSchedule = array('Mon' => array($r['resto_time3'], $r['resto_time4'], $r['resto_time3a'], $r['resto_time4a']));
	// }
	// else{
	//  $storeSchedule = array('Mon' => array($r['resto_time3'], $r['resto_time4']));
	// }
	$st=$r['resto_time3'].$r['resto_time4'];
  }

  if ($hari=='Tue') {
 //    if(!empty($r['resto_time5a']) && !empty($r['resto_time6a'])){
	//   $storeSchedule = array('Tue' => array($r['resto_time5'], $r['resto_time6'], $r['resto_time5a'], $r['resto_time6a']));
	// }
	// else{
	//  $storeSchedule = array('Tue' => array($r['resto_time5'], $r['resto_time6']));
	// }
	$st=$r['resto_time5'].$r['resto_time6'];
  }

  if ($hari=='Wed') {
 //    if(!empty($r['resto_time7a']) && !empty($r['resto_time8a'])){
	//   $storeSchedule = array('Wed' => array($r['resto_time7'], $r['resto_time8'], $r['resto_time7a'], $r['resto_time8a']));
	// }
	// else{
	//  $storeSchedule = array('Wed' => array($r['resto_time7'], $r['resto_time8']));
	// }
	$st=$r['resto_time7'].$r['resto_time8'];
  }

  if ($hari=='Thu') {
 //    if(!empty($r['resto_time9a']) && !empty($r['resto_time10a'])){
	//   $storeSchedule = array('Thu' => array($r['resto_time9'], $r['resto_time10'], $r['resto_time9a'], $r['resto_time10a']));
	// }
	// else{
	//  $storeSchedule = array('Thu' => array($r['resto_time9'], $r['resto_time10']));
	// }
	$st=$r['resto_time9'].$r['resto_time10'];
  }

  if ($hari=='Fri') {
 //    if(!empty($r['resto_time11a']) && !empty($r['resto_time12a'])){
	//   $storeSchedule = array('Fri' => array($r['resto_time11'], $r['resto_time12'], $r['resto_time11a'], $r['resto_time12a']));
	// }
	// else{
	//  $storeSchedule = array('Fri' => array($r['resto_time11'], $r['resto_time12']));
	// }
	$st=$r['resto_time11'].$r['resto_time12'];
  }
  
  if ($hari=='Sat') {
 //    if(!empty($r['resto_time13a']) && !empty($r['resto_time14a'])){
	//   $storeSchedule = array('Sat' => array($r['resto_time13'], $r['resto_time14'], $r['resto_time13a'], $r['resto_time14a']));
	// }
	// else{
	//  $storeSchedule = in_array('Sat' => array($r['resto_time13'], $r['resto_time14']));
	// }
	$st=$r['resto_time13'].$r['resto_time14'];
  }

// $st=$r['resto_time13'];
// $storeSchedule = [

//     'Sat' => ['05:00 PM'=> '11:50 PM', $r['resto_time13a'] => $r['resto_time13']]
// ];
?>
