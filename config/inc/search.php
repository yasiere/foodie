<div id="carousel-example-generic" class="carousel slide mb20 mt20" data-ride="carousel">
	<!-- Indicators -->
	<ol class="carousel-indicators">
		<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
		<li data-target="#carousel-example-generic" data-slide-to="1"></li>
	</ol>
	<!-- Wrapper for slides -->
	<div class="carousel-inner" role="listbox">
		<div class="item active sign-banner">
			<div class="carousel-caption">
				<h4 class="f-merah">SIGN UP NOW!</h4>
				<p>Be our member and and let the world know your post about Restaurants, Recipes or pictures of a food or beverage. Take us away from hungry! </p>
				<?php if(empty($_SESSION['food_member'])){
					echo"<div class='text-center'><a href='$base_url/sign-up' class='btn btn-danger'>Sign Up</a></div>";
				} ?>
			</div>
		</div>
		<div class="item search-banner">
			<div class="carousel-caption">
				<h4 class="f-merah">SUBMIT &amp; SEARCH</h4>
				<div class="dropdown">
					<button type="button" class="btn btn-danger btn-block dropdown-toggle mb10" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-check"></i> New Submit</button>
					<ul class="dropdown-menu" role="menu">
						<?php
						if(!empty($_SESSION['food_member'])){
							echo"<li><a href='$base_url/$u[username]/my-restaurant/new'>Restaurant</a></li>
							<li><a href='$base_url/$u[username]/my-food/new'>Food</a></li>
							<li><a href='$base_url/$u[username]/my-beverage/new'>Beverage</a></li>
							<li><a href='$base_url/$u[username]/my-recipe/new'>Recipe</a></li>
							<li><a href='$base_url/$u[username]/my-article/new'>Article</a></li>
							<li><a href='$base_url/$u[username]/my-video/new'>Video</a></li>";
						}
						else{
							echo"<li><a href='$base_url/login-area'>Restaurant</a></li>
							<li><a href='$base_url/login-area'>Food</a></li>
							<li><a href='$base_url/login-area'>Beverage</a></li>
							<li><a href='$base_url/login-area'>Recipe</a></li>
							<li><a href='$base_url/login-area'>Article</a></li>
							<li><a href='$base_url/login-area'>Video</a></li>";
						}
						?>
					</ul>
				</div>
				<p>Post and share yours and make this life more tasty!</p>
				<p>Are you hungry and looking for some food or a restaurant? Search below!</p>
				<form method="get" action="<?php echo"$base_url/pages/all/search"; ?>">
					<strong>Search</strong>
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search.." name="keyword" required>
						<span class="input-group-btn">
							<button class="btn btn-danger" type="submit"><i class="fa fa-search"></i></button>
						</span>
					</div><!-- /input-group -->
				</form>
			</div>
		</div>
	</div>
</div>
