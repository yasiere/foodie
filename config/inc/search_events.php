<div class="panel-groupmb10 mt10" id="accordionsssasu" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default head-pan">
        <div class="panel-heading jud-pan" role="tab" id="recipe1" data-toggle="collapse" data-parent="#accordionsssasu" href="#collapse_events" aria-expanded="true" aria-controls="collapse_events">
            <h4 class="panel-title">
                <a>❯ Events</a>
            </h4>
        </div>
        <div id="collapse_events" class="panel-collapse collapse <?php echo (isset($in) && $in == 'events') ? 'in' : ''; ?>" role="tabpanel" aria-labelledby="recipe1">
            <div class="panel-body">
                <form method="get" action="<?php echo"$base_url/pages/events/search/"; ?>" class="border-form" id="ale-form" onsubmit="myFunction()">
                    <input type="hidden" name="sort" value="<?php echo"$_GET[sort]"; ?>">
                    <input type="hidden" name="batas" value="<?php echo"$_GET[batas]"; ?>">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading1">
                            <h4 class="panel-title">
                                <a <?php error_reporting(0); if($in == 'events' && $_GET['sort']=='total DESC'){ echo "class='akt'";} ?> href="<?php echo"$base_url" ?>/pages/events/search/?sort=total+DESC&batas=25">Top Rangking</a>
                            </h4>
                        </div>
                        <div class="panel-heading" role="tab" id="heading1">
                            <h4 class="panel-title">
                                <a <?php if($in == 'events' && $_GET['sort']=='dilihat DESC'){ echo "class='akt'";} ?> href="<?php echo"$base_url" ?>/pages/events/search/?sort=dilihat+DESC&batas=25">Popular</a>
                            </h4>
                        </div>
                        <div class="panel-heading" role="tab" id="heading1">
                            <h4 class="panel-title">
                                <a <?php if($in == 'events' && $_GET['sort']=='direkomendasi DESC'){ echo "class='akt'";} ?> href="<?php echo"$base_url" ?>/pages/events/search/?sort=direkomendasi+DESC">Recomended</a>
                            </h4>
                        </div>
                        <div class="panel-heading" role="tab" id="headingevents1">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" <?php if($in == 'events' && !empty($_GET['country']) || !empty($_GET['state']) || !empty($_GET['city'])
                                    ){ echo "class='akt'";} ?> data-parent="#collapse" href="#collapseevents1" aria-expanded="true" aria-controls="collapseevents1">Location Filter</a>
                            </h4>
                        </div>
                        <div id="collapseevents1"  <?php if($in == 'events' && !empty($_GET['country']) || !empty($_GET['state']) || !empty($_GET['city'])
                            ){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="headingevents1">
                            <div class="panel-body">
                                <div class="form-group">
                                    <label>Country</label>
                                    <select class="form-control" name="country" id="negara5">
                                        <option value="" selected>Select Country</option>
                                        <?php
                                        $sql=mysqli_query($koneksi,"select * from negara order by nama_negara asc");
                                        while($b=mysqli_fetch_array($sql)){
                                            if($b['id_negara']==$_GET['country']){
                                                echo"<option value='$b[id_negara]' selected>$b[nama_negara]</option>";
                                            }
                                            else{
                                                echo"<option value='$b[id_negara]'>$b[nama_negara]</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>States/Province</label>
                                    <select class="form-control" name="state" id="propinsi5">
                                        <option value="" selected>Select States/ Province</option>
                                        <?php
                                        $sql=mysqli_query($koneksi,"select * from propinsi where id_negara='$_GET[country]' order by nama_propinsi asc");
                                        while($b=mysqli_fetch_array($sql)){
                                            if($b['id_propinsi']==$_GET['state']){
                                                echo"<option value='$b[id_propinsi]' selected>$b[nama_propinsi]</option>";
                                            }
                                            else{
                                                echo"<option value='$b[id_propinsi]'>$b[nama_propinsi]</option>";
                                            }
                                        }?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>City</label>
                                    <select class="form-control" name="city" id="kota5">
                                        <option value="" selected>Select City</option>
                                        <?php
                                        $sql=mysqli_query($koneksi,"select * from kota where id_propinsi='$_GET[state]' order by nama_kota asc");
                                        while($b=mysqli_fetch_array($sql)){
                                            if($b['id_kota']==$_GET['city']){
                                                echo"<option value='$b[id_kota]' selected>$b[nama_kota]</option>";
                                            }
                                            else{
                                                echo"<option value='$b[id_kota]'>$b[nama_kota]</option>";
                                            }
                                        }?>
                                    </select>
                                </div>

                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingevents2cat">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" <?php if($in == 'events' && !empty($_GET['category'])){ echo "class='akt'";} ?> data-parent="#accordionbeverage" href="#collapseevents2cat" aria-expanded="true" aria-controls="collapsebeverage2tag">Category</a>
                                </h4>
                            </div>
                            <div id="collapseevents2cat" <?php if($in == 'events' && !empty($_GET['category']) ){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="headingbeverage2cat">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <select class="form-control" name="category">
                                            <option value="" selected>Select Category</option>
                                            <?php
                                            $sql=mysqli_query($koneksi,"select * from events_category order by nama_events_category asc");
                                            while($b=mysqli_fetch_array($sql)){

                                                if($b['id_events_category']==$_GET['category']){
                                                    echo"<option value='$b[id_events_category]' selected>$b[nama_events_category]</option>";
                                                }
                                                else{
                                                    echo"<option value='$b[id_events_category]'>$b[nama_events_category]</option>";
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingevents2coe">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" <?php if($in == 'events' && !empty($_GET['year'])){ echo "class='akt'";} ?> data-parent="#accordionbeverage" href="#collapseevents2coe" aria-expanded="true" aria-controls="collapsebeverage2tag">Calender of Events</a>
                                </h4>
                            </div>
                            <div id="collapseevents2coe" <?php if($in == 'events' && !empty($_GET['year']) ){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="headingbeverage2cat">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label>Month</label>	
                                        <select class="form-control" name="month">
                                            <option value="">Select Month</option>

                                            <option value="1" <?php echo ($_GET['month'] == 1) ? 'selected' : '' ; ?>> January </option>
                                            <option value="2" <?php echo ($_GET['month'] == 2) ? 'selected' : '' ; ?>> February </option>
                                            <option value="3" <?php echo ($_GET['month'] == 3) ? 'selected' : '' ; ?>> March </option>
                                            <option value="4" <?php echo ($_GET['month'] == 4) ? 'selected' : '' ; ?>> April </option>
                                            <option value="5" <?php echo ($_GET['month'] == 5) ? 'selected' : '' ; ?>> May </option>
                                            <option value="6" <?php echo ($_GET['month'] == 6) ? 'selected' : '' ; ?>> June </option>
                                            <option value="7" <?php echo ($_GET['month'] == 7) ? 'selected' : '' ; ?>> July </option>
                                            <option value="8" <?php echo ($_GET['month'] == 8) ? 'selected' : '' ; ?>> August </option>
                                            <option value="9" <?php echo ($_GET['month'] == 9) ? 'selected' : '' ; ?>> September </option>
                                            <option value="10" <?php echo ($_GET['month'] == 10) ? 'selected' : '' ; ?>> October </option>
                                            <option value="11" <?php echo ($_GET['month'] == 11) ? 'selected' : '' ; ?>> November </option>
                                            <option value="12" <?php echo ($_GET['month'] == 12) ? 'selected' : '' ; ?>> December </option>
                                            <?php  ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                    <!-- ".$_GET['year'] = $i ? 'selected' : ''." -->
                                        <label>Year</label>	
                                        <select class="form-control" name="year">
                                            <option value="" selected>Select Year</option>
                                            <?php
                                                $futureDate=date('Y', strtotime('+3 year'));
                                                for($i=2000;$i<=$futureDate;$i++){
                                                    $retVal = ($_GET['year'] == $i) ? 'selected' : '' ;
                                                    echo "<option value=".$i." $retVal>".$i."</option>";																		
                                                }
                                            ?>																
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingratingevents">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" <?php if($in == 'events' && !empty($_GET['events_rate']) || !empty($_GET['clean']) || !empty($_GET['security']) || !empty($_GET['comfort'])
                                        || !empty($_GET['organise'])){ echo "class='akt'";} ?> data-parent="#accordion" href="#collapseratingevents" aria-expanded="true" aria-controls="collapseratingevents">Events Rating</a>
                                </h4>
                            </div>
                            <div id="collapseratingevents" <?php if($in == 'events' && !empty($_GET['events_rate']) || !empty($_GET['clean']) || !empty($_GET['security']) || !empty($_GET['comfort'])
                                || !empty($_GET['organise'])){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="headingbeverage3">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label>Overall</label>
                                        <select class="form-control" name="events_rate">
                                            <option value="">Select Rate of Events</option>
                                            <option value="1" <?php if($_GET['events_rate']==1){ echo "selected";}?>>1 Star</option>
                                            <option value="2" <?php if($_GET['events_rate']==2){ echo "selected";}?>>2 Star</option>
                                            <option value="3" <?php if($_GET['events_rate']==3){ echo "selected";}?>>3 Star</option>
                                            <option value="4" <?php if($_GET['events_rate']==4){ echo "selected";}?>>4 Star</option>
                                            <option value="5" <?php if($_GET['events_rate']==5){ echo "selected";}?>>5 Star</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Security</label>
                                        <select class="form-control select" name="security">
                                            <option value="" selected>Select Rate of Security</option>
                                            <option value="1" <?php if($_GET['security']=='1'){ echo "selected";}?>>1 Star</option>
                                            <option value="2" <?php if($_GET['security']=='2'){ echo "selected";}?>>2 Star</option>
                                            <option value="3" <?php if($_GET['security']=='3'){ echo "selected";}?>>3 Star</option>
                                            <option value="4" <?php if($_GET['security']=='4'){ echo "selected";}?>>4 Star</option>
                                            <option value="5" <?php if($_GET['security']=='5'){ echo "selected";}?>>5 Star</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Clean</label>
                                        <select class="form-control select" name="clean">
                                            <option value="" selected>Select Rate of Clean</option>
                                            <option value="1" <?php if($_GET['clean']=='1'){ echo "selected";}?>>1 Star</option>
                                            <option value="2" <?php if($_GET['clean']=='2'){ echo "selected";}?>>2 Star</option>
                                            <option value="3" <?php if($_GET['clean']=='3'){ echo "selected";}?>>3 Star</option>
                                            <option value="4" <?php if($_GET['clean']=='4'){ echo "selected";}?>>4 Star</option>
                                            <option value="5" <?php if($_GET['clean']=='5'){ echo "selected";}?>>5 Star</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Comfort</label>
                                        <select class="form-control select" name="comfort">
                                            <option value="" selected>Select Rate of Comfort</option>
                                            <option value="1" <?php if($_GET['comfort']=='1'){ echo "selected";}?>>1 Star</option>
                                            <option value="2" <?php if($_GET['comfort']=='2'){ echo "selected";}?>>2 Star</option>
                                            <option value="3" <?php if($_GET['comfort']=='3'){ echo "selected";}?>>3 Star</option>
                                            <option value="4" <?php if($_GET['comfort']=='4'){ echo "selected";}?>>4 Star</option>
                                            <option value="5" <?php if($_GET['comfort']=='5'){ echo "selected";}?>>5 Star</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>organise</label>
                                        <select class="form-control select" name="organise">
                                            <option value="" selected>Select Rate of organise</option>
                                            <option value="1" <?php if($_GET['organise']=='1'){ echo "selected";}?>>1 Star</option>
                                            <option value="2" <?php if($_GET['organise']=='2'){ echo "selected";}?>>2 Star</option>
                                            <option value="3" <?php if($_GET['organise']=='3'){ echo "selected";}?>>3 Star</option>
                                            <option value="4" <?php if($_GET['organise']=='4'){ echo "selected";}?>>4 Star</option>
                                            <option value="5" <?php if($_GET['organise']=='5'){ echo "selected";}?>>5 Star</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <button type="submit" class="btn btn-success btn-block">Search</button>
                    <a href="<?php echo $base_url;?>/pages/events/search/" class="btn btn-warning btn-block">Reset</a>
                </form>
            </div>
        </div>
    </div>
</div>