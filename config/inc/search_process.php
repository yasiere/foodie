<div class="panel-groupmb10 mt10" id="accordionprocess" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default head-pan">
        <div class="panel-heading jud-pan" role="tab" id="process" data-toggle="collapse" data-parent="#accordionprocess" href="#collapseprocess" aria-expanded="true" aria-controls="collapseprocess">
            <h4 class="panel-title">
                <a>❯ Process Food</a>
            </h4>
        </div>
        <div id="collapseprocess" class="panel-collapse collapse <?php echo (isset($in) && $in == 'process') ? 'in' : ''; ?>" role="tabpanel" aria-labelledby="process">
            <div class="panel-body">
                <form method="get" action="<?php echo"$base_url/pages/process/search/"; ?>" class="border-form" id="ale-form" onsubmit="myFunction()">
                    <input type="hidden" name="sort" value="<?php echo"$_GET[sort]"; ?>">
                    <input type="hidden" name="batas" value="<?php echo"$_GET[batas]"; ?>">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading1">
                            <h4 class="panel-title">
                                <a <?php error_reporting(0); if($in == 'process' && $_GET['sort']=='total DESC'){ echo "class='akt'";} ?> href="<?php echo"$base_url" ?>/pages/process/search/?sort=total+DESC&batas=25">Top Rangking</a>
                            </h4>
                        </div>
                        <div class="panel-heading" role="tab" id="heading1">
                            <h4 class="panel-title">
                                <a <?php if($in == 'process' && $_GET['sort']=='dilihat DESC'){ echo "class='akt'";} ?> href="<?php echo"$base_url" ?>/pages/process/search/?sort=dilihat+DESC&batas=25">Popular</a>
                            </h4>
                        </div>
                        <div class="panel-heading" role="tab" id="heading1">
                            <h4 class="panel-title">
                                <a <?php if($in == 'process' && $_GET['sort']=='direkomendasi DESC'){ echo "class='akt'";} ?> href="<?php echo"$base_url" ?>/pages/process/search/?sort=direkomendasi+DESC">Recomended</a>
                            </h4>
                        </div>
                        <div class="panel-heading" role="tab" id="headingevents1">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" <?php if($in == 'process' && !empty($_GET['countryoforigin'])){ echo "class='akt'";} ?> data-parent="#collapse" href="#cooprocess-food" aria-expanded="true" aria-controls="cooprocess-food">Country of Product Origin</a>
                            </h4>
                        </div>
                        <div id="cooprocess-food"  <?php if($in == 'process' && !empty($_GET['countryoforigin'])){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="headingevents1">
                            <div class="panel-body">
                                <div class="form-group">
                                    <select class="form-control" name="countryoforigin" id="negara5">
                                        <option value="" selected>Select Country</option>
                                        <?php
                                        $sql=mysqli_query($koneksi,"select * from negara order by nama_negara asc");
                                        while($b=mysqli_fetch_array($sql)){
                                            if($b['id_negara']==$_GET['countryoforigin']){
                                                echo"<option value='$b[id_negara]' selected>$b[nama_negara]</option>";
                                            }
                                            else{
                                                echo"<option value='$b[id_negara]'>$b[nama_negara]</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="panel-heading" role="tab" id="headingevents1">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" <?php if($in == 'process' && !empty($_GET['reviewedcountry'])){ echo "class='akt'";} ?> data-parent="#collapse" href="#rcprocess-food" aria-expanded="true" aria-controls="rcprocess-food">Reviewed Country</a>
                            </h4>
                        </div>
                        <div id="rcprocess-food"  <?php if($in == 'process' && !empty($_GET['reviewedcountry'])){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="headingevents1">
                            <div class="panel-body">
                                <div class="form-group">
                                    <select class="form-control" name="reviewedcountry" id="negara5">
                                        <option value="" selected>Select Country</option>
                                        <?php
                                        $sql=mysqli_query($koneksi,"select * from negara order by nama_negara asc");
                                        while($b=mysqli_fetch_array($sql)){
                                            if($b['nama_negara']==$_GET['reviewedcountry']){
                                                echo"<option value='$b[nama_negara]' selected>$b[nama_negara]</option>";
                                            }
                                            else{
                                                echo"<option value='$b[nama_negara]'>$b[nama_negara]</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingevents2cat">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" <?php if($in == 'process' && !empty($_GET['category'])){ echo "class='akt'";} ?> data-parent="#accordionbeverage" href="#catprocess-food" aria-expanded="true" aria-controls="collapsebeverage2tag">Category</a>
                                </h4>
                            </div>
                            <div id="catprocess-food" <?php if($in == 'process' && !empty($_GET['category']) ){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="headingbeverage2cat">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <select class="form-control" name="category">
                                            <option value="" selected>Select Category</option>
                                            <?php
                                            $sql=mysqli_query($koneksi,"SELECT * from process_category where type_process='1' order by nama_process_category asc");
                                            while($b=mysqli_fetch_array($sql)){

                                                if($b['id_process_category']==$_GET['category']){
                                                    echo"<option value='$b[id_process_category]' selected>$b[nama_process_category]</option>";
                                                }
                                                else{
                                                    echo"<option value='$b[id_process_category]'>$b[nama_process_category]</option>";
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingratingevents">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" <?php if($in == 'process' && !empty($_GET['process_rate']) || !empty($_GET['expired_priode']) || !empty($_GET['taste']) || !empty($_GET['freshness'])
                                        || !empty($_GET['serving'])){ echo "class='akt'";} ?> data-parent="#accordion" href="#ratingprocess-food" aria-expanded="true" aria-controls="ratingprocess-food">Rating</a>
                                </h4>
                            </div>
                            <div id="ratingprocess-food" <?php if($in == 'process' && !empty($_GET['process_rate']) || !empty($_GET['expired_priode']) || !empty($_GET['taste']) || !empty($_GET['freshness'])
                                        || !empty($_GET['serving'])){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="headingbeverage3">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label>Overall</label>
                                        <select class="form-control" name="process_rate">
                                            <option value="">Select Rate of Process</option>
                                            <option value="1" <?php if($_GET['process_rate']==1){ echo "selected";}?>>1 Star</option>
                                            <option value="2" <?php if($_GET['process_rate']==2){ echo "selected";}?>>2 Star</option>
                                            <option value="3" <?php if($_GET['process_rate']==3){ echo "selected";}?>>3 Star</option>
                                            <option value="4" <?php if($_GET['process_rate']==4){ echo "selected";}?>>4 Star</option>
                                            <option value="5" <?php if($_GET['process_rate']==5){ echo "selected";}?>>5 Star</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Expired Periode</label>
                                        <select class="form-control select" name="expired_priode">
                                            <option value="" selected>Select Rate of Expired Periode</option>
                                            <option value="1" <?php if($_GET['expired_priode']=='1'){ echo "selected";}?>>1 Star</option>
                                            <option value="2" <?php if($_GET['expired_priode']=='2'){ echo "selected";}?>>2 Star</option>
                                            <option value="3" <?php if($_GET['expired_priode']=='3'){ echo "selected";}?>>3 Star</option>
                                            <option value="4" <?php if($_GET['expired_priode']=='4'){ echo "selected";}?>>4 Star</option>
                                            <option value="5" <?php if($_GET['expired_priode']=='5'){ echo "selected";}?>>5 Star</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Taste</label>
                                        <select class="form-control select" name="taste">
                                            <option value="" selected>Select Rate of Taste</option>
                                            <option value="1" <?php if($_GET['taste']=='1'){ echo "selected";}?>>1 Star</option>
                                            <option value="2" <?php if($_GET['taste']=='2'){ echo "selected";}?>>2 Star</option>
                                            <option value="3" <?php if($_GET['taste']=='3'){ echo "selected";}?>>3 Star</option>
                                            <option value="4" <?php if($_GET['taste']=='4'){ echo "selected";}?>>4 Star</option>
                                            <option value="5" <?php if($_GET['taste']=='5'){ echo "selected";}?>>5 Star</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Freshness</label>
                                        <select class="form-control select" name="freshness">
                                            <option value="" selected>Select Rate of Freshness</option>
                                            <option value="1" <?php if($_GET['freshness']=='1'){ echo "selected";}?>>1 Star</option>
                                            <option value="2" <?php if($_GET['freshness']=='2'){ echo "selected";}?>>2 Star</option>
                                            <option value="3" <?php if($_GET['freshness']=='3'){ echo "selected";}?>>3 Star</option>
                                            <option value="4" <?php if($_GET['freshness']=='4'){ echo "selected";}?>>4 Star</option>
                                            <option value="5" <?php if($_GET['freshness']=='5'){ echo "selected";}?>>5 Star</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Serving</label>
                                        <select class="form-control select" name="serving">
                                            <option value="" selected>Select Rate of Serving</option>
                                            <option value="1" <?php if($_GET['serving']=='1'){ echo "selected";}?>>1 Star</option>
                                            <option value="2" <?php if($_GET['serving']=='2'){ echo "selected";}?>>2 Star</option>
                                            <option value="3" <?php if($_GET['serving']=='3'){ echo "selected";}?>>3 Star</option>
                                            <option value="4" <?php if($_GET['serving']=='4'){ echo "selected";}?>>4 Star</option>
                                            <option value="5" <?php if($_GET['serving']=='5'){ echo "selected";}?>>5 Star</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <button type="submit" class="btn btn-success btn-block">Search</button>
                    <a href="<?php echo $base_url;?>/pages/process/search/" class="btn btn-warning btn-block">Reset</a>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="panel-groupmb10 mt10" id="accordionprocess_beverage" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default head-pan">
        <div class="panel-heading jud-pan" role="tab" id="process" data-toggle="collapse" data-parent="#accordionprocess_beverage" href="#collapseprocess_beverage" aria-expanded="true" aria-controls="collapseprocess">
            <h4 class="panel-title">
                <a>❯ Process Beverage</a>
            </h4>
        </div>
        <div id="collapseprocess_beverage" class="panel-collapse collapse <?php echo (isset($in) && $in == 'process_beverage') ? 'in' : ''; ?>" role="tabpanel" aria-labelledby="process">
            <div class="panel-body">
                <form method="get" action="<?php echo"$base_url/pages/process_beverage/search/"; ?>" class="border-form" id="ale-form" onsubmit="myFunction()">
                    <input type="hidden" name="sort" value="<?php echo"$_GET[sort]"; ?>">
                    <input type="hidden" name="batas" value="<?php echo"$_GET[batas]"; ?>">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading1">
                            <h4 class="panel-title">
                                <a <?php error_reporting(0); if($in == 'process_beverage' && $_GET['sort']=='total DESC'){ echo "class='akt'";} ?> href="<?php echo"$base_url" ?>/pages/process_beverage/search/?sort=total+DESC&batas=25">Top Rangking</a>
                            </h4>
                        </div>
                        <div class="panel-heading" role="tab" id="heading1">
                            <h4 class="panel-title">
                                <a <?php if($in == 'process_beverage' && $_GET['sort']=='dilihat DESC'){ echo "class='akt'";} ?> href="<?php echo"$base_url" ?>/pages/process_beverage/search/?sort=dilihat+DESC&batas=25">Popular</a>
                            </h4>
                        </div>
                        <div class="panel-heading" role="tab" id="heading1">
                            <h4 class="panel-title">
                                <a <?php if($in == 'process_beverage' && $_GET['sort']=='direkomendasi DESC'){ echo "class='akt'";} ?> href="<?php echo"$base_url" ?>/pages/process_beverage/search/?sort=direkomendasi+DESC">Recomended</a>
                            </h4>
                        </div>
                        <div class="panel-heading" role="tab" id="headingevents1">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" <?php if($in == 'process_beverage' && !empty($_GET['countryoforigin'])){ echo "class='akt'";} ?> data-parent="#collapse" href="#cooprocess-beverage" aria-expanded="true" aria-controls="cooprocess-beverage">Country of Product Origin</a>
                            </h4>
                        </div>
                        <div id="cooprocess-beverage"  <?php if($in == 'process_beverage' && !empty($_GET['countryoforigin'])){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="headingevents1">
                            <div class="panel-body">
                                <div class="form-group">
                                    <select class="form-control" name="countryoforigin" id="negara5">
                                        <option value="" selected>Select Country</option>
                                        <?php
                                        $sql=mysqli_query($koneksi,"select * from negara order by nama_negara asc");
                                        while($b=mysqli_fetch_array($sql)){
                                            if($b['id_negara']==$_GET['countryoforigin']){
                                                echo"<option value='$b[id_negara]' selected>$b[nama_negara]</option>";
                                            }
                                            else{
                                                echo"<option value='$b[id_negara]'>$b[nama_negara]</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="panel-heading" role="tab" id="headingevents1">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" <?php if($in == 'process_beverage' && !empty($_GET['reviewedcountry'])){ echo "class='akt'";} ?> data-parent="#collapse" href="#rcprocess-beverage" aria-expanded="true" aria-controls="rcprocess-beverage">Reviewed Country</a>
                            </h4>
                        </div>
                        <div id="rcprocess-beverage"  <?php if($in == 'process_beverage' && !empty($_GET['reviewedcountry'])){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="headingevents1">
                            <div class="panel-body">
                                <div class="form-group">
                                    <select class="form-control" name="reviewedcountry" id="negara5">
                                        <option value="" selected>Select Country</option>
                                        <?php
                                        $sql=mysqli_query($koneksi,"select * from negara order by nama_negara asc");
                                        while($b=mysqli_fetch_array($sql)){
                                            if($b['nama_negara']==$_GET['reviewedcountry']){
                                                echo"<option value='$b[nama_negara]' selected>$b[nama_negara]</option>";
                                            }
                                            else{
                                                echo"<option value='$b[nama_negara]'>$b[nama_negara]</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingevents2cat">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" <?php if($in == 'process_beverage' && !empty($_GET['category'])){ echo "class='akt'";} ?> data-parent="#accordionbeverage" href="#catprocess-beverage" aria-expanded="true" aria-controls="collapsebeverage2tag">Category</a>
                                </h4>
                            </div>
                            <div id="catprocess-beverage" <?php if($in == 'process_beverage' && !empty($_GET['category']) ){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="headingbeverage2cat">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <select class="form-control" name="category">
                                            <option value="" selected>Select Category</option>
                                            <?php
                                            $sql=mysqli_query($koneksi,"SELECT * from process_category where type_process='2' order by nama_process_category asc");
                                            while($b=mysqli_fetch_array($sql)){

                                                if($b['id_process_category']==$_GET['category']){
                                                    echo"<option value='$b[id_process_category]' selected>$b[nama_process_category]</option>";
                                                }
                                                else{
                                                    echo"<option value='$b[id_process_category]'>$b[nama_process_category]</option>";
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingratingevents">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" <?php if($in == 'process_beverage' && !empty($_GET['process_rate']) || !empty($_GET['expired_priode']) || !empty($_GET['taste']) || !empty($_GET['freshness'])
                                        || !empty($_GET['serving'])){ echo "class='akt'";} ?> data-parent="#accordion" href="#ratingprocess-beverage" aria-expanded="true" aria-controls="ratingprocess-beverage">Rating</a>
                                </h4>
                            </div>
                            <div id="ratingprocess-beverage" <?php if($in == 'process_beverage' && !empty($_GET['process_rate']) || !empty($_GET['expired_priode']) || !empty($_GET['taste']) || !empty($_GET['freshness'])
                                        || !empty($_GET['serving'])){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="headingbeverage3">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label>Overall</label>
                                        <select class="form-control" name="process_rate">
                                            <option value="">Select Rate of Process</option>
                                            <option value="1" <?php if($_GET['process_rate']==1){ echo "selected";}?>>1 Star</option>
                                            <option value="2" <?php if($_GET['process_rate']==2){ echo "selected";}?>>2 Star</option>
                                            <option value="3" <?php if($_GET['process_rate']==3){ echo "selected";}?>>3 Star</option>
                                            <option value="4" <?php if($_GET['process_rate']==4){ echo "selected";}?>>4 Star</option>
                                            <option value="5" <?php if($_GET['process_rate']==5){ echo "selected";}?>>5 Star</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Expired Periode</label>
                                        <select class="form-control select" name="expired_priode">
                                            <option value="" selected>Select Rate of Expired Periode</option>
                                            <option value="1" <?php if($_GET['expired_priode']=='1'){ echo "selected";}?>>1 Star</option>
                                            <option value="2" <?php if($_GET['expired_priode']=='2'){ echo "selected";}?>>2 Star</option>
                                            <option value="3" <?php if($_GET['expired_priode']=='3'){ echo "selected";}?>>3 Star</option>
                                            <option value="4" <?php if($_GET['expired_priode']=='4'){ echo "selected";}?>>4 Star</option>
                                            <option value="5" <?php if($_GET['expired_priode']=='5'){ echo "selected";}?>>5 Star</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Taste</label>
                                        <select class="form-control select" name="taste">
                                            <option value="" selected>Select Rate of Taste</option>
                                            <option value="1" <?php if($_GET['taste']=='1'){ echo "selected";}?>>1 Star</option>
                                            <option value="2" <?php if($_GET['taste']=='2'){ echo "selected";}?>>2 Star</option>
                                            <option value="3" <?php if($_GET['taste']=='3'){ echo "selected";}?>>3 Star</option>
                                            <option value="4" <?php if($_GET['taste']=='4'){ echo "selected";}?>>4 Star</option>
                                            <option value="5" <?php if($_GET['taste']=='5'){ echo "selected";}?>>5 Star</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Freshness</label>
                                        <select class="form-control select" name="freshness">
                                            <option value="" selected>Select Rate of Freshness</option>
                                            <option value="1" <?php if($_GET['freshness']=='1'){ echo "selected";}?>>1 Star</option>
                                            <option value="2" <?php if($_GET['freshness']=='2'){ echo "selected";}?>>2 Star</option>
                                            <option value="3" <?php if($_GET['freshness']=='3'){ echo "selected";}?>>3 Star</option>
                                            <option value="4" <?php if($_GET['freshness']=='4'){ echo "selected";}?>>4 Star</option>
                                            <option value="5" <?php if($_GET['freshness']=='5'){ echo "selected";}?>>5 Star</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Serving</label>
                                        <select class="form-control select" name="serving">
                                            <option value="" selected>Select Rate of Serving</option>
                                            <option value="1" <?php if($_GET['serving']=='1'){ echo "selected";}?>>1 Star</option>
                                            <option value="2" <?php if($_GET['serving']=='2'){ echo "selected";}?>>2 Star</option>
                                            <option value="3" <?php if($_GET['serving']=='3'){ echo "selected";}?>>3 Star</option>
                                            <option value="4" <?php if($_GET['serving']=='4'){ echo "selected";}?>>4 Star</option>
                                            <option value="5" <?php if($_GET['serving']=='5'){ echo "selected";}?>>5 Star</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <button type="submit" class="btn btn-success btn-block">Search</button>
                    <a href="<?php echo $base_url;?>/pages/process_beverage/search" class="btn btn-warning btn-block">Reset</a>
                </form>
            </div>
        </div>
    </div>
</div>