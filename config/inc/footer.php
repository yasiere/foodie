<footer>
	<div class="container">
		<div class="wrap mene-atas">
			<img src="<?php echo"$base_url"; ?>/assets/img/theme/logo_footer.png" class="logo-footer">
			<ul class="social">
				<li><em>follow us on</em></li>
				<li><a href="https://www.facebook.com/foodieguidances"><img src="<?php echo"$base_url"; ?>/assets/img/theme/icon_fb.png"></a></li>
				<li><a href="https://twitter.com/foodieguidances"><img src="<?php echo"$base_url"; ?>/assets/img/theme/icon_tw.png"></a></li>
				<li><a href="https://id.pinterest.com/foodieguidances"><img src="<?php echo"$base_url"; ?>/assets/img/theme/icon_pinterest.png"></a></li>
				<li><a href="https://plus.google.com/+foodieguidances"><img src="<?php echo"$base_url"; ?>/assets/img/theme/icon_google.png"></a></li>
				<li><a href="https://www.instagram.com/foodieguidances_com/"><img src="<?php echo"$base_url"; ?>/assets/img/theme/icon_rss.png"></a></li>
				<!-- <li><em>Mobile or Dekstop</em></li>
				<li>
					<a href="<?php echo"$base_url"; ?>/" class="tab"></a>
					<a href="<?php echo"$base_url"; ?>/" class="laptop"></a>
					<div class="clearfix"></div>
				</li> -->
			</ul>
		</div>
		<div class="wrap">
			<ul class="footer-menu">
				<li>
					<ul>
						<li>Restaurant</li>
						<!-- <li><a href="<?php echo"$base_url/pages/restaurant/search/"; ?>">All Restaurant</a></li> -->
					</ul>
				</li>
				<li>
					<ul>
						<li>Food</li>
						<!-- <li><a href="<?php echo"$base_url/pages/food/search/"; ?>">All Food</a></li> -->
					</ul>
				</li>
				<li>
					<ul>
						<li>Beverage</li>
						<!-- <li><a href="<?php echo"$base_url/pages/beverage/search/"; ?>">All Beverage</a></li> -->
					</ul>
				</li>
				<li>
					<ul>
						<li>Recipe</li>
						<!-- <li><a href="<?php echo"$base_url/pages/recipe/search/"; ?>">All Recipe</a></li> -->
					</ul>
				</li>
				<!-- <li>
					<ul>
						<li>FGMART<li>
						<li><a href="<?php echo"$base_url/pages/fgmart/search/"; ?>">All FGMART</a></li>
					</ul>
				</li> -->
				<li>
					<ul>
						<li>Coupon</li>
						<!-- <li><a href="<?php echo"$base_url/pages/coupon/search/"; ?>">All Coupon</a></li> -->
					</ul>
				</li>
				<li>
					<ul>
						<li>Article</li>
						<!-- <li><a href="<?php echo"$base_url/pages/article/search/"; ?>">All Article</a></li> -->
					</ul>
				</li>
				<li>
					<ul>
						<li>Videos</li>
						<!-- <li><a href="<?php echo"$base_url/pages/video/search/"; ?>">All Videos</a></li> -->
					</ul>
				</li>
			</ul>
		</div>
		<div class="wrap pl60">
			<ul class="bot-menu">
				<li><a href="<?php echo"$base_url"; ?>/pages/about">About Us</a></li>
				<li><a href="<?php echo"$base_url"; ?>/pages/news">Foodie News</a></li>
				<li><a href="<?php echo"$base_url"; ?>/pages/advertise">Advertise</a></li>
				<li><a href="<?php echo"$base_url"; ?>/pages/contact">Contact Us</a></li>
				<li><a href="<?php echo"$base_url"; ?>/pages/certificates">Certificates</a></li>
				<li><a href="<?php echo"$base_url"; ?>/pages/references">References</a></li>
				<li><a href="<?php echo"$base_url"; ?>/pages/faq">FAQ</a></li>
				<!-- <li><a href="<?php echo"$base_url"; ?>/pages/sitemap">Sitemap</a></li> -->
				<li><a href="<?php echo"$base_url"; ?>/pages/term-of-services">Terms of Services</a></li>
				<li><a href="<?php echo"$base_url"; ?>/pages/privacy-policy">Privacy Policy</a></li>
				<li><a href="<?php echo"$base_url"; ?>/pages/intellectual-property-rights">Intellectual Property Rights</a></li>
				<li><a href="<?php echo"$base_url"; ?>/pages/disclaimer">Disclaimer</a></li>
			</ul>
			<em class="f-12">&copy; 2015 Foodie Guidances.com. All Right Reserved.</em>
		</div>
	</div>
</footer>
<div class="iklanan">

	<?php
		if (empty($type)) {
		}
		else{
			echo"
				<div class='tutup'>
					<img src='$base_url/assets/images/cancel.png' width='100%' />
				</div>
			";
			$ikl=mysqli_query($koneksi, "select * from iklan where type='$type'");
			while($ik=mysqli_fetch_array($ikl)){
				echo "
					<div class='mb20 rlt'>
						<img src='$base_url/assets/img/banner/$ik[mobile]'>
						<span class='tag-1'></span>
					</div>
				";
			}
		}
	?>
</div>
<!-- Start of StatCounter Code for Default Guide -->
<script type="text/javascript">
var sc_project=10609645;
var sc_invisible=1;
var sc_security="2ac52c59";
var scJsHost = (("https:" == document.location.protocol) ?
"https://secure." : "http://www.");
document.write("<sc"+"ript type='text/javascript' src='" +
scJsHost+
"statcounter.com/counter/counter.js'></"+"script>");
</script>
<noscript><div class="statcounter"><a title="web analytics"
href="http://statcounter.com/" target="_blank"><img
class="statcounter"
src="http://c.statcounter.com/10609645/0/2ac52c59/1/"
alt="web analytics"></a></div></noscript>
<!-- End of StatCounter Code for Default Guide -->

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-51794299-31', 'auto');
ga('send', 'pageview');

</script>
<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript">
		function googleTranslateElementInit() {
				new google.translate.TranslateElement({
						pageLanguage: 'en',
						layout:  google.translate.TranslateElement.InlineLayout.SIMPLE
				}, 'google_translate_element');
		}
</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('.transs').click(function() {
		$('#lightu').show();
		$('#fadeu').show();
			$('.goog-te-menu-frame').show();
			$('.goog-te-menu-frame').contents().find('body').css({
			'width':'250px',
			'maxWidth':'250px',
			 'overflow':'hidden',
			 'height':'275px'
			});
			if($('.goog-te-menu-frame').contents().find('.goog-te-menu2').length) {
				$('.goog-te-menu-frame').contents().find('.goog-te-menu2').css({
				'width':'250px',
				'maxWidth':'250px',
				'overflowX':'scroll',
				'box-sizing':'border-box',
				'height':'275px'
				});
			}
			else {
				setTimeout(changeGoogleStyles, 50);
			}

	});
	$('.transs-close').click(function() {
		$('#lightu').hide();
		$('#fadeu').hide();
		$('.goog-te-menu-frame').hide();
	});
});
</script>
<style media="screen">
	.goog-te-menu-frame {
	max-width:100% !important;
	overflow: scroll !important;
	height:290px !important;
	box-shadow: none !important;
	}
	.goog-te-menu2 {
		background-color: #ffffff;
		text-decoration: none;
		border: none !important;
		overflow: hidden;
		padding: 4px 0;
		box-shadow: none;
		width: 275px !important;

	}
	.goog-te-gadget-simple .goog-te-menu-value span {
		border: none !important;
		color: #000000 !important;
		font-size: 17px;
	}

</style>


<style media="screen">
.black_overlay{
  display: none;
  position: absolute;
  top: 0%;
  left: 0%;
  width: 100%;
  height: 100%;
  background-color: black;
  z-index:1001;
  -moz-opacity: 0.8;
  opacity:.80;
  filter: alpha(opacity=80);
}
.white_content {
  display: none;
  position: fixed;
  top: 20%;
  left: 40%;
  width: 20%;
  height: 56%;
  padding: 24px;
  border: 5px solid #C00606;
  background-color: #FFF;
  z-index: 1002;
  overflow-y: auto;
  overflow-x: hidden;
}
.goog-te-menu2 {
    background-color: #ffffff;
    text-decoration: none;
    border: 1px solid #6b90da;
    overflow: auto !important;
    padding: 4px;
    width: 237px !important;
}
</style>
<div id="lightu" class="white_content" style="width: 325px; height: 375px !important; overflow: hidden">
  <h3 style="font-size: 20px; margin: 0px 0px 15px;">Translate</h3>
  <div class="traaa">
		<div id="google_translate_element" style="width:200px"></div>
	</div>
  <a style="position: absolute; border: 6px solid rgb(255, 255, 255); right: -6px; top: -6px; z-index: 999999999; background: #C00606; color: rgb(255, 255, 255); padding: 6px 10px;" class="transs-close"><i class="fa fa-close"></i></a>
</div>
<div id="fadeu" class="black_overlay transs-close"></div>


<style media="screen">
.spn-direction-sign {
	position: fixed;
	display: inline-block;
	border: 0;
	width: 66px;
	height: 68px;
	top: 50%;
	margin-top: -34px;
	z-index: 121;
	color: white;
	font-size: 32px;
	text-align: center;
	line-height: 68px;
	background-color: #00a4e4;
	opacity: 0;
 -moz-opacity: 0;

 -webkit-transition: opacity 0.3s linear, -webkit-transform 0.3s linear;
 -moz-transition: opacity 0.3s linear, -mox-transform 0.3s linear;
	-ms-transition: opacity 0.3s linear, -ms-transform 0.3s linear;
	 -o-transition: opacity 0.3s linear, -o-transform 0.3s linear;
			transition: opacity 0.3s linear, transform 0.3s linear;
}

.spn-direction-sign.next {
			right: 0;
			-webkit-transform: translate(100%);
				 -moz-transform: translate(100%);
					-ms-transform: translate(100%);
					 -o-transform: translate(100%);
							transform: translate(100%);

	}
			.spn-direction-sign.next::after {
					content: "\25b6";
			}
	.spn-direction-sign.prev {
			left: 0;
			-webkit-transform: translate(-100%);
			-moz-transform: translate(-100%);
			 -ms-transform: translate(-100%);
				-o-transform: translate(-100%);
					 transform: translate(-100%);
	}
			.spn-direction-sign.prev::after {
					content: "\25c0";
			}
	.spn-direction-sign.visible {
			opacity: 1;
			-webkit-transform: translate(0);
				 -moz-transform: translate(0);
					-ms-transform: translate(0);
					 -o-transform: translate(0);
							transform: translate(0);

	}


.spn-freezing-overlay {
	position: fixed;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	z-index: 9999999999999999999999999999999999999;
	background-color: #000;
	 opacity: 0.5;
 -moz-opacity:0.5;
 -khtml-opacity: 0.5;
 -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=50)";
 filter: alpha(opacity=50);
}

	.spn-freezing-overlay::after {
			position: absolute;
			width: 100%;
			top: 50%;
			font-size: 32px;
			text-align: center;
			color: white;
			content: "Loading..."
	}
</style>
<script type="text/javascript">
	/*
	* @package SPN
	* @author sheiko
	* @version 0.1
	* @license MIT
	* @copyright (c) Dmitry Sheiko http://www.dsheiko.com
	* Code style: http://docs.jquery.com/JQuery_Core_Style_Guidelines
	*/

	(function( window ){

	var window = window,
		document = window.document,
		screen = window.screen,
		touchSwipeListener = function( options ) {
				// Private members
				var track = {
								startX: 0,
								endX: 0
						},
						defaultOptions = {
								moveHandler: function( direction ) {},
								endHandler: function( direction ) {},
								minLengthRatio: 0.3
						},
						getDirection = function() {
								return track.endX > track.startX ? "prev" : "next";
						},
						isDeliberateMove = function() {
								var minLength = Math.ceil( screen.width * options.minLengthRatio );
								return Math.abs(track.endX - track.startX) > minLength;
						},
						extendOptions = function() {
								for (var prop in defaultOptions) {
										if ( defaultOptions.hasOwnProperty( prop ) ) {
												options[ prop ] || ( options[ prop ] = defaultOptions[ prop ] );
										}
								}
						},
						handler = {
								touchStart: function( event ) {
										// At least one finger has touched the screen
										if ( event.touches.length > 0  ) {
												track.startX = event.touches[0].pageX;
										}
								},
								touchMove: function( event ) {
										if ( event.touches.length > 0  ) {
												track.endX = event.touches[0].pageX;
												options.moveHandler( getDirection(), isDeliberateMove() );
										}
								},
								touchEnd: function( event ) {
										var touches = event.changedTouches || event.touches;
										if ( touches.length > 0  ) {
												track.endX = touches[0].pageX;
												isDeliberateMove() && options.endHandler( getDirection() );
										}
								}
						};

				extendOptions();
				// Graceful degradation
				if ( !document.addEventListener ) {
						return {
								on: function() {},
								off: function() {}
						}
				}
				return {
						on: function() {
								document.addEventListener('touchstart', handler.touchStart, false);
								document.addEventListener('touchmove', handler.touchMove, false);
								document.addEventListener('touchend', handler.touchEnd, false);
						},
						off: function() {
								document.removeEventListener('touchstart', handler.touchStart);
								document.removeEventListener('touchmove', handler.touchMove);
								document.removeEventListener('touchend', handler.touchEnd);
						}
				}
		}
		// Expose global
		window.touchSwipeListener = touchSwipeListener;

	}( window ));



	(function( window ){
		var document = window.document,
				// Element helpers
				Util = {
						addClass: function( el, className ) {
								el.className += " " + className;
						},
						hasClass: function( el, className ) {
								var re = new RegExp("\s?" + className, "gi");
								return re.test( el.className );
						},
						removeClass: function( el, className ) {
								var re = new RegExp("\s?" + className, "gi");
								el.className = el.className.replace(re, "");
						}
				},
				swipePageNav = (function() {
						// Page sibling links like <link rel="prev" title=".." href=".." />
						// See also http://diveintohtml5.info/semantics.html
						var elLink = {
										prev: null,
										next: null
								},
								// Arrows, which slide in to indicate the shift direction
								elArrow = {
										prev: null,
										next: null
								},
								swipeListener;
						return {
								init: function() {
										this.retrievePageSiblings();
										// Swipe navigation makes sense only if any of sibling page link available
										if ( !elLink.prev && !elLink.next ) {
												return;
										}
										this.renderArows();
										this.syncUI();
								},
								syncUI: function() {
										var that = this;
										// Assign handlers for swipe "in progress" / "complete" events
										swipeListener = new window.touchSwipeListener({
											 moveHandler: function( direction, isDeliberateMove ) {
													 if ( isDeliberateMove ) {
															 if ( elArrow[ direction ] && elLink[ direction ] ) {
																	 Util.hasClass( elArrow[ direction ], "visible" ) ||
																			 Util.addClass( elArrow[ direction ], "visible" );
															 }
													 } else {
															 Util.removeClass( elArrow.next, "visible" );
															 Util.removeClass( elArrow.prev, "visible" );
													 }
											 },
											 endHandler: function( direction ) {
														that[ direction ] && that[ direction ]();
											 }
										});
										swipeListener.on();
								},
								retrievePageSiblings: function() {
										elLink.prev = document.querySelector( "head > link[rel=prev]");
										elLink.next = document.querySelector( "head > link[rel=next]");
								},
								renderArows: function() {
										var renderArrow = function( direction ) {
												var div = document.createElement("div");
												div.className = "spn-direction-sign " + direction;
												document.getElementsByTagName( "body" )[ 0 ].appendChild( div );
												return div;
										}
										elArrow.next = renderArrow( "next" );
										elArrow.prev = renderArrow( "prev" );
								},
								// When the shift (page swap) is requested, this overlay indicates that
								// the current page is frozen and a new one is loading
								showLoadingScreen: function() {
										var div = document.createElement("div");
										div.className = "spn-freezing-overlay";
										document.getElementsByTagName( "body" )[ 0 ].appendChild( div );
								},
								// Request the previous sibling page
								prev: function() {
										if ( elLink.prev ) {
												this.showLoadingScreen();
												window.location.href = elLink.prev.href;
										}
								},
								// Request the next sibling page
								next: function() {
										if ( elLink.next ) {
												this.showLoadingScreen();
												window.location.href = elLink.next.href;
										}
								}
						}
				}())

		// Apply when document is ready
		document.addEventListener( "DOMContentLoaded", function(){
				document.removeEventListener( "DOMContentLoaded", arguments.callee, false );
				try {
						swipePageNav.init();
				} catch (e) {
						alert(e);
				}
		}, false );


	}( window ));
</script>
<script type="text/javascript">
	function changeGoogleStyles() {
    if(($goog = $('.goog-te-menu-frame').contents().find('body')).length) {
        var stylesHtml = '<style>'+
        		
	            '.goog-te-menu2 {'+
	                'width:100% !important;'+
	                'max-width: 100% !important;'+
	                'margin-left: 0px;'+
	                'height:auto !important;'+
	            '}'+

        '</style>';
        $goog.prepend(stylesHtml);
    } else {
        setTimeout(changeGoogleStyles, 50);
    }
}
changeGoogleStyles();
</script>