<div class="wrap-pencarian mene-atas">
	<!-- <h1>FoodieGuidances.com</h1> -->
	<div class="box-pencarian">

		<div class="pull-left">
			<strong>Search</strong>
			<form method="get" action="<?php echo"$base_url/pages/all/search/"; ?>" id="ale-form" onsubmit="myFunction()">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Search based on Name, Category, Country, State, City..." id="search" name="keyword">
					<span class="input-group-btn">
						<button class="btn btn-danger" type="submit"><i class="fa fa-search"></i></button>
					</span>
				</div><!-- /input-group -->
			</form>
		</div>

		<div class="pull-right">
			<strong class="blk">Submit</strong>
			<div class="btn-group">
				<button type="button" class="btn btn-danger btn-block dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-check"></i> New Submit</button>
				<ul class="dropdown-menu" role="menu">
					<?php
					if(!empty($_SESSION['food_member'])){
						echo"<li><a href='$base_url/$u[username]/my-restaurant/new'>Restaurant</a></li>
						<li><a href='$base_url/$u[username]/my-food/new'>Food</a></li>
						<li><a href='$base_url/$u[username]/my-beverage/new'>Beverage</a></li>
						<li><a href='$base_url/$u[username]/my-process/new'>Process F&B</a></li>
						<li><a href='$base_url/$u[username]/my-events/new'>Events</a></li>
						<li><a href='$base_url/$u[username]/my-recipe/new'>Recipe</a></li>
						<li><a href='$base_url/$u[username]/my-article/new'>Article</a></li>
						<li><a href='$base_url/$u[username]/my-video/new'>Video</a></li>";
					}
					else{
						echo"<li><a href='$base_url/login-area'>Restaurant</a></li>
						<li><a href='$base_url/login-area'>Food</a></li>
						<li><a href='$base_url/login-area'>Beverage</a></li>
						<li><a href='$base_url/login-area'>Process F&B</a></li>
						<li><a href='$base_url/login-area'>Events</a></li>
						<li><a href='$base_url/login-area'>Recipe</a></li>
						<li><a href='$base_url/login-area'>Article</a></li>
						<li><a href='$base_url/login-area'>Video</a></li>";
					}
					?>
				</ul>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>