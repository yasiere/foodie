<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";
include "config/func/jumlah_data.php";
include "config/func/id_masking.php";
$auto_logout=600;
if(!empty($_SESSION['food_member'])){
	if (time()-$_SESSION['timestamp']>$auto_logout){
		session_destroy();
		session_unset();
		header("Location: ".$base_url."/auto-logout");
		exit();
	}else{
		$_SESSION['timestamp']=time();
	}
	include "config/func/member_data.php";
}
$active = "recipe";
$type = "recipe";
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
    <title>Recipe - Foodie Guidances</title>
	<meta name="keywords" content="">
    <meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
		<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
		<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
		<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="msapplication-TileImage" content="mstile-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
		<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
		<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
		<link href="<?php echo"$base_url"; ?>/assets/css/jquery-ui.css" rel="stylesheet">
		<link href="<?php echo"$base_url"; ?>/assets/css/superslides.css" rel="stylesheet">
		<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
		<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
		<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
		<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>

		<link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">

		<link rel="prev" title="Page 2" href="<?php echo "$base_url/pages/beverage" ?>" />
		<link rel="next" title="Page 1" href="<?php echo  "$base_url/pages/fgmart" ?>" />

   <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body style="position: initial;" class="drawer drawer--right">
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>
	<div class="pencarian">
		<div class="pencari">
			<form method="get" action="<?php echo"$base_url/pages/recipe/search/"; ?>" id="ale-form" onsubmit="myFunction()">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Search based on Name, Category, Country, State, City..." id="search" name="keyword">
					<span class="input-group-btn">
						<button class="btn btn-danger" type="submit"><i class="fa fa-search"></i></button>
					</span>
				</div><!-- /input-group -->
			</form>
		</div>
		<div id="slides">
			<div class="slides-container">
				<?php
				$sql=mysqli_query($koneksi,"SELECT * FROM slideshow ORDER BY id_slideshow ASC");
				while($w=mysqli_fetch_array($sql)){
					echo"<img src='$base_url/assets/img/slideshow/$w[gambar]' alt='foodie guidances'>";
				}
				?>
			</div>
		</div>
		<?php include "config/inc/pencarian.php"; ?>
	</div>
    <div class="container">
		<div class="row">
			<div class="col-8 col-8a no-padding">
				<div class="col-8 col-8a" style="width:50%">
					<h4 class="f-merah no-mb mt20 ">
						<a href="<?php echo"$base_url/pages/recipe/search/?sort=r.id_recipe+DESC"; ?>">Latest Recipe</a>
					</h4>
					<span class="f-abu f-12 blk mb20 mb-j20">Worldwide data collection</span>
				</div>
				<div class="col-8 col-8a"  style="width:50%">
					<h4 class="f-merah no-mb mt20">
						<a href="<?php echo"$base_url/pages/recipe/search/?keyword="; ?>"><i class="fa fa-search" style="margin-right:5px"></i> Advance Search</a>
					</h4>
					 <span class="f-abu f-12">All Beverage</span>
				</div>
				<div style="clear: both;"></div>

				<div class="media">
					<?php
					$sql=mysqli_query($koneksi,"select *,(select p.gambar_recipe_photo from recipe_photo p where p.id_recipe=f.id_recipe order by p.id_recipe_photo desc limit 1) as gambar,(SELECT COUNT(h.id_member) FROM recipe_like h WHERE h.id_recipe=f.id_recipe) AS dilike, (select count(h.id_recipe) from recipe_here h where h.id_recipe=f.id_recipe) as jumlah from recipe f,recipe_category k, cuisine c, difficulty d, duration u where f.id_recipe_category=k.id_recipe_category and f.id_cuisine=c.id_cuisine and f.id_difficulty=d.id_difficulty and f.id_duration=u.id_duration order by f.id_recipe desc limit 2");
					$v=mysqli_fetch_array($sql);
					$slug=seo($v['nama_recipe']);
					$post=date("jS M, Y", strtotime($v['tgl_post']));
					$id = id_masking($v['id_recipe']);
					if($v['pork_serving']=="Halal"){
						$ikon="icon_halal.png";
					}
					elseif($v['pork_serving']=="Yes"){
						$ikon="icon_pork.png";
					}
					elseif($v['pork_serving']=="No"){
						$ikon="icon_nonhalal.png";
					}
					else{
						$ikon="icon_vegetarian.png";
					}
					echo"<div class='row col-4a'>
						<div class='col-8 thumb col-8a no-padding'>
							<a href='$base_url/pages/recipe/info/$id/$slug'><img data-original='$base_url/assets/img/recipe/big_$v[gambar]' class='lazy' width='220' height='165'></a>
							<a href='$base_url/pages/recipe'><span>Recipe</span></a>
						</div>
						<div class='col-8 info col-8a'>
							<em>posted on $post</em>
							<h3 class='sembunyi'><a href='$base_url/pages/recipe/info/$id/$slug'>$v[nama_recipe]</a></h3>
							<ul class='list-unstyled f-12'>
								<li><em>category</em> <a href='$base_url/pages/recipe/search/?category=$v[id_recipe_category]'>$v[nama_recipe_category]</a></li>
								<li><em>cuisine</em> <a href='$base_url/pages/recipe/search/?cuisine=$v[id_cuisine]'>$v[nama_cuisine]</a></li>
								<li><em>dish</em> <a href='$base_url/pages/recipe/search/?dish=$v[dish]'>$v[dish]</a></li>
								<li><i class='fa fa-signal f-16'></i> $v[nama_difficulty] <span class='bullet'>&#8226;</span> <i class='fa fa-clock-o f-16'></i> $v[nama_duration]</li>
								<li>$v[dilike] Like <span class='bullet'>&#8226;</span> $v[jumlah] Trying</li>
							</ul>
							<img src='$base_url/assets/img/theme/$ikon' width='30' class='db mt5' style='
    border: 1px solid;
    border-radius: 20px;
'>
						</div>
					</div>";
					?>
				</div>

				<h4 class="f-merah no-mb ml10-j"><a href="<?php echo"$base_url/pages/recipe/search/?sort=r.dilihat+DESC"; ?>">Popular Recipe</a></h4>
				<span class="f-abu f-12 blk mb20 ml10-j mb-j2">Worldwide data collection</span>
				<div class="media">
					<?php
					$v=null;
					$sql=mysqli_query($koneksi,"select *,(select p.gambar_recipe_photo from recipe_photo p where p.id_recipe=f.id_recipe order by p.id_recipe_photo desc limit 1) as gambar,(SELECT COUNT(h.id_member) FROM recipe_like h WHERE h.id_recipe=f.id_recipe) AS dilike, (select count(h.id_recipe) from recipe_here h where h.id_recipe=f.id_recipe) as jumlah from recipe f,recipe_category k, cuisine c, difficulty d, duration u where f.id_recipe_category=k.id_recipe_category and f.id_cuisine=c.id_cuisine and f.id_difficulty=d.id_difficulty and f.id_duration=u.id_duration order by f.dilihat desc limit 2");
					$v=mysqli_fetch_array($sql);
					$slug=seo($v['nama_recipe']);
					$post=date("jS M, Y", strtotime($v['tgl_post']));
					$id = id_masking($v['id_recipe']);
					if($v['pork_serving']=="Halal"){
						$ikon="icon_halal.png";
					}
					elseif($v['pork_serving']=="Yes"){
						$ikon="icon_pork.png";
					}
					elseif($v['pork_serving']=="No"){
						$ikon="icon_nonhalal.png";
					}
					else{
						$ikon="icon_vegetarian.png";
					}
					echo"<div class='row col-4a'>
						<div class='col-8 thumb col-8a no-padding'>
							<a href='$base_url/pages/recipe/info/$id/$slug'><img data-original='$base_url/assets/img/recipe/big_$v[gambar]' class='lazy' width='220' height='165'></a>
							<a href='$base_url/pages/recipe'><span>Recipe</span></a>
						</div>
						<div class='col-8 info col-8a'>
							<em>posted on $post</em>
							<h3 class='sembunyi'><a href='$base_url/pages/recipe/info/$id/$slug'>$v[nama_recipe]</a></h3>
							<ul class='list-unstyled f-12'>
								<li><em>category</em> <a href='$base_url/pages/recipe/search/?category=$v[id_recipe_category]'>$v[nama_recipe_category]</a></li>
								<li><em>cuisine</em> <a href='$base_url/pages/recipe/search/?cuisine=$v[id_cuisine]'>$v[nama_cuisine]</a></li>
								<li><em>dish</em> <a href='$base_url/pages/recipe/search/?dish=$v[dish]'>$v[dish]</a></li>
								<li><i class='fa fa-signal f-16'></i> $v[nama_difficulty] <span class='bullet'>&#8226;</span> <i class='fa fa-clock-o f-16'></i> $v[nama_duration]</li>
								<li>$v[dilike] Like <span class='bullet'>&#8226;</span> $v[jumlah] Trying</li>
							</ul>
							<img src='$base_url/assets/img/theme/$ikon' width='30' class='db mt5' style='
    border: 1px solid;
    border-radius: 20px;
'>
						</div>
					</div>";
					?>
				</div>

				<h4 class="f-merah no-mb ml10-j"><a href="<?php echo"$base_url/pages/recipe/search/?sort=direkomendasi+DESC"; ?>">Recommended Recipe</a></h4>
				<span class="f-abu f-12 blk mb20 ml10-j mb-j2">Worldwide data collection</span>
				<div class="media">
					<?php
					$v=null;
					$sql=mysqli_query($koneksi,"select *,(select count(h.id_recipe) from recipe_rekomendasi h where h.id_recipe=f.id_recipe) as direkomendasi,(select p.gambar_recipe_photo from recipe_photo p where p.id_recipe=f.id_recipe order by p.id_recipe_photo desc limit 1) as gambar,(SELECT COUNT(h.id_member) FROM recipe_like h WHERE h.id_recipe=f.id_recipe) AS dilike, (select count(h.id_recipe) from recipe_here h where h.id_recipe=f.id_recipe) as jumlah from recipe f,recipe_category k, cuisine c, difficulty d, duration u where f.id_recipe_category=k.id_recipe_category and f.id_cuisine=c.id_cuisine and f.id_difficulty=d.id_difficulty and f.id_duration=u.id_duration order by direkomendasi desc limit 2");
					$v=mysqli_fetch_array($sql);
					$slug=seo($v['nama_recipe']);
					$post=date("jS M, Y", strtotime($v['tgl_post']));
					$id = id_masking($v['id_recipe']);
					if($v['pork_serving']=="Halal"){
						$ikon="icon_halal.png";
					}
					elseif($v['pork_serving']=="Yes"){
						$ikon="icon_pork.png";
					}
					elseif($v['pork_serving']=="No"){
						$ikon="icon_nonhalal.png";
					}
					else{
						$ikon="icon_vegetarian.png";
					}
					echo"<div class='row col-4a'>
						<div class='col-8 thumb col-8a no-padding'>
							<a href='$base_url/pages/recipe/info/$id/$slug'><img data-original='$base_url/assets/img/recipe/big_$v[gambar]' class='lazy' width='220' height='165'></a>
							<a href='$base_url/pages/recipe'><span>Recipe</span></a>
						</div>
						<div class='col-8 info col-8a'>
							<em>posted on $post</em>
							<h3 class='sembunyi'><a href='$base_url/pages/recipe/info/$id/$slug'>$v[nama_recipe]</a></h3>
							<ul class='list-unstyled f-12'>
								<li><em>category</em> <a href='$base_url/pages/recipe/search/?category=$v[id_recipe_category]'>$v[nama_recipe_category]</a></li>
								<li><em>cuisine</em> <a href='$base_url/pages/recipe/search/?cuisine=$v[id_cuisine]'>$v[nama_cuisine]</a></li>
								<li><em>dish</em> <a href='$base_url/pages/recipe/search/?dish=$v[dish]'>$v[dish]</a></li>
								<li><i class='fa fa-signal f-16'></i> $v[nama_difficulty] <span class='bullet'>&#8226;</span> <i class='fa fa-clock-o f-16'></i> $v[nama_duration]</li>
								<li>$v[dilike] Like <span class='bullet'>&#8226;</span> $v[jumlah] Trying</li>
							</ul>
							<img src='$base_url/assets/img/theme/$ikon' width='30'  style='
    border: 1px solid;
    border-radius: 20px;
' class='db mt5'>
						</div>
					</div>";
					?>
				</div>
			</div>
			<div class="menu-bawah">
				<a href="<?php echo"$base_url/pages/recipe/search"; ?>">World Top Recipe</a>
			</div>
			<div class="col-4 mene-atas">
				<h4 class="f-merah no-mb mt20"><a href="<?php echo"$base_url/pages/recipe/search"; ?>">World Top Recipe</a></h4>
				<span class="f-abu f-12 blk mb10">Worldwide data collection</span>
				<ol class="list-number">
					<?php
					$sql=mysqli_query($koneksi,"select *, (select count(h.id_recipe) from recipe_like h where h.id_recipe=f.id_recipe) as jumlah from recipe f,recipe_category k where f.id_recipe_category=k.id_recipe_category order by jumlah desc limit 21");
					while($b=mysqli_fetch_array($sql)){
						$slug=seo($b['nama_recipe']);
						echo"<li><h4><a href='$base_url/pages/recipe/info/$b[id_recipe]/$slug'>$b[nama_recipe]</a></h4><a href='$base_url/pages/recipe/search/?category=$b[id_recipe_category]' class='f-12 f-merah'>$b[nama_recipe_category]</a></li>";
					}
					?>
				</ol>
				<em class="blk mb10 f-12">The rankings are obtain from our members most liked recipe</em>
				<h4 class="f-merah no-mb mt30">Foodie's Video</h4>
				<span class="f-abu f-12 blk mb10 ">Popular Foodie's Video</span>
				<?php
				$sql=mysqli_query($koneksi,"select * from video order by RAND() desc limit 1");
				while($v=mysqli_fetch_array($sql)){
					$url = $v['url'];
					preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
					$id = $matches[1];
					echo "<iframe class='mb20' type='text/html' width='220' height='123' src='https://www.youtube.com/embed/$id?rel=0&showinfo=0&color=white&iv_load_policy=3' frameborder='0' allowfullscreen></iframe>";
				}
				?>
			</div>
			<div class="col-4 mene-atas">
				<?php include"config/inc/search.php"; ?>
				<?php include"config/inc/iklan.php"; ?>
				<div class="fb-like-box" data-href="https://www.facebook.com/foodieguidances#_=_" data-width="220" data-height="350" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
			</div>
		</div>
    </div>
    <?php include"config/inc/footer.php"; ?>
	 <a class="scrollToNext" href="<?php echo "$base_url/pages/beverage" ?>" style="bottom: 320px;"><img src="<?php echo"$base_url/assets/img/theme/kiri.png" ?>"></a>
	 <a class="scrollToNext" href="<?php echo  "$base_url/pages/recipe" ?>" style="bottom: 273px;"><img src="<?php echo"$base_url/assets/img/theme/kanan.png" ?>"></a>
	 <a href="#" class="scrollToTop"><img src="<?php echo"$base_url/assets/img/theme/atas.png" ?>"></a>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery-ui.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.superslides.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.lazyload.min.js"></script>

	<script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
   <script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
   <script>
      $(document).ready(function() {
	      $('.drawer').drawer();

			$('.tutup').click(function(){
				$('.iklanan').toggleClass('hilang', 500);
			});

			$('.cart').click(function(e){
				e.stopPropagation();
				$('.dis-cart').toggle();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$('.plus').click(function(e){
				e.stopPropagation();
				$('.dis-plus').toggle();
				$('.dis-cart').hide();
				$('.dis-share').hide();
			});
			$('.share').click(function(e){
				e.stopPropagation();
				$('.dis-share').toggle();
				$('.dis-plus').hide();
				$('.dis-cart').hide();
			});
			$('.drawer-toggle').click(function(){
				$('.dis-cart').hide();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$(document).click(function () {
				 var $el = $(".dis-share");
				 if ($el.is(":visible")) {
					  $el.fadeOut(200);
				 }
				 var $ela = $(".dis-plus");
				 if ($ela.is(":visible")) {
					  $ela.fadeOut(200);
				 }
				 var $elu = $(".dis-cart");
				 if ($elu.is(":visible")) {
					  $elu.fadeOut(200);
				 }
		   });
      });
   </script>

	<?php if(!empty($_SESSION['food_member'])){ ?>
		<script src="<?php echo"$base_url"; ?>/assets/js/theme.js"></script>
	<?php } ?>
	<script type="text/javascript">
		$(document).ready(function () {
			$(document).on("contextmenu",function(e){
				if(e.target.nodeName != "INPUT" && e.target.nodeName != "TEXTAREA")
				e.preventDefault();
			});
			$.fn.disableTextSelect = function() {
				return this.each(function() {
					$(this).css({
						'MozUserSelect':'none',
						'webkitUserSelect':'none'
					}).attr('unselectable','on').bind('selectstart', function() {
						return false;
					});
				});
			};
			$('body').disableTextSelect();
		});
	</script>

	<script type="text/javascript">
	$(document).ready(function() {
	$(window).scroll(function(){
		 if ($(this).scrollTop() > 100) {
			 $('.scrollToTop').fadeIn();
		 } else {
			 $('.scrollToTop').fadeOut();
		 }
	});

	$(window).scroll(function(){
		if ($(this).scrollTop() > 50) {
			$('.scrollToNext').fadeIn();
		} else {
			$('.scrollToNext').fadeOut();
		}
	});

	  $('#go-to-vote').click(function() {
		  $('html, body').animate({
			  scrollTop: $( $(this).attr('href') ).offset().top
		  }, 500);
		  $('#tab-vote a[href="#feature"]').tab('show');
		  return false;
	  });
	});
	</script>
	<div id="fb-root"></div>
	<script>
	$(document).ready(function () {
		$("img.lazy").lazyload({
			effect : "fadeIn"
		});
		$('#slides').superslides({
			animation: 'slide',
			inherit_height_from: '.pencarian',
			play: 8000,
			pagination: true
		});
		$('.carousel').carousel({
			interval: 10000
		});
		$('#search').autocomplete({
			source: "<?php echo "$base_url/config/func/ajax_recipe_search.php"; ?>",
			minLength: 3
		});
	});
	/*=====For developer to config ====*/
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=348272391978609&version=v2.0";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script>
</body>
</html>
