<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";
include "config/func/id_masking.php";
date_default_timezone_set("Asia/Jakarta");
$sekarang=date("Y-m-d");
$id = id_masking($_GET['id']);
$r=mysqli_fetch_array(mysqli_query($koneksi,"select id_events, nama_events from events where id_events='$id'"));
$seo=seo($r['nama_events']);

if(isset($_SESSION['food_member'])){
	if($_GET['fungsi']=="1"){
		$ada=mysqli_num_rows(mysqli_query($koneksi,"select id_events_like from events_like where id_events='$id' and id_member='$_SESSION[food_member]' and tgl_like='$sekarang'"));
		if($ada==0){
			mysqli_query($koneksi,"insert into events_like (id_member,id_events,tgl_like) values('$_SESSION[food_member]','$id','$sekarang')");
			$_SESSION['resto_notif']     = "suka";
		}
		else{
			$_SESSION['resto_notif']     = "suka_gagal";
		}
	}
	elseif($_GET['fungsi']=="2"){
		$ada=mysqli_num_rows(mysqli_query($koneksi,"select id_events_rekomendasi from events_rekomendasi where id_events='$id' and id_member='$_SESSION[food_member]' and tgl_rekomendasi='$sekarang'"));
		if($ada==0){
			mysqli_query($koneksi,"insert into events_rekomendasi (id_member,id_events,tgl_rekomendasi) values('$_SESSION[food_member]','$id','$sekarang')");
			$_SESSION['resto_notif']     = "rekomendasi";
		}
		else{
			$_SESSION['resto_notif']     = "rekomendasi_gagal";
		}
	}
	elseif($_GET['fungsi']=="3"){
		$ada_bookmark=mysqli_num_rows(mysqli_query($koneksi,"select id_bookmark from bookmark where id='$id' and jenis='events' and id_member='$_SESSION[food_member]'"));
		if($ada_bookmark==0){
			mysqli_query($koneksi,"insert into bookmark (id_member,jenis,id,tgl_bookmark) values('$_SESSION[food_member]','events','$id','$sekarang')");
			$_SESSION['resto_notif']     = "bookmark";
		}
		else{
			$_SESSION['resto_notif']     = "bookmark_gagal";
		}
	}
	elseif($_GET['fungsi']=="4"){
		$ada_here=mysqli_num_rows(mysqli_query($koneksi,"select id_events_here from events_here where id_events='$id' and id_member='$_SESSION[food_member]'"));
		if($ada_here==0){
			mysqli_query($koneksi,"insert into events_here (id_member,id_events,tgl_here) values('$_SESSION[food_member]','$id','$sekarang')");
			$_SESSION['resto_notif']     = "disini";
		}
		else{
			$_SESSION['resto_notif']     = "disini_gagal";
		}
	}
	elseif($_GET['fungsi']=="5"){
		$ada_report=mysqli_num_rows(mysqli_query($koneksi,"select id_events_report from events_report where id_events='$id' and id_member='$_SESSION[food_member]'"));
		if($ada_report==0){
			mysqli_query($koneksi,"insert into events_report (id_member,id_events,tgl_report) values('$_SESSION[food_member]','$id','$sekarang')");
			$_SESSION['resto_notif']     = "lapor";
		}
		else{
			$_SESSION['resto_notif']     = "lapor_gagal";
		}
	}
	elseif($_GET['fungsi']=="6"){
		$ada_report=mysqli_num_rows(mysqli_query($koneksi,"select id_rating from events_rating where id_events='$id' and id_member='$_SESSION[food_member]' and tgl_rating='$sekarang'"));
		if($ada_report==0){

			if(!empty($_GET['rating'])){
				$r1=$_GET['rating'] * 0.30;
				$r2=$_GET['rating'] * 0.27;
				$r3=$_GET['rating'] * 0.23;
				$r4=$_GET['rating'] * 0.20;
			}
			else{
				$r1=$_GET['security'] * 0.30;
				$r2=$_GET['clean']      * 0.27;
				$r3=$_GET['comfort']   * 0.23;
				$r4=$_GET['organise']     * 0.20;
			}
			mysqli_query($koneksi,"INSERT INTO `events_rating`(`id_events`, `id_member`,`security`, `clean`, `comfort`, `organise`, `tgl_rating`) 
									VALUES ('$id','$_SESSION[food_member]','$r1','$r2','$r3','$r4','$sekarang')");

			$_SESSION['resto_notif'] = "rating";

		}
		else{
			$_SESSION['resto_notif']     = "rating_gagal";
		}
	}
	if($_GET['fungsi']=="7"){
		$ada=mysqli_num_rows(mysqli_query($koneksi,"select id_events_photo_like from events_photo_like where id_events_photo_like='$_GET[idp]' and id_member='$_SESSION[food_member]' and tgl_photo_like='$sekarang'"));
		if($ada==0){
			mysqli_query($koneksi,"insert into events_photo_like (id_member,id_events_photo,tgl_photo_like) values('$_SESSION[food_member]','$_GET[idp]','$sekarang')");
			$_SESSION['resto_notif']     = "suka_photo";
			// pages/events/info/524288/test-img
			header("Location: ".$base_url."/pages/events/info/".$_GET['id']."/".$seo);
		}
		else{
			$_SESSION['resto_notif']     = "suka_photo_gagal";
		}
		header("Location: ".$base_url."/pages/events/info/".$_GET['id']."/".$seo);
		
		exit;
	}
}
else{
	$_SESSION['resto_notif']     = "login_dulu";
}
// header("Location: ".$base_url."/pages/events/".$_GET['page']."/".$_GET['id']."/".$seo);
?>
