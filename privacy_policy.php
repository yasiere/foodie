<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";	
include "config/func/jumlah_data.php";	
$auto_logout=600;
if(!empty($_SESSION['food_member'])){
	if (time()-$_SESSION['timestamp']>$auto_logout){
		session_destroy();
		session_unset();
		header("Location: ".$base_url."/auto-logout");
		exit();
	}else{
		$_SESSION['timestamp']=time();
	}			
	include "config/func/member_data.php";
}
$active = "privacy";
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
    <title>Privacy Policy - Foodie Guidances</title>
	<meta name="keywords" content="">
    <meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/superslides.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>
  <link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
 <body style="position: initial;" class="drawer drawer--right">
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>
	<div class="pencarian">
		<div id="slides">
			<div class="slides-container">
				<img src="<?php echo"$base_url"; ?>/assets/img/theme/about.jpg" alt="foodie guidances">
			</div>
		</div>
	</div>
    <div class="container">
		<h1 class="f-merah">Privacy Policy</h1>
		<p>When signing up for a membership at foodieguidances.com, you will be asked to provide certain private and confidential information to us such as Name, Username, Password, Gender, and Email Address - these information must be filled up.  The other optional information required are: Personal Profile Photo, Nationality and Age.</p>
		<p>All information, such as personal information, and photos provided by our members belong to the Foodie Guidances Company. If a member decided to cancel its membership, all of the submitted information data will be retained as the data are belong to the Foodie Guidances Company, as stated in <u>Intellectual Property Rights above</u>. Personal information including Name, Username, Password, Gender, and Email Address will be deleted, with the exception of the name posted onprevious reviews.</p> 
		<p>If and when applicable, Foodie Guidances Company’s staff, including authorized website owners, editors, programmers, designers, customer service, and outsource website design firms will be able to access necessary information data without location limitation. The personal data will be used within our entire website to enhance the user experience for our members and visitors on foodieguidances.com.</p>
		<p>As for the FGMart page on the website, we use the reliable and trustworthy payment gateway to handle our online transactions. Among them are PayPal, Google Wallets and Apple Pay. All the transactions will be performed via respective payment gateways, which usually will launch a new tab or browser.</p>  
		<p>Members are advised to check on their internet security feature on the domain and make sure it does provide a verified certificate by a trusted and authorized Certificate Authority. All domain who has a verified certificate will have a green color “Lock”  or a green bar with the “Lock” in it. Since it is up most important to protect the Privacy of our members, foodieguidances.com had acquired the Encrypted and Authorization Certificate from DigiCert, Inc., U.S.A. under SSL Plus Certificate.</p> 
		<p><i>We will only require the necessary information from you for our website. For your information, all the data and contents belongs to foodieguidances.com.  In addition, we do provide a secure web browsing experience by having a verified SSL Plus Certificate from Digicert, Inc., U.S.A.</i></p>
		<h1 class="f-merah">Cookies Policy</h1>
		<p>Cookies are small text files which are placed on your personal computer by the websites you visit. The websites which placed the cookies on your computer can read the small text files. That way, when you return, the website will recognize you. Cookies do not count as an invasion of privacy under the relevant laws.</p> 
		<p>Foodieguidances.com uses cookies, so we can identify you when you visit our website, and your settings and preferences will be saved. You can block cookies anytime you want, by disabling them from the Internet options menu in your browser.</p>
		<p><i>Our website uses cookies but you can disable it in your browser setting.</i></p>
		<h1 class="f-merah">Contact</h1> 
		<p>If you have any questions about Privacy Policy and Cookies Policy, feel free to contact us at: cs@foodieguidances.com.</p>
	</div>
    <?php include"config/inc/footer.php"; ?>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.superslides.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/theme.js"></script>
	<div id="fb-root"></div>
	<script>
	$(document).ready(function () {
		$('#slides').superslides({
			animation: 'fade',
			inherit_height_from: '.pencarian',
			play: 8000
		});
	});
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=348272391978609&version=v2.0";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
  <script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
	<script>
		 $(document).ready(function() {
			 $('.drawer').drawer();

		 $('.tutup').click(function(){
			 $('.iklanan').toggleClass('hilang', 500);
		 });

		 $('.cart').click(function(e){
			 e.stopPropagation();
			 $('.dis-cart').toggle();
			 $('.dis-plus').hide();
			 $('.dis-share').hide();
		 });
		 $('.plus').click(function(e){
			 e.stopPropagation();
			 $('.dis-plus').toggle();
			 $('.dis-cart').hide();
			 $('.dis-share').hide();
		 });
		 $('.share').click(function(e){
			 e.stopPropagation();
			 $('.dis-share').toggle();
			 $('.dis-plus').hide();
			 $('.dis-cart').hide();
		 });
		 $('.drawer-toggle').click(function(){
			 $('.dis-cart').hide();
			 $('.dis-plus').hide();
			 $('.dis-share').hide();
		 });
		 $('.btn-caris').click(function(){
			 $('.search_kiri').toggleClass('search_kiri_tam', 500);
		 });
		 $(document).click(function () {
				var $el = $(".dis-share");
				if ($el.is(":visible")) {
					 $el.fadeOut(200);
				}
				var $ela = $(".dis-plus");
				if ($ela.is(":visible")) {
					 $ela.fadeOut(200);
				}
				var $elu = $(".dis-cart");
				if ($elu.is(":visible")) {
					 $elu.fadeOut(200);
				}
			});

		 });
	</script>
</body>
</html>