<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";
include "config/func/jumlah_data.php";
include "config/func/id_masking.php";
$auto_logout=1800000;
if(!empty($_SESSION['food_member'])){
	if (time()-$_SESSION['timestamp']>$auto_logout){
		session_destroy();
		session_unset();
		header("Location: ".$base_url."/auto-logout");
		exit();
	}else{
		$_SESSION['timestamp']=time();
	}
	include "config/func/member_data.php";
}
$id = id_masking($_GET['id']);
$active = "restaurant";
$type = "restaurant_beverage";
$resto=mysqli_query($koneksi,"SELECT *,r.id_negara as kode_negara, d.id_kota,(SELECT COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS jumlah_vote,(SELECT COUNT(f.id_member) FROM restaurant_like f WHERE f.id_restaurant=r.id_restaurant) AS jumlah_like,(SELECT COUNT(f.id_member) FROM restaurant_rekomendasi f WHERE f.id_restaurant=r.id_restaurant) AS jumlah_rekomendasi,(SELECT SUM(f.cleanlines)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_cleanlines,(SELECT SUM(f.customer_services)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_customer,(SELECT SUM(f.food_beverage)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_food,(SELECT SUM(f.comfort)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_comfort,(SELECT SUM(f.value_money)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_money,(SELECT (SUM(f.cleanlines) + SUM(f.customer_services) + SUM(f.food_beverage) + SUM(f.comfort) + SUM(f.value_money)) / COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant =r.id_restaurant) AS total_rate FROM restaurant r LEFT JOIN type_of_business a ON r.id_type_of_business = a.id_type_of_business LEFT JOIN negara b ON r.id_negara = b.id_negara LEFT JOIN propinsi c ON r.id_propinsi = c.id_propinsi LEFT JOIN kota d ON r.id_kota = d.id_kota LEFT JOIN landmark e ON r.id_landmark = e.id_landmark LEFT JOIN mall f ON r.id_mall = f.id_mall LEFT JOIN operation_hour g ON r.id_operation_hour = g.id_operation_hour LEFT JOIN price_index i ON r.id_price_index = i.id_price_index LEFT JOIN suitable_for j ON r.id_suitable_for = j.id_suitable_for LEFT JOIN serving_time m ON r.id_serving_time = m.id_serving_time LEFT JOIN type_of_service n ON r.id_type_of_service = n.id_type_of_service LEFT JOIN wifi p ON r.id_wifi = p.id_wifi LEFT JOIN term_of_payment q ON r.id_term_of_payment = q.id_term_of_payment LEFT JOIN premise_security s ON r.id_premise_security = s.id_premise_security LEFT JOIN premise_fire_safety t ON r.id_premise_fire_safety = t.id_premise_fire_safety LEFT JOIN premise_maintenance u ON r.id_premise_maintenance = u.id_premise_maintenance LEFT JOIN parking_spaces v ON r.id_parking_spaces = v.id_parking_spaces LEFT JOIN ambience w ON r.id_ambience = w.id_ambience LEFT JOIN attire x ON r.id_attire = x.id_attire LEFT JOIN clean_washroom y ON r.id_clean_washroom = y.id_clean_washroom LEFT JOIN tables_availability z ON r.id_tables_availability = z.id_tables_availability LEFT JOIN noise_level ON r.id_noise_level = noise_level.id_noise_level LEFT JOIN waiter_tipping ON r.id_waiter_tipping = waiter_tipping.id_waiter_tipping LEFT JOIN member ON r.id_member = member.id_member LEFT JOIN air_conditioning ON r.id_air_conditioning = air_conditioning.id_air_conditioning LEFT JOIN heating_system ON r.id_heating_system = heating_system.id_heating_system LEFT JOIN premise_hygiene ON r.id_premise_hygiene = premise_hygiene.id_premise_hygiene where r.id_restaurant='$id'");
$r=mysqli_fetch_array($resto);
$slug=seo($r['restaurant_name']);
$post=date("jS M, Y", strtotime($r['tgl_post']));
$total_cleanlines=number_format((float)$r['t_cleanlines'], 2, '.', '');
$img_cleanlines=$total_cleanlines / 0.26;
$total_customer=number_format((float)$r['t_customer'], 2, '.', '');
$img_customer=$total_customer / 0.24;
$total_food=number_format((float)$r['t_food'], 2, '.', '');
$img_food=$total_food / 0.23;
$total_comfort=number_format((float)$r['t_comfort'], 2, '.', '');
$img_comfort=$total_comfort / 0.14;
$total_money=number_format((float)$r['t_money'], 2, '.', '');
$img_money=$total_money / 0.13;
$total_rating=number_format((float)$r['total_rate'], 2, '.', '');
$gsql=mysqli_query($koneksi,"select p.gambar_restaurant_photo, (select count(l.id_restaurant_photo_like) from restaurant_photo_like l where l.id_restaurant_photo=p.id_restaurant_photo) as total_like from restaurant_photo p where p.id_restaurant='$id' order by total_like desc,p.id_restaurant_photo limit 1");
$g=mysqli_fetch_array($gsql);
$gambar=$g['gambar_restaurant_photo'];
if(mysqli_num_rows($gsql)==0){
	$gambar="foodieguidances.jpg";
}
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
    <title><?php echo"$r[restaurant_name]"; ?> - Foodie Guidances</title>
	<meta name="keywords" content="">
    <meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<meta property="og:title" content="<?php echo"$r[restaurant_name]"; ?>" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="<?php echo"$base_url/pages/restaurant/info/$_GET[id]/$slug"; ?>" />
	<meta property="og:image" content="<?php echo"$base_url/assets/img/restaurant/medium_$gambar"; ?>" />
	<meta property="og:description" content="<?php echo"$r[restaurant_name]"; ?>" />
	<meta property="og:site_name" content="Foodie Guidances" />
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/slick.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/lightbox.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>
	<link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
 <body style="position: initial;" class="drawer drawer--right">
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>
    <div class="container hook">
		<div class="row">
			<div class="col-12 col-8a">
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="<?php echo"$base_url/pages/restaurant/search/"; ?>">Restaurant</a></li>
					<li><a href="<?php echo"$base_url/pages/restaurant/search/?country=$r[id_negara]"; ?>"><?php echo"$r[nama_negara]"; ?></a></li>
					<li><a href="<?php echo"$base_url/pages/restaurant/search/?state=$r[id_propinsi]"; ?>"><?php echo"$r[nama_propinsi]"; ?></a></li>
					<li><a href="<?php echo"$base_url/pages/restaurant/search/?city=$r[id_kota]"; ?>"><?php echo"$r[nama_kota]"; ?></a></li>
					<li class="active"><?php echo"$r[restaurant_name]"; ?></li>
				</ol>
				<?php
					if(isset($_SESSION['resto_notif'])){
						if($_SESSION['resto_notif']=="suka"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully like this restaurant.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="suka_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was liked this restaurant before. You can only do one time in 24 hours.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="rekomendasi"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully recommend this restaurant.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="rekomendasi_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was recommended this restaurant before. You can only do one time in 24 hours.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="bookmark"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully bookmark this restaurant.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="bookmark_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was bookmark this restaurant before. You can only do one time.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="disini"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully added the data to be here at this restaurant.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="disini_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was added the data at this restaurant before. You can only do one time.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="lapor"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully report this restaurant.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="lapor_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was reported this restaurant before. You can only do one time.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="rating"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully rating this restaurant.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="rating_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was rated this restaurant before. You can only do one time in 24 hours.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="rating_pilih"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> Please select star rating value before vote.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="login_dulu"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Must Login!</strong> You must login or register before doing this action.
							</div>";
						}
						unset($_SESSION['resto_notif']);
					}
				?>
				<h1 class="judul">
				<?php
					echo"$r[restaurant_name] <br>";
					if($total_rating<>0){
						echo"";?>
						<a href = "#" style="text-decoration: none;" class="klik-bintang">
							<?php echo"<input type='hidden' class='rating' data-filled='fa fa-star' data-empty='fa fa-star-o' readonly='readonly' value='$total_rating'>";?>
						</a>
						<?php echo"<span style='font-size: 14px;'>Rating : $total_rating </span><em class='f-12'>from $r[jumlah_vote] votes</em>";
					}
				?>
				</h1>
				<div class="mb10 f-12"><em>type</em> <?php echo"$r[nama_type_of_business]"; ?> <span class="bullet">&#8226;</span>
					<a href="<?php echo"$base_url/$r[username]"; ?>"><em>by</em> <?php if(!empty($r['nama_depan'])){echo"$r[nama_depan] $r[nama_belakang]";}else{echo"Admin";} ?></a>
					<span class="bullet">&#8226;</span> <em>post</em> <?php echo"$post"; ?>
				</div>
				<div class="box-info-1">
					
					<?php
						include 'config/func/kiri_resto.php';
					?>
					<?php
						include 'config/func/kanan_resto.php';
					?>


					<div class="clearfix"></div>
					<div class="mt20 tag">Tagged under
						<?php
						$tag  = explode(",",$r['tag']);
						foreach ( $tag as $item_tag ) {
							echo"<a href='$base_url/pages/restaurant/search/?tag=$item_tag'>#$item_tag</a> ";
						}
						?>
					</div>
				</div>
				<div role="tabpanel">
					<ul class="nav nav-tabs" role="tablist">
						<li><a href="<?php echo"$base_url/pages/restaurant/info/$_GET[id]/$slug"; ?>">Info</a></li>
						<li><a href="<?php echo"$base_url/pages/restaurant/menu/$_GET[id]/$slug"; ?>">Menu</a></li>
						<li><a href="<?php echo"$base_url/pages/restaurant/food/$_GET[id]/$slug"; ?>">Food</a></li>
						<li class="active"><a href="#">Beverage</a></li>
						<li><a href="<?php echo"$base_url/pages/restaurant/photo/$_GET[id]/$slug"; ?>">Photo Gallery</a></li>
						<li><a href="<?php echo"$base_url/pages/restaurant/voter/$_GET[id]/$slug"; ?>">Voter</a></li>
						<li><a href="<?php echo"$base_url/pages/restaurant/map/$_GET[id]/$slug"; ?>">Map</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-heading">
							<?php if(!empty($_SESSION['food_member'])){ echo"<a href='$base_url/$u[username]/my-beverage/new'>Add Photo</a>";}?>
							<div class="clearfix"></div>
						</div>
						<div class="tab-panel">
							<ul id="menu-page" class="grid cs-style-3">
								<?php
								$sql=mysqli_query($koneksi,"select f.id_beverage,f.nama_beverage,(select p.gambar_beverage_photo from beverage_photo p where p.id_beverage=f.id_beverage order by p.id_beverage_photo desc limit 1) as gambar,f.dilihat,(SELECT COUNT(h.id_member) FROM beverage_like h WHERE h.id_beverage=f.id_beverage) AS dilike from beverage f where f.id_restaurant='$r[id_restaurant]'");
								$ada_menu=mysqli_num_rows($sql);
								if($ada_menu<>0){
									while($m=mysqli_fetch_array($sql)){
										$se=seo($m['nama_beverage']);
										$id_beverage = id_masking($m['id_beverage']);
										echo"<li>
										<figure>
											<a href='$base_url/assets/img/beverage/big_$m[gambar]' data-lightbox='foodie' data-title='$m[nama_beverage]'><img data-original='$base_url/assets/img/beverage/big_$m[gambar]' class='lazy' width='213' height='160'></a>
											<figcaption>
												<div class='pull-left'>
													<h3><a href='$base_url/pages/beverage/info/$id_beverage/$se'>$m[nama_beverage]</a></h3>
													<span class='f-12'>$m[dilike] Like <span class='bullet'>&#8226;</span> $m[dilihat] Views</span>
												</div>
												<div class='pull-right'>
													<a href='$base_url/pages/beverage/event/info/$id_beverage/1'><i class='fa fa-heart f-merah'></i></a>
													<a href='$base_url/pages/beverage/event/info//$id_beverage/5'><i class='fa fa-flag f-merah'></i></a>
												</div>
												<div class='clearfix'></div>
											</figcaption>
										</figure>
									</li>";
									}
								}
								else{
									echo"<p>There is no beverage data who associated with the restaurant</p>";
								}
								?>
							</ul>
							<div class="holder holder-1"></div>
						</div>
					</div>
				</div>
				<div role="tabpanel" id="tab-vote">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#result" aria-controls="result" role="tab" data-toggle="tab">Result</a></li>
						<li role="presentation"><a href="#feature" aria-controls="feature" role="tab" data-toggle="tab">Feature Vote</a></li>
						<li role="presentation"><a href="#quick" aria-controls="quick" role="tab" data-toggle="tab">Quick Vote</a></li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane tab-panel active fade in" id="result">
							<table class="tablebintang">
								<tr>
									<td>Cleanliness <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall cleanliness of the restaurant, including the washroom, dining area, food & beverage, kitchens, cooks and the servers."></i></td>
									<td><input type="hidden" class="rating" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" readonly="readonly" value="<?php echo"$img_cleanlines"; ?>"/></td>
									<td rowspan="5" class="mene-atas">
										<span><?php echo"$total_rating"; ?></span>
										<em>from <?php echo"$r[jumlah_vote]"; ?> vote</em>
									</td>
								</tr>
								<tr>
									<td>Customer Service <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall customer services of the restaurant including the recepcionist, the phone operators, the food & beverage servers, manages and cashiers."></i></td>
									<td><input type="hidden" class="rating" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" readonly="readonly" value="<?php echo"$img_customer"; ?>"/></td>
								</tr>
								<tr>
									<td>Food &amp; Beverage <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall food & beverage assessment based on cleanliness, flavor, ingredient freshness, cooking or making, presentation & aroma and value for money."></i></td>
									<td><input type="hidden" class="rating" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" readonly="readonly" value="<?php echo"$img_food"; ?>"/></td>
								</tr>
								<tr>
									<td>Comfort <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment of the restaurant's comfortless on the sitting arrangement whether the table is too close to each other or the light is too direct to your eyes to be uncomforted or the coziness environment which make you feel home and comfort, etc."></i></td>
									<td><input type="hidden" class="rating" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" readonly="readonly" value="<?php echo"$img_comfort"; ?>"/></td>
								</tr>
								<tr>
									<td>Value for Money <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment on the restaurant's food & beverage, comfortless, and services they provides to us a whole compare to the price we paid."></i></td>
									<td><input type="hidden" class="rating" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" readonly="readonly" value="<?php echo"$img_money"; ?>"/></td>
								</tr>
								<tr class="total-vote">
									<td colspan="2">
										<span><?php echo"$total_rating"; ?></span>
										<em>from <?php echo"$r[jumlah_vote]"; ?> vote</em>
									</td>
								</tr>
							</table>
						</div>
						<div role="tabpanel" class="tab-pane tab-panel fade" id="feature">
							<form method="get" action="<?php echo"$base_url/restaurant_aksi.php"; ?>">
								<input type="hidden" value="6" name="fungsi">
								<input type="hidden" value="info" name="page">
								<input type="hidden" value="<?php echo"$_GET[id]"; ?>" name="id">
								<table class="tablebintang">
									<tr>
										<td>Cleanliness <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall cleanliness of the restaurant, including the washroom, dining area, food & beverage, kitchens, cooks and the servers."></i></td>
										<td><input type="hidden" class="rating" id="rat1" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="cleanlines"></td>
										<td rowspan="5" class="mene-atas"><button class="btn btn-danger" type="submit">Vote</button>
											<div class='alert-danger alert-dismissible as' role='alert'>
						                  <strong>Please submit minimum one star for each rating categories in other to be rated</strong>
						               </div>
										</td>
									</tr>
									<tr>
										<td>Customer Service <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall customer services of the restaurant including the recepcionist, the phone operators, the food & beverage servers, manages and cashiers."></i></td>
										<td><input type="hidden" class="rating" id="rat2" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="customer_services"></td>
									</tr>
									<tr>
										<td>Food &amp; Beverage <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall food & beverage assessment based on cleanliness, flavor, ingredient freshness, cooking or making, presentation & aroma and value for money."></i></td>
										<td><input type="hidden" class="rating" id="rat3" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="food_beverage"></td>
									</tr>
									<tr>
										<td>Comfort <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment of the restaurant's comfortless on the sitting arrangement whether the table is too close to each other or the light is too direct to your eyes to be uncomforted or the coziness environment which make you feel home and comfort, etc."></td>
										<td><input type="hidden" class="rating" id="rat4" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="comfort"></td>
									</tr>
									<tr>
										<td>Value for Money <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment on the restaurant's food & beverage, comfortless, and services they provides to us a whole compare to the price we paid."></i></td>
										<td><input type="hidden" class="rating" id="rat5" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="value_money"></td>
									</tr>
									<tr class="total-vote">
										<td colspan="2">
											<button class="btn btn-danger" type="submit">Vote</button>
											<div class='alert-danger alert-dismissible as' role='alert'>
						                  <strong>Please submit minimum one star for each rating categories in other to be rated</strong>
						               </div>
										</td>
									</tr>
								</table>
							</form>
						</div>
						<div role="tabpanel" class="tab-pane tab-panel fade" id="quick">
							<form method="get" action="<?php echo"$base_url/restaurant_aksi.php"; ?>">
								<input type="hidden" value="6" name="fungsi">
								<input type="hidden" value="info" name="page">
								<input type="hidden" value="<?php echo"$_GET[id]"; ?>" name="id">
								<table class="tablebintang">
									<tr>
										<td><strong>Give your vote</strong></td>
										<td><input type="hidden" class="rating" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="rating"></td>
										<td><button class="btn btn-danger" type="submit">Vote</button></td>
									</tr>
								</table>
							</form>
						</div>
					</div>
					<ul class="list-inline">
						<li><i class="fa fa-star"></i> Poor</li>
						<li><i class="fa fa-star"></i><i class="fa fa-star"></i> Below Average</li>
						<li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Average</li>
						<li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Very Good</li>
						<li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Excellent</li>
					</ul>
				</div>
				<?php
				$here=mysqli_query($koneksi,"select * from restaurant_here left join member on restaurant_here.id_member=member.id_member where id_restaurant='$id'");
				$jumlah_here=mysqli_num_rows($here);
				?>
				<h5 class="f-merah"><?php echo"$jumlah_here"; ?> Members Have Being Here</h5>
				<?php
				if($jumlah_here<>0){
					echo"<div class='member-carousel'>";
					while($h=mysqli_fetch_array($here)){
						echo"<div><img data-original='$base_url/assets/img/member/$h[gambar_thumb]' class='lazy' width='40' height='40'></div>";
					}
					echo"</div>";
				}
				else{
					echo"<p>There is no member was here.</p>";
				}
				?>
				<h5 class="f-merah">Similar Restaurant</h5>
				<div class="media" style="overflow: unset">
					<div class="row">
						<div id="owl-demo" class="owl-carousel">
							<?php
							$sama=mysqli_query($koneksi,"select r.id_restaurant, r.restaurant_name, r.tgl_post, r.id_propinsi,(select p.gambar_restaurant_photo from restaurant_photo p where p.id_restaurant=r.id_restaurant order by p.id_restaurant_photo desc limit 1) as gambar, (select count(h.id_restaurant_here) from restaurant_here h where h.id_restaurant=r.id_restaurant) as jumlah, (SELECT COUNT(f.id_member) FROM restaurant_like f WHERE f.id_restaurant=r.id_restaurant) AS jumlah_like from restaurant r where r.id_propinsi='$r[id_propinsi]' and r.id_restaurant <> '$r[id_restaurant]' order by r.tgl_post DESC limit 8");
							$ada_resto=mysqli_num_rows($sama);
							if($ada_resto<>0){
								while($s=mysqli_fetch_array($sama)){
									$slug=seo($s['restaurant_name']);
									$post=date("jS M, Y", strtotime($s['tgl_post']));
									$id = id_masking($s['id_restaurant']);
									echo"<div class='item'>
									<div class='thumb'>
										<a href='$base_url/pages/restaurant/info/$id/$slug'><img src='$base_url/assets/img/restaurant/big_$s[gambar]' width='160' height='120'></a>
									</div>
									<div class='info'>
										<em>posted on $post</em>
										<h3 class='sembunyi'><a href='$base_url/pages/restaurant/info/$id/$slug'>$s[restaurant_name]</a></h3>
										<ul class='list-unstyled f-12'>
											<li>$s[jumlah_like] Like <span class='bullet'>&#8226;</span> $s[jumlah] Being here</li>
										</ul>
									</div>
								</div>";
								}
							}
							else{
								echo"<div class='col-12'>There is no restaurant with same state right now.</div>";
							}
							?>
						</div>
					</div>
				</div>
				<hr>
				<div id="disqus_thread"></div>
			</div>
			<div class="col-4 mene-atas">
				<?php include"config/inc/search.php"; ?>
				<?php include"config/inc/iklan.php"; ?>
				<div class="fb-like-box" data-href="https://www.facebook.com/foodieguidances#_=_" data-width="220" data-height="350" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
			</div>
		</div>
    </div>
   <?php include"config/inc/footer.php"; ?>
	<?php
	  $idu = id_masking($_GET['id']);
		  $pb=mysqli_query($koneksi, "select * from restaurant where id_restaurant < $idu ORDER BY id_restaurant desc limit 1");
	  $jumpb=mysqli_num_rows($pb);
	  $tpb=mysqli_fetch_array($pb);
	  if ($jumpb<>0) {
		  $ba = id_masking($tpb['id_restaurant']);
		  $slug=seo($tpb['restaurant_name']);
		  $next="$base_url/pages/restaurant/info/$ba/$slug";
	  } else {
		  $pba=mysqli_query($koneksi, "select * from restaurant ORDER BY id_restaurant desc limit 1");
		  $jumpba=mysqli_num_rows($pba);
		  $tpba=mysqli_fetch_array($pba);
		  $baa = id_masking($tpba['id_restaurant']);
		  $sluga=seo($tpba['restaurant_name']);
		  $next="$base_url/pages/restaurant/info/$baa/$sluga";
	  }

	  $pn=mysqli_query($koneksi, "select * from restaurant where id_restaurant > $idu ORDER BY id_restaurant limit 1");
	  $jumpn=mysqli_num_rows($pn);
	  $tpn=mysqli_fetch_array($pn);
	  if ($jumpn<>0) {
		  $na = id_masking($tpn['id_restaurant']);
		  $slug=seo($tpn['restaurant_name']);
		  $back="$base_url/pages/restaurant/info/$na/$slug";
	  } else {
		  $pna=mysqli_query($koneksi, "select * from restaurant ORDER BY id_restaurant limit 1");
		  $jumpna=mysqli_num_rows($pna);
		  $tpna=mysqli_fetch_array($pna);
		  $naa = id_masking($tpna['id_restaurant']);
		  $sluga=seo($tpna['restaurant_name']);
		  $back="$base_url/pages/restaurant/info/$naa/$sluga";
	  }
  ?>
  <a class="scrollToNext" href="<?php echo $back ?>" style="bottom: 320px;"><img src="<?php echo"$base_url/assets/img/theme/kiri.png" ?>"></a>
  <a class="scrollToNext" href="<?php echo $next ?>" style="bottom: 273px;"><img src="<?php echo"$base_url/assets/img/theme/kanan.png" ?>"></a>
  <a href="#" class="scrollToTop"><img src="<?php echo"$base_url/assets/img/theme/atas.png" ?>"></a>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap-rating.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/slick.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/lightbox.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jpages.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.lazyload.min.js"></script>

	<script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
   <script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
   <script>
      $(document).ready(function() {
	      $('.drawer').drawer();

			$('.tutup').click(function(){
				$('.iklanan').toggleClass('hilang', 500);
			});

			$('.cart').click(function(e){
				e.stopPropagation();
				$('.dis-cart').toggle();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$('.plus').click(function(e){
				e.stopPropagation();
				$('.dis-plus').toggle();
				$('.dis-cart').hide();
				$('.dis-share').hide();
			});
			$('.share').click(function(e){
				e.stopPropagation();
				$('.dis-share').toggle();
				$('.dis-plus').hide();
				$('.dis-cart').hide();
			});
			$('.drawer-toggle').click(function(){
				$('.dis-cart').hide();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$(document).click(function () {
				 var $el = $(".dis-share");
				 if ($el.is(":visible")) {
					  $el.fadeOut(200);
				 }
				 var $ela = $(".dis-plus");
				 if ($ela.is(":visible")) {
					  $ela.fadeOut(200);
				 }
				 var $elu = $(".dis-cart");
				 if ($elu.is(":visible")) {
					  $elu.fadeOut(200);
				 }
		   });

      });
   </script>

	<?php if(!empty($_SESSION['food_member'])){ ?>
		<script src="<?php echo"$base_url"; ?>/assets/js/theme.js"></script>
	<?php } ?>
	<script type="text/javascript">
		$(document).ready(function () {
			$(document).on("contextmenu",function(e){
				if(e.target.nodeName != "INPUT" && e.target.nodeName != "TEXTAREA")
				e.preventDefault();
			});
			$.fn.disableTextSelect = function() {
				return this.each(function() {
					$(this).css({
						'MozUserSelect':'none',
						'webkitUserSelect':'none'
					}).attr('unselectable','on').bind('selectstart', function() {
						return false;
					});
				});
			};
			$('body').disableTextSelect();
		});
	</script>

	<script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>
	<script type="text/javascript">stLight.options({publisher: "7b97d330-d7b9-49c3-b5ee-b3aaa89bbe66", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=348272391978609&version=v2.0";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
	<script src="<?php echo"$base_url"; ?>/assets/js/owl.carousel.js"></script>
	<link href="<?php echo"$base_url"; ?>/assets/css/owl.carousel.css" rel="stylesheet">
   <link href="<?php echo"$base_url"; ?>/assets/css/owl.theme.css" rel="stylesheet">
	<style>
	#owl-demo .item{
		 margin: 3px;
	}
	#owl-demo .item img{
		 display: block;
		 width: 100%;
		 height: auto;
	}
	</style>
	<script>
	$(document).ready(function() {
	  $("#owl-demo").owlCarousel({
		 items : 4,
		 navigation : true
	  });
	});
	</script>
	<script type="text/javascript">
		$(document).ready(function () {

			$(window).scroll(function(){
	 		  if ($(this).scrollTop() > 100) {
	 			  $('.scrollToTop').fadeIn();
	 		  } else {
	 			  $('.scrollToTop').fadeOut();
	 		  }
	 	  });

	 	  $(window).scroll(function(){
	 		 if ($(this).scrollTop() > 50) {
	 			 $('.scrollToNext').fadeIn();
	 		 } else {
	 			 $('.scrollToNext').fadeOut();
	 		 }
	 	  });

				$(".as").hide();
		      var rat1 = $("#rat1").val();
		      var rat2 = $("#rat2").val();
		      var rat3 = $("#rat3").val();
		      var rat4 = $("#rat4").val();
		      var rat5 = $("#rat5").val();
		      var rat5 = $("#rat5").val();
		      $("#rat1,#rat2,#rat3,#rat4,#rat5").change(function(){
		         if ((rat1 < 1) && (rat2 < 1) && (rat3 < 1) && (rat4 < 1) && (rat5 < 1) ) {
		            $(".btn-danger").hide();
		            $(".as").show();
		         }
		      });

		      $("#rat1,#rat2,#rat3,#rat4,#rat5").change(function(){
		         var rat1 = $("#rat1").val();
		         var rat2 = $("#rat2").val();
		         var rat3 = $("#rat3").val();
		         var rat4 = $("#rat4").val();
		         var rat5 = $("#rat5").val();
		         if ((rat1 > 0) && (rat2 > 0) && (rat3 > 0) && (rat4 > 0) && (rat5 > 0)) {
		            $(".btn-danger").show();
		            $(".as").hide();
		         }
		      });

			$("img.lazy").lazyload({
				effect : "fadeIn"
			});
			$(".alert").fadeTo(5000, 500).fadeOut(500, function(){
				$(".alert").alert('close');
			});
			$('.tip').tooltip();
			$('.rating').rating();
			$('.member-carousel').slick({
				infinite: true,
				slidesToShow: 14,
				slidesToScroll: 14,
				arrows:false,
				autoplay:true,
				autoplaySpeed: 10000
			});
			$(".holder-1").jPages({
				containerID : "menu-page",
				previous : "Previous Page",
				next :"Next Page",
				links: "blank",
				perPage:6
			});
			$('#go-to-vote').click(function() {
				$('html, body').animate({
					scrollTop: $( $(this).attr('href') ).offset().top
				}, 500);
				$('#tab-vote a[href="#feature"]').tab('show');
				return false;
			});
			$('.social-share').click( function(event){
				event.stopPropagation();
				$( ".social-box" ).toggle(1);
			});
			$('.carousel').carousel({
				interval: 10000
			});
			$(document).click( function(){
				$('.social-box').hide();
			});
		});
		/* * * CONFIGURATION VARIABLES * * */
		var disqus_shortname = 'foodieguidancescom';

		/* * * DON'T EDIT BELOW THIS LINE * * */
		(function() {
			var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
			dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
			(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
		})();
	</script>

	<?php
		include "config/func/foot_resto.php";
	?>

</body>
</html>
