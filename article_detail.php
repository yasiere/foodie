<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";
include "config/func/jumlah_data.php";
include "config/func/id_masking.php";
$auto_logout=1800000;
if(!empty($_SESSION['food_member'])){
	if (time()-$_SESSION['timestamp']>$auto_logout){
		session_destroy();
		session_unset();
		header("Location: ".$base_url."/auto-logout");
		exit();
	}else{
		$_SESSION['timestamp']=time();
	}
	include "config/func/member_data.php";
}
$id = id_masking($_GET['id']);
$active="article";
$type="article_detail";
mysqli_query($koneksi,"update article set dilihat=dilihat + 1 where id_article='$id'");
$resto=mysqli_query($koneksi,"select *,(select count(h.id_article) from article_like h where h.id_article=f.id_article) as dilike from article f LEFT JOIN member m ON f.id_member=m.id_member WHERE f.id_article='$id'");
$r=mysqli_fetch_array($resto);
$slug=seo($r['nama_article']);
$post=date("jS M, Y", strtotime($r['tgl_post']));
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
    <title>Article Detail - Foodie Guidances</title>
	<meta name="keywords" content="">
  <meta name="description" content="">
	<link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<meta property="og:title" content="<?php echo"$r[nama_article]"; ?>" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="<?php echo"$base_url/pages/article/info/$_GET[id]/$slug"; ?>" />
	<meta property="og:image" content="<?php echo"$base_url/assets/img/medium_foodieguidances.jpg"; ?>" />
	<meta property="og:description" content="<?php echo"$r[nama_article]"; ?>" />
	<meta property="og:site_name" content="Foodie Guidances" />
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/lightbox.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/flexslider.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/slick.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>
	<link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
 <body style="position: initial;" class="drawer drawer--right">
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>
    <div class="container hook">
		<div class="row">
			<div class="col-12 col-8a">
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Article</a></li>
					<li class="active"><?php echo"$r[nama_article]"; ?></li>
				</ol>
				<?php
					if(isset($_SESSION['resto_notif'])){
						if($_SESSION['resto_notif']=="suka"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully like this article.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="suka_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was liked this article before. You can only do one time in 24 hours.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="bookmark"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully bookmark this article.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="bookmark_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was bookmark this article before. You can only do one time.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="lapor"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully report this article.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="lapor_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was reported this article before. You can only do one time.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="login_dulu"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Must Login!</strong> You must login or register before doing this action.
							</div>";
						}
						unset($_SESSION['resto_notif']);
					}
				?>
				<h1 class="judul"><?php echo"$r[nama_article]"; ?></h1>
				<div class="mb10 f-12">
					<a href="<?php echo"$base_url/$r[username]"; ?>"><em>by</em> <?php if(!empty($r['nama_depan'])){echo"$r[nama_depan] $r[nama_belakang]";}else{echo"Admin";} ?></a>
					<span class="bullet">&#8226;</span> <em>post</em> <?php echo"$post"; ?></div>
				<div class="box-info-1">
					<div class="pull-left">
						<div class="flexslider">
							<ul class="slides">
								<?php
								$gsql=mysqli_query($koneksi,"select gambar_article_photo,nama_article_photo from article_photo where id_article='$id' order by id_article_photo desc");
								while($g=mysqli_fetch_array($gsql)){
									echo"<li data-thumb='$base_url/assets/img/article/medium_$g[gambar_article_photo]'>
										<a href='$base_url/assets/img/article/big_$g[gambar_article_photo]' data-lightbox='$g[nama_article_photo]' data-title='$g[nama_article_photo]'><img src='$base_url/assets/img/article/medium_$g[gambar_article_photo]'></a>
									</li>";
								}
								?>
							</ul>
						</div>
					</div>
					<div class="pull-right">
						<div class="detail-menu">
							<a href="<?php echo"$base_url/pages/article/event/$_GET[id]/1"; ?>" title="Like"><i class="fa fa-heart-o"></i></a>
							<a href="<?php echo"$base_url/pages/article/event/$_GET[id]/3"; ?>" title="Bookmark"><i class="fa fa-bookmark-o"></i></a>
							<span class="social-share"><i class="fa fa-share-alt"></i>
								<div class="social-box" style=" left: -90px;">
									<span class='st_facebook_large' displayText='Facebook'></span>
									<span class='st_linkedin_large' displayText='LinkedIn'></span>
									<span class='st_pinterest_large' displayText='Pinterest'></span>
									<span class='st_googleplus_large' displayText='Google +'></span>
									<span class='st_twitter_large' displayText='Tweet'></span>
									<span class='st_sharethis_large' displayText='ShareThis'></span>
								</div>
							</span>
							<a href="<?php echo"$base_url/pages/article/event/$_GET[id]/5"; ?>" title="Report"><i class="fa fa-flag-o"></i></a>
						</div>
						<div class="statistik">
							<em>
								<i class="fa fa-heart"></i>
								<a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">
									<?php	echo"$r[dilike] likes"; ?>
								</a>
								<?php
								if(!empty($_SESSION['food_member'])){
									$me=mysqli_query($koneksi,"select tgl_like from article_like where id_member='$id_member' and id_article='$id' order by id_article_like desc limit 1");
									$ada_melike=mysqli_num_rows($me);
									if($ada_melike<>0){
										$q=mysqli_fetch_array($me);
										$like_date=date("jS M, Y", strtotime($q['tgl_like']));
										echo" (you has liked on $like_date)";
									}
								}
								?>
							</em>
							<em><i class="fa fa-eye"></i> <?php echo"$r[dilihat]"; ?> views</em>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="box-info-2">
					<?php echo"$r[konten_article]"; ?>
				</div>
				<hr>
				<h5 class="f-merah">Other Article</h5>
				<div class="media">
					<div class="row">
						<?php
						$resto=mysqli_query($koneksi,"select *,(select p.gambar_article_photo from article_photo p where p.id_article=f.id_article order by p.id_article desc limit 1) as gambar,(select count(h.id_article) from article_like h where h.id_article=f.id_article) as dilike from article f where f.id_article <> '$id' order by rand() limit 4");
						$ada_resto=mysqli_num_rows($resto);
						if($ada_resto<>0){
							while($s=mysqli_fetch_array($resto)){
								$slug=seo($s['nama_article']);
								$post=date("jS M, Y", strtotime($s['tgl_post']));
								echo"<div class='col-4'>
									<div class='thumb'>
										<a href='$base_url/pages/article/$s[id_article]/$slug'><img data-original='$base_url/assets/img/article/small_$s[gambar]' class='lazy' width='160' height='120'></a>
									</div>
									<div class='info'>
										<em>posted on $post</em>
										<h3 class='sembunyi'><a href='$base_url/pages/article/$s[id_article]/$slug'>$s[nama_article]</a></h3>
										<ul class='list-unstyled f-12'>
											<li>$s[dilike] Like <span class='bullet'>&#8226;</span> $s[dilihat] View</li>
										</ul>
									</div>
								</div>";
							}
						}
						else{
							echo"<div class='col-12'>There is no other article right now.</div>";
						}
						?>
					</div>
				</div>
				<hr>
				<div id="disqus_thread"></div>
			</div>
			<div class="col-4 mene-atas">
				<?php include"config/inc/search.php"; ?>
				<?php include"config/inc/iklan.php"; ?>
				<div class="fb-like-box" data-href="https://www.facebook.com/foodieguidances#_=_" data-width="220" data-height="350" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
			</div>
		</div>
    </div>
    <?php include"config/inc/footer.php"; ?>
	 <?php
    	   $idu = id_masking($_GET['id']);
    			$pb=mysqli_query($koneksi, "select * from article where id_article < $idu ORDER BY id_article desc limit 1");
    	   $jumpb=mysqli_num_rows($pb);
    	   $tpb=mysqli_fetch_array($pb);
    	   if ($jumpb<>0) {
    			$ba = id_masking($tpb['id_article']);
    			$slug=seo($tpb['nama_article']);
    			$next="$base_url/pages/article/$ba/$slug";
    	   } else {
    			$pba=mysqli_query($koneksi, "select * from article ORDER BY id_article desc limit 1");
    			$jumpba=mysqli_num_rows($pba);
    			$tpba=mysqli_fetch_array($pba);
    			$baa = id_masking($tpba['id_article']);
    			$sluga=seo($tpba['nama_article']);
    			$next="$base_url/pages/article/$baa/$sluga";
    	   }

    	   $pn=mysqli_query($koneksi, "select * from article where id_article > $idu ORDER BY id_article limit 1");
    	   $jumpn=mysqli_num_rows($pn);
    	   $tpn=mysqli_fetch_array($pn);
    	   if ($jumpn<>0) {
    			$na = id_masking($tpn['id_article']);
    			$slug=seo($tpn['nama_article']);
    			$back="$base_url/pages/article/$na/$slug";
    	   } else {
    			$pna=mysqli_query($koneksi, "select * from article ORDER BY id_article limit 1");
    			$jumpna=mysqli_num_rows($pna);
    			$tpna=mysqli_fetch_array($pna);
    			$naa = id_masking($tpna['id_article']);
    			$sluga=seo($tpna['nama_article']);
    			$back="$base_url/pages/article/$naa/$sluga";
    	   }
    	 ?>
    	 <a class="scrollToNext" href="<?php echo $back ?>" style="bottom: 320px;"><img src="<?php echo"$base_url/assets/img/theme/kiri.png" ?>"></a>
    	 <a class="scrollToNext" href="<?php echo $next ?>" style="bottom: 273px;"><img src="<?php echo"$base_url/assets/img/theme/kanan.png" ?>"></a>
    	 <a href="#" class="scrollToTop"><img src="<?php echo"$base_url/assets/img/theme/atas.png" ?>"></a>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/lightbox.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.flexslider-min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.lazyload.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
   <script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
   <script>
      $(document).ready(function() {
				$('.drawer').drawer({
					iscroll: {
						scrollbars: true,
						mouseWheel: true,
						interactiveScrollbars: true,
						shrinkScrollbars: 'scale',
						fadeScrollbars: true,
						click: true
					},
					showOverlay: true
			 });

			$('.tutup').click(function(){
				$('.iklanan').toggleClass('hilang', 500);
			});

			$('.cart').click(function(e){
				e.stopPropagation();
				$('.dis-cart').toggle();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$('.plus').click(function(e){
				e.stopPropagation();
				$('.dis-plus').toggle();
				$('.dis-cart').hide();
				$('.dis-share').hide();
			});
			$('.share').click(function(e){
				e.stopPropagation();
				$('.dis-share').toggle();
				$('.dis-plus').hide();
				$('.dis-cart').hide();
			});
			$('.drawer-toggle').click(function(){
				$('.dis-cart').hide();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$(document).click(function () {
				 var $el = $(".dis-share");
				 if ($el.is(":visible")) {
					  $el.fadeOut(200);
				 }
				 var $ela = $(".dis-plus");
				 if ($ela.is(":visible")) {
					  $ela.fadeOut(200);
				 }
				 var $elu = $(".dis-cart");
				 if ($elu.is(":visible")) {
					  $elu.fadeOut(200);
				 }
		   });

      });
   </script>
	<?php if(!empty($_SESSION['food_member'])){ ?>
		<script src="<?php echo"$base_url"; ?>/assets/js/theme.js"></script>
	<?php } ?>
	<script type="text/javascript">
		$(document).ready(function () {
			$(document).on("contextmenu",function(e){
				if(e.target.nodeName != "INPUT" && e.target.nodeName != "TEXTAREA")
				e.preventDefault();
			});
			$.fn.disableTextSelect = function() {
				return this.each(function() {
					$(this).css({
						'MozUserSelect':'none',
						'webkitUserSelect':'none'
					}).attr('unselectable','on').bind('selectstart', function() {
						return false;
					});
				});
			};
			$('body').disableTextSelect();
		});
	</script>
	<script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
	$(window).scroll(function(){
	    if ($(this).scrollTop() > 100) {
	       $('.scrollToTop').fadeIn();
	    } else {
	       $('.scrollToTop').fadeOut();
	    }
	});

	$(window).scroll(function(){
	   if ($(this).scrollTop() > 50) {
	      $('.scrollToNext').fadeIn();
	   } else {
	      $('.scrollToNext').fadeOut();
	   }
	});

	  $('#go-to-vote').click(function() {
	     $('html, body').animate({
	        scrollTop: $( $(this).attr('href') ).offset().top
	     }, 500);
	     $('#tab-vote a[href="#feature"]').tab('show');
	     return false;
	  });
	});
	</script>
	<script type="text/javascript">stLight.options({publisher: "7b97d330-d7b9-49c3-b5ee-b3aaa89bbe66", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=348272391978609&version=v2.0";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
	<script type="text/javascript">
		$(document).ready(function () {
			$("img.lazy").lazyload({
				effect : "fadeIn"
			});
			$(".alert").fadeTo(5000, 500).fadeOut(500, function(){
				$(".alert").alert('close');
			});
			$('.flexslider').flexslider({
				animation: "slide",
				controlNav: "thumbnails",
				start: function(slider){
				  $('body').removeClass('loading');
				}
			});
			$('.social-share').click( function(event){
				event.stopPropagation();
				$( ".social-box" ).toggle(1);
			});
			$('.carousel').carousel({
				interval: 10000
			});
			$(document).click( function(){
				$('.social-box').hide();
			});
			/* * * CONFIGURATION VARIABLES * * */
			var disqus_shortname = 'foodieguidancescom';

			/* * * DON'T EDIT BELOW THIS LINE * * */
			(function() {
				var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
				dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
				(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
			})();
		});
	</script>
	<style media="screen">
	.black_overlay{
		display: none;
		position: absolute;
		top: 0%;
		left: 0%;
		width: 100%;
		height: 100%;
		background-color: black;
		z-index:1001;
		-moz-opacity: 0.8;
		opacity:.80;
		filter: alpha(opacity=80);
	}
	.white_content {
		display: none;
		position: fixed;
		top: 20%;
		left: 40%;
		width: 20%;
		height: 56%;
		padding: 24px;
		border: 5px solid #C00606;
		background-color: #FFF;
		z-index: 1002;
		overflow-y: auto;
		overflow-x: hidden;
	}
	</style>
	<div id="light" class="white_content">
		<h3 style="font-size: 20px; margin: 0px 0px 15px;">Like</h3>
		<table class="table">
			<?php
				$idu=id_masking($_GET['id']);
				$me=mysqli_query($koneksi,"SELECT * FROM article_like r left join member m on m.id_member=r.id_member where r.id_article='$idu'");
				$no=1;
				$juma=mysqli_num_rows($me);
				if (!empty($juma)) {
					while ($tame=mysqli_fetch_array($me)) {
						$like_date=date("jS M, Y", strtotime($tame['tgl_like']));
							echo "<tr><td>$no.</td>
										<td><img src='$base_url/assets/img/member/$tame[gambar_thumb]' class='foto_profil' width='30px'></td> <td>$tame[username]</td> <td>$like_date</td></tr>";
						$no++;
					}
				}
				else {
					echo "Empty";
				}
			?>
		</table>
		<a style="position: absolute; border: 6px solid rgb(255, 255, 255); right: -6px; top: -6px; z-index: 999999999; background: #C00606; color: rgb(255, 255, 255); padding: 6px 10px;" href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'"><i class="fa fa-close"></i></a>
	</div>
	<div id="lighta" class="white_content">
		<h3 style="font-size: 20px; margin: 0px 0px 15px;">Recommendation</h3>
		<table class="table">
			<?php
				$idu=id_masking($_GET['id']);
				$me=mysqli_query($koneksi,"SELECT * FROM recipe_rekomendasi r left join member m on m.id_member=r.id_member where r.id_recipe='2'");
				$no=1;
				$juma=mysqli_num_rows($me);
				if (!empty($juma)) {
					while ($tame=mysqli_fetch_array($me)) {
						$like_date=date("jS M, Y", strtotime($tame['tgl_rekomendasi']));
							echo "<tr><td>$no.</td>
										<td><img src='$base_url/assets/img/member/$tame[gambar_thumb]' class='foto_profil' width='30px'></td> <td>$tame[username]</td> <td>$like_date</td></tr>";
						$no++;
					}
				}
				else {
					echo "Empty";
				}
			?>
		</table>
		<a style="position: absolute; border: 6px solid rgb(255, 255, 255); right: -6px; top: -6px; z-index: 999999999; background: #C00606; color: rgb(255, 255, 255); padding: 6px 10px;" href = "javascript:void(0)" onclick = "document.getElementById('lighta').style.display='none';document.getElementById('fadea').style.display='none'"><i class="fa fa-close"></i></a>
	</div>
	<div id="fade" class="black_overlay"  onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'"></div>
	<div id="fadea" class="black_overlay"  onclick = "document.getElementById('lighta').style.display='none';document.getElementById('fadea').style.display='none'"></div>
</body>
</html>
