<?php error_reporting(0);
session_start();
$auto_logout=1800000;
include "config/func/base_url.php";
include "config/func/id_masking.php";

if(!empty($_SESSION['food_member'])){
if (time()-$_SESSION['timestamp']>$auto_logout){
    session_destroy();
    session_unset();
	header("Location: ".$base_url."/auto-logout");
	exit();
}else{
    $_SESSION['timestamp']=time();
}
include "config/database/db.php";
include "config/func/member_data.php";
$active = "member";
$type = "process";
$id = id_masking($_GET['id']);

// $resto=mysqli_query($koneksi,"SELECT *,d.id_kota,r.id_negara as negara, r.id_propinsi as propinsi, r.id_kota as kota,(SELECT COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS jumlah_vote,(SELECT COUNT(f.id_member) FROM restaurant_like f WHERE f.id_restaurant=r.id_restaurant) AS jumlah_like,(SELECT COUNT(f.id_member) FROM restaurant_rekomendasi f WHERE f.id_restaurant=r.id_restaurant) AS jumlah_rekomendasi,(SELECT SUM(f.cleanlines)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_cleanlines,(SELECT SUM(f.customer_services)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_customer,(SELECT SUM(f.food_beverage)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_food,(SELECT SUM(f.comfort)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_comfort,(SELECT SUM(f.value_money)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_money,(SELECT (SUM(f.cleanlines) + SUM(f.customer_services) + SUM(f.food_beverage) + SUM(f.comfort) + SUM(f.value_money)) / COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant =r.id_restaurant) AS total_rate FROM restaurant r LEFT JOIN type_of_business a ON r.id_type_of_business = a.id_type_of_business LEFT JOIN negara b ON r.id_negara = b.id_negara LEFT JOIN propinsi c ON r.id_propinsi = c.id_propinsi LEFT JOIN kota d ON r.id_kota = d.id_kota LEFT JOIN landmark e ON r.id_landmark = e.id_landmark LEFT JOIN mall f ON r.id_mall = f.id_mall LEFT JOIN operation_hour g ON r.id_operation_hour = g.id_operation_hour LEFT JOIN price_index i ON r.id_price_index = i.id_price_index LEFT JOIN suitable_for j ON r.id_suitable_for = j.id_suitable_for LEFT JOIN serving_time m ON r.id_serving_time = m.id_serving_time LEFT JOIN type_of_service n ON r.id_type_of_service = n.id_type_of_service LEFT JOIN wifi p ON r.id_wifi = p.id_wifi LEFT JOIN term_of_payment q ON r.id_term_of_payment = q.id_term_of_payment LEFT JOIN premise_security s ON r.id_premise_security = s.id_premise_security LEFT JOIN premise_fire_safety t ON r.id_premise_fire_safety = t.id_premise_fire_safety LEFT JOIN premise_maintenance u ON r.id_premise_maintenance = u.id_premise_maintenance LEFT JOIN parking_spaces v ON r.id_parking_spaces = v.id_parking_spaces LEFT JOIN ambience w ON r.id_ambience = w.id_ambience LEFT JOIN attire x ON r.id_attire = x.id_attire LEFT JOIN clean_washroom y ON r.id_clean_washroom = y.id_clean_washroom LEFT JOIN tables_availability z ON r.id_tables_availability = z.id_tables_availability LEFT JOIN noise_level ON r.id_noise_level = noise_level.id_noise_level LEFT JOIN waiter_tipping ON r.id_waiter_tipping = waiter_tipping.id_waiter_tipping LEFT JOIN member ON r.id_member = member.id_member LEFT JOIN air_conditioning ON r.id_air_conditioning = air_conditioning.id_air_conditioning LEFT JOIN heating_system ON r.id_heating_system = heating_system.id_heating_system LEFT JOIN premise_hygiene ON r.id_premise_hygiene = premise_hygiene.id_premise_hygiene where r.id_restaurant='$id'");
// $e=mysqli_fetch_array($resto);

$process=mysqli_query($koneksi,"select *, (select p.gambar_process_photo from process_photo p where p.id_process=f.id_process order by p.id_process_photo desc limit 1) as gambar,
	(SELECT COUNT(h.id_member) FROM process_like h WHERE h.id_process=f.id_process) AS dilike, 
	(select count(id_process_rating) as jumlah from process_rating where process_rating.id_process=f.id_process) as jumlah_vote,
		(SELECT SUM(r.expired) / COUNT(r.id_member) FROM process_rating r WHERE r.id_process=f.id_process) AS t_expired,
		(SELECT SUM(r.taste) / COUNT(r.id_member) FROM process_rating r WHERE r.id_process=f.id_process) AS t_taste,
		(SELECT SUM(r.serving) / COUNT(r.id_member) FROM process_rating r WHERE r.id_process=f.id_process) AS t_serving,
		(SELECT SUM(r.freshness) / COUNT(r.id_member) FROM process_rating r WHERE r.id_process=f.id_process) AS t_freshness,   
	(select count(h.id_process) from process_rekomendasi h where h.id_process=f.id_process) as direkomendasi,
	(SELECT (SUM(r.expired) + SUM(r.taste) + SUM(r.freshness) + SUM(r.serving)) / COUNT(r.id_member) FROM process_rating r WHERE r.id_process =f.id_process) AS total 
	from process f,process_category k, price_index p, negara n where f.id_negara=n.id_negara and f.categories=k.id_process_category and f.id_price_index=p.id_price_index and f.id_process='$id'");
$r=mysqli_fetch_array($process);

$gsql=mysqli_query($koneksi,"select * from process_photo p where p.id_process='$id' ORDER By id_process_photo DESC limit 1");
$g=mysqli_fetch_array($gsql);
$gambar=$g['gambar_process_photo'];
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
  <title>Submit Process - Foodie Guidances</title>
	<meta name="keywords" content="">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/jquery-ui.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/select2.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/awesome-bootstrap-checkbox.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
 <body style="position: initial;" class="drawer drawer--right">
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>
   <div class="loadi">
      <img src="<?php echo "$base/assets/img/theme/load.gif"?>">
   </div>
    <div class="container hook">
		<div class="row">
			<div class="col-16">
				<ul class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Account</a></li>
					<li class="active">Submit Process</li>
				</ul>
				<?php
				if(isset($_SESSION['notif'])){
					if($_SESSION['notif']=="gambar"){
						echo"<div class='alert alert-danger alert-dismissible' role='alert'>
							<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
							<strong>Failed!</strong> Image used have to be in JPG, PNG, and GIF format and the maximum total images size is 50MB also minimal dimension must be 800x600 pixel.
						</div>";
					}
					unset($_SESSION['notif']);
				}
				?>
				<h3 class="f-merah mb10">Edit Process F&B</h3>
				<p><span class="f-merah">*</span> required field</p>
				<form class="border-form form-horizontal" method="post" action="<?php echo"$base_url"; ?>/config/func/edit_process.php" enctype="multipart/form-data">
					<input type="hidden" name="id" value="<?php echo $id;?>">
					<input type="hidden" name="gambar_processku" value="<?php echo $gambar;?>">
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Process Type <span class="f-merah">*</span></label></div>
						<div class="col-6 col-4b">
							<select name="process_type" required class="form-control">
								<option value="">Select Process Type</option>
								<option value="1" <?php echo ($r['type_process'] == 1 ) ? 'selected' : '' ; ?> >Food</option>
								<option value="2" <?php echo ($r['type_process'] == 2 ) ? 'selected' : '' ; ?>>Beverage</option>
							</select>
						</div>
					</div>   
               		<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Process Name <span class="f-merah">*</span></label></div>
						<div class="col-6 col-4b">
							<input type="text" class="form-control" placeholder="Write Name" id="food" value="<?php echo $r[nama_process]; ?>" autocomplete="off" name="nama_process" required>
						</div>
					</div>

					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Manufactured by</label></div>
						<div class="col-6 col-4b">
							<input type="text" class="form-control" placeholder="Write Manufactured by" value="<?php echo $r[manufactured_by]; ?>" id="food" autocomplete="off" name="manufactured_by">
						</div>
					</div>

					<div class="row mb20">
					  <div class="col-4 col-4b"><label class="control-label">Country of Origin <span class="f-merah">*</span></label></div>
					  <div class="col-6 col-4b">
					     <select name="negara" required class="form-control" id="negara">
					        <option value="">Select Country</option>
					        <?php
					        $sql=mysqli_query($koneksi,"select * from negara order by nama_negara asc");
					        while($b=mysqli_fetch_array($sql)){
					           $selected="";
					           if($b['id_negara']==$r['id_negara']){
					              $selected="selected";
					           }
					           echo"<option value='$b[id_negara]' $selected>$b[nama_negara]</option>";
					        }
					        ?>
					     </select>
					  </div>
					</div>

					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Reviewed Country</label></div>
						<div class="col-6 col-4b">
							<?php 
								// echo $_SESSION['food_member'];
								$sql = "SELECT * FROM member m left join negara n on m.id_negara=n.id_negara WHERE `id_member` = $_SESSION[food_member]";
								$negara = mysqli_fetch_array(mysqli_query($koneksi, $sql));
							?>
							<input type="text" class="form-control" placeholder="Write Manufactured by" value="<?php echo $r[nama_negara]; ?>" readonly autocomplete="off" required name="reviewed_country">
						</div>
					</div>

					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Month of Manufactured</label></div>
						<div class="col-6 col-4b">
							<input type="text" class="form-control" placeholder="Write Month of Manufactured" id="food" value="<?php echo $r[month_of_manufactured]; ?>" autocomplete="off" name="month_of_manufactured">
						</div>
					</div>

					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Date of Reviewed</label></div>
						<div class="col-6 col-4b">							
							<input type="text" class="form-control" placeholder="Write Month of Manufactured" value="<?php echo $r[date_of_viewed]; ?>" readonly autocomplete="off" name="date_of_viewed">
						</div>
					</div>

					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Date of Expired</label></div>
						<div class="col-6 col-4b">
							<input type="text" class="form-control datepicker" placeholder="Write Month of Manufactured" value="<?php echo $r[date_of_expired]; ?>" autocomplete="off" name="date_of_expired">
						</div>
					</div>

					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Expired Periode</label></div>
						<div class="col-6 col-4b">
							<input type="text" class="form-control" placeholder="Write Month of Manufactured" id="exp"  value="<?php echo $r[expired_periode]; ?>" readonly autocomplete="off" name="expired_periode">
						</div>
					</div>

					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Photo of Product <span class="f-merah">*</span></label></div>
						<div class="col-6 col-4b">
							<?php 
							echo "<img src='$base_url/assets/img/process/big_$gambar' width='100%'>";
							?>
							<p class="help-block"><i>if not replaced. then, do not fill it</i></p><br>
							<div class="fileinput fileinput-new" data-provides="fileinput">
							  <div class="fileinput-preview upload" data-trigger="fileinput"></div>
							  <input type="file" name="gambar_process" class="hidden" id="gam">
							</div>
							<p class="help-block">Image used have to be in JPG, PNG, and GIF format and the maximum total images size is 50MB also minimal dimension must be 800x600 pixel.</p>
						</div>
					</div>

					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Price Index</label></div>
						<div class="col-6 col-4b">
							<select name="id_price_index" required class="form-control">
								<?php
								$sql=mysqli_query($koneksi,"select * from price_index order by nama_price_index asc");
								while($g=mysqli_fetch_array($sql)){
									$selected="";
									if($g['id_price_index']==$r['id_price_index']){
							              $selected="selected";
							        }
									echo"<option value='$g[id_price_index]' $selected>$g[nama_price_index]</option>";
								}
								?>
							</select>
						</div>
					</div>

					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Food Chemical Used</label></div>
						<div class="col-6 col-4b">
							<select name="food_chemical_used" required class="form-control">
								<option value="got" <?php if ($r['food_chemical_used'] == 'got') { echo "selected";} ?>>Got Food Chemical Used</option>
								<option value="no" <?php if ($r['food_chemical_used'] == 'no') { echo "selected";} ?>>No Food Chemical Used</option>
							</select>
						</div>
					</div>

					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Categories</label> <span class="f-merah">*</span></div>
						<div class="col-6 col-4b">
							<?php 
								echo $g['id_categories'];
							 ?>
							<select name="categories" required class="form-control cat">
								<option value=''>Select Category</option>
								<?php

								$sql=mysqli_query($koneksi,"select * from process_category order by nama_process_category asc");
								while($g=mysqli_fetch_array($sql)){
									$selected="";
									if($g['id_process_category'] == $r['categories'] ){
							              $selected="selected";
							        }
									echo"<option value='$g[ket]##$g[id_process_category]' $selected>$g[nama_process_category]</option>";
								}
								?>
							</select>
						</div>
					</div>

					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Tag </label></div>
						<div class="col-6 col-4b">
							<input type="text" class="form-control" placeholder="Write tag (separated by comma)" name="tag" <?php echo"value='$r[tag]'"; ?>>
						</div>
					</div>


					
                 
					<button type="submit" class="btn btn-danger">Submit</button> 
					<button type="reset" class="btn btn-danger">Reset</button>
              
               
				</form>
			</div>
		</div>
    </div>
    <?php include"config/inc/footer.php"; ?>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery-ui.min.js"></script>

	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/select2.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jasny-bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap-rating.min.js"></script>
    <script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/moment.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/moment-precise-range.js"></script>
	<script>
		$(document).ready(function() {
			 $('.drawer').drawer();

		 $('.tutup').click(function(){
			 $('.iklanan').toggleClass('hilang', 500);
		 });

		 $('.cart').click(function(e){
			 e.stopPropagation();
			 $('.dis-cart').toggle();
			 $('.dis-plus').hide();
			 $('.dis-share').hide();
		 });
		 $('.plus').click(function(e){
			 e.stopPropagation();
			 $('.dis-plus').toggle();
			 $('.dis-cart').hide();
			 $('.dis-share').hide();
		 });
		 $('.share').click(function(e){
			 e.stopPropagation();
			 $('.dis-share').toggle();
			 $('.dis-plus').hide();
			 $('.dis-cart').hide();
		 });
		 $('.drawer-toggle').click(function(){
			 $('.dis-cart').hide();
			 $('.dis-plus').hide();
			 $('.dis-share').hide();
		 });
		 $('.btn-cari').click(function(){
			 $('.search_kiri').toggleClass('search_kiri_tam', 500);
		 });
		 $(document).click(function () {
				var $el = $(".dis-share");
				if ($el.is(":visible")) {
					 $el.fadeOut(200);
				}
				var $ela = $(".dis-plus");
				if ($ela.is(":visible")) {
					 $ela.fadeOut(200);
				}
				var $elu = $(".dis-cart");
				if ($elu.is(":visible")) {
					 $elu.fadeOut(200);
				}
			});

		 });
	</script>
   <script type="text/javascript">
		$(document).ready(function () {
			$(document).on("contextmenu",function(e){
				if(e.target.nodeName != "INPUT" && e.target.nodeName != "TEXTAREA")
				e.preventDefault();
			});
			$.fn.disableTextSelect = function() {
				return this.each(function() {
					$(this).css({
						'MozUserSelect':'none',
						'webkitUserSelect':'none'
					}).attr('unselectable','on').bind('selectstart', function() {
						return false;
					});
				});
			};
			$('body').disableTextSelect();
		});
	</script>

	<script type="text/javascript">
		

	$(document).ready(function () {


		function strstr(haystack, needle, bool) {  

		    var pos = 0;

		    haystack += '';
		    pos = haystack.toLowerCase().indexOf((needle + '').toLowerCase());
		    if (pos == -1) {
		        return false;
		    } else {
		        if (bool) {
		            return haystack.substr(0, pos);
		        } else {
		            return haystack.slice(pos);
		        }
		    }
		}
		
			
		$(".as").hide();
		$(".cat").change(function() {
			var y = $(this).val();
			var x = strstr(y, '##', true);

			

			if (x == 1) {
				$(".pertama").hide();
				$(".kedua").show();
					$(".btn-danger").show();
					$(".as").hide();
					$("#rata1,#rata2,#rata3,#rata4").change(function(){
						var rata1 = $("#rata1").val();
						var rata2 = $("#rata2").val();
						var rata3 = $("#rata3").val();
						var rata4 = $("#rata4").val();						
						if (rata1 > 0 && rata2 > 0 && rata3 > 0 && rata4 > 0 ) {
							$(".btn-danger").show();
							$(".as").hide();					
						}
						else{
							
							$(".btn-danger").hide();
								$(".as").show();
						}
					});

			}
			else{
				$( ".pertama" ).show();
				$( ".kedua" ).hide();
				$("#rat1,#rat2,#rat3").change(function(){
						var rat1 = $("#rat1").val();
						var rat2 = $("#rat2").val();
						var rat3 = $("#rat3").val();					
						if (rat1 > 0 && rat2 > 0 && rat3 > 0) {
							$(".btn-danger").show();
							$(".as").hide();					
						}
						else{
							
							$(".btn-danger").hide();
							$(".as").show();
						}
					});
				
			}
			// alert(x);
		});
			
		$( ".datepicker" ).change(function() {
			var m1 = moment('<?php echo date("Y-m-d") ?>','YYYY-MM-DD');
			var dateku = $(this).val();
			var m2 = moment(dateku,'YYYY-MM-DD');
			$( "#exp" ).val(moment.preciseDiff(m1, m2));
			// alert(moment.preciseDiff(m1, m2));

			// var m1 = moment('2014-01-01','YYYY-MM-DD');
			// var m2 = moment('2014-02-03','YYYY-MM-DD');
			// alert(moment.preciseDiff(m1, m2));
		});
		

			// alert(x);

			// $( ".datepicker" ).change(function() {
				
			// });
		
		    $( ".datepicker" ).datepicker({
		      changeMonth: true,
		      changeYear: true,
		      dateFormat: 'yy-mm-dd'
		    });
		 

      
      
      // $("#rat1,#rat2,#rat3,#rat4,#rat5,#rat6").change(function(){
      //    var rat1 = $("#rat1").val();
      //    var rat2 = $("#rat2").val();
      //    var rat3 = $("#rat3").val();
      //    var rat4 = $("#rat4").val();
      //    var rat5 = $("#rat5").val();
      //    var rat5 = $("#rat5").val();
      //    var rat6 = $("#rat6").val();
      //    if ((rat1 > 0) && (rat2 > 0) && (rat3 > 0) && (rat4 > 0) && (rat5 > 0) && (rat6 > 0)) {
            
      //    }
      // });

		$('.tip').tooltip();
		$("select").select2();
		$('.rating').rating();
		$('#search').autocomplete({
			source: "<?php echo "$base_url/config/func/ajax_resto_search.php"; ?>",
			minLength: 3
		});
		$(".alert").fadeTo(5000, 500).fadeOut(500, function(){
			$(".alert").alert('close');
		});

      $("#negara").change(function(){
			var id = $("#negara").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_propinsi.php",
				data: "id=" + id,
				success: function(data){
					$("#propinsi").html(data);
					$("#propinsi").fadeIn(2000);
				}
			});
		});
		$("#propinsi").change(function(){
			var id = $("#propinsi").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_kota.php",
				data: "id=" + id,
				success: function(data){
					$("#kota").html(data);
					$("#kota").fadeIn(2000);
				}
			});
		});

      $("#kota").change(function(){
			var id = $("#kota").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_resto.php",
				data: "id=" + id,
				success: function(data){
					$("#search").html(data);
					$("#search").fadeIn(2000);
				}
			});
		});

      $(".box-head").click(function(){
         var targeta = $(this).children(".klik");
         $(targeta).toggleClass("fa-angle-up fa-angle-down");
			var target = $(this).parent().children(".box-panel");
			$(target).slideToggle();
		});

	});
	</script>
   <script type="text/javascript">
      // $("form").submit(function( event ) {
      // 	var kota = $("#kota").val();
      // 	var search_box = $("#search").val();
      // 	var food = $("#food").val();
      // 	var kat = $("#kat").val();
      //    var gam = $("#gam").val();
      //    if (!(kota === "") && !(search_box === "") && !(food === "") && !(kat === "") && !(gam === "")) {
      //       $(".loadi").show();
      //    }
      //    else {

      //    }
      // });
	</script>
</body>
</html>
<?php
}
else{
	header("Location: ".$base_url."/login-area");
}
?>
