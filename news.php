<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";
include "config/func/jumlah_data.php";
include "config/func/paging.php";
include "config/func/id_masking.php";
$auto_logout=600;
if(!empty($_SESSION['food_member'])){
	if (time()-$_SESSION['timestamp']>$auto_logout){
		session_destroy();
		session_unset();
		header("Location: ".$base_url."/auto-logout");
		exit();
	}else{
		$_SESSION['timestamp']=time();
	}
	include "config/func/member_data.php";
}
$active = "news";
$type = "news";
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
    <title>News - Foodie Guidances</title>
	<meta name="keywords" content="">
    <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>
  <link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">
  <style media="" data-href="https://www.foodieguidances.com/assets/css/superslides.css">#slides {
  position: relative;
  min-width:940px;
}
#slides .slides-container {
  display: none;
}
#slides .scrollable {
  *zoom: 1;
  position: relative;
  top: 0;
  left: 0;
  overflow-y: auto;
  -webkit-overflow-scrolling: touch;
  height: 100%;
}
#slides .scrollable:after {
  content: "";
  display: table;
  clear: both;
}

.slides-navigation {
  margin: 0 auto;
  position: absolute;
  z-index: 3;
  top: 46%;
  width: 100%;
}
.slides-navigation a {
  position: absolute;
  display: block;
}
.slides-navigation a.prev {
  left: 0;
}
.slides-navigation a.next {
  right: 0;
}

.slides-pagination {
  position: absolute;
  z-index: 3;
  bottom: 8px;
  text-align: center;
  width: 100%;
}
.slides-pagination a {
  border-radius: 15px;
  width: 10px;
  height: 10px;
  display: -moz-inline-stack;
  display: inline-block;
  vertical-align: middle;
  *vertical-align: auto;
  zoom: 1;
  *display: inline;
  background:#fff;
  margin: 2px;
  overflow: hidden;
  text-indent: -100%;
  content:"" !important;
}
.slides-pagination a.current {
  background: #ed1c24;
}
.slides-container img{min-width:940px!important;}</style>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
 <body style="position: initial;" class="drawer drawer--right">
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>
	<div class="pencarian">
		<div class="pencari">
			<form method="get" action="<?php echo"$base_url/pages/coupon/search/"; ?>" id="ale-form" onsubmit="myFunction()">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Search based on Name, Category, Country, State, City..." id="search" name="keyword">
					<span class="input-group-btn">
						<button class="btn btn-danger" type="submit"><i class="fa fa-search"></i></button>
					</span>
				</div><!-- /input-group -->
			</form>
		</div>
		<div id="slides">
			<div class="slides-container">
				<?php
				$sql=mysqli_query($koneksi,"SELECT * FROM slideshow ORDER BY id_slideshow ASC");
				while($w=mysqli_fetch_array($sql)){
					echo"<img src='$base_url/assets/img/slideshow/$w[gambar]' alt='foodie guidances'>";
				}
				?>
			</div>
		</div>
		<div class="wrap-pencarian mene-atas">
			<!-- <h1>Coupon, FoodieGuidances.com</h1> -->
			<div class="box-pencarian">
				<div class="col-16 al">
					<form method="get" action="<?php echo"$base_url/pages/coupon/search/"; ?>">
						<strong>Search</strong>
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Search based on Coupon title.." id="search" name="keyword">
							<span class="input-group-btn">
								<button class="btn btn-danger" type="submit"><i class="fa fa-search"></i></button>
							</span>
						</div><!-- /input-group -->
					</form>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
    <div class="container">
		<div class="row">
			<div class="col-12 col-8a no-padding">
				<div class="col-8" style="width:50%">
					<h4 class="f-merah no-mb mt20">
						<a href="<?php echo"$base_url/pages/coupon/search/?sort=r.id_coupon+DESC"; ?>">Latest Coupon</a>
					</h4>
					<span class="f-abu f-12 blk mb20">Worldwide data collection</span>
				</div>
				<div class="col-8" style="width:50%">
					<h4 class="f-merah no-mb mt20">
						<a href="<?php echo"$base_url/pages/coupon/search/?keyword="; ?>">
							<i class="fa fa-search" style="margin-right:5px"></i> Advance Search
						</a>
					</h4>
					 <span class="f-abu f-12">All Coupon</span>
				</div>
				<div style="clear: both;"></div>


				<div class="row col-4a">
					<div class="media jarak1">
						<?php
						$p      = new Paging;
						$batas  = 10;
						$posisi = $p->cariPosisi($batas);
						$sql=mysqli_query($koneksi,"select *,(select count(h.id_news) from news_like h where h.id_news=f.id_news) as dilike from news f order by f.id_news ASC limit $posisi,$batas");
						$jmldata = mysqli_num_rows(mysqli_query($koneksi,"SELECT * from news"));
						while($l=mysqli_fetch_array($sql)){
							$slug=seo($l['nama_news']);
							$post=date("jS M, Y", strtotime($l['tgl_post']));
							$konten_berita = htmlentities(strip_tags($l['konten_news']));
							$isi_berita = substr($konten_berita,0,350);
							$isi_berita = substr($konten_berita,0,strrpos($isi_berita," "));
							$isi_berita = html_entity_decode($isi_berita);
							$id = id_masking($l['id_news']);
							echo"<div class='row col-4a'>
								<div class='col-5 thumb col-8a'>
									<a href='$base_url/pages/news/$id/$slug'><img data-original='$base_url/assets/img/news/small_$l[gambar_news]' class='lazy' width='220' height='165'></a>
									<a href='$base_url/pages/news'><span>News</span></a>
								</div>
								<div class='col-11 info col-8a'>
									<em>posted on $post</em>
									<h3><a href='$base_url/pages/news/$id/$slug'>$l[nama_news]</a></h3>
									<ul class='list-unstyled f-12'>
										<li>$isi_berita...</li>
										<li>$l[dilike] Like <span class='bullet'>&#8226;</span> $l[dilihat] View</li>
									</ul>
								</div>
							</div>";

						}
						?>

					</div>
				</div>
				<nav class="ac">
					<?php
					$link="$base_url/pages/news";
					$jmlhalaman  = $p->jumlahHalaman($jmldata, $batas);
					$linkHalaman = $p->navHalaman($link,$_GET['page'], $jmlhalaman);
					echo "<ul class='pagination'>$linkHalaman</ul>";
					?>
				</nav>
			</div>
			<div class="col-4 mene-atas">
				<?php include"config/inc/search.php"; ?>
				<?php include"config/inc/iklan.php"; ?>
				<div class="fb-like-box" data-href="https://www.facebook.com/foodieguidances#_=_" data-width="220" data-height="350" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
			</div>
		</div>
	</div>
    <?php include"config/inc/footer.php"; ?>
	<a class="scrollToNext" href="<?php echo "$base_url/pages/fgmart" ?>" style="bottom: 320px;"><img src="<?php echo"$base_url/assets/img/theme/kiri.png" ?>"></a>
	 <a class="scrollToNext" href="<?php echo  "$base_url/pages/article" ?>" style="bottom: 273px;"><img src="<?php echo"$base_url/assets/img/theme/kanan.png" ?>"></a>
	 <a href="#" class="scrollToTop"><img src="<?php echo"$base_url/assets/img/theme/atas.png" ?>"></a>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery-ui.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.superslides.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.lazyload.min.js"></script>

	<script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
   <script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
   <script>
      $(document).ready(function() {
	      $('.drawer').drawer();

			$('.tutup').click(function(){
				$('.iklanan').toggleClass('hilang', 500);
			});

			$('.cart').click(function(e){
				e.stopPropagation();
				$('.dis-cart').toggle();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$('.plus').click(function(e){
				e.stopPropagation();
				$('.dis-plus').toggle();
				$('.dis-cart').hide();
				$('.dis-share').hide();
			});
			$('.share').click(function(e){
				e.stopPropagation();
				$('.dis-share').toggle();
				$('.dis-plus').hide();
				$('.dis-cart').hide();
			});
			$('.drawer-toggle').click(function(){
				$('.dis-cart').hide();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$(document).click(function () {
				 var $el = $(".dis-share");
				 if ($el.is(":visible")) {
					  $el.fadeOut(200);
				 }
				 var $ela = $(".dis-plus");
				 if ($ela.is(":visible")) {
					  $ela.fadeOut(200);
				 }
				 var $elu = $(".dis-cart");
				 if ($elu.is(":visible")) {
					  $elu.fadeOut(200);
				 }
		   });
      });
   </script>

	<?php if(!empty($_SESSION['food_member'])){ ?>
		<script src="<?php echo"$base_url"; ?>/assets/js/theme.js"></script>
	<?php } ?>
	<script type="text/javascript">
		$(document).ready(function () {
			$(document).on("contextmenu",function(e){
				if(e.target.nodeName != "INPUT" && e.target.nodeName != "TEXTAREA")
				e.preventDefault();
			});
			$.fn.disableTextSelect = function() {
				return this.each(function() {
					$(this).css({
						'MozUserSelect':'none',
						'webkitUserSelect':'none'
					}).attr('unselectable','on').bind('selectstart', function() {
						return false;
					});
				});
			};
			$('body').disableTextSelect();
		});
	</script>
	<div id="fb-root"></div>
	<script type="text/javascript">
	$(document).ready(function() {
	$(window).scroll(function(){
		 if ($(this).scrollTop() > 100) {
			 $('.scrollToTop').fadeIn();
		 } else {
			 $('.scrollToTop').fadeOut();
		 }
	});

	$(window).scroll(function(){
		if ($(this).scrollTop() > 50) {
			$('.scrollToNext').fadeIn();
		} else {
			$('.scrollToNext').fadeOut();
		}
	});

	  $('#go-to-vote').click(function() {
		  $('html, body').animate({
			  scrollTop: $( $(this).attr('href') ).offset().top
		  }, 500);
		  $('#tab-vote a[href="#feature"]').tab('show');
		  return false;
	  });
	});
	</script>
	<script>
	$(document).ready(function () {
		$("img.lazy").lazyload({
			effect : "fadeIn"
		});
		$('#slides').superslides({
			animation: 'slide',
			inherit_height_from: '.pencarian',
			play: 8000,
			pagination: true
		});
		$('.carousel').carousel({
			interval: 10000
		});
		$('#search').autocomplete({
			source: "<?php echo "$base_url/config/func/ajax_coupon_search.php"; ?>",
			minLength: 3
		});
	});
	/*=====For developer to config ====*/
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=348272391978609&version=v2.0";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script>
</body>
</html>
