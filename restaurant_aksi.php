<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";
include "config/func/id_masking.php";
date_default_timezone_set("Asia/Jakarta");
$sekarang=date("Y-m-d");
$id = id_masking($_GET['id']);
$r=mysqli_fetch_array(mysqli_query($koneksi,"select id_restaurant, restaurant_name from restaurant where id_restaurant='$id'"));
$seo=seo($r['restaurant_name']);
if(isset($_SESSION['food_member'])){
	if($_GET['fungsi']=="1"){
		$ada=mysqli_num_rows(mysqli_query($koneksi,"select id_restaurant_like from restaurant_like where id_restaurant='$id' and id_member='$_SESSION[food_member]' and tgl_like='$sekarang'"));
		if($ada==0){
			mysqli_query($koneksi,"insert into restaurant_like (id_member,id_restaurant,tgl_like) values('$_SESSION[food_member]','$id','$sekarang')");
			$_SESSION['resto_notif']     = "suka";
		}
		else{
			$_SESSION['resto_notif']     = "suka_gagal";
		}
	}
	elseif($_GET['fungsi']=="2"){
		$ada=mysqli_num_rows(mysqli_query($koneksi,"select id_restaurant_rekomendasi from restaurant_rekomendasi where id_restaurant='$id' and id_member='$_SESSION[food_member]' and tgl_rekomendasi='$sekarang'"));
		if($ada==0){
			mysqli_query($koneksi,"insert into restaurant_rekomendasi (id_member,id_restaurant,tgl_rekomendasi) values('$_SESSION[food_member]','$id','$sekarang')");
			$_SESSION['resto_notif']     = "rekomendasi";
		}
		else{
			$_SESSION['resto_notif']     = "rekomendasi_gagal";
		}
	}
	elseif($_GET['fungsi']=="3"){
		$ada_bookmark=mysqli_num_rows(mysqli_query($koneksi,"select id_bookmark from bookmark where id='$id' and jenis='restaurant' and id_member='$_SESSION[food_member]'"));
		if($ada_bookmark==0){
			mysqli_query($koneksi,"insert into bookmark (id_member,jenis,id,tgl_bookmark) values('$_SESSION[food_member]','restaurant','$id','$sekarang')");
			$_SESSION['resto_notif']     = "bookmark";
		}
		else{
			$_SESSION['resto_notif']     = "bookmark_gagal";
		}
	}
	elseif($_GET['fungsi']=="4"){
		$ada_here=mysqli_num_rows(mysqli_query($koneksi,"select id_restaurant_here from restaurant_here where id_restaurant='$id' and id_member='$_SESSION[food_member]'"));
		if($ada_here==0){
			mysqli_query($koneksi,"insert into restaurant_here (id_member,id_restaurant,tgl_here) values('$_SESSION[food_member]','$id','$sekarang')");
			$_SESSION['resto_notif']     = "disini";
		}
		else{
			$_SESSION['resto_notif']     = "disini_gagal";
		}
	}
	elseif($_GET['fungsi']=="5"){
		$ada_report=mysqli_num_rows(mysqli_query($koneksi,"select id_restaurant_report from restaurant_report where id_restaurant='$id' and id_member='$_SESSION[food_member]'"));
		if($ada_report==0){
			mysqli_query($koneksi,"insert into restaurant_report (id_member,id_restaurant,tgl_report) values('$_SESSION[food_member]','$id','$sekarang')");
			$_SESSION['resto_notif']     = "lapor";
		}
		else{
			$_SESSION['resto_notif']     = "lapor_gagal";
		}
	}
	elseif($_GET['fungsi']=="6"){

		$ada_report=mysqli_num_rows(mysqli_query($koneksi,"select id_restaurant_rating from restaurant_rating where id_restaurant='$id' and id_member='$_SESSION[food_member]' and tgl_restaurant_rating='$sekarang'"));
		if($ada_report==0){
			if(!empty($_GET['rating'])){
				$r1=$_GET['rating'] * 0.26;
				$r2=$_GET['rating'] * 0.24;
				$r3=$_GET['rating'] * 0.23;
				$r4=$_GET['rating'] * 0.14;
				$r5=$_GET['rating'] * 0.13;
				mysqli_query($koneksi,"INSERT INTO restaurant_rating (id_restaurant,id_member,cleanlines,customer_services,food_beverage,comfort,value_money,tgl_restaurant_rating) values('$id','$_SESSION[food_member]','$r1','$r2','$r3','$r4','$r5','$sekarang')");
				mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('restaurant','$id')");
				$_SESSION['resto_notif']     = "rating";

				$global=mysqli_query($koneksi,"SELECT id_restaurant FROM restaurant");
				while ($b=mysqli_fetch_array($global)) {
					$rank=mysqli_fetch_array(mysqli_query($koneksi, "SELECT
						((SUM(cleanlines) + SUM(customer_services) + SUM(food_beverage) + SUM(comfort) + SUM(value_money)) / COUNT(id_member)) as rank,
						(SELECT COUNT(h.id_member) FROM restaurant_like h WHERE $b[id_restaurant]=h.id_restaurant) AS jumlah_like,
						(SELECT COUNT(id_restaurant_rating) AS jumlah from restaurant_rating WHERE $b[id_restaurant] = restaurant_rating.id_restaurant) AS jumlah_vote
						FROM
					restaurant_rating f WHERE $b[id_restaurant] = f.id_restaurant"));
						mysqli_query($koneksi,"UPDATE restaurant set jumlah='$rank[rank]', rank_vote='$rank[jumlah_vote]', rank_like='$rank[jumlah_like]'  where id_restaurant='$b[id_restaurant]'");
				}

			}
			else{
				if(!empty($_GET['cleanlines']) and !empty($_GET['customer_services']) and !empty($_GET['food_beverage']) and !empty($_GET['comfort']) and !empty($_GET['value_money'])){
					$r1=$_GET['cleanlines'] * 0.26;
					$r2=$_GET['customer_services'] * 0.24;
					$r3=$_GET['food_beverage'] * 0.23;
					$r4=$_GET['comfort'] * 0.14;
					$r5=$_GET['value_money'] * 0.13;
					mysqli_query($koneksi,"INSERT INTO restaurant_rating (id_restaurant,id_member,cleanlines,customer_services,food_beverage,comfort,value_money,tgl_restaurant_rating) values('$id','$_SESSION[food_member]','$r1','$r2','$r3','$r4','$r5','$sekarang')");
					$_SESSION['resto_notif']     = "rating";
					mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('restaurant','$id')");

					$global=mysqli_query($koneksi,"SELECT id_restaurant FROM restaurant");
					while ($b=mysqli_fetch_array($global)) {
						$rank=mysqli_fetch_array(mysqli_query($koneksi, "SELECT
							((SUM(cleanlines) + SUM(customer_services) + SUM(food_beverage) + SUM(comfort) + SUM(value_money)) / COUNT(id_member)) as rank,
							(SELECT COUNT(h.id_member) FROM restaurant_like h WHERE $b[id_restaurant]=h.id_restaurant) AS jumlah_like,
							(SELECT COUNT(id_restaurant_rating) AS jumlah from restaurant_rating WHERE $b[id_restaurant] = restaurant_rating.id_restaurant) AS jumlah_vote
							FROM
						restaurant_rating f WHERE $b[id_restaurant] = f.id_restaurant"));
							mysqli_query($koneksi,"UPDATE restaurant set jumlah='$rank[rank]', rank_vote='$rank[jumlah_vote]', rank_like='$rank[jumlah_like]'  where id_restaurant='$b[id_restaurant]'");
					}

				}
				else{
					include "config/func/rank_restaurant.php";
					$_SESSION['resto_notif']     = "rating_pilih";
				}
			}
		}
		else{
			$_SESSION['resto_notif']     = "rating_gagal";
			include "config/func/rank_restaurant.php";
		}
	}
	if($_GET['fungsi']=="7"){
		$ada=mysqli_num_rows(mysqli_query($koneksi,"select id_restaurant_photo_like from restaurant_photo_like where id_restaurant_photo_like='$_GET[idp]' and id_member='$_SESSION[food_member]' and tgl_photo_like='$sekarang'"));
		if($ada==0){
			mysqli_query($koneksi,"insert into restaurant_photo_like (id_member,id_restaurant_photo,tgl_photo_like) values('$_SESSION[food_member]','$_GET[idp]','$sekarang')");
			$_SESSION['resto_notif']     = "suka_photo";
		}
		else{
			$_SESSION['resto_notif']     = "suka_photo_gagal";
		}
	}
}
else{
	$_SESSION['resto_notif']     = "login_dulu";
}
header("Location: ".$base_url."/pages/restaurant/".$_GET['page']."/".$_GET['id']."/".$seo);
?>
