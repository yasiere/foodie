<?php
error_reporting(0);
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";
include "config/func/paging_search.php";
include "config/func/id_masking.php";

$auto_logout=1800000;
if(!empty($_SESSION['food_member'])){
	if (time()-$_SESSION['timestamp']>$auto_logout){
		session_destroy();
		session_unset();
		header("Location: ".$base_url."/auto-logout");
		exit();
	}else{
		$_SESSION['timestamp']=time();
	}
	include "config/func/member_data.php";
}
$active = "restaurant";
$type = "restaurant";
function tulis_cekbox($field,$koneksi,$judul) {
	$query ="select * from ".$judul." order by nama_".$judul;
	$r = mysqli_query($koneksi,$query);
	$_arrNilai = explode('+', $field);
	$str = '';
	$no=1;
	while ($w = mysqli_fetch_array($r)) {
		$_ck = (array_search($w[1], $_arrNilai) === false)? '' : 'checked';
		$str .= "<div class=''>
			<div class='radio radio-inline radio-danger'>
				<input type='radio' name='".$judul."' value='$w[1]' id='$judul$no' $_ck>
				<label for='$judul$no'> $w[1]</label>
			</div>
		</div>";
		$no++;
	}
	return $str;
}

function tulis_cekboxxer($field,$koneksi,$judul) {
	$query ="select * from ".$judul." order by nama_".$judul;
	$r = mysqli_query($koneksi,$query);
	$_arrNilai = explode('+', $field);
	$str = '';
	$no=1;
	while ($w = mysqli_fetch_array($r)) {
		$_ck = (array_search($w[1], $_arrNilai) === false)? '' : 'checked';
		$str .= "<div class=''>
			<div class='checkbox checkbox-inline checkbox-danger'>
				<input type='checkbox' name='".$judul."[]' value='$w[1]' id='$judul$no' $_ck>
				<label for='$judul$no'> $w[1]</label>
			</div>
		</div>";
		$no++;
	}
	return $str;
}
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
    <title>Restaurant Result - Foodie Guidances</title>
	<meta name="keywords" content="">
    <meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/select2.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/awesome-bootstrap-checkbox.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>

	<link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
 <body style="position: initial;" class="drawer drawer--right">
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>
	<div class="container hook">
		<form method="get" action="<?php echo"$base_url/pages/restaurant/search/"; ?>" class="border-form" id="ale-form" onsubmit="myFunction()">
		<div class="row">
			<div class="btn-cari">
				<a href="#">
					<img src="<?php echo"$base_url/assets/images/cari2.png"; ?>" alt="" height="100%" />
				</a>
			</div>
			<div class="col-4 search_kiri">


				<div class="border-form filter-form">
				<h4 class="panel-title" style="margin-top: 20px; color: rgb(237, 28, 36);">
					Advance Search
				</h4>
				<?php
					if(!empty($_GET['batas'])){
						$batas=$_GET['batas'];
					}
					else {
						$batas="25";
					}
				?>
					<div class="panel-group mt20 mb10" id="accordionsss" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default head-pan">
							<div class="panel-heading jud-pan" role="tab" id="resto1" data-toggle="collapse" data-parent="#accordionsss" href="#collapsesss1" aria-expanded="true" aria-controls="collapsesss1">
								<h4 class="panel-title">
									❯ Restaurant
								</h4>
							</div>
							<div id="collapsesss1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="resto1">
								<div class="panel-body">
									<form method="get" action="<?php echo"$base_url/pages/restaurant/search/"; ?>" class="border-form" id="ale-form" onsubmit="myFunction()">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a <?php if($_GET['sort']=='total_vote DESC'){ echo "class='akt'";} ?> href="<?php echo"$base_url" ?>/pages/restaurant/search/?sort=total_vote+DESC&amp;batas=<?php echo $batas; ?>">Top Ranking</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a <?php if($_GET['sort']=='r.dilihat DESC'){ echo "class='akt'";} ?> href="<?php echo"$base_url" ?>/pages/restaurant/search/?sort=r.dilihat+DESC&amp;batas=<?php echo $batas; ?>">Popular</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a <?php if($_GET['sort']=='direkomendasi DESC'){ echo "class='akt'";} ?> href="<?php echo"$base_url" ?>/pages/restaurant/search/?sort=direkomendasi+DESC&amp;batas=<?php echo $batas; ?>">Recomended</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="heading14">
												<h4 class="panel-title">
													<a data-toggle="collapse" <?php if(!empty($_GET['opening'])){ echo "class='akt'";} ?> data-parent="#accordion" href="#collapse14" aria-expanded="true" aria-controls="collapse1">Opening Status</a>
												</h4>
											</div>
											<div id="collapse14" <?php if(!empty($_GET['opening'])){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="heading14">
												<div class="panel-body">
													<div class="form-group">
														<select class="form-group" name="opening">
															<option value="">Select Opening Status</option>
															<option value="open" <?php if($_GET['opening']=='open'){ echo "selected";} ?>>Open</option>
															<option value="close" <?php if($_GET['opening']=='close'){ echo "selected";} ?>>Close</option>
															<option value="no" <?php if($_GET['opening']=='no'){ echo "selected";} ?>>No Information</option>
														</select>
													</div>
												</div>
											</div>
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a data-toggle="collapse" <?php if(!empty($_GET['country']) || !empty($_GET['state']) || !empty($_GET['city'])
													|| !empty($_GET['mall'])){ echo "class='akt'";} ?> data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">Location Filter</a>
												</h4>
											</div>
											<div id="collapse1" <?php if(!empty($_GET['country']) || !empty($_GET['state']) || !empty($_GET['city'])|| !empty($_GET['mall'])){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="heading1">
												<div class="panel-body">
													<div class="form-group">
														<label>Country</label>
														<select class="form-control" name="country" id="negara">
															<option value="" selected>Select Country</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from negara order by nama_negara asc");
															while($b=mysqli_fetch_array($sql)){
																if($b['id_negara']==$_GET['country']){
																	echo"<option value='$b[id_negara]' selected>$b[nama_negara]</option>";
																}
																else{
																	echo"<option value='$b[id_negara]'>$b[nama_negara]</option>";
																}
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>States/Province</label>
														<select class="form-control" name="state" id="propinsi">
															<option value="" selected>Select States/ Province</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from propinsi where id_negara='$_GET[country]' order by nama_propinsi asc");
															while($b=mysqli_fetch_array($sql)){
																if($b['id_propinsi']==$_GET['state']){
																	echo"<option value='$b[id_propinsi]' selected>$b[nama_propinsi]</option>";
																}
																else{
																	echo"<option value='$b[id_propinsi]'>$b[nama_propinsi]</option>";
																}
															}?>
														</select>
													</div>
													<div class="form-group">
														<label>City</label>
														<select class="form-control" name="city" id="kota">
															<option value="" selected>Select City</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from kota where id_propinsi='$_GET[state]' order by nama_kota asc");
															while($b=mysqli_fetch_array($sql)){
																if($b['id_kota']==$_GET['city']){
																	echo"<option value='$b[id_kota]' selected>$b[nama_kota]</option>";
																}
																else{
																	echo"<option value='$b[id_kota]'>$b[nama_kota]</option>";
																}
															}?>
														</select>
													</div>

												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading8lan">
												<h4 class="panel-title">
													<a data-toggle="collapse" <?php if(!empty($_GET['landmark'])){ echo "class='akt'";} ?> data-parent="#accordion" href="#collapse8lan" aria-expanded="true" aria-controls="collapse8lan">Landmark</a>
												</h4>
											</div>
											<div id="collapse8lan" <?php if(!empty($_GET['landmark'])){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="heading8lan">
												<div class="panel-body">
													<div class="form-group">
														<select class="form-control" name="landmark">
															<option value="" selected>Select Landmark </option>
															<?php
															$sql=mysqli_query($koneksi,"select * from landmark order by nama_landmark asc");
															while($d=mysqli_fetch_array($sql)){
																if($d['id_landmark']==$_GET['landmark']){
																	echo"<option value='$d[id_landmark]' selected>$d[nama_landmark]</option>";
																}
																else{
																	echo"<option value='$d[id_landmark]'>$d[nama_landmark]</option>";
																}
															}
															?>
														</select>
													</div>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading8sho">
												<h4 class="panel-title">
													<a data-toggle="collapse" <?php if(!empty($_GET['mall'])){ echo "class='akt'";} ?> data-parent="#accordion" href="#collapse8sho" aria-expanded="true" aria-controls="collapse8sho">Shopping Mall</a>
												</h4>
											</div>
											<div id="collapse8sho" <?php if(!empty($_GET['mall'])){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="heading8sho">
												<div class="panel-body">
													<div class="form-group">
														<select name="mall" class="form-control" id="mall">
															<option value="">Select Shopping Mall (You must select a city first)</option>
														</select>
													</div>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading8bus">
												<h4 class="panel-title">
													<a data-toggle="collapse" <?php if(!empty($_GET['business_type'])){ echo "class='akt'";} ?> data-parent="#accordion" href="#collapse8bus" aria-expanded="true" aria-controls="collapse8bus">Business Type</a>
												</h4>
											</div>
											<div id="collapse8bus" <?php if(!empty($_GET['business_type'])){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="heading8bus">
												<div class="panel-body">
													<div class="form-group">
														<select class="form-control" name="business_type">
															<option value="" selected>Select Business Type</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from type_of_business order by nama_type_of_business asc");
															while($a=mysqli_fetch_array($sql)){
																if($a['id_type_of_business']==$_GET['business_type']){
																	echo"<option value='$a[id_type_of_business]' selected>$a[nama_type_of_business]</option>";
																}
																else{
																	echo"<option value='$a[id_type_of_business]'>$a[nama_type_of_business]</option>";
																}
															}
															?>
														</select>
													</div>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading5">
												<h4 class="panel-title">
													<a data-toggle="collapse" <?php if(!empty($_GET['cuisine'])){ echo "class='akt'";} ?> data-parent="#accordion" href="#collapse5" aria-expanded="true" aria-controls="collapse5">Type of Cuisine</a>
												</h4>
											</div>
											<div id="collapse5" <?php if(!empty($_GET['cuisine'])){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="heading5">
												<div class="panel-body">
													<?php
														$curr='';
														foreach($_GET['cuisine'] as $currency){
															$curr.=$currency."+";
														}
														$cuisine=tulis_cekboxxer($curr,$koneksi,'cuisine');
														echo"$cuisine";
													?>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading2">
												<h4 class="panel-title">
													<a data-toggle="collapse" <?php if(!empty($_GET['overall']) || !empty($_GET['clean']) || !empty($_GET['services']) || !empty($_GET['food'])
														|| !empty($_GET['comfort']) || !empty($_GET['money'])){ echo "class='akt'";} ?> data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">Restaurant Rating</a>
												</h4>
											</div>
											<div id="collapse2" <?php if(!empty($_GET['overall']) || !empty($_GET['clean']) || !empty($_GET['services']) || !empty($_GET['food'])
												|| !empty($_GET['comfort']) || !empty($_GET['money'])){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="heading2">
												<div class="panel-body">
													<div class="form-group">
														<label>Overall</label>
														<select class="form-control" name="overall">
															<option value="" selected>Select Overate Rate</option>
															<option value="1" <?php if($_GET['overall']==1){ echo "selected";} ?>>1 Star</option>
															<option value="2" <?php if($_GET['overall']==2){ echo "selected";}?>>2 Star</option>
															<option value="3" <?php if($_GET['overall']==3){ echo "selected";}?>>3 Star</option>
															<option value="4" <?php if($_GET['overall']==4){ echo "selected";}?>>4 Star</option>
															<option value="5" <?php if($_GET['overall']==5){ echo "selected";}?>>5 Star</option>
														</select>
													</div>
													<div class="form-group">
														<label>Cleanliness</label>
														<select class="form-control select" name="clean">
															<option value="" selected>Select Rate of Cleanliness</option>
															<option value="0.26" <?php if($_GET['clean']=='0.26'){ echo "selected";}?>>1 Star</option>
															<option value="0.52" <?php if($_GET['clean']=='0.52'){ echo "selected";}?>>2 Star</option>
															<option value="0.78" <?php if($_GET['clean']=='0.78'){ echo "selected";}?>>3 Star</option>
															<option value="1.04" <?php if($_GET['clean']=='1.04'){ echo "selected";}?>>4 Star</option>
															<option value="1.3" <?php if($_GET['clean']=='1.3'){ echo "selected";}?>>5 Star</option>
														</select>
													</div>
													<div class="form-group">
														<label>Customer Services</label>
														<select class="form-control select" name="services">
															<option value="" selected>Select Rate of Customer Services</option>
															<option value="0.24" <?php if($_GET['services']=='0.24'){ echo "selected";}?>>1 Star</option>
															<option value="0.48" <?php if($_GET['services']=='0.48'){ echo "selected";}?>>2 Star</option>
															<option value="0.72" <?php if($_GET['services']=='0.72'){ echo "selected";}?>>3 Star</option>
															<option value="0.96" <?php if($_GET['services']=='0.96'){ echo "selected";}?>>4 Star</option>
															<option value="1.2" <?php if($_GET['services']=='1.2'){ echo "selected";}?>>5 Star</option>
														</select>
													</div>
													<div class="form-group">
														<label>Food &amp; Beverages</label>
														<select class="form-control select" name="food">
															<option value="" selected>Select Rate of Food &amp; Beverages</option>
															<option value="0.23" <?php if($_GET['food']=='0.23'){ echo "selected";}?>>1 Star</option>
															<option value="0.46" <?php if($_GET['food']=='0.46'){ echo "selected";}?>>2 Star</option>
															<option value="0.69" <?php if($_GET['food']=='0.69'){ echo "selected";}?>>3 Star</option>
															<option value="0.92" <?php if($_GET['food']=='0.96'){ echo "selected";}?>>4 Star</option>
															<option value="1.15" <?php if($_GET['food']=='1.15'){ echo "selected";}?>>5 Star</option>
														</select>
													</div>
													<div class="form-group">
														<label>Comfort</label>
														<select class="form-control select" name="comfort">
															<option value="" selected>Select Rate of Comfort</option>
															<option value="0.14" <?php if($_GET['comfort']=='0.14'){ echo "selected";}?>>1 Star</option>
															<option value="0.28" <?php if($_GET['comfort']=='0.28'){ echo "selected";}?>>2 Star</option>
															<option value="0.42" <?php if($_GET['comfort']=='0.42'){ echo "selected";}?>>3 Star</option>
															<option value="0.56" <?php if($_GET['comfort']=='0.56'){ echo "selected";}?>>4 Star</option>
															<option value="0.7" <?php if($_GET['comfort']=='0.7'){ echo "selected";}?>>5 Star</option>
														</select>
													</div>
													<div class="form-group">
														<label>Value for Money</label>
														<select class="form-control select" name="money">
															<option value="" selected>Select Rate of Value for Money</option>
															<option value="0.13" <?php if($_GET['money']=='0.13'){ echo "selected";}?>>1 Star</option>
															<option value="0.26" <?php if($_GET['money']=='0.26'){ echo "selected";}?>>2 Star</option>
															<option value="0.39" <?php if($_GET['money']=='0.39'){ echo "selected";}?>>3 Star</option>
															<option value="0.52" <?php if($_GET['money']=='0.52'){ echo "selected";}?>>4 Star</option>
															<option value="0.65" <?php if($_GET['money']=='0.65'){ echo "selected";}?>>5 Star</option>
														</select>
													</div>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading3">
												<h4 class="panel-title">
													<a data-toggle="collapse" <?php if(!empty($_GET['alcohol']) || !empty($_GET['pork'])){ echo "class='akt'";} ?> data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">Popular Filter</a>
												</h4>
											</div>
											<div id="collapse3" <?php if(!empty($_GET['alcohol']) || !empty($_GET['pork'])){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="heading3">
												<div class="panel-body">
													<div class="radio radio-danger mb10">
														<input type="radio" name="alcohol" <?php if($_GET[alcohol]=='Yes'){echo "checked";}; ?> value="Yes" id="opt1">
														<label for="opt1">Alcohol</label>
													</div>
													<div class="radio radio-danger mb10">
														<input type="radio" name="alcohol" <?php if($_GET[alcohol]=='No'){echo "checked";}; ?> value="No" id="opt2">
														<label for="opt2">No Alcohol</label>
													</div>
													<div class="radio radio-danger mb10">
														<input type="radio" name="pork" <?php if($_GET[pork]=='Yes'){echo "checked";}; ?> value="Yes" id="opt3">
														<label for="opt3">Pork</label>
													</div>
													<div class="radio radio-danger mb10">
														<input type="radio" name="pork" <?php if($_GET[pork]=='No'){echo "checked";}; ?> value="No" id="opt4">
														<label for="opt4">No Pork</label>
													</div>
													<div class="radio radio-danger mb10">
														<input type="radio" name="pork" <?php if($_GET[pork]=='Halal'){echo "checked";}; ?> value="Halal" id="opt5">
														<label for="opt5">Halal</label>
													</div>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading4">
												<h4 class="panel-title">
													<a data-toggle="collapse" <?php if(!empty($_GET['price_index']) || !empty($_GET['suitable_for']) || !empty($_GET['serving_time'])
													|| !empty($_GET['type_of_service']) || !empty($_GET['air_conditioning']) || !empty($_GET['heating_system']) || !empty($_GET['wifi'])
													|| !empty($_GET['term_of_payment']) || !empty($_GET['premise_security']) || !empty($_GET['parking_spaces']) || !empty($_GET['premise_fire_safety'])
													|| !empty($_GET['premise_hygiene']) || !empty($_GET['ambience']) || !empty($_GET['attire']) || !empty($_GET['clean_washroom'])
													|| !empty($_GET['tables_availability']) || !empty($_GET['waiter_tipping'])){ echo "class='akt'";} ?> data-parent="#accordion" href="#collapse4" aria-expanded="true" aria-controls="collapse4">Other Filter</a>
												</h4>
											</div>
											<div id="collapse4" <?php if(!empty($_GET['price_index']) || !empty($_GET['suitable_for']) || !empty($_GET['serving_time'])
											|| !empty($_GET['type_of_service']) || !empty($_GET['air_conditioning']) || !empty($_GET['heating_system']) || !empty($_GET['wifi'])
											|| !empty($_GET['term_of_payment']) || !empty($_GET['premise_security']) || !empty($_GET['parking_spaces']) || !empty($_GET['premise_fire_safety'])
											|| !empty($_GET['premise_hygiene']) || !empty($_GET['ambience']) || !empty($_GET['attire']) || !empty($_GET['clean_washroom'])
											|| !empty($_GET['tables_availability']) || !empty($_GET['waiter_tipping'])){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="heading4">
												<div class="panel-body">
													<div class="form-group">
														<label>Price Index</label>
														<select class="form-control" name="price_index">
															<option value="" selected>Select Price Index</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from price_index order by nama_price_index asc");
															while($g=mysqli_fetch_array($sql)){
																if($g['id_price_index']==$_GET['price_index']){
																	echo"<option value='$g[id_price_index]' selected>$g[nama_price_index]</option>";
																}
																else{
																	echo"<option value='$g[id_price_index]'>$g[nama_price_index]</option>";
																}
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Suitable For</label>
														<select class="form-control" name="suitable_for">
															<option value="" selected>Select Suitable For</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from suitable_for order by nama_suitable_for asc");
															while($h=mysqli_fetch_array($sql)){
																if($h['id_suitable_for']==$_GET['suitable_for']){
																	echo"<option value='$h[id_suitable_for]' selected>$h[nama_suitable_for]</option>";
																}
																else{
																	echo"<option value='$h[id_suitable_for]'>$h[nama_suitable_for]</option>";
																}
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Serving Time</label>
														<select class="form-control" name="serving_time">
															<option value="" selected>Select Serving Time</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from serving_time order by nama_serving_time asc");
															while($k=mysqli_fetch_array($sql)){
																if($k['id_serving_time']==$_GET['serving_time']){
																	echo"<option value='$k[id_serving_time]' selected>$k[nama_serving_time]</option>";
																}
																else{
																	echo"<option value='$k[id_serving_time]'>$k[nama_serving_time]</option>";
																}
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Type of Service</label>
														<select class="form-control" name="type_of_service">
															<option value="" selected>Select Type of Service</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from type_of_service order by nama_type_of_service asc");
															while($l=mysqli_fetch_array($sql)){
																if($l['id_type_of_service']==$_GET['type_of_service']){
																	echo"<option value='$l[id_type_of_service]' selected>$l[nama_type_of_service]</option>";
																}
																else{
																	echo"<option value='$l[id_type_of_service]'>$l[nama_type_of_service]</option>";
																}
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Air Conditioning</label>
														<select class="form-control" name="air_conditioning">
															<option value="" selected>Air Conditioning</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from air_conditioning order by nama_air_conditioning asc");
															while($l1=mysqli_fetch_array($sql)){
																if($l1['id_air_conditioning']==$_GET['air_conditioning']){
																	echo"<option value='$l1[id_air_conditioning]' selected>$l1[nama_air_conditioning]</option>";
																}
																else{
																	echo"<option value='$l1[id_air_conditioning]'>$l1[nama_air_conditioning]</option>";
																}
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Heating System</label>
														<select class="form-control" name="heating_system">
															<option value="" selected>Heating System</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from heating_system order by nama_heating_system asc");
															while($l2=mysqli_fetch_array($sql)){
																if($l2['id_heating_system']==$_GET['heating_system']){
																	echo"<option value='$l2[id_heating_system]' selected>$l2[nama_heating_system]</option>";
																}
																else{
																	echo"<option value='$l2[id_heating_system]'>$l2[nama_heating_system]</option>";
																}
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Wi-Fi</label>
														<select class="form-control" name="wifi">
															<option value="" selected>Select Wi-Fi</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from wifi order by nama_wifi asc");
															while($n=mysqli_fetch_array($sql)){
																if($n['id_wifi']==$_GET['wifi']){
																	echo"<option value='$n[id_wifi]' selected>$n[nama_wifi]</option>";
																}
																else{
																	echo"<option value='$n[id_wifi]'>$n[nama_wifi]</option>";
																}
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Payments</label>
														<select class="form-control" name="term_of_payment">
															<option value="" selected>Select Payments</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from term_of_payment order by nama_term_of_payment asc");
															while($o=mysqli_fetch_array($sql)){
																if($o['id_term_of_payment']==$_GET['term_of_payment']){
																	echo"<option value='$o[id_term_of_payment]' selected>$o[nama_term_of_payment]</option>";
																}
																else{
																	echo"<option value='$o[id_term_of_payment]'>$o[nama_term_of_payment]</option>";
																}
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Premise's Security</label>
														<select class="form-control" name="premise_security">
															<option value="" selected>Select Premise Security</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from premise_security order by nama_premise_security asc");
															while($p=mysqli_fetch_array($sql)){
																if($p['id_premise_security']==$_GET['premise_security']){
																	echo"<option value='$p[id_premise_security]' selected>$p[nama_premise_security]</option>";
																}
																else{
																	echo"<option value='$p[id_premise_security]'>$p[nama_premise_security]</option>";
																}
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Parking Spaces</label>
														<select class="form-control" name="parking_spaces">
															<option value="" selected>Select Parking Spaces</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from parking_spaces order by nama_parking_spaces asc");
															while($q=mysqli_fetch_array($sql)){
																if($q['id_parking_spaces']==$_GET['parking_spaces']){
																	echo"<option value='$q[id_parking_spaces]' selected>$q[nama_parking_spaces]</option>";
																}
																else{
																	echo"<option value='$q[id_parking_spaces]'>$q[nama_parking_spaces]</option>";
																}
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Premise's Fire Safety </label>
														<select class="form-control" name="premise_fire_safety">
															<option value="" selected>Select Premise's Fire Safety</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from premise_fire_safety order by nama_premise_fire_safety asc");
															while($r=mysqli_fetch_array($sql)){
																if($r['id_premise_fire_safety']==$_GET['premise_fire_safety']){
																	echo"<option value='$r[id_premise_fire_safety]' selected>$r[nama_premise_fire_safety]</option>";
																}
																else{
																	echo"<option value='$r[id_premise_fire_safety]'>$r[nama_premise_fire_safety]</option>";
																}
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Premise's Hygiene</label>
														<select class="form-control" name="premise_hygiene">
															<option value="" selected>Select Premise Hygiene</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from premise_hygiene order by nama_premise_hygiene asc");
															while($r1=mysqli_fetch_array($sql)){
																if($r1['id_premise_hygiene']==$_GET['premise_hygiene']){
																	echo"<option value='$r1[id_premise_hygiene]' selected>$r1[nama_premise_hygiene]</option>";
																}
																else{
																	echo"<option value='$r1[id_premise_hygiene]'>$r1[nama_premise_hygiene]</option>";
																}
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Premise's Maintenance</label>
														<select class="form-control" name="premise_maintenance">
															<option value="" selected>Select Premise Maintenance</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from premise_maintenance order by nama_premise_maintenance asc");
															while($s=mysqli_fetch_array($sql)){
																if($s['id_premise_maintenance']==$_GET['premise_maintenance']){
																	echo"<option value='$s[id_premise_maintenance]' selected>$s[nama_premise_maintenance]</option>";
																}
																else{
																	echo"<option value='$s[id_premise_maintenance]'>$s[nama_premise_maintenance]</option>";
																}
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Ambience</label>
														<select class="form-control" name="ambience">
															<option value="" selected>Select Ambience</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from ambience order by nama_ambience asc");
															while($t=mysqli_fetch_array($sql)){
																if($t['id_ambience']==$_GET['ambience']){
																	echo"<option value='$t[id_ambience]' selected>$t[nama_ambience]</option>";
																}
																else{
																	echo"<option value='$t[id_ambience]'>$t[nama_ambience]</option>";
																}
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Attire</label>
														<select class="form-control" name="attire">
															<option value="" selected>Select Attire</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from attire order by nama_attire asc");
															while($u=mysqli_fetch_array($sql)){
																if($u['id_attire']==$_GET['attire']){
																	echo"<option value='$u[id_attire]' selected>$u[nama_attire]</option>";
																}
																else{
																	echo"<option value='$u[id_attire]'>$u[nama_attire]</option>";
																}
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Clean Washroom</label>
														<select class="form-control" name="clean_washroom">
															<option value="" selected>Select Clean Washroom</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from clean_washroom order by nama_clean_washroom asc");
															while($v=mysqli_fetch_array($sql)){
																if($v['id_clean_washroom']==$_GET['clean_washroom']){
																	echo"<option value='$v[id_clean_washroom]' selected>$v[nama_clean_washroom]</option>";
																}
																else{
																	echo"<option value='$v[id_clean_washroom]'>$v[nama_clean_washroom]</option>";
																}
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Tables Availability</label>
														<select class="form-control" name="tables_availability">
															<option value="" selected>Select Tables Availability</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from tables_availability order by nama_tables_availability asc");
															while($w=mysqli_fetch_array($sql)){
																if($w['id_tables_availability']==$_GET['tables_availability']){
																	echo"<option value='$w[id_tables_availability]' selected>$w[nama_tables_availability]</option>";
																}
																else{
																	echo"<option value='$w[id_tables_availability]'>$w[nama_tables_availability]</option>";
																}
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Noise Level</label>
														<select class="form-control" name="noise_level">
															<option value="" selected>Select Noise Level</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from noise_level order by nama_noise_level asc");
															while($x=mysqli_fetch_array($sql)){

																if($x['id_noise_level']==$_GET['noise_level']){
																	echo"<option value='$x[id_noise_level]' selected>$x[nama_noise_level]</option>";
																}
																else{
																	echo"<option value='$x[id_noise_level]'>$x[nama_noise_level]</option>";
																}
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Waiter Tipping</label>
														<select class="form-control" name="waiter_tipping">
															<option value="" selected>Select Waiter Tipping</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from waiter_tipping order by nama_waiter_tipping asc");
															while($y=mysqli_fetch_array($sql)){
																if($y['id_waiter_tipping']==$_GET['waiter_tipping']){
																	echo"<option value='$y[id_waiter_tipping]' selected>$y[nama_waiter_tipping]</option>";
																}
																else{
																	echo"<option value='$y[id_waiter_tipping]'>$y[nama_waiter_tipping]</option>";
																}
															}
															?>
														</select>
													</div>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading6">
												<h4 class="panel-title">
													<a data-toggle="collapse" <?php if(!empty($_GET['facility'])){ echo "class='akt'";} ?> data-parent="#accordion" href="#collapse6" aria-expanded="true" aria-controls="collapse6">Features and Facility</a>
												</h4>
											</div>
											<div id="collapse6" <?php if(!empty($_GET['facility'])){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="heading6">
												<div class="panel-body">
													<?php
														$fac='';
														foreach($_GET['facility'] as $facility){
															$fac.=$facility."+";
														}
														$facility=tulis_cekboxxer($fac,$koneksi,'facility');
														echo"$facility";
													?>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading7">
												<h4 class="panel-title">
													<a data-toggle="collapse" <?php if(!empty($_GET['serving'])){ echo "class='akt'";} ?> data-parent="#accordion" href="#collapse7" aria-expanded="true" aria-controls="collapse7">Serving</a>
												</h4>
											</div>
											<div id="collapse7" <?php if(!empty($_GET['serving'])){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="heading7">
												<div class="panel-body">
													<?php
														$ser='';
														foreach($_GET['serving'] as $serving){
															$ser.=$serving."+";
														}
														$serving=tulis_cekboxxer($ser,$koneksi,'serving');
														echo"$serving";
													?>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading8">
												<h4 class="panel-title">
													<a data-toggle="collapse" <?php if(!empty($_GET['type_of_serving'])){ echo "class='akt'";} ?> data-parent="#accordion" href="#collapse8" aria-expanded="true" aria-controls="collapse8">Type of Serving</a>
												</h4>
											</div>
											<div id="collapse8" <?php if(!empty($_GET['type_of_serving'])){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="heading8">
												<div class="panel-body">
													<?php
													$$toss='';
													foreach($_GET['type_of_serving'] as $tos){
														$toss.=$tos."+";
													}
													$type_of_serving=tulis_cekboxxer($toss,$koneksi,'type_of_serving');
													echo"$type_of_serving";
													?>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading8tag">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" <?php if(!empty($_GET['tag'])){ echo "class='akt'";} ?> href="#collapse8tag" aria-expanded="true" aria-controls="collapse8tag">Tag</a>
												</h4>
											</div>
											<div id="collapse8tag"<?php if(!empty($_GET['tag'])){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="heading8tag">
												<div class="panel-body">
													<div class="form-group">
														<div class="input-group">
																<input class="form-control tag" name="tag" value="<?php echo"$_GET[tag]";?>">
																<div class="input-group-addon">x</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<button type="submit" class="btn btn-success btn-block">Search</button>
										<a href="https://www.foodieguidances.com/pages/restaurant/search/" class="btn btn-warning btn-block">Reset</a>
									</form>
								</div>
							</div>
						</div>
					</div>


					<?php include "config/func/search_food.php"; ?>

					<?php include "config/func/search_beverage.php"; ?>

					<?php include 'config/inc/search_process.php' ?>										
					<?php include 'config/inc/search_events.php' ?>

					<div class="panel-groupmb10 mt10" id="accordionsssasu" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default head-pan">
							<div class="panel-heading jud-pan" role="tab" id="recipe1" data-toggle="collapse" data-parent="#accordionsssasu" href="#collapsesssasu1" aria-expanded="true" aria-controls="collapsesssasu1">
								<h4 class="panel-title">
									❯ Recipe
								</h4>
							</div>
							<div id="collapsesssasu1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="recipe1">
								<div class="panel-body">
									<form method="get" action="<?php echo"$base_url/pages/recipe/search/"; ?>" class="border-form" id="ale-form" onsubmit="myFunction()">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a href="<?php echo"$base_url" ?>/pages/recipe/search/?sort=r.dilihat+DESC&amp;batas=25">Popular</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a href="<?php echo"$base_url" ?>/pages/recipe/search/?sort=direkomendasi+DESC&amp;batas=25">Recomended</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="recipe1">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapsesssasug1" aria-expanded="true" aria-controls="collapsesssasu1">Advance Filter</a>
												</h4>
											</div>
											<div id="collapsesssasug1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="recipe1">
												<div class="panel-body">
													<div class="form-group">
														<label>Category</label>
														<select class="form-control" name="category">
															<option value="" selected>Select Category</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from recipe_category order by nama_recipe_category asc");
															while($b=mysqli_fetch_array($sql)){
																echo"<option value='$b[id_recipe_category]'>$b[nama_recipe_category]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Cuisine</label>
														<select class="form-control" name="cuisine">
															<option value="" selected>Select Cuisine</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from cuisine order by nama_cuisine asc");
															while($c=mysqli_fetch_array($sql)){
																echo"<option value='$c[id_cuisine]'>$c[nama_cuisine]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>MSG Level</label>
														<select class="form-control" name="msg_level">
															<option value="" selected>Select MSG Level</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from msg_level order by nama_msg_level asc");
															while($e=mysqli_fetch_array($sql)){
																echo"<option value='$e[id_msg_level]'>$e[nama_msg_level]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Difficulty</label>
														<select class="form-control" name="difficulty">
															<option value="" selected>Select Difficulty</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from difficulty order by nama_difficulty asc");
															while($d=mysqli_fetch_array($sql)){
																echo"<option value='$d[id_difficulty]'>$d[nama_difficulty]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Cooking Methode</label>
														<select class="form-control" name="cooking_methode">
															<option value="" selected>Select Cooking Methode</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from cooking_methode order by nama_cooking_methode asc");
															while($a=mysqli_fetch_array($sql)){
																echo"<option value='$a[id_cooking_methode]'>$a[nama_cooking_methode]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Duration</label>
														<select class="form-control" name="duration">
															<option value="" selected>Select Duration</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from duration order by nama_duration asc");
															while($f=mysqli_fetch_array($sql)){
																echo"<option value='$f[id_duration]'>$f[nama_duration]</option>";
															}
															?>
														</select>
													</div>
												</div>
											</div>
										</div>
										<button type="submit" class="btn btn-success btn-block">Search</button>
										<a href="https://www.foodieguidances.com/pages/restaurant/search/" class="btn btn-warning btn-block">Reset</a>
									</form>
								</div>
							</div>
						</div>
					</div>

					<div class="panel-groupmb10 mt10" id="accordionsssasulk" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default head-pan">
							<div class="panel-heading jud-pan" role="tab" id="fgmart1" data-toggle="collapse" data-parent="#accordionsssasulk" href="#collapsesssasulk1" aria-expanded="true" aria-controls="collapsesssasulk1">
								<h4 class="panel-title">
									❯ FGMART
								</h4>
							</div>
							<div id="collapsesssasulk1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="fgmart1">
								<form method="get" action="<?php echo"$base_url/pages/fgmart/search/"; ?>" class="border-form" id="ale-form" onsubmit="myFunction()">
									<div class="panel-body">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a href="<?php echo"$base_url" ?>/pages/fgmart/search/?sort=r.dilihat+DESC&amp;batas=25">Popular</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a href="<?php echo"$base_url" ?>/pages/fgmart/search/?sort=dilike+DESC&amp;batas=25">Liked</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="fgmart1">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordionsssasulks" href="#collapsesssasulks1" aria-expanded="true" aria-controls="collapse1">Advanced Filter</a>
												</h4>
											</div>
											<div id="collapsesssasulks1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="fgmart1">
												<div class="panel-body">
													<div class="form-group">
														<label>Category</label>
														<select class="form-control" name="category">
															<option value="" selected>Select Category</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from photo_category order by nama_photo_category asc");
															while($b=mysqli_fetch_array($sql)){
																echo"<option value='$b[id_photo_category]'>$b[nama_photo_category]</option>";
															}
															?>
														</select>
													</div>
												</div>
											</div>
										</div>
										<button type="submit" class="btn btn-success btn-block">Search</button>
										<a href="https://www.foodieguidances.com/pages/restaurant/search/" class="btn btn-warning btn-block">Reset</a>
									</div>
								</form>
							</div>
						</div>
					</div>

					<div class="panel-groupmb10 mt10" id="accordionsssasul" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default head-pan">
							<div class="panel-heading jud-pan" role="tab" id="coupon1" data-toggle="collapse" data-parent="#accordionsssasul" href="#collapsesssasul1" aria-expanded="true" aria-controls="collapsesssasul1">
								<h4 class="panel-title">
									❯ Coupon
								</h4>
							</div>
							<div id="collapsesssasul1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="coupon1">
								<form method="get" action="<?php echo"$base_url/pages/coupon/search/"; ?>" class="border-form" id="ale-form" onsubmit="myFunction()">
									<div class="panel-body">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a href="<?php echo"$base_url" ?>/pages/coupon/search/?sort=r.dilihat+DESC&amp;batas=25">Popular</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapsesssasula14" aria-expanded="true" aria-controls="collapse1">Coupon Title</a>
												</h4>
											</div>
											<div id="collapsesssasula14" class="panel-collapse collapse" role="tabpanel" aria-labelledby="coupon1">
												<div class="panel-body">
													<div class="form-group">
														<label>Coupon Title</label>
														<input class="form-control select" name="keyword" id="search">
													</div>
												</div>
											</div>
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a href="<?php echo"$base_url" ?>/pages/coupon/search/?sort=dilike+DESC&amp;batas=25">Liked</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapsesssasula1" aria-expanded="true" aria-controls="collapse1">Location Filter</a>
												</h4>
											</div>
											<div id="collapsesssasula1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="coupon1">
												<div class="panel-body">
													<div class="form-group">
														<label>Country</label>
														<select class="form-control" name="country" id="negara5">
															<option value="" selected>Select Country</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from negara order by nama_negara asc");
															while($b=mysqli_fetch_array($sql)){
																echo"<option value='$b[id_negara]'>$b[nama_negara]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>States/Province</label>
														<select class="form-control" name="state" id="propinsi5">
															<option value="" selected>Select States/ Province</option>
														</select>
													</div>
													<div class="form-group">
														<label>City</label>
														<select class="form-control" name="city" id="kota5">
															<option value="" selected>Select City</option>
														</select>
													</div>

												</div>
											</div>
										</div>
										<button type="submit" class="btn btn-success btn-block">Search</button>
										<a href="https://www.foodieguidances.com/pages/restaurant/search/" class="btn btn-warning btn-block">Reset</a>
									</div>
								</form>
							</div>
						</div>
					</div>

					<div class="panel-groupmb10 mt10" id="accordionsssasulkp" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default head-pan">
							<div class="panel-heading jud-pan" role="tab" id="artikel1" data-toggle="collapse" data-parent="#accordionsssasulkp" href="#collapsesssasulkp1" aria-expanded="true" aria-controls="collapsesssasulkp1">
								<h4 class="panel-title">
									❯ Article
								</h4>
							</div>
							<div id="collapsesssasulkp1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="artikel1">
								<form method="get" action="<?php echo"$base_url/pages/article/search/"; ?>" class="border-form" id="ale-form" onsubmit="myFunction()">
									<div class="panel-body">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="artikel1">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordionsssasulkpg1" href="#collapsesssasulkpa1" aria-expanded="true" aria-controls="collapse1">Advanced Filter</a>
												</h4>
											</div>
											<div id="collapsesssasulkpa1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="artikel1">
												<div class="panel-body">
													<div class="form-group">
														<label>Article Title</label>
														<input class="form-control" name="keyword" id="search">
													</div>
												</div>
											</div>
										</div>
										<button type="submit" class="btn btn-success btn-block">Search</button>
										<a href="https://www.foodieguidances.com/pages/restaurant/search/" class="btn btn-warning btn-block">Reset</a>
									</div>
								</form>
							</div>
						</div>
					</div>

					<div class="panel-groupmb10 mt10" id="accordionsssasulkpw" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default head-pan">
							<div class="panel-heading jud-pan" role="tab" id="video1" data-toggle="collapse" data-parent="#accordionsssasulkpw" href="#collapsesssasulkpw1" aria-expanded="true" aria-controls="collapsesssasulkpw1">
								<h4 class="panel-title">
									❯ Video
								</h4>
							</div>
							<div id="collapsesssasulkpw1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="video1">
								<form method="get" action="<?php echo"$base_url/pages/video/search/"; ?>" class="border-form" id="ale-form" onsubmit="myFunction()">
									<div class="panel-body">
										<div class="panel panel-default">
											<div class="panel panel-default">
												<div class="panel-heading" role="tab" id="video1">
													<h4 class="panel-title">
														<a data-toggle="collapse" data-parent="#accordionsssasulkpw1" href="#collapsesssasulkpwj1" aria-expanded="true" aria-controls="collapse1">Advanced Filter</a>
													</h4>
												</div>
												<div id="collapsesssasulkpwj1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="video1">
													<div class="panel-body">
														<div class="form-group">
															<label>Video Title</label>
															<input class="form-control select" name="keyword" id="search">
														</div>
														<div class="form-group">
															<label>Category</label>
															<select class="form-control" name="category">
																<option value="" selected>Select Category</option>
																<?php
																$sql=mysqli_query($koneksi,"select * from video_category order by nama_video_category asc");
																while($d=mysqli_fetch_array($sql)){
																	echo"<option value='$d[id_video_category]'>$d[nama_video_category]</option>";
																}
																?>
															</select>
														</div>
													</div>
												</div>
											</div>
										</div>
										<button type="submit" class="btn btn-success btn-block">Search</button>
										<a href="https://www.foodieguidances.com/pages/restaurant/search/" class="btn btn-warning btn-block">Reset</a>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-8 col-8a no-padding">
				<h4 class="f-merah no-mb" style="">
					<?php
					$kondisi=" WHERE r.id_restaurant <> 0";
					if(!empty($_GET['keyword'])){
						$kondisi .= " AND r.restaurant_name = '$_GET[keyword]' OR tag LIKE '%$_GET[keyword]%' OR t.nama_type_of_business = '$_GET[keyword]' OR n.nama_negara = '$_GET[keyword]' OR o.nama_propinsi = '$_GET[keyword]' OR k.nama_kota = '$_GET[keyword]'";
					}
					if(!empty($_GET['tag'])){
						$kondisi .= " AND r.tag like '%$_GET[tag]%'";
					}
					if(!empty($_GET['country'])){
						$kondisi .= " AND r.id_negara = '$_GET[country]'";
					}
					if(!empty($_GET['state'])){
						$kondisi .= " AND r.id_propinsi = '$_GET[state]'";
					}
					if(!empty($_GET['city'])){
						$kondisi .= " AND r.id_kota = '$_GET[city]'";
					}
					if(!empty($_GET['landmark'])){
						$kondisi .= " AND r.id_landmark = '$_GET[landmark]'";
					}
					if(!empty($_GET['mall'])){
						$kondisi .= " AND id_mall = '$_GET[mall]'";
					}
					if(!empty($_GET['business_type'])){
						$kondisi .= " AND r.id_type_of_business = '$_GET[business_type]'";
					}
					if(!empty($_GET['cuisine'])){
						$curr='';
						foreach($_GET['cuisine'] as $currency){
							$curr.=$currency."+";
						}
						$cusss=rtrim($curr, "+");
						$kondisi .= " AND cuisine like '%$cusss%'";
					}
					if(!empty($_GET['overall'])){
						$kondisi .= " AND (SELECT (SUM(f.cleanlines) + SUM(f.customer_services) + SUM(f.food_beverage) + SUM(f.comfort) + SUM(f.value_money)) / COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant =r.id_restaurant) >= '$_GET[overall]'";
					}
					else{
						if(!empty($_GET['clean'])){
							$kondisi .= " AND (SELECT SUM(cleanlines)/ COUNT(id_member) AS total FROM restaurant_rating where restaurant_rating.id_restaurant=r.id_restaurant) >= '$_GET[clean]'";
						}
						if(!empty($_GET['service'])){
							$kondisi .= " AND (SELECT SUM(customer_services)/ COUNT(id_member) AS total FROM restaurant_rating where restaurant_rating.id_restaurant=r.id_restaurant) >= '$_GET[service]'";
						}
						if(!empty($_GET['food'])){
							$kondisi .= " AND (SELECT SUM(food_beverage)/ COUNT(id_member) AS total FROM restaurant_rating where restaurant_rating.id_restaurant=r.id_restaurant) >= '$_GET[food]'";
						}
						if(!empty($_GET['comfort'])){
							$kondisi .= " AND (SELECT SUM(comfort)/ COUNT(id_member) AS total FROM restaurant_rating where restaurant_rating.id_restaurant=r.id_restaurant) >= '$_GET[comfort]'";
						}
						if(!empty($_GET['money'])){
							$kondisi .= " AND (SELECT SUM(value_money)/ COUNT(id_member) AS total FROM restaurant_rating where restaurant_rating.id_restaurant=r.id_restaurant) >= '$_GET[money]'";
						}
					}
					if(!empty($_GET['alcohol'])){
						$kondisi .= " AND r.alcohol_serving = '$_GET[alcohol]'";
					}
					if(!empty($_GET['pork'])){
						$kondisi .= " AND r.pork_serving = '$_GET[pork]'";
					}
					if(!empty($_GET['price_index'])){
						$kondisi .= " AND r.id_price_index = '$_GET[price_index]'";
					}
					if(!empty($_GET['suitable_for'])){
						$kondisi .= " AND r.id_suitable_for = '$_GET[suitable_for]'";
					}
					if(!empty($_GET['serving'])){

							$kondisi .= " AND serving like '%$_GET[serving]%'";

					}
					if(!empty($_GET['type_of_serving'])){
						$curr='';
						foreach($_GET['type_of_serving'] as $currency){
							$curr.=$currency."+";
						}
						$cusss=rtrim($curr, "+");
						$kondisi .= " AND type_of_serving like '%$cusss%'";
					}
					if(!empty($_GET['opening'])){
						date_default_timezone_set("Asia/Jakarta");
						$jam = date("H:i:s");
						if($_GET['opening']=='open'){
							$kondisi .= " AND TIME_to_sec(buka) < time_to_sec('$jam') and time_to_sec('$jam') < time_to_sec(tutup)";
						}
						elseif($_GET['opening']=='no'){
							$kondisi .= " TIME_to_sec(buka) = time_to_sec('00:00:00') and time_to_sec('00:00:00') = time_to_sec(tutup)";
						}
						else {
							if($jam >= '01:00:00' and $jam <= '12:00:00'){
								$kondisi .= " AND TIME_to_sec(buka) > time_to_sec('$jam') and time_to_sec('$jam') < time_to_sec(tutup) and buka <> '00:00:00'";
							}
							if($jam >= '12:00:00' and $jam <= '24:00:00'){
								$kondisi .= " AND TIME_to_sec(buka) < time_to_sec('$jam') and time_to_sec('$jam') > time_to_sec(tutup) and buka <> '00:00:00'";
							}
						}
					}
					if(!empty($_GET['serving_time'])){
						$kondisi .= " AND r.id_serving_time = '$_GET[serving_time]'";
					}
					if(!empty($_GET['type_of_service'])){
						$kondisi .= " AND r.id_type_of_service = '$_GET[type_of_service]'";
					}
					if(!empty($_GET['air_conditioning'])){
						$kondisi .= " AND r.id_air_conditioning = '$_GET[air_conditioning]'";
					}
					if(!empty($_GET['heating_system'])){
						$kondisi .= " AND r.id_heating_system = '$_GET[heating_system]'";
					}
					if(!empty($_GET['facility'])){
						$fac='';
						foreach($_GET['facility'] as $facility){
							$fac.=$facility."+";
						}
						$facss=rtrim($fac, "+");
						$kondisi .= " AND facility like '%$facss%'";
					}
					if(!empty($_GET['wifi'])){
						$kondisi .= " AND r.id_wifi = '$_GET[wifi]'";
					}
					if(!empty($_GET['term_of_payment'])){
						$kondisi .= " AND r.id_term_of_payment = '$_GET[term_of_payment]'";
					}
					if(!empty($_GET['premise_security'])){
						$kondisi .= " AND r.id_premise_security = '$_GET[premise_security]'";
					}
					if(!empty($_GET['parking_spaces'])){
						$kondisi .= " AND r.id_parking_spaces = '$_GET[parking_spaces]'";
					}
					if(!empty($_GET['premise_fire_safety'])){
						$kondisi .= " AND r.id_premise_fire_safety = '$_GET[premise_fire_safety]'";
					}
					if(!empty($_GET['premise_hygiene'])){
						$kondisi .= " AND r.id_premise_hygiene = '$_GET[premise_hygiene]'";
					}
					if(!empty($_GET['premise_maintenance'])){
						$kondisi .= " AND r.id_premise_maintenance = '$_GET[premise_maintenance]'";
					}
					if(!empty($_GET['ambience'])){
						$kondisi .= " AND r.id_ambience = '$_GET[ambience]'";
					}
					if(!empty($_GET['attire'])){
						$kondisi .= " AND r.id_attire = '$_GET[attire]'";
					}
					if(!empty($_GET['clean_washroom'])){
						$kondisi .= " AND r.id_clean_washroom = '$_GET[clean_washroom]'";
					}
					if(!empty($_GET['tables_availability'])){
						$kondisi .= " AND r.id_tables_availability = '$_GET[tables_availability]'";
					}
					if(!empty($_GET['noise_level'])){
						$kondisi .= " AND r.id_noise_level = '$_GET[noise_level]'";
					}
					if(!empty($_GET['waiter_tipping'])){
						$kondisi .= " AND r.id_waiter_tipping = '$_GET[waiter_tipping]'";
					}
					$p      = new Paging;
					$batas  = 25;
					if(isset($_GET['batas'])){
						$batas  = $_GET['batas'];
					}



					if($_GET['sort']=="total_vote DESC") {
						$sort =  "total_vote DESC,jumlah_vote DESC,jumlah_like DESC,r.restaurant_name ASC";
					}
					elseif (empty($_GET['sort'])) {
						$sort="r.id_restaurant DESC";
					}
					elseif ($_GET['sort']=='direkomendasi') {
						$sort="direkomendasi DESC and direkomendasi <> 0";
					}
					else {
						$sort  = $_GET['sort'];
					}

					$sqla="SELECT *,(select p.gambar_restaurant_photo from restaurant_photo p where p.id_restaurant=r.id_restaurant order by p.id_restaurant_photo desc limit 1) as gambar,
					(SELECT COUNT(h.id_member) FROM restaurant_like h WHERE h.id_restaurant=r.id_restaurant) AS jumlah_like,
					(SELECT COUNT(h.id_member) FROM restaurant_rekomendasi h WHERE h.id_restaurant=r.id_restaurant) AS direkomendasi,
					(SELECT COUNT(id_restaurant_here) AS jumlah FROM restaurant_here WHERE restaurant_here.id_restaurant=r.id_restaurant) AS jumlah_disini,
					(SELECT COUNT(id_restaurant_rating) AS jumlah from restaurant_rating WHERE restaurant_rating.id_restaurant=r.id_restaurant) AS jumlah_vote,
					(SELECT (SUM(cleanlines) + SUM(customer_services) + SUM(food_beverage) + SUM(comfort) + SUM(value_money)) / COUNT(id_member) AS total FROM restaurant_rating WHERE restaurant_rating.id_restaurant=r.id_restaurant) AS total_vote FROM restaurant r
					LEFT JOIN negara n ON r.id_negara = n.id_negara
					LEFT JOIN propinsi o ON r.id_propinsi = o.id_propinsi
					LEFT JOIN kota k ON r.id_kota = k.id_kota LEFT JOIN price_index p ON r.id_price_index = p.id_price_index
					LEFT JOIN type_of_business t ON r.id_type_of_business = t.id_type_of_business".$kondisi." ORDER BY ".$sort."";
						$latesta=mysqli_query($koneksi,$sqla);
						$la=mysqli_fetch_array($latesta);
						echo"<div class='breadcrumba flat ml10-j'>";
							if (!empty($_GET['country'])) {
								echo "<a href='#'>$la[nama_negara]</a> ";
							}
							if (!empty($_GET['state'])) {
								echo "<a href='#'>$la[nama_propinsi] </a> ";
							}
							if (!empty($_GET['city'])) {
								echo "<a href='#'>$la[nama_kota] </a> ";
							}
							if ($_GET['sort']=='r.dilihat DESC') {
								echo "<a href='#'>Popular </a>";
							}
							if ($_GET['sort']=='direkomendasi DESC') {
								echo "<a href='#'>Recomended </a>";
							}
							if (!empty($_GET['opening'])) {
								echo "<a href='#'>$_GET[opening] </a>";
							}
							if ($_GET['sort']=='total_vote DESC') {
								echo "<a href='#'>Top Ranking </a>";
							}
							if (!empty($_GET['mall'])) {
								echo "<a href='#'>$_GET[mall] </a>";
							}

							if (!empty($_GET['landmark'])) {
								$sql=mysqli_query($koneksi,"select * from landmark where id_landmark='$_GET[landmark]' order by nama_landmark asc");
								while($d=mysqli_fetch_array($sql)){
									echo"<a href='#'>$d[nama_landmark] </a>";
								}
							}
							if (!empty($_GET['business_type'])) {
								echo "<a href='#'>$la[nama_type_of_business] </a>";
							}
							if (!empty($_GET['tag'])) {
								echo "<a href='#'>Tag : $_GET[tag] </a> ";
							}
							if (!empty($_GET['cuisine'])) {
								$currn=0;
								$currs='';
								foreach($_GET['cuisine'] as $currencys){
									$currn++;
									$currs.=$currencys;
								}
								if ($currn==1) {
									echo "<a href='#'>$currs</a>";
								}
								else {
									echo "<a href='#'>Multiple cuisine</a>";
								}
							}
							if (!empty($_GET['overall']) || !empty($_GET['clean']) || !empty($_GET['services']) || !empty($_GET['food']) || !empty($_GET['comfort']) || !empty($_GET['money'])) {
								echo "<a href='#'>Restaurant Rating / </a>";
							}
							if ($_GET['alcohol']=='Yes') {
								echo "<a href='#'>Alcohol </a>";
							}
							if ($_GET['alcohol']=='No') {
								echo "<a href='#'>Non Alcohol </a>";
							}
							if ($_GET['pork']=='Yes') {
								echo "<a href='#'>Pork </a>";
							}
							if ($_GET['pork']=='No') {
								echo "<a href='#'>Non Pork</a> ";
							}
							if ($_GET['pork']=='Halal') {
								echo "<a href='#'>Halal </a>";
							}
							if (!empty($_GET['price_index'])) {
								$sql=mysqli_query($koneksi,"select * from price_index where id_price_index='$_GET[price_index]'");
								while($d=mysqli_fetch_array($sql)){
									echo"<a href='#'>$d[nama_price_index]</a>";
								}
							}
							if (!empty($_GET['suitable_for'])) {
								$sql=mysqli_query($koneksi,"select * from suitable_for where id_suitable_for='$_GET[suitable_for]'");
								while($d=mysqli_fetch_array($sql)){
									echo"<a href='#'>$d[nama_suitable_for]</a>";
								}
							}
							if (!empty($_GET['serving_time'])) {
								$sql=mysqli_query($koneksi,"select * from serving_time where id_serving_time='$_GET[serving_time]'");
								while($d=mysqli_fetch_array($sql)){
									echo"<a href='#'>$d[nama_serving_time]</a>";
								}
							}
							if (!empty($_GET['type_of_service'])) {
								$sql=mysqli_query($koneksi,"select * from type_of_service where id_type_of_service='$_GET[type_of_service]'");
								while($d=mysqli_fetch_array($sql)){
									echo"<a href='#'>$d[nama_type_of_service]</a>";
								}
							}
							if (!empty($_GET['air_conditioning'])) {
								$sql=mysqli_query($koneksi,"select * from air_conditioning where id_air_conditioning='$_GET[air_conditioning]'");
								while($d=mysqli_fetch_array($sql)){
									echo"<a href='#'>$d[nama_air_conditioning]</a>";
								}
							}
							if (!empty($_GET['heating_system'])) {
								$sql=mysqli_query($koneksi,"select * from heating_system where id_heating_system='$_GET[heating_system]'");
								while($d=mysqli_fetch_array($sql)){
									echo"<a href='#'>$d[nama_heating_system]</a>";
								}
							}
							if (!empty($_GET['term_of_payment'])) {
								$sql=mysqli_query($koneksi,"select * from term_of_payment where id_term_of_payment='$_GET[term_of_payment]'");
								while($d=mysqli_fetch_array($sql)){
									echo"<a href='#'>$d[nama_term_of_payment]</a>";
								}
							}
							if (!empty($_GET['premise_security'])) {
								$sql=mysqli_query($koneksi,"select * from premise_security where id_premise_security='$_GET[premise_security]'");
								while($d=mysqli_fetch_array($sql)){
									echo"<a href='#'>$d[nama_premise_security]</a>";
								}
							}
							if (!empty($_GET['parking_spaces'])) {
								$sql=mysqli_query($koneksi,"select * from parking_spaces where id_parking_spaces='$_GET[parking_spaces]'");
								while($d=mysqli_fetch_array($sql)){
									echo"<a href='#'>$d[nama_parking_spaces]</a>";
								}
							}
							if (!empty($_GET['premise_fire_safety'])) {
								$sql=mysqli_query($koneksi,"select * from premise_fire_safety where id_premise_fire_safety='$_GET[premise_fire_safety]'");
								while($d=mysqli_fetch_array($sql)){
									echo"<a href='#'>$d[nama_premise_fire_safety]</a>";
								}
							}
							if (!empty($_GET['premise_hygiene'])) {
								$sql=mysqli_query($koneksi,"select * from premise_hygiene where id_premise_hygiene='$_GET[premise_hygiene]'");
								while($d=mysqli_fetch_array($sql)){
									echo"<a href='#'>$d[nama_premise_hygiene]</a>";
								}
							}
							if (!empty($_GET['premise_maintenance'])) {
								$sql=mysqli_query($koneksi,"select * from premise_maintenance where id_premise_maintenance='$_GET[premise_maintenance]'");
								while($d=mysqli_fetch_array($sql)){
									echo"<a href='#'>$d[nama_premise_maintenance]</a>";
								}
							}
							if (!empty($_GET['ambience'])) {
								$sql=mysqli_query($koneksi,"select * from ambience where id_ambience='$_GET[ambience]'");
								while($d=mysqli_fetch_array($sql)){
									echo"<a href='#'>$d[nama_ambience]</a>";
								}
							}
							if (!empty($_GET['attire'])) {
								$sql=mysqli_query($koneksi,"select * from attire where id_attire='$_GET[attire]'");
								while($d=mysqli_fetch_array($sql)){
									echo"<a href='#'>$d[nama_attire]</a>";
								}
							}
							if (!empty($_GET['clean_washroom'])) {
								$sql=mysqli_query($koneksi,"select * from clean_washroom where id_clean_washroom='$_GET[clean_washroom]'");
								while($d=mysqli_fetch_array($sql)){
									echo"<a href='#'>$d[nama_clean_washroom]</a>";
								}
							}
							if (!empty($_GET['tables_availability'])) {
								$sql=mysqli_query($koneksi,"select * from tables_availability where id_tables_availability='$_GET[tables_availability]'");
								while($d=mysqli_fetch_array($sql)){
									echo"<a href='#'>d[nama_tables_availability]</a>";
								}
							}
							if (!empty($_GET['noise_level'])) {
								$sql=mysqli_query($koneksi,"select * from noise_level where id_noise_level='$_GET[noise_level]'");
								while($d=mysqli_fetch_array($sql)){
									echo"<a href='#'>$d[nama_noise_level]</a>";
								}
							}
							if (!empty($_GET['waiter_tipping'])) {
								$sql=mysqli_query($koneksi,"select * from waiter_tipping where id_waiter_tipping='$_GET[waiter_tipping]'");
								while($d=mysqli_fetch_array($sql)){
									echo"<a href='#'>$d[nama_waiter_tipping]</a>";
								}
							}
							if (!empty($_GET['serving'])) {
								$currn=0;
								$facs='';
								foreach($_GET['serving'] as $servings){
									$currn++;
									$fasc.=$servings;
								}
								if ($currn==1) {
									echo "<a href='#'>$fasc</a>";
								}
								else {
									echo "<a href='#'>Multiple Serving</a>";
								}
							}
							if (!empty($_GET['type_of_serving'])) {
								$currn=0;
								$currs='';
								foreach($_GET['type_of_serving'] as $currencys){
									$currn++;
									$currs.=$currencys;
								}
								if ($currn==1) {
									echo "<a href='#'>$currs</a>";
								}
								else {
									echo "<a href='#'>Multiple Type Of Serving</a>";
								}

							}
							if (!empty($_GET['facility'])) {
								$currn=0;
								$facs='';
								foreach($_GET['facility'] as $facilitys){
									$currn++;
									$fasc.=$facilitys;
								}
								if ($currn==1) {
									echo "<a href='#'>$fasc</a>";
								}
								else {
									echo "<a href='#'>Multiple Facility</a>";
								}
							}
							echo "</div>";

							if ($_GET['sort']=='r.id_restaurant DESC') {
								echo "<div style='float: right; color: rgb(141, 141, 141);'></div>";
							}
							if ($_GET['sort']=='r.restaurant_name ASC') {
								echo "<div style='float: right; color: rgb(141, 141, 141);'></div>";
							}
							if ($_GET['sort']=='r.restaurant_name DESC') {
								echo "<div style='float: right; color: rgb(141, 141, 141);'></div>";
							}
							if ($_GET['sort']=='r.dilihat DESC') {
								echo "<div style='float: right; color: rgb(141, 141, 141);'></div>";
							}
							if ($_GET['sort']=='direkomendasi DESC') {
								echo "<div style='float: right; color: rgb(141, 141, 141);'></div>";
							}
							if ($_GET['sort']=='dilike DESC') {
								echo "<div style='float: right; color: rgb(141, 141, 141);'></div>";
							}

					?>
				</h4>
				<div class="form-inline border-form sort-form ml10-j">
					<div class="row row-m">
						<div class="col-6 col-6a">
							<div class="form-group">
								<select class="form-control no-padding" name="sort">
									<option value="r.id_restaurant DESC" <?php if(isset($_GET['sort'])){if($_GET['sort']=="r.id_restaurant DESC"){echo"selected";}} ?>>Latest</option>
									<option value="total_vote DESC" <?php if(isset($_GET['sort'])){if($_GET['sort']=="total_vote DESC"){echo"selected";}} ?>>Top Ranking</option>
									<option value="r.restaurant_name ASC" <?php if(isset($_GET['sort'])){if($_GET['sort']=="r.restaurant_name ASC"){echo"selected";}} ?>>Name A-Z</option>
									<option value="r.restaurant_name DESC" <?php if(isset($_GET['sort'])){if($_GET['sort']=="r.restaurant_name DESC"){echo"selected";}} ?>>Name Z-A</option>
									<!-- <option value="r.dilihat DESC" <?php if(isset($_GET['sort'])){if($_GET['sort']=="r.dilihat DESC"){echo"selected";}} ?>>Popular</option>
									<option value="direkomendasi DESC" <?php if(isset($_GET['sort'])){if($_GET['sort']=="direkomendasi DESC"){echo"selected";}} ?>>Recomended</option> -->
									<option value="dilike DESC" <?php if(isset($_GET['sort'])){if($_GET['sort']=="dilike DESC"){echo"selected";}} ?>>Favored</option>
								</select>
							</div>
						</div>
						<div class="col-6 col-6a">
							<div class="form-group">
								<select class="form-control no-padding" name="batas">
									<option value="25" <?php if(isset($_GET['batas'])){if($_GET['batas']==25){echo"selected";}} ?>>Show 25 per Page</option>
									<option value="50" <?php if(isset($_GET['batas'])){if($_GET['batas']==50){echo"selected";}} ?>>Show 50 per Page</option>
									<option value="100" <?php if(isset($_GET['batas'])){if($_GET['batas']==100){echo"selected";}} ?>>Show 100 per Page</option>
								</select>
							</div>
						</div>
						<div class="col-4 mt-s10"><button type="submit" class="btn btn-danger btn-block">Submit</button></div>
					</div>
				</div>
				<div class="media jarak1">
					<?php
					$kondisi=" WHERE r.id_restaurant <> 0";
					if(!empty($_GET['keyword'])){
						$kondisi .= " AND r.restaurant_name = '$_GET[keyword]' OR tag LIKE '%$_GET[keyword]%' OR t.nama_type_of_business = '$_GET[keyword]' OR n.nama_negara = '$_GET[keyword]' OR o.nama_propinsi = '$_GET[keyword]' OR k.nama_kota = '$_GET[keyword]'";
					}
					if(!empty($_GET['tag'])){
						$kondisi .= " AND r.tag like '%$_GET[tag]%'";
					}
					if(!empty($_GET['country'])){
						$kondisi .= " AND r.id_negara = '$_GET[country]'";
					}
					if(!empty($_GET['state'])){
						$kondisi .= " AND r.id_propinsi = '$_GET[state]'";
					}
					if(!empty($_GET['city'])){
						$kondisi .= " AND r.id_kota = '$_GET[city]'";
					}
					if(!empty($_GET['landmark'])){
						$kondisi .= " AND r.id_landmark = '$_GET[landmark]'";
					}
					if(!empty($_GET['mall'])){
						$kondisi .= " AND id_mall = '$_GET[mall]'";
					}
					if(!empty($_GET['business_type'])){
						$kondisi .= " AND r.id_type_of_business = '$_GET[business_type]'";
					}
					if(!empty($_GET['cuisine'])){
						$curr='';
						foreach($_GET['cuisine'] as $currency){
							$curr.="cuisine LIKE '%".$currency."%' AND ";
						}
						$cusss=rtrim($curr, " AND ");
						$kondisi .= " AND $cusss";
					}

					if(!empty($_GET['opening'])){
						date_default_timezone_set("Asia/Jakarta");
						$jam = date("H:i:s");
						if($_GET['opening']=='open'){
							$kondisi .= " AND TIME_to_sec(buka) < time_to_sec('$jam') and time_to_sec('$jam') < time_to_sec(tutup)";
						}
						elseif($_GET['opening']=='no'){
							$kondisi .= " TIME_to_sec(buka) = time_to_sec('00:00:00') and time_to_sec('00:00:00') = time_to_sec(tutup)";
						}
						else {
							if($jam >= '01:00:00' and $jam <= '12:00:00'){
								$kondisi .= " AND TIME_to_sec(buka) > time_to_sec('$jam') and time_to_sec('$jam') < time_to_sec(tutup) and buka <> '00:00:00'";
							}
							if($jam >= '12:00:00' and $jam <= '24:00:00'){
								$kondisi .= " AND TIME_to_sec(buka) < time_to_sec('$jam') and time_to_sec('$jam') > time_to_sec(tutup) and buka <> '00:00:00'";
							}
						}
					}

					if(!empty($_GET['overall'])){
						$kondisi .= " AND (SELECT (SUM(f.cleanlines) + SUM(f.customer_services) + SUM(f.food_beverage) + SUM(f.comfort) + SUM(f.value_money)) / COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant =r.id_restaurant) >= '$_GET[overall]'";
					}
					else{
						if(!empty($_GET['clean'])){
							$kondisi .= " AND (SELECT SUM(cleanlines)/ COUNT(id_member) AS total FROM restaurant_rating where restaurant_rating.id_restaurant=r.id_restaurant) >= '$_GET[clean]'";
						}
						if(!empty($_GET['service'])){
							$kondisi .= " AND (SELECT SUM(customer_services)/ COUNT(id_member) AS total FROM restaurant_rating where restaurant_rating.id_restaurant=r.id_restaurant) >= '$_GET[service]'";
						}
						if(!empty($_GET['food'])){
							$kondisi .= " AND (SELECT SUM(food_beverage)/ COUNT(id_member) AS total FROM restaurant_rating where restaurant_rating.id_restaurant=r.id_restaurant) >= '$_GET[food]'";
						}
						if(!empty($_GET['comfort'])){
							$kondisi .= " AND (SELECT SUM(comfort)/ COUNT(id_member) AS total FROM restaurant_rating where restaurant_rating.id_restaurant=r.id_restaurant) >= '$_GET[comfort]'";
						}
						if(!empty($_GET['money'])){
							$kondisi .= " AND (SELECT SUM(value_money)/ COUNT(id_member) AS total FROM restaurant_rating where restaurant_rating.id_restaurant=r.id_restaurant) >= '$_GET[money]'";
						}
					}
					if(!empty($_GET['alcohol'])){
						$kondisi .= " AND r.alcohol_serving = '$_GET[alcohol]'";
					}
					if(!empty($_GET['pork'])){
						$kondisi .= " AND r.pork_serving = '$_GET[pork]'";
					}
					if(!empty($_GET['price_index'])){
						$kondisi .= " AND r.id_price_index = '$_GET[price_index]'";
					}
					if(!empty($_GET['suitable_for'])){
						$kondisi .= " AND r.id_suitable_for = '$_GET[suitable_for]'";
					}
					if(!empty($_GET['serving'])){
						$curr='';
						foreach($_GET['serving'] as $currency){
							$curr.="serving LIKE '%".$currency."%' AND ";
						}
						$cusss=rtrim($curr, " AND ");
						$kondisi .= " AND $cusss";

							//$kondisi .= " AND serving like '%$_GET[serving]%'";

					}
					if(!empty($_GET['type_of_serving'])){
						$curr='';
						foreach($_GET['type_of_serving'] as $currency){
							$curr.="type_of_serving LIKE '%".$currency."%' AND ";
						}
						$cusss=rtrim($curr, " AND ");
						$kondisi .= " AND $cusss";

					}
					if(!empty($_GET['serving_time'])){
						$kondisi .= " AND r.id_serving_time = '$_GET[serving_time]'";
					}
					if(!empty($_GET['type_of_service'])){
						$kondisi .= " AND r.id_type_of_service = '$_GET[type_of_service]'";
					}

					if(!empty($_GET['air_conditioning'])){
						$kondisi .= " AND r.id_air_conditioning = '$_GET[air_conditioning]'";
					}

					if(!empty($_GET['heating_system'])){
						$kondisi .= " AND r.id_heating_system = '$_GET[heating_system]'";
					}
					if(!empty($_GET['facility'])){
						$curr='';
						foreach($_GET['facility'] as $currency){
							$curr.="facility LIKE '%".$currency."%' AND ";
						}
						$cusss=rtrim($curr, " AND ");
						$kondisi .= " AND $cusss";

					}
					if(!empty($_GET['wifi'])){
						$kondisi .= " AND r.id_wifi = '$_GET[wifi]'";
					}
					if(!empty($_GET['term_of_payment'])){
						$kondisi .= " AND r.id_term_of_payment = '$_GET[term_of_payment]'";
					}
					if(!empty($_GET['premise_security'])){
						$kondisi .= " AND r.id_premise_security = '$_GET[premise_security]'";
					}
					if(!empty($_GET['parking_spaces'])){
						$kondisi .= " AND r.id_parking_spaces = '$_GET[parking_spaces]'";
					}
					if(!empty($_GET['premise_fire_safety'])){
						$kondisi .= " AND r.id_premise_fire_safety = '$_GET[premise_fire_safety]'";
					}
					if(!empty($_GET['premise_hygiene'])){
						$kondisi .= " AND r.id_premise_hygiene = '$_GET[premise_hygiene]'";
					}
					if(!empty($_GET['premise_maintenance'])){
						$kondisi .= " AND r.id_premise_maintenance = '$_GET[premise_maintenance]'";
					}
					if(!empty($_GET['ambience'])){
						$kondisi .= " AND r.id_ambience = '$_GET[ambience]'";
					}
					if(!empty($_GET['attire'])){
						$kondisi .= " AND r.id_attire = '$_GET[attire]'";
					}
					if(!empty($_GET['clean_washroom'])){
						$kondisi .= " AND r.id_clean_washroom = '$_GET[clean_washroom]'";
					}
					if(!empty($_GET['tables_availability'])){
						$kondisi .= " AND r.id_tables_availability = '$_GET[tables_availability]'";
					}
					if(!empty($_GET['noise_level'])){
						$kondisi .= " AND r.id_noise_level = '$_GET[noise_level]'";
					}
					if(!empty($_GET['waiter_tipping'])){
						$kondisi .= " AND r.id_waiter_tipping = '$_GET[waiter_tipping]'";
					}
					$p      = new Paging;
					$batas  = 25;
					if(isset($_GET['batas'])){
						$batas  = $_GET['batas'];
					}

					$sort="r.id_restaurant DESC";
					if($_GET['sort']=="total_vote DESC") {
						$sort =  "total_vote DESC, jumlah_vote DESC, jumlah_like DESC, r.restaurant_name ASC";
					}
					elseif (empty($_GET['sort'])) {
						$sort="r.id_restaurant DESC";
					}
					else {
						$sort  = $_GET['sort'];
					}

					$posisi = $p->cariPosisi($batas);
					$no = $posisi+1;

					$sql="SELECT *,(select p.gambar_restaurant_photo from restaurant_photo p where p.id_restaurant=r.id_restaurant order by p.id_restaurant_photo desc limit 1) as gambar,
					(SELECT COUNT(h.id_member) FROM restaurant_like h WHERE h.id_restaurant=r.id_restaurant) AS jumlah_like,
					(SELECT COUNT(h.id_member) FROM restaurant_rekomendasi h WHERE h.id_restaurant=r.id_restaurant) AS direkomendasi,
					(SELECT COUNT(id_restaurant_here) AS jumlah FROM restaurant_here WHERE restaurant_here.id_restaurant=r.id_restaurant) AS jumlah_disini,
					(SELECT COUNT(id_restaurant_rating) AS jumlah from restaurant_rating WHERE restaurant_rating.id_restaurant=r.id_restaurant) AS jumlah_vote,
					(SELECT (SUM(cleanlines) + SUM(customer_services) + SUM(food_beverage) + SUM(comfort) + SUM(value_money)) / COUNT(id_member) AS total FROM restaurant_rating WHERE restaurant_rating.id_restaurant=r.id_restaurant) AS total_vote FROM restaurant r
					LEFT JOIN negara n ON r.id_negara = n.id_negara
					LEFT JOIN propinsi o ON r.id_propinsi = o.id_propinsi
					LEFT JOIN kota k ON r.id_kota = k.id_kota LEFT JOIN price_index p ON r.id_price_index = p.id_price_index
					LEFT JOIN type_of_business t ON r.id_type_of_business = t.id_type_of_business".$kondisi." ORDER BY ".$sort." LIMIT $posisi,$batas";

					//echo $sql;

					$sql2="SELECT * FROM restaurant r LEFT JOIN negara n ON r.id_negara = n.id_negara LEFT JOIN propinsi o ON r.id_propinsi = o.id_propinsi LEFT JOIN kota k ON r.id_kota = k.id_kota LEFT JOIN price_index p ON r.id_price_index = p.id_price_index LEFT JOIN type_of_business t ON r.id_type_of_business = t.id_type_of_business".$kondisi;


					$jmldata = mysqli_num_rows(mysqli_query($koneksi,$sql2));
					if($jmldata<>0){
						$latest=mysqli_query($koneksi,$sql);
						while($l=mysqli_fetch_array($latest)){
							// mysqli_query($koneksi,"UPDATE restaurant set rank_global='$no' where id_restaurant='$l[id_restaurant]'");
							// echo "UPDATE restaurant set rank_global='$no' where id_restaurant='$l[id_restaurant]'";
							$negara=$l['negara'];
							$slug=seo($l['restaurant_name']);
							$post=date("jS M, Y", strtotime($l['tgl_post']));
							$total_rating=number_format((float)$l['total_vote'], 2, '.', '');
							$img_rating=floor($total_rating);
							$id = id_masking($l['id_restaurant']);
							$gambar=$l['gambar'];
								if(empty($l['gambar'])){
								$gambar="foodieguidances.jpg";
							}
							echo"<div class='row col-4a'>
								<div class='col-1'>$no.</div>
								<div class='col-8 thumb col-8a no-padding'>
									<a href='$base_url/pages/restaurant/info/$id/$slug'><img data-original='$base_url/assets/img/restaurant/big_$gambar' class='lazy' width='220' height='165'></a>
									<a href='$base_url/pages/restaurant'><span>Restaurant</span></a>
								</div>
								<div class='col-7 info col-8a'>
									<em>posted on $post</em>
									<h3 class='sembunyi'><a href='$base_url/pages/restaurant/info/$id/$slug'>$l[restaurant_name]</a></h3>
									<ul class='list-unstyled f-12'>
										<li><em>category</em> <a href='$base_url/pages/restaurant/search/?business_type=$l[id_type_of_business]'>$l[nama_type_of_business]</a></li>
										<li class='sembunyi'><strong>$l[nama_negara]</strong>,  $l[street_address]</li>
										<li><em>cost</em> <a href='$base_url/pages/restaurant/search/?price_index=$l[id_price_index]'>$l[nama_price_index]</a></li>
										<li>$l[jumlah_like] Like <span class='bullet'>&#8226;</span> $l[jumlah_disini] Being Here</li>
									</ul>
									<div class='bintang'>";
									if($total_rating<>0){
										echo"<input type='hidden' class='rating' data-filled='fa fa-star' data-empty='fa fa-star-o' readonly='readonly' value='$total_rating'>&nbsp;&nbsp;&nbsp;&nbsp;<strong class='f-18'>$total_rating </strong>&nbsp;&nbsp;<br><em>from $l[jumlah_vote] vote</em>";
									}
								echo"</div>
								</div>
							</div>";
							$no++;
						}
						echo"</div><nav class='ac'>";
						$link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
						$jmlhalaman  = $p->jumlahHalaman($jmldata, $batas);
						$linkHalaman = $p->navHalaman($link,$_GET['page'], $jmlhalaman);
						echo "<ul class='pagination'>$linkHalaman</ul>";
						echo"</nav>";
					}
					else{
						echo"<div class='row'>
							<div class='col-16'>
								<h4 class='f-merah'>Oops! We can't found what you are looking for.</h4>
								<p>Would you like to share it with us?.</p>
								<a href=";
									if(!empty($_SESSION['food_member'])){ echo"'$base_url/$username/my-restaurant/new'";}else{ echo"'$base_url/login-area'";}
							    echo" class='btn btn-danger'>Submit New Restaurant Review</a>
							</div>
						</div></div>";
					}
					?>
			</div>
			<div class="col-4 mene-atas"><br><br>
			<?php include"config/inc/search.php"; ?>
			<?php include"config/inc/iklan.php"; ?>
				<div class="fb-like-box" data-href="https://www.facebook.com/foodieguidances#_=_" data-width="220" data-height="350" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
			</div>
		</div>
		</form>
    </div>
    <?php include"config/inc/footer.php"; ?>
	 <a class="scrollToNext" href="<?php echo "$base_url" ?>" style="bottom: 320px;"><img src="<?php echo"$base_url/assets/img/theme/kiri.png" ?>"></a>
	 <a class="scrollToNext" href="<?php echo  "$base_url/pages/food" ?>" style="bottom: 273px;"><img src="<?php echo"$base_url/assets/img/theme/kanan.png" ?>"></a>
	 <a href="#" class="scrollToTop"><img src="<?php echo"$base_url/assets/img/theme/atas.png" ?>"></a>

	 	<?php //include "config/func/rank_restaurant.php"; ?>

	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap-rating.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.superslides.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/select2.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.lazyload.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/theme.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
   <script>
      $(document).ready(function() {
	      $('.drawer').drawer();

			$('.tutup').click(function(){
				$('.iklanan').toggleClass('hilang', 500);
			});

			$('.cart').click(function(e){
				e.stopPropagation();
				$('.dis-cart').toggle();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$('.plus').click(function(e){
				e.stopPropagation();
				$('.dis-plus').toggle();
				$('.dis-cart').hide();
				$('.dis-share').hide();
			});
			$('.share').click(function(e){
				e.stopPropagation();
				$('.dis-share').toggle();
				$('.dis-plus').hide();
				$('.dis-cart').hide();
			});
			$('.drawer-toggle').click(function(){
				$('.dis-cart').hide();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$('.btn-cari').click(function(){
				$('.search_kiri').toggleClass('search_kiri_tam', 500);
			});
			$(document).click(function () {
				 var $el = $(".dis-share");
				 if ($el.is(":visible")) {
					  $el.fadeOut(200);
				 }
				 var $ela = $(".dis-plus");
				 if ($ela.is(":visible")) {
					  $ela.fadeOut(200);
				 }
				 var $elu = $(".dis-cart");
				 if ($elu.is(":visible")) {
					  $elu.fadeOut(200);
				 }
		   });

      });
   </script>
	<div id="fb-root"></div>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.input-group-addon').click(function(){
				$('.tag').val('');
			});
		});
	</script>
	<script type="text/javascript">
		$('input[type="reset"]').click(function() {
			$("select").select2().val();
			$("select").select2().val();
		});
	</script>
	<script>
	function myFunction(){
		var myForm = document.getElementById('ale-form');
		var allInputs = myForm.getElementsByTagName('select');
		var teks = myForm.getElementsByTagName('input');
		var input, i, grup, a;

		for(i = 0; input = allInputs[i]; i++) {
			if(input.getAttribute('name') && !input.value) {
				input.setAttribute('name', '');
			}
		}
		for(a = 0; grup = teks[a]; a++) {
			if(grup.getAttribute('name') && !grup.value) {
				grup.setAttribute('name', '');
			}
		}
	}
	$(document).ready(function () {
		$(window).scroll(function(){
			if ($(this).scrollTop() > 50) {
				$('.scrollToNext').fadeIn();
			} else {
				$('.scrollToNext').fadeOut();
			}
		});
		$(window).scroll(function(){
			 if ($(this).scrollTop() > 100) {
				 $('.scrollToTop').fadeIn();
			 } else {
				 $('.scrollToTop').fadeOut();
			 }
		});

		$("img.lazy").lazyload({
			effect : "fadeIn"
		});
		$('.rating').rating();
		$("select").select2();
		$("#negara").change(function(){
			var id = $("#negara").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_propinsi.php",
				data: "id=" + id,
				success: function(data){
					$("#propinsi").html(data);
					$("#propinsi").fadeIn(2000);
					$('#propinsi').selectpicker('refresh');
				}
			});
		});
		$("#propinsi").change(function(){
			var id = $("#propinsi").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_kota.php",
				data: "id=" + id,
				success: function(data){
					$("#kota").html(data);
					$("#kota").fadeIn(2000);
					$('#kota').selectpicker('refresh');
				}
			});
		});
		$("#kota").change(function(){
			var id = $("#kota").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_mall.php",
				data: "id=" + id,
				success: function(data){
					$("#mall").html(data);
					$("#mall").fadeIn(2000);
				}
			});
		});

		$("#negara2").change(function(){
			var id = $("#negara2").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_propinsi.php",
				data: "id=" + id,
				success: function(data){
					$("#propinsi2").html(data);
					$("#propinsi2").fadeIn(2000);
					$('#propinsi2').selectpicker('refresh');
				}
			});
		});
		$("#propinsi2").change(function(){
			var id = $("#propinsi2").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_kota.php",
				data: "id=" + id,
				success: function(data){
					$("#kota2").html(data);
					$("#kota2").fadeIn(2000);
					$('#kota2').selectpicker('refresh');
				}
			});
		});
		$("#kota2").change(function(){
			var id = $("#kota2").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_mall.php",
				data: "id=" + id,
				success: function(data){
					$("#mall").html(data);
					$("#mall").fadeIn(2000);
				}
			});
		});

		$("#negara3").change(function(){
			var id = $("#negara3").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_propinsi.php",
				data: "id=" + id,
				success: function(data){
					$("#propinsi3").html(data);
					$("#propinsi3").fadeIn(2000);
					$('#propinsi3').selectpicker('refresh');
				}
			});
		});
		$("#propinsi3").change(function(){
			var id = $("#propinsi3").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_kota.php",
				data: "id=" + id,
				success: function(data){
					$("#kota3").html(data);
					$("#kota3").fadeIn(2000);
					$('#kota3').selectpicker('refresh');
				}
			});
		});
		$("#kota3").change(function(){
			var id = $("#kota3").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_mall.php",
				data: "id=" + id,
				success: function(data){
					$("#mall").html(data);
					$("#mall").fadeIn(2000);
				}
			});
		});

		$("#negara4").change(function(){
			var id = $("#negara4").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_propinsi.php",
				data: "id=" + id,
				success: function(data){
					$("#propinsi4").html(data);
					$("#propinsi4").fadeIn(2000);
					$('#propinsi4').selectpicker('refresh');
				}
			});
		});
		$("#propinsi4").change(function(){
			var id = $("#propinsi4").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_kota.php",
				data: "id=" + id,
				success: function(data){
					$("#kota4").html(data);
					$("#kota4").fadeIn(2000);
					$('#kota4').selectpicker('refresh');
				}
			});
		});
		$("#kota4").change(function(){
			var id = $("#kota4").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_resto.php",
				data: "id=" + id,
				success: function(data){
					$("#mall").html(data);
					$("#mall").fadeIn(2000);
				}
			});
		});
		$("#negara5").change(function(){
			var id = $("#negara5").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_propinsi.php",
				data: "id=" + id,
				success: function(data){
					$("#propinsi5").html(data);
					$("#propinsi5").fadeIn(2000);
					$('#propinsi5').selectpicker('refresh');
				}
			});
		});
		$("#propinsi5").change(function(){
			var id = $("#propinsi5").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_kota.php",
				data: "id=" + id,
				success: function(data){
					$("#kota5").html(data);
					$("#kota5").fadeIn(2000);
					$('#kota5').selectpicker('refresh');
				}
			});
		});
	});
	/*=====For developer to config ====*/
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=348272391978609&version=v2.0";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script>

</body>
</html>
