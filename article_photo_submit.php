<?php
session_start();
$auto_logout=1800000;
include "config/func/base_url.php";
if(!empty($_SESSION['food_member'])){
if (time()-$_SESSION['timestamp']>$auto_logout){
    session_destroy();
    session_unset();
	header("Location: ".$base_url."/auto-logout");
	exit();
}else{
    $_SESSION['timestamp']=time();
}
include "config/database/db.php";
include "config/func/member_data.php";
$active = "member";
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
    <title>Submit Article Photo - Foodie Guidances</title>
	<meta name="keywords" content="">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/jquery-ui.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>
  <link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body style="position: initial;" class="drawer drawer--right">
    <!-- Fixed navbar -->
  <?php include"config/inc/header.php"; ?>
    <div class="container hook">
		<div class="row">
			<div class="col-16">
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Account</a></li>
					<li class="active">Submit Article Photo</li>
				</ol>
				<?php
				if(isset($_SESSION['notif'])){
					if($_SESSION['notif']=="gambar"){
						echo"<div class='alert alert-danger alert-dismissible' role='alert'>
							<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
							<strong>Failed!</strong> Image used have to be in JPG, PNG, and GIF format and the maximum total images size is 50MB also minimal dimension must be 800x600 pixel.
						</div>";
					}
					unset($_SESSION['notif']);
				}
				?>
				<h3 class="f-merah mb10">Submit Article Photo</h3>
				<p>*required field</p>
				<form class="border-form form-horizontal" method="post" action="<?php echo"$base_url"; ?>/config/func/save_article_photo.php" enctype="multipart/form-data">
					<div class="row mb20">
						<div class="col-4"><label class="control-label">Article <span class="f-merah">*</span></label></div>
						<div class="col-6">
							<input type="text" class="form-control" placeholder="Write Name" name="article" required id="search">
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4"><label class="control-label">Photo Title <span class="f-merah">*</span></label></div>
						<div class="col-6">
							<input type="text" class="form-control" placeholder="Write Name" name="nama" required maxlength="50">
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4"><label class="control-label">Cover Photo <span class="f-merah">*</span></label></div>
						<div class="col-6">
							<div class="fileinput fileinput-new" data-provides="fileinput">
							  <div class="fileinput-preview upload" data-trigger="fileinput"></div>
							  <input type="file" name="gambar" class="hidden" required>
							</div>
							<p class="help-block">Image used have to be in JPG, PNG, and GIF format and the maximum total images size is 50MB also minimal dimension must be 800x600 pixel.</p>
						</div>
					</div>
					<button type="submit" class="btn btn-danger">Submit</button> <button type="reset" class="btn btn-danger">Reset</button>
				</form>
			</div>
		</div>
    </div>
    <?php include"config/inc/footer.php"; ?>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jasny-bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery-ui.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/theme.js"></script>
  <script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
  <script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
  <script>
     $(document).ready(function() {
       $('.drawer').drawer({
        iscroll: {
          scrollbars: true,
          mouseWheel: true,
          interactiveScrollbars: true,
          shrinkScrollbars: 'scale',
          fadeScrollbars: true,
          click: true
        },
        showOverlay: true
     });

     $('.tutup').click(function(){
       $('.iklanan').toggleClass('hilang', 500);
     });

     $('.cart').click(function(e){
       e.stopPropagation();
       $('.dis-cart').toggle();
       $('.dis-plus').hide();
       $('.dis-share').hide();
     });
     $('.plus').click(function(e){
       e.stopPropagation();
       $('.dis-plus').toggle();
       $('.dis-cart').hide();
       $('.dis-share').hide();
     });
     $('.share').click(function(e){
       e.stopPropagation();
       $('.dis-share').toggle();
       $('.dis-plus').hide();
       $('.dis-cart').hide();
     });
     $('.drawer-toggle').click(function(){
       $('.dis-cart').hide();
       $('.dis-plus').hide();
       $('.dis-share').hide();
     });
     $(document).click(function () {
        var $el = $(".dis-share");
        if ($el.is(":visible")) {
           $el.fadeOut(200);
        }
        var $ela = $(".dis-plus");
        if ($ela.is(":visible")) {
           $ela.fadeOut(200);
        }
        var $elu = $(".dis-cart");
        if ($elu.is(":visible")) {
           $elu.fadeOut(200);
        }
      });
     });
  </script>
	<script type="text/javascript">
	$(document).ready(function () {
		$('#search').autocomplete({
			source: "<?php echo "$base_url/config/func/ajax_articles_search.php"; ?>",
			minLength: 3
		});
		$(".alert").fadeTo(5000, 500).fadeOut(500, function(){
			$(".alert").alert('close');
		});
	});
	</script>
</body>
</html>
<?php
}
else{
	header("Location: ".$base_url."/login-area");
}
?>
