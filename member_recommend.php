<?php
session_start();
include "config/func/base_url.php";
$active = "member";
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
    <title>Recommend - Foodie Guidances</title>
	<meta name="keywords" content="">
    <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/select2.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap-datepicker3.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/awesome-bootstrap-checkbox.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>
  <link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="favicon.ico">
</head>
 <body style="position: initial;" class="drawer drawer--right">
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>
    <div class="container hook">
		<div class="row">
      <div class="btn-caris">
        <a href="#">
          <img src="<?php echo"$base_url/assets/img/member/$u[gambar_thumb]"; ?>" class="foto_profil">
        </a>
      </div>
			<div class="col-4 search_kiri">
				<ol class="breadcrumb">
					<li><a href="#">Account</a></li>
					<li class="active">Recommend</li>
				</ol>
				<div>
					<img src="<?php echo"$base_url"; ?>/assets/img/member/foto_profil.jpg">
					<h4>Ale Kyo</h4>
				</div>
				<hr class="mtb10">
				<ul class="list-unstyled">
					<li><a href="<?php echo"$base_url/$u[username]/activity-feed"; ?>">Activity Feed</a></li>
					<li><span class="buka">
						Account
						<ul class="pl50 sub-menu">
							<li><a href="<?php echo"$base_url/$u[username]/edit-profile"; ?>">Edit Profile</a></li>
							<li><a href="<?php echo"$base_url/$u[username]/change-password"; ?>">Change Password</a></li>
							<li><a href="<?php echo"$base_url/$u[username]/activity-setting"; ?>">Activity Feed Setting</a></li>
							<li><a href="<?php echo"$base_url/$u[username]/social-media"; ?>">Social Media</a></li>
							<li><a href="<?php echo"$base_url/$u[username]/delete-account"; ?>">Delete Account</a></li>
						</ul>
						</span>
					</li>
					<li><span class="buka">
						Submit
						<ul class="pl50 sub-menu">
							<li><a href="<?php echo"$base_url/$u[username]/my-restaurant/new"; ?>">Restaurant</a></li>
							<li><a href="<?php echo"$base_url/$u[username]/my-food/new"; ?>">Food</a></li>
							<li><a href="<?php echo"$base_url/$u[username]/my-beverage/new"; ?>">Beverage</a></li>
							<li><a href="<?php echo"$base_url/$u[username]/my-recipe/new"; ?>">Recipe</a></li>
							<li><a href="<?php echo"$base_url/$u[username]/my-article/new"; ?>">Article</a></li>
							<li><a href="<?php echo"$base_url/$u[username]/my-video/new"; ?>">Video</a></li>
						</ul>
						</span>
					</li>
					<li><a href="<?php echo"$base_url/$u[username]/my-restaurant"; ?>">My Restaurant</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-food"; ?>">My Food</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-beverage"; ?>">My Beverage</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-photo"; ?>">My Photo Gallery</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-menu"; ?>">My Restaurant Menu</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-recipe"; ?>">My Recipe</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-article"; ?>">My Article</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-video"; ?>">My Video</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-recommend"; ?>" class="f-merah">Recommend</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-bookmark"; ?>">Bookmark</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-vote"; ?>">Give Vote to</a></li>
				</ul>
				<hr class="mtb10">
				<strong>FGMart</strong>
				<ul class="list-unstyled">
					<li><a href="<?php echo"$base_url/$u[username]/stock-cart"; ?>">Stock Cart</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/download-link"; ?>">Download Link</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/order-history"; ?>">Order History</a></li>
				</ul>
			</div>
			<div class="col-12 col-8a">
				<h4 class="f-merah no-mb mt20 mb10">Recommend</h4>
				<form class="form-inline border-form sort-form mb20">
					<div class="row">
						<div class="col-4">
							<div class="form-group">
								<input type="text" name="awal" class="form-control date" placeholder="Select start date..">
							</div>
						</div>
						<div class="col-4">
							<div class="form-group">
								<input type="text" name="akhir" class="form-control date" placeholder="Select end date..">
							</div>
						</div>
						<div class="col-4">
							<div class="form-group" style="position:relative;">
								<select class="form-control select" name="sort">
									<option value="25" selected>Show 25 per Page</option>
									<option value="25" selected>Show 50 per Page</option>
									<option value="25" selected>Show 100 per Page</option>
								</select>
							</div>
						</div>
						<div class="col-3"><button type="submit" class="btn btn-danger btn-block">Submit</button></div>
						<div class="col-1"><button type="submit" class="btn btn-danger btn-block"><i class="fa fa-trash-o"></i></button></div>
					</div>
				</form>
				<table class="table">
					<tr>
						<th width="40px">No</th>
						<th width="120px"></th>
						<th>Date</th>
						<th>Description</th>
						<th></th>	
					</tr>
					<tr>
						<td>1.</td>
						<td><img src="<?php echo"$base_url"; ?>/assets/img/resto/small_resto1.jpg" width="100px"></td>
						<td>26/08/2015</td>
						<td>restaurant Bu Rudi Surabaya</td>
						<td class="text-right">
							<a href="" class="btn btn-danger">Denounce</a>
						</td>
					</tr>
				</table>
				<nav class="ac">
				  <ul class="pagination">
					<li class="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
					<li class="active"><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li>
					  <a href="#" aria-label="Next">
						<span aria-hidden="true">&raquo;</span>
					  </a>
					</li>
				  </ul>
				</nav>
			</div>
		</div>
    </div>
    <?php include"config/inc/footer.php"; ?>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/select2.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap-datepicker.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/theme.js"></script>
  <script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
	<script>
		 $(document).ready(function() {
			 $('.drawer').drawer();

		 $('.tutup').click(function(){
			 $('.iklanan').toggleClass('hilang', 500);
		 });

		 $('.cart').click(function(e){
			 e.stopPropagation();
			 $('.dis-cart').toggle();
			 $('.dis-plus').hide();
			 $('.dis-share').hide();
		 });
		 $('.plus').click(function(e){
			 e.stopPropagation();
			 $('.dis-plus').toggle();
			 $('.dis-cart').hide();
			 $('.dis-share').hide();
		 });
		 $('.share').click(function(e){
			 e.stopPropagation();
			 $('.dis-share').toggle();
			 $('.dis-plus').hide();
			 $('.dis-cart').hide();
		 });
		 $('.drawer-toggle').click(function(){
			 $('.dis-cart').hide();
			 $('.dis-plus').hide();
			 $('.dis-share').hide();
		 });
		 $('.btn-caris').click(function(){
			 $('.search_kiri').toggleClass('search_kiri_tam', 500);
		 });
		 $(document).click(function () {
				var $el = $(".dis-share");
				if ($el.is(":visible")) {
					 $el.fadeOut(200);
				}
				var $ela = $(".dis-plus");
				if ($ela.is(":visible")) {
					 $ela.fadeOut(200);
				}
				var $elu = $(".dis-cart");
				if ($elu.is(":visible")) {
					 $elu.fadeOut(200);
				}
			});

		 });
	</script>
	<script type="text/javascript">
	$(document).ready(function () {
		$('.date').datepicker({
	        format: "dd/mm/yyyy",
	        todayHighlight: true
	    });
		$('.sub-menu').hide(); //Hide children by default
	
		$('.buka').click(function(event){
			$(this).children('.sub-menu').slideToggle('slow');
		});
	});
	</script>
</body>
</html>