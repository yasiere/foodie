<?php error_reporting(0);
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";
include "config/func/paging_search.php";
include "config/func/id_masking.php";
$auto_logout=1800000;
if(!empty($_SESSION['food_member'])){
	if (time()-$_SESSION['timestamp']>$auto_logout){
		session_destroy();
		session_unset();
		header("Location: ".$base_url."/auto-logout");
		exit();
	}else{
		$_SESSION['timestamp']=time();
	}
	include "config/func/member_data.php";
}
$active = "beverage";
$type = "beverage";
function tulis_cekbox($field,$koneksi,$judul) {
	$query ="select * from ".$judul." order by nama_".$judul;
	$r = mysqli_query($koneksi,$query);
	$_arrNilai = explode('+', $field);
	$str = '';
	$no=1;
	while ($w = mysqli_fetch_array($r)) {
		$_ck = (array_search($w[1], $_arrNilai) === false)? '' : 'checked';
		$str .= "<div class=''>
			<div class='radio radio-inline radio-danger'>
				<input type='radio' name='".$judul."' value='$w[1]' id='$judul.$no' $_ck>
				<label for='$judul.$no'> $w[1]</label>
			</div>
		</div>";
		$no++;
	}
	return $str;
}
function tulis_cekboxxer($field,$koneksi,$judul,$cl) {
	$query ="select * from ".$judul." order by nama_".$judul;
	$r = mysqli_query($koneksi,$query);
	$_arrNilai = explode('+', $field);
	$str = '';
	$no=1;
	while ($w = mysqli_fetch_array($r)) {
		$_ck = (array_search($w[1], $_arrNilai) === false)? '' : 'checked';
		$str .= "<div class=''>
			<div class='checkbox checkbox-inline checkbox-danger'>
				<input type='checkbox' name='".$judul."[]' value='$w[1]' id='$judul$no' $_ck>
				<label for='$judul$no'> $w[1]</label>
			</div>
		</div>";
		$no++;
	}
	return $str;
}
if(!empty($_GET['batas'])){
	$batas=$_GET['batas'];
}
else {
	$batas="25";
}
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
    <title>Beverage Result - Foodie Guidances</title>
	<meta name="keyWorlds" content="">
  <meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/select2.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/awesome-bootstrap-checkbox.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>

	<link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
 <body style="position: initial;" class="drawer drawer--right">
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>
	<div class="container hook">
		<form method="get" action="<?php echo"$base_url/pages/beverage/search/"; ?>" class="border-form" id="ale-form" onsubmit="myFunction()">
		<div class="row">
			<div class="btn-cari">
				<a href="#">
					<img src="<?php echo"$base_url/assets/images/cari2.png"; ?>" alt="" height="100%" />
				</a>
			</div>
			<div class="col-4 search_kiri">


				<div class="border-form filter-form">
					<h4 class="panel-title" style="margin-top: 20px; color: rgb(237, 28, 36);">
						Advance Search
					</h4>
					<?php
						if(!empty($_GET['batas'])){
							$batas=$_GET['batas'];
						}
						else {
							$batas="25";
						}
					?>

					<?php include "config/func/search_resto.php"; ?>

					<?php include "config/func/search_food.php"; ?>


					<div class="panel-groupmb10" id="accordionsssas" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default head-pan">
							<div class="panel-heading jud-pan" role="tab" id="beverage1" data-toggle="collapse" data-parent="#accordionsssas" href="#collapsesssas1" aria-expanded="true" aria-controls="collapsesssas1">
								<h4 class="panel-title">
									❯ Beverage
								</h4>
							</div>
							<div id="collapsesssas1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="beverage1">
								<div class="panel-body">
									<form method="get" action="<?php echo"$base_url/pages/beverage/search/"; ?>" class="border-form" id="ale-form" onsubmit="myFunction()">
										<input type="hidden" name="sort" value="<?php echo"$_GET[sort]";?>">
										<input type="hidden" name="batas" value="<?php echo"$batas";?>">
										<?php
											if(!empty($_GET['batas'])){
												$batas=$_GET['batas'];
											}
											else {
												$batas="25";
											}
										?>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a <?php if($_GET['sort']=='total DESC'){ echo "class='akt'";} ?> href="<?php echo"$base_url" ?>/pages/beverage/search/?sort=total+DESC&batas=<?php echo"$batas";?>">World Ranking</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a <?php if($_GET['sort']=='r.dilihat DESC'){ echo "class='akt'";} ?> href="<?php echo"$base_url" ?>/pages/beverage/search/?sort=r.dilihat+DESC&batas=<?php echo"$batas";?>">Popular</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a <?php if($_GET['sort']=='direkomendasi DESC'){ echo "class='akt'";} ?> href="<?php echo"$base_url" ?>/pages/beverage/search/?sort=direkomendasi+DESC&batas=<?php echo"$batas";?>">Recomended</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="headingbeverage1">
												<h4 class="panel-title">
													<a data-toggle="collapse" <?php if(!empty($_GET['country']) || !empty($_GET['state']) || !empty($_GET['city'])
													 ){ echo "class='akt'";} ?> data-parent="#collapse" href="#collapsebeverage1" aria-expanded="true" aria-controls="collapsebeverage1">Location Filter</a>
												</h4>
											</div>
											<div id="collapsebeverage1"  <?php if(!empty($_GET['country']) || !empty($_GET['state']) || !empty($_GET['city'])
											 ){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="headingbeverage1">
												<div class="panel-body">
													<div class="form-group">
														<label>Country</label>
														<select class="form-control" name="country" id="negara3">
															<option value="" selected>Select Country</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from negara order by nama_negara asc");
															while($b=mysqli_fetch_array($sql)){
																if($b['id_negara']==$_GET['country']){
																	echo"<option value='$b[id_negara]' selected>$b[nama_negara]</option>";
																}
																else{
																	echo"<option value='$b[id_negara]'>$b[nama_negara]</option>";
																}
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>States/Province</label>
														<select class="form-control" name="state" id="propinsi3">
															<option value="" selected>Select States/ Province</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from propinsi where id_negara='$_GET[country]' order by nama_propinsi asc");
															while($b=mysqli_fetch_array($sql)){
																if($b['id_propinsi']==$_GET['state']){
																	echo"<option value='$b[id_propinsi]' selected>$b[nama_propinsi]</option>";
																}
																else{
																	echo"<option value='$b[id_propinsi]'>$b[nama_propinsi]</option>";
																}
															}?>
														</select>
													</div>
													<div class="form-group">
														<label>City</label>
														<select class="form-control" name="city" id="kota3">
															<option value="" selected>Select City</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from kota where id_propinsi='$_GET[state]' order by nama_kota asc");
															while($b=mysqli_fetch_array($sql)){
																if($b['id_kota']==$_GET['city']){
																	echo"<option value='$b[id_kota]' selected>$b[nama_kota]</option>";
																}
																else{
																	echo"<option value='$b[id_kota]'>$b[nama_kota]</option>";
																}
															}?>
														</select>
													</div>

												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="headingbeverage2cat">
												<h4 class="panel-title">
													<a data-toggle="collapse" <?php if(!empty($_GET['category'])){ echo "class='akt'";} ?> data-parent="#accordionbeverage" href="#collapsebeverage2cat" aria-expanded="true" aria-controls="collapsebeverage2tag">Category</a>
												</h4>
											</div>
											<div id="collapsebeverage2cat" <?php if(!empty($_GET['category']) ){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="headingbeverage2cat">
												<div class="panel-body">
													<div class="form-group">
														<select class="form-control" name="category">
															<option value="" selected>Select Category</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from beverage_category order by nama_beverage_category asc");
															while($b=mysqli_fetch_array($sql)){

																if($b['id_beverage_category']==$_GET['category']){
																	echo"<option value='$b[id_beverage_category]' selected>$b[nama_beverage_category]</option>";
																}
																else{
																	echo"<option value='$b[id_beverage_category]'>$b[nama_beverage_category]</option>";
																}
															}
															?>
														</select>
													</div>
												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="headingbeverage2pri">
												<h4 class="panel-title">
													<a data-toggle="collapse" <?php if(!empty($_GET['price_index'])){ echo "class='akt'";} ?> data-parent="#accordionbeverage" href="#collapsebeverage2pri" aria-expanded="true" aria-controls="collapsebeverage2pri">Price Index</a>
												</h4>
											</div>
											<div id="collapsebeverage2pri" <?php if(!empty($_GET['price_index']) ){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="headingbeverage2pri">
												<div class="panel-body">
													<div class="form-group">
														<select class="form-control" name="price_index">
															<option value="" selected>Select Price Index</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from price_index order by nama_price_index asc");
															while($d=mysqli_fetch_array($sql)){
																if($d['id_price_index']==$_GET['price_index']){
																	echo"<option value='$d[id_price_index]' selected>$d[nama_price_index]</option>";
																}
																else{
																	echo"<option value='$d[id_price_index]'>$d[nama_price_index]</option>";
																}
															}
															?>
														</select>
													</div>
												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="headingbeverage2">
												<h4 class="panel-title">
													<a data-toggle="collapse" <?php if(!empty($_GET['making_methode'])){ echo "class='akt'";} ?> data-parent="#accordion" href="#collapsebeverage2" aria-expanded="true" aria-controls="collapsebeverage5">Making Methode</a>
												</h4>
											</div>
											<div id="collapsebeverage2" <?php if(!empty($_GET['making_methode'])){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="headingbeverage2">
												<div class="panel-body">
													<?php
													$coo='';
													foreach($_GET['making_methode'] as $currency){
														$coo.=$currency."+";
													}
													$cooking_methode=tulis_cekboxxer($coo,$koneksi,'making_methode','mkm');
													echo"$cooking_methode";
													?>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="headingbeverage3">
												<h4 class="panel-title">
													<a data-toggle="collapse" <?php if(!empty($_GET['overall']) || !empty($_GET['clean']) || !empty($_GET['flavor']) || !empty($_GET['freshness'])
														|| !empty($_GET['cooking']) || !empty($_GET['aroma']) || !empty($_GET['serving'])){ echo "class='akt'";} ?> data-parent="#accordion" href="#collapsebeverage3" aria-expanded="true" aria-controls="collapsebeverage3">Beverage Rating</a>
												</h4>
											</div>
											<div id="collapsebeverage3" <?php if(!empty($_GET['overall']) || !empty($_GET['clean']) || !empty($_GET['flavor']) || !empty($_GET['freshness'])
												|| !empty($_GET['cooking']) || !empty($_GET['aroma']) || !empty($_GET['serving'])){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="headingbeverage3">
												<div class="panel-body">
													<div class="form-group">
														<label>Overall</label>
														<select class="form-control" name="overall">
															<option value="">Select Overate Rate</option>
															<option value="1" <?php if($_GET['overall']==1){ echo "selected";}?>>1 Star</option>
															<option value="2" <?php if($_GET['overall']==2){ echo "selected";}?>>2 Star</option>
															<option value="3" <?php if($_GET['overall']==3){ echo "selected";}?>>3 Star</option>
															<option value="4" <?php if($_GET['overall']==4){ echo "selected";}?>>4 Star</option>
															<option value="5" <?php if($_GET['overall']==5){ echo "selected";}?>>5 Star</option>
														</select>
													</div>
													<div class="form-group">
														<label>Cleanliness</label>
														<select class="form-control select" name="clean">
															<option value="" selected>Select Rate of Cleanliness</option>
															<option value="0.21" <?php if($_GET['clean']=='0.21'){ echo "selected";}?>>1 Star</option>
															<option value="0.42" <?php if($_GET['clean']=='0.42'){ echo "selected";}?>>2 Star</option>
															<option value="0.63" <?php if($_GET['clean']=='0.63'){ echo "selected";}?>>3 Star</option>
															<option value="0.84" <?php if($_GET['clean']=='0.84'){ echo "selected";}?>>4 Star</option>
															<option value="1.05" <?php if($_GET['clean']=='1.05'){ echo "selected";}?>>5 Star</option>
														</select>
													</div>
													<div class="form-group">
														<label>Flavor</label>
														<select class="form-control select" name="flavor">
															<option value="" selected>Select Rate of Flavor</option>
															<option value="0.20" <?php if($_GET['flavor']=='0.20'){ echo "selected";}?>>1 Star</option>
															<option value="0.40" <?php if($_GET['flavor']=='0.40'){ echo "selected";}?>>2 Star</option>
															<option value="0.60" <?php if($_GET['flavor']=='0.60'){ echo "selected";}?>>3 Star</option>
															<option value="0.80" <?php if($_GET['flavor']=='0.80'){ echo "selected";}?>>4 Star</option>
															<option value="1.00" <?php if($_GET['flavor']=='1.00'){ echo "selected";}?>>5 Star</option>
														</select>
													</div>
													<div class="form-group">
														<label>Freshness</label>
														<select class="form-control select" name="freshness">
															<option value="" selected>Select Rate of Freshness</option>
															<option value="0.18" <?php if($_GET['freshness']=='0.18'){ echo "selected";}?>>1 Star</option>
															<option value="0.36" <?php if($_GET['freshness']=='0.36'){ echo "selected";}?>>2 Star</option>
															<option value="0.54" <?php if($_GET['freshness']=='0.54'){ echo "selected";}?>>3 Star</option>
															<option value="0.72" <?php if($_GET['freshness']=='0.72'){ echo "selected";}?>>4 Star</option>
															<option value="0.90" <?php if($_GET['freshness']=='0.90'){ echo "selected";}?>>5 Star</option>
														</select>
													</div>
													<div class="form-group">
														<label>Cooking</label>
														<select class="form-control select" name="cooking">
															<option value="" selected>Select Rate of Cooking</option>
															<option value="0.17" <?php if($_GET['cooking']=='0.17'){ echo "selected";}?>>1 Star</option>
															<option value="0.34" <?php if($_GET['cooking']=='0.34'){ echo "selected";}?>>2 Star</option>
															<option value="0.51" <?php if($_GET['cooking']=='0.51'){ echo "selected";}?>>3 Star</option>
															<option value="0.68" <?php if($_GET['cooking']=='0.68'){ echo "selected";}?>>4 Star</option>
															<option value="0.85" <?php if($_GET['cooking']=='0.85'){ echo "selected";}?>>5 Star</option>
														</select>
													</div>
													<div class="form-group">

														<label>Presentasion &amp; Aroma</label>
														<select class="form-control select" name="aroma">
															<option value="" selected>Select Rate of Presentasion &amp; Aroma</option>
															<option value="0.15" <?php if($_GET['aroma']=='0.15'){ echo "selected";}?>>1 Star</option>
															<option value="0.30" <?php if($_GET['aroma']=='0.30'){ echo "selected";}?>>2 Star</option>
															<option value="0.45" <?php if($_GET['aroma']=='0.45'){ echo "selected";}?>>3 Star</option>
															<option value="0.60" <?php if($_GET['aroma']=='0.60'){ echo "selected";}?>>4 Star</option>
															<option value="0.75" <?php if($_GET['aroma']=='0.75'){ echo "selected";}?>>5 Star</option>
														</select>
													</div>
													<div class="form-group">
														<label>Serving</label>
														<select class="form-control select" name="serving">
															<option value="" selected>Select Rate of Serving</option>
															<option value="0.09" <?php if($_GET['serving']=='0.09'){ echo "selected";}?>>1 Star</option>
															<option value="0.18" <?php if($_GET['serving']=='0.18'){ echo "selected";}?>>2 Star</option>
															<option value="0.27" <?php if($_GET['serving']=='0.27'){ echo "selected";}?>>3 Star</option>
															<option value="0.36" <?php if($_GET['serving']=='0.36'){ echo "selected";}?>>4 Star</option>
															<option value="0.45" <?php if($_GET['serving']=='0.45'){ echo "selected";}?>>5 Star</option>
														</select>
													</div>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="headingbeverage2tag">
												<h4 class="panel-title">
													<a data-toggle="collapse" <?php if(!empty($_GET['tag'])){ echo "class='akt'";} ?> data-parent="#accordionfood" href="#collapsebeverage2tag" aria-expanded="true" aria-controls="collapsebeverage2tag">Tag</a>
												</h4>
											</div>
											<div id="collapsebeverage2tag" <?php if(!empty($_GET['tag']) ){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="headingbeverage2tag">
												<div class="panel-body">
													<div class="form-group">
														<div class="input-group">
																<input class="form-control tag" name="tag" value="<?php echo"$_GET[tag]";?>">
																<div class="input-group-addon">x</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<button type="submit" class="btn btn-success btn-block">Search</button>
										<a href="https://www.foodieguidances.com/pages/beverage/search/" class="btn btn-warning btn-block">Reset</a>
									</form>
								</div>
							</div>
						</div>
					</div>
					
					<?php include 'config/inc/search_process.php' ?>										
					<?php include 'config/inc/search_events.php' ?>

					<div class="panel-groupmb10 mt10" id="accordionsssasu" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default head-pan">
							<div class="panel-heading jud-pan" role="tab" id="recipe1" data-toggle="collapse" data-parent="#accordionsssasu" href="#collapsesssasu1" aria-expanded="true" aria-controls="collapsesssasu1">
								<h4 class="panel-title">
									❯ Recipe
								</h4>
							</div>
							<div id="collapsesssasu1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="recipe1">
								<div class="panel-body">
									<form method="get" action="<?php echo"$base_url/pages/recipe/search/"; ?>" class="border-form" id="ale-form" onsubmit="myFunction()">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a href="<?php echo"$base_url" ?>/pages/recipe/search/?sort=r.dilihat+DESC&batas=25">Popular</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a href="<?php echo"$base_url" ?>/pages/recipe/search/?sort=direkomendasi+DESC&batas=25">Recomended</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="recipe1">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapsesssasug1" aria-expanded="true" aria-controls="collapsesssasu1">Advance Filter</a>
												</h4>
											</div>
											<div id="collapsesssasug1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="recipe1">
												<div class="panel-body">
													<div class="form-group">
														<label>Category</label>
														<select class="form-control" name="category">
															<option value="" selected>Select Category</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from recipe_category order by nama_recipe_category asc");
															while($b=mysqli_fetch_array($sql)){
																echo"<option value='$b[id_recipe_category]'>$b[nama_recipe_category]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Cuisine</label>
														<select class="form-control" name="cuisine">
															<option value="" selected>Select Cuisine</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from cuisine order by nama_cuisine asc");
															while($c=mysqli_fetch_array($sql)){
																echo"<option value='$c[id_cuisine]'>$c[nama_cuisine]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>MSG Level</label>
														<select class="form-control" name="msg_level">
															<option value="" selected>Select MSG Level</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from msg_level order by nama_msg_level asc");
															while($e=mysqli_fetch_array($sql)){
																echo"<option value='$e[id_msg_level]'>$e[nama_msg_level]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Difficulty</label>
														<select class="form-control" name="difficulty">
															<option value="" selected>Select Difficulty</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from difficulty order by nama_difficulty asc");
															while($d=mysqli_fetch_array($sql)){
																echo"<option value='$d[id_difficulty]'>$d[nama_difficulty]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Cooking Methode</label>
														<select class="form-control" name="cooking_methode">
															<option value="" selected>Select Cooking Methode</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from cooking_methode order by nama_cooking_methode asc");
															while($a=mysqli_fetch_array($sql)){
																echo"<option value='$a[id_cooking_methode]'>$a[nama_cooking_methode]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Duration</label>
														<select class="form-control" name="duration">
															<option value="" selected>Select Duration</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from duration order by nama_duration asc");
															while($f=mysqli_fetch_array($sql)){
																echo"<option value='$f[id_duration]'>$f[nama_duration]</option>";
															}
															?>
														</select>
													</div>
												</div>
											</div>
										</div>
										<button type="submit" class="btn btn-success btn-block">Search</button>
										<a href="https://www.foodieguidances.com/pages/beverage/search/" class="btn btn-warning btn-block">Reset</a>
									</form>
								</div>
							</div>
						</div>
					</div>

					<div class="panel-groupmb10 mt10" id="accordionsssasulk" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default head-pan">
							<div class="panel-heading jud-pan" role="tab" id="fgmart1" data-toggle="collapse" data-parent="#accordionsssasulk" href="#collapsesssasulk1" aria-expanded="true" aria-controls="collapsesssasulk1">
								<h4 class="panel-title">
									❯ FGMART
								</h4>
							</div>
							<div id="collapsesssasulk1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="fgmart1">
								<form method="get" action="<?php echo"$base_url/pages/fgmart/search/"; ?>" class="border-form" id="ale-form" onsubmit="myFunction()">
									<div class="panel-body">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a href="<?php echo"$base_url" ?>/pages/fgmart/search/?sort=r.dilihat+DESC&batas=25">Popular</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a href="<?php echo"$base_url" ?>/pages/fgmart/search/?sort=dilike+DESC&batas=25">Liked</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="fgmart1">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordionsssasulks" href="#collapsesssasulks1" aria-expanded="true" aria-controls="collapse1">Advanced Filter</a>
												</h4>
											</div>
											<div id="collapsesssasulks1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="fgmart1">
												<div class="panel-body">
													<div class="form-group">
														<label>Category</label>
														<select class="form-control" name="category">
															<option value="" selected>Select Category</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from photo_category order by nama_photo_category asc");
															while($b=mysqli_fetch_array($sql)){
																echo"<option value='$b[id_photo_category]'>$b[nama_photo_category]</option>";
															}
															?>
														</select>
													</div>
												</div>
											</div>
										</div>
										<button type="submit" class="btn btn-success btn-block">Search</button>
										<a href="https://www.foodieguidances.com/pages/beverage/search/" class="btn btn-warning btn-block">Reset</a>
									</div>
								</form>
							</div>
						</div>
					</div>

					<div class="panel-groupmb10 mt10" id="accordionsssasul" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default head-pan">
							<div class="panel-heading jud-pan" role="tab" id="coupon1" data-toggle="collapse" data-parent="#accordionsssasul" href="#collapsesssasul1" aria-expanded="true" aria-controls="collapsesssasul1">
								<h4 class="panel-title">
									❯ Coupon
								</h4>
							</div>
							<div id="collapsesssasul1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="coupon1">
								<form method="get" action="<?php echo"$base_url/pages/coupon/search/"; ?>" class="border-form" id="ale-form" onsubmit="myFunction()">
									<div class="panel-body">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a href="<?php echo"$base_url" ?>/pages/coupon/search/?sort=r.dilihat+DESC&batas=25">Popular</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapsesssasula14" aria-expanded="true" aria-controls="collapse1">Coupon Title</a>
												</h4>
											</div>
											<div id="collapsesssasula14" class="panel-collapse collapse" role="tabpanel" aria-labelledby="coupon1">
												<div class="panel-body">
													<div class="form-group">
														<label>Coupon Title</label>
														<input class="form-control select" name="keyWorld" id="search">
													</div>
												</div>
											</div>
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a href="<?php echo"$base_url" ?>/pages/coupon/search/?sort=dilike+DESC&batas=25">Liked</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapsesssasula1" aria-expanded="true" aria-controls="collapse1">Location Filter</a>
												</h4>
											</div>
											<div id="collapsesssasula1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="coupon1">
												<div class="panel-body">
													<div class="form-group">
														<label>Country</label>
														<select class="form-control" name="country" id="negara4">
															<option value="" selected>Select Country</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from negara order by nama_negara asc");
															while($b=mysqli_fetch_array($sql)){
																echo"<option value='$b[id_negara]'>$b[nama_negara]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>States/Province</label>
														<select class="form-control" name="state" id="propinsi4">
															<option value="" selected>Select States/ Province</option>
														</select>
													</div>
													<div class="form-group">
														<label>City</label>
														<select class="form-control" name="city" id="kota4">
															<option value="" selected>Select City</option>
														</select>
													</div>

												</div>
											</div>
										</div>
										<button type="submit" class="btn btn-success btn-block">Search</button>
										<a href="https://www.foodieguidances.com/pages/beverage/search/" class="btn btn-warning btn-block">Reset</a>
									</div>
								</form>
							</div>
						</div>
					</div>

					<div class="panel-groupmb10 mt10" id="accordionsssasulkp" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default head-pan">
							<div class="panel-heading jud-pan" role="tab" id="artikel1" data-toggle="collapse" data-parent="#accordionsssasulkp" href="#collapsesssasulkp1" aria-expanded="true" aria-controls="collapsesssasulkp1">
								<h4 class="panel-title">
									❯ Article
								</h4>
							</div>
							<div id="collapsesssasulkp1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="artikel1">
								<form method="get" action="<?php echo"$base_url/pages/article/search/"; ?>" class="border-form" id="ale-form" onsubmit="myFunction()">
									<div class="panel-body">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="artikel1">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordionsssasulkpg1" href="#collapsesssasulkpa1" aria-expanded="true" aria-controls="collapse1">Advanced Filter</a>
												</h4>
											</div>
											<div id="collapsesssasulkpa1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="artikel1">
												<div class="panel-body">
													<div class="form-group">
														<label>Article Title</label>
														<input class="form-control" name="keyWorld" id="search">
													</div>
												</div>
											</div>
										</div>
										<button type="submit" class="btn btn-success btn-block">Search</button>
										<a href="https://www.foodieguidances.com/pages/beverage/search/" class="btn btn-warning btn-block">Reset</a>
									</div>
								</form>
							</div>
						</div>
					</div>

					<div class="panel-groupmb10 mt10" id="accordionsssasulkpw" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default head-pan">
							<div class="panel-heading jud-pan" role="tab" id="video1">
								<h4 class="panel-title">
									❯ Video
								</h4>
							</div>
							<div id="collapsesssasulkpw1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="video1">
								<form method="get" action="<?php echo"$base_url/pages/video/search/"; ?>" class="border-form" id="ale-form" onsubmit="myFunction()">
									<div class="panel-body">
										<div class="panel panel-default">
											<div class="panel panel-default">
												<div class="panel-heading" role="tab" id="video1">
													<h4 class="panel-title">
														<a data-toggle="collapse" data-parent="#accordionsssasulkpw1" href="#collapsesssasulkpwj1" aria-expanded="true" aria-controls="collapse1">Advanced Filter</a>
													</h4>
												</div>
												<div id="collapsesssasulkpwj1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="video1">
													<div class="panel-body">
														<div class="form-group">
															<label>Video Title</label>
															<input class="form-control select" name="keyWorld" id="search">
														</div>
														<div class="form-group">
															<label>Category</label>
															<select class="form-control" name="category">
																<option value="" selected>Select Category</option>
																<?php
																$sql=mysqli_query($koneksi,"select * from video_category order by nama_video_category asc");
																while($d=mysqli_fetch_array($sql)){
																	echo"<option value='$d[id_video_category]'>$d[nama_video_category]</option>";
																}
																?>
															</select>
														</div>
													</div>
												</div>
											</div>
										</div>
										<button type="submit" class="btn btn-success btn-block">Search</button>
										<a href="https://www.foodieguidances.com/pages/beverage/search/" class="btn btn-warning btn-block">Reset</a>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-8 col-8a no-padding">
				<h4 class="f-merah no-mb" style="">

					<?php
						echo"<div class='breadcrumba flat'>";
						if (!empty($_GET['country'])) {
							$sql=mysqli_query($koneksi,"select * from negara where id_negara='$_GET[country]'");
							while($d=mysqli_fetch_array($sql)){
								echo"<a href='#'>$d[nama_negara]</a>";
							}
						}
						if (!empty($_GET['state'])) {
							$sql=mysqli_query($koneksi,"select * from propinsi where id_propinsi='$_GET[state]'");
							while($d=mysqli_fetch_array($sql)){
								echo"<a href='#'>$d[nama_propinsi]</a>";
							}
						}

						if (!empty($_GET['city'])) {
							$sql=mysqli_query($koneksi,"select * from kota where id_kota='$_GET[city]'");
							while($d=mysqli_fetch_array($sql)){
								echo"<a href='#'>$d[nama_kota]</a>";
							}
						}
						if ($_GET['sort']=='total DESC') {
							echo "<a href='#'>Top Ranking</a>";
						}
						if ($_GET['sort']=='r.dilihat DESC') {
							echo "<a href='#'>Popular</a>";
						}
						if ($_GET['sort']=='direkomendasi DESC') {
							echo "<a href='#'>Recomended</a>";
						}
						if (!empty($_GET['category'])) {
							$sql=mysqli_query($koneksi,"select * from beverage_category where id_beverage_category='$_GET[category]'");
							while($d=mysqli_fetch_array($sql)){
								echo"<a href='#'>$d[nama_beverage_category]</a>";
							}
						}
						if (!empty($_GET['price_index'])) {
							$sql=mysqli_query($koneksi,"select * from price_index where id_price_index='$_GET[price_index]'");
							while($d=mysqli_fetch_array($sql)){
								echo"<a href='#'>$d[nama_price_index]</a>";
							}
						}
						if (!empty($_GET['making_methode'])) {
							$sql=mysqli_query($koneksi,"select * from making_methode where id_making_methode='$_GET[making_methode]'");
							while($d=mysqli_fetch_array($sql)){
								echo"<a href='#'>$d[nama_making_methode]</a>";
							}
						}
						if (!empty($_GET['tag'])) {
								echo"<a href='#'>$_GET[tag]</a>";
						}
						echo"</div>";
					?>
				</h4>

				<div class="form-inline border-form sort-form ml10-j">
					<div class="row">
						<div class="col-6 col-6a">
							<div class="form-group">
								<select class="form-control" name="sort">
									<option value="r.id_beverage DESC" <?php if(isset($_GET['sort'])){if($_GET['sort']=="r.id_beverage DESC"){echo"selected";}} ?>>Latest</option>
									<option value="total DESC" <?php if(isset($_GET['sort'])){if($_GET['sort']=="total DESC"){echo"selected";}} ?>>Top Ranking</option>
									<option value="r.nama_beverage ASC" <?php if(isset($_GET['sort'])){if($_GET['sort']=="r.nama_beverage ASC"){echo"selected";}} ?>>Name A-Z</option>
									<option value="r.nama_beverage DESC" <?php if(isset($_GET['sort'])){if($_GET['sort']=="r.nama_beverage DESC"){echo"selected";}} ?>>Name Z-A</option>
									<option value="r.dilihat DESC" <?php if(isset($_GET['sort'])){if($_GET['sort']=="r.dilihat DESC"){echo"selected";}} ?>>Popular</option>
									<option value="direkomendasi DESC" <?php if(isset($_GET['sort'])){if($_GET['sort']=="direkomendasi DESC"){echo"selected";}} ?>>Recomended</option>
									<option value="dilike DESC" <?php if(isset($_GET['sort'])){if($_GET['sort']=="dilike DESC"){echo"selected";}} ?>>Favored</option>
								</select>
							</div>
						</div>
						<div class="col-6 col-6a">
							<div class="form-group">
								<select class="form-control" name="batas">
									<option value="25" <?php if(isset($_GET['batas'])){if($_GET['batas']==25){echo"selected";}} ?>>Show 25 per Page</option>
									<option value="50" <?php if(isset($_GET['batas'])){if($_GET['batas']==50){echo"selected";}} ?>>Show 50 per Page</option>
									<option value="100" <?php if(isset($_GET['batas'])){if($_GET['batas']==100){echo"selected";}} ?>>Show 100 per Page</option>
								</select>
							</div>
						</div>
						<div class="col-4  mt-s10"><button type="submit" class="btn btn-danger btn-block">Submit</button></div>
					</div>
				</div>
				<div class="media jarak1">
					<?php
					$kondisi=" WHERE r.id_beverage <> 0";
					if(!empty($_GET['keyWorld'])){
						$kondisi .= " AND r.nama_beverage LIKE '%$_GET[keyWorld]%' OR c.nama_beverage_category LIKE '%$_GET[keyWorld]%' OR id_tag LIKE '%$_GET[keyWorld]%'";
					}
					if(!empty($_GET['tag'])){
						$kondisi .= " AND r.id_tag like '%$_GET[tag]%'";
					}
					if(!empty($_GET['category'])){
						$kondisi .= " AND r.id_beverage_category = '$_GET[category]'";
					}
					if(!empty($_GET['price_index'])){
						$kondisi .= " AND r.id_price_index = '$_GET[price_index]'";
					}
					if(!empty($_GET['country'])){
						$kondisi .= " AND w.id_negara = '$_GET[country]'";
					}
					if(!empty($_GET['state'])){
						$kondisi .= " AND w.id_propinsi = '$_GET[state]'";
					}
					if(!empty($_GET['city'])){
						$kondisi .= " AND w.id_kota = '$_GET[city]'";
					}
					if(!empty($_GET['making_methode'])){
						$curr='';
						foreach($_GET['making_methode'] as $currency){
							$curr.="making_methode LIKE '%".$currency."%' AND ";
						}
						$cusss=rtrim($curr, " AND ");
						$kondisi .= " AND $cusss";
					}
					if(!empty($_GET['overall'])){
						$kondisi .= " AND (SELECT (SUM(f.cleanlines) + SUM(f.flavor) + SUM(f.freshness) + SUM(f.cooking) + SUM(f.aroma) + SUM(f.serving)) / COUNT(f.id_member) FROM beverage_rating f WHERE f.id_beverage =r.id_beverage) >= '$_GET[overall]'";
					}

					else{
						if(!empty($_GET['clean'])){
							$kondisi .= " AND (SELECT SUM(cleanlines)/ COUNT(id_member) AS total FROM beverage_rating where beverage_rating.id_beverage=r.id_beverage) >= '$_GET[clean]'";
						}
						if(!empty($_GET['flavor'])){
							$kondisi .= " AND (SELECT SUM(flavor)/ COUNT(id_member) AS total FROM beverage_rating where beverage_rating.id_beverage=r.id_beverage) >= '$_GET[flavor]'";
						}
						if(!empty($_GET['freshness'])){
							$kondisi .= " AND (SELECT SUM(freshness)/ COUNT(id_member) AS total FROM beverage_rating where beverage_rating.id_beverage=r.id_beverage) >= '$_GET[freshness]'";
						}
						if(!empty($_GET['cooking'])){
							$kondisi .= " AND (SELECT SUM(cooking)/ COUNT(id_member) AS total FROM beverage_rating where beverage_rating.id_beverage=r.id_beverage) >= '$_GET[cooking]'";
						}
						if(!empty($_GET['aroma'])){
							$kondisi .= " AND (SELECT SUM(aroma)/ COUNT(id_member) AS total FROM beverage_rating where beverage_rating.id_beverage=r.id_beverage) >= '$_GET[aroma]'";
						}
						if(!empty($_GET['serving'])){
							$kondisi .= " AND (SELECT SUM(serving)/ COUNT(id_member) AS total FROM beverage_rating where beverage_rating.id_beverage=r.id_beverage) >= '$_GET[serving]'";
						}
					}
					$p      = new Paging;
					$batas  = 25;
					if(isset($_GET['batas'])){
						$batas  = $_GET['batas'];
					}
					// $sort="r.nama_beverage ASC";
					// if(isset($_GET['sort'])){
					// 	$sort  = $_GET['sort'];
					// }
					// if ($_GET['sort']=='total DESC') {
					// 	  $kondisi .=" AND f.cleanlines <> 0";
					// }

					if($_GET['sort']=="total DESC") {
						$sort =  "total DESC,jumlah_vote DESC,dilike DESC,r.nama_beverage ASC";
					}
					elseif(!empty($_GET['sort'])) {
						$sort  = $_GET['sort'];
					}
					else{
						$sort="r.id_beverage DESC";
					}
					$posisi = $p->cariPosisi($batas);
					$no = $posisi+1;


						$sql="SELECT *, f.cleanlines,
						(SELECT (SUM(f.cleanlines) + SUM(f.flavor) + SUM(f.freshness) + SUM(f.cooking) + SUM(f.aroma) + SUM(f.serving)) / COUNT(f.id_member) FROM beverage_rating f WHERE f.id_beverage =r.id_beverage) AS total,
						(select count(id_beverage_rating) as jumlah from beverage_rating where beverage_rating.id_beverage=r.id_beverage) as jumlah_vote,
						(select count(h.id_beverage) from beverage_here h where h.id_beverage=r.id_beverage) as jumlah,
						(select p.gambar_beverage_photo from beverage_photo p where p.id_beverage=r.id_beverage order by p.id_beverage_photo desc limit 1) as gambar,
						(SELECT COUNT(h.id_member) FROM beverage_like h WHERE h.id_beverage=r.id_beverage) AS dilike,
						(SELECT COUNT(h.id_member) FROM beverage_rekomendasi h WHERE h.id_beverage=r.id_beverage) AS direkomendasi,
						(select count(h.id_beverage) from beverage h where h.id_restaurant=r.id_restaurant and h.id_beverage<>r.id_beverage) as jumlah FROM beverage r
						LEFT JOIN beverage_category c ON r.id_beverage_category = c.id_beverage_category
 						LEFT JOIN beverage_rating f ON r.id_beverage =f.id_beverage
						LEFT JOIN restaurant w ON r.id_restaurant = w.id_restaurant
						LEFT JOIN price_index p ON r.id_price_index = p.id_price_index ".$kondisi." GROUP BY r.id_beverage ORDER BY ".$sort." LIMIT $posisi,$batas";

					$sql2="SELECT *, f.cleanlines  FROM beverage r
					LEFT JOIN beverage_category c ON r.id_beverage_category = c.id_beverage_category
					LEFT JOIN restaurant w ON w.id_restaurant = r.id_restaurant
					LEFT JOIN beverage_rating f ON f.id_beverage =r.id_beverage LEFT JOIN price_index p ON r.id_price_index = p.id_price_index".$kondisi;

					//echo $sql;

					$latest=mysqli_query($koneksi,$sql);
					$jmldata = mysqli_num_rows(mysqli_query($koneksi,$sql2));
					if($jmldata<>0){
						while($b=mysqli_fetch_array($latest)){
							$slug=seo($b['nama_beverage']);
							$post=date("jS M, Y", strtotime($b['tgl_post']));
							$total_rating=number_format((float)$b['total'], 2, '.', '');
							$id = id_masking($b['id_beverage']);
							echo"<div class='row col-4a'>
								<div class='col-1'>$no.</div>
								<div class='col-8 thumb col-8a no-padding'>
									<a href='$base_url/pages/beverage/info/$id/$slug'><img data-original='$base_url/assets/img/beverage/big_$b[gambar]' class='lazy' width='220' height='165'></a>
									<a href='$base_url/pages/beverage'><span>Beverage</span></a>
								</div>
								<div class='col-7 info col-8a'>
									<em>posted on $post</em>
									<h3 class='sembunyi'><a href='$base_url/pages/beverage/info/$id/$slug'>$b[nama_beverage]</a></h3>
									<ul class='list-unstyled f-12 '>
										<li><em>category</em> <a href='$base_url/pages/beverage/search/?category=$b[id_beverage_category]'>$b[nama_beverage_category]</a></li>
										<li><em>cost</em> <a href='$base_url/pages/beverage/search/?price_index=$b[id_price_index]'>$b[nama_price_index]</a></li>
										<li>$b[dilike] Like <span class='bullet'>&#8226;</span> $b[jumlah] Taste it</li>
									</ul>";
									if($total_rating<>0){
										echo"<div class='bintang'><input type='hidden' class='rating' data-filled='fa fa-star' data-empty='fa fa-star-o' readonly='readonly' value='$total_rating'>&nbsp;&nbsp;&nbsp;&nbsp;<strong class='f-18'>$total_rating</strong>&nbsp;&nbsp;<em>from $b[jumlah_vote] vote</em></div>";
									}
								echo"</div>
							</div>";
							$no++;
							}
							echo"</div><nav class='ac'>";
							$link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
							$jmlhalaman  = $p->jumlahHalaman($jmldata, $batas);
							$linkHalaman = $p->navHalaman($link,$_GET['page'], $jmlhalaman);
							echo "<ul class='pagination'>$linkHalaman</ul>";
							echo"</nav>";
					}
					else{
						echo"<div class='row'>
							<div class='col-16'>
								<h4 class='f-merah'>Oops! We can't found what you are looking for.</h4>
								<p>Would you like to share it with us?.</p>
								<a href=";
									if(!empty($_SESSION['food_member'])){ echo"'$base_url/$username/my-beverage/new'";}else{ echo"'$base_url/login-area'";}
							    echo" class='btn btn-danger'>Submit New Beverage Review</a>
							</div>
						</div></div>";
					}
					?>
			</div>
			<div class="col-4 mene-atas"><br><br>
				  	<?php include"config/inc/search.php"; ?>
				  	<?php include"config/inc/iklan.php"; ?>
				<div class="fb-like-box" data-href="https://www.facebook.com/foodieguidances#_=_" data-width="220" data-height="350" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
			</div>
		</div>
		</form>
    </div>
	<?php include"config/inc/footer.php"; ?>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap-rating.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.superslides.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/select2.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.lazyload.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/theme.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
	<script>
		 $(document).ready(function() {
			 $('.drawer').drawer();

		 $('.tutup').click(function(){
			 $('.iklanan').toggleClass('hilang', 500);
		 });

		 $('.cart').click(function(e){
			 e.stopPropagation();
			 $('.dis-cart').toggle();
			 $('.dis-plus').hide();
			 $('.dis-share').hide();
		 });
		 $('.plus').click(function(e){
			 e.stopPropagation();
			 $('.dis-plus').toggle();
			 $('.dis-cart').hide();
			 $('.dis-share').hide();
		 });
		 $('.share').click(function(e){
			 e.stopPropagation();
			 $('.dis-share').toggle();
			 $('.dis-plus').hide();
			 $('.dis-cart').hide();
		 });
		 $('.drawer-toggle').click(function(){
			 $('.dis-cart').hide();
			 $('.dis-plus').hide();
			 $('.dis-share').hide();
		 });
		 $('.btn-cari').click(function(){
			 $('.search_kiri').toggleClass('search_kiri_tam', 500);
		 });
		 $(document).click(function () {
				var $el = $(".dis-share");
				if ($el.is(":visible")) {
					 $el.fadeOut(200);
				}
				var $ela = $(".dis-plus");
				if ($ela.is(":visible")) {
					 $ela.fadeOut(200);
				}
				var $elu = $(".dis-cart");
				if ($elu.is(":visible")) {
					 $elu.fadeOut(200);
				}
			});

		 });
	</script>
	<div id="fb-root"></div>
	<script type="text/javascript">
		$('input[type="reset"]').click(function() {
			$("select").select2().val();
			$("select").select2().val();
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.input-group-addon').click(function(){
				$('.tag').val('');
			});
		});
	</script>
	<script>
	function myFunction(){
		var myForm = document.getElementById('ale-form');
		var allInputs = myForm.getElementsByTagName('select');
		var teks = myForm.getElementsByTagName('input');
		var input, i, grup, a;

		for(i = 0; input = allInputs[i]; i++) {
			if(input.getAttribute('name') && !input.value) {
				input.setAttribute('name', '');
			}
		}
		for(a = 0; grup = teks[a]; a++) {
			if(grup.getAttribute('name') && !grup.value) {
				grup.setAttribute('name', '');
			}
		}
	}
	$(document).ready(function () {
		$('.rating').rating();
		$("select").select2();
		$("img.lazy").lazyload({
			effect : "fadeIn"
		});
		$("#negara").change(function(){
			var id = $("#negara").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_propinsi.php",
				data: "id=" + id,
				success: function(data){
					$("#propinsi").html(data);
					$("#propinsi").fadeIn(2000);
					$('#propinsi').selectpicker('refresh');
				}
			});
		});
		$("#propinsi").change(function(){
			var id = $("#propinsi").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_kota.php",
				data: "id=" + id,
				success: function(data){
					$("#kota").html(data);
					$("#kota").fadeIn(2000);
					$('#kota').selectpicker('refresh');
				}
			});
		});
		$("#kota").change(function(){
			var id = $("#kota").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_mall.php",
				data: "id=" + id,
				success: function(data){
					$("#mall").html(data);
					$("#mall").fadeIn(2000);
				}
			});
		});

		$("#negara2").change(function(){
			var id = $("#negara2").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_propinsi.php",
				data: "id=" + id,
				success: function(data){
					$("#propinsi2").html(data);
					$("#propinsi2").fadeIn(2000);
					$('#propinsi2').selectpicker('refresh');
				}
			});
		});
		$("#propinsi2").change(function(){
			var id = $("#propinsi2").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_kota.php",
				data: "id=" + id,
				success: function(data){
					$("#kota2").html(data);
					$("#kota2").fadeIn(2000);
					$('#kota2').selectpicker('refresh');
				}
			});
		});
		$("#kota2").change(function(){
			var id = $("#kota2").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_mall.php",
				data: "id=" + id,
				success: function(data){
					$("#mall").html(data);
					$("#mall").fadeIn(2000);
				}
			});
		});

		$("#negara3").change(function(){
			var id = $("#negara3").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_propinsi.php",
				data: "id=" + id,
				success: function(data){
					$("#propinsi3").html(data);
					$("#propinsi3").fadeIn(2000);
					$('#propinsi3').selectpicker('refresh');
				}
			});
		});
		$("#propinsi3").change(function(){
			var id = $("#propinsi3").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_kota.php",
				data: "id=" + id,
				success: function(data){
					$("#kota3").html(data);
					$("#kota3").fadeIn(2000);
					$('#kota3').selectpicker('refresh');
				}
			});
		});
		$("#kota3").change(function(){
			var id = $("#kota3").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_mall.php",
				data: "id=" + id,
				success: function(data){
					$("#mall").html(data);
					$("#mall").fadeIn(2000);
				}
			});
		});

		$("#negara4").change(function(){
			var id = $("#negara4").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_propinsi.php",
				data: "id=" + id,
				success: function(data){
					$("#propinsi4").html(data);
					$("#propinsi4").fadeIn(2000);
					$('#propinsi4').selectpicker('refresh');
				}
			});
		});
		$("#propinsi4").change(function(){
			var id = $("#propinsi4").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_kota.php",
				data: "id=" + id,
				success: function(data){
					$("#kota4").html(data);
					$("#kota4").fadeIn(2000);
					$('#kota4').selectpicker('refresh');
				}
			});
		});
		$("#kota4").change(function(){
			var id = $("#kota4").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_resto.php",
				data: "id=" + id,
				success: function(data){
					$("#mall").html(data);
					$("#mall").fadeIn(2000);
				}
			});
		});
	});
	/*=====For developer to config ====*/
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=348272391978609&version=v2.0";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script>
</body>
</html>
