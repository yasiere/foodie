<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";
include "config/func/id_masking.php";
date_default_timezone_set("Asia/Jakarta");
$sekarang=date("Y-m-d");
$id = id_masking($_GET['id']);
$r=mysqli_fetch_array(mysqli_query($koneksi,"select id_events_beverage, nama_events_beverage from events_beverage where id_events_beverage='$id'"));

$seo=seo($r['nama_events_beverage']);

if(isset($_SESSION['food_member'])){ 
	if($_GET['fungsi']=="1"){
        $ada=mysqli_num_rows(mysqli_query($koneksi,"select id_events_beverage_like from events_beverage_like where id_events_beverage='$id' and id_member='$_SESSION[food_member]' and tgl_like='$sekarang'"));
		if($ada==0){
			mysqli_query($koneksi,"insert into events_beverage_like (id_member,id_events_beverage,tgl_like) values('$_SESSION[food_member]','$id','$sekarang')");
			$_SESSION['resto_notif']     = "suka";
		}
		else{
			$_SESSION['resto_notif']     = "suka_gagal";
		}
	}
	elseif($_GET['fungsi']=="2"){
        $ada=mysqli_num_rows(mysqli_query($koneksi,"select id_events_beverage_rekomendasi from events_beverage_rekomendasi where id_events_beverage='$id' and id_member='$_SESSION[food_member]' and tgl_rekomendasi='$sekarang'"));
		if($ada==0){
			mysqli_query($koneksi,"insert into events_beverage_rekomendasi (id_member,id_events_beverage,tgl_rekomendasi) values('$_SESSION[food_member]','$id','$sekarang')");
			$_SESSION['resto_notif']     = "rekomendasi";
		}
		else{
			$_SESSION['resto_notif']     = "rekomendasi_gagal";
		}
	}
	elseif($_GET['fungsi']=="3"){
		$ada_bookmark=mysqli_num_rows(mysqli_query($koneksi,"select id_bookmark from bookmark where id='$id' and jenis='events_beverage' and id_member='$_SESSION[food_member]'"));
		if($ada_bookmark==0){
			mysqli_query($koneksi,"insert into bookmark (id_member,jenis,id,tgl_bookmark) values('$_SESSION[food_member]','events_beverage','$id','$sekarang')");
			$_SESSION['resto_notif']     = "bookmark";
		}
		else{
			$_SESSION['resto_notif']     = "bookmark_gagal";
		}
	}
	elseif($_GET['fungsi']=="4"){
		$ada_here=mysqli_num_rows(mysqli_query($koneksi,"select id_events_beverage_here from events_here where id_events_beverage='$id' and id_member='$_SESSION[food_member]'"));
		if($ada_here==0){
			mysqli_query($koneksi,"insert into events_here (id_member,id_events_beverage,tgl_here) values('$_SESSION[food_member]','$id','$sekarang')");
			$_SESSION['resto_notif']     = "disini";
		}
		else{
			$_SESSION['resto_notif']     = "disini_gagal";
		}
	}
	elseif($_GET['fungsi']=="5"){
		$ada_report=mysqli_num_rows(mysqli_query($koneksi,"select id_events_beverage_report from events_beverage_report where id_events_beverage='$id' and id_member='$_SESSION[food_member]'"));
		if($ada_report==0){
			mysqli_query($koneksi,"insert into events_beverage_report (id_member,id_events_beverage,tgl_report) values('$_SESSION[food_member]','$id','$sekarang')");
			$_SESSION['resto_notif']     = "lapor";
		}
		else{
			$_SESSION['resto_notif']     = "lapor_gagal";
		}
	}
}
else{
	$_SESSION['resto_notif']     = "login_dulu";
}
header("Location: ".$base_url."/pages/events/beverage/".$_GET['id']."/".$seo);
?>
