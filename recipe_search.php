<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";
include "config/func/paging_search.php";
include "config/func/id_masking.php";
$auto_logout=1800000;
if(!empty($_SESSION['food_member'])){
	if (time()-$_SESSION['timestamp']>$auto_logout){
		session_destroy();
		session_unset();
		header("Location: ".$base_url."/auto-logout");
		exit();
	}else{
		$_SESSION['timestamp']=time();
	}
	include "config/func/member_data.php";
}
$active = "recipe";
$type = "recipe";
function tulis_cekbox($field,$koneksi,$judul) {
	$query ="select * from ".$judul." order by nama_".$judul;
	$r = mysqli_query($koneksi,$query);
	$_arrNilai = explode('+', $field);
	$str = '';
	$no=1;
	while ($w = mysqli_fetch_array($r)) {
		$_ck = (array_search($w[1], $_arrNilai) === false)? '' : 'checked';
		$str .= "<div class=''>
			<div class='radio radio-inline radio-danger'>
				<input type='radio' name='".$judul."' value='$w[1]' id='$judul$no' $_ck>
				<label for='$judul$no'> $w[1]</label>
			</div>
		</div>";
		$no++;
	}
	return $str;
}
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
    <title>Recipe Result - Foodie Guidances</title>
	<meta name="keywords" content="">
    <meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/select2.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/awesome-bootstrap-checkbox.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap-select.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>

	<link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
 <body style="position: initial;" class="drawer drawer--right">
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>
	<div class="container hook">
		<form method="get" action="<?php echo"$base_url/pages/recipe/search/"; ?>" class="border-form" id="ale-form" onsubmit="myFunction()">
		<div class="row">
			<div class="btn-cari">
				<a href="#">
					<img src="<?php echo"$base_url/assets/images/cari2.png"; ?>" alt="" height="100%" />
				</a>
			</div>

			<div class="col-4 search_kiri">
				<div class="border-form filter-form">
					<h4 class="panel-title" style="margin-top: 20px; color: rgb(237, 28, 36);">
						Advance Search
					</h4>
					<?php
						if(!empty($_GET['batas'])){
							$batas=$_GET['batas'];
						}
						else {
							$batas="25";
						}
					?>
					<div class="panel-group mt20 mb10" id="accordionsss" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default head-pan">
							<div class="panel-heading jud-pan" role="tab" id="resto1" data-toggle="collapse" data-parent="#accordionsss" href="#collapsesss1" aria-expanded="true" aria-controls="collapsesss1">
								<h4 class="panel-title">
									<a>❯ Restaurant</a>
								</h4>
							</div>
							<div id="collapsesss1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="resto1">
								<div class="panel-body">
									<form method="get" action="<?php echo"$base_url/pages/restaurant/search/"; ?>" class="border-form" id="ale-form" onsubmit="myFunction()">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a href="<?php echo"$base_url" ?>/pages/restaurant/search/?sort=total_vote+DESC&batas=25">Top Ranking</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a href="<?php echo"$base_url" ?>/pages/restaurant/search/?sort=r.dilihat+DESC&batas=25">Popular</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a href="<?php echo"$base_url" ?>/pages/restaurant/search/?sort=direkomendasi+DESC&batas=25">Recomended</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="heading14">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapse14" aria-expanded="true" aria-controls="collapse1">Opening Status</a>
												</h4>
											</div>
											<div id="collapse14" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading14">
												<div class="panel-body">
													<div class="form-group">
														<select class="form-group" name="opening">
															<option value="" selected>Select Opening Status</option>
															<option value="open">Open</option>
															<option value="close">Close</option>
															<option value="no">No Information</option>
														</select>
													</div>
												</div>
											</div>
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">Location Filter</a>
												</h4>
											</div>
											<div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
												<div class="panel-body">
													<div class="form-group">
														<label>Country</label>
														<select class="form-control" name="country" id="negara">
															<option value="" selected>Select Country</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from negara order by nama_negara asc");
															while($b=mysqli_fetch_array($sql)){
																echo"<option value='$b[id_negara]'>$b[nama_negara]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>States/Province</label>
														<select class="form-control" name="state" id="propinsi">
															<option value="" selected>Select States/ Province</option>
														</select>
													</div>
													<div class="form-group">
														<label>City</label>
														<select class="form-control" name="city" id="kota">
															<option value="" selected>Select City</option>
														</select>
													</div>
												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading8lan">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapse8lan" aria-expanded="true" aria-controls="collapse8lan">Landmark</a>
												</h4>
											</div>
											<div id="collapse8lan" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8lan">
												<div class="panel-body">
													<div class="form-group">
														<select class="form-control" name="landmark">
															<option value="" selected>Select Landmark</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from landmark order by nama_landmark asc");
															while($d=mysqli_fetch_array($sql)){
																echo"<option value='$d[id_landmark]'>$d[nama_landmark]</option>";
															}
															?>
														</select>
													</div>
												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading8sho">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapse8sho" aria-expanded="true" aria-controls="collapse8sho">Shopping Mall</a>
												</h4>
											</div>
											<div id="collapse8sho" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8sho">
												<div class="panel-body">
													<div class="form-group">
														<select name="mall" class="form-control" id="mall">
															<option value="">Select Shopping Mall</option>
														</select>
													</div>
												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading8bus">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapse8bus" aria-expanded="true" aria-controls="collapse8bus">Business Type</a>
												</h4>
											</div>
											<div id="collapse8bus" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8bus">
												<div class="panel-body">
													<div class="form-group">
														<label>Business Type</label>
														<select class="form-control" name="business_type">
															<option value="" selected>Select Business Type</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from type_of_business order by nama_type_of_business asc");
															while($a=mysqli_fetch_array($sql)){
																echo"<option value='$a[id_type_of_business]'>$a[nama_type_of_business]</option>";
															}
															?>
														</select>
													</div>
												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading5">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="true" aria-controls="collapse5">Type of Cuisine</a>
												</h4>
											</div>
											<div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
												<div class="panel-body">
													<?php
														$cuisine=tulis_cekbox($_GET['cuisine'],$koneksi,'cuisine');
														echo"$cuisine";
													?>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading2">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">Restaurant Rating</a>
												</h4>
											</div>
											<div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
												<div class="panel-body">
													<div class="form-group">
														<label>Overall</label>
														<select class="form-control" name="overall">
															<option value="" selected>Select Overate Rate</option>
															<option value="1">1 Star</option>
															<option value="2">2 Star</option>
															<option value="3">3 Star</option>
															<option value="4">4 Star</option>
															<option value="5">5 Star</option>
														</select>
													</div>
													<div class="form-group">
														<label>Cleanliness</label>
														<select class="form-control select" name="clean">
															<option value="" selected>Select Rate of Cleanliness</option>
															<option value="0.26">1 Star</option>
															<option value="0.52">2 Star</option>
															<option value="0.78">3 Star</option>
															<option value="1.04">4 Star</option>
															<option value="1.3">5 Star</option>
														</select>
													</div>
													<div class="form-group">
														<label>Customer Services</label>
														<select class="form-control select" name="services">
															<option value="" selected>Select Rate of Customer Services</option>
															<option value="0.24">1 Star</option>
															<option value="0.48">2 Star</option>
															<option value="0.72">3 Star</option>
															<option value="0.96">4 Star</option>
															<option value="1.2">5 Star</option>
														</select>
													</div>
													<div class="form-group">
														<label>Food &amp; Beverages</label>
														<select class="form-control select" name="food">
															<option value="" selected>Select Rate of Food &amp; Beverages</option>
															<option value="0.23">1 Star</option>
															<option value="0.46">2 Star</option>
															<option value="0.69">3 Star</option>
															<option value="0.92">4 Star</option>
															<option value="1.15">5 Star</option>
														</select>
													</div>
													<div class="form-group">
														<label>Comfort</label>
														<select class="form-control select" name="comfort">
															<option value="" selected>Select Rate of Comfort</option>
															<option value="0.14">1 Star</option>
															<option value="0.28">2 Star</option>
															<option value="0.42">3 Star</option>
															<option value="0.56">4 Star</option>
															<option value="0.7">5 Star</option>
														</select>
													</div>
													<div class="form-group">
														<label>Value for Money</label>
														<select class="form-control select" name="money">
															<option value="" selected>Select Rate of Value for Money</option>
															<option value="0.13">1 Star</option>
															<option value="0.26">2 Star</option>
															<option value="0.39">3 Star</option>
															<option value="0.52">4 Star</option>
															<option value="0.65">5 Star</option>
														</select>
													</div>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading3">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">Popular Filter</a>
												</h4>
											</div>
											<div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
												<div class="panel-body">
													<div class="radio radio-danger mb10">
														<input type="radio" name="alcohol" value="Yes" id="opt1">
														<label for="opt1">Alcohol</label>
													</div>
													<div class="radio radio-danger mb10">
														<input type="radio" name="alcohol" value="No" id="opt2">
														<label for="opt2">No Alcohol</label>
													</div>
													<div class="radio radio-danger mb10">
														<input type="radio" name="pork" value="Yes" id="opt3">
														<label for="opt3">Pork</label>
													</div>
													<div class="radio radio-danger mb10">
														<input type="radio" name="pork" value="No" id="opt4">
														<label for="opt4">No Pork</label>
													</div>
													<div class="radio radio-danger mb10">
														<input type="radio" name="pork" value="Halal" id="opt5">
														<label for="opt5">Halal</label>
													</div>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading4">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="true" aria-controls="collapse4">Other Filter</a>
												</h4>
											</div>
											<div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
												<div class="panel-body">
													<div class="form-group">
														<label>Price Index</label>
														<select class="form-control" name="price_index">
															<option value="" selected>Select Price Index</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from price_index order by nama_price_index asc");
															while($g=mysqli_fetch_array($sql)){
																echo"<option value='$g[id_price_index]'>$g[nama_price_index]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Suitable For</label>
														<select class="form-control" name="suitable_for">
															<option value="" selected>Select Suitable For</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from suitable_for order by nama_suitable_for asc");
															while($h=mysqli_fetch_array($sql)){
																echo"<option value='$h[id_suitable_for]'>$h[nama_suitable_for]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Serving Time</label>
														<select class="form-control" name="serving_time">
															<option value="" selected>Select Serving Time</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from serving_time order by nama_serving_time asc");
															while($k=mysqli_fetch_array($sql)){
																echo"<option value='$k[id_serving_time]'>$k[nama_serving_time]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Type of Service</label>
														<select class="form-control" name="type_of_service">
															<option value="" selected>Select Type of Service</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from type_of_service order by nama_type_of_service asc");
															while($l=mysqli_fetch_array($sql)){
																echo"<option value='$l[id_type_of_service]'>$l[nama_type_of_service]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Air Conditioning</label>
														<select class="form-control" name="air_conditioning">
															<option value="" selected>Air Conditioning</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from air_conditioning order by nama_air_conditioning asc");
															while($l1=mysqli_fetch_array($sql)){
																echo"<option value='$l1[id_air_conditioning]'>$l1[nama_air_conditioning]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Heating System</label>
														<select class="form-control" name="heating_system">
															<option value="" selected>Heating System</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from heating_system order by nama_heating_system asc");
															while($l2=mysqli_fetch_array($sql)){
																echo"<option value='$l2[id_heating_system]'>$l2[nama_heating_system]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Wi-Fi</label>
														<select class="form-control" name="wifi">
															<option value="" selected>Select Wi-Fi</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from wifi order by nama_wifi asc");
															while($n=mysqli_fetch_array($sql)){
																echo"<option value='$n[id_wifi]'>$n[nama_wifi]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Payments</label>
														<select class="form-control" name="term_of_payment">
															<option value="" selected>Select Payments</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from term_of_payment order by nama_term_of_payment asc");
															while($o=mysqli_fetch_array($sql)){
																echo"<option value='$o[id_term_of_payment]'>$o[nama_term_of_payment]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Premise's Security</label>
														<select class="form-control" name="premise_security">
															<option value="" selected>Select Premise Security</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from premise_security order by nama_premise_security asc");
															while($p=mysqli_fetch_array($sql)){
																echo"<option value='$p[id_premise_security]'>$p[nama_premise_security]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Parking Spaces</label>
														<select class="form-control" name="parking_spaces">
															<option value="" selected>Select Parking Spaces</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from parking_spaces order by nama_parking_spaces asc");
															while($q=mysqli_fetch_array($sql)){
																echo"<option value='$q[id_parking_spaces]'>$q[nama_parking_spaces]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Premise's Fire Safety </label>
														<select class="form-control" name="premise_fire_safety">
															<option value="" selected>Select Premise's Fire Safety</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from premise_fire_safety order by nama_premise_fire_safety asc");
															while($r=mysqli_fetch_array($sql)){
																echo"<option value='$r[id_premise_fire_safety]'>$r[nama_premise_fire_safety]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Premise's Hygiene</label>
														<select class="form-control" name="premise_hygiene">
															<option value="" selected>Select Premise Hygiene</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from premise_hygiene order by nama_premise_hygiene asc");
															while($r1=mysqli_fetch_array($sql)){
																echo"<option value='$r1[id_premise_hygiene]'>$r1[nama_premise_hygiene]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Premise's Maintenance</label>
														<select class="form-control" name="premise_maintenance">
															<option value="" selected>Select Premise Maintenance</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from premise_maintenance order by nama_premise_maintenance asc");
															while($s=mysqli_fetch_array($sql)){
																echo"<option value='$s[id_premise_maintenance]'>$s[nama_premise_maintenance]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Ambience</label>
														<select class="form-control" name="ambience">
															<option value="" selected>Select Ambience</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from ambience order by nama_ambience asc");
															while($t=mysqli_fetch_array($sql)){
																echo"<option value='$t[id_ambience]'>$t[nama_ambience]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Attire</label>
														<select class="form-control" name="attire">
															<option value="" selected>Select Attire</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from attire order by nama_attire asc");
															while($u=mysqli_fetch_array($sql)){
																echo"<option value='$u[id_attire]'>$u[nama_attire]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Clean Washroom</label>
														<select class="form-control" name="clean_washroom">
															<option value="" selected>Select Clean Washroom</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from clean_washroom order by nama_clean_washroom asc");
															while($v=mysqli_fetch_array($sql)){
																echo"<option value='$v[id_clean_washroom]'>$v[nama_clean_washroom]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Tables Availability</label>
														<select class="form-control" name="tables_availability">
															<option value="" selected>Select Tables Availability</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from tables_availability order by nama_tables_availability asc");
															while($w=mysqli_fetch_array($sql)){
																echo"<option value='$w[id_tables_availability]'>$w[nama_tables_availability]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Noise Level</label>
														<select class="form-control" name="noise_level">
															<option value="" selected>Select Noise Level</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from noise_level order by nama_noise_level asc");
															while($x=mysqli_fetch_array($sql)){
																echo"<option value='$x[id_noise_level]'>$x[nama_noise_level]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Waiter Tipping</label>
														<select class="form-control" name="waiter_tipping">
															<option value="" selected>Select Waiter Tipping</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from waiter_tipping order by nama_waiter_tipping asc");
															while($y=mysqli_fetch_array($sql)){
																echo"<option value='$y[id_waiter_tipping]'>$y[nama_waiter_tipping]</option>";
															}
															?>
														</select>
													</div>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading6">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="true" aria-controls="collapse6">Features and Facility</a>
												</h4>
											</div>
											<div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
												<div class="panel-body">
													<?php
														$fasilitas=tulis_cekbox($_GET['facility'],$koneksi,'facility');
														echo"$fasilitas";
													?>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading7">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="true" aria-controls="collapse7">Serving</a>
												</h4>
											</div>
											<div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
												<div class="panel-body">
													<?php
														$serving=tulis_cekbox($_GET['serving'],$koneksi,'serving');
														echo"$serving";
													?>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading8">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapse8" aria-expanded="true" aria-controls="collapse8">Type of Serving</a>
												</h4>
											</div>
											<div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8">
												<div class="panel-body">
													<?php
														$type_of_serving=tulis_cekbox($_GET['type_of_serving'],$koneksi,'type_of_serving');
														echo"$type_of_serving";
													?>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading8tag">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapse8tag" aria-expanded="true" aria-controls="collapse8tag">Tag</a>
												</h4>
											</div>
											<div id="collapse8tag" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8tag">
												<div class="panel-body">
													<div class="form-group">
														<input class="form-control" name="tag">
													</div>
												</div>
											</div>
										</div>
										<button type="submit" class="btn btn-success btn-block">Search</button>
										<a href="https://www.foodieguidances.com/pages/food/search/" class="btn btn-warning btn-block">Reset</a>
									</form>
								</div>
							</div>
						</div>
					</div>

					<div class="panel-groupmb10 mb10" id="accordionsssa" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default head-pan">
							<div class="panel-heading jud-pan" role="tab" id="food1" data-toggle="collapse" data-parent="#accordionsssa" href="#collapsesssa1" aria-expanded="true" aria-controls="collapsesssa1">
								<h4 class="panel-title">
									❯ Food
								</h4>
							</div>
							<div id="collapsesssa1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="food1">
								<div class="panel-body">
									<form method="get" action="<?php echo"$base_url/pages/food/search/"; ?>" class="border-form" id="ale-form" onsubmit="myFunction()">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a href="<?php echo"$base_url" ?>/pages/food/search/?sort=total+DESC&batas=25">World Ranking</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a href="<?php echo"$base_url" ?>/pages/food/search/?sort=r.dilihat+DESC&batas=25">Popular</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a href="<?php echo"$base_url" ?>/pages/food/search/?sort=direkomendasi+DESC&batas=25">Recomended</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="headingfood1">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordionfood1" href="#collapsefood1" aria-expanded="true" aria-controls="collapsefood1">Location Filter</a>
												</h4>
											</div>
											<div id="collapsefood1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfood1">
												<div class="panel-body">

													<div class="form-group">
														<label>Country</label>
														<select class="form-control" name="country" id="negara2">
															<option value="" selected>Select Country</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from negara order by nama_negara asc");
															while($b=mysqli_fetch_array($sql)){
																echo"<option value='$b[id_negara]'>$b[nama_negara]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>States/Province</label>
														<select class="form-control" name="state" id="propinsi2">
															<option value="" selected>Select States/ Province</option>
														</select>
													</div>
													<div class="form-group">
														<label>City</label>
														<select class="form-control" name="city" id="kota2">
															<option value="" selected>Select City</option>
														</select>
													</div>
												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="headingfood2cat">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordionfood" href="#collapsefood2cat" aria-expanded="true" aria-controls="collapsefood2cat">Category</a>
												</h4>
											</div>
											<div id="collapsefood2cat" class='panel-collapse collapse' role="tabpanel" aria-labelledby="headingfood2cat">
												<div class="panel-body">
													<div class="form-group">
														<select class="form-control" name="category">
															<option value="" selected>Select Category</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from food_category order by nama_food_category asc");
															while($b=mysqli_fetch_array($sql)){
																echo"<option value='$b[id_food_category]'>$b[nama_food_category]</option>";
															}
															?>
														</select>
													</div>
												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="headingfood2pri">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordionfood" href="#collapsefood2pri" aria-expanded="true" aria-controls="collapsefood2pri">Price Index</a>
												</h4>
											</div>
											<div id="collapsefood2pri" class='panel-collapse collapse' role="tabpanel" aria-labelledby="headingfood2pri">
												<div class="panel-body">
													<div class="form-group">
														<select class="form-control" name="price_index">
															<option value="" selected>Select Price Index</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from price_index order by nama_price_index asc");
															while($d=mysqli_fetch_array($sql)){
																echo"<option value='$d[id_price_index]'>$d[nama_price_index]</option>";
															}
															?>
														</select>
													</div>
												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="headingfood2cui">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordionfood" href="#collapsefood2cui" aria-expanded="true" aria-controls="collapsefood2cui">Cuisine</a>
												</h4>
											</div>
											<div id="collapsefood2cui" class='panel-collapse collapse' role="tabpanel" aria-labelledby="headingfood2cui">
												<div class="panel-body">
													<div class="form-group">
														<select class="form-control" name="cuisine">
															<option value="" selected>Select Cuisine</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from cuisine order by nama_cuisine asc");
															while($c=mysqli_fetch_array($sql)){
																echo"<option value='$c[id_cuisine]'>$c[nama_cuisine]</option>";
															}
															?>
														</select>
													</div>
												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="headingfood2msg">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordionfood" href="#collapsefood2msg" aria-expanded="true" aria-controls="collapsefood2msg">MSG Level</a>
												</h4>
											</div>
											<div id="collapsefood2msg" class='panel-collapse collapse' role="tabpanel" aria-labelledby="headingfood2msg">
												<div class="panel-body">
													<div class="form-group">
														<select class="form-control" name="msg_level">
															<option value="" selected>Select MSG Level</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from msg_level order by nama_msg_level asc");
															while($e=mysqli_fetch_array($sql)){
																echo"<option value='$e[id_msg_level]'>$e[nama_msg_level]</option>";
															}
															?>
														</select>
													</div>
												</div>
											</div>
										</div>


										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="headingfood2">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordionfood" href="#collapsefood2" aria-expanded="true" aria-controls="collapsefood2">Cooking Methode</a>
												</h4>
											</div>
											<div id="collapsefood2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfood2">
												<div class="panel-body">
													<?php
														$cuisine=tulis_cekbox($_GET['cooking_methode'],$koneksi,'cooking_methode');
														echo"$cuisine";
													?>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="headingfood3">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordionfood" href="#collapsefood3" aria-expanded="true" aria-controls="collapsefood3">Food Rating</a>
												</h4>
											</div>
											<div id="collapsefood3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfood3">
												<div class="panel-body">
													<div class="form-group">
														<label>Overall</label>
														<select class="form-control" name="overall">
															<option value="" selected>Select Overate Rate</option>
															<option value="1">1 Star</option>
															<option value="2">2 Star</option>
															<option value="3">3 Star</option>
															<option value="4">4 Star</option>
															<option value="5">5 Star</option>
														</select>
													</div>
													<div class="form-group">
														<label>Cleanliness</label>
														<select class="form-control select" name="clean">
															<option value="" selected>Select Rate of Cleanliness</option>
															<option value="0.21">1 Star</option>
															<option value="0.42">2 Star</option>
															<option value="0.63">3 Star</option>
															<option value="0.84">4 Star</option>
															<option value="1.05">5 Star</option>
														</select>
													</div>
													<div class="form-group">
														<label>Flavor</label>
														<select class="form-control select" name="flavor">
															<option value="" selected>Select Rate of Flavor</option>
															<option value="0.20">1 Star</option>
															<option value="0.40">2 Star</option>
															<option value="0.60">3 Star</option>
															<option value="0.80">4 Star</option>
															<option value="1.00">5 Star</option>
														</select>
													</div>
													<div class="form-group">
														<label>Freshness</label>
														<select class="form-control select" name="freshness">
															<option value="" selected>Select Rate of Freshness</option>
															<option value="0.18">1 Star</option>
															<option value="0.36">2 Star</option>
															<option value="0.54">3 Star</option>
															<option value="0.72">4 Star</option>
															<option value="0.90">5 Star</option>
														</select>
													</div>
													<div class="form-group">
														<label>Cooking</label>
														<select class="form-control select" name="cooking">
															<option value="" selected>Select Rate of Cooking</option>
															<option value="0.17">1 Star</option>
															<option value="0.34">2 Star</option>
															<option value="0.51">3 Star</option>
															<option value="0.68">4 Star</option>
															<option value="0.85">5 Star</option>
														</select>
													</div>
													<div class="form-group">
														<label>Presentasion &amp; Aroma</label>
														<select class="form-control select" name="aroma">
															<option value="" selected>Select Rate of Presentasion &amp; Aroma</option>
															<option value="0.15">1 Star</option>
															<option value="0.30">2 Star</option>
															<option value="0.45">3 Star</option>
															<option value="0.60">4 Star</option>
															<option value="0.75">5 Star</option>
														</select>
													</div>
													<div class="form-group">
														<label>Serving</label>
														<select class="form-control select" name="serving">
															<option value="" selected>Select Rate of Serving</option>
															<option value="0.09">1 Star</option>
															<option value="0.18">2 Star</option>
															<option value="0.27">3 Star</option>
															<option value="0.36">4 Star</option>
															<option value="0.45">5 Star</option>
														</select>
													</div>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="headingfood2tag">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordionfood" href="#collapsefood2tag" aria-expanded="true" aria-controls="collapsefood2tag">Tag</a>
												</h4>
											</div>
											<div id="collapsefood2tag" class='panel-collapse collapse' role="tabpanel" aria-labelledby="headingfood2tag">
												<div class="panel-body">
													<div class="form-group">
														<input class="form-control" name="tag" value="">
													</div>
												</div>
											</div>
										</div>
										<button type="submit" class="btn btn-success btn-block">Search</button>
										<a href="https://www.foodieguidances.com/pages/beverage/search/" class="btn btn-warning btn-block">Reset</a>
									</form>
								</div>
							</div>
						</div>
					</div>

					<div class="panel-groupmb10" id="accordionsssas" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default head-pan">
							<div class="panel-heading jud-pan" role="tab" id="beverage1" data-toggle="collapse" data-parent="#accordionsssas" href="#collapsesssas1" aria-expanded="true" aria-controls="collapsesssas1">
								<h4 class="panel-title">
									❯ Beverage
								</h4>
							</div>
							<div id="collapsesssas1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="beverage1">
								<div class="panel-body">
									<form method="get" action="<?php echo"$base_url/pages/beverage/search/"; ?>" class="border-form" id="ale-form" onsubmit="myFunction()">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a href="<?php echo"$base_url" ?>/pages/beverage/search/?sort=total+DESC&batas=25">Top Ranking</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a href="<?php echo"$base_url" ?>/pages/beverage/search/?sort=r.dilihat+DESC&batas=25">Popular</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a href="<?php echo"$base_url" ?>/pages/beverage/search/?sort=direkomendasi+DESC&batas=25">Recomended</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="headingbeverage1">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#collapse" href="#collapsebeverage1" aria-expanded="true" aria-controls="collapsebeverage1">Location Filter</a>
												</h4>
											</div>
											<div id="collapsebeverage1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingbeverage1">
												<div class="panel-body">
													<div class="form-group">
														<label>Tag</label>
														<input class="form-control" name="tag" value="<?php echo"$_GET[tag]";?>">
													</div>
													<div class="form-group">
														<label>Country</label>
														<select class="form-control" name="country" id="negara3">
															<option value="" selected>Select Country</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from negara order by nama_negara asc");
															while($b=mysqli_fetch_array($sql)){
																echo"<option value='$b[id_negara]'>$b[nama_negara]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>States/Province</label>
														<select class="form-control" name="state" id="propinsi3">
															<option value="" selected>Select States/ Province</option>
														</select>
													</div>
													<div class="form-group">
														<label>City</label>
														<select class="form-control" name="city" id="kota3">
															<option value="" selected>Select City</option>
														</select>
													</div>
												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="headingbeverage2cat">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordionfood" href="#collapsebeverage2cat" aria-expanded="true" aria-controls="collapsebeverage2cat">Category</a>
												</h4>
											</div>
											<div id="collapsebeverage2cat" class='panel-collapse collapse' role="tabpanel" aria-labelledby="headingbeverage2cat">
												<div class="panel-body">
													<div class="form-group">
														<select class="form-control" name="category">
															<option value="" selected>Select Category</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from beverage_category order by nama_beverage_category asc");
															while($b=mysqli_fetch_array($sql)){
																echo"<option value='$b[id_beverage_category]'>$b[nama_beverage_category]</option>";
															}
															?>
														</select>
													</div>
												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="headingbeverage2pri">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordionfood" href="#collapsebeverage2pri" aria-expanded="true" aria-controls="collapsebeverage2pri">Price Index</a>
												</h4>
											</div>
											<div id="collapsebeverage2pri" class='panel-collapse collapse' role="tabpanel" aria-labelledby="headingbeverage2pri">
												<div class="panel-body">
													<div class="form-group">
														<select class="form-control" name="price_index">
															<option value="" selected>Select Price Index</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from price_index order by nama_price_index asc");
															while($d=mysqli_fetch_array($sql)){
																echo"<option value='$d[id_price_index]'>$d[nama_price_index]</option>";
															}
															?>
														</select>
													</div>
												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="headingbeverage2">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapsebeverage2" aria-expanded="true" aria-controls="collapsebeverage5">Making Methode</a>
												</h4>
											</div>
											<div id="collapsebeverage2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingbeverage2">
												<div class="panel-body">
													<?php
														$making_methode=tulis_cekbox($_GET['making_methode'],$koneksi,'making_methode');
														echo"$making_methode";
													?>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="headingbeverage3">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapsebeverage3" aria-expanded="true" aria-controls="collapsebeverage3">Beverage Rating</a>
												</h4>
											</div>
											<div id="collapsebeverage3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingbeverage3">
												<div class="panel-body">
													<div class="form-group">
														<label>Overall</label>
														<select class="form-control" name="overall">
															<option value="">Select Overate Rate</option>
															<option value="1" <?php if($_GET['overall']=='1'){ echo "selected";}?>>1 Star</option>
															<option value="2" <?php if($_GET['overall']=='2'){ echo "selected";}?>>2 Star</option>
															<option value="3" <?php if($_GET['overall']=='3'){ echo "selected";}?>>3 Star</option>
															<option value="4" <?php if($_GET['overall']=='4'){ echo "selected";}?>>4 Star</option>
															<option value="5" <?php if($_GET['overall']=='5'){ echo "selected";}?>>5 Star</option>
														</select>
													</div>
													<div class="form-group">
														<label>Cleanliness</label>
														<select class="form-control select" name="clean">
															<option value="">Select Rate of Cleanliness</option>
															<option value="0.21" <?php if($_GET['clean']=='0.21'){ echo "selected";} ?>>1 Star</option>
															<option value="0.42" <?php if($_GET['clean']=='0.42'){ echo "selected";} ?>>2 Star</option>
															<option value="0.63" <?php if($_GET['clean']=='0.63'){ echo "selected";} ?>>3 Star</option>
															<option value="0.84" <?php if($_GET['clean']=='0.84'){ echo "selected";} ?>>4 Star</option>
															<option value="1.05" <?php if($_GET['clean']=='1.05'){ echo "selected";} ?>>5 Star</option>
														</select>
													</div>
													<div class="form-group">
														<label>Flavor</label>
														<select class="form-control select" name="flavor">
															<option value="">Select Rate of Flavor</option>
															<option value="0.20" <?php if($_GET['flavor']=='0.20'){ echo "selected";} ?>>1 Star</option>
															<option value="0.40" <?php if($_GET['flavor']=='0.40'){ echo "selected";} ?>>2 Star</option>
															<option value="0.60" <?php if($_GET['flavor']=='0.60'){ echo "selected";} ?>>3 Star</option>
															<option value="0.80" <?php if($_GET['flavor']=='0.80'){ echo "selected";} ?>>4 Star</option>
															<option value="1.00" <?php if($_GET['flavor']=='1.00'){ echo "selected";} ?>>5 Star</option>
														</select>
													</div>
													<div class="form-group">
														<label>Freshness</label>
														<select class="form-control select" name="freshness">
															<option value="">Select Rate of Freshness</option>
															<option value="0.18" <?php if($_GET['freshness']=='0.18'){ echo "selected";} ?>>1 Star</option>
															<option value="0.36" <?php if($_GET['freshness']=='0.36'){ echo "selected";} ?>>2 Star</option>
															<option value="0.54" <?php if($_GET['freshness']=='0.54'){ echo "selected";} ?>>3 Star</option>
															<option value="0.72" <?php if($_GET['freshness']=='0.72'){ echo "selected";} ?>>4 Star</option>
															<option value="0.90" <?php if($_GET['freshness']=='0.90'){ echo "selected";} ?>>5 Star</option>
														</select>
													</div>
													<div class="form-group">
														<label>Cooking</label>
														<select class="form-control select" name="cooking">
															<option value="" >Select Rate of Cooking</option>
															<option value="0.17" <?php if($_GET['cooking']=='0.17'){ echo "selected";} ?>>1 Star</option>
															<option value="0.34" <?php if($_GET['cooking']=='0.34'){ echo "selected";} ?>>2 Star</option>
															<option value="0.51" <?php if($_GET['cooking']=='0.51'){ echo "selected";} ?>>3 Star</option>
															<option value="0.68" <?php if($_GET['cooking']=='0.68'){ echo "selected";} ?>>4 Star</option>
															<option value="0.85" <?php if($_GET['cooking']=='0.85'){ echo "selected";} ?>>5 Star</option>
														</select>
													</div>
													<div class="form-group">

														<label>Presentasion &amp; Aroma</label>
														<select class="form-control select" name="aroma">
															<option value="" >Select Rate of Presentasion &amp; Aroma</option>
															<option value="0.15" <?php if($_GET['aroma']=='0.15'){ echo "selected";} ?>>1 Star</option>
															<option value="0.30" <?php if($_GET['aroma']=='0.30'){ echo "selected";} ?>>2 Star</option>
															<option value="0.45" <?php if($_GET['aroma']=='0.45'){ echo "selected";} ?>>3 Star</option>
															<option value="0.60" <?php if($_GET['aroma']=='0.60'){ echo "selected";} ?>>4 Star</option>
															<option value="0.75" <?php if($_GET['aroma']=='0.75'){ echo "selected";} ?>>5 Star</option>
														</select>
													</div>
													<div class="form-group">
														<label>Serving</label>
														<select class="form-control select" name="serving">
															<option value="" >Select Rate of Serving</option>
															<option value="0.09" <?php if($_GET['serving']=='0.09'){ echo "selected";} ?>>1 Star</option>
															<option value="0.18" <?php if($_GET['serving']=='0.18'){ echo "selected";} ?>>2 Star</option>
															<option value="0.27" <?php if($_GET['serving']=='0.27'){ echo "selected";} ?>>3 Star</option>
															<option value="0.36" <?php if($_GET['serving']=='0.36'){ echo "selected";} ?>>4 Star</option>
															<option value="0.45" <?php if($_GET['serving']=='0.45'){ echo "selected";} ?>>5 Star</option>
														</select>
													</div>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="headingbeverage2tag">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordionfood" href="#collapsebeverage2tag" aria-expanded="true" aria-controls="collapsebeverage2tag">Tag</a>
												</h4>
											</div>
											<div id="collapsebeverage2tag" class='panel-collapse collapse' role="tabpanel" aria-labelledby="headingbeverage2tag">
												<div class="panel-body">
													<div class="form-group">
														<input class="form-control" name="tag">
													</div>
												</div>
											</div>
										</div>
										<button type="submit" class="btn btn-success btn-block">Search</button>
										<a href="https://www.foodieguidances.com/pages/food/search/" class="btn btn-warning btn-block">Reset</a>
									</form>
								</div>
							</div>
						</div>
					</div>
					
					<?php include 'config/inc/search_process.php' ?>										
					<?php include 'config/inc/search_events.php' ?>


					<div class="panel-groupmb10 mt10" id="accordionsssasu" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default head-pan">
							<div class="panel-heading jud-pan" role="tab" id="recipe1" data-toggle="collapse" data-parent="#accordionsssasu" href="#collapsesssasu1" aria-expanded="true" aria-controls="collapsesssasu1">
								<h4 class="panel-title">
									<a>❯ Recipe</a>
								</h4>
							</div>
							<div id="collapsesssasu1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="recipe1">
								<div class="panel-body">
									<form method="get" action="<?php echo"$base_url/pages/recipe/search/"; ?>" class="border-form" id="ale-form" onsubmit="myFunction()">
										<input type="hidden" name="sort" value="<?php echo"$_GET[sort]"; ?>">
										<input type="hidden" name="batas" value="<?php echo"$_GET[batas]"; ?>">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a <?php error_reporting(0); if($_GET['sort']=='r.dilihat DESC'){ echo "class='akt'";} ?> href="<?php echo"$base_url" ?>/pages/recipe/search/?sort=r.dilihat+DESC">Popular</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a <?php if($_GET['sort']=='direkomendasi DESC'){ echo "class='akt'";} ?> href="<?php echo"$base_url" ?>/pages/recipe/search/?sort=direkomendasi+DESC">Recomended</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="recipe1">
												<h4 class="panel-title">
													<a data-toggle="collapse" <?php if(!empty($_GET['category']) || !empty($_GET['cuisine']) || !empty($_GET['msg_level'])
														|| !empty($_GET['difficulty']) || !empty($_GET['cooking_methode']) || !empty($_GET['duration']) ){ echo "class='akt'";} ?> data-parent="#accordion" href="#collapsesssasug1" aria-expanded="true" aria-controls="collapsesssasu1">Advance Filter</a>
												</h4>
											</div>
											<div id="collapsesssasug1" <?php if(!empty($_GET['category']) || !empty($_GET['cuisine']) || !empty($_GET['msg_level'])
												|| !empty($_GET['difficulty']) || !empty($_GET['cooking_methode']) || !empty($_GET['duration']) ){ echo "class='panel-collapse collapse in'";} else {echo "class='panel-collapse collapse'";} ?> role="tabpanel" aria-labelledby="recipe1">
												<div class="panel-body">
													<div class="form-group">
														<label>Category</label>
														<select class="form-control" name="category">
															<option value="" selected>Select Category</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from recipe_category order by nama_recipe_category asc");
															while($b=mysqli_fetch_array($sql)){
																if($b['id_recipe_category']==$_GET['category']){
																	echo"<option value='$b[id_recipe_category]' selected>$b[nama_recipe_category]</option>";
																}
																else{
																	echo"<option value='$b[id_recipe_category]'>$b[nama_recipe_category]</option>";
																}
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Cuisine</label>
														<select class="form-control" name="cuisine">
															<option value="" selected>Select Cuisine</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from cuisine order by nama_cuisine asc");
															while($c=mysqli_fetch_array($sql)){
																if($c['id_cuisine']==$_GET['cuisine']){
																	echo"<option value='$c[id_cuisine]' selected>$c[nama_cuisine]</option>";
																}
																else{
																	echo"<option value='$c[id_cuisine]'>$c[nama_cuisine]</option>";
																}
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>MSG Level</label>
														<select class="form-control" name="msg_level">
															<option value="" selected>Select MSG Level</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from msg_level order by nama_msg_level asc");
															while($e=mysqli_fetch_array($sql)){
																if($e['id_msg_level']==$_GET['msg_level']){
																	echo"<option value='$e[id_msg_level]' selected>$e[nama_msg_level]</option>";
																}
																else{
																	echo"<option value='$e[id_msg_level]'>$e[nama_msg_level]</option>";
																}
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Difficulty</label>
														<select class="form-control" name="difficulty">
															<option value="" selected>Select Difficulty</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from difficulty order by nama_difficulty asc");
															while($d=mysqli_fetch_array($sql)){
																if($d['id_difficulty']==$_GET['difficulty']){
																	echo"<option value='$d[id_difficulty]' selected>$d[nama_difficulty]</option>";
																}
																else{
																	echo"<option value='$d[id_difficulty]'>$d[nama_difficulty]</option>";
																}
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Cooking Methode</label>
														<select class="form-control" name="cooking_methode">
															<option value="" selected>Select Cooking Methode</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from cooking_methode order by nama_cooking_methode asc");
															while($a=mysqli_fetch_array($sql)){
																if($a['id_cooking_methode']==$_GET['cooking_methode']){
																	echo"<option value='$a[id_cooking_methode]' selected>$a[nama_cooking_methode]</option>";
																}
																else{
																	echo"<option value='$a[id_cooking_methode]'>$a[nama_cooking_methode]</option>";
																}
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Duration</label>
														<select class="form-control" name="duration">
															<option value="" selected>Select Duration</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from duration order by nama_duration asc");
															while($f=mysqli_fetch_array($sql)){
																if($f['id_duration']==$_GET['duration']){
																	echo"<option value='$f[id_duration]' selected>$f[nama_duration]</option>";
																}
																else{
																	echo"<option value='$f[id_duration]'>$f[nama_duration]</option>";
																}
															}
															?>
														</select>
													</div>
												</div>
											</div>
										</div>
										<button type="submit" class="btn btn-success btn-block">Search</button>
										<a href="https://www.foodieguidances.com/pages/recipe/search/" class="btn btn-warning btn-block">Reset</a>
									</form>
								</div>
							</div>
						</div>
					</div>

					<div class="panel-groupmb10 mt10" id="accordionsssasulk" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default head-pan">
							<div class="panel-heading jud-pan" role="tab" id="fgmart1" data-toggle="collapse" data-parent="#accordionsssasulk" href="#collapsesssasulk1" aria-expanded="true" aria-controls="collapsesssasulk1">
								<h4 class="panel-title">
									<a>❯ FGMART</a>
								</h4>
							</div>
							<div id="collapsesssasulk1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="fgmart1">
								<form method="get" action="<?php echo"$base_url/pages/fgmart/search/"; ?>" class="border-form" id="ale-form" onsubmit="myFunction()">
									<div class="panel-body">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a href="<?php echo"$base_url" ?>/pages/fgmart/search/?sort=r.dilihat+DESC&batas=25">Popular</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a href="<?php echo"$base_url" ?>/pages/fgmart/search/?sort=dilike+DESC&batas=25">Liked</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="fgmart1">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordionsssasulks" href="#collapsesssasulks1" aria-expanded="true" aria-controls="collapse1">Advanced Filter</a>
												</h4>
											</div>
											<div id="collapsesssasulks1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="fgmart1">
												<div class="panel-body">
													<div class="form-group">
														<label>Category</label>
														<select class="form-control" name="category">
															<option value="" selected>Select Category</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from photo_category order by nama_photo_category asc");
															while($b=mysqli_fetch_array($sql)){
																echo"<option value='$b[id_photo_category]'>$b[nama_photo_category]</option>";
															}
															?>
														</select>
													</div>
												</div>
											</div>
										</div>
										<button type="submit" class="btn btn-success btn-block">Search</button>
										<a href="https://www.foodieguidances.com/pages/recipe/search/" class="btn btn-warning btn-block">Reset</a>
									</div>
								</form>
							</div>
						</div>
					</div>

					<div class="panel-groupmb10 mt10" id="accordionsssasul" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default head-pan">
							<div class="panel-heading jud-pan" role="tab" id="coupon1" data-toggle="collapse" data-parent="#accordionsssasul" href="#collapsesssasul1" aria-expanded="true" aria-controls="collapsesssasul1">
								<h4 class="panel-title">
									<a>❯ Coupon</a>
								</h4>
							</div>
							<div id="collapsesssasul1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="coupon1">
								<form method="get" action="<?php echo"$base_url/pages/coupon/search/"; ?>" class="border-form" id="ale-form" onsubmit="myFunction()">
									<div class="panel-body">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a href="<?php echo"$base_url" ?>/pages/coupon/search/?sort=r.dilihat+DESC&batas=25">Popular</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapsesssasula14" aria-expanded="true" aria-controls="collapse1">Coupon Title</a>
												</h4>
											</div>
											<div id="collapsesssasula14" class="panel-collapse collapse" role="tabpanel" aria-labelledby="coupon1">
												<div class="panel-body">
													<div class="form-group">
														<label>Coupon Title</label>
														<input class="form-control select" name="keyword" id="search">
													</div>
												</div>
											</div>
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a href="<?php echo"$base_url" ?>/pages/coupon/search/?sort=dilike+DESC&batas=25">Liked</a>
												</h4>
											</div>
											<div class="panel-heading" role="tab" id="heading1">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapsesssasula1" aria-expanded="true" aria-controls="collapse1">Location Filter</a>
												</h4>
											</div>
											<div id="collapsesssasula1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="coupon1">
												<div class="panel-body">
													<div class="form-group">
														<label>Country</label>
														<select class="form-control" name="country" id="negara4">
															<option value="" selected>Select Country</option>
															<?php
															$sql=mysqli_query($koneksi,"select * from negara order by nama_negara asc");
															while($b=mysqli_fetch_array($sql)){
																echo"<option value='$b[id_negara]'>$b[nama_negara]</option>";
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>States/Province</label>
														<select class="form-control" name="state" id="propinsi4">
															<option value="" selected>Select States/ Province</option>
														</select>
													</div>
													<div class="form-group">
														<label>City</label>
														<select class="form-control" name="city" id="kota4">
															<option value="" selected>Select City</option>
														</select>
													</div>

												</div>
											</div>
										</div>
										<button type="submit" class="btn btn-success btn-block">Search</button>
										<a href="https://www.foodieguidances.com/pages/recipe/search/" class="btn btn-warning btn-block">Reset</a>
									</div>
								</form>
							</div>
						</div>
					</div>

					<div class="panel-groupmb10 mt10" id="accordionsssasulkp" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default head-pan">
							<div class="panel-heading jud-pan" role="tab" id="artikel1" data-toggle="collapse" data-parent="#accordionsssasulkp" href="#collapsesssasulkp1" aria-expanded="true" aria-controls="collapsesssasulkp1">
								<h4 class="panel-title">
									<a>❯ Article</a>
								</h4>
							</div>
							<div id="collapsesssasulkp1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="artikel1">
								<form method="get" action="<?php echo"$base_url/pages/article/search/"; ?>" class="border-form" id="ale-form" onsubmit="myFunction()">
									<div class="panel-body">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="artikel1">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordionsssasulkpg1" href="#collapsesssasulkpa1" aria-expanded="true" aria-controls="collapse1">Advanced Filter</a>
												</h4>
											</div>
											<div id="collapsesssasulkpa1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="artikel1">
												<div class="panel-body">
													<div class="form-group">
														<label>Article Title</label>
														<input class="form-control" name="keyword" id="search">
													</div>
												</div>
											</div>
										</div>
										<button type="submit" class="btn btn-success btn-block">Search</button>
										<a href="https://www.foodieguidances.com/pages/recipe/search/" class="btn btn-warning btn-block">Reset</a>
									</div>
								</form>
							</div>
						</div>
					</div>

					<div class="panel-groupmb10 mt10" id="accordionsssasulkpw" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default head-pan">
							<div class="panel-heading jud-pan" role="tab" id="video1" data-toggle="collapse" data-parent="#accordionsssasulkpw" href="#collapsesssasulkpw1" aria-expanded="true" aria-controls="collapsesssasulkpw1">
								<h4 class="panel-title">
									<a>❯ Video</a>
								</h4>
							</div>
							<div id="collapsesssasulkpw1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="video1">
								<form method="get" action="<?php echo"$base_url/pages/video/search/"; ?>" class="border-form" id="ale-form" onsubmit="myFunction()">
									<div class="panel-body">
										<div class="panel panel-default">
											<div class="panel panel-default">
												<div class="panel-heading" role="tab" id="video1">
													<h4 class="panel-title">
														<a data-toggle="collapse" data-parent="#accordionsssasulkpw1" href="#collapsesssasulkpwj1" aria-expanded="true" aria-controls="collapse1">Advanced Filter</a>
													</h4>
												</div>
												<div id="collapsesssasulkpwj1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="video1">
													<div class="panel-body">
														<div class="form-group">
															<label>Video Title</label>
															<input class="form-control select" name="keyword" id="search">
														</div>
														<div class="form-group">
															<label>Category</label>
															<select class="form-control" name="category">
																<option value="" selected>Select Category</option>
																<?php
																$sql=mysqli_query($koneksi,"select * from video_category order by nama_video_category asc");
																while($d=mysqli_fetch_array($sql)){
																	echo"<option value='$d[id_video_category]'>$d[nama_video_category]</option>";
																}
																?>
															</select>
														</div>
													</div>
												</div>
											</div>
										</div>
										<button type="submit" class="btn btn-success btn-block">Search</button>
										<a href="https://www.foodieguidances.com/pages/recipe/search/" class="btn btn-warning btn-block">Reset</a>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-8 col-8a no-padding">
				<h4 class="f-merah no-mb">
					<?php error_reporting(0);
						echo"<div class='breadcrumba flat'>";
						if ($_GET['sort']=='r.dilihat DESC') {
							echo "<a href='#'>Popular</a>";
						}
						if ($_GET['sort']=='direkomendasi DESC') {
							echo "<a href='#'>Recomended</a>";
						}
						if(!empty($_GET['category']) || !empty($_GET['cuisine']) || !empty($_GET['msg_level'])
							|| !empty($_GET['difficulty']) || !empty($_GET['cooking_methode']) || !empty($_GET['duration']) ){ echo "<a href='#'>Advance Filter</a>";}
						echo"</div>";
					?>
				</h4>
				<div class="form-inline border-form sort-form ml10-j">
					<div class="row">
						<div class="col-6">
							<div class="form-group">
								<select class="form-control" name="sort">
									<option value="r.id_recipe DESC" <?php if(isset($_GET['sort'])){if($_GET['sort']=="r.id_recipe DESC"){echo"selected";}} ?>>Latest</option>
									<option value="total_vote DESC" <?php if(isset($_GET['sort'])){if($_GET['sort']=="total_vote DESC"){echo"selected";}} ?>>Top Ranking</option>
									<option value="r.nama_recipe ASC" <?php if(isset($_GET['sort'])){if($_GET['sort']=="r.nama_recipe ASC"){echo"selected";}} ?>>Name A-Z</option>
									<option value="r.nama_recipe DESC" <?php if(isset($_GET['sort'])){if($_GET['sort']=="r.nama_recipe DESC"){echo"selected";}} ?>>Name Z-A</option>
									<option value="r.dilihat DESC" <?php if(isset($_GET['sort'])){if($_GET['sort']=="r.dilihat DESC"){echo"selected";}} ?>>Popular</option>
									<option value="direkomendasi DESC" <?php if(isset($_GET['sort'])){if($_GET['sort']=="direkomendasi DESC"){echo"selected";}} ?>>Recomended</option>
									<option value="dilike DESC" <?php if(isset($_GET['sort'])){if($_GET['sort']=="dilike DESC"){echo"selected";}} ?>>Favored</option>
								</select>
							</div>
						</div>
						<div class="col-6">
							<div class="form-group">
								<select class="form-control" name="batas">
									<option value="25" <?php if(isset($_GET['batas'])){if($_GET['batas']==25){echo"selected";}} ?>>Show 25 per Page</option>
									<option value="50" <?php if(isset($_GET['batas'])){if($_GET['batas']==50){echo"selected";}} ?>>Show 50 per Page</option>
									<option value="100" <?php if(isset($_GET['batas'])){if($_GET['batas']==100){echo"selected";}} ?>>Show 100 per Page</option>
								</select>
							</div>
						</div>
						<div class="col-4"><button type="submit" class="btn btn-danger btn-block mt-s10">Submit</button></div>
					</div>
				</div>
				<div class="media jarak1">
					<?php
					$kondisi=" WHERE r.id_recipe <> 0";
					if(!empty($_GET['keyword'])){
						$kondisi .= " AND r.nama_recipe like '$_GET[keyword]%' OR c.nama_recipe_category like '$_GET[keyword]%' OR k.nama_cuisine like '$_GET[keyword]%'";
					}
					if(!empty($_GET['category'])){
						$kondisi .= " AND r.id_recipe_category = '$_GET[category]'";
					}
					if(!empty($_GET['difficulty'])){
						$kondisi .= " AND r.id_difficulty = '$_GET[difficulty]'";
					}
					if(!empty($_GET['cuisine'])){
						$kondisi .= " AND r.id_cuisine = '$_GET[cuisine]'";
					}
					if(!empty($_GET['cooking_methode'])){
						$kondisi .= " AND r.id_cooking_methode = '$_GET[cooking_methode]'";
					}
					if(!empty($_GET['msg_level'])){
						$kondisi .= " AND r.id_msg_level = '$_GET[msg_level]'";
					}
					if(!empty($_GET['duration'])){
						$kondisi .= " AND r.id_duration = '$_GET[duration]'";
					}
					$p      = new Paging;
					$batas  = 25;
					if(isset($_GET['batas'])){
						$batas  = $_GET['batas'];
					}
					$sort="r.id_recipe DESC";
					if(isset($_GET['sort'])){
						$sort  = $_GET['sort'];
					}
					$posisi = $p->cariPosisi($batas);
					$no = $posisi+1;
					$sql="SELECT *,(select count(h.id_recipe) from recipe_here h where h.id_recipe=r.id_recipe) as jumlah,(select p.gambar_recipe_photo from recipe_photo p where p.id_recipe=r.id_recipe order by p.id_recipe_photo desc limit 1) as gambar,(select count(h.id_recipe) from recipe_like h where h.id_recipe=r.id_recipe) as dilike,(select count(h.id_recipe) from recipe_rekomendasi h where h.id_recipe=r.id_recipe) as direkomendasi FROM recipe r
					LEFT JOIN recipe_category c ON r.id_recipe_category = c.id_recipe_category
					LEFT JOIN difficulty p ON r.id_difficulty = p.id_difficulty
					LEFT JOIN cooking_methode m ON r.id_cooking_methode = m.id_cooking_methode
					LEFT JOIN cuisine k ON r.id_cuisine=k.id_cuisine
					LEFT JOIN msg_level l ON r.id_msg_level=l.id_msg_level
					LEFT JOIN duration j ON r.id_duration=j.id_duration".$kondisi." ORDER BY ".$sort." LIMIT $posisi,$batas";

					$sql2="SELECT * FROM recipe r LEFT JOIN recipe_category c ON r.id_recipe_category = c.id_recipe_category LEFT JOIN difficulty p ON r.id_difficulty = p.id_difficulty LEFT JOIN cooking_methode m ON r.id_cooking_methode = m.id_cooking_methode LEFT JOIN cuisine k ON r.id_cuisine=k.id_cuisine LEFT JOIN msg_level l ON r.id_msg_level=l.id_msg_level LEFT JOIN duration j ON r.id_duration=j.id_duration".$kondisi;
					$latest=mysqli_query($koneksi,$sql);
					$jmldata = mysqli_num_rows(mysqli_query($koneksi,$sql2));
					if($jmldata<>0){
						while($v=mysqli_fetch_array($latest)){
							$slug=seo($v['nama_recipe']);
							$post=date("jS M, Y", strtotime($v['tgl_post']));
							$id = id_masking($v['id_recipe']);
							if($v['pork_serving']=="Halal"){
								$ikon="icon_halal.png";
							}
							elseif($v['pork_serving']=="Yes"){
								$ikon="icon_pork.png";
							}
							elseif($v['pork_serving']=="No"){
								$ikon="icon_nonhalal.png";
							}
							else{
								$ikon="icon_vegetarian.png";
							}
							echo"<div class='row col-4a'>
								<div class='col-1'>$no.</div>
								<div class='col-8 thumb col-8a no-padding'>
									<a href='$base_url/pages/recipe/info/$id/$slug'><img data-original='$base_url/assets/img/recipe/big_$v[gambar]' class='lazy' width='220' height='165'></a>
									<a href='$base_url/pages/recipe'><span>Recipe</span></a>
								</div>
								<div class='col-7 info col-8a'>
									<em>posted on $post</em>
									<h3 class='sembunyi'><a href='$base_url/pages/recipe/info/$id/$slug'>$v[nama_recipe]</a></h3>
									<ul class='list-unstyled f-12'>
										<li><em>category</em> <a href='$base_url/pages/recipe/search/?category=$v[id_recipe_category]'>$v[nama_recipe_category]</a></li>
										<li><em>cuisine</em> <a href='$base_url/pages/recipe/search/?cuisine=$v[id_cuisine]'>$v[nama_cuisine]</a></li>
										<li><em>dish</em> <a href='$base_url/pages/recipe/search/?dish=$v[dish]'>$v[dish]</a></li>
										<li><i class='fa fa-signal f-16'></i> $v[nama_difficulty] <span class='bullet'>&#8226;</span> <i class='fa fa-clock-o f-16'></i> $v[nama_duration]</li>
										<li>$v[dilike] Like <span class='bullet'>&#8226;</span> $v[jumlah] Trying</li>
									</ul>";
									// <img src='$base_url/assets/img/theme/$ikon' width='30' class='db mt5'>
								echo"</div>
							</div>";
							$no++;
						}
						echo"</div><nav class='ac'>";
						$link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
						$jmlhalaman  = $p->jumlahHalaman($jmldata, $batas);
						$linkHalaman = $p->navHalaman($link,$_GET['page'], $jmlhalaman);
						echo "<ul class='pagination'>$linkHalaman</ul>";
						echo"</nav>";
					}
					else{
						echo"<div class='row'><div class='col-16'><h4 class='f-merah'>Ups..! We cant found anything</h4><p>There is no data that matches the condition you're looking for.</p></div></div></div>";
					}
					?>
			</div>
			<div class="col-4 mene-atas"><br><br>
			  	<?php include"config/inc/search.php"; ?>
			  	<?php include"config/inc/iklan.php"; ?>
				<div class="fb-like-box" data-href="https://www.facebook.com/foodieguidances#_=_" data-width="220" data-height="350" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
			</div>
		</div>
		</form>
    </div>
	<?php include"config/inc/footer.php"; ?>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.superslides.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap-select.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.lazyload.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/theme.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/select2.min.js"></script>
	<link href="<?php echo"$base_url"; ?>/assets/css/select2.min.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
	<script>
		 $(document).ready(function() {
			 $('.drawer').drawer();

		 $('.tutup').click(function(){
			 $('.iklanan').toggleClass('hilang', 500);
		 });

		 $('.cart').click(function(e){
			 e.stopPropagation();
			 $('.dis-cart').toggle();
			 $('.dis-plus').hide();
			 $('.dis-share').hide();
		 });
		 $('.plus').click(function(e){
			 e.stopPropagation();
			 $('.dis-plus').toggle();
			 $('.dis-cart').hide();
			 $('.dis-share').hide();
		 });
		 $('.share').click(function(e){
			 e.stopPropagation();
			 $('.dis-share').toggle();
			 $('.dis-plus').hide();
			 $('.dis-cart').hide();
		 });
		 $('.drawer-toggle').click(function(){
			 $('.dis-cart').hide();
			 $('.dis-plus').hide();
			 $('.dis-share').hide();
		 });
		 $('.btn-cari').click(function(){
			 $('.search_kiri').toggleClass('search_kiri_tam', 500);
		 });
		 $(document).click(function () {
				var $el = $(".dis-share");
				if ($el.is(":visible")) {
					 $el.fadeOut(200);
				}
				var $ela = $(".dis-plus");
				if ($ela.is(":visible")) {
					 $ela.fadeOut(200);
				}
				var $elu = $(".dis-cart");
				if ($elu.is(":visible")) {
					 $elu.fadeOut(200);
				}
			});

		 });
	</script>
	<div id="fb-root"></div>
	<script type="text/javascript">
		$('input[type="reset"]').click(function() {
			$("select").select2().val();
			$("select").select2().val();
		});
	</script>
	<script>
	function myFunction(){
		var myForm = document.getElementById('ale-form');
		var allInputs = myForm.getElementsByTagName('select');
		var teks = myForm.getElementsByTagName('input');
		var input, i, grup, a;

		for(i = 0; input = allInputs[i]; i++) {
			if(input.getAttribute('name') && !input.value) {
				input.setAttribute('name', '');
			}
		}
		for(a = 0; grup = teks[a]; a++) {
			if(grup.getAttribute('name') && !grup.value) {
				grup.setAttribute('name', '');
			}
		}
	}
	$(document).ready(function () {
		$("select").select2();
		$("img.lazy").lazyload({
			effect : "fadeIn"
		});
		// $('select').selectpicker();
		$("select").select2();
		$("#negara").change(function(){
			var id = $("#negara").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_propinsi.php",
				data: "id=" + id,
				success: function(data){
					$("#propinsi").html(data);
					$("#propinsi").fadeIn(2000);
					$('#propinsi').selectpicker('refresh');
				}
			});
		});
		$("#propinsi").change(function(){
			var id = $("#propinsi").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_kota.php",
				data: "id=" + id,
				success: function(data){
					$("#kota").html(data);
					$("#kota").fadeIn(2000);
					$('#kota').selectpicker('refresh');
				}
			});
		});
		$("#kota").change(function(){
			var id = $("#kota").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_mall.php",
				data: "id=" + id,
				success: function(data){
					$("#mall").html(data);
					$("#mall").fadeIn(2000);
				}
			});
		});

		$("#negara2").change(function(){
			var id = $("#negara2").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_propinsi.php",
				data: "id=" + id,
				success: function(data){
					$("#propinsi2").html(data);
					$("#propinsi2").fadeIn(2000);
					$('#propinsi2').selectpicker('refresh');
				}
			});
		});
		$("#propinsi2").change(function(){
			var id = $("#propinsi2").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_kota.php",
				data: "id=" + id,
				success: function(data){
					$("#kota2").html(data);
					$("#kota2").fadeIn(2000);
					$('#kota2').selectpicker('refresh');
				}
			});
		});
		$("#kota2").change(function(){
			var id = $("#kota2").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_mall.php",
				data: "id=" + id,
				success: function(data){
					$("#mall").html(data);
					$("#mall").fadeIn(2000);
				}
			});
		});

		$("#negara3").change(function(){
			var id = $("#negara3").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_propinsi.php",
				data: "id=" + id,
				success: function(data){
					$("#propinsi3").html(data);
					$("#propinsi3").fadeIn(2000);
					$('#propinsi3').selectpicker('refresh');
				}
			});
		});
		$("#propinsi3").change(function(){
			var id = $("#propinsi3").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_kota.php",
				data: "id=" + id,
				success: function(data){
					$("#kota3").html(data);
					$("#kota3").fadeIn(2000);
					$('#kota3').selectpicker('refresh');
				}
			});
		});
		$("#kota3").change(function(){
			var id = $("#kota3").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_mall.php",
				data: "id=" + id,
				success: function(data){
					$("#mall").html(data);
					$("#mall").fadeIn(2000);
				}
			});
		});

		$("#negara4").change(function(){
			var id = $("#negara4").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_propinsi.php",
				data: "id=" + id,
				success: function(data){
					$("#propinsi4").html(data);
					$("#propinsi4").fadeIn(2000);
					$('#propinsi4').selectpicker('refresh');
				}
			});
		});
		$("#propinsi4").change(function(){
			var id = $("#propinsi4").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_kota.php",
				data: "id=" + id,
				success: function(data){
					$("#kota4").html(data);
					$("#kota4").fadeIn(2000);
					$('#kota4').selectpicker('refresh');
				}
			});
		});
		$("#kota4").change(function(){
			var id = $("#kota4").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_resto.php",
				data: "id=" + id,
				success: function(data){
					$("#mall").html(data);
					$("#mall").fadeIn(2000);
				}
			});
		});
	});
	/*=====For developer to config ====*/
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=348272391978609&version=v2.0";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script>
</body>
</html>
