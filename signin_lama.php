<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
$active = "signin";
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
    <title>Sign In - Foodie Guidances</title>
	<meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>

	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>

	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/select2.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/awesome-bootstrap-checkbox.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>


   <link rel="stylesheet" href="<?php echo"$base_url"; ?>/assets/css/example.wink.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
 <body>
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>
    <div class="container hook">
		<div class="row">
			<div class="col-16">
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li class="active">Sign In</li>
				</ol>
				<h3 class="f-merah mb10">Sign In</h3>
				<?php
					if(isset($_GET['info'])){
						if($_GET['info']=="auto"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Logout!</strong> You have being logout for a period of inactivity. Please login again to continue access your account.
							</div>";
						}
						elseif($_GET['info']=="forgot"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Logout!</strong> Congratulations, you have successfully reset your password. Please sign in via <a href='$base_url'>foodieguidances.com</a> sign in panel below.
							</div>";
						}
						else{
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Logout!</strong> You have successfully logout.
							</div>";
						}
					}
				?>
				<p>*required field</p>
				<?php
					$sid_baru = session_id();
					$s=mysqli_query($koneksi, "SELECT * from member where id_session='$sid_baru'");
					$ts=mysqli_fetch_array($s);
					?>
				<form class="border-form form-horizontal" method="get" action="<?php echo"$base_url"; ?>/config/func/process_signin.php" id="signup-daftar">
					<div class="row mb20">
						<div class="col-4"><label class="control-label">Email <span class="f-merah">*</span></label></div>
						<div class="col-6">
							<input type="email" class="form-control" placeholder="Write Email" name="m_surat"  autocomplete="off" id="email">
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4"><label class="control-label">Password <span class="f-merah">*</span></label></div>
						<div class="col-6">
							<div class="input-group" style="width: 370px;">
						      <input type="password"  class="form-control" placeholder="Write Password" name="m_kode">
						   </div>
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4"></div>
						<div class=" col-6 checkbox checkbox-inline checkbox-danger" style="margin-left: 10px;">
							<input name="remember" value="1" id="remember" type="checkbox">
							<label for="remember" style="margin-top: -3px; margin-right: 3px;"> Remember Me</label>
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4"></div>
						<div class="col-6 f-12">
							<a href="<?php echo"$base_url/forgot-password"; ?>" class="f-merah">Forgot your password?</a>
						</div>
					</div>
					<button type="submit" class="btn btn-danger">Sign In</button> <button type="reset" class="btn btn-danger">Reset</button>
				</form>
			</div>
		</div>
    </div>
    <?php include"config/inc/footer.php"; ?>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/hideShowPassword.min.js"></script>
	<script>
	$('#password-1').hidePassword(true);
	</script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.validate.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/theme.js"></script>
	<script type="text/javascript">
	$(document).ready(function () {
		$(".alert").fadeTo(5000, 500).fadeOut(500, function(){
			$(".alert").alert('close');
		});



		$('#signup-daftar').validate({
			rules: {
				m_surat: {
					remote: {
						url: "config/func/check-email-login.php",
						type: "post",
						data: {
						  m_surat: function() {
							return $( "#email" ).val();
						  }
						}
					}
				},
				m_kode: {
					remote: {
						url: "config/func/check-password-login.php",
						type: "post",
						data: {
						  m_surat: function() {
							return $( "#email" ).val();
						  },
						  m_kode: function() {
							return $( "#password" ).val();
						  },
						}
					}
				}
			},
			messages: {
				m_surat: {
					required: "Email cant be blank",
					email: "Email not valid",
					remote: ""
				},
				m_kode: {
					required: "Password cant be blank",
					remote: "Invalid email or password"
				}
			}
		});
	});
	</script>
</body>
</html>
