<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";
include "config/func/id_masking.php";
date_default_timezone_set("Asia/Jakarta");
$sekarang=date("Y-m-d");
$id = id_masking($_GET['id']);
$r=mysqli_fetch_array(mysqli_query($koneksi,"select id_process, nama_process from process where id_process='$id'"));
$seo=seo($r['nama_process']);
if(isset($_SESSION['food_member'])){
	if($_GET['fungsi']=="1"){
		$ada=mysqli_num_rows(mysqli_query($koneksi,"select id_process_like from process_like where id_process='$id' and id_member='$_SESSION[food_member]' and tgl_like='$sekarang'"));
		if($ada==0){
			mysqli_query($koneksi,"insert into process_like (id_member,id_process,tgl_like) values('$_SESSION[food_member]','$id','$sekarang')");
			$_SESSION['resto_notif']     = "suka";
		}
		else{
			$_SESSION['resto_notif']     = "suka_gagal";
		}
	}
	elseif($_GET['fungsi']=="2"){
		$ada=mysqli_num_rows(mysqli_query($koneksi,"select id_process_rekomendasi from process_rekomendasi where id_process='$id' and id_member='$_SESSION[food_member]' and tgl_rekomendasi='$sekarang'"));
		if($ada==0){
			mysqli_query($koneksi,"insert into process_rekomendasi (id_member,id_process,tgl_rekomendasi) values('$_SESSION[food_member]','$id','$sekarang')");
			$_SESSION['resto_notif']     = "rekomendasi";
		}
		else{
			$_SESSION['resto_notif']     = "rekomendasi_gagal";
		}
	}
	elseif($_GET['fungsi']=="3"){
		$ada_bookmark=mysqli_num_rows(mysqli_query($koneksi,"select id_bookmark from bookmark where id='$id' and jenis='process' and id_member='$_SESSION[food_member]'"));
		if($ada_bookmark==0){
			mysqli_query($koneksi,"insert into bookmark (id_member,jenis,id,tgl_bookmark) values('$_SESSION[food_member]','process','$id','$sekarang')");
			$_SESSION['resto_notif']     = "bookmark";
		}
		else{
			$_SESSION['resto_notif']     = "bookmark_gagal";
		}
	}
	elseif($_GET['fungsi']=="4"){
		$ada_here=mysqli_num_rows(mysqli_query($koneksi,"select id_process_here from process_here where id_process='$id' and id_member='$_SESSION[food_member]'"));
		if($ada_here==0){
			mysqli_query($koneksi,"insert into process_here (id_member,id_process,tgl_here) values('$_SESSION[food_member]','$id','$sekarang')");
			$_SESSION['resto_notif']     = "disini";
		}
		else{
			$_SESSION['resto_notif']     = "disini_gagal";
		}
	}
	elseif($_GET['fungsi']=="5"){
		$ada_report=mysqli_num_rows(mysqli_query($koneksi,"select id_process_report from process_report where id_process='$id' and id_member='$_SESSION[food_member]'"));
		if($ada_report==0){
			mysqli_query($koneksi,"insert into process_report (id_member,id_process,tgl_report) values('$_SESSION[food_member]','$id','$sekarang')");
			$_SESSION['resto_notif']     = "lapor";
		}
		else{
			$_SESSION['resto_notif']     = "lapor_gagal";
		}
	}
	elseif($_GET['fungsi']=="6"){
		$ada_report=mysqli_num_rows(mysqli_query($koneksi,"select id_process_rating from process_rating where id_process='$id' and id_member='$_SESSION[food_member]' and tgl_process_rating='$sekarang'"));
		if($ada_report==0){
			if(!empty($_GET['rating']) ){
				if($_GET['kat'] == 1){
					$r1=$_GET['rating'] * 0.3;
					$r2=$_GET['rating'] * 0.3;
					$r3=$_GET['rating'] * 0.3;
					$r4=$_GET['rating'] * 0.1;
					mysqli_query($koneksi,"INSERT INTO process_rating(`id_process`, `id_member`, `expired`, `taste`, `freshness`, `serving`, tgl_process_rating) 
							values('$id','$_SESSION[food_member]','$r1','$r2','$r3','$r4','$sekarang')");
					mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('process','$id')");					
					$_SESSION['resto_notif']     = "rating";
				}
				else{
					$r1=$_GET['rating'] * 0.35;
					$r2=$_GET['rating'] * 0.35;
					$r3="";
					$r4=$_GET['rating'] * 0.3;
					mysqli_query($koneksi,"INSERT INTO process_rating(`id_process`, `id_member`, `expired`, `taste`, `freshness`, `serving`, tgl_process_rating) 
							values('$id','$_SESSION[food_member]','$r1','$r2','$r3','$r4','$sekarang')");
					mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('process','$id')");

					$_SESSION['resto_notif']     = "rating";
				}

			}
			else{
				if($_GET['kat'] == 1){
					$r1=$_GET['exp'] * 0.3;
					$r2=$_GET['taste'] * 0.3;
					$r3=$_GET['freshness'] * 0.3;
					$r4=$_GET['serving'] * 0.1;
					echo "asasasa";
					mysqli_query($koneksi,"INSERT INTO process_rating(`id_process`, `id_member`, `expired`, `taste`, `freshness`, `serving`, tgl_process_rating) 
						values('$id','$_SESSION[food_member]','$r1','$r2','$r3','$r4','$sekarang')");
					mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('process','$id')");
					$_SESSION['resto_notif']     = "rating";
				}
				else{
					$r1=$_GET['exp'] * 0.35;
					$r2=$_GET['taste'] * 0.35;
					$r3="";
					$r4=$_GET['serving'] * 0.3;

					mysqli_query($koneksi,"INSERT INTO process_rating(`id_process`, `id_member`, `expired`, `taste`, `freshness`, `serving`, tgl_process_rating) 
						values('$id','$_SESSION[food_member]','$r1','$r2','$r3','$r4','$sekarang')");
					mysqli_query($koneksi,"INSERT INTO feed (jenis,id) VALUES('process','$id')");
					$_SESSION['resto_notif']     = "rating";
				}
				
			}
		}
		
		else{
			$_SESSION['resto_notif']     = "rating_gagal";
			
		}
	}
	if($_GET['fungsi']=="7"){
		$ada=mysqli_num_rows(mysqli_query($koneksi,"select id_process_photo_like from process_photo_like where id_process_photo_like='$_GET[idp]' and id_member='$_SESSION[food_member]' and tgl_photo_like='$sekarang'"));
		if($ada==0){
			mysqli_query($koneksi,"insert into process_photo_like (id_member,id_process_photo,tgl_photo_like) values('$_SESSION[food_member]','$_GET[idp]','$sekarang')");
			$_SESSION['resto_notif']     = "suka_photo";
		}
		else{
			$_SESSION['resto_notif']     = "suka_photo_gagal";
		}
		header("Location: ".$base_url."/pages/process/info/".$_GET['id']."/".$seo);
		
		exit;
	}
}
else{
	$_SESSION['resto_notif']     = "login_dulu";
}
header("Location: ".$base_url."/pages/process/".$_GET['page']."/".$_GET['id']."/".$seo);
?>
