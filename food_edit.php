<?php
session_start();
$auto_logout=1800000;
include "config/func/base_url.php";
if(!empty($_SESSION['food_member'])){
if (time()-$_SESSION['timestamp']>$auto_logout){
    session_destroy();
    session_unset();
	header("Location: ".$base_url."/auto-logout");
	exit();
}else{
    $_SESSION['timestamp']=time();
}
include "config/database/db.php";
include "config/func/member_data.php";
$active = "member";
// $edit=mysqli_query($koneksi,"select * from food where id_food = '$_GET[id]' and id_member='$id_member'");
$edit=mysqli_query($koneksi,"select * from food where id_food = '$_GET[id]'");
$e=mysqli_fetch_array($edit);
function tulis_cekbox($field,$koneksi,$judul) {
	$query ="select * from ".$judul." order by nama_".$judul;
	$r = mysqli_query($koneksi,$query);
	$_arrNilai = explode('+', $field);
	$str = '';
	$no=1;
	while ($w = mysqli_fetch_array($r)) {
		$_ck = (array_search($w[1], $_arrNilai) === false)? '' : 'checked';
		$str .= "<div class='col-5 de'>
			<div class='checkbox checkbox-inline checkbox-danger'>
				<input type='checkbox' name='".$judul."[]' value='$w[1]' id='$judul$no' $_ck>
				<label for='$judul$no'> $w[1]</label>
			</div>
		</div>";
		$no++;
	}
	return $str;
}
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
  <title>Edit Food - Foodie Guidances</title>
	<meta name="keywords" content="">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/select2.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/awesome-bootstrap-checkbox.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>
  <link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">
  <style media="screen">
    @media (max-width: 975px) and (min-width: 200px){
      .se{
        margin-left: 10px;
      }
      .de {
          width: 50%;
      }
    }
  </style>
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
 <body style="position: initial;" class="drawer drawer--right">
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>
    <div class="container hook">
		<div class="row">
			<div class="col-16">
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Account</a></li>
					<li class="active">Edit Food</li>
				</ol>
				<?php
				if(isset($_SESSION['notif'])){
					if($_SESSION['notif']=="sukses"){
						echo"<div class='alert alert-success alert-dismissible' role='alert'>
							<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
							<strong>Success!</strong> Food review has been edited.
						</div>";
					}
					unset($_SESSION['notif']);
				}
				?>
				<h3 class="f-merah mb10">Edit Food Review</h3>
				<p>*required field</p>
				<form class="border-form form-horizontal" method="post" action="<?php echo"$base_url"; ?>/config/func/edit_food.php" enctype="multipart/form-data">
					<input type="hidden" name="id" <?php echo"value='$e[id_food]'"; ?>>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Food Name <span class="f-merah">*</span></label></div>
						<div class="col-6 col-4b">
							<input type="text" class="form-control" placeholder="Write Name" name="food_name" autocomplete="off" required value="<?php echo"$e[nama_food]"; ?>" maxlength="50">
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Category <span class="f-merah">*</span></label></div>
						<div class="col-6 col-4b">
							<select name="category" required class="form-control">
								<?php
								$sql=mysqli_query($koneksi,"select * from food_category order by nama_food_category asc");
								while($a=mysqli_fetch_array($sql)){
									$selected="";
									if($a['id_food_category']==$e['id_food_category']){
										$selected="selected";
									}
									echo"<option value='$a[id_food_category]' $selected>$a[nama_food_category]</option>";
								}
								?>
							</select>
						</div>
					</div>
					<!-- <div class="row mb20">
						<div class="col-4"><label class="control-label">Cuisine <span class="f-merah">*</span></label></div>
						<div class="col-6">
							<select name="cuisine" required class="form-control">
								<?php
								$sql=mysqli_query($koneksi,"select * from cuisine order by nama_cuisine asc");
								while($f=mysqli_fetch_array($sql)){
									$selected="";
									if($f['id_cuisine']==$e['id_cuisine']){
										$selected="selected";
									}
									echo"<option value='$f[id_cuisine]' $selected>$f[nama_cuisine]</option>";
								}
								?>
							</select>
						</div>
					</div> -->
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Price Index</label></div>
						<div class="col-6 col-4b">
							<select name="price_index" required class="form-control">
								<option>Select Price Index</option>
								<?php
								$sql=mysqli_query($koneksi,"select * from price_index order by nama_price_index asc");
								while($g=mysqli_fetch_array($sql)){
									$selected="";
									if($g['id_price_index']==$e['id_price_index']){
										$selected="selected";
									}
									echo"<option value='$g[id_price_index]' $selected>$g[nama_price_index]</option>";
								}
								?>
							</select>
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">MSG Level</label></div>
						<div class="col-6 col-4b">
							<select name="msg_level" class="form-control">
								<option>Select MSG Level</option>
								<?php
								$sql=mysqli_query($koneksi,"select * from msg_level order by nama_msg_level asc");
								while($d=mysqli_fetch_array($sql)){
									$selected="";
									if($d['id_msg_level']==$e['id_msg_level']){
										$selected="selected";
									}
									echo"<option value='$d[id_msg_level]' $selected>$d[nama_msg_level]</option>";
								}
								?>
							</select>
						</div>
					</div>
               <div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Tag </label></div>
						<div class="col-6 col-4b">
							<input type="text" class="form-control" placeholder="Write tag (separated by comma)" autocomplete="off" name="tag" <?php echo"value='$e[id_tag]'"; ?>>
						</div>
					</div>
         <div class="row mb20">
            <div class="col-4 col-4b"><label class="control-label">Pork Serving</label></div>
            <div class="col-8 col-8b">
               <div class="radio radio-inline radio-danger col-4b">
                  <input type="radio" <?php if ($e['type_serving']=='Yes') { echo "checked";} ?> name="pork" value="Yes" id="pork1">
                  <label for="pork1"> Yes </label>
               </div>
               <div class="radio radio-inline radio-danger col-4b">
                  <input type="radio" <?php if ($e['type_serving']=='No') { echo "checked";} ?> name="pork" value="No" id="pork2">
                  <label for="pork2"> No </label>
               </div>
               <div class="radio radio-inline radio-danger col-4b">
                  <input type="radio" <?php if ($e['type_serving']=='Halal') { echo "checked";} ?> name="pork" value="Halal" id="pork3">
                  <label for="pork3"> Halal </label>
               </div>
               <div class="radio radio-inline radio-danger col-4b">
                  <input type="radio" <?php if ($e['type_serving']=='Vege') { echo "checked";} ?> name="pork" value="vege" id="pork4">
                  <label for="pork4"> Vegetable </label>
               </div>
            </div>
         </div>
          <div class="box-info-2">
						<div class="box-head">
							Cuisine<i class="fa fa-angle-down klik"></i>
						</div>
						<div class="box-panel">
							<div class="row mb20">
								<?php
								$cooking=tulis_cekbox($e['id_cuisine'],$koneksi,'cuisine');
								echo"$cooking";
								?>
							</div>
						</div>
					</div>
               <div class="box-info-2">
						<div class="box-head">
							Cooking Methode<i class="fa fa-angle-down klik"></i>
						</div>
						<div class="box-panel">
							<div class="row mb20">
								<?php
								$cooking=tulis_cekbox($e['cooking_methode'],$koneksi,'cooking_methode');
								echo"$cooking";
								?>
							</div>
						</div>
					</div>
					<button type="submit" class="btn btn-danger">Submit</button>
				</form>
			</div>
		</div>
    </div>
    <?php include"config/inc/footer.php"; ?>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jasny-bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/select2.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/theme.js"></script>
  <script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
   <script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
   <script>
      $(document).ready(function() {
	      $('.drawer').drawer();

			$('.tutup').click(function(){
				$('.iklanan').toggleClass('hilang', 500);
			});

			$('.cart').click(function(e){
				e.stopPropagation();
				$('.dis-cart').toggle();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$('.plus').click(function(e){
				e.stopPropagation();
				$('.dis-plus').toggle();
				$('.dis-cart').hide();
				$('.dis-share').hide();
			});
			$('.share').click(function(e){
				e.stopPropagation();
				$('.dis-share').toggle();
				$('.dis-plus').hide();
				$('.dis-cart').hide();
			});
			$('.drawer-toggle').click(function(){
				$('.dis-cart').hide();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$(document).click(function () {
				 var $el = $(".dis-share");
				 if ($el.is(":visible")) {
					  $el.fadeOut(200);
				 }
				 var $ela = $(".dis-plus");
				 if ($ela.is(":visible")) {
					  $ela.fadeOut(200);
				 }
				 var $elu = $(".dis-cart");
				 if ($elu.is(":visible")) {
					  $elu.fadeOut(200);
				 }
		   });

      });
   </script>
	<script type="text/javascript">
	$(document).ready(function () {
		$("select").select2();
		$(".alert").fadeTo(5000, 500).fadeOut(500, function(){
			$(".alert").alert('close');
		});

      $(".box-head").click(function(){
         var targeta = $(this).children(".klik");
         $(targeta).toggleClass("fa-angle-up fa-angle-down");
			var target = $(this).parent().children(".box-panel");
			$(target).slideToggle();
		});

	});
	</script>
</body>
</html>
<?php
}
else{
	header("Location: ".$base_url."/login-area");
}
?>
