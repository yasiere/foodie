<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";
include "config/func/id_masking.php";
date_default_timezone_set("Asia/Jakarta");
$sekarang=date("Y-m-d");
$id = id_masking($_GET['id']);
$r=mysqli_fetch_array(mysqli_query($koneksi,"select id_events_beverage, nama_events_beverage from events_beverage where id_events_beverage='$id'"));
$seo=seo($r['nama_events_beverage']);

if(isset($_SESSION['food_member'])){

	if($_GET['fungsi']=="6"){
		$ada_report=mysqli_num_rows(mysqli_query($koneksi,"select id_rating from events_beverage_rating where id_beverage='$id' and id_member='$_SESSION[beverage_member]' and tgl_rating='$sekarang'"));
		if($ada_report==0){

			if(!empty($_GET['rating'])){
				$r1=$_POST['rating'] * 0.166;
				$r2=$_POST['rating'] * 0.166;
				$r3=$_POST['rating'] * 0.166;
				$r4=$_POST['rating'] * 0.166;
				$r5=$_POST['rating'] * 0.168;
				$r6=$_POST['rating'] * 0.168;
			}
			else{
				$r1=$_GET['cleanliness'] * 0.18;
				$r2=$_GET['flavor']      * 0.18;
				$r3=$_GET['freshness']   * 0.18;
				$r4=$_GET['cooking']     * 0.18;
				$r5=$_GET['pna']         * 0.14;
				$r6=$_GET['serving']     * 0.14;
			}
			mysqli_query($koneksi,"INSERT INTO `events_beverage_rating`(`id_beverage`, `id_member`, `cleanliness`, `flavor`, `freshness`, `cooking`, `pna`, `serving`, `tgl_rating`) 
									VALUES ('$id','$_SESSION[beverage_member]','$r1','$r2','$r3','$r4','$r5','$r6','$sekarang')");

			$_SESSION['resto_notif'] = "rating";

		}
		else{
			$_SESSION['resto_notif']     = "rating_gagal";
		}
	}
	
}
else{
	$_SESSION['resto_notif']     = "login_dulu";
}
 header("Location: ".$base_url."/pages/events/beverage/".$_GET['id']."/".$seo);
?>
