<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";
include "config/func/id_masking.php";
date_default_timezone_set("Asia/Jakarta");
$sekarang=date("Y-m-d");
$id = id_masking($_GET['id']);
$r=mysqli_fetch_array(mysqli_query($koneksi,"select id_events_food, nama_events_food from events_food where id_events_food='$id'"));
$seo=seo($r['nama_events_food']);
if(isset($_SESSION['food_member'])){

	if($_GET['fungsi']=="6"){
		$ada_report=mysqli_num_rows(mysqli_query($koneksi,"select id_rating from events_food_rating where id_food='$id' and id_member='$_SESSION[food_member]' and tgl_rating='$sekarang'"));
		if($ada_report==0){

			if(!empty($_GET['rating'])){
				$r1=$_GET['rating'] * 0.166;
				$r2=$_GET['rating'] * 0.166;
				$r3=$_GET['rating'] * 0.166;
				$r4=$_GET['rating'] * 0.166;
				$r5=$_GET['rating'] * 0.168;
				$r6=$_GET['rating'] * 0.168;
			}
			else{
				$r1=$_GET['cleanliness'] * 0.18;
				$r2=$_GET['flavor']      * 0.18;
				$r3=$_GET['freshness']   * 0.18;
				$r4=$_GET['cooking']     * 0.18;
				$r5=$_GET['pna']         * 0.14;
				$r6=$_GET['serving']     * 0.14;
			}
			mysqli_query($koneksi,"INSERT INTO `events_food_rating`(`id_food`, `id_member`, `cleanliness`, `flavor`, `freshness`, `cooking`, `pna`, `serving`, `tgl_rating`) 
									VALUES ('$id','$_SESSION[food_member]','$r1','$r2','$r3','$r4','$r5','$r6','$sekarang')");

			$_SESSION['resto_notif'] = "rating";

		}
		else{
			$_SESSION['resto_notif']     = "rating_gagal";
		}
	}
	
}
else{
	$_SESSION['resto_notif']     = "login_dulu";
}
header("Location: ".$base_url."/pages/events/food/".$_GET['id']."/".$seo);
?>
