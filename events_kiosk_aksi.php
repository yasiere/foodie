<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";
include "config/func/id_masking.php";
date_default_timezone_set("Asia/Jakarta");
$sekarang=date("Y-m-d");
$id = id_masking($_GET['id']);
$r=mysqli_fetch_array(mysqli_query($koneksi,"select id_events_kiosk, nama_events_kiosk from events_kiosk where id_events_kiosk='$id'"));
$seo=seo($r['nama_events_kiosk']);
if(isset($_SESSION['food_member'])){

	if($_GET['fungsi']=="6"){
		$ada_report=mysqli_num_rows(mysqli_query($koneksi,"select id_rating from events_kiosk_rating where id_kiosk='$id' and id_member='$_SESSION[food_member]' and tgl_rating='$sekarang'"));
		if($ada_report==0){

			if(!empty($_GET['rating'])){
				$r1=$_GET['rating'] * 0.25;
				$r2=$_GET['rating'] * 0.25;
				$r3=$_GET['rating'] * 0.25;
				$r4=$_GET['rating'] * 0.25;
			}
			else{
				$r1=$_GET['cleanliness'] * 0.2;
				$r2=$_GET['cusser'] * 0.2;
				$r3=$_GET['fnb'] * 0.4;
				$r4=$_GET['vfm'] * 0.2;
			}
			mysqli_query($koneksi,"INSERT INTO `events_kiosk_rating`(`id_kiosk`, `id_member`, `cleanliness`, `cusser`, `fnb`, `vfm`, `tgl_rating`) 
						VALUES ('$id','$_SESSION[food_member]', '$r1', '$r2', '$r3', '$r4', '$sekarang')");

			$_SESSION['resto_notif'] = "rating";

		}
		else{
			$_SESSION['resto_notif']     = "rating_gagal";
		}
	}
	
}
else{
	$_SESSION['resto_notif']     = "login_dulu";
}
 header("Location: ".$base_url."/pages/events/kiosk/".$_GET['id']."/".$seo);
?>
