<?php error_reporting(0);
session_start();
$auto_logout=18000000000000000000;
include "config/func/base_url.php";
if(!empty($_SESSION['food_member'])){
if (time()-$_SESSION['timestamp']>$auto_logout){
    session_destroy();
    session_unset();
	header("Location: ".$base_url."/auto-logout");
	exit();
}else{
    $_SESSION['timestamp']=time();
}
include "config/database/db.php";
include "config/func/member_data.php";
$active = "member";
$type = "member";
// mysqli_query($koneksi,"update restaurant set dilihat=dilihat + 1 where id_restaurant='$id'");

$id=$_GET['id'];

$resto=mysqli_query($koneksi,"SELECT *,d.id_kota,r.id_negara as negara, r.id_propinsi as propinsi, r.id_kota as kota, (SELECT COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS jumlah_vote,(SELECT COUNT(f.id_member) FROM restaurant_like f WHERE f.id_restaurant=r.id_restaurant) AS jumlah_like,(SELECT COUNT(f.id_member) FROM restaurant_rekomendasi f WHERE f.id_restaurant=r.id_restaurant) AS jumlah_rekomendasi,(SELECT SUM(f.cleanlines)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_cleanlines,(SELECT SUM(f.customer_services)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_customer,(SELECT SUM(f.food_beverage)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_food,(SELECT SUM(f.comfort)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_comfort,(SELECT SUM(f.value_money)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_money,(SELECT (SUM(f.cleanlines) + SUM(f.customer_services) + SUM(f.food_beverage) + SUM(f.comfort) + SUM(f.value_money)) / COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant =r.id_restaurant) AS total_rate FROM restaurant r LEFT JOIN type_of_business a ON r.id_type_of_business = a.id_type_of_business LEFT JOIN negara b ON r.id_negara = b.id_negara LEFT JOIN propinsi c ON r.id_propinsi = c.id_propinsi LEFT JOIN kota d ON r.id_kota = d.id_kota LEFT JOIN landmark e ON r.id_landmark = e.id_landmark LEFT JOIN mall f ON r.id_mall = f.id_mall LEFT JOIN operation_hour g ON r.id_operation_hour = g.id_operation_hour LEFT JOIN price_index i ON r.id_price_index = i.id_price_index LEFT JOIN suitable_for j ON r.id_suitable_for = j.id_suitable_for LEFT JOIN serving_time m ON r.id_serving_time = m.id_serving_time LEFT JOIN type_of_service n ON r.id_type_of_service = n.id_type_of_service LEFT JOIN wifi p ON r.id_wifi = p.id_wifi LEFT JOIN term_of_payment q ON r.id_term_of_payment = q.id_term_of_payment LEFT JOIN premise_security s ON r.id_premise_security = s.id_premise_security LEFT JOIN premise_fire_safety t ON r.id_premise_fire_safety = t.id_premise_fire_safety LEFT JOIN premise_maintenance u ON r.id_premise_maintenance = u.id_premise_maintenance LEFT JOIN parking_spaces v ON r.id_parking_spaces = v.id_parking_spaces LEFT JOIN ambience w ON r.id_ambience = w.id_ambience LEFT JOIN attire x ON r.id_attire = x.id_attire LEFT JOIN clean_washroom y ON r.id_clean_washroom = y.id_clean_washroom LEFT JOIN tables_availability z ON r.id_tables_availability = z.id_tables_availability LEFT JOIN noise_level ON r.id_noise_level = noise_level.id_noise_level LEFT JOIN waiter_tipping ON r.id_waiter_tipping = waiter_tipping.id_waiter_tipping LEFT JOIN member ON r.id_member = member.id_member LEFT JOIN air_conditioning ON r.id_air_conditioning = air_conditioning.id_air_conditioning LEFT JOIN heating_system ON r.id_heating_system = heating_system.id_heating_system LEFT JOIN premise_hygiene ON r.id_premise_hygiene = premise_hygiene.id_premise_hygiene where r.id_restaurant='$id'");
$e=mysqli_fetch_array($resto);
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
  <title>Submit Food - Foodie Guidances</title>
	<meta name="keywords" content="">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/jquery-ui.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/select2.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/awesome-bootstrap-checkbox.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>
  <link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">
  <style media="screen">
    @media (min-width: 200px) and (max-width: 399px) {
      .tab-content .tab-panel {
        padding: 15px 0;
      }
      .de {
        width: 100%;
      }
      .des {
        width: 100%;
        margin-left: 0px;
      }
      .se{
        width: 50%
      }
    }
  </style>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
 <body style="position: initial;" class="drawer drawer--right">
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>
    <div class="loadi">
       <img src="<?php echo "$base/assets/img/theme/load.gif"?>">
    </div>
    <div class="container hook">
		<div class="row">
			<div class="col-16">
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Account</a></li>
					<li class="active">Submit Food</li>
				</ol>
				<?php
				if(isset($_SESSION['notif'])){
					if($_SESSION['notif']=="gambar"){
						echo"<div class='alert alert-danger alert-dismissible' role='alert'>
							<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
							<strong>Failed!</strong> Image used have to be in JPG, PNG, and GIF format and the maximum total images size is 50MB also minimal dimension must be 800x600 pixel.
						</div>";
					}
					unset($_SESSION['notif']);
				}
				?>
				<h3 class="f-merah mb10">Submit Food</h3>
				<p>*required field</p>
				<form class="border-form form-horizontal" method="post" action="<?php echo"$base_url"; ?>/config/func/save_food.php" enctype="multipart/form-data">
               <div class="row mb20">
                  <div class="col-4 col-4b"><label class="control-label">Country <span class="f-merah">*</span></label></div>
                  <div class="col-6 col-4b">
                     <select name="negara" class="form-control" id="negara">
                        <option value="">Select Country</option>
                        <?php

                        $sql=mysqli_query($koneksi,"select * from negara order by nama_negara asc");
                        while($b=mysqli_fetch_array($sql)){
                           $selected="";
                           if($b['id_negara']==$e['negara']){
                              $selected="selected";
                           }
                           echo"<option value='$b[id_negara]' $selected>$b[nama_negara]</option>";
                        }
                        ?>
                     </select>
                  </div>
               </div>
               <div class="row mb20">
                  <div class="col-4 col-4b"><label class="control-label">State/ Province <span class="f-merah">*</span></label></div>
                  <div class="col-6 col-4b">
                     <select name="propinsi" class="form-control" id="propinsi">
                        <option value="">Select State/ Province</option>
                        <?php
                           $sql=mysqli_query($koneksi,"select * from propinsi where id_negara = '$e[negara]' order by nama_propinsi asc");
                           while($b1=mysqli_fetch_array($sql)){
                              $selected="";
                              if($b1['id_propinsi']==$e['propinsi']){
                                 $selected="selected";
                              }
                              echo"<option value='$b1[id_propinsi]' $selected>$b1[nama_propinsi]</option>";
                           }
                        ?>
                     </select>
                  </div>
               </div>
               <div class="row mb20">
                  <div class="col-4 col-4b"><label class="control-label">City <span class="f-merah">*</span></label></div>
                  <div class="col-6 col-4b">
                     <select name="kota"  class="form-control" id="kota">
                        <option value="">Select City</option>
                        <?php
                        $sql=mysqli_query($koneksi,"select * from kota where id_propinsi = '$e[propinsi]' order by nama_kota asc");
                        while($b2=mysqli_fetch_array($sql)){
                           $selected="";
                           if($b2['id_kota']==$e['kota']){
                              $selected="selected";
                           }
                           echo"<option value='$b2[id_kota]' $selected>$b2[nama_kota]</option>";
                        }
                        ?>
                     </select>
                  </div>
               </div>


               <div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Restaurant <span class="f-merah">*</span></label></div>
						<div class="col-6 col-4b">
                     <select name="restaurant" class="form-control" id="search">
                        <option value="">Select Restaurant</option>
                        <?php
                        $sql=mysqli_query($koneksi,"select * from restaurant where id_kota = '$e[id_kota]' order by restaurant_name asc");
                        while($b2=mysqli_fetch_array($sql)){
                           $selected="";
                           if($b2['id_restaurant']==$e['id_restaurant']){
                              $selected="selected";
                           }
                           echo"<option value='$b2[id_restaurant]' $selected>$b2[restaurant_name]</option>";
                        }
                        ?>
                     </select>
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Food Name <span class="f-merah">*</span></label></div>
						<div class="col-6 col-4b">
							<input type="text" class="form-control" placeholder="Write Name" id="food" name="food_name" required maxlength="50">
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Category <span class="f-merah">*</span></label></div>
						<div class="col-6 col-4b">
							<select name="category" required class="form-control" id="kat">
								<option value="">Select Category</option>
								<?php
								$sql=mysqli_query($koneksi,"select * from food_category order by nama_food_category asc");
								while($a=mysqli_fetch_array($sql)){
									echo"<option value='$a[id_food_category]'>$a[nama_food_category]</option>";
								}
								?>
							</select>
						</div>
					</div>

					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Price Index</label></div>
						<div class="col-6 col-4b">
							<select name="price_index" required class="form-control">
								<option>Select Price Index</option>
								<?php
								$sql=mysqli_query($koneksi,"select * from price_index order by nama_price_index asc");
								while($g=mysqli_fetch_array($sql)){
									echo"<option value='$g[id_price_index]'>$g[nama_price_index]</option>";
								}
								?>
							</select>
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">MSG Level</label></div>
						<div class="col-6 col-4b">
							<select name="msg_level" class="form-control">
								<option>Select MSG Level</option>
								<?php
								$sql=mysqli_query($koneksi,"select * from msg_level order by nama_msg_level asc");
								while($d=mysqli_fetch_array($sql)){
									echo"<option value='$d[id_msg_level]'>$d[nama_msg_level]</option>";
								}
								?>
							</select>
						</div>
					</div>
               <div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Tag </label></div>
						<div class="col-6 col-4b">
							<input type="text" class="form-control" placeholder="Write tag (separated by comma)" name="tag">
						</div>
					</div>
               <div class="row mb20">
                  <div class="col-4 col-4b"><label class="control-label">Pork Serving</label></div>
                  <div class="col-8 col-4b">
                     <div class="radio radio-inline radio-danger des">
                        <input type="radio" checked="" name="pork" value="Yes" id="pork1">
                        <label for="pork1"> Yes </label>
                     </div>
                     <div class="radio radio-inline radio-danger de">
                        <input type="radio" name="pork" value="No" id="pork2">
                        <label for="pork2"> No </label>
                     </div>
                     <div class="radio radio-inline radio-danger de">
                        <input type="radio" name="pork" value="Halal" id="pork3">
                        <label for="pork3"> Halal </label>
                     </div>
                     <div class="radio radio-inline radio-danger de">
                        <input type="radio" name="pork" value="vege" id="pork4">
                        <label for="pork4"> Vegetable </label>
                     </div>
                  </div>
               </div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Cover Photo <span class="f-merah">*</span></label></div>
						<div class="col-6 col-4b">
							<div class="fileinput fileinput-new" data-provides="fileinput">
							  <div class="fileinput-preview upload" data-trigger="fileinput"></div>
							  <input type="file" name="gambar_food" id='gam' class="hidden" required>
							</div>
							<p class="help-block">Image used have to be in JPG, PNG, and GIF format and the maximum total images size is 50MB also minimal dimension must be 800x600 pixel.</p>
						</div>
					</div>
               <div class="box-info-2">
						<div class="box-head">
							Cuisine<i class="fa fa-angle-down klik"></i>
						</div>
						<div class="box-panel dn">
							<div class="row mb20">
								<?php
								$sql=mysqli_query($koneksi,"select * from cuisine order by nama_cuisine asc");
                $t=1;
								while($c=mysqli_fetch_array($sql)){
									echo"<div class='col-5 se'>
										<div class='checkbox checkbox-inline checkbox-danger'>
											<input type='checkbox' name='cuisine[]' value='$c[nama_cuisine]' id='cuisine$t'>
											<label for='cuisine$t'> $c[nama_cuisine]</label>
										</div>
									</div>";
									$t++;
								}
								?>
							</div>
						</div>
					</div>
					<div class="box-info-2">
						<div class="box-head">
							Cooking Methode<i class="fa fa-angle-up klik"></i>
						</div>
						<div class="box-panel">
							<div class="row mb20">
								<?php
								$sql=mysqli_query($koneksi,"select * from cooking_methode order by nama_cooking_methode asc");
								$t=1;
								while($s=mysqli_fetch_array($sql)){
									echo"<div class='col-5 se'>
										<div class='checkbox checkbox-inline checkbox-danger'>
											<input type='checkbox' name='cooking[]' value='$s[nama_cooking_methode]' id='cooking$t'>
											<label for='cooking$t'> $s[nama_cooking_methode]</label>
										</div>
									</div>";
									$t++;
								}
								?>
							</div>
						</div>
					</div>
					<div role="tabpanel" id="tab-vote">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#feature" aria-controls="feature" role="tab" data-toggle="tab">Feature Vote</a></li>
							<li role="presentation"><a href="#quick" aria-controls="quick" role="tab" data-toggle="tab">Quick Vote</a></li>
						</ul>
						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane tab-panel active fade in" id="feature">
								<table class="tablebintang">
									<tr>
										<td>Cleanliness <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall cleanliness of the food and plates served, which can include whether the food has unwanted insect, plastic, hair or unclean plates, bowls and utensils used."></i></td>
										<td><input type="hidden" class="rating" id="rat1" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="cleanlines"></td>
									</tr>
									<tr>
										<td>Flavour <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall taste of the food which has unique personality."></i></td>
										<td><input type="hidden" class="rating" id="rat2" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="flavor"></td>
									</tr>
									<tr>
										<td>Freshness <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment on the food ingredient's freshness which can include meat, seafood, vegetables and sauces."></i></td>
										<td><input type="hidden" class="rating" id="rat3" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="freshness"></td>
									</tr>
									<tr>
										<td>Cooking <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment of the way the food is being cooked wheither it is undercooked or overcooked. However, it also depend on how we like the food to be cook."></i></td>
										<td><input type="hidden" class="rating" id="rat4" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="cooking"></td>
									</tr>
									<tr>
										<td>Presentation &amp; Aroma <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment on the food's presentation and its aroma. A good presentation on food will increase our appetite for the food. likewise for the aroma too. A fragrance food cook meters away can attract our sense of smell. Good food makes better with great presentation and aroma."></i></td>
										<td><input type="hidden" class="rating" id="rat5" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="aroma"></td>
									</tr>
									<tr>
										<td>Serving <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Is the food being served having adequate serving? Too much or too little? Unlike Western cuisine which have courses of food for a complete meal, Eastern cuisine are more direct and usually treat as the main course. Therefore, the serving is vary."></i></td>
										<td><input type="hidden" class="rating" id="rat6" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="serving"></td>
									</tr>
								</table>
							</div>
							<div role="tabpanel" class="tab-pane tab-panel fade" id="quick">
								<table class="tablebintang">
									<tr>
										<td><strong>Give your vote</strong></td>
										<td><input type="hidden" class="rating" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="rating"></td>
									</tr>
								</table>
							</div>
						</div>
                  <ul class="list-inline">
                     <li><i class="fa fa-star"></i> Poor</li>
                     <li><i class="fa fa-star"></i><i class="fa fa-star"></i> Below Average</li>
                     <li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Average</li>
                     <li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Very Good</li>
                     <li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Excellent</li>
                  </ul>
					</div>
					<button type="submit" class="btn btn-danger">Submit</button> <button type="reset" class="btn btn-danger">Reset</button>
               <div class="btn btn-danger as btn-as">Submit</div> <div class="btn btn-danger as btn-as">Reset</div><br><br>
               <div class='alert-danger alert-dismissible as' role='alert' style="display:none; padding: 15px">
                  <strong>Please submit minimum one star for each rating categories in other to be rated</strong>
               </div>
				</form>
			</div>
		</div>
    </div>
    <?php include"config/inc/footer.php"; ?>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jasny-bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/select2.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jasny-bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap-rating.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery-ui.min.js"></script>
  <script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
	<script>
		 $(document).ready(function() {
			 $('.drawer').drawer();

		 $('.tutup').click(function(){
			 $('.iklanan').toggleClass('hilang', 500);
		 });

		 $('.cart').click(function(e){
			 e.stopPropagation();
			 $('.dis-cart').toggle();
			 $('.dis-plus').hide();
			 $('.dis-share').hide();
		 });
		 $('.plus').click(function(e){
			 e.stopPropagation();
			 $('.dis-plus').toggle();
			 $('.dis-cart').hide();
			 $('.dis-share').hide();
		 });
		 $('.share').click(function(e){
			 e.stopPropagation();
			 $('.dis-share').toggle();
			 $('.dis-plus').hide();
			 $('.dis-cart').hide();
		 });
		 $('.drawer-toggle').click(function(){
			 $('.dis-cart').hide();
			 $('.dis-plus').hide();
			 $('.dis-share').hide();
		 });
		 $('.btn-cari').click(function(){
			 $('.search_kiri').toggleClass('search_kiri_tam', 500);
		 });
		 $(document).click(function () {
				var $el = $(".dis-share");
				if ($el.is(":visible")) {
					 $el.fadeOut(200);
				}
				var $ela = $(".dis-plus");
				if ($ela.is(":visible")) {
					 $ela.fadeOut(200);
				}
				var $elu = $(".dis-cart");
				if ($elu.is(":visible")) {
					 $elu.fadeOut(200);
				}
			});

		 });
	</script>
   <script type="text/javascript">
      $(document).ready(function () {
         $(document).on("contextmenu",function(e){
            if(e.target.nodeName != "INPUT" && e.target.nodeName != "TEXTAREA")
            e.preventDefault();
         });
         $.fn.disableTextSelect = function() {
            return this.each(function() {
               $(this).css({
                  'MozUserSelect':'none',
                  'webkitUserSelect':'none'
               }).attr('unselectable','on').bind('selectstart', function() {
                  return false;
               });
            });
         };
         $('body').disableTextSelect();
      });
   </script>

	<script type="text/javascript">
	$(document).ready(function () {
		$('.tip').tooltip();
		$("select").select2();
		$('.rating').rating();

      $(".as").hide();
      var rat1 = $("#rat1").val();
      var rat2 = $("#rat2").val();
      var rat3 = $("#rat3").val();
      var rat4 = $("#rat4").val();
      var rat5 = $("#rat5").val();
      var rat6 = $("#rat6").val();
      $("#rat1,#rat2,#rat3,#rat4,#rat5,#rat6").change(function(){
         if ((rat1 < 1) && (rat2 < 1) && (rat3 < 1) && (rat4 < 1) && (rat5 < 1) && (rat6 < 1) ) {
            $(".btn-danger").hide();
            $(".as").show();
         }
      });

      $("#rat1,#rat2,#rat3,#rat4,#rat5,#rat6").change(function(){
         var rat1 = $("#rat1").val();
         var rat2 = $("#rat2").val();
         var rat3 = $("#rat3").val();
         var rat4 = $("#rat4").val();
         var rat5 = $("#rat5").val();
         var rat5 = $("#rat5").val();
         var rat6 = $("#rat6").val();
         if ((rat1 > 0) && (rat2 > 0) && (rat3 > 0) && (rat4 > 0) && (rat5 > 0) && (rat6 > 0)) {
            $(".btn-danger").show();
            $(".as").hide();
         }
      });

   	$("#search-box").keyup(function(){
         var id = $("#kota").val();
   		$.ajax({
   		type: "POST",
   		url: "<?php echo"$base_url"; ?>/config/func/resto.php",
   		data:'keyword='+$(this).val() + '&id='+id,
   		success: function(data){
   			$("#suggesstion-box").show();
   			$("#suggesstion-box").html(data);
   			$("#search-box").css("background","#FFF");
   		}
   		});
   	});

      $("#negara").change(function(){
			var id = $("#negara").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_propinsi.php",
				data: "id=" + id,
				success: function(data){
					$("#propinsi").html(data);
					$("#propinsi").fadeIn(2000);
				}
			});
		});
		$("#propinsi").change(function(){
			var id = $("#propinsi").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_kota.php",
				data: "id=" + id,
				success: function(data){
					$("#kota").html(data);
					$("#kota").fadeIn(2000);
				}
			});
		});

      $("#kota").change(function(){
			var id = $("#kota").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_resto.php",
				data: "id=" + id,
				success: function(data){
					$("#search").html(data);
					$("#search").fadeIn(2000);
				}
			});
		});

      $(".box-head").click(function(){
         var targeta = $(this).children(".klik");
         $(targeta).toggleClass("fa-angle-up fa-angle-down");
			var target = $(this).parent().children(".box-panel");
			$(target).slideToggle();
		});

	});
	</script>
   <script type="text/javascript">
      $("form").submit(function( event ) {
      	var kota = $("#kota").val();
      	var search_box = $("#search").val();
      	var food = $("#food").val();
      	var kat = $("#kat").val();
         var gam = $("#gam").val();
         if (!(kota === "") && !(search_box === "") && !(food === "") && !(kat === "") && !(gam === "")) {
            $(".loadi").show();
         }
         else {

         }
      });
	</script>
</body>
</html>
<?php
}
else{
	header("Location: ".$base_url."/login-area");
}
?>
