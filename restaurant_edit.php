<?php
session_start();
$auto_logout=1800000;
include "config/func/base_url.php";
if(!empty($_SESSION['food_member'])){
if (time()-$_SESSION['timestamp']>$auto_logout){
    session_destroy();
    session_unset();
	header("Location: ".$base_url."/auto-logout");
	exit();
}else{
    $_SESSION['timestamp']=time();
}
include "config/database/db.php";
include "config/func/member_data.php";
$active = "member";
// $edit=mysqli_query($koneksi,"select * from restaurant where id_restaurant = '$_GET[id]' and id_member='$id_member'");
$edit=mysqli_query($koneksi,"select * from restaurant where id_restaurant = '$_GET[id]'");
$e=mysqli_fetch_array($edit);
function tulis_cekbox($field,$koneksi,$judul) {
	$query ="select * from ".$judul." order by nama_".$judul;
	$r = mysqli_query($koneksi,$query);
	$_arrNilai = explode('+', $field);
	$str = '';
	$no=1;
	while ($w = mysqli_fetch_array($r)) {
		$_ck = (array_search($w[1], $_arrNilai) === false)? '' : 'checked';
		$str .= "<div class='col-5 col-4b ckbox'>
			<div class='checkbox checkbox-inline checkbox-danger'>
				<input type='checkbox' name='".$judul."[]' value='$w[1]' id='$judul$no' $_ck>
				<label for='$judul$no'> $w[1]</label>
			</div>
		</div>";
		$no++;
	}
	return $str;
}
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
    <title>Edit Restaurant - Foodie Guidances</title>
	<meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap-timepicker.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/select2.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/awesome-bootstrap-checkbox.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>
  <link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">
  <style>
		#map {
			width: 100%;
			height: 400px;
		}
		.controls {
			margin-top: 10px;
			border: 1px solid transparent;
			border-radius: 2px 0 0 2px;
			box-sizing: border-box;
			-moz-box-sizing: border-box;
			height: 32px;
			outline: none;
			box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
		}
		#searchInput {
			background-color: #fff;
			font-family: Roboto;
			font-size: 15px;
			font-weight: 300;
			margin-left: 12px;
			padding: 0 11px 0 13px;
			text-overflow: ellipsis;
			width: 50%;
		}
		#searchInput:focus {
		 	border-color: #4d90fe;
		}
	</style>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
 <body style="position: initial;" class="drawer drawer--right">
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>
    <div class="container hook">
		<div class="row">
			<div class="col-16">
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Account</a></li>
					<li class="active">Edit Restaurant</li>
				</ol>
				<?php
				if(isset($_SESSION['notif'])){
					if($_SESSION['notif']=="sukses"){
						echo"<div class='alert alert-success alert-dismissible' role='alert'>
							<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
							<strong>Success!</strong> Restaurant review has been edited.
						</div>";
					}
					unset($_SESSION['notif']);
				}
				?>
				<h3 class="f-merah mb10">Edit Restaurant Review</h3>
				<p class="help-block">*required field. Image extension must be JPG, PNG, GIF also maximum image size is 50 Mb</p>
				<form class="border-form form-horizontal" method="post" action="<?php echo"$base_url"; ?>/config/func/edit_restaurant.php" enctype="multipart/form-data">
					<input type="hidden" name="id" <?php echo"value='$e[id_restaurant]'"; ?>>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Restaurant Name <span class="f-merah">*</span></label></div>
						<div class="col-6 col-4b">
							<input type="text" class="form-control" placeholder="Write Name" name="restaurant_name" required <?php echo"value='$e[restaurant_name]'"; ?> maxlength="50">
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Business Type <span class="f-merah">*</span></label></div>
						<div class="col-6 col-4b">
							<select name="type_of_business" required class="form-control">
								<?php
								$sql=mysqli_query($koneksi,"select * from type_of_business order by nama_type_of_business asc");
								while($a=mysqli_fetch_array($sql)){
									$selected="";
									if($a['id_type_of_business']==$e['id_type_of_business']){
										$selected="selected";
									}
									echo"<option value='$a[id_type_of_business]' $selected>$a[nama_type_of_business]</option>";
								}
								?>
							</select>
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Tag <span class="f-merah">*</span></label></div>
						<div class="col-6 col-4b">
							<input type="text" class="form-control" placeholder="Write tag (separated by comma)" name="tag" required <?php echo"value='$e[tag]'"; ?>>
						</div>
					</div>
					<div class="box-info-2">
						<div class="box-head">
							Basic Information<i class="fa fa-angle-up klik"></i>
						</div>
						<div class="box-panel">
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Street Address</label></div>
								<div class="col-6 col-4b">
									<input type="text" class="form-control mb10" placeholder="Street Address" name="street_address" <?php echo"value='$e[street_address]'"; ?> maxlength="100">
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Country <span class="f-merah">*</span></label></div>
								<div class="col-6 col-4b">
									<select name="negara" required class="form-control" id="negara">
										<?php
										$sql=mysqli_query($koneksi,"select * from negara order by nama_negara asc");
										while($b=mysqli_fetch_array($sql)){
											$selected="";
											if($b['id_negara']==$e['id_negara']){
												$selected="selected";
											}
											echo"<option value='$b[id_negara]' $selected>$b[nama_negara]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">State/ Province <span class="f-merah">*</span></label></div>
								<div class="col-6 col-4b">
									<select name="propinsi" required class="form-control" id="propinsi">
										<?php
										$sql=mysqli_query($koneksi,"select * from propinsi where id_negara = '$e[id_negara]' order by nama_propinsi asc");
										while($b1=mysqli_fetch_array($sql)){
											$selected="";
											if($b1['id_propinsi']==$e['id_propinsi']){
												$selected="selected";
											}
											echo"<option value='$b1[id_propinsi]' $selected>$b1[nama_propinsi]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">City <span class="f-merah">*</span></label></div>
								<div class="col-6 col-4b">
									<select name="kota" required class="form-control" id="kota">
										<?php
										$sql=mysqli_query($koneksi,"select * from kota where id_propinsi = '$e[id_propinsi]' order by nama_kota asc");
										while($b2=mysqli_fetch_array($sql)){
											$selected="";
											if($b2['id_kota']==$e['id_kota']){
												$selected="selected";
											}
											echo"<option value='$b2[id_kota]' $selected>$b2[nama_kota]</option>";
										}
										?>
									</select>
								</div>
							</div>
                     <div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Shopping Mall</label></div>
								<div class="col-6 col-4b">
									<select name="mall" class="form-control">
										<option>Select Landmark</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from mall");
										while($d=mysqli_fetch_array($sql)){
											$selected="";
											if($d['id_mall']==$e['id_mall']){
												$selected="selected";
											}
											echo"<option value='$d[id_mall]' $selected>$d[nama_mall]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Landmark</label></div>
								<div class="col-6 col-4b">
									<select name="landmark" class="form-control">
										<option>Select Landmark</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from landmark order by nama_landmark asc");
										while($d=mysqli_fetch_array($sql)){
											$selected="";
											if($d['id_landmark']==$e['id_landmark']){
												$selected="selected";
											}
											echo"<option value='$d[id_landmark]' $selected>$d[nama_landmark]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Telephone</label></div>
								<div class="col-6 col-4b">
									<input type="text" class="form-control" placeholder="Write Phone Number" name="telephone" <?php echo"value='$e[telephone]'"; ?>>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Facsimile</label></div>
								<div class="col-6 col-4b">
									<input type="text" class="form-control" placeholder="Write Facsimile" name="facsimile" <?php echo"value='$e[fax]'"; ?>>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Postal Code</label></div>
								<div class="col-6 col-4b">
									<input type="text" class="form-control" placeholder="Write Postal Code" name="postal_code" <?php echo"value='$e[postal_code]'"; ?>>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Reservation</label></div>
								<div class="col-6 col-4b">
									<input type="text" class="form-control" placeholder="Write Phone Number" name="reservation_phone" <?php echo"value='$e[reservation_phone]'"; ?>>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Email</label></div>
								<div class="col-6 col-4b">
									<input type="email" class="form-control" placeholder="Write Email" name="email_resto" <?php echo"value='$e[email_resto]'"; ?>>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Website</label></div>
								<div class="col-6 col-4b">
									<input type="text" class="form-control" placeholder="Write Website Url" name="web" <?php echo"value='$e[web]'"; ?>>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Social Media</label></div>
								<div class="col-6 col-4b">
									<input type="text" class="form-control mb10" placeholder="Write Facebook Url" name="facebook" <?php echo"value='$e[facebook]'"; ?>>
									<input type="text" class="form-control mb10" placeholder="Write Twitter Url" name="twitter" <?php echo"value='$e[twitter]'"; ?>>
									<input type="text" class="form-control mb10" placeholder="Write Instagram Url" name="instagram" <?php echo"value='$e[instagram]'"; ?>>
									<input type="text" class="form-control" placeholder="Write Pinterest Url" name="pinterest" <?php echo"value='$e[pinterest]'"; ?>>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Branch</label></div>
								<div class="col-6 col-17">
									<textarea class="form-control" name="branch_name" placeholder="Write Branch" rows="2"><?php echo"$e[branch_name]"; ?></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="box-info-2">
						<div class="box-head">
							Main Information<i class="fa fa-angle-up klik"></i>
						</div>
						<div class="box-panel">
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Pork Serving</label></div>
								<div class="col-8 col-4b">
									<div class="radio radio-inline radio-danger">
										<input type="radio" <?php if($e['pork_serving']=="Yes"){echo"checked=''";} ?> name="pork" value="Yes" id="pork1">
										<label for="pork1"> Yes </label>
									</div>
									<div class="radio radio-inline radio-danger">
										<input type="radio" <?php if($e['pork_serving']=="No"){echo"checked=''";} ?> name="pork" value="No" id="pork2">
										<label for="pork2"> No </label>
									</div>
									<div class="radio radio-inline radio-danger">
										<input type="radio" <?php if($e['pork_serving']=="Halal"){echo"checked=''";} ?> name="pork" value="Halal" id="pork3">
										<label for="pork3"> Halal </label>
									</div>
                           <div class="radio radio-inline radio-danger">
										<input type="radio" <?php if($e['pork_serving']=="vege"){echo"checked=''";} ?> name="pork" value="vege" id="pork4">
										<label for="pork4"> Vegetable </label>
									</div>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Alcohol Serving</label></div>
								<div class="col-8 col-4b">
									<div class="radio radio-inline radio-danger">
										<input type="radio" <?php if($e['alcohol_serving']=="Yes"){echo"checked=''";} ?> name="alcohol" value="Yes" id="alcohol1">
										<label for="alcohol1"> Yes </label>
									</div>
									<div class="radio radio-inline radio-danger">
										<input type="radio" <?php if($e['alcohol_serving']=="No"){echo"checked=''";} ?> name="alcohol" value="No" id="alcohol2">
										<label for="alcohol2"> No </label>
									</div>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Price Index <span class="f-merah">*</span></label></div>
								<div class="col-6 col-4b">
									<select name="price_index" required class="form-control">
										<?php
										$sql=mysqli_query($koneksi,"select * from price_index order by nama_price_index asc");
										while($g=mysqli_fetch_array($sql)){
											$selected="";
											if($g['id_price_index']==$e['id_price_index']){
												$selected="selected";
											}
											echo"<option value='$g[id_price_index]' $selected>$g[nama_price_index]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Suitable For <span class="f-merah">*</span></label></div>
								<div class="col-6 col-4b">
									<select name="suitable_for" required class="form-control">
										<?php
										$sql=mysqli_query($koneksi,"select * from suitable_for order by nama_suitable_for asc");
										while($h=mysqli_fetch_array($sql)){
											$selected="";
											if($h['id_suitable_for']==$e['id_suitable_for']){
												$selected="selected";
											}
											echo"<option value='$h[id_suitable_for]' $selected>$h[nama_suitable_for]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Serving Time</label></div>
								<div class="col-6 col-4b">
									<select name="serving_time" class="form-control">
										<option value="0">Select Serving Time</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from serving_time order by nama_serving_time asc");
										while($k=mysqli_fetch_array($sql)){
											$selected="";
											if($k['id_serving_time']==$e['id_serving_time']){
												$selected="selected";
											}
											echo"<option value='$k[id_serving_time]' $selected>$k[nama_serving_time]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Type of Service</label></div>
								<div class="col-6 col-4b">
									<select name="type_of_service" class="form-control">
										<option value="0">Select Type of Service</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from type_of_service order by nama_type_of_service asc");
										while($l=mysqli_fetch_array($sql)){
											$selected="";
											if($l['id_type_of_service']==$e['id_type_of_service']){
												$selected="selected";
											}
											echo"<option value='$l[id_type_of_service]' $selected>$l[nama_type_of_service]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Air Conditioning</label></div>
								<div class="col-6 col-4b">
									<select name="air_conditioning" class="form-control">
										<option value="0">Select Air Conditioning</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from air_conditioning order by nama_air_conditioning asc");
										while($l1=mysqli_fetch_array($sql)){
											$selected="";
											if($l1['id_air_conditioning']==$e['id_air_conditioning']){
												$selected="selected";
											}
											echo"<option value='$l1[id_air_conditioning]' $selected>$l1[nama_air_conditioning]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Heating System</label></div>
								<div class="col-6 col-4b">
									<select name="heating_system" class="form-control">
										<option value="0">Select Heating Conditioning</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from heating_system order by nama_heating_system asc");
										while($l2=mysqli_fetch_array($sql)){
											$selected="";
											if($l2['id_heating_system']==$e['id_heating_system']){
												$selected="selected";
											}
											echo"<option value='$l2[id_heating_system]' $selected>$l2[nama_heating_system]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Wi-Fi <span class="f-merah">*</span></label></div>
								<div class="col-6 col-4b">
									<select name="wifi" required class="form-control">
										<?php
										$sql=mysqli_query($koneksi,"select * from wifi order by nama_wifi asc");
										while($n=mysqli_fetch_array($sql)){
											$selected="";
											if($n['id_wifi']==$e['id_wifi']){
												$selected="selected";
											}
											echo"<option value='$n[id_wifi]' $selected>$n[nama_wifi]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Payment <span class="f-merah">*</span></label></div>
								<div class="col-6 col-4b">
									<select name="payment" required class="form-control">
										<?php
										$sql=mysqli_query($koneksi,"select * from term_of_payment order by nama_term_of_payment asc");
										while($o=mysqli_fetch_array($sql)){
											$selected="";
											if($o['id_term_of_payment']==$e['id_term_of_payment']){
												$selected="selected";
											}
											echo"<option value='$o[id_term_of_payment]' $selected>$o[nama_term_of_payment]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Premise's Security</label></div>
								<div class="col-6 col-4b">
									<select name="premise_security" class="form-control">
										<option value="0">Select premise's Security</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from premise_security order by nama_premise_security asc");
										while($p=mysqli_fetch_array($sql)){
											$selected="";
											if($p['id_premise_security']==$e['id_premise_security']){
												$selected="selected";
											}
											echo"<option value='$p[id_premise_security]' $selected>$p[nama_premise_security]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Parking Spaces</label></div>
								<div class="col-6 col-4b">
									<select name="parking_spaces" class="form-control">
										<option value="0">Select Parking Spaces</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from parking_spaces order by nama_parking_spaces asc");
										while($q=mysqli_fetch_array($sql)){
											$selected="";
											if($q['id_parking_spaces']==$e['id_parking_spaces']){
												$selected="selected";
											}
											echo"<option value='$q[id_parking_spaces]' $selected>$q[nama_parking_spaces]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Premise's Fire Safety</label></div>
								<div class="col-6 col-4b">
									<select name="premise_fire_safety" class="form-control">
										<option value="0">Select Premise's Fire Safety</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from premise_fire_safety order by nama_premise_fire_safety asc");
										while($r=mysqli_fetch_array($sql)){
											$selected="";
											if($r['id_premise_fire_safety']==$e['id_premise_fire_safety']){
												$selected="selected";
											}
											echo"<option value='$r[id_premise_fire_safety]' $selected>$r[nama_premise_fire_safety]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Premise's Hygiene</label></div>
								<div class="col-6 col-4b">
									<select name="premise_hygiene" class="form-control">
										<option value="0">Select Premise's Hygiene</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from premise_hygiene order by nama_premise_hygiene asc");
										while($r1=mysqli_fetch_array($sql)){
											$selected="";
											if($r1['id_premise_hygiene']==$e['id_premise_hygiene']){
												$selected="selected";
											}
											echo"<option value='$r1[id_premise_hygiene]' $selected>$r1[nama_premise_hygiene]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Premise's maintenance</label></div>
								<div class="col-6 col-4b">
									<select name="premise_maintenance" class="form-control">
										<option value="0">Select Premise Maintenance</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from premise_maintenance order by nama_premise_maintenance asc");
										while($s=mysqli_fetch_array($sql)){
											$selected="";
											if($s['id_premise_maintenance']==$e['id_premise_maintenance']){
												$selected="selected";
											}
											echo"<option value='$s[id_premise_maintenance]' $selected>$s[nama_premise_maintenance]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Ambience</label></div>
								<div class="col-6 col-4b">
									<select name="ambience" class="form-control">
										<option value="0">Select Ambience</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from ambience order by nama_ambience asc");
										while($t=mysqli_fetch_array($sql)){
											$selected="";
											if($t['id_ambience']==$e['id_ambience']){
												$selected="selected";
											}
											echo"<option value='$t[id_ambience]' $selected>$t[nama_ambience]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Attire</label></div>
								<div class="col-6 col-4b">
									<select name="attire" class="form-control">
										<option value="0">Select Attire </option>
										<?php
										$sql=mysqli_query($koneksi,"select * from attire order by nama_attire asc");
										while($u=mysqli_fetch_array($sql)){
											$selected="";
											if($u['id_attire']==$e['id_attire']){
												$selected="selected";
											}
											echo"<option value='$u[id_attire]' $selected>$u[nama_attire]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Clean Washroom</label></div>
								<div class="col-6 col-4b">
									<select name="clean_washroom" class="form-control">
										<option value="0">Select Clean Washroom</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from clean_washroom order by nama_clean_washroom asc");
										while($v=mysqli_fetch_array($sql)){
											$selected="";
											if($v['id_clean_washroom']==$e['id_clean_washroom']){
												$selected="selected";
											}
											echo"<option value='$v[id_clean_washroom]' $selected>$v[nama_clean_washroom]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Tables Availability</label></div>
								<div class="col-6 col-4b">
									<select name="tables_availability" class="form-control">
										<option value="0">Select Tables Availability</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from tables_availability order by nama_tables_availability asc");
										while($w=mysqli_fetch_array($sql)){
											$selected="";
											if($w['id_tables_availability']==$e['id_tables_availability']){
												$selected="selected";
											}
											echo"<option value='$w[id_tables_availability]' $selected>$w[nama_tables_availability]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Noise Level</label></div>
								<div class="col-6 col-4b">
									<select name="noise_level" class="form-control">
										<option value="0">Select Noise Level</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from noise_level order by nama_noise_level asc");
										while($x=mysqli_fetch_array($sql)){
											$selected="";
											if($x['id_noise_level']==$e['id_noise_level']){
												$selected="selected";
											}
											echo"<option value='$x[id_noise_level]' $selected>$x[nama_noise_level]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Waiter Tipping</label></div>
								<div class="col-6 col-4b">
									<select name="waiter_tipping" class="form-control">
										<option value="0">Select Waiter Tipping</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from waiter_tipping order by nama_waiter_tipping asc");
										while($y=mysqli_fetch_array($sql)){
											$selected="";
											if($y['id_waiter_tipping']==$e['id_waiter_tipping']){
												$selected="selected";
											}
											echo"<option value='$y[id_waiter_tipping]' $selected>$y[nama_waiter_tipping]</option>";
										}
										?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="box-info-2">
						<div class="box-head">
							Business Hours<i class="fa fa-angle-down klik"></i>
						</div>
						<div class="box-panel dn">
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Operational</label></div>
								<div class="col-6 col-4b">
									<select name="operation_hour" class="form-control">
                    <option value='0'>No Information</option>
										<?php
										$sql=mysqli_query($koneksi,"select * from operation_hour order by nama_operation_hour asc");
										while($e1=mysqli_fetch_array($sql)){
											$selected="";
											if($e1['id_operation_hour']==$e['id_operation_hour']){
												$selected="selected";
											}
											echo"<option value='$e1[id_operation_hour]' $selected>$e1[nama_operation_hour]</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-16 res-table">
									<table class="table">
										<tr>
											<td></td>
											<th colspan="2">Restaurant Hour</th>
											<th colspan="2">Bar Hour</th>
                                 <th colspan="2">Option</th>
										</tr>

										<tr>
											<th class="vc">Monday</th>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="resto_time3" name="resto_time3" <?php echo"value='$e[resto_time3]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="resto_time4" name="resto_time4" <?php echo"value='$e[resto_time4]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="resto_time3a" name="resto_time3a" <?php echo"value='$e[resto_time3a]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="resto_time4a" name="resto_time4a" <?php echo"value='$e[resto_time4a]'"; ?>></td>
                                 <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                           				<input name="resto_monday" value="Close" id="resto_monday" <?php if (empty($e['resto_time3'])) { echo "checked";} ?> type="checkbox">
                           				<label for="resto_monday"> Close</label>
                           			</div>
                                 </td>
                                 <td>
                                   <div class="checkbox checkbox-inline checkbox-danger">
                                 <div class="btn btn-danger" id="duplicate">
                                      Apply All Days
                                      </div>
                               </div>
                                 </td>
										</tr>
										<tr>
											<th class="vc">Tuesday</th>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="resto_time5" name="resto_time5" <?php echo"value='$e[resto_time5]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="resto_time6" name="resto_time6" <?php echo"value='$e[resto_time6]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="resto_time5a" name="resto_time5a" <?php echo"value='$e[resto_time5a]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="resto_time6a" name="resto_time6a" <?php echo"value='$e[resto_time6a]'"; ?>></td>
                                 <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                           				<input name="resto_tuesday" value="Close" id="resto_tuesday" <?php if (empty($e['resto_time6'])) { echo "checked";} ?> type="checkbox">
                           				<label for="resto_tuesday"> Close</label>
                           			</div>
                                 </td>
                                 <td></td>
										</tr>
										<tr>
											<th class="vc">Wednesday</th>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="resto_time7" name="resto_time7" <?php echo"value='$e[resto_time7]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="resto_time8" name="resto_time8" <?php echo"value='$e[resto_time8]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="resto_time7a" name="resto_time7a" <?php echo"value='$e[resto_time7a]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="resto_time8a" name="resto_time8a" <?php echo"value='$e[resto_time8a]'"; ?>></td>
                                 <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                           				<input name="resto_wednesday" value="Close" id="resto_wednesday" <?php if (empty($e['resto_time7'])) { echo "checked";} ?> type="checkbox">
                           				<label for="resto_wednesday"> Close</label>
                           			</div>
                                 </td>
                                 <td></td>
										</tr>
										<tr>
											<th class="vc">Thursday</th>
											<td><input type="text" class="form-control time" placeholder="Open Time"  id="resto_time9" name="resto_time9" <?php echo"value='$e[resto_time9]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="resto_time10" name="resto_time10" <?php echo"value='$e[resto_time10]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="resto_time9a" name="resto_time9a" <?php echo"value='$e[resto_time9a]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="resto_time10a" name="resto_time10a" <?php echo"value='$e[resto_time10a]'"; ?>></td>
                                 <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                           				<input name="resto_thursday" value="Close" <?php if (empty($e['resto_time9'])) { echo "checked";} ?> id="resto_thursday" type="checkbox">
                           				<label for="resto_thursday"> Close</label>
                           			</div>
                                 </td>
                                 <td></td>
										</tr>
										<tr>
											<th class="vc">Friday</th>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="resto_time11" name="resto_time11" <?php echo"value='$e[resto_time11]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="resto_time12" name="resto_time12" <?php echo"value='$e[resto_time12]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="resto_time11a" name="resto_time11a" <?php echo"value='$e[resto_time11a]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="resto_time12a" name="resto_time12a" <?php echo"value='$e[resto_time12a]'"; ?>></td>
                                 <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                           				<input name="resto_friday" value="Close" <?php if (empty($e['resto_time11'])) { echo "checked";} ?> id="resto_friday" type="checkbox">
                           				<label for="resto_friday"> Close</label>
                           			</div>
                                 </td>
                                 <td></td>
										</tr>
										<tr>
											<th class="vc">Saturday</th>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="resto_time13" name="resto_time13" <?php echo"value='$e[resto_time13]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Select Time" id="resto_time14" name="resto_time14" <?php echo"value='$e[resto_time14]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="resto_time13a" name="resto_time13a" <?php echo"value='$e[resto_time13a]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Select Time" id="resto_time14a" name="resto_time14a" <?php echo"value='$e[resto_time14a]'"; ?>></td>
                                 <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                           				<input name="resto_saturday" value="Close" <?php if (empty($e['resto_time13'])) { echo "checked";} ?> id="resto_saturday" type="checkbox">
                           				<label for="resto_saturday"> Close</label>
                           			</div>
                                 </td>
                                 <td></td>
										</tr>
                    <tr>
											<th class="vc">Sunday</th>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="resto_time1" name="resto_time1" <?php echo"value='$e[resto_time1]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="resto_time2" name="resto_time2" <?php echo"value='$e[resto_time2]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="resto_time1a" name="resto_time1a" <?php echo"value='$e[resto_time1a]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="resto_time2a" name="resto_time2a" <?php echo"value='$e[resto_time2a]'"; ?>></td>
                                 <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                           				<input name="resto_sunday" value="Close" id="resto_sunday" <?php if (empty($e['resto_time1'])) { echo "checked";} ?> type="checkbox">
                           				<label for="resto_sunday"> Close</label>
                           			</div>
                                 </td>
                                 <td>

                                 </td>
										</tr>
									</table>

                           <table class="table">
										<tr>
											<td></td>
											<th colspan="2">Bar Hour 1</th>
											<th colspan="2">Bar Hour 2</th>
                                 <th colspan="2">Option</th>
										</tr>

										<tr>
											<th class="vc">Monday</th>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="bar_time3" name="bar_time3" <?php echo"value='$e[bar_time3]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="bar_time4" name="bar_time4" <?php echo"value='$e[bar_time4]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="bar_time3a" name="bar_time3a" <?php echo"value='$e[bar_time3a]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="bar_time4a" name="bar_time4a" <?php echo"value='$e[bar_time4a]'"; ?>></td>
                                 <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                           				<input name="bar_monday" value="Close" id="bar_monday" <?php if (empty($e['bar_time3'])) { echo "checked";} ?> type="checkbox">
                           				<label for="bar_monday"> Close</label>
                           			</div>
                                 </td>
                                 <td>
                                   <div class="checkbox checkbox-inline checkbox-danger">
                                     <div class="btn btn-danger" id="duplicate2">
                                        Apply All Days
                                      </div>
                                   </div>
                                 </td>
										</tr>
										<tr>
											<th class="vc">Tuesday</th>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="bar_time5" name="bar_time5" <?php echo"value='$e[bar_time5]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="bar_time6" name="bar_time6" <?php echo"value='$e[bar_time6]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="bar_time5a" name="bar_time5a" <?php echo"value='$e[bar_time5a]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="bar_time6a" name="bar_time6a" <?php echo"value='$e[bar_time6a]'"; ?>></td>
                                 <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                           				<input name="bar_tuesday" value="Close" id="bar_tuesday" <?php if (empty($e['bar_time6'])) { echo "checked";} ?> type="checkbox">
                           				<label for="bar_tuesday"> Close</label>
                           			</div>
                                 </td>
                                 <td></td>
										</tr>
										<tr>
											<th class="vc">Wednesday</th>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="bar_time7" name="bar_time7" <?php echo"value='$e[bar_time7]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="bar_time8" name="bar_time8" <?php echo"value='$e[bar_time8]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="bar_time7a" name="bar_time7a" <?php echo"value='$e[bar_time7a]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="bar_time8a" name="bar_time8a" <?php echo"value='$e[bar_time8a]'"; ?>></td>
                                 <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                           				<input name="bar_wednesday" value="Close" id="bar_wednesday" <?php if (empty($e['bar_time7'])) { echo "checked";} ?> type="checkbox">
                           				<label for="bar_wednesday"> Close</label>
                           			</div>
                                 </td>
                                 <td></td>
										</tr>
										<tr>
											<th class="vc">Thursday</th>
											<td><input type="text" class="form-control time" placeholder="Open Time"  id="bar_time9" name="bar_time9" <?php echo"value='$e[bar_time9]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="bar_time10" name="bar_time10" <?php echo"value='$e[bar_time10]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="bar_time9a" name="bar_time9a" <?php echo"value='$e[bar_time9a]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="bar_time10a" name="bar_time10a" <?php echo"value='$e[bar_time10a]'"; ?>></td>
                                 <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                           				<input name="bar_thursday" value="Close" <?php if (empty($e['bar_time9'])) { echo "checked";} ?> id="bar_thursday" type="checkbox">
                           				<label for="bar_thursday"> Close</label>
                           			</div>
                                 </td>
                                 <td></td>
										</tr>
										<tr>
											<th class="vc">Friday</th>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="bar_time11" name="bar_time11" <?php echo"value='$e[bar_time11]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="bar_time12" name="bar_time12" <?php echo"value='$e[bar_time12]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="bar_time11a" name="bar_time11a" <?php echo"value='$e[bar_time11a]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="bar_time12a" name="bar_time12a" <?php echo"value='$e[bar_time12a]'"; ?>></td>
                                 <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                           				<input name="bar_friday" value="Close" <?php if (empty($e['bar_time11'])) { echo "checked";} ?> id="bar_friday" type="checkbox">
                           				<label for="bar_friday"> Close</label>
                           			</div>
                                 </td>
                                 <td></td>
										</tr>
										<tr>
											<th class="vc">Saturday</th>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="bar_time13" name="bar_time13" <?php echo"value='$e[bar_time13]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Select Time" id="bar_time14" name="bar_time14" <?php echo"value='$e[bar_time14]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="bar_time13a" name="bar_time13a" <?php echo"value='$e[bar_time13a]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Select Time" id="bar_time14a" name="bar_time14a" <?php echo"value='$e[bar_time14a]'"; ?>></td>
                                 <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                           				<input name="bar_saturday" value="Close" <?php if (empty($e['bar_time13'])) { echo "checked";} ?> id="bar_saturday" type="checkbox">
                           				<label for="bar_saturday"> Close</label>
                           			</div>
                                 </td>
                                 <td></td>
										</tr>
                    <tr>
											<th class="vc">Sunday</th>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="bar_time1" name="bar_time1" <?php echo"value='$e[bar_time1]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="bar_time2" name="bar_time2" <?php echo"value='$e[bar_time2]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Open Time" id="bar_time1a" name="bar_time1a" <?php echo"value='$e[bar_time1a]'"; ?>></td>
											<td><input type="text" class="form-control time" placeholder="Close Time" id="bar_time2a" name="bar_time2a" <?php echo"value='$e[bar_time2a]'"; ?>></td>
                                 <td>
                                    <div class="checkbox checkbox-inline checkbox-danger">
                           				<input name="bar_sunday" value="Close" id="bar_sunday" <?php if (empty($e['bar_time1'])) { echo "checked";} ?> type="checkbox">
                           				<label for="bar_sunday"> Close</label>
                           			</div>
                                 </td>
                                 <td>

                                 </td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="box-info-2">
						<div class="box-head">
							Map<i class="fa fa-angle-up klik"></i>
						</div>
						<div class="box-panel">
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Input Address</label></div>
                <div class="col-6 col-4b">
                  <input id="search_address" class="controls form-control" type="text" value="<?php echo"$e[restaurant_name]";?>" placeholder="Enter a location" /><br>
                  <div id="geoloc" class="btn btn-danger">Your Location</div>
                  <br>
								</div>
              </div>
							<div class="row">
								<div class="col-16"><br>
                  <div id="map-canvas" style="height:500px"></div>
									<ul id="geoData">
                    <input id="lat" name="lat" value="<?php echo"$e[latitude]";?>" type="hidden" />
                    <input id="long" name="lng" value="<?php echo"$e[longitude]";?>" type="hidden" />
									</ul>
                  <?php
                  $sqls=mysqli_query($koneksi,"select * from negara where id_negara='$e[id_negara]' order by nama_negara asc");
                  $bs=mysqli_fetch_array($sqls);

                  if (empty($e['latitude'])) {
                    $lat = str_replace(',', '.', $bs['latitude_negara']) ;  $lng = str_replace(',', '.', $bs['longitude_negara']); $nama= $bs['nama_negara'];
                  }
                  else {
                    $lat = $e['latitude'];  $lng = $e['longitude']; $nama= $e['restaurant_name'];
                  }
                    echo "$bs[latitude_negara] --- $bs[longitude_negara]";
                   ?>
                </div><!-- col-sm-6 -->
							</div>
						</div>
					</div>
					<div class="box-info-2">
						<div class="box-head">
							Restaurant Description<i class="fa fa-angle-down klik"></i>
						</div>
						<div class="box-panel dn">
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Description</label></div>
								<div class="col-17 col-17">
									<textarea class="form-control" name="description" placeholder="Write Restaurant Description" rows="4"><?php echo"$e[restaurant_description]"; ?></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="box-info-2">
						<div class="box-head">
							Business Status<i class="fa fa-angle-down klik"></i>
						</div>
						<div class="box-panel dn">
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Status</label></div>
								<div class="col-8 col-4b">
									<div class="radio radio-inline radio-danger">
										<input type="radio" <?php if($e['business_status']=="Ongoing"){echo"checked=''";} ?>  name="business_status" value="Ongoing" id="status1">
										<label for="status1"> Ongoing </label>
									</div>
									<div class="radio radio-inline radio-danger">
										<input type="radio" <?php if($e['business_status']=="Renovation"){echo"checked=''";} ?> name="business_status" value="Renovation" id="status2">
										<label for="status2"> Renovation </label>
									</div>
									<div class="radio radio-inline radio-danger">
										<input type="radio" <?php if($e['business_status']=="Relocate"){echo"checked=''";} ?> name="business_status" value="Relocate" id="status3">
										<label for="status3"> Relocate </label>
									</div>
									<div class="radio radio-inline radio-danger">
										<input type="radio" <?php if($e['business_status']=="Ceased Operation"){echo"checked=''";} ?> name="business_status" value="Ceased Operation" id="status4">
										<label for="status4"> Ceased Operation </label>
									</div>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-4 col-4b"><label class="control-label">Description</label></div>
								<div class="col-17 col-17">
									<textarea class="form-control" name="business_description" placeholder="Write further information if applicable" rows="4"><?php echo"$e[business_status_description]"; ?></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="box-info-2">
						<div class="box-head">
							Features and Facility<i class="fa fa-angle-down klik"></i>
						</div>
						<div class="box-panel dn">
							<div class="row mb20">
								<?php
								$fasilitas=tulis_cekbox($e['facility'],$koneksi,'facility');
								echo"$fasilitas";
								?>
							</div>
						</div>
					</div>
					<div class="box-info-2">
						<div class="box-head">
							Cuisine<i class="fa fa-angle-down klik"></i>
						</div>
						<div class="box-panel dn">
							<div class="row mb20">
								<?php
								$cuisine=tulis_cekbox($e['cuisine'],$koneksi,'cuisine');
								echo"$cuisine";
								?>
							</div>
						</div>
					</div>
					<div class="box-info-2">
						<div class="box-head">
							Serving<i class="fa fa-angle-down klik"></i>
						</div>
						<div class="box-panel dn">
							<div class="row mb20">
								<?php
								$serving=tulis_cekbox($e['serving'],$koneksi,'serving');
								echo"$serving";
								?>
							</div>
						</div>
					</div>
					<div class="box-info-2">
						<div class="box-head">
							Type of Serving<i class="fa fa-angle-down klik"></i>
						</div>
						<div class="box-panel dn">
							<div class="row mb20">
								<?php
								$type_serving=tulis_cekbox($e['type_of_serving'],$koneksi,'type_of_serving');
								echo"$type_serving";
								?>
							</div>
						</div>
					</div>
					<button type="submit" class="btn btn-danger">Submit</button>
				</form>
			</div>
		</div>
    </div>
    <?php include"config/inc/footer.php"; ?>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/select2.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap-timepicker.min.js"></script>

  <script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
   <script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
   <script>
      $(document).ready(function() {
        $('.drawer').drawer({
			    iscroll: {
						scrollbars: true,
						mouseWheel: true,
						interactiveScrollbars: true,
						shrinkScrollbars: 'scale',
						fadeScrollbars: true,
						click: true
			    },
			    showOverlay: true
	      });

			$('.tutup').click(function(){
				$('.iklanan').toggleClass('hilang', 500);
			});

			$('.cart').click(function(e){
				e.stopPropagation();
				$('.dis-cart').toggle();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$('.plus').click(function(e){
				e.stopPropagation();
				$('.dis-plus').toggle();
				$('.dis-cart').hide();
				$('.dis-share').hide();
			});
			$('.share').click(function(e){
				e.stopPropagation();
				$('.dis-share').toggle();
				$('.dis-plus').hide();
				$('.dis-cart').hide();
			});
			$('.drawer-toggle').click(function(){
				$('.dis-cart').hide();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$(document).click(function () {
				 var $el = $(".dis-share");
				 if ($el.is(":visible")) {
					  $el.fadeOut(200);
				 }
				 var $ela = $(".dis-plus");
				 if ($ela.is(":visible")) {
					  $ela.fadeOut(200);
				 }
				 var $elu = $(".dis-cart");
				 if ($elu.is(":visible")) {
					  $elu.fadeOut(200);
				 }
		   });

      });
   </script>
   <script type="text/javascript">
		$(document).ready(function () {
			$(document).on("contextmenu",function(e){
				if(e.target.nodeName != "INPUT" && e.target.nodeName != "TEXTAREA")
				e.preventDefault();
			});
			$.fn.disableTextSelect = function() {
				return this.each(function() {
					$(this).css({
						'MozUserSelect':'none',
						'webkitUserSelect':'none'
					}).attr('unselectable','on').bind('selectstart', function() {
						return false;
					});
				});
			};
			$('body').disableTextSelect();
		});
	</script>

	<script type="text/javascript">
	$(document).ready(function () {
		$(".dn").hide();
		$("select").select2();
		$(".alert").fadeTo(5000, 500).fadeOut(500, function(){
			$(".alert").alert('close');
		});

    $("#duplicate").click(function(){
       var resto_time1 = $("#resto_time1").val();
       var resto_time2 = $("#resto_time2").val();
       var resto_time3 = $("#resto_time3").val();
       var resto_time4 = $("#resto_time4").val();
       var resto_time5 = $("#resto_time5").val();
       var resto_time6 = $("#resto_time6").val();
       var resto_time7 = $("#resto_time7").val();
       var resto_time8 = $("#resto_time8").val();
       var resto_time9 = $("#resto_time9").val();
       var resto_time10 = $("#resto_time10").val();
       var resto_time11 = $("#resto_time11").val();
       var resto_time12 = $("#resto_time12").val();
       var resto_time13 = $("#resto_time13").val();
       var resto_time14 = $("#resto_time14").val();

       var resto_time1a = $("#resto_time1a").val();
       var resto_time2a = $("#resto_time2a").val();
       var resto_time3a = $("#resto_time3a").val();
       var resto_time4a = $("#resto_time4a").val();
       var resto_time5a = $("#resto_time5a").val();
       var resto_time6a = $("#resto_time6a").val();
       var resto_time7a = $("#resto_time7a").val();
       var resto_time8a = $("#resto_time8a").val();
       var resto_time9a = $("#resto_time9a").val();
       var resto_time10a = $("#resto_time10a").val();
       var resto_time11a = $("#resto_time11a").val();
       var resto_time12a = $("#resto_time12a").val();
       var resto_time13a = $("#resto_time13a").val();
       var resto_time14a = $("#resto_time14a").val();

       $("#resto_time1").val(resto_time3);
       $("#resto_time2").val(resto_time4);
       $("#resto_time5").val(resto_time3);
       $("#resto_time6").val(resto_time4);
       $("#resto_time7").val(resto_time3);
       $("#resto_time8").val(resto_time4);
       $("#resto_time9").val(resto_time3);
       $("#resto_time10").val(resto_time4);
       $("#resto_time11").val(resto_time3);
       $("#resto_time12").val(resto_time4);
       $("#resto_time13").val(resto_time3);
       $("#resto_time14").val(resto_time4);

       $("#resto_time1a").val(resto_time3a);
       $("#resto_time2a").val(resto_time4a);
       $("#resto_time5a").val(resto_time3a);
       $("#resto_time6a").val(resto_time4a);
       $("#resto_time7a").val(resto_time3a);
       $("#resto_time8a").val(resto_time4a);
       $("#resto_time9a").val(resto_time3a);
       $("#resto_time10a").val(resto_time4a);
       $("#resto_time11a").val(resto_time3a);
       $("#resto_time12a").val(resto_time4a);
       $("#resto_time13a").val(resto_time3a);
       $("#resto_time14a").val(resto_time4a);
    });

    $("#duplicate2").click(function(){
      var bar_time1 = $("#bar_time1").val();
      var bar_time2 = $("#bar_time2").val();
      var bar_time3 = $("#bar_time3").val();
      var bar_time4 = $("#bar_time4").val();
      var bar_time5 = $("#bar_time5").val();
      var bar_time6 = $("#bar_time6").val();
      var bar_time7 = $("#bar_time7").val();
      var bar_time8 = $("#bar_time8").val();
      var bar_time9 = $("#bar_time9").val();
      var bar_time10 = $("#bar_time10").val();
      var bar_time11 = $("#bar_time11").val();
      var bar_time12 = $("#bar_time12").val();
      var bar_time13 = $("#bar_time13").val();
      var bar_time14 = $("#bar_time14").val();

      var bar_time1a = $("#bar_time1a").val();
      var bar_time2a = $("#bar_time2a").val();
      var bar_time3a = $("#bar_time3a").val();
      var bar_time4a = $("#bar_time4a").val();
      var bar_time5a = $("#bar_time5a").val();
      var bar_time6a = $("#bar_time6a").val();
      var bar_time7a = $("#bar_time7a").val();
      var bar_time8a = $("#bar_time8a").val();
      var bar_time9a = $("#bar_time9a").val();
      var bar_time10a = $("#bar_time10a").val();
      var bar_time11a = $("#bar_time11a").val();
      var bar_time12a = $("#bar_time12a").val();
      var bar_time13a = $("#bar_time13a").val();
      var bar_time14a = $("#bar_time14a").val();

      $("#bar_time1").val(bar_time3);
      $("#bar_time2").val(bar_time4);
      $("#bar_time5").val(bar_time3);
      $("#bar_time6").val(bar_time4);
      $("#bar_time7").val(bar_time3);
      $("#bar_time8").val(bar_time4);
      $("#bar_time9").val(bar_time3);
      $("#bar_time10").val(bar_time4);
      $("#bar_time11").val(bar_time3);
      $("#bar_time12").val(bar_time4);
      $("#bar_time13").val(bar_time3);
      $("#bar_time14").val(bar_time4);

      $("#bar_time1a").val(bar_time3a);
      $("#bar_time2a").val(bar_time4a);
      $("#bar_time5a").val(bar_time3a);
      $("#bar_time6a").val(bar_time4a);
      $("#bar_time7a").val(bar_time3a);
      $("#bar_time8a").val(bar_time4a);
      $("#bar_time9a").val(bar_time3a);
      $("#bar_time10a").val(bar_time4a);
      $("#bar_time11a").val(bar_time3a);
      $("#bar_time12a").val(bar_time4a);
      $("#bar_time13a").val(bar_time3a);
      $("#bar_time14a").val(bar_time4a);
    });


		$("#negara").change(function(){
			var id = $("#negara").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_propinsi.php",
				data: "id=" + id,
				success: function(data){
					$("#propinsi").html(data);
					$("#propinsi").fadeIn(2000);
					$("#propinsi").select2();
				}
			});
		});
		$("#propinsi").change(function(){
			var id = $("#propinsi").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_kota.php",
				data: "id=" + id,
				success: function(data){
					$("#kota").html(data);
					$("#kota").fadeIn(2000);
					$("#kota").select2();
				}
			});
		});

      $(".box-head").click(function(){
         var targeta = $(this).children(".klik");
         $(targeta).toggleClass("fa-angle-up fa-angle-down");
			var target = $(this).parent().children(".box-panel");
			$(target).slideToggle();
		});

		$('.time').timepicker({
			defaultTime:''
		});

	});
	</script>


  <script type="text/javascript">
  $('#search_address').on('keypress', function(e) {
    return e.which !== 13;
  });

  $('.form-control').on('keypress', function (e) {
      var ingnore_key_codes = [34, 39];
      if ($.inArray(e.which, ingnore_key_codes) >= 0) {
          e.preventDefault();
          alert("\'\" is not allowed");
      } else {}
  });

    function initialize() {
    var marker;
    var latlng = new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lng; ?>);
    var geocoder = new google.maps.Geocoder();
    var mapOptions = {
      center: latlng,
      zoom: 17,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      draggableCursor: "pointer",
      streetViewControl: false
    };

    var imageicon = 'https://www.foodieguidances.com/assets/images/foodie-point.png';

    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
          var markers = new google.maps.Marker({
            position: latlng,
            map: map,
            icon : imageicon
          });

    google.maps.event.addListener(map, "click", function (location) {
      //map.setCenter(location.latLng);
      setLatLong(location.latLng.lat(), location.latLng.lng());

      marker.setMap(null);
      markers.setMap(null);

      marker = new google.maps.Marker({
        position: location.latLng,
        map: map,
        icon : imageicon
      });
      marker.setPosition(location.latLng);
      setGeoCoder(location.latLng);
    });

    var input = (document.getElementById('search_address'));

    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);
    autocomplete.setTypes([]);

    var infowindow = new google.maps.InfoWindow();
    marker = new google.maps.Marker({
      map: map,
      anchorPoint: new google.maps.Point(0, -29),
      icon : imageicon
    });

    google.maps.event.addListener(autocomplete, 'place_changed', function() {
      //infowindow.close();
      marker.setVisible(true);
      var place = autocomplete.getPlace();

      if (!place.geometry) return;

      // If the place has a geometry, then present it on a map.
      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);  // Why 17? Because it looks good.
      }
      marker.setIcon(imageicon);
      map.setCenter(20);
      marker.setPosition(place.geometry.location);
      marker.setVisible(true);
      setLatLong(place.geometry.location.lat(), place.geometry.location.lng());

    });

    document.getElementById('geoloc').onclick=function(){
      markers.setMap(null);
      if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

          map.setCenter(pos);
          marker.setPosition(pos);

          setGeoCoder(pos);
          setLatLong(position.coords.latitude, position.coords.longitude);

        }, function() {
          handleNoGeolocation(true);
        });
      } else {
        // Browser doesn't support Geolocation
        handleNoGeolocation(false);
      }
    };


    function setLatLong(lat, long) {
      document.getElementById('lat').value=lat;
      document.getElementById('long').value=long;
    }

    function setGeoCoder(pos) {
      geocoder.geocode({'location': pos}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[0]) {
            document.getElementById('search_address').value=results[0].formatted_address;
          } else {
            document.getElementById('search_address').value='';
          }
        } else {
          document.getElementById('search_address').value='';
        }
      });
    }
    }


    google.maps.event.addDomListener(window, 'load', initialize);

	</script>
<script async defer
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCwYMs0LtJBgwkV937PkNKKR3dbEiD0zA&callback=initialize&libraries=places">
</script>
</body>
</html>
<?php
}
else{
	header("Location: ".$base_url."/login-area");
}
?>
