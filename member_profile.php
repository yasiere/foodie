<?php
session_start();
$auto_logout=1800000;
include "config/func/base_url.php";

include "config/database/db.php";
include "config/func/member_profil.php";
include "config/func/seo.php";
include "config/func/id_masking.php";
$active = "member";
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
    <title><?php echo"$u[nama_depan] $u[nama_belakang]"; ?> - Foodie Guidances</title>
	<meta name="keywords" content="">
    <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>
  <link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<style media="screen">
	.info.ml10-j {
		min-height: 53px;
	}
		@media screen and (min-width: 1025px){
			.member-bg{background:url(<?php echo"$base_url/assets/img/member/$u[gambar_landscape]"; ?>) no-repeat scroll center top ; margin: -20px auto 0px;}
		}
		@media screen and (max-width: 1024px){
			.member-bg{background:url(<?php echo"$base_url/assets/img/member/$u[gambar_landscape]"; ?>) no-repeat scroll center top /cover; margin: -20px auto 0px;}
		}
		@media (max-width: 975px) and (min-width: 200px){
			.text-left{
				float: unset;
			}
			.text-right{
				float: left;
				text-align: left;
			}
		}

	</style>
</head>
 <body style="position: initial;" class="drawer drawer--right">
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>
	<div class="member-bg" style=""></div>
	<div class="member-desc">
		<div class="container">

			<div class="row">
				<div class="col-4 col-8a cfr">
					<img src="<?php echo"$base_url/assets/img/member/$u[gambar_thumb]"; ?>" class="foto_profil">
					<div class="detail">
						<?php echo"<h6>$u[nama_depan] $u[nama_belakang]</h6> $u[gender], $u[nama_negara]"; ?>
					</div>
				</div>
				<div class="col-12 kolj col-8a">
					<h3 >About Me</h3>
					<p><?php echo"$u[deskripsi]"; ?></p>
				</div>
			</div>
		</div>
	</div>
    <div class="container mt20">

		<div class="row">
			<div class="btn-caris">
				<a href="#">
					<img src="<?php echo"$base_url/assets/img/member/$u[gambar_thumb]"; ?>" class="foto_profil">
				</a>
			</div>
			<div class="col-4 search_kiri">
				<ul class="list-unstyled member-social">
					<?php
					if($u['social_fb']==""){
						echo"<li><img src='$base_url/assets/img/theme/fb_29_off.png'> Facebook</li>";
					}
					else{
						echo"<li><img src='$base_url/assets/img/theme/fb_29.png'> <a href='$u[social_fb]' target='_blank'>Facebook</a></li>";
					}
					if($u['social_twitter']==""){
						echo"<li><img src='$base_url/assets/img/theme/tw_29_off.png'> Twitter</li>";
					}
					else{
						echo"<li><img src='$base_url/assets/img/theme/tw_29.png'> <a href='$u[social_twitter]' target='_blank'>Twitter</a></li>";
					}
					if($u['social_pint']==""){
						echo"<li><img src='$base_url/assets/img/theme/pin_29_off.png'> Pinterest</li>";
					}
					else{
						echo"<li><img src='$base_url/assets/img/theme/pin_29.png'> <a href='$u[social_pint]' target='_blank'>Pinterest</a></li>";
					}
					if($u['social_google']==""){
						echo"<li><img src='$base_url/assets/img/theme/gog_29_off.png'> Google+</li>";
					}
					else{
						echo"<li><img src='$base_url/assets/img/theme/gog_29.png'> <a href='$u[social_google]' target='_blank'>Google+</a></li>";
					}
					if($u['social_insta']==""){
						echo"<li><img src='$base_url/assets/img/theme/insta_29_off.png'> Instagram</li>";
					}
					else{
						echo"<li><img src='$base_url/assets/img/theme/insta_29.png'> <a href='$u[social_insta]' target='_blank'>Instagram</a></li>";
					}
					?>
				</ul>
				<hr class="mtb10">
				<ul class="list-unstyled">
					<?php if($session_member==$_GET['id']){ ?>
						<li><a href="<?php echo"$base_url/$u[username]/activity-feed"; ?>">Activity Feed</a></li>
                  <li><span class="buka">
   						Account <div class="pull-right">+</div>
   						<ul class="pl50 sub-menu">
   							<li><a href="<?php echo"$base_url/$u[username]/edit-profile"; ?>">Edit Profile</a></li>
   							<li><a href="<?php echo"$base_url/$u[username]/change-password"; ?>">Change Password</a></li>
   							<li><a href="<?php echo"$base_url/$u[username]/activity-setting"; ?>" >Activity Feed Setting</a></li>
   							<li><a href="<?php echo"$base_url/$u[username]/social-media"; ?>">Social Media</a></li>
   							<li><a href="<?php echo"$base_url/$u[username]/delete-account"; ?>">Delete Account</a></li>
   						</ul>
   						</span>
   					</li>
   					<li><span class="buka">
   						Submit <div class="pull-right">+</div>
   						<ul class="pl50 sub-menu">
   							<li><a href="<?php echo"$base_url/$u[username]/my-restaurant/new"; ?>">Restaurant</a></li>
   							<li><a href="<?php echo"$base_url/$u[username]/my-food/new"; ?>">Food</a></li>
   							<li><a href="<?php echo"$base_url/$u[username]/my-beverage/new"; ?>">Beverage</a></li>
   							<li><a href="<?php echo"$base_url/$u[username]/my-process/new"; ?>">Process F&B</a></li>
   							<li><a href="<?php echo"$base_url/$u[username]/my-recipe/new"; ?>">Recipe</a></li>
   							<li><a href="<?php echo"$base_url/$u[username]/my-article/new"; ?>">Article</a></li>
   							<li><a href="<?php echo"$base_url/$u[username]/my-video/new"; ?>">Video</a></li>
   						</ul>
   						</span>
   					</li>
               <?php } ?>
					<li><a href="<?php echo"$base_url/$u[username]/my-restaurant"; ?>">My Restaurant</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-restaurant-photo"; ?>">My Restaurant Photo</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-restaurant-menu"; ?>">My Restaurant Menu</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-food"; ?>">My Food</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-food-photo"; ?>">My Food Photo</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-beverage"; ?>">My Beverage</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-beverage-photo"; ?>">My Beverage Photo</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-process"; ?>">My Process F&B</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-process-photo"; ?>">My Process F&B Photo</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-events"; ?>">My Events</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-recipe"; ?>">My Recipe</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-recipe-photo"; ?>">My Recipe Photo</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-article"; ?>">My Article</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-article-photo"; ?>">My Article Photo</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-video"; ?>">My Video</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-bookmark"; ?>">Bookmark</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-vote"; ?>">Give Vote to</a></li>
				</ul>
				<?php if($session_member==$_GET['id']){ ?>
				<hr class="mtb10">
				<strong>FGMart</strong>
				<ul class="list-unstyled">
					<li><a href="<?php echo"$base_url/$u[username]/stock-cart"; ?>">Stock Cart</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/download-link"; ?>">Download Link</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/order-history"; ?>">Order History</a></li>
				</ul>
				<?php } ?>
			</div>
			<div class="col-12 col-8a no-padding">
				<div class="row">
					<div class="col-8 col-8a text-left">
						<h4 class="no-mb">Restaurant Review</h4>
					</div>
					<div class="col-8 col-8a text-right">
						<a href="<?php echo"$base_url/$u[username]/my-restaurant/new"; ?>" class="f-merah" style="margin-right:10px">Submit Restaurant</a>
						<a href="<?php echo"$base_url/$u[username]/my-restaurant"; ?>" class="f-merah">View All</a>
					</div>
				</div>
				<div class="media">
					<div class="row">
						<?php
						$resto=mysqli_query($koneksi,"SELECT *,
(SELECT COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS jumlah_vote,
(select p.gambar_restaurant_photo from restaurant_photo p where p.id_restaurant=r.id_restaurant order by p.id_restaurant_photo desc limit 1) as gambar,
(SELECT (SUM(f.cleanlines) + SUM(f.customer_services) + SUM(f.food_beverage) + SUM(f.comfort) + SUM(f.value_money)) / COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant =r.id_restaurant) AS total_rate
FROM restaurant r
LEFT JOIN waiter_tipping ON r.id_waiter_tipping = waiter_tipping.id_waiter_tipping
LEFT JOIN member ON r.id_member = member.id_member
LEFT JOIN premise_hygiene ON r.id_premise_hygiene = premise_hygiene.id_premise_hygiene where member.id_member='$u[id_member]'
ORDER BY `r`.`id_restaurant`  DESC LIMIT 4");
						$ada_resto=mysqli_num_rows($resto);
						if($ada_resto<>0){
							while($s=mysqli_fetch_array($resto)){
                $total_rating=number_format((float)$s['total_rate'], 2, '.', '');
								$slug=seo($s['restaurant_name']);
								$post=date("jS M, Y", strtotime($s['tgl_post']));
								$id_restaurant = id_masking($s['id_restaurant']);
								$gambar=$s['gambar'];
								if(empty($s['gambar'])){
									$gambar="foodieguidances.jpg";
								}
								echo"<div class='col-4 col-4b no-padding no-margin'>
									<div class='thumb'>
										<a href='$base_url/pages/restaurant/info/$id_restaurant/$slug'><img src='$base_url/assets/img/restaurant/small_$gambar'></a>
									</div>
									<div class='info ml10-j'>
										<em>posted on $post</em>
										<h3 class='sembunyi'><a href='$base_url/pages/restaurant/info/$id_restaurant/$slug'>$s[restaurant_name]</a></h3>
										<ul class='list-unstyled f-12'>";
											// <li>$s[dilike] Like <span class='bullet'>&#8226;</span> $s[jumlah] Being here</li>
											echo"Rating : $total_rating from $s[jumlah_vote] votes
										</ul>
									</div>
								</div>";
							}
						}
						else{
							echo"<div class='col-12'>You have still not create restaurant review. Let's make one.</div>";
						}
						?>
					</div>
				</div>
				<div class="row">
					<div class="col-8 col-8a text-left">
						<h4 class="no-mb">Food Review</h4>
					</div>
					<div class="col-8 col-8a text-right">
						<a href="<?php echo"$base_url/$u[username]/my-food/new"; ?>" class="f-merah" style="margin-right:10px">Submit Food</a>
						<a href="<?php echo"$base_url/$u[username]/my-food"; ?>" class="f-merah">View All</a>
					</div>
				</div>
				<div class="media">
					<div class="row">
						<?php
						$resto=mysqli_query($koneksi,"select r.id_food, r.nama_food, r.tgl_posta,
            (select count(h.id_food) from food_like h where h.id_food=r.id_food) as dilike,
            (SELECT COUNT(f.id_member) FROM food_rating f WHERE r.id_food=f.id_food) AS jumlah_vote,
						(SELECT (SUM(fr.cleanlines) + SUM(fr.flavor) + SUM(fr.freshness) + SUM(fr.cooking) + SUM(fr.aroma) + SUM(fr.serving)) / COUNT(fr.id_member) FROM food_rating fr WHERE fr.id_food =r.id_food) AS total_rate,
            (select count(h.id_food) from food h where h.id_restaurant=r.id_restaurant and h.id_food<>r.id_food) as jumlah,
            (select p.gambar_food_photo from food_photo p where p.id_food=r.id_food order by p.id_food_photo desc limit 1) as gambar from
            food r where r.id_member = '$u[id_member]' order by r.id_food desc limit 4");
						$ada_resto=mysqli_num_rows($resto);
						if($ada_resto<>0){
							while($b=mysqli_fetch_array($resto)){
								$total_rating=number_format((float)$b['total_rate'], 2, '.', '');
								$slug=seo($b['nama_food']);
								$post=date("jS M, Y", strtotime($b['tgl_posta']));
								$id_food = id_masking($b['id_food']);
								echo"<div class='col-4 col-4b no-padding'>
									<div class='thumb'>
										<a href='$base_url/pages/food/info/$id_food/$slug'><img src='$base_url/assets/img/food/small_$b[gambar]'></a>
									</div>
									<div class='info ml10-j'>
										<em>posted on $post</em>
										<h3 class='sembunyi'><a href='$base_url/pages/food/info/$id_food/$slug'>$b[nama_food]</a></h3>
										<ul class='list-unstyled f-12'>";
											// <li>$b[dilike] Like <span class='bullet'>&#8226;</span> $b[jumlah] other photo</li>
											echo"
											Rating : $total_rating from $b[jumlah_vote] votes
										</ul>
									</div>
								</div>";
							}
						}
						else{
							echo"<div class='col-12'>You have still not create food review. Let's make one.</div>";
						}
						?>
					</div>
				</div>
				<div class="row">
					<div class="col-8 col-8a text-left">
						<h4 class="no-mb">Beverage Review</h4>
					</div>
					<div class="col-8 col-8a text-right">
						<a href="<?php echo"$base_url/$u[username]/my-beverage/new"; ?> " class="f-merah" style="margin-right:10px">Submit Beverage</a>
						<a href="<?php echo"$base_url/$u[username]/my-beverage"; ?>" class="f-merah">View All</a>
					</div>
				</div>
				<div class="media">
					<div class="row">
						<?php
						$resto=mysqli_query($koneksi,"select r.id_beverage, r.nama_beverage, r.tgl_post,
						(select count(h.id_beverage) from beverage_like h where h.id_beverage=r.id_beverage) as dilike,
						(SELECT COUNT(brr.id_member) FROM beverage_rating brr WHERE brr.id_beverage=r.id_beverage) AS jumlah_vote,
						(SELECT (SUM(br.cleanlines) + SUM(br.flavor) + SUM(br.freshness) + SUM(br.cooking) + SUM(br.aroma) + SUM(br.serving)) / COUNT(br.id_member) FROM beverage_rating br WHERE br.id_beverage =r.id_beverage) AS total_rate,
						(select count(h.id_beverage) from beverage h where h.id_restaurant=r.id_restaurant and h.id_beverage<>r.id_beverage) as jumlah,
						(select p.gambar_beverage_photo from beverage_photo p where p.id_beverage=r.id_beverage order by p.id_beverage_photo desc limit 1) as gambar
						from beverage r where r.id_member = '$u[id_member]' order by r.id_beverage desc limit 4");
						$ada_resto=mysqli_num_rows($resto);
						if($ada_resto<>0){
							while($b=mysqli_fetch_array($resto)){
								$total_rating=number_format((float)$b['total_rate'], 2, '.', '');
								$slug=seo($b['nama_beverage']);
								$post=date("jS M, Y", strtotime($b['tgl_post']));
								$id_beverage = id_masking($b['id_beverage']);
								echo"<div class='col-4 col-4b no-padding'>
									<div class='thumb'>
										<a href='$base_url/pages/beverage/info/$id_beverage/$slug'><img src='$base_url/assets/img/beverage/small_$b[gambar]'></a>
									</div>
									<div class='info ml10-j'>
										<em>posted on $post</em>
										<h3 class='sembunyi'><a href='$base_url/pages/beverage/info/$id_beverage/$slug'>$b[nama_beverage]</a></h3>
										<ul class='list-unstyled f-12'>";
											// <li>$b[dilike] Like <span class='bullet'>&#8226;</span> $b[jumlah] other photo</li>
											echo"
											 Rating : $total_rating from $b[jumlah_vote] votes
										</ul>
									</div>
								</div>";
							}
						}
						else{
							echo"<div class='col-12'>You have still not create beverage review. Let's make one.</div>";
						}
						?>
					</div>
				</div>
				<div class="row">
					<div class="col-8 col-8a text-left">
						<h4 class="no-mb">Recipe Review</h4>
					</div>
					<div class="col-8 col-8a text-right">
						<a href="<?php echo"$base_url/$u[username]/my-recipe/new"; ?>" class="f-merah" style="margin-right:10px">Submit Recipe</a>
						<a href="<?php echo"$base_url/$u[username]/my-recipe"; ?>" class="f-merah">View All</a>
					</div>
				</div>
				<div class="media">
					<div class="row">
						<?php
						$resto=mysqli_query($koneksi,"select r.id_recipe, r.nama_recipe, r.tgl_post,(select p.gambar_recipe_photo from recipe_photo p where p.id_recipe=r.id_recipe order by p.id_recipe_photo desc limit 1) as gambar, (select count(h.id_recipe) from recipe_like h where h.id_recipe=r.id_recipe) as dilike,(select count(h.id_recipe) from recipe_here h where h.id_recipe=r.id_recipe) as jumlah from recipe r where r.id_member = '$u[id_member]' order by r.id_recipe desc limit 4");
						$ada_resto=mysqli_num_rows($resto);
						if($ada_resto<>0){
							while($c=mysqli_fetch_array($resto)){
								$slug=seo($c['nama_recipe']);
								$post=date("jS M, Y", strtotime($c['tgl_post']));
								$id_recipe = id_masking($c['id_recipe']);
								echo"<div class='col-4 col-4b no-padding'>
									<div class='thumb'>
										<a href='$base_url/pages/recipe/info/$id_recipe/$slug'><img src='$base_url/assets/img/recipe/small_$c[gambar]'></a>
									</div>
									<div class='info ml10-j'>
										<em>posted on $post</em>
										<h3 class='sembunyi'><a href='$base_url/pages/recipe/info/$id_recipe/$slug'>$c[nama_recipe]</a></h3>
										<ul class='list-unstyled f-12'>
											<li>$c[dilike] Like <span class='bullet'>&#8226;</span> $c[jumlah] Tried</li>
										</ul>
									</div>
								</div>";
							}
						}
						else{
							echo"<div class='col-12'>You have still not create recipe review. Let's make one.</div>";
						}
						?>
					</div>
				</div>
				<div class="row">
					<div class="col-8 col-8a text-left">
						<h4 class="no-mb">Article Review</h4>
					</div>
					<div class="col-8 col-8a text-right">
						<a href="<?php echo"$base_url/$u[username]/my-article/new"; ?>" class="f-merah" style="margin-right:10px">Submit Article</a>
						<a href="<?php echo"$base_url/$u[username]/my-article"; ?>" class="f-merah">View All</a>
					</div>
				</div>
				<div class="media">
					<div class="row">
						<?php
						$resto=mysqli_query($koneksi,"select *,(select p.gambar_article_photo from article_photo p where p.id_article=r.id_article order by p.id_article_photo desc limit 1) as gambar,(select count(h.id_article) from article_like h where h.id_article=r.id_article) as dilike from article r where r.id_member = '$u[id_member]' order by r.id_article desc limit 4");
						$ada_resto=mysqli_num_rows($resto);
						if($ada_resto<>0){
							while($j=mysqli_fetch_array($resto)){
								$slug=seo($j['nama_article']);
								$post=date("jS M, Y", strtotime($j['tgl_post']));
								$id_article = id_masking($j['id_article']);
								echo"<div class='col-4 col-4b no-padding'>
									<div class='thumb'>
										<a href='$base_url/pages/article/$id_article/$slug'><img src='$base_url/assets/img/article/small_$j[gambar]'></a>
									</div>
									<div class='info ml10-j'>
										<em>posted on $post</em>
										<h3 class='sembunyi'><a href='$base_url/pages/article/$id_article/$slug'>$j[nama_article]</a></h3>
										<ul class='list-unstyled f-12'>
											<li>$j[dilike] Like <span class='bullet'>&#8226;</span> $j[dilihat] View</li>
										</ul>
									</div>
								</div>";
							}
						}
						else{
							echo"<div class='col-12'>You have still not create article. Let's make one.</div>";
						}
						?>
					</div>
				</div>
				<div class="row">
					<div class="col-8 col-8a text-left">
						<h4 class="no-mb">Video Review</h4>
					</div>
					<div class="col-8 col-8a text-right">
						<a href="<?php echo"$base_url/$u[username]/my-video/new"; ?>" class="f-merah" style="margin-right:10px">Submit Video</a>
						<a href="<?php echo"$base_url/$u[username]/my-video"; ?>" class="f-merah">View All</a>
					</div>
				</div>
				<div class="media">
					<div class="row">
						<?php
						$resto=mysqli_query($koneksi,"SELECT * from video r LEFT JOIN video_category v ON r.id_video_category=v.id_video_category where r.id_member = '$u[id_member]' order by r.id_video desc LIMIT 4");
						$ada_resto=mysqli_num_rows($resto);
						if($ada_resto<>0){
							while($j=mysqli_fetch_array($resto)){
								$post=date("jS M, Y", strtotime($j['tgl_post']));
								$url = $j['url'];
								preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
								$id = $matches[1];
								echo"<div class='col-4 col-4b no-padding'>
									<div class=' thumb'>
										<iframe type='text/html' width='220' height='123' src='https://www.youtube.com/embed/$id?rel=0&showinfo=0&color=white&iv_load_policy=3' frameborder='0' allowfullscreen></iframe>
									</div>
									<div class='info ml10-j'>
										<em>posted on $post</em>
										<h3>$j[nama_video]</h3>
										<ul class='list-unstyled f-12'>
											<li><em>category</em>&nbsp;&nbsp;$j[nama_video_category]</li>
										</ul>
									</div>
								</div>";
							}
						}
						else{
							echo"<div class='col-12'>You have still not create article. Let's make one.</div>";
						}
						?>
					</div>
				</div>
			</div>
		</div>
    </div>
    <?php include"config/inc/footer.php"; ?>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/theme.js"></script>
  <script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
	<script>
		 $(document).ready(function() {
			 $('.drawer').drawer();

		 $('.tutup').click(function(){
			 $('.iklanan').toggleClass('hilang', 500);
		 });

		 $('.cart').click(function(e){
			 e.stopPropagation();
			 $('.dis-cart').toggle();
			 $('.dis-plus').hide();
			 $('.dis-share').hide();
		 });
		 $('.plus').click(function(e){
			 e.stopPropagation();
			 $('.dis-plus').toggle();
			 $('.dis-cart').hide();
			 $('.dis-share').hide();
		 });
		 $('.share').click(function(e){
			 e.stopPropagation();
			 $('.dis-share').toggle();
			 $('.dis-plus').hide();
			 $('.dis-cart').hide();
		 });
		 $('.drawer-toggle').click(function(){
			 $('.dis-cart').hide();
			 $('.dis-plus').hide();
			 $('.dis-share').hide();
		 });
		 $('.btn-caris').click(function(){
			 $('.search_kiri').toggleClass('search_kiri_tam', 500);
		 });
		 $(document).click(function () {
				var $el = $(".dis-share");
				if ($el.is(":visible")) {
					 $el.fadeOut(200);
				}
				var $ela = $(".dis-plus");
				if ($ela.is(":visible")) {
					 $ela.fadeOut(200);
				}
				var $elu = $(".dis-cart");
				if ($elu.is(":visible")) {
					 $elu.fadeOut(200);
				}
			});

		 });
	</script>
	<script type="text/javascript">
	$(document).ready(function () {
		$('.sub-menu').hide(); //Hide children by default

		$('.buka').click(function(event){
			$(this).children('.sub-menu').slideToggle('slow');
		});
	});
	</script>
</body>
</html>
