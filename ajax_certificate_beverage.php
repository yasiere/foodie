  <?php

include "config/database/db.php";
include "config/func/base_url.php";
$id=$_POST['id'];
?>

  <link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
  <link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
  <link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">

<script type="text/javascript">
$('.fitin div').css('font-size', '30px');
while( $('.fitin div').height() > $('.fitin').height() ) {
    $('.fitin div').css('font-size', (parseInt($('.fitin div').css('font-size')) - 1) + "px" );
};
$('.restos div').css('font-size', '16px');
while( $('.restos div').height() > $('.restos').height() ) {
    $('.restos div').css('font-size', (parseInt($('.restos div').css('font-size')) - 1) + "px" );
};
$('.restos_al div').css('font-size', '14px');
while( $('.restos_al div').height() > $('.restos_al').height() ) {
    $('.restos_al div').css('font-size', (parseInt($('.restos_al div').css('font-size')) - 1) + "px" );
};
</script>

  <script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
  <script src="<?php echo"$base_url"; ?>/assets/js/bootstrap-ratingys.js"></script>
  <script type="text/javascript">
    $(document).ready(function () {
       $('.ratingys').ratingys();
    });
  </script>

<?php
$resto=mysqli_query($koneksi,"select * , r.id_negara as kode_negara ,f.dilihat,f.id_price_index,f.tgl_post as tgl_bev,(SELECT COUNT(h.id_member) FROM beverage_like h WHERE h.id_beverage=f.id_beverage) AS dilike,(SELECT COUNT(h.id_member) FROM beverage_rekomendasi h WHERE h.id_beverage=f.id_beverage) AS direkomendasi,(SELECT COUNT(r.id_member) FROM beverage_rating r WHERE r.id_beverage=f.id_beverage) AS jumlah_vote,(SELECT SUM(r.cleanlines)/ COUNT(r.id_member) FROM beverage_rating r WHERE r.id_beverage=f.id_beverage) AS t_cleanlines,(SELECT SUM(r.flavor)/ COUNT(r.id_member) FROM beverage_rating r WHERE r.id_beverage=f.id_beverage) AS t_flavor,(SELECT SUM(r.freshness)/ COUNT(r.id_member) FROM beverage_rating r WHERE r.id_beverage=f.id_beverage) AS t_freshness,(SELECT SUM(r.cooking)/ COUNT(r.id_member) FROM beverage_rating r WHERE r.id_beverage=f.id_beverage) AS t_cooking,(SELECT SUM(r.aroma)/ COUNT(r.id_member) FROM beverage_rating r WHERE r.id_beverage=f.id_beverage) AS t_aroma,(SELECT SUM(r.serving)/ COUNT(r.id_member) FROM beverage_rating r WHERE r.id_beverage=f.id_beverage) AS t_serving,(SELECT (SUM(r.cleanlines) + SUM(r.flavor) + SUM(r.freshness) + SUM(r.cooking) + SUM(r.aroma) + SUM(r.serving)) / COUNT(r.id_member) FROM beverage_rating r WHERE r.id_beverage =f.id_beverage) AS total_rate FROM beverage f LEFT JOIN beverage_category k ON f.id_beverage_category=k.id_beverage_category LEFT JOIN price_index p ON f.id_price_index=p.id_price_index LEFT JOIN restaurant r ON f.id_restaurant=r.id_restaurant LEFT JOIN type_of_business a ON r.id_type_of_business = a.id_type_of_business LEFT JOIN negara b ON r.id_negara = b.id_negara LEFT JOIN propinsi d ON r.id_propinsi = d.id_propinsi LEFT JOIN kota e ON r.id_kota = e.id_kota LEFT JOIN landmark g ON r.id_landmark = g.id_landmark LEFT JOIN member n ON f.id_member = n.id_member where f.id_beverage='$_POST[id]'");
$r=mysqli_fetch_array($resto);
$total_rating=number_format((float)$r['total_rate'], 2, '.', '');
  $pr=mysqli_fetch_array(mysqli_query($koneksi, "SELECT *
    FROM (
      SELECT @curRank := @curRank + 1 AS rank
           , b.id_beverage
           , b.nama_beverage
           , b.jumlah
           , b.rank_vote
           , b.rank_like
      FROM beverage b
      CROSS JOIN (SELECT @curRank := 0) vars
      ORDER BY jumlah desc, rank_vote DESC, rank_like DESC, nama_beverage ASC
    ) p WHERE p.id_beverage=$id"));
  $rank_global=$pr['rank'];
 ?>
 <?php
   $pr=mysqli_fetch_array(mysqli_query($koneksi, "SELECT *
      FROM (
        SELECT @curRank := @curRank + 1 AS rank
             , b.id_beverage
             , b.nama_beverage
             , b.jumlah
             , b.rank_vote
             , b.rank_like
        FROM beverage b
        CROSS JOIN (SELECT @curRank := 0) vars WHERE negara=$r[kode_negara]
        ORDER BY jumlah desc, rank_vote DESC, rank_like DESC, nama_beverage ASC
      ) p WHERE p.id_beverage=$id"));
    $rank_negara=$pr['rank'];
  ?>
  <?php
    $pr=mysqli_fetch_array(mysqli_query($koneksi, "SELECT *
      FROM (
        SELECT @curRank := @curRank + 1 AS rank
             , b.id_beverage
             , b.nama_beverage
             , b.jumlah
             , b.rank_vote
             , b.rank_like
        FROM beverage b
        CROSS JOIN (SELECT @curRank := 0) vars WHERE propinsi=$r[id_propinsi]
        ORDER BY jumlah desc, rank_vote DESC, rank_like DESC, nama_beverage ASC
      ) p WHERE p.id_beverage=$id"));
    $rank_state=$pr['rank'];
  ?>
  <?php
    $pr=mysqli_fetch_array(mysqli_query($koneksi, "SELECT *
      FROM (
        SELECT @curRank := @curRank + 1 AS rank
             , b.id_beverage
             , b.nama_beverage
             , b.jumlah
             , b.rank_vote
             , b.rank_like
        FROM beverage b
        CROSS JOIN (SELECT @curRank := 0) vars WHERE kota=$r[id_kota]
        ORDER BY jumlah desc, rank_vote DESC, rank_like DESC, nama_beverage ASC
      ) p WHERE p.id_beverage=$id"));
    $rank_city=$pr['rank'];
  ?>
  <?php
    if ($r['id_mall']<>0) {
      $pr=mysqli_fetch_array(mysqli_query($koneksi, "SELECT *
          FROM (
            SELECT @curRank := @curRank + 1 AS rank
                 , b.id_beverage
                 , b.nama_beverage
                 , b.jumlah
                 , b.rank_vote
                 , b.rank_like
            FROM beverage b
            CROSS JOIN (SELECT @curRank := 0) vars WHERE mall=$r[id_mall]
            ORDER BY jumlah desc, rank_vote DESC, rank_like DESC, nama_beverage ASC
          ) p WHERE p.id_beverage=$id"));
        $rank_mall=$pr['rank'];
    }
    else {
      $rank_mall="N.A.";
    }
?>
<?php
$da=date("Y");
if ($da==$_POST['tahun']) {
?>
  <div class="piagam-isi-food ta2017">
    <div class="fitin">
      <div class="h1a"><?php echo "$r[nama_beverage]"; ?></div>
    </div>
    <div class="of">Of</div>
    <div class="restos">
      <div class="h1"><?php echo "$r[restaurant_name]"; ?></div>
    </div>
    <div class="restos_al">
      <div class="alamat"><?php echo "$r[street_address]"; ?></div>
    </div>
    <div class="negara"><?php echo $r['nama_negara']; ?></div>
    <div class="bintang"><?php echo "<input type='hidden' class='ratingys' data-filled='fa fa-star' data-empty='fa fa-star-o' readonly='readonly' value='$total_rating'>"; ?></div>
    <div class="rating"><?php echo "$total_rating / <em class='f-12'> $r[jumlah_vote] votes</em>"; ?></div>
    <span class="world"># <span style="  color: #ed1c24;"><?php echo "$rank_global"; ?></span></span>
    <span class="ranknegara"># <span style="  color: #ed1c24;"><?php echo "$rank_negara"; ?></span></span>
    <span class="rankstate"># <span style="  color: #ed1c24;"><?php echo "$rank_state"; ?></span></span>
    <span class="rankcity"># <span style="  color: #ed1c24;"><?php echo "$rank_city"; ?></span></span>
    <span class="rankmall"># <span style="  color: #ed1c24;"><?php echo "$rank_mall"; ?></span></span>
    <span class="tahun"><span><?php echo date("Y"); ?></span></span>
    <span class="tahuna"><span><?php echo date("Y"); ?></span></span>
  </div>
  <img src="<?php echo"$base_url";?>/assets/img/theme/certificate-food.jpg" alt="" width="100%">

<?php } else{ ?>

<div class="piagam-isi-food ta2016">
  <?php
    $ra=mysqli_query($koneksi,"select * from rank_beverage WHERE YEAR(Dates) = $_POST[tahun] and id_beverage=$_POST[id]");
    $ru=mysqli_fetch_array($ra);
   ?>
  <div class="fitin">
    <div class="h1a"><?php echo "$r[nama_beverage]"; ?></div>
  </div>
  <div class="of">Of</div>
  <div class="restos">
    <div class="h1"><?php echo "$r[restaurant_name]"; ?></div>
  </div>
  <div class="restos_al">
    <div class="alamat"><?php echo "$r[street_address]"; ?></div>
  </div>
  <div class="negara"><?php echo $r['nama_negara']; ?></div>
  <div class="bintang"><?php echo "<input type='hidden' class='ratingys' data-filled='fa fa-star' data-empty='fa fa-star-o' readonly='readonly' value='$ru[jumlah]'>"; ?></div>
  <div class="rating"><?php echo "$ru[jumlah] / <em class='f-12'> $r[jumlah_vote] votes</em>"; ?></div>
  <span class="world"># <span style="  color: #ed1c24;"><?php echo "$ru[global]"; ?></span></span>
  <span class="ranknegara"># <span style="  color: #ed1c24;"><?php echo "$ru[negara]"; ?></span></span>
  <span class="rankstate"># <span style="  color: #ed1c24;"><?php echo "$ru[propinsi]"; ?></span></span>
  <span class="rankcity"># <span style="  color: #ed1c24;"><?php echo "$ru[kota]"; ?></span></span>
  <span class="rankmall"># <span style="  color: #ed1c24;"><?php echo "$ru[mall]"; ?></span></span>
  <span class="tahun"><span><?php echo $_POST['tahun']; ?></span></span>
  <span class="tahuna"><span><?php echo $_POST['tahun']; ?></span></span>
</div>
<img src="<?php echo"$base_url";?>/assets/img/theme/certificate-food.jpg" alt="" width="100%">
<?php } ?>
