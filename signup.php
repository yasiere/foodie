<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
$active = "signup";
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
    <title>Sign Up - Foodie Guidances</title>
	<meta name="keywords" content="">
    <meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/select2.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/awesome-bootstrap-checkbox.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>
	<link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">
		<link rel="stylesheet" href="<?php echo"$base_url"; ?>/assets/css/example.wink.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
		<style media="screen">
			label.error{
				position: absolute;
			}
		</style>
</head>
 <body style="position: initial;" class="drawer drawer--right">
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>
    <div class="container hook">
		<div class="row">
			<div class="col-16">
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li class="active">Sign Up</li>
				</ol>
				<h3 class="f-merah mb10">Sign Up</h3>
				<p>*required field</p>
				<form class="border-form form-horizontal" method="post" action="<?php echo"$base_url"; ?>/config/func/process_signup.php" id="signup-daftar">
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Username <span class="f-merah">*</span></label></div>
						<div class="col-6  col-4b">
							<div class="">
								<label>foodieguidances.com/</label>
								<input type="text" class="form-control" placeholder="Your Username" name="username" autofocus id="username">
							</div>
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Email <span class="f-merah">*</span></label></div>
						<div class="col-6 col-4b">
							<input type="email" class="form-control" placeholder="Your Email" name="m_surat" autocomplete="off" id="email">
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Password <span class="f-merah">*</span></label></div>
						<div class="col-6 col-4b">
							<input type="password" id="password"  class="form-control" placeholder="Write Password" name="m_kode">
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Confirm Password <span class="f-merah">*</span></label></div>
						<div class="col-6 col-4b">
							<input type="password" class="form-control" id="password1" placeholder="Re-Write Password" name="m_kode2">
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Country <span class="f-merah">*</span></label></div>
						<div class="col-6 col-4b">
							<select name="negara" required class="form-control select" id="negara">
								<option value="">Select Country</option>
								<?php
								$sql=mysqli_query($koneksi,"select * from negara order by nama_negara asc");
								while($b=mysqli_fetch_array($sql)){
									echo"<option value='$b[id_negara]'>$b[nama_negara]</option>";
								}
								?>
							</select>
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Gender <span class="f-merah">*</span></label></div>
						<div class="col-6 col-4b">
							<div class="radio radio-danger radio-inline">
								<input type="radio" id="inlineRadio1" value="Male" name="gender" checked>
								<label for="inlineRadio1"> Male </label>
							</div>
							<div class="radio radio-inline">
								<input type="radio" id="inlineRadio2" value="Female" name="gender">
								<label for="inlineRadio2"> Female </label>
							</div>
						</div>
					</div>
					<div class="row mb20">
						<div class="col-16 f-12">
							<div class="checkbox checkbox-inline checkbox-danger">
								<input name="cuisineasasasas" value="African Cuisine" required id="cuisine18" type="checkbox">
								<label for="cuisine18" style="margin-top: -3px; margin-right: 3px;"> By clicking</label>
							</div>
							You agree to our
							<a href="" class="f-merah" data-toggle="modal" data-target="#term">Terms of Services</a> and that you have read our
							<a href="" class="f-merah" data-toggle="modal" data-target="#term">Privacy Policy</a>, including our Cookie Policy.
						</div>
					</div>
					<button type="submit" class="btn btn-danger as" style="display:none">Sign Up</button>
					<button type="reset" style="display:initial; background: rgb(243, 190, 192) none repeat scroll 0% 0%; border-color: rgb(0, 0, 0);" class="btn btn-danger ad" style="display:none">Sign Up</button> <button type="reset" class="btn btn-danger">Reset</button>
				</form>
			</div>
		</div>
    </div>
	<div class="modal fade" id="term">
	  <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Term of Services</h4>
			</div>
			<div class="modal-body">
				<h4 class="f-merah">Welcome to our website</h4>
				<p>Foodieguidances.com (collectively as “website”) is operated by Foodie Guidances Company (collectively as “we”, “our”, “us”), a partnership company registered in Kuching, Sarawak, Malaysia.  Within our website, we have various pages such as Restaurant, Food, Beverage, Recipe, FGMart, Coupons, Articles and Video where the context “page” is meant as a sub website within the main website.</p>
				<p>All foodies need to read these Terms and Conditions carefully, understand them and agree upon before joining us and accessing foodieguidances.com. Any participation, such as accessing and using of the website or our mobile version of the website, whether via desktop, laptop, Smart TV or mobile smart phone for browsing or viewing this website, or submitting your contents, you AGREE to be bound by these Terms and Conditions:</p>
				<ol>
					<li>Members <strong>AGREE</strong> to keep their log-in credentials safe and secure, and not to disclose them to any third party.</li>
					<li>Members <strong>WILL NOT</strong> accept any paid reviews and ratings on Restaurant, Food and Beverage within our website. We strictly condemn this unlawful act and we <strong>WILL NOT ACCEPT</strong> any kind of paid reviews and ratings on our website.</li>
					<li>Members <strong>AGREE</strong> to review and rate the restaurants, food, beverage, etc., in a way which is FAIR and <strong>PROFESSIONAL</strong>. We have several rating categories on Restaurant, food, and beverage. Members need to rate it fairly. For example, if the service in the restaurant was poor, but the food was excellent, members should not rate the food as poor just because you were not satisfied with their service.</li>
					<li>Members <strong>WILL GIVE</strong> an honest review and ratings without bias towards restaurant, food and beverage reviews and ratings on our website.</li>
					<li>Members <strong>AGREE</strong> not to copy, download, and print the contents of the website, whether it is for business or personal profit. Failure to comply will lead to infringement of our Copyright and legal proceeding will be considered by our management.</li>
					<li>Members <strong>WILL NOT</strong> upload indecent photos and non-related photos to our website.</li>
					<li>Members <strong>WILL NOT</strong> use bad language towards other members, editors, or when writing reviews and write-ups within our website. Those posts will be deleted, without any prior notice.</li>
					<li>Members <strong>AGREE</strong> not to sell their membership, as foodieguidances.com’s membership <strong>IS NOT FOR SALE</strong>.</li>
					<li>Members who failed to comply with our Terms of Use listed above will be <strong>SUSPENDED</strong> without prior notice.</li>
				</ol>
				<p>Members need to know that accessing the foodieguidances.com’s website and our social media sites (listed below) are totally different entities.</p>
				<p>a. Facebook’s Page (www.facebook.com/foodieguidances),<br>
				b. Facebook’s Groups (www.facebook.com/groups/foodieguidances),<br>
				c. Google + (www.google.com/)<br>
				d. Twitter (www.twitter.com/foodieguidances),<br>
				e. Instagram (www.instagram.com/foodieguidances_com),<br>
				f. Pinterest (www.pinterest.com/foodieguidances).</p>
				<p>Members need to separately agree upon their respective Terms of Use, Disclaimer, and Privacy Policy, whichever applied.</p>
				<p>Please, keep in mind that we reserve the rights to change this Terms of Use at any time without prior notice. If you AGREE with our Terms of Use, you are certainly more than WELCOME YOU TO JOIN US and hopefully you will enjoy the benefits of the service we provide.</p>
				<h4 class="f-merah">Termination</h4>
				<p>We reserve the rights to terminate the membership of a member for foodieguidances.com if he or she failed to comply with our Term of Use listed above.  Once the member decided to quit, or being suspended, we will delete the entire personal data and profile photo from our website at any time, without prior notice to its members. The only exception is the name will remain for those reviews posted previously.</p>
				<p><i>Members have to agree with Term of Use. If you do not agree with these terms and conditions, please do not use this website. If you continue to use the foodieguidances.com website, you agree to be bound by these Terms and Conditions.</i></p>
				<h4 class="f-merah">Contact</h4>
				<p>If you have any questions about Terms of Use, feel free to contact us at: cs@foodieguidances.com</p>
			</div>

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Privacy Policy</h4>
			</div>
			<div class="modal-body">
				<p>When signing up for a membership at foodieguidances.com, you will be asked to provide certain private and confidential information to us such as Name, Username, Password, Gender, and Email Address - these information must be filled up.  The other optional information required are: Personal Profile Photo, Nationality and Age.</p>
				<p>All information, such as personal information, and photos provided by our members belong to the Foodie Guidances Company. If a member decided to cancel its membership, all of the submitted information data will be retained as the data are belong to the Foodie Guidances Company, as stated in <u>Intellectual Property Rights above</u>. Personal information including Name, Username, Password, Gender, and Email Address will be deleted, with the exception of the name posted onprevious reviews.</p>
				<p>If and when applicable, Foodie Guidances Company’s staff, including authorized website owners, editors, programmers, designers, customer service, and outsource website design firms will be able to access necessary information data without location limitation. The personal data will be used within our entire website to enhance the user experience for our members and visitors on foodieguidances.com.</p>
				<p>As for the FGMart page on the website, we use the reliable and trustworthy payment gateway to handle our online transactions. Among them are PayPal, Google Wallets and Apple Pay. All the transactions will be performed via respective payment gateways, which usually will launch a new tab or browser.</p>
				<p>Members are advised to check on their internet security feature on the domain and make sure it does provide a verified certificate by a trusted and authorized Certificate Authority. All domain who has a verified certificate will have a green color “Lock”  or a green bar with the “Lock” in it. Since it is up most important to protect the Privacy of our members, foodieguidances.com had acquired the Encrypted and Authorization Certificate from DigiCert, Inc., U.S.A. under SSL Plus Certificate.</p>
				<p><i>We will only require the necessary information from you for our website. For your information, all the data and contents belongs to foodieguidances.com.  In addition, we do provide a secure web browsing experience by having a verified SSL Plus Certificate from Digicert, Inc., U.S.A.</i></p>
				<h4 class="f-merah">Cookies Policy</h4>
				<p>Cookies are small text files which are placed on your personal computer by the websites you visit. The websites which placed the cookies on your computer can read the small text files. That way, when you return, the website will recognize you. Cookies do not count as an invasion of privacy under the relevant laws.</p>
				<p>Foodieguidances.com uses cookies, so we can identify you when you visit our website, and your settings and preferences will be saved. You can block cookies anytime you want, by disabling them from the Internet options menu in your browser.</p>
				<p><i>Our website uses cookies but you can disable it in your browser setting.</i></p>
				<h4 class="f-merah">Contact</h4>
				<p>If you have any questions about Privacy Policy and Cookies Policy, feel free to contact us at: cs@foodieguidances.com.</p>
			</div>
			<div class="modal-footer"></div>
		</div>
	  </div>
	</div>
    <?php include"config/inc/footer.php"; ?>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/select2.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.validate.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/theme.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
   <script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
	 <script src="<?php echo"$base_url"; ?>/assets/js/hideShowPassword.min.js"></script>
	 <script>
	 $('#password').hidePassword(true);
	 $('#password1').hidePassword(true);
	 </script>
   <script>
      $(document).ready(function() {
	      $('.drawer').drawer();

			$('.tutup').click(function(){
				$('.iklanan').toggleClass('hilang', 500);
			});

			$('.cart').click(function(e){
				e.stopPropagation();
				$('.dis-cart').toggle();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$('.plus').click(function(e){
				e.stopPropagation();
				$('.dis-plus').toggle();
				$('.dis-cart').hide();
				$('.dis-share').hide();
			});
			$('.share').click(function(e){
				e.stopPropagation();
				$('.dis-share').toggle();
				$('.dis-plus').hide();
				$('.dis-cart').hide();
			});
			$('.drawer-toggle').click(function(){
				$('.dis-cart').hide();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$(document).click(function () {
				 var $el = $(".dis-share");
				 if ($el.is(":visible")) {
					  $el.fadeOut(200);
				 }
				 var $ela = $(".dis-plus");
				 if ($ela.is(":visible")) {
					  $ela.fadeOut(200);
				 }
				 var $elu = $(".dis-cart");
				 if ($elu.is(":visible")) {
					  $elu.fadeOut(200);
				 }
		   });

      });
   </script>
	<script type="text/javascript">
	$(document).ready(function () {
		$(".select").select2();
		$.validator.addMethod("usertheme",function(value,element){
         return this.optional(element) || /^[a-z][a-z0-9_-]{3,12}$/i.test(value);
      });
		// $.validator.addMethod("passkuat",function(value,element){
      //    return this.optional(element) || /^[a-z0-9_-]{7,20}$/i.test(value);
      // });
		$.validator.addMethod("passkuat",function(value,element){
         return this.optional(element) || /^(?=.*[A-Z])(?=.*[!@#$&%^*~?])(?=.*[0-9]).{7,20}$/i.test(value);
      });
		$.validator.addMethod("accept_char",function(value,element){
         return this.optional(element) || /^([a-zA-Z0-9,\./<>\?;':""[\]\\{}\|`~!@#\$%\^&\*()-_=\+]*)$/i.test(value);
      });

		$("#cuisine18").click(function(){
			$(".as").toggle();
			$(".ad").toggle();

		})

		$('#signup-daftar').validate({
			rules: {
				username: {
					required: true,
					minlength: 4,
					maxlength: 20,
					usertheme:true,
					remote: {
						url: "config/func/check-username.php",
						type: "post",
						data: {
						  username: function() {
							return $( "#username" ).val();
						  }
						}
					}
				},
				m_surat: {
					required: true,
					email: true,
					remote: {
						url: "config/func/check-email.php",
						type: "post",
						data: {
						  m_surat: function() {
							return $( "#email" ).val();
						  }
						}
					}
				},
				m_kode: {
					required: true,
					minlength: 8,
					maxlength: 20,
					passkuat:true,
					accept_char:true
				},
				m_kode2: {
					equalTo : "#password"
				},
				negara: {
					required: true
				}
			},
			messages: {
				username: {
					required: "Username cant be blank.",
					minlength: "Username must be between 4 - 20 characters",
					maxlength: "Username must be between 4 - 20 characters",
					usertheme: "Username first character must be letter (a-z) follow by letter, number, underscore or a hyphen",
					remote: "username is already in use"
				},
				m_surat: {
					required: "Email cant be blank",
					email: "Email not valid",
					remote: "Email is already registered"
				},
				m_kode: {
					required: "Password cant be blank.",
					minlength: "Fill in the password minimum have to be 8 characters including numbers and symbol. the symbol have to be desktop keyboard symbol. symbol like ♠ ♣ ♥ ♦ ♪ ■ £ ¥ Will not accepted",
					maxlength: "Password must be between 8 - 20 characters",
					passkuat: "Your Password is weak! at least one uppercase letter, one number and one special character",
					accept_char: "Only keyboard character allowed (!,@,#,$,%,^,&,*,~,?)"
				},
				m_kode2: {
					equalTo : "Your passwords do not match. Please try again"
				},
				negara: {
					required: "Country cant be blank."
				}
			}
		});
	});
	</script>
</body>
</html>
