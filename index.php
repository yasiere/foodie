<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";
include "config/func/id_masking.php";

$auto_logout=1800000;
if(!empty($_SESSION['food_member'])){
	if (time()-$_SESSION['timestamp']>$auto_logout){
		session_destroy();
		session_unset();
		header("Location: https://www.foodieguidances.com/auto-logout");
		exit();
	}else{
		$_SESSION['timestamp']=time();
	}
	include "config/func/member_data.php";
}

$active = "home";
$type="home";
date_default_timezone_set("Asia/Jakarta");
$batas = date("Y-m-d", strtotime(date( "Y-m-d", strtotime( date("Y-m-d"))) . "-1 month" ));
mysqli_query($koneksi,"delete from member WHERE tgl_berhenti <= '$batas' and berhenti=1");
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
    <title>Foodie Guidances</title>
	<meta name="keywords" content="">
    <meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/jquery-ui.css" rel="stylesheet">

	<link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">

	<link rel="prev" title="Page 2" href="<?php echo "$base_url/pages/all/search" ?>" />
	<link rel="next" title="Page 1" href="<?php echo  "$base_url/pages/restaurant" ?>" />

	<link rel="stylesheet" type="text/css" href="<?php echo"$base_url"; ?>/assets/css/spn.css" />

	<link href="<?php echo"$base_url"; ?>/assets/css/superslides.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<script defer="defer" src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script defer="defer" src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>
	  <script defer="defer" src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
   <script defer="defer">
   	$(document).ready(function () {
		  $("script").attr('defer', "defer");
	   });
   	</script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
 <body style="position: initial;" class="drawer drawer--right">
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>



	<div class="pencarian">
		<div class="pencari">
			<form method="get" action="<?php echo"$base_url/pages/all/search/"; ?>" id="ale-form" onsubmit="myFunction()">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Search based on Name, Category, Country, State, City..." id="search" name="keyword">
					<span class="input-group-btn">
						<button class="btn btn-danger" type="submit"><i class="fa fa-search"></i></button>
					</span>
				</div><!-- /input-group -->
			</form>
		</div>

		<div id="slides">
			<div class="slides-container">
				<?php
				$sql=mysqli_query($koneksi,"SELECT * FROM slideshow ORDER BY urutan desc");
				while($w=mysqli_fetch_array($sql)){
					echo"<img src='$base_url/assets/img/slideshow/$w[gambar]' alt='foodie guidances'>";
				}
				?>
			</div>
		</div>
		<?php include "config/inc/pencarian.php"; ?>
	</div>
    <div class="container no-padding">
		<div class="row no-padding">
			<div class="col-8 col-8a no-padding">
				<a href="<?php echo"$base_url"; ?>/pages/all/search">
					<h4 class="f-merah no-mb mt20 ml-10">Latest Foodie's Feeds</h4></a>
					<span class="f-abu f-12 blk mb20 ml-10 mb-0">Worldwide data collection</span>
				<div class="media mt-0">
					<?php
					$sql=mysqli_query($koneksi,"SELECT * FROM feed where id <> 0 ORDER BY id_feed DESC LIMIT 10");
					while($y=mysqli_fetch_array($sql)){
						if($y['jenis']=="Restaurant" || $y['jenis']=="restaurant"){
							$l=mysqli_fetch_array(mysqli_query($koneksi,"select *,(select p.gambar_restaurant_photo from restaurant_photo p where p.id_restaurant=r.id_restaurant order by p.id_restaurant_photo desc limit 1) as gambar, (select count(id_restaurant_here) as jumlah from restaurant_here where restaurant_here.id_restaurant=r.id_restaurant) as jumlah_disini, (SELECT COUNT(f.id_member) FROM restaurant_like f WHERE f.id_restaurant=r.id_restaurant) AS jumlah_like, (select count(id_restaurant_rating) as jumlah from restaurant_rating where restaurant_rating.id_restaurant=r.id_restaurant) as jumlah_vote, (select (SUM(cleanlines) + SUM(customer_services) + SUM(food_beverage) + SUM(comfort) + SUM(value_money)) / COUNT(id_member) AS total FROM restaurant_rating WHERE restaurant_rating.id_restaurant=r.id_restaurant) as total_vote from restaurant r, negara n, price_index p,type_of_business t where r.id_type_of_business = t.id_type_of_business and r.id_negara=n.id_negara and r.id_price_index=p.id_price_index and r.id_restaurant='$y[id]'"));
							$slug=seo($l['restaurant_name']);
							$post=date("jS M, Y", strtotime($l['tgl_post']));
							$total_rating=number_format((float)$l['total_vote'], 2, '.', '');
							$id = id_masking($l['id_restaurant']);
							$gambar=$l['gambar'];
							if(empty($l['gambar'])){
								$gambar="foodieguidances.jpg";
							}
							echo"<div class='row col-4a no-padding'>
								<div class='col-8 thumb col-8a no-padding'>
									<a href='$base_url/pages/restaurant/info/$id/$slug'><img data-original='$base_url/assets/img/restaurant/big_$gambar' class='lazy' width='220' height='165'></a>
									<a href='$base_url/pages/restaurant'><span>Restaurant</span></a>
								</div>
								<div class='col-8 info col-8a'>
									<em>posted on $post</em>
									<h3 class='sembunyi'><a href='$base_url/pages/restaurant/info/$id/$slug'>$l[restaurant_name]</a></h3>
									<ul class='list-unstyled f-12'>
										<li><em>category</em> <a href='$base_url/pages/restaurant/search/?business_type=$l[id_type_of_business]'>$l[nama_type_of_business]</a></li>
										<li class='sembunyi'><strong>$l[nama_negara]</strong>,  $l[street_address]</li>
										<li><em>cost</em> <a href='$base_url/pages/restaurant/search/?price_index=$l[id_price_index]'>$l[nama_price_index]</a></li>
										<li>$l[jumlah_like] Like <span class='bullet'>&#8226;</span> $l[jumlah_disini] Being Here</li>
									</ul>";
									if($total_rating<>0){
										echo"<div><input type='hidden' class='rating' data-filled='fa fa-star' data-empty='fa fa-star-o' readonly='readonly' value='$total_rating'>&nbsp;&nbsp;&nbsp;&nbsp;<strong class='f-18'>$total_rating</strong>&nbsp;&nbsp;<br><em>from $l[jumlah_vote] vote</em></div>";
									}
								echo"</div>
							</div>";
						}
						elseif($y['jenis']=="Food" || $y['jenis']=="food"){
							$f=mysqli_fetch_array(mysqli_query($koneksi,"select *,(select p.gambar_food_photo from food_photo p where p.id_food=f.id_food order by p.id_food_photo desc limit 1) as gambar,(SELECT COUNT(h.id_member) FROM food_like h WHERE h.id_food=f.id_food) AS dilike, (select count(h.id_food) from food_here h where h.id_food=f.id_food) as jumlah,(SELECT (SUM(r.cleanlines) + SUM(r.flavor) + SUM(r.freshness) + SUM(r.cooking) + SUM(r.aroma) + SUM(r.serving)) / COUNT(r.id_member) FROM food_rating r WHERE r.id_food =f.id_food) AS total,(select count(id_food_rating) as jumlah from food_rating where food_rating.id_food=f.id_food) as jumlah_vote from food f left join food_category k on f.id_food_category=k.id_food_category left join price_index p on f.id_price_index=p.id_price_index where f.id_food='$y[id]'"));
							$slug=seo($f['nama_food']);
							$post=date("jS M, Y", strtotime($f['tgl_posta']));
							$total_rating=number_format((float)$f['total'], 2, '.', '');
							$id = id_masking($f['id_food']);
							$cusa=str_replace('+', ', ', $f['id_cuisine']);
							echo"<div class='row col-4a no-padding'>
								<div class='col-8 thumb col-8a no-padding'>
									<a href='$base_url/pages/food/info/$id/$slug'><img data-original='$base_url/assets/img/food/big_$f[gambar]' class='lazy' width='220' height='165'></a>
									<a href='$base_url/pages/food'><span>Food</span></a>
								</div>
								<div class='col-8 info col-8a'>
									<em>posted on $post</em>
									<h3 class='sembunyi'><a href='$base_url/pages/food/info/$id/$slug'>$f[nama_food]</a></h3>
									<ul class='list-unstyled f-12 media'>
										<li><em>category</em> <a href='$base_url/pages/food/search/?category=$f[id_food_category]'>$f[nama_food_category]</a></li>
										<li class='sembunyi'><em>cuisine</em> <a href='$base_url/pages/food/search/?cuisine=$f[id_cuisine]'>$cusa</a></li>
										<li><em>cost</em> <a href='$base_url/pages/food/search/?price_index=$f[id_price_index]'>$f[nama_price_index]</a></li>
										<li>$f[dilike] Like <span class='bullet'>&#8226;</span> $f[jumlah] Taste it</li>
									</ul>";
									if($total_rating<>0){
										echo"<div><input type='hidden' class='rating' data-filled='fa fa-star' data-empty='fa fa-star-o' readonly='readonly' value='$total_rating'>&nbsp;&nbsp;&nbsp;&nbsp;<strong class='f-18'>$total_rating</strong>&nbsp;&nbsp;<br><em>from $f[jumlah_vote] vote</em></div>";
									}
								echo"</div>
							</div>";
						}
						elseif($y['jenis']=="Beverage" || $y['jenis']=="beverage"){
							$b=mysqli_fetch_array(mysqli_query($koneksi,"select *,(select p.gambar_beverage_photo from beverage_photo p where p.id_beverage=f.id_beverage order by p.id_beverage_photo desc limit 1) as gambar,(SELECT COUNT(h.id_member) FROM beverage_like h WHERE h.id_beverage=f.id_beverage) AS dilike, (select count(h.id_beverage) from beverage_here h where h.id_beverage=f.id_beverage) as jumlah,(select count(id_beverage_rating) as jumlah from beverage_rating where beverage_rating.id_beverage=f.id_beverage) as jumlah_vote,(SELECT (SUM(r.cleanlines) + SUM(r.flavor) + SUM(r.freshness) + SUM(r.cooking) + SUM(r.aroma) + SUM(r.serving)) / COUNT(r.id_member) FROM beverage_rating r WHERE r.id_beverage =f.id_beverage) AS total from beverage f,beverage_category k, price_index p where f.id_beverage_category=k.id_beverage_category and f.id_price_index=p.id_price_index and f.id_beverage='$y[id]'"));
							$slug=seo($b['nama_beverage']);
							$post=date("jS M, Y", strtotime($b['tgl_post']));
							$total_rating=number_format((float)$b['total'], 2, '.', '');
							$id = id_masking($b['id_beverage']);
							echo"<div class='row col-4a no-padding'>
								<div class='col-8 thumb col-8a no-padding'>
									<a href='$base_url/pages/beverage/info/$id/$slug'><img data-original='$base_url/assets/img/beverage/big_$b[gambar]' class='lazy' width='220' height='165'></a>
									<a href='$base_url/pages/beverage'><span>Beverage</span></a>
								</div>
								<div class='col-8 info col-8a'>
									<em>posted on $post</em>
									<h3 class='sembunyi'><a href='$base_url/pages/beverage/info/$id/$slug'>$b[nama_beverage]</a></h3>
									<ul class='list-unstyled f-12'>
										<li><em>category</em> <a href='$base_url/pages/beverage/search/?category=$b[id_beverage_category]'>$b[nama_beverage_category]</a></li>
										<li><em>making Methode</em> <a href='$base_url/pages/beverage/search/?making_methode=$b[making_methode]'>$b[making_methode]</a></li>
										<li><em>cost</em> <a href='$base_url/pages/beverage/search/?price_index=$b[id_price_index]'>$b[nama_price_index]</a></li>
										<li>$b[dilike] Like <span class='bullet'>&#8226;</span> $b[jumlah] Taste it</li>
									</ul>";
									if($total_rating<>0){
										echo"<div><input type='hidden' class='rating' data-filled='fa fa-star' data-empty='fa fa-star-o' readonly='readonly' value='$total_rating'>&nbsp;&nbsp;&nbsp;&nbsp;<strong class='f-18'>$total_rating</strong>&nbsp;&nbsp;<br><em>from $b[jumlah_vote] vote</em></div>";
									}
								echo"</div>
							</div>";
						}
						elseif($y['jenis']=="Recipe" || $y['jenis']=="recipe"){
							$v=mysqli_fetch_array(mysqli_query($koneksi,"select *,(select p.gambar_recipe_photo from recipe_photo p where p.id_recipe=f.id_recipe order by p.id_recipe_photo desc limit 1) as gambar,(SELECT COUNT(h.id_member) FROM recipe_like h WHERE h.id_recipe=f.id_recipe) AS dilike, (select count(h.id_recipe) from recipe_here h where h.id_recipe=f.id_recipe) as jumlah from recipe f,recipe_category k, cuisine c, difficulty d, duration u where f.id_recipe_category=k.id_recipe_category and f.id_cuisine=c.id_cuisine and f.id_difficulty=d.id_difficulty and f.id_duration=u.id_duration and f.id_recipe='$y[id]'"));
							$slug=seo($v['nama_recipe']);
							$post=date("jS M, Y", strtotime($v['tgl_post']));
							$id = id_masking($v['id_recipe']);
							if($v['pork_serving']=="Halal"){
								$ikon="icon_halal.png";
							}
							elseif($v['pork_serving']=="Yes"){
								$ikon="icon_pork.png";
							}
							elseif($v['pork_serving']=="No"){
								$ikon="icon_nonhalal.png";
							}
							else{
								$ikon="icon_vegetarian.png";
							}
							echo"<div class='row col-4a no-padding'>
								<div class='col-8 thumb col-8a no-padding'>
									<a href='$base_url/pages/recipe/info/$id/$slug'><img data-original='$base_url/assets/img/recipe/big_$v[gambar]' class='lazy' width='220' height='165'></a>
									<a href='$base_url/pages/recipe'><span>Recipe</span></a>
								</div>
								<div class='col-8 info col-8a'>
									<em>posted on $post</em>
									<h3 class='sembunyi'><a href='$base_url/pages/recipe/info/$id/$slug'>$v[nama_recipe]</a></h3>
									<ul class='list-unstyled f-12'>
										<li><em>category</em> <a href='$base_url/pages/recipe/search/?category=$v[id_recipe_category]'>$v[nama_recipe_category]</a></li>
										<li><em>cuisine</em> <a href='$base_url/pages/recipe/search/?cuisine=$v[id_cuisine]'>$v[nama_cuisine]</a></li>
										<li><em>dish</em> <a href='$base_url/pages/recipe/search/?dish=$v[dish]'>$v[dish]</a></li>
										<li><i class='fa fa-signal f-16'></i> $v[nama_difficulty] <span class='bullet'>&#8226;</span> <i class='fa fa-clock-o f-16'></i> $v[nama_duration]</li>
										<li>$v[dilike] Like <span class='bullet'>&#8226;</span> $v[jumlah] Trying</li>
									</ul>
									<img src='$base_url/assets/img/theme/$ikon' width='30' class='db mt5'>
								</div>
							</div>";
						}
						elseif($y['jenis']=="FGMART"){
							$z=mysqli_fetch_array(mysqli_query($koneksi,"select *,(SELECT COUNT(h.id_member) FROM fgmart_like h WHERE h.id_fgmart=f.id_fgmart) AS dilike from fgmart f where f.id_fgmart='$y[id]'"));
							$slug=seo($z['nama_fgmart']);
							$post=date("jS M, Y", strtotime($z['tgl_post']));
							$id = id_masking($z['id_fgmart']);
							echo"<div class='row col-4a no-padding'>
								<div class='col-8 thumb col-8a no-padding'>
									<a href='$base_url/pages/fgmart/$id/$slug'><img data-original='$base_url/assets/img/fgmart/big_$z[gambar_fgmart]' class='lazy' width='220' height='165'></a>
									<a href='$base_url/pages/fgmart'><span>FGMART</span></a>
								</div>
								<div class='col-8 info col-8a'>
									<em>posted on $post</em>
									<h3 class='sembunyi'><a href='$base_url/pages/fgmart/$id/$slug'>$z[nama_fgmart]</a></h3>
									<ul class='list-unstyled f-12'>
										<li>$z[dilike] Like <span class='bullet'>&#8226;</span> $z[dilihat] View</li>
									</ul>
								</div>
							</div>";
						}
						elseif($y['jenis']=="Coupon" || $y['jenis']=="coupon"){
							$k=mysqli_fetch_array(mysqli_query($koneksi,"select *,(SELECT COUNT(h.id_member) FROM coupon_like h WHERE h.id_coupon=f.id_coupon) AS dilike from coupon f left join negara n on f.id_negara=n.id_negara left join propinsi p on f.id_propinsi=p.id_propinsi left join kota k on f.id_kota=k.id_kota where f.id_coupon='$y[id]'"));
							$slug=seo($k['nama_coupon']);
							$post=date("jS M, Y", strtotime($k['tgl_post']));
							$konten_berita = htmlentities(strip_tags($k['konten_coupon']));
							$isi_berita = substr($konten_berita,0,165);
							$isi_berita = substr($konten_berita,0,strrpos($isi_berita," "));
							$isi_berita = html_entity_decode($isi_berita);
							$id = id_masking($k['id_coupon']);
							if(isset($_SESSION['food_member'])){
								$url="pages/coupon/".$id."/".$slug;
							}
							else{
								$url="login-area";
							}
							echo"<div class='row col-4a no-padding'>
								<div class='col-8 thumb col-8a no-padding'>
									<a href='$base_url/$url'><img data-original='$base_url/assets/img/coupon/big_$k[gambar_coupon]' class='lazy' width='220' height='165'></a>
									<a href='$base_url/pages/coupon'><span>Coupon</span></a>
								</div>
								<div class='col-8 info col-8a'>
									<em>posted on $post</em>
									<h3><a href='$base_url/$url'>$k[nama_coupon]</a></h3>
									<ul class='list-unstyled f-12'>
										<li>$isi_berita...</li>
										<li class='sembunyi'><strong>$k[nama_negara]</strong>, $k[nama_propinsi], $k[nama_kota]</li>
										<li>$k[dilike] Like <span class='bullet'>&#8226;</span> $k[dilihat] View</li>
									</ul>
								</div>
							</div>";
						}
						elseif($y['jenis']=="Article" || $y['jenis']=="article"){
							$j=mysqli_fetch_array(mysqli_query($koneksi,"select *,(select p.gambar_article_photo from article_photo p where p.id_article=f.id_article order by p.id_article desc limit 1) as gambar,(SELECT COUNT(h.id_member) FROM article_like h WHERE h.id_article=f.id_article) AS dilike from article f where f.id_article='$y[id]'"));
							$slug=seo($j['nama_article']);
							$post=date("jS M, Y", strtotime($j['tgl_post']));
							$konten_berita = htmlentities(strip_tags($j['konten_article']));
							$isi_berita = substr($konten_berita,0,150);
							$isi_berita = substr($konten_berita,0,strrpos($isi_berita," "));
							$isi_berita = html_entity_decode($isi_berita);
							$id = id_masking($j['id_article']);
							echo"<div class='row col-4a no-padding'>
								<div class='col-8 thumb col-8a no-padding'>
									<a href='$base_url/pages/article/$id/$slug'><img data-original='$base_url/assets/img/article/big_$j[gambar]' class='lazy' width='220' height='165'></a>
									<a href='$base_url/pages/article'><span>Article</span></a>
								</div>
								<div class='col-8 info col-8a'>
									<em>posted on $post</em>
									<h3><a href='$base_url/pages/article/$id/$slug'>$j[nama_article]</a></h3>
									<ul class='list-unstyled f-12'>
										<li>$isi_berita...</li>
										<li>$j[dilike] Like <span class='bullet'>&#8226;</span> $j[dilihat] View</li>
									</ul>
								</div>
							</div>";
						}
						elseif($y['jenis']=="Video" || $y['jenis']=="video"){
							$g=mysqli_fetch_array(mysqli_query($koneksi,"select *,(SELECT COUNT(h.id_member) FROM video_like h WHERE h.id_video=f.id_video) AS dilike from video f left join video_category v on f.id_video_category=v.id_video_category where id_video='$y[id]'"));
							$url = $g['url'];
							preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
							$id_video = $matches[1];
							$post=date("jS M, Y", strtotime($g['tgl_post']));
							$slug=seo($g['nama_video']);
							$id = id_masking($g['id_video']);
							echo"<div class='row col-4a no-padding'>
								<div class='col-8 thumb col-8a no-padding'>
									<a href='$base_url/pages/video/$id/$slug'>
									<div style='background: #000;'>

										<img data-original='https://img.youtube.com/vi/$id_video/hqdefault.jpg' class='lazy' width='220' height='165'></a>
									</div>
									<a href='$base_url/pages/video'><span>Video</span></a>
								</div>
								<div class='col-8 info col-8a'>
									<em>posted on $post</em>
									<h3 class='sembunyi'><a href='$base_url/pages/video/$id/$slug'>$g[nama_video]</a></h3>
									<ul class='list-unstyled f-12'>
										<li><em>category</em> <a href='$base_url/pages/video/search/?category=$g[id_video_category]'>$g[nama_video_category]</a></li>
										<li>$g[dilike] Like <span class='bullet'>&#8226;</span> $g[dilihat] View</li>
									</ul>
									<br><br>
								</div>
							</div>";
						}
						elseif($y['jenis']=="News"){
							$n=mysqli_fetch_array(mysqli_query($koneksi,"select *,(SELECT COUNT(h.id_member) FROM news_like h WHERE h.id_news=f.id_news) AS dilike from news f where f.id_news='$y[id]'"));
							$slug=seo($n['nama_news']);
							$post=date("jS M, Y", strtotime($n['tgl_post']));
							$konten_news = htmlentities(strip_tags($n['konten_news']));
							$isi_news = substr($konten_news,0,165);
							$isi_news = substr($konten_news,0,strrpos($isi_news," "));
							$isi_news = html_entity_decode($isi_news);
							$id = id_masking($n['id_news']);
							echo"<div class='row col-4a no-padding'>
								<div class='col-8 thumb col-8a no-padding'>
									<a href='$base_url/pages/news/$id/$slug'><img data-original='$base_url/assets/img/news/big_$n[gambar_news]' class='lazy' width='220' height='165'></a>
									<a href='$base_url/pages/news'><span>News</span></a>
								</div>
								<div class='col-8 info col-8a'>
									<em>posted on $post</em>
									<h3><a href='$base_url/pages/news/$id/$slug'>$n[nama_news]</a></h3>
									<ul class='list-unstyled f-12'>
										<li>$isi_news...</li>
										<li>$n[dilike] Like <span class='bullet'>&#8226;</span> $n[dilihat] View</li>
									</ul>
								</div>
							</div>";
						}
						elseif($y['jenis']=="Process"){
							$b=mysqli_fetch_array(mysqli_query($koneksi,"select *,(SELECT (SUM(r.expired) + SUM(r.taste) + SUM(r.serving) + SUM(r.freshness)) / COUNT(r.id_member) FROM process_rating r WHERE r.id_process =f.id_process) AS total, (SELECT COUNT(h.id_member) FROM process_like h WHERE h.id_process=f.id_process) AS dilike 
							from process f 
							LEFT JOIN process_photo p on f.id_process=p.id_process 
							LEFT JOIN process_category c on f.categories=c.id_process_category
							LEFT JOIN price_index i on f.id_price_index=i.id_price_index
							where f.id_process='$y[id]'"));
							$slug=seo($b['nama_process']);
							$post=date("jS M, Y", strtotime($b['date_of_viewed']));
							$total_rating=number_format((float)$b['total'], 2, '.', '');
							$id = id_masking($b['id_process']);
							echo"<div class='row col-4a'>
								<div class='col-8 thumb col-8a no-padding'>
									<a href='$base_url/pages/process/info/$id/$slug'>
										<img data-original='$base_url/assets/img/process/big_$b[gambar_process_photo]' class='lazy' width='220' height='165'>
									</a>
									<a href='$base_url/pages/process'><span>Process F&B</span></a>
								</div>
								<div class='col-8 info col-8a'>
									<em>posted on $post</em>
									<h3 class='sembunyi'><a href='$base_url/pages/process/info/$id/$slug'>$b[nama_process]</a></h3>
									<ul class='list-unstyled f-12'>
										<li><em>category</em> <a href='$base_url/pages/process/search/?category=$b[id_process_category]'>$b[nama_process_category]</a></li>
										<li><em>cost</em> <a href='$base_url/pages/process/search/?price_index=$b[id_price_index]'>$b[nama_price_index]</a></li>
										<li>$b[dilike] Like <span class='bullet'>&#8226;</span> $b[dilihat] Views</li>
									</ul>";
									if($total_rating<>0){
										echo"<div><input type='hidden' class='rating' data-filled='fa fa-star' data-empty='fa fa-star-o' readonly='readonly' value='$total_rating'>&nbsp;&nbsp;&nbsp;&nbsp;<strong class='f-18'>$total_rating</strong>&nbsp;&nbsp;<em>from $b[jumlah_vote] vote</em></div>";
									}
								echo"</div>
							</div>";
						}
					}
					?>

					<?php
					$sql=mysqli_query($koneksi,"SELECT * FROM feed where coupon ='1'");
					while($y=mysqli_fetch_array($sql)){
						if($y['jenis']=="Restaurant" || $y['jenis']=="restaurant"){
							$l=mysqli_fetch_array(mysqli_query($koneksi,"select *,(select p.gambar_restaurant_photo from restaurant_photo p where p.id_restaurant=r.id_restaurant order by p.id_restaurant_photo desc limit 1) as gambar, (select count(id_restaurant_here) as jumlah from restaurant_here where restaurant_here.id_restaurant=r.id_restaurant) as jumlah_disini, (SELECT COUNT(f.id_member) FROM restaurant_like f WHERE f.id_restaurant=r.id_restaurant) AS jumlah_like, (select count(id_restaurant_rating) as jumlah from restaurant_rating where restaurant_rating.id_restaurant=r.id_restaurant) as jumlah_vote, (select (SUM(cleanlines) + SUM(customer_services) + SUM(food_beverage) + SUM(comfort) + SUM(value_money)) / COUNT(id_member) AS total FROM restaurant_rating WHERE restaurant_rating.id_restaurant=r.id_restaurant) as total_vote from restaurant r, negara n, price_index p,type_of_business t where r.id_type_of_business = t.id_type_of_business and r.id_negara=n.id_negara and r.id_price_index=p.id_price_index and r.id_restaurant='$y[id]'"));
							$slug=seo($l['restaurant_name']);
							$post=date("jS M, Y", strtotime($l['tgl_post']));
							$total_rating=number_format((float)$l['total_vote'], 2, '.', '');
							$id = id_masking($l['id_restaurant']);
							$gambar=$l['gambar'];
							if(empty($l['gambar'])){
								$gambar="foodieguidances.jpg";
							}
							echo"<div class='row col-4a no-padding'>
								<div class='col-8 thumb col-8a no-padding'>
									<a href='$base_url/pages/restaurant/info/$id/$slug'><img data-original='$base_url/assets/img/restaurant/big_$gambar' class='lazy' width='220' height='165'></a>
									<a href='$base_url/pages/restaurant'><span>Restaurant</span></a>
								</div>
								<div class='col-8 info col-8a'>
									<em>posted on $post</em>
									<h3 class='sembunyi'><a href='$base_url/pages/restaurant/info/$id/$slug'>$l[restaurant_name]</a></h3>
									<ul class='list-unstyled f-12'>
										<li><em>category</em> <a href='$base_url/pages/restaurant/search/?business_type=$l[id_type_of_business]'>$l[nama_type_of_business]</a></li>
										<li class='sembunyi'><strong>$l[nama_negara]</strong>,  $l[street_address]</li>
										<li><em>cost</em> <a href='$base_url/pages/restaurant/search/?price_index=$l[id_price_index]'>$l[nama_price_index]</a></li>
										<li>$l[jumlah_like] Like <span class='bullet'>&#8226;</span> $l[jumlah_disini] Being Here</li>
									</ul>";
									if($total_rating<>0){
										echo"<div><input type='hidden' class='rating' data-filled='fa fa-star' data-empty='fa fa-star-o' readonly='readonly' value='$total_rating'>&nbsp;&nbsp;&nbsp;&nbsp;<strong class='f-18'>$total_rating</strong>&nbsp;&nbsp;<br><em>from $l[jumlah_vote] vote</em></div>";
									}
								echo"</div>
							</div>";
						}
						elseif($y['jenis']=="Food" || $y['jenis']=="food"){
							$f=mysqli_fetch_array(mysqli_query($koneksi,"select *,(select p.gambar_food_photo from food_photo p where p.id_food=f.id_food order by p.id_food_photo desc limit 1) as gambar,(SELECT COUNT(h.id_member) FROM food_like h WHERE h.id_food=f.id_food) AS dilike, (select count(h.id_food) from food_here h where h.id_food=f.id_food) as jumlah,(SELECT (SUM(r.cleanlines) + SUM(r.flavor) + SUM(r.freshness) + SUM(r.cooking) + SUM(r.aroma) + SUM(r.serving)) / COUNT(r.id_member) FROM food_rating r WHERE r.id_food =f.id_food) AS total,(select count(id_food_rating) as jumlah from food_rating where food_rating.id_food=f.id_food) as jumlah_vote from food f left join food_category k on f.id_food_category=k.id_food_category left join price_index p on f.id_price_index=p.id_price_index where f.id_food='$y[id]'"));
							$slug=seo($f['nama_food']);
							$post=date("jS M, Y", strtotime($f['tgl_posta']));
							$total_rating=number_format((float)$f['total'], 2, '.', '');
							$id = id_masking($f['id_food']);
							$cusa=str_replace('+', ', ', $f['id_cuisine']);
							echo"<div class='row col-4a no-padding'>
								<div class='col-8 thumb col-8a no-padding'>
									<a href='$base_url/pages/food/info/$id/$slug'><img data-original='$base_url/assets/img/food/big_$f[gambar]' class='lazy' width='220' height='165'></a>
									<a href='$base_url/pages/food'><span>Food</span></a>
								</div>
								<div class='col-8 info col-8a'>
									<em>posted on $post</em>
									<h3 class='sembunyi'><a href='$base_url/pages/food/info/$id/$slug'>$f[nama_food]</a></h3>
									<ul class='list-unstyled f-12 media'>
										<li><em>category</em> <a href='$base_url/pages/food/search/?category=$f[id_food_category]'>$f[nama_food_category]</a></li>
										<li class='sembunyi'><em>cuisine</em> <a href='$base_url/pages/food/search/?cuisine=$f[id_cuisine]'>$cusa</a></li>
										<li><em>cost</em> <a href='$base_url/pages/food/search/?price_index=$f[id_price_index]'>$f[nama_price_index]</a></li>
										<li>$f[dilike] Like <span class='bullet'>&#8226;</span> $f[jumlah] Taste it</li>
									</ul>";
									if($total_rating<>0){
										echo"<div><input type='hidden' class='rating' data-filled='fa fa-star' data-empty='fa fa-star-o' readonly='readonly' value='$total_rating'>&nbsp;&nbsp;&nbsp;&nbsp;<strong class='f-18'>$total_rating</strong>&nbsp;&nbsp;<br><em>from $f[jumlah_vote] vote</em></div>";
									}
								echo"</div>
							</div>";
						}
						elseif($y['jenis']=="Beverage" || $y['jenis']=="beverage"){
							$b=mysqli_fetch_array(mysqli_query($koneksi,"select *,(select p.gambar_beverage_photo from beverage_photo p where p.id_beverage=f.id_beverage order by p.id_beverage_photo desc limit 1) as gambar,(SELECT COUNT(h.id_member) FROM beverage_like h WHERE h.id_beverage=f.id_beverage) AS dilike, (select count(h.id_beverage) from beverage_here h where h.id_beverage=f.id_beverage) as jumlah,(select count(id_beverage_rating) as jumlah from beverage_rating where beverage_rating.id_beverage=f.id_beverage) as jumlah_vote,(SELECT (SUM(r.cleanlines) + SUM(r.flavor) + SUM(r.freshness) + SUM(r.cooking) + SUM(r.aroma) + SUM(r.serving)) / COUNT(r.id_member) FROM beverage_rating r WHERE r.id_beverage =f.id_beverage) AS total from beverage f,beverage_category k, price_index p where f.id_beverage_category=k.id_beverage_category and f.id_price_index=p.id_price_index and f.id_beverage='$y[id]'"));
							$slug=seo($b['nama_beverage']);
							$post=date("jS M, Y", strtotime($b['tgl_post']));
							$total_rating=number_format((float)$b['total'], 2, '.', '');
							$id = id_masking($b['id_beverage']);
							echo"<div class='row col-4a no-padding'>
								<div class='col-8 thumb col-8a no-padding'>
									<a href='$base_url/pages/beverage/info/$id/$slug'><img data-original='$base_url/assets/img/beverage/big_$b[gambar]' class='lazy' width='220' height='165'></a>
									<a href='$base_url/pages/beverage'><span>Beverage</span></a>
								</div>
								<div class='col-8 info col-8a'>
									<em>posted on $post</em>
									<h3 class='sembunyi'><a href='$base_url/pages/beverage/info/$id/$slug'>$b[nama_beverage]</a></h3>
									<ul class='list-unstyled f-12'>
										<li><em>category</em> <a href='$base_url/pages/beverage/search/?category=$b[id_beverage_category]'>$b[nama_beverage_category]</a></li>
										<li><em>making Methode</em> <a href='$base_url/pages/beverage/search/?making_methode=$b[making_methode]'>$b[making_methode]</a></li>
										<li><em>cost</em> <a href='$base_url/pages/beverage/search/?price_index=$b[id_price_index]'>$b[nama_price_index]</a></li>
										<li>$b[dilike] Like <span class='bullet'>&#8226;</span> $b[jumlah] Taste it</li>
									</ul>";
									if($total_rating<>0){
										echo"<div><input type='hidden' class='rating' data-filled='fa fa-star' data-empty='fa fa-star-o' readonly='readonly' value='$total_rating'>&nbsp;&nbsp;&nbsp;&nbsp;<strong class='f-18'>$total_rating</strong>&nbsp;&nbsp;<br><em>from $b[jumlah_vote] vote</em></div>";
									}
								echo"</div>
							</div>";
						}
						elseif($y['jenis']=="Recipe" || $y['jenis']=="recipe"){
							$v=mysqli_fetch_array(mysqli_query($koneksi,"select *,(select p.gambar_recipe_photo from recipe_photo p where p.id_recipe=f.id_recipe order by p.id_recipe_photo desc limit 1) as gambar,(SELECT COUNT(h.id_member) FROM recipe_like h WHERE h.id_recipe=f.id_recipe) AS dilike, (select count(h.id_recipe) from recipe_here h where h.id_recipe=f.id_recipe) as jumlah from recipe f,recipe_category k, cuisine c, difficulty d, duration u where f.id_recipe_category=k.id_recipe_category and f.id_cuisine=c.id_cuisine and f.id_difficulty=d.id_difficulty and f.id_duration=u.id_duration and f.id_recipe='$y[id]'"));
							$slug=seo($v['nama_recipe']);
							$post=date("jS M, Y", strtotime($v['tgl_post']));
							$id = id_masking($v['id_recipe']);
							if($v['pork_serving']=="Halal"){
								$ikon="icon_halal.png";
							}
							elseif($v['pork_serving']=="Yes"){
								$ikon="icon_pork.png";
							}
							elseif($v['pork_serving']=="No"){
								$ikon="icon_nonhalal.png";
							}
							else{
								$ikon="icon_vegetarian.png";
							}
							echo"<div class='row col-4a no-padding'>
								<div class='col-8 thumb col-8a no-padding'>
									<a href='$base_url/pages/recipe/info/$id/$slug'><img data-original='$base_url/assets/img/recipe/big_$v[gambar]' class='lazy' width='220' height='165'></a>
									<a href='$base_url/pages/recipe'><span>Recipe</span></a>
								</div>
								<div class='col-8 info col-8a'>
									<em>posted on $post</em>
									<h3 class='sembunyi'><a href='$base_url/pages/recipe/info/$id/$slug'>$v[nama_recipe]</a></h3>
									<ul class='list-unstyled f-12'>
										<li><em>category</em> <a href='$base_url/pages/recipe/search/?category=$v[id_recipe_category]'>$v[nama_recipe_category]</a></li>
										<li><em>cuisine</em> <a href='$base_url/pages/recipe/search/?cuisine=$v[id_cuisine]'>$v[nama_cuisine]</a></li>
										<li><em>dish</em> <a href='$base_url/pages/recipe/search/?dish=$v[dish]'>$v[dish]</a></li>
										<li><i class='fa fa-signal f-16'></i> $v[nama_difficulty] <span class='bullet'>&#8226;</span> <i class='fa fa-clock-o f-16'></i> $v[nama_duration]</li>
										<li>$v[dilike] Like <span class='bullet'>&#8226;</span> $v[jumlah] Trying</li>
									</ul>
									<img src='$base_url/assets/img/theme/$ikon' width='30' class='db mt5'>
								</div>
							</div>";
						}
						elseif($y['jenis']=="FGMART"){
							$z=mysqli_fetch_array(mysqli_query($koneksi,"select *,(SELECT COUNT(h.id_member) FROM fgmart_like h WHERE h.id_fgmart=f.id_fgmart) AS dilike from fgmart f where f.id_fgmart='$y[id]'"));
							$slug=seo($z['nama_fgmart']);
							$post=date("jS M, Y", strtotime($z['tgl_post']));
							$id = id_masking($z['id_fgmart']);
							echo"<div class='row col-4a no-padding'>
								<div class='col-8 thumb col-8a no-padding'>
									<a href='$base_url/pages/fgmart/$id/$slug'><img data-original='$base_url/assets/img/fgmart/big_$z[gambar_fgmart]' class='lazy' width='220' height='165'></a>
									<a href='$base_url/pages/fgmart'><span>FGMART</span></a>
								</div>
								<div class='col-8 info col-8a'>
									<em>posted on $post</em>
									<h3 class='sembunyi'><a href='$base_url/pages/fgmart/$id/$slug'>$z[nama_fgmart]</a></h3>
									<ul class='list-unstyled f-12'>
										<li>$z[dilike] Like <span class='bullet'>&#8226;</span> $z[dilihat] View</li>
									</ul>
								</div>
							</div>";
						}
						elseif($y['jenis']=="Coupon" || $y['jenis']=="coupon"){
							$k=mysqli_fetch_array(mysqli_query($koneksi,"select *,(SELECT COUNT(h.id_member) FROM coupon_like h WHERE h.id_coupon=f.id_coupon) AS dilike from coupon f left join negara n on f.id_negara=n.id_negara left join propinsi p on f.id_propinsi=p.id_propinsi left join kota k on f.id_kota=k.id_kota where f.id_coupon='$y[id]'"));
							$slug=seo($k['nama_coupon']);
							$post=date("jS M, Y", strtotime($k['tgl_post']));
							$konten_berita = htmlentities(strip_tags($k['konten_coupon']));
							$isi_berita = substr($konten_berita,0,165);
							$isi_berita = substr($konten_berita,0,strrpos($isi_berita," "));
							$isi_berita = html_entity_decode($isi_berita);
							$id = id_masking($k['id_coupon']);
							if(isset($_SESSION['food_member'])){
								$url="pages/coupon/".$id."/".$slug;
							}
							else{
								$url="login-area";
							}
							echo"<div class='row col-4a no-padding'>
								<div class='col-8 thumb col-8a no-padding'>
									<a href='$base_url/$url'><img data-original='$base_url/assets/img/coupon/big_$k[gambar_coupon]' class='lazy' width='220' height='165'></a>
									<a href='$base_url/pages/coupon'><span>Coupon</span></a>
								</div>
								<div class='col-8 info col-8a'>
									<em>posted on $post</em>
									<h3><a href='$base_url/$url'>$k[nama_coupon]</a></h3>
									<ul class='list-unstyled f-12'>
										<li>$isi_berita...</li>
										<li class='sembunyi'><strong>$k[nama_negara]</strong>, $k[nama_propinsi], $k[nama_kota]</li>
										<li>$k[dilike] Like <span class='bullet'>&#8226;</span> $k[dilihat] View</li>
									</ul>
								</div>
							</div>";
						}
						elseif($y['jenis']=="Article" || $y['jenis']=="article"){
							$j=mysqli_fetch_array(mysqli_query($koneksi,"select *,(select p.gambar_article_photo from article_photo p where p.id_article=f.id_article order by p.id_article desc limit 1) as gambar,(SELECT COUNT(h.id_member) FROM article_like h WHERE h.id_article=f.id_article) AS dilike from article f where f.id_article='$y[id]'"));
							$slug=seo($j['nama_article']);
							$post=date("jS M, Y", strtotime($j['tgl_post']));
							$konten_berita = htmlentities(strip_tags($j['konten_article']));
							$isi_berita = substr($konten_berita,0,165);
							$isi_berita = substr($konten_berita,0,strrpos($isi_berita," "));
							$isi_berita = html_entity_decode($isi_berita);
							$id = id_masking($j['id_article']);
							echo"<div class='row col-4a no-padding'>
								<div class='col-8 thumb col-8a no-padding'>
									<a href='$base_url/pages/article/$id/$slug'><img data-original='$base_url/assets/img/article/big_$j[gambar]' class='lazy' width='220' height='165'></a>
									<a href='$base_url/pages/article'><span>Article</span></a>
								</div>
								<div class='col-8 info col-8a'>
									<em>posted on $post</em>
									<h3><a href='$base_url/pages/article/$id/$slug'>$j[nama_article]</a></h3>
									<ul class='list-unstyled f-12'>
										<li>$isi_berita...</li>
										<li>$j[dilike] Like <span class='bullet'>&#8226;</span> $j[dilihat] View</li>
									</ul>
								</div>
							</div>";
						}
						elseif($y['jenis']=="Video" || $y['jenis']=="video"){
							$g=mysqli_fetch_array(mysqli_query($koneksi,"select *,(SELECT COUNT(h.id_member) FROM video_like h WHERE h.id_video=f.id_video) AS dilike from video f left join video_category v on f.id_video_category=v.id_video_category where id_video='$y[id]'"));
							$url = $g['url'];
							preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
							$id_video = $matches[1];
							$post=date("jS M, Y", strtotime($g['tgl_post']));
							$slug=seo($g['nama_video']);
							$id = id_masking($g['id_video']);
							echo"<div class='row col-4a no-padding'>
								<div class='col-8 thumb col-8a no-padding'>
									<a href='$base_url/pages/video/$id/$slug'>
									<div style='background: #000;'>

										<img data-original='https://img.youtube.com/vi/$id_video/hqdefault.jpg' class='lazy' width='220' height='165'></a>
									</div>
									<a href='$base_url/pages/video'><span>Video</span></a>
								</div>
								<div class='col-8 info col-8a'>
									<em>posted on $post</em>
									<h3 class='sembunyi'><a href='$base_url/pages/video/$id/$slug'>$g[nama_video]</a></h3>
									<ul class='list-unstyled f-12'>
										<li><em>category</em> <a href='$base_url/pages/video/search/?category=$g[id_video_category]'>$g[nama_video_category]</a></li>
										<li>$g[dilike] Like <span class='bullet'>&#8226;</span> $g[dilihat] View</li>
									</ul>
									<br><br>
								</div>
							</div>";
						}
						elseif($y['jenis']=="News"){
							$n=mysqli_fetch_array(mysqli_query($koneksi,"select *,(SELECT COUNT(h.id_member) FROM news_like h WHERE h.id_news=f.id_news) AS dilike from news f where f.id_news='$y[id]'"));
							$slug=seo($n['nama_news']);
							$post=date("jS M, Y", strtotime($n['tgl_post']));
							$konten_news = htmlentities(strip_tags($n['konten_news']));
							$isi_news = substr($konten_news,0,165);
							$isi_news = substr($konten_news,0,strrpos($isi_news," "));
							$isi_news = html_entity_decode($isi_news);
							$id = id_masking($n['id_news']);
							echo"<div class='row col-4a no-padding'>
								<div class='col-8 thumb col-8a no-padding'>
									<a href='$base_url/pages/news/$id/$slug'><img data-original='$base_url/assets/img/news/big_$n[gambar_news]' class='lazy' width='220' height='165'></a>
									<a href='$base_url/pages/news'><span>News</span></a>
								</div>
								<div class='col-8 info col-8a'>
									<em>posted on $post</em>
									<h3><a href='$base_url/pages/news/$id/$slug'>$n[nama_news]</a></h3>
									<ul class='list-unstyled f-12'>
										<li>$isi_news...</li>
										<li>$n[dilike] Like <span class='bullet'>&#8226;</span> $n[dilihat] View</li>
									</ul>
								</div>
							</div>";
						}
					}
					?>
				</div>
			</div>
			<div class="menu-bawah">
				<a href="<?php echo"$base_url/pages/restaurant/search/?sort=total_vote+DESC"; ?>">World Top Restaurant</a>
				<a href="<?php echo"$base_url/pages/food/search/?sort=total+DESC"; ?>">World Top Food</a>
				<a href="<?php echo"$base_url/pages/beverage/search/?sort=total+DESC"; ?>">World Top Beverage</a>
			</div>
			<div class="col-4 mene-atas">
				<h4 class="f-merah no-mb mt20"><a href="<?php echo"$base_url/pages/restaurant/search/?sort=total_vote+DESC"; ?>">World Top Restaurant</a></h4>
				<span class="f-abu f-12 blk mb10">Worldwide data collection</span>
				<ol class="list-number media">
					<?php
					$top=mysqli_query($koneksi,"SELECT r.id_restaurant,r.id_kota,r.id_negara, r.restaurant_name, c.nama_propinsi, p.nama_kota, n.nama_negara,(SELECT (SUM(f.cleanlines) + SUM(f.customer_services) + SUM(f.food_beverage) + SUM(f.comfort) + SUM(f.value_money)) / COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant =r.id_restaurant) AS total FROM restaurant r LEFT JOIN negara n ON r.id_negara=n.id_negara LEFT JOIN propinsi c ON r.id_propinsi=c.id_propinsi LEFT JOIN kota p ON r.id_kota=p.id_kota ORDER BY total DESC, r.restaurant_name limit 10");
					while($t=mysqli_fetch_array($top)){
						if($t['total']<>0){
							$slug=seo($t['restaurant_name']);
							$id = id_masking($t['id_restaurant']);
							echo"<li class=''><div class='info'><h4 class='sembunyi'><a href='$base_url/pages/restaurant/info/$id/$slug'>$t[restaurant_name]</a></h4></div>
							<span class='f-12'>
							<a href='$base_url/pages/restaurant/search/?city=$t[id_kota]' class='f-merah'>$t[nama_kota]</a>,
							<a href='$base_url/pages/restaurant/search/?country=$t[id_negara]' class='f-merah'>$t[nama_negara]</a></span></li>";
						}
					}
					?>
				</ol>
				<h4 class="f-merah no-mb"><a href="<?php echo"$base_url/pages/food/search/?sort=total+DESC"; ?>">World Top Food</a></h4>
				<span class="f-abu f-12 blk mb10">Worldwide data collection</span>
				<ol class="list-number media">
					<?php
					$top=mysqli_query($koneksi,"SELECT  r.id_food, r.nama_food,j.id_negara,n.nama_negara, p.id_kota, p.nama_kota, c.id_food_category,c.nama_food_category,(SELECT (SUM(f.cleanlines) + SUM(f.flavor) + SUM(f.freshness) + SUM(f.cooking) + SUM(f.aroma) + SUM(f.serving)) / COUNT(f.id_member) FROM food_rating f WHERE f.id_food =r.id_food) AS total FROM food r LEFT JOIN food_category c ON r.id_food_category=c.id_food_category LEFT JOIN restaurant j ON r.id_restaurant=j.id_restaurant LEFT JOIN negara n ON j.id_negara=n.id_negara LEFT JOIN propinsi k ON j.id_propinsi=k.id_propinsi LEFT JOIN kota p ON j.id_kota=p.id_kota ORDER BY total DESC,r.nama_food limit 10");
					while($tf=mysqli_fetch_array($top)){
						if($tf['total']<>0){
							$slug=seo($tf['nama_food']);
							$id = id_masking($tf['id_food']);
							echo"<li><div class='info'><h4 class='sembunyi'><a href='$base_url/pages/food/info/$id/$slug'>$tf[nama_food]</a></h4></div>
							<a class='f-12 f-merah' href='$base_url/pages/food/search/?category=$tf[id_food_category]'>$tf[nama_food_category]</a>
							<a href='$base_url/pages/restaurant/search/?city=$tf[id_kota]' class=' f-12 f-merah'>/ $tf[nama_kota]</a>,
							<a href='$base_url/pages/restaurant/search/?country=$tf[id_negara]' class='f-12 f-merah'>$tf[nama_negara]</a></li>";
						}
					}
					?>
				</ol>
				<h4 class="f-merah no-mb"><a href="<?php echo"$base_url/pages/beverage/search/?sort=total+DESC"; ?>">World Top Beverage</a></h4>
				<span class="f-abu f-12 blk mb10">Worldwide data collection </span>
				<ol class="list-number media">
					<?php
					$top=mysqli_query($koneksi,"SELECT r.id_beverage,j.id_negara,n.nama_negara, p.id_kota, p.nama_kota, r.nama_beverage, c.id_beverage_category,c.nama_beverage_category, (SELECT (SUM(f.cleanlines) + SUM(f.flavor) + SUM(f.freshness) + SUM(f.cooking) + SUM(f.aroma) + SUM(f.serving)) / COUNT(f.id_member) FROM beverage_rating f WHERE f.id_beverage =r.id_beverage) AS total FROM beverage r LEFT JOIN beverage_category c ON r.id_beverage_category=c.id_beverage_category LEFT JOIN restaurant j ON r.id_restaurant=j.id_restaurant LEFT JOIN negara n ON j.id_negara=n.id_negara LEFT JOIN propinsi k ON j.id_propinsi=k.id_propinsi LEFT JOIN kota p ON j.id_kota=p.id_kota ORDER BY total DESC,r.nama_beverage limit 10");
					while($tb=mysqli_fetch_array($top)){
						if($tb['total']<>0){
							$slug=seo($tf['nama_beverage']);
							$id = id_masking($tb['id_beverage']);
							echo"<li><div class='info'><h4 class='sembunyi'><a href='$base_url/pages/beverage/info/$id/$slug'>$tb[nama_beverage]</a></h4></div>
								<a class='f-12 f-merah' href='$base_url/pages/beverage/search/?category=$tb[id_beverage_category]'>$tb[nama_beverage_category]</a><br>
								<a href='$base_url/pages/restaurant/search/?city=$tb[id_kota]' class=' f-12 f-merah'>$tb[nama_kota]</a>,
								<a href='$base_url/pages/restaurant/search/?country=$tb[id_negara]' class='f-12 f-merah'>$tb[nama_negara]</a></span></li>";
						}
					}
					?>
				</ol>
			</div>
			<div class="col-4 side-kanan">
				<?php include"config/inc/search.php"; ?>
				<?php include"config/inc/iklan.php"; ?>
				<div class="fb-like-box" data-href="https://www.facebook.com/foodieguidances#_=_" data-width="220" data-height="350" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
			</div>
		</div>
    </div>
    <?php include"config/inc/footer.php"; ?>

	<a class="scrollToNext" href="<?php echo "$base_url/pages/all/search" ?>" style="bottom: 320px;"><img src="<?php echo"$base_url/assets/img/theme/kiri.png" ?>"></a>
	<a class="scrollToNext" href="<?php echo  "$base_url/pages/restaurant" ?>" style="bottom: 273px;"><img src="<?php echo"$base_url/assets/img/theme/kanan.png" ?>"></a>
	<a href="#" class="scrollToTop"><img src="<?php echo"$base_url/assets/img/theme/atas.png" ?>"></a>

	
	<script defer="defer" src="<?php echo"$base_url"; ?>/assets/js/jquery-ui.min.js"></script>
	<script defer="defer" src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script defer="defer" src="<?php echo"$base_url"; ?>/assets/js/bootstrap-rating.min.js"></script>
	<script defer="defer" src="<?php echo"$base_url"; ?>/assets/js/jquery.lazyload.min.js"></script>
	<script defer="defer" src="<?php echo"$base_url"; ?>/assets/js/jquery.superslides.js"></script>

	<script defer="defer" type="text/javascript" src="<?php echo"$base_url"; ?>/assets/js/spn.js"></script>

   <script defer="defer" src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
   <script defer="defer" src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
   <script defer="defer">
   
      $(document).ready(function() {
	      $('.drawer').drawer({
			    iscroll: {
						scrollbars: true,
						mouseWheel: true,
						interactiveScrollbars: true,
						shrinkScrollbars: 'scale',
						fadeScrollbars: true,
						click: true
			    },
			    showOverlay: true
	      });

			$('.tutup').click(function(){
				$('.iklanan').toggleClass('hilang', 500);
			});

			$('.cart').click(function(e){
				e.stopPropagation();
				$('.dis-cart').toggle();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$('.plus').click(function(e){
				e.stopPropagation();
				$('.dis-plus').toggle();
				$('.dis-cart').hide();
				$('.dis-share').hide();
			});
			$('.share').click(function(e){
				e.stopPropagation();
				$('.dis-share').toggle();
				$('.dis-plus').hide();
				$('.dis-cart').hide();
			});
			$('.drawer-toggle').click(function(){
				$('.dis-cart').hide();
				$('.dis-plus').hide();
				$('.dis-share').hide();
			});
			$(document).click(function () {
				 var $el = $(".dis-share");
				 if ($el.is(":visible")) {
					  $el.fadeOut(200);
				 }
				 var $ela = $(".dis-plus");
				 if ($ela.is(":visible")) {
					  $ela.fadeOut(200);
				 }
				 var $elu = $(".dis-cart");
				 if ($elu.is(":visible")) {
					  $elu.fadeOut(200);
				 }
		   });
      });
   </script>

	<?php if(!empty($_SESSION['food_member'])){ ?>
		<script defer="defer" src="<?php echo"$base_url"; ?>/assets/js/theme.js"></script>
	<?php } ?>
	<script type="text/javascript">
		$(document).ready(function () {
			$(document).on("contextmenu",function(e){
				if(e.target.nodeName != "INPUT" && e.target.nodeName != "TEXTAREA")
				e.preventDefault();
			});
			$.fn.disableTextSelect = function() {
				return this.each(function() {
					$(this).css({
						'MozUserSelect':'none',
						'webkitUserSelect':'none'
					}).attr('unselectable','on').bind('selectstart', function() {
						return false;
					});
				});
			};
			$('body').disableTextSelect();
		});
	</script>

	<div id="fb-root"></div>
	<script>
		
	function myFunction(){
		var myForm = document.getElementById('ale-form');
		var allInputs = myForm.getElementsByTagName('select');
		var teks = myForm.getElementsByTagName('input');
		var input, i, grup, a;

		for(i = 0; input = allInputs[i]; i++) {
			if(input.getAttribute('name') && !input.value) {
				input.setAttribute('name', '');
			}
		}
		for(a = 0; grup = teks[a]; a++) {
			if(grup.getAttribute('name') && !grup.value) {
				grup.setAttribute('name', '');
			}
		}
	}
	$(document).ready(function () {
		$("img.lazy").lazyload({
			effect : "fadeIn"
		});
		$('#slides').superslides({
			animation: 'slide',
			inherit_height_from: '.pencarian',
			play: 8000,
			pagination: true
		});
		$('.rating').rating();
		$('#search').autocomplete({
			source: "<?php echo "$base_url/config/func/ajax_restaurant_search.php"; ?>",
			minLength: 3
		});
		$('.carousel').carousel({
			interval: 10000
		});

		$(window).scroll(function(){
			if ($(this).scrollTop() > 100) {
				$('.scrollToTop').fadeIn();
			} else {
				$('.scrollToTop').fadeOut();
			}
		});
		$(window).scroll(function(){
			if ($(this).scrollTop() > 50) {
				$('.scrollToNext').fadeIn();
			} else {
				$('.scrollToNext').fadeOut();
			}
		});

		//Click event to scroll to top
		$('.scrollToTop').click(function(){
			$('html, body').animate({scrollTop : 0},800);
			return false;
		});
	});
	/*=====For developer to config ====*/
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=348272391978609&version=v2.0";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script>
</body>
</html>
