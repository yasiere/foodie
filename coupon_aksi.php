<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";	
include "config/func/id_masking.php";
date_default_timezone_set("Asia/Jakarta");
$sekarang=date("Y-m-d");
$id = id_masking($_GET['id']);
$r=mysqli_fetch_array(mysqli_query($koneksi,"select id_coupon, nama_coupon from coupon where id_coupon='$id'"));
$seo=seo($r['nama_coupon']);
if(isset($_SESSION['food_member'])){
	if($_GET['fungsi']=="1"){
		$ada=mysqli_num_rows(mysqli_query($koneksi,"select id_coupon_like from coupon_like where id_coupon='$id' and id_member='$_SESSION[food_member]' and tgl_like='$sekarang'"));
		if($ada==0){
			mysqli_query($koneksi,"insert into coupon_like (id_member,id_coupon,tgl_like) values('$_SESSION[food_member]','$id','$sekarang')");
			$_SESSION['resto_notif']     = "suka";
		}
		else{
			$_SESSION['resto_notif']     = "suka_gagal";
		}
	}
	elseif($_GET['fungsi']=="3"){
		$ada_bookmark=mysqli_num_rows(mysqli_query($koneksi,"select id_bookmark from bookmark where id='$id' and jenis='coupon' and id_member='$_SESSION[food_member]'"));
		if($ada_bookmark==0){
			mysqli_query($koneksi,"insert into bookmark (id_member,jenis,id,tgl_bookmark) values('$_SESSION[food_member]','coupon','$id','$sekarang')");
			$_SESSION['resto_notif']     = "bookmark";
		}	
		else{
			$_SESSION['resto_notif']     = "bookmark_gagal";
		}
	}
}
else{
	$_SESSION['resto_notif']     = "login_dulu";
}
header("Location: ".$base_url."/pages/coupon/".$_GET['id']."/".$seo);
?>