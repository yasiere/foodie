<?php
session_start();
$auto_logout=1800000;
include "config/func/base_url.php";
if(!empty($_SESSION['food_member'])){
if (time()-$_SESSION['timestamp']>$auto_logout){
    session_destroy();
    session_unset();
	header("Location: ".$base_url."/auto-logout");
	exit();
}else{
    $_SESSION['timestamp']=time();
}
include "config/database/db.php";
include "config/func/seo.php";
include "config/func/jumlah_data.php";
include "config/func/member_data.php";
include "config/func/id_masking.php";
$id = id_masking($_GET['id']);
$active="coupon";
$type="coupon_detail";
mysqli_query($koneksi,"update coupon set dilihat=dilihat + 1 where id_coupon='$id'");
$resto=mysqli_query($koneksi,"select *,(select count(h.id_coupon) from coupon_like h where h.id_coupon=f.id_coupon) as dilike from coupon f where f.id_coupon='$id'");
$r=mysqli_fetch_array($resto);

$res=mysqli_query($koneksi,"select * from restaurant where id_restaurant='$r[id_restaurant]'");
$tam_res=mysqli_fetch_array($res);
$id_res = id_masking($tam_res['id_restaurant']);
$slug_res=seo($tam_res['restaurant_name']);

$slug=seo($r['nama_coupon']);
$post=date("jS M, Y", strtotime($r['tgl_post']));
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
  <title><?php echo"$r[nama_coupon]"; ?> - Foodie Guidances</title>
	<meta name="keywords" content="">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/lightbox.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/slick.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>

  <link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
 <body style="position: initial;" class="drawer drawer--right">
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>
    <div class="container hook">
		<div class="row">
			<div class="col-12 col-8a">
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Coupon</a></li>
					<li class="active"><?php echo"$r[nama_coupon]"; ?></li>
				</ol>
				<?php
					if(isset($_SESSION['resto_notif'])){
						if($_SESSION['resto_notif']=="suka"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully like this beverage.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="suka_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was liked this coupon before. You can only do one time in 24 hours.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="bookmark"){
							echo"<div class='alert alert-success alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Success!</strong> You have successfully bookmark this beverage.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="bookmark_gagal"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Failed!</strong> You was bookmark this beverage before. You can only do one time.
							</div>";
						}
						elseif($_SESSION['resto_notif']=="login_dulu"){
							echo"<div class='alert alert-danger alert-dismissible' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<strong>Must Login!</strong> You must login or register before doing this action.
							</div>";
						}
						unset($_SESSION['resto_notif']);
					}
				?>
				<a href="<?php echo"$base_url/pages/restaurant/info/$id_res/$slug_res"; ?>"><h1 class="judul"><?php echo"$r[nama_coupon]"; ?></h1></a>
				<div class="mb10 f-12"><em>by</em> Admin <span class="bullet">&#8226;</span> <em>post</em> <?php echo"$post"; ?></div>
				<div class="box-info-1">
					<div class="pull-left">
						<?php echo"<a href='$base_url/assets/img/coupon/big_$r[gambar_coupon]' data-lightbox='$r[nama_coupon]' data-title='$r[nama_coupon]'>
            <img src='$base_url/assets/img/coupon/medium_$r[gambar_coupon]' width='100%'></a>"; ?>
					</div>
					<div class="pull-right">
						<div class='member-coupon' style='text-align: -moz-center;'>
              <div style='width: 280px; text-align: center;'>
  							<?php
  							echo"<div class='bb'><a href='#'><img src='$base_url/assets/img/member/$u[gambar_thumb]' width='140'></a></div>
  							<div class='br'>
  								<strong class='db f-merah'>First Name</strong>
  								$u[nama_depan]
  								<strong class='db f-merah'>Last Name</strong>
  								$u[nama_belakang]
  								<strong class='db f-merah'>Country</strong>
  								Indonesia
  							</div>
  							<div class='clearfix'></div>";
  							?>
              </div>
						</div>
						<div class="detail-menu">
							<a href="<?php echo"$base_url/pages/coupon/event/$_GET[id]/1"; ?>" title="Like"><i class="fa fa-heart-o"></i></a>
							<a href="<?php echo"$base_url/pages/coupon/event/$_GET[id]/3"; ?>" title="Bookmark"><i class="fa fa-bookmark-o"></i></a>
							<span class="social-share"><i class="fa fa-share-alt"></i>
								<div class="social-box" style=" left: -90px;">
									<span class='st_facebook_large' displayText='Facebook'></span>
									<span class='st_linkedin_large' displayText='LinkedIn'></span>
									<span class='st_pinterest_large' displayText='Pinterest'></span>
									<span class='st_googleplus_large' displayText='Google +'></span>
									<span class='st_twitter_large' displayText='Tweet'></span>
									<span class='st_sharethis_large' displayText='ShareThis'></span>
								</div>
							</span>
						</div>
						<div class="statistik">
							<em>
								<i class="fa fa-heart"></i>
                        <a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">
									<?php	echo"$r[dilike] likes"; ?>
								</a>
								<?php
								if(!empty($_SESSION['food_member'])){
									$me=mysqli_query($koneksi,"select tgl_like from coupon_like where id_member='$id_member' and id_coupon='$id' order by id_coupon_like desc limit 1");
									$ada_melike=mysqli_num_rows($me);
									if($ada_melike<>0){
										$q=mysqli_fetch_array($me);
										$like_date=date("jS M, Y", strtotime($q['tgl_like']));
										echo" (you has liked on $like_date)";
									}
								}
								?>
							</em>
							<em><i class="fa fa-eye"></i> <?php echo"$r[dilihat]"; ?> views</em>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="box-info-2">
					<?php echo"$r[konten_coupon]"; ?>
				</div>
				<hr>
				<h5 class="f-merah">Other Coupon</h5>
				<div class="media">
					<div class="row">
						<?php
						$resto=mysqli_query($koneksi,"select *,(select count(h.id_coupon) from coupon_like h where h.id_coupon=f.id_coupon) as dilike from coupon f where f.id_negara='$r[id_negara]' and f.id_coupon <> '$id' order by rand() limit 4");
						$ada_resto=mysqli_num_rows($resto);
						if($ada_resto<>0){
							while($s=mysqli_fetch_array($resto)){
								$slug=seo($s['nama_coupon']);
								$post=date("jS M, Y", strtotime($s['tgl_post']));
								$id = id_masking($s['id_coupon']);
								echo"<div class='col-4'>
									<div class='thumb'>
										<a href='$base_url/pages/coupon/$id/$slug'><img src='$base_url/assets/img/coupon/small_$s[gambar_coupon]'></a>
									</div>
									<div class='info'>
										<em>posted on $post</em>
										<h3 class='sembunyi'><a href='$base_url/pages/coupon/$id/$slug'>$s[nama_coupon]</a></h3>
										<ul class='list-unstyled f-12'>
											<li>$s[dilike] Like <span class='bullet'>&#8226;</span> $s[dilihat] View</li>
										</ul>
									</div>
								</div>";
							}
						}
						else{
							echo"<div class='col-12'>There is no coupon with same country right now.</div>";
						}
						?>
					</div>
				</div>
				<hr>
				<div id="disqus_thread"></div>
			</div>
			<div class="col-4 mene-atas">
				<?php include"config/inc/search.php"; ?>
				<?php include"config/inc/iklan.php"; ?>
				<div class="fb-like-box" data-href="https://www.facebook.com/foodieguidances#_=_" data-width="220" data-height="350" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
			</div>
		</div>
    </div>
    <?php include"config/inc/footer.php"; ?>
    <?php
    	   $idu = id_masking($_GET['id']);
    			$pb=mysqli_query($koneksi, "select * from coupon where id_coupon < $idu ORDER BY id_coupon desc limit 1");
    	   $jumpb=mysqli_num_rows($pb);
    	   $tpb=mysqli_fetch_array($pb);
    	   if ($jumpb<>0) {
    			$ba = id_masking($tpb['id_coupon']);
    			$slug=seo($tpb['nama_coupon']);
    			$next="$base_url/pages/coupon/$ba/$slug";
    	   } else {
    			$pba=mysqli_query($koneksi, "select * from coupon ORDER BY id_coupon desc limit 1");
    			$jumpba=mysqli_num_rows($pba);
    			$tpba=mysqli_fetch_array($pba);
    			$baa = id_masking($tpba['id_coupon']);
    			$sluga=seo($tpba['nama_coupon']);
    			$next="$base_url/pages/coupon/$baa/$sluga";
    	   }

    	   $pn=mysqli_query($koneksi, "select * from coupon where id_coupon > $idu ORDER BY id_coupon limit 1");
    	   $jumpn=mysqli_num_rows($pn);
    	   $tpn=mysqli_fetch_array($pn);
    	   if ($jumpn<>0) {
    			$na = id_masking($tpn['id_coupon']);
    			$slug=seo($tpn['nama_coupon']);
    			$back="$base_url/pages/coupon/$na/$slug";
    	   } else {
    			$pna=mysqli_query($koneksi, "select * from coupon ORDER BY id_coupon limit 1");
    			$jumpna=mysqli_num_rows($pna);
    			$tpna=mysqli_fetch_array($pna);
    			$naa = id_masking($tpna['id_coupon']);
    			$sluga=seo($tpna['nama_coupon']);
    			$back="$base_url/pages/coupon/$naa/$sluga";
    	   }
    	 ?>
    	 <a class="scrollToNext" href="<?php echo $back ?>" style="bottom: 320px;"><img src="<?php echo"$base_url/assets/img/theme/kiri.png" ?>"></a>
    	 <a class="scrollToNext" href="<?php echo $next ?>" style="bottom: 273px;"><img src="<?php echo"$base_url/assets/img/theme/kanan.png" ?>"></a>
    	 <a href="#" class="scrollToTop"><img src="<?php echo"$base_url/assets/img/theme/atas.png" ?>"></a>

	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.superslides.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap-rating.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/lightbox.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/slick.min.js"></script>
  <script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
  <script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
  <script>
    $(document).ready(function() {
      $('.drawer').drawer({});

    $('.tutup').click(function(){
      $('.iklanan').toggleClass('hilang', 500);
    });

    $('.cart').click(function(e){
      e.stopPropagation();
      $('.dis-cart').toggle();
      $('.dis-plus').hide();
      $('.dis-share').hide();
    });
    $('.plus').click(function(e){
      e.stopPropagation();
      $('.dis-plus').toggle();
      $('.dis-cart').hide();
      $('.dis-share').hide();
    });
    $('.share').click(function(e){
      e.stopPropagation();
      $('.dis-share').toggle();
      $('.dis-plus').hide();
      $('.dis-cart').hide();
    });
    $('.drawer-toggle').click(function(){
      $('.dis-cart').hide();
      $('.dis-plus').hide();
      $('.dis-share').hide();
    });
    $(document).click(function () {
       var $el = $(".dis-share");
       if ($el.is(":visible")) {
          $el.fadeOut(200);
       }
       var $ela = $(".dis-plus");
       if ($ela.is(":visible")) {
          $ela.fadeOut(200);
       }
       var $elu = $(".dis-cart");
       if ($elu.is(":visible")) {
          $elu.fadeOut(200);
       }
     });

    });
  </script>
	<?php if(!empty($_SESSION['food_member'])){ ?>
  <script src="<?php echo"$base_url"; ?>/assets/js/theme.js"></script>
<?php } ?>
<script type="text/javascript">
  $(document).ready(function () {
    $(document).on("contextmenu",function(e){
      if(e.target.nodeName != "INPUT" && e.target.nodeName != "TEXTAREA")
      e.preventDefault();
    });
    $.fn.disableTextSelect = function() {
      return this.each(function() {
        $(this).css({
          'MozUserSelect':'none',
          'webkitUserSelect':'none'
        }).attr('unselectable','on').bind('selectstart', function() {
          return false;
        });
      });
    };
    $('body').disableTextSelect();
  });
</script>
	<script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>
   <script type="text/javascript">
  $(document).ready(function() {
     $(window).scroll(function(){
          if ($(this).scrollTop() > 100) {
             $('.scrollToTop').fadeIn();
          } else {
             $('.scrollToTop').fadeOut();
          }
      });

      $(window).scroll(function(){
         if ($(this).scrollTop() > 50) {
            $('.scrollToNext').fadeIn();
         } else {
            $('.scrollToNext').fadeOut();
         }
      });

        $('#go-to-vote').click(function() {
           $('html, body').animate({
              scrollTop: $( $(this).attr('href') ).offset().top
           }, 500);
           $('#tab-vote a[href="#feature"]').tab('show');
           return false;
        });
  });
  </script>
	<script type="text/javascript">stLight.options({publisher: "7b97d330-d7b9-49c3-b5ee-b3aaa89bbe66", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=348272391978609&version=v2.0";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
	<script type="text/javascript">
		$(document).ready(function () {
			$(".alert").fadeTo(5000, 500).fadeOut(500, function(){
				$(".alert").alert('close');
			});
			setInterval(function(){
				$('.bb').toggleClass('gloss');
            }, 2000);
			$('.social-share').click( function(event){
				event.stopPropagation();
				$( ".social-box" ).toggle(1);
			});
			$('.carousel').carousel({
				interval: 10000
			});
			$(document).click( function(){
				$('.social-box').hide();
			});
		});
		/* * * CONFIGURATION VARIABLES * * */
		var disqus_shortname = 'foodieguidancescom';

		/* * * DON'T EDIT BELOW THIS LINE * * */
		(function() {
			var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
			dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
			(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
		})();
	</script>
   <style media="screen">
   .black_overlay{
      display: none;
      position: absolute;
      top: 0%;
      left: 0%;
      width: 100%;
      height: 100%;
      background-color: black;
      z-index:1001;
      -moz-opacity: 0.8;
      opacity:.80;
      filter: alpha(opacity=80);
   }
   .white_content {
      display: none;
      position: fixed;
      top: 20%;
      left: 40%;
      width: 20%;
      height: 56%;
      padding: 24px;
      border: 5px solid #C00606;
      background-color: #FFF;
      z-index: 1002;
      overflow-y: auto;
      overflow-x: hidden;
   }
   </style>
   <div id="light" class="white_content">
      <h3 style="font-size: 20px; margin: 0px 0px 15px;">Like</h3>
      <table class="table">
         <?php
            $idu=id_masking($_GET['id']);
            $me=mysqli_query($koneksi,"SELECT * FROM coupon_like r left join member m on m.id_member=r.id_member where r.id_coupon='$idu'");
            $no=1;
            $juma=mysqli_num_rows($me);
            if (!empty($juma)) {
               while ($tame=mysqli_fetch_array($me)) {
                  $like_date=date("jS M, Y", strtotime($tame['tgl_like']));
                     echo "<tr><td>$no.</td>
                              <td><img src='$base_url/assets/img/member/$tame[gambar_thumb]' class='foto_profil' width='30px'></td> <td>$tame[username]</td> <td>$like_date</td></tr>";
                  $no++;
               }
            }
            else {
               echo "Empty";
            }
         ?>
      </table>
      <a style="position: absolute; border: 6px solid rgb(255, 255, 255); right: -6px; top: -6px; z-index: 999999999; background: #C00606; color: rgb(255, 255, 255); padding: 6px 10px;" href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'"><i class="fa fa-close"></i></a>
   </div>
   <div id="lighta" class="white_content">
      <h3 style="font-size: 20px; margin: 0px 0px 15px;">Recommendation</h3>
      <table class="table">
         <?php
            $idu=id_masking($_GET['id']);
            $me=mysqli_query($koneksi,"SELECT * FROM recipe_rekomendasi r left join member m on m.id_member=r.id_member where r.id_recipe='2'");
            $no=1;
            $juma=mysqli_num_rows($me);
            if (!empty($juma)) {
               while ($tame=mysqli_fetch_array($me)) {
                  $like_date=date("jS M, Y", strtotime($tame['tgl_rekomendasi']));
                     echo "<tr><td>$no.</td>
                              <td><img src='$base_url/assets/img/member/$tame[gambar_thumb]' class='foto_profil' width='30px'></td> <td>$tame[username]</td> <td>$like_date</td></tr>";
                  $no++;
               }
            }
            else {
               echo "Empty";
            }
         ?>
      </table>
      <a style="position: absolute; border: 6px solid rgb(255, 255, 255); right: -6px; top: -6px; z-index: 999999999; background: #C00606; color: rgb(255, 255, 255); padding: 6px 10px;" href = "javascript:void(0)" onclick = "document.getElementById('lighta').style.display='none';document.getElementById('fadea').style.display='none'"><i class="fa fa-close"></i></a>
   </div>
   <div id="fade" class="black_overlay"  onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'"></div>
   <div id="fadea" class="black_overlay"  onclick = "document.getElementById('lighta').style.display='none';document.getElementById('fadea').style.display='none'"></div>
</body>
</html>
<?php
}
else{
	header("Location: ".$base_url."/login-area");
}
?>
