<?php error_reporting(0);
session_start();
$auto_logout=1800000;
include "config/func/base_url.php";
if(!empty($_SESSION['food_member'])){
if (time()-$_SESSION['timestamp']>$auto_logout){
    session_destroy();
    session_unset();
	header("Location: ".$base_url."/auto-logout");
	exit();
}else{
    $_SESSION['timestamp']=time();
}
include "config/database/db.php";
include "config/func/member_data.php";
$active = "events";
$type = "events";
$id=$_GET['id'];

$resto=mysqli_query($koneksi,"SELECT *,d.id_kota,r.id_negara as negara, r.id_propinsi as propinsi, r.id_kota as kota,(SELECT COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS jumlah_vote,(SELECT COUNT(f.id_member) FROM restaurant_like f WHERE f.id_restaurant=r.id_restaurant) AS jumlah_like,(SELECT COUNT(f.id_member) FROM restaurant_rekomendasi f WHERE f.id_restaurant=r.id_restaurant) AS jumlah_rekomendasi,(SELECT SUM(f.cleanlines)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_cleanlines,(SELECT SUM(f.customer_services)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_customer,(SELECT SUM(f.food_beverage)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_food,(SELECT SUM(f.comfort)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_comfort,(SELECT SUM(f.value_money)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_money,(SELECT (SUM(f.cleanlines) + SUM(f.customer_services) + SUM(f.food_beverage) + SUM(f.comfort) + SUM(f.value_money)) / COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant =r.id_restaurant) AS total_rate FROM restaurant r LEFT JOIN type_of_business a ON r.id_type_of_business = a.id_type_of_business LEFT JOIN negara b ON r.id_negara = b.id_negara LEFT JOIN propinsi c ON r.id_propinsi = c.id_propinsi LEFT JOIN kota d ON r.id_kota = d.id_kota LEFT JOIN landmark e ON r.id_landmark = e.id_landmark LEFT JOIN mall f ON r.id_mall = f.id_mall LEFT JOIN operation_hour g ON r.id_operation_hour = g.id_operation_hour LEFT JOIN price_index i ON r.id_price_index = i.id_price_index LEFT JOIN suitable_for j ON r.id_suitable_for = j.id_suitable_for LEFT JOIN serving_time m ON r.id_serving_time = m.id_serving_time LEFT JOIN type_of_service n ON r.id_type_of_service = n.id_type_of_service LEFT JOIN wifi p ON r.id_wifi = p.id_wifi LEFT JOIN term_of_payment q ON r.id_term_of_payment = q.id_term_of_payment LEFT JOIN premise_security s ON r.id_premise_security = s.id_premise_security LEFT JOIN premise_fire_safety t ON r.id_premise_fire_safety = t.id_premise_fire_safety LEFT JOIN premise_maintenance u ON r.id_premise_maintenance = u.id_premise_maintenance LEFT JOIN parking_spaces v ON r.id_parking_spaces = v.id_parking_spaces LEFT JOIN ambience w ON r.id_ambience = w.id_ambience LEFT JOIN attire x ON r.id_attire = x.id_attire LEFT JOIN clean_washroom y ON r.id_clean_washroom = y.id_clean_washroom LEFT JOIN tables_availability z ON r.id_tables_availability = z.id_tables_availability LEFT JOIN noise_level ON r.id_noise_level = noise_level.id_noise_level LEFT JOIN waiter_tipping ON r.id_waiter_tipping = waiter_tipping.id_waiter_tipping LEFT JOIN member ON r.id_member = member.id_member LEFT JOIN air_conditioning ON r.id_air_conditioning = air_conditioning.id_air_conditioning LEFT JOIN heating_system ON r.id_heating_system = heating_system.id_heating_system LEFT JOIN premise_hygiene ON r.id_premise_hygiene = premise_hygiene.id_premise_hygiene where r.id_restaurant='$id'");
$e=mysqli_fetch_array($resto);
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
  <title>Submit Events - Foodie Guidances</title>
	<meta name="keywords" content="">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/jquery-ui.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/select2.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/awesome-bootstrap-checkbox.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap-timepicker.css" rel="stylesheet">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style type="text/css">
  	span.hapus_menu.btn.btn-danger {
	    top: -34px;
	    display: inline;
	    position: relative;
	    right: 0px;
	    float: right;
	    margin-bottom: -27px;
	}
	span.hapus_menuku.btn.btn-danger {
	    top: -34px;
	    display: inline;
	    position: relative;
	    right: 0px;
	    float: right;
	    margin-bottom: -27px;
	}
	span.hapus_menumu.btn.btn-danger {
	    top: -34px;
	    display: inline;
	    position: relative;
	    right: 0px;
	    float: right;
	    margin-bottom: -27px;
	}
  </style>
  <link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
 <body style="position: initial;" class="drawer drawer--right">
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>
   <div class="loadi">
      <img src="<?php echo "$base/assets/img/theme/load.gif"?>">
   </div>
    <div class="container hook">
		<div class="row">
			<div class="col-16">
				<ul class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Account</a></li>
					<li class="active">Submit Process</li>
				</ul>
				<?php
				if(isset($_SESSION['notif'])){
					if($_SESSION['notif']=="gambar"){
						echo"<div class='alert alert-danger alert-dismissible' role='alert'>
							<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
							<strong>Failed!</strong> Image used have to be in JPG, PNG, and GIF format and the maximum total images size is 50MB also minimal dimension must be 800x600 pixel.
						</div>";
					}
					unset($_SESSION['notif']);
				}
				?>
				<h3 class="f-merah mb10">Submit Events</h3>
				<p><span class="f-merah">*</span> required field</p>
				<form class="border-form form-horizontal" method="post" action="<?php echo"$base_url"; ?>/config/func/save_events.php" enctype="multipart/form-data">
               		<div class="row mb20">
	                  <div class="col-4 col-4b"><label class="control-label">Country <span class="f-merah">*</span></label></div>
	                  <div class="col-6 col-4b">
	                     <select name="negara" required class="form-control" id="negara">
	                        <option value="">Select Country</option>
	                        <?php
	                        $sql=mysqli_query($koneksi,"select * from negara order by nama_negara asc");
	                        while($b=mysqli_fetch_array($sql)){
	                           echo"<option value='$b[id_negara]'>$b[nama_negara]</option>";
	                        }
	                        ?>
	                     </select>
	                  </div>
	               </div>
	               <div class="row mb20">
	                  <div class="col-4 col-4b"><label class="control-label">State/ Province <span class="f-merah">*</span></label></div>
	                  <div class="col-6 col-4b">
	                     <select name="propinsi" required class="form-control" id="propinsi">
	                        <option value="">Select State/ Province</option>
	                     </select>
	                  </div>
	               </div>
	               <div class="row mb20">
	                  <div class="col-4 col-4b"><label class="control-label">City <span class="f-merah">*</span></label></div>
	                  <div class="col-6 col-4b">
	                     <select name="kota" required class="form-control" id="kota">
	                        <option value="">Select City</option>
	                     </select>
	                  </div>
	               </div>


               		<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Event Name <span class="f-merah">*</span></label></div>
						<div class="col-6 col-4b">
							<input type="text" class="form-control" placeholder="Write Name" id="food" autocomplete="off" name="nama_events" required>
						</div>
					</div>

					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Address of Event <span class="f-merah">*</span></label></div>
						<div class="col-6 col-4b">
							<input type="text" class="form-control" placeholder="Write Name" id="food" autocomplete="off" name="address_events" required>
						</div>
					</div>

					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Categories</label> <span class="f-merah">*</span></div>
						<div class="col-6 col-4b">
							<select name="categories" required class="form-control cat">
								<option value=''>Select Category</option>
								<?php
								$sql=mysqli_query($koneksi,"select * from events_category order by nama_events_category asc");
								while($g=mysqli_fetch_array($sql)){
									echo"<option value='$g[id_events_category]'>$g[nama_events_category]</option>";
								}
								?>
							</select>
						</div>
					</div>

					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Cover Photo</label></div>
						<div class="col-6 col-4b">
							<div class="fileinput fileinput-new" data-provides="fileinput">
							  <div class="fileinput-preview upload" data-trigger="fileinput"></div>
							  <input type="file" name="menu[]" class="hidden">
							</div>
							<p class="help-block">Image used have to be in JPG, PNG, and GIF format and the maximum total images size is 50MB also minimal dimension must be 800x600 pixel.</p>
						</div>
					</div>

					<div class="box-panel dn">
						<div class="col-4 col-4b"><label class="control-label"></label></div>
						<div class="menu_wrap">
							<div class="row mb20">
								<div class="col-17">
									<div class="fileinput fileinput-new" data-provides="fileinput">
										<div class="fileinput-preview upload" data-trigger="fileinput"></div>
										<input type="file" name="menu[]" class="hidden">
									</div>
								</div>
								<div class="col-4"><input type="text"  autocomplete="off" class="form-control mt30" placeholder="Photo Caption" name="menu_caption[]"></div>
							</div>
						</div>

						<button type="button" class="tambah_menu btn btn-success">Add More Photo</button>
					</div>		
					<br><br>		
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Price Index</label></div>
						<div class="col-6 col-4b">
							<select name="id_price_index" required class="form-control">
								<?php
								$sql=mysqli_query($koneksi,"select * from price_index order by nama_price_index asc");
								while($g=mysqli_fetch_array($sql)){
									echo"<option value='$g[id_price_index]'>$g[nama_price_index]</option>";
								}
								?>
							</select>
						</div>
					</div>

					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Periode</label></div>
						<div class="col-6 col-4b">
							<input type="text" class="form-control datepicker" placeholder="Periode Date ..." autocomplete="off" name="periode">
						</div>
					</div>

					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Open</label></div>
						<div class="col-6 col-4b">
							<input type="text" class="form-control time" placeholder="Open Time" name="events_time1">
						</div>
					</div>

					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Close</label></div>
						<div class="col-6 col-4b">
							<input type="text" class="form-control time" placeholder="Close Time" name="events_time2">
						</div>
					</div>
					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Tag </label></div>
						<div class="col-6 col-4b">
							<input type="text" class="form-control" placeholder="Write tag (separated by comma)" name="tag">
						</div>
					</div>

					<!-- <div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Kiosk</label></div>
						<div class="col-6 col-4b">
							<input type="text" class="form-control" placeholder="Kiosk" name="kiosk[]"><br>
							<div class="menu_wrap"></div>
						</div>
						
						<div class="tambah_menu btn btn-success">Add More Kiosk</div>
					</div>

					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Food</label></div>
						<div class="col-6 col-4b">
							<input type="text" class="form-control" placeholder="Food" name="food[]"><br>
							<div class="menu_wrapku"></div>
						</div>
						
						<div class="tambah_menuku btn btn-success">Add More Food</div>
					</div>

					<div class="row mb20">
						<div class="col-4 col-4b"><label class="control-label">Beverage</label></div>
						<div class="col-6 col-4b">
							<input type="text" class="form-control" placeholder="Beverage" name="beverage[]"><br>
							<div class="menu_wrapmu"></div>
						</div>
						
						<div class="tambah_menumu btn btn-success">Add More Beverage</div>
					</div> -->
					
					<div role="tabpanel" id="tab-vote">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#feature" aria-controls="feature" role="tab" data-toggle="tab">Feature Vote</a></li>
						<li role="presentation"><a href="#quick" aria-controls="quick" role="tab" data-toggle="tab">Quick Vote</a></li>
					</ul>
					<!-- Tab panes -->
					
					<div class="tab-content">	
						<div role="tabpanel" class="tab-pane tab-panel active fade in" id="feature">
							<input type="hidden" value="6" name="fungsi">
							<input type="hidden" value="info" name="page">
							<input type="hidden" value="<?php echo"$_GET[id]"; ?>" name="id">
							<table class="tablebintang pertama">
								<tr>
									<td>Security<i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assesment of the taste of the beverage which has unique personality."></i></td>
									<td><input type="hidden" class="rating" id="rat1" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="security"></td>
									<td rowspan="5" class="mene-atas">
										<div class='alert-danger alert-dismissible as' role='alert' >
											<strong>Please submit minimum one star for each rating categories in other to be rated</strong>
											</div>
									</td>
								</tr>
								<tr>
									<td>Clean<i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment on the beverage ingredient's freshness which can include fresh fruits used for the fruit juices beverage or fresh tea and coffe beans."></i></td>
									<td><input type="hidden" class="rating" id="rat2" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="clean"></td>
								</tr>
								<tr>
									<td>Comfort <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment on the beverage ingredient's freshness which can include fresh fruits used for the fruit juices beverage or fresh tea and coffe beans."></i></td>
									<td><input type="hidden" class="rating" id="rat3" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="comfort"></td>
								</tr>
								<tr>
									<td>Organise <i class="fa fa-info-circle tip" data-toggle="tooltip" data-placement="right" title="Overall assessment on the beverage ingredient's freshness which can include fresh fruits used for the fruit juices beverage or fresh tea and coffe beans."></i></td>
									<td><input type="hidden" class="rating" id="rat4" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="organise"></td>
								</tr>
								<tr class="total-vote">
									<td colspan="2">
										<button class="btn btn-danger" type="submit">Vote</button>
										<div class='alert-danger alert-dismissible as' role='alert'>
											<strong>Please submit minimum one star for each rating categories in other to be rated</strong>
										</div>
									</td>
								</tr>
							</table>
						</div>
						<div role="tabpanel" class="tab-pane tab-panel fade" id="quick">
							<input type="hidden" value="6" name="fungsi">
							<input type="hidden" value="info" name="page">
							<input type="hidden" value="<?php echo"$_GET[id]"; ?>" name="id">
							<table class="tablebintang">
								<tr>
									<td><strong>Give your vote</strong></td>
									<td><input type="hidden" class="rating" data-filled="fa fa-star fa-2x f-merah" data-empty="fa fa-star-o fa-2x" name="rating"></td>
									<td rowspan="2" class="mene-atas">
										<div class='alert-danger alert-dismissible as' role='alert'>
											<strong>Please submit minimum one star for each rating categories in other to be rated</strong>
											</div>
									</td>
								</tr>
								<tr class="total-vote">
									<td colspan="2">
										
									</td>
								</tr>
							</table>
						</div>
					</div>
					<ul class="list-inline">
						<li><i class="fa fa-star"></i> Poor</li>
						<li><i class="fa fa-star"></i><i class="fa fa-star"></i> Below Average</li>
						<li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Average</li>
						<li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Very Good</li>
						<li><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Excellent</li>
					</ul>
				</div>

					<button type="submit" class="btn btn-danger">Submit</button> <button type="reset" class="btn btn-danger">Reset</button>
               <div class="btn btn-danger as btn-as">Submit</div> <div class="btn btn-danger as btn-as">Reset</div><br><br>
               <div class='alert-danger alert-dismissible as' role='alert' style="display:none; padding: 15px">
                  <strong>Please submit minimum one star for each rating categories in other to be rated</strong>
               </div>
				</form>
			</div>
		</div>
    </div>
    <?php include"config/inc/footer.php"; ?>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery-ui.min.js"></script>

	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/select2.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jasny-bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap-rating.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap-timepicker.min.js"></script>
    <script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/moment.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/moment-precise-range.js"></script>
	<script>
		$(document).ready(function() {
			var rat1 = $("#rat1").val();
			var rat2 = $("#rat2").val();
			var rat3 = $("#rat3").val();
			var rat4 = $("#rat4").val();
			$("#rat1,#rat2,#rat3,#rat4").change(function(){
				if ((rat1 < 1) && (rat2 < 1) && (rat3 < 1) && (rat4 < 1)) {
				    $(".btn-danger").hide();
				    $(".as").show();
				}
			});

			$("#rat1,#rat2,#rat3,#rat4").change(function(){
			var rat1 = $("#rat1").val();
			var rat2 = $("#rat2").val();
			var rat3 = $("#rat3").val();
			var rat4 = $("#rat4").val();
				if ((rat1 > 0) && (rat2 > 0) && (rat3 > 0) && (rat4 > 0)) {
				    $(".btn-danger").show();
				    $(".as").hide();
				}
			});

		$('.drawer').drawer();

		 $('.tutup').click(function(){
			 $('.iklanan').toggleClass('hilang', 500);
		 });

		 $('.cart').click(function(e){
			 e.stopPropagation();
			 $('.dis-cart').toggle();
			 $('.dis-plus').hide();
			 $('.dis-share').hide();
		 });
		 $('.plus').click(function(e){
			 e.stopPropagation();
			 $('.dis-plus').toggle();
			 $('.dis-cart').hide();
			 $('.dis-share').hide();
		 });
		 $('.share').click(function(e){
			 e.stopPropagation();
			 $('.dis-share').toggle();
			 $('.dis-plus').hide();
			 $('.dis-cart').hide();
		 });
		 $('.drawer-toggle').click(function(){
			 $('.dis-cart').hide();
			 $('.dis-plus').hide();
			 $('.dis-share').hide();
		 });
		 $('.btn-cari').click(function(){
			 $('.search_kiri').toggleClass('search_kiri_tam', 500);
		 });
		 $(document).click(function () {
				var $el = $(".dis-share");
				if ($el.is(":visible")) {
					 $el.fadeOut(200);
				}
				var $ela = $(".dis-plus");
				if ($ela.is(":visible")) {
					 $ela.fadeOut(200);
				}
				var $elu = $(".dis-cart");
				if ($elu.is(":visible")) {
					 $elu.fadeOut(200);
				}
			});

		 });
	</script>
   <script type="text/javascript">
		$(document).ready(function () {
			$(document).on("contextmenu",function(e){
				if(e.target.nodeName != "INPUT" && e.target.nodeName != "TEXTAREA")
				e.preventDefault();
			});
			$.fn.disableTextSelect = function() {
				return this.each(function() {
					$(this).css({
						'MozUserSelect':'none',
						'webkitUserSelect':'none'
					}).attr('unselectable','on').bind('selectstart', function() {
						return false;
					});
				});
			};
			$('body').disableTextSelect();
		});
	</script>

	<script type="text/javascript">
		

	$(document).ready(function () {

		var x = 1;
		$(".tambah_menu").click(function(e){ //on add input button click
			e.preventDefault();
			if(x < 5){ //max input box allowed
				x++; //text box increment
				$(".menu_wrap").append('<div class="row mb20"><div class="col-4 col-4b"><label class="control-label"></label></div><div class="col-17"><div class="fileinput fileinput-new" data-provides="fileinput"> <div class="fileinput-preview upload" data-trigger="fileinput"></div><input type="file" name="menu[]" class="hidden"></div></div><div class="col-4"><input type="text" class="form-control mt15" placeholder="Photo Caption" name="menu_caption[]"></div><div class="col-1"><button type="button" class="hapus_menu btn btn-danger mt15">Remove</button></div></div>'); //add input box
			}
			if(x > 4){
				$(".tambah_menu").hide();
			}
		});

		$(".menu_wrap").on("click",".hapus_menu", function(e){ //user click on remove text
			e.preventDefault(); $(this).parent().parent('.row').remove(); x--;
			if($(".tambah_menu").is(":hidden")){
				$(".tambah_menu").show();
			}
		});


		$(".tambah_menuku").click(function(e){ //on add input button click
			e.preventDefault();
			if(x < 30){ //max input box allowed
				x++; //text box increment
				$(".menu_wrapku").append('<div class="asku"><input type="text" class="form-control" placeholder="Food" name="food[]"><span class="hapus_menuku btn btn-danger">X</span></div>'); //add input box
			}
			if(x > 29){
				$(".tambah_menuku").hide();
			}
		});
		$(".menu_wrapku").on("click",".hapus_menuku", function(e){ //user click on remove text
			$(this).parent('.asku').remove(); x--;
			if($(".tambah_menuku").is(":hidden")){
				$(".tambah_menuku").show();
			}
		});



		$(".tambah_menumu").click(function(e){ //on add input button click
			e.preventDefault();
			if(x < 30){ //max input box allowed
				x++; //text box increment
				$(".menu_wrapmu").append('<div class="asmu"><input type="text" class="form-control" placeholder="Beverage" name="beverage[]"><span class="hapus_menumu btn btn-danger">X</span></div>'); //add input box
			}
			if(x > 29){
				$(".tambah_menumu").hide();
			}
		});

		$(".menu_wrapmu").on("click",".hapus_menumu", function(e){ //user click on remove text
			$(this).parent('.asmu').remove(); x--;
			if($(".tambah_menumu").is(":hidden")){
				$(".tambah_menumu").show();
			}
		});


		$('.time').timepicker({
			defaultTime:''
		});
		function strstr(haystack, needle, bool) {  

		    var pos = 0;

		    haystack += '';
		    pos = haystack.toLowerCase().indexOf((needle + '').toLowerCase());
		    if (pos == -1) {
		        return false;
		    } else {
		        if (bool) {
		            return haystack.substr(0, pos);
		        } else {
		            return haystack.slice(pos);
		        }
		    }
		}
		
			
		$(".as").hide();
		$(".cat").change(function() {
			var y = $(this).val();
			var x = strstr(y, '##', true);

			

			if (x == 1) {
				$(".pertama").hide();
				$(".kedua").show();
					$(".btn-danger").show();
					$(".as").hide();
					$("#rata1,#rata2,#rata3,#rata4").change(function(){
						var rata1 = $("#rata1").val();
						var rata2 = $("#rata2").val();
						var rata3 = $("#rata3").val();
						var rata4 = $("#rata4").val();						
						if (rata1 > 0 && rata2 > 0 && rata3 > 0 && rata4 > 0 ) {
							$(".btn-danger").show();
							$(".as").hide();					
						}
						else{
							
							$(".btn-danger").hide();
								$(".as").show();
						}
					});

			}
			else{
				$( ".pertama" ).show();
				$( ".kedua" ).hide();
				$("#rat1,#rat2,#rat3").change(function(){
						var rat1 = $("#rat1").val();
						var rat2 = $("#rat2").val();
						var rat3 = $("#rat3").val();					
						if (rat1 > 0 && rat2 > 0 && rat3 > 0) {
							$(".btn-danger").show();
							$(".as").hide();					
						}
						else{
							
							$(".btn-danger").hide();
							$(".as").show();
						}
					});
				
			}
			// alert(x);
		});
			
		$( ".datepicker" ).change(function() {
			var m1 = moment('<?php echo date("Y-m-d") ?>','YYYY-MM-DD');
			var dateku = $(this).val();
			var m2 = moment(dateku,'YYYY-MM-DD');
			$( "#exp" ).val(moment.preciseDiff(m1, m2));
			// alert(moment.preciseDiff(m1, m2));

			// var m1 = moment('2014-01-01','YYYY-MM-DD');
			// var m2 = moment('2014-02-03','YYYY-MM-DD');
			// alert(moment.preciseDiff(m1, m2));
		});
		

			// alert(x);

			// $( ".datepicker" ).change(function() {
				
			// });
		
		    $( ".datepicker" ).datepicker({
		      changeMonth: true,
		      changeYear: true,
		      dateFormat: 'yy-mm-dd'
		    });
		 

      
      
      // $("#rat1,#rat2,#rat3,#rat4,#rat5,#rat6").change(function(){
      //    var rat1 = $("#rat1").val();
      //    var rat2 = $("#rat2").val();
      //    var rat3 = $("#rat3").val();
      //    var rat4 = $("#rat4").val();
      //    var rat5 = $("#rat5").val();
      //    var rat5 = $("#rat5").val();
      //    var rat6 = $("#rat6").val();
      //    if ((rat1 > 0) && (rat2 > 0) && (rat3 > 0) && (rat4 > 0) && (rat5 > 0) && (rat6 > 0)) {
            
      //    }
      // });

		$('.tip').tooltip();
		$("select").select2();
		$('.rating').rating();
		$('#search').autocomplete({
			source: "<?php echo "$base_url/config/func/ajax_resto_search.php"; ?>",
			minLength: 3
		});
		$(".alert").fadeTo(5000, 500).fadeOut(500, function(){
			$(".alert").alert('close');
		});

      $("#negara").change(function(){
			var id = $("#negara").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_propinsi.php",
				data: "id=" + id,
				success: function(data){
					$("#propinsi").html(data);
					$("#propinsi").fadeIn(2000);
				}
			});
		});
		$("#propinsi").change(function(){
			var id = $("#propinsi").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_kota.php",
				data: "id=" + id,
				success: function(data){
					$("#kota").html(data);
					$("#kota").fadeIn(2000);
				}
			});
		});

      $("#kota").change(function(){
			var id = $("#kota").val();
			$.ajax({
				type:"POST",
				url: "<?php echo"$base_url"; ?>/config/func/ajax_resto.php",
				data: "id=" + id,
				success: function(data){
					$("#search").html(data);
					$("#search").fadeIn(2000);
				}
			});
		});

      $(".box-head").click(function(){
         var targeta = $(this).children(".klik");
         $(targeta).toggleClass("fa-angle-up fa-angle-down");
			var target = $(this).parent().children(".box-panel");
			$(target).slideToggle();
		});

	});
	</script>
   <script type="text/javascript">
      // $("form").submit(function( event ) {
      // 	var kota = $("#kota").val();
      // 	var search_box = $("#search").val();
      // 	var food = $("#food").val();
      // 	var kat = $("#kat").val();
      //    var gam = $("#gam").val();
      //    if (!(kota === "") && !(search_box === "") && !(food === "") && !(kat === "") && !(gam === "")) {
      //       $(".loadi").show();
      //    }
      //    else {

      //    }
      // });
	</script>
</body>
</html>
<?php
}
else{
	header("Location: ".$base_url."/login-area");
}
?>
