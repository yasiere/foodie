<?php
  include "config/database/db.php";
  include "config/func/base_url.php";
  $id = $_POST['id'];
 ?>

<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">

<script type="text/javascript">
  $('.fitin div').css('font-size', '30px');
  while( $('.fitin div').height() > $('.fitin').height() ) {
      $('.fitin div').css('font-size', (parseInt($('.fitin div').css('font-size')) - 1) + "px" );
  };
  $('.restos div').css('font-size', '16px');
  while( $('.restos div').height() > $('.restos').height() ) {
      $('.restos div').css('font-size', (parseInt($('.restos div').css('font-size')) - 1) + "px" );
  };
</script>

<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap-ratingys.js"></script>
<script type="text/javascript">
  $(document).ready(function () {
     $('.ratingys').ratingys();
  });
</script>

<?php
$resto=mysqli_query($koneksi,"SELECT *,r.id_negara as kode_negara, d.id_kota,
  (SELECT COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS jumlah_vote,
  (SELECT COUNT(f.id_member) FROM restaurant_like f WHERE f.id_restaurant=r.id_restaurant) AS jumlah_like,
  (SELECT COUNT(f.id_member) FROM restaurant_rekomendasi f WHERE f.id_restaurant=r.id_restaurant) AS jumlah_rekomendasi,
  (SELECT SUM(f.cleanlines)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_cleanlines,
  (SELECT SUM(f.customer_services)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_customer,
  (SELECT SUM(f.food_beverage)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_food,
  (SELECT SUM(f.comfort)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_comfort,
  (SELECT SUM(f.value_money)/ COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=r.id_restaurant) AS t_money,
  (SELECT (SUM(f.cleanlines) + SUM(f.customer_services) + SUM(f.food_beverage) + SUM(f.comfort) + SUM(f.value_money)) / COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant =r.id_restaurant) AS total_rate FROM restaurant r
  LEFT JOIN type_of_business a ON r.id_type_of_business = a.id_type_of_business
  LEFT JOIN negara b ON r.id_negara = b.id_negara
  LEFT JOIN propinsi c ON r.id_propinsi = c.id_propinsi
  LEFT JOIN kota d ON r.id_kota = d.id_kota
  LEFT JOIN landmark e ON r.id_landmark = e.id_landmark
  LEFT JOIN mall f ON r.id_mall = f.id_mall
  LEFT JOIN member ON r.id_member = member.id_member
  where r.id_restaurant='$id'");
$r=mysqli_fetch_array($resto);
$total_rating=number_format((float)$r['total_rate'], 2, '.', '');
$jumv=mysqli_query($koneksi, "SELECT COUNT(f.id_member) FROM restaurant_rating f WHERE f.id_restaurant=$r[id_restaurant] and YEAR(tgl_restaurant_rating) = $_POST[tahun]");
$jv=mysqli_num_rows($jumv);
 ?>
  <?php
    $pr=mysqli_fetch_array(mysqli_query($koneksi, "SELECT *
      FROM (
        SELECT @curRank := @curRank + 1 AS rank
           , b.id_restaurant
           , b.restaurant_name
           , b.jumlah
           , b.rank_vote
           , b.rank_like
           , b.id_negara
      FROM restaurant b
      CROSS JOIN (SELECT @curRank := 0) vars
      ORDER BY jumlah desc, rank_vote DESC, rank_like DESC, restaurant_name ASC
    ) p WHERE p.id_restaurant=$id"));
    $rank_global=$pr['rank'];
  ?>
  <?php
    $pr=mysqli_fetch_array(mysqli_query($koneksi, "SELECT *
      FROM (
        SELECT @curRank := @curRank + 1 AS rank
             , b.id_restaurant
             , b.restaurant_name
             , b.jumlah
             , b.rank_vote
             , b.rank_like
             , b.id_negara
        FROM restaurant b
        CROSS JOIN (SELECT @curRank := 0) vars WHERE b.id_negara=$r[kode_negara]
        ORDER BY jumlah desc, rank_vote DESC, rank_like DESC, restaurant_name ASC
      ) p WHERE p.id_restaurant=$id"));
    $rank_negara=$pr['rank'];
  ?>
  <?php
    $pr=mysqli_fetch_array(mysqli_query($koneksi, "SELECT *
        FROM (
        SELECT @curRank := @curRank + 1 AS rank
             , b.id_restaurant
             , b.restaurant_name
             , b.jumlah
             , b.rank_vote
             , b.rank_like
             , b.id_propinsi
        FROM restaurant b
        CROSS JOIN (SELECT @curRank := 0) vars WHERE b.id_propinsi=$r[id_propinsi]
        ORDER BY jumlah desc, rank_vote DESC, rank_like DESC, restaurant_name ASC
      ) p WHERE p.id_restaurant=$id"));
    $rank_state=$pr['rank'];
  ?>
  <?php
    $pr=mysqli_fetch_array(mysqli_query($koneksi, "SELECT *
      FROM (
        SELECT @curRank := @curRank + 1 AS rank
             , b.id_restaurant
             , b.restaurant_name
             , b.jumlah
             , b.rank_vote
             , b.rank_like
             , b.id_kota
        FROM restaurant b
        CROSS JOIN (SELECT @curRank := 0) vars WHERE b.id_kota=$r[id_kota]
        ORDER BY jumlah desc, rank_vote DESC, rank_like DESC, restaurant_name ASC
      ) p WHERE p.id_restaurant=$id"));
    $rank_city=$pr['rank'];
  ?>
  <?php
    if ($r['id_mall']<>0) {
      $pr=mysqli_fetch_array(mysqli_query($koneksi, "SELECT *
          FROM (
            SELECT @curRank := @curRank + 1 AS rank
                 , b.id_restaurant
                 , b.restaurant_name
                 , b.jumlah
                 , b.rank_vote
                 , b.rank_like
                 , b.id_mall
            FROM restaurant b
            CROSS JOIN (SELECT @curRank := 0) vars WHERE b.id_mall=$r[id_mall]
            ORDER BY jumlah desc, rank_vote DESC, rank_like DESC, restaurant_name ASC
          ) p WHERE p.id_restaurant=$id"));
        $rank_mall=$pr['rank'];
    }
    else {
      $rank_mall="N.A.";
    }
  ?>
<?php
$da=date("Y");
if ($da==$_POST['tahun']) {
?>
  <div class="piagam-isi ta2017">
    <div class="fitin">
      <div class="h1"><?php echo "$r[restaurant_name]"; ?></div>
    </div>
    <div class="restos">
      <div class="alamat"><?php echo "$r[street_address]"; ?></div>
    </div>
    <div class="negara"><?php echo $r['nama_negara']; ?></div>
    <div class="bintang"><?php echo "<input type='hidden' class='ratingys' data-filled='fa fa-star' data-empty='fa fa-star-o' readonly='readonly' value='$total_rating'>"; ?></div>
    <div class="rating"><?php echo "$total_rating / <em class='f-12'> $r[jumlah_vote] votes</em>"; ?></div>
    <span class="world"># <span style="  color: #ed1c24;"><?php echo "$rank_global"; ?></span></span>
    <span class="ranknegara"># <span style="  color: #ed1c24;"><?php echo "$rank_negara"; ?></span></span>
    <span class="rankstate"># <span style="  color: #ed1c24;"><?php echo "$rank_state"; ?></span></span>
    <span class="rankcity"># <span style="  color: #ed1c24;"><?php echo "$rank_city"; ?></span></span>
    <span class="rankmall"># <span style="  color: #ed1c24;"><?php echo "$rank_mall"; ?></span></span>
    <span class="tahun"><span><?php echo date("Y"); ?></span></span>
    <span class="tahuna"><span><?php echo date("Y"); ?></span></span>
  </div>
  <img src="<?php echo"$base_url";?>/assets/img/theme/certificate.jpg" alt="" width="100%">

<?php } else{ ?>

<div class="piagam-isi ta2016">
  <?php
    $ra=mysqli_query($koneksi,"select * from rank_resto WHERE YEAR(dates) = $_POST[tahun] and id_restaurant=$_POST[id]");
    $ru=mysqli_fetch_array($ra);
   ?>
  <div class="fitin">
    <div class="h1"><?php echo "$r[restaurant_name]"; ?></div>
  </div>
  <div class="restos">
    <div class="alamat"><?php echo "$r[street_address]"; ?></div>
  </div>
  <div class="negara"><?php echo $r['nama_negara']; ?></div>
  <div class="bintang"><?php echo "<input type='hidden' class='ratingys' data-filled='fa fa-star' data-empty='fa fa-star-o' readonly='readonly' value='$ru[jumlah]'>"; ?></div>
  <div class="rating"><?php echo "$ru[jumlah] / <em class='f-12'> $jv votes</em>"; ?></div>
  <span class="world"># <span style="  color: #ed1c24;"><?php echo "$ru[global]"; ?></span></span>
  <span class="ranknegara"># <span style="  color: #ed1c24;"><?php echo "$ru[negara]"; ?></span></span>
  <span class="rankstate"># <span style="  color: #ed1c24;"><?php echo "$ru[propinsi]"; ?></span></span>
  <span class="rankcity"># <span style="  color: #ed1c24;"><?php echo "$ru[kota]"; ?></span></span>
  <span class="rankmall"># <span style="  color: #ed1c24;"><?php echo "$ru[mall]"; ?></span></span>
  <span class="tahun"><span><?php echo $_POST['tahun']; ?></span></span>
  <span class="tahuna"><span><?php echo $_POST['tahun']; ?></span></span>
</div>
<img src="<?php echo"$base_url";?>/assets/img/theme/certificate.jpg" alt="" width="100%">
<?php } ?>
