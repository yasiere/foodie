<?php
session_start();
include "config/func/base_url.php";
include "config/database/db.php";
include "config/func/seo.php";	
include "config/func/jumlah_data.php";	
$auto_logout=600;
if(!empty($_SESSION['food_member'])){
	if (time()-$_SESSION['timestamp']>$auto_logout){
		session_destroy();
		session_unset();
		header("Location: ".$base_url."/auto-logout");
		exit();
	}else{
		$_SESSION['timestamp']=time();
	}			
	include "config/func/member_data.php";
}
$active = "term";
?>
<!DOCTYPE html>
<html lang="en" class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
    <title>Term of Services - Foodie Guidances</title>
	<meta name="keywords" content="">
    <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo"$base_url"; ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo"$base_url"; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo"$base_url"; ?>/assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-translate-customization" content="e3ecc502510fe6cc-7ac2c2eba727aa38-g50acbd1f74c94289-13"></meta>
	<link href="<?php echo"$base_url"; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/font.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/superslides.css" rel="stylesheet">
	<link href="<?php echo"$base_url"; ?>/assets/css/theme.css" rel="stylesheet">
	<script src="<?php echo"$base_url"; ?>/assets/js/prefixfree.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/modernizr.custom.js"></script>
  <link href="<?php echo"$base_url"; ?>/assets/css/drawer.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body style="position: initial;" class="drawer drawer--right">
    <!-- Fixed navbar -->
    <?php include"config/inc/header.php"; ?>
	<div class="pencarian">
		<div id="slides">
			<div class="slides-container">
				<img src="<?php echo"$base_url"; ?>/assets/img/theme/about.jpg" alt="foodie guidances">
			</div>
		</div>
	</div>
    <div class="container">
		<h1 class="f-merah">Term of Services</h1>
		<p><strong>Welcome to our website</strong></p>
		<p>Foodieguidances.com (collectively as “website”) is operated by Foodie Guidances Company (collectively as “we”, “our”, “us”), a partnership company registered in Kuching, Sarawak, Malaysia.  Within our website, we have various pages such as Restaurant, Food, Beverage, Recipe, FGMart, Coupons, Articles and Video where the context “page” is meant as a sub website within the main website.</p> 
		<p>All foodies need to read these Terms and Conditions carefully, understand them and agree upon before joining us and accessing foodieguidances.com. Any participation, such as accessing and using of the website or our mobile version of the website, whether via desktop, laptop, Smart TV or mobile smart phone for browsing or viewing this website, or submitting your contents, you AGREE to be bound by these Terms and Conditions:</p>
		<ol>
			<li>Members <strong>AGREE</strong> to keep their log-in credentials safe and secure, and not to disclose them to any third party.</li> 
			<li>Members <strong>WILL NOT</strong> accept any paid reviews and ratings on Restaurant, Food and Beverage within our website. We strictly condemn this unlawful act and we <strong>WILL NOT ACCEPT</strong> any kind of paid reviews and ratings on our website.</li>  
			<li>Members <strong>AGREE</strong> to review and rate the restaurants, food, beverage, etc., in a way which is FAIR and <strong>PROFESSIONAL</strong>. We have several rating categories on Restaurant, food, and beverage. Members need to rate it fairly. For example, if the service in the restaurant was poor, but the food was excellent, members should not rate the food as poor just because you were not satisfied with their service.</li>  
			<li>Members <strong>WILL GIVE</strong> an honest review and ratings without bias towards restaurant, food and beverage reviews and ratings on our website.</li> 
			<li>Members <strong>AGREE</strong> not to copy, download, and print the contents of the website, whether it is for business or personal profit. Failure to comply will lead to infringement of our Copyright and legal proceeding will be considered by our management.</li> 
			<li>Members <strong>WILL NOT</strong> upload indecent photos and non-related photos to our website.</li> 
			<li>Members <strong>WILL NOT</strong> use bad language towards other members, editors, or when writing reviews and write-ups within our website. Those posts will be deleted, without any prior notice.</li> 
			<li>Members <strong>AGREE</strong> not to sell their membership, as foodieguidances.com’s membership <strong>IS NOT FOR SALE</strong>.</li> 
			<li>Members who failed to comply with our Terms of Use listed above will be <strong>SUSPENDED</strong> without prior notice.</li> 
		</ol>
		<p>Members need to know that accessing the foodieguidances.com’s website and our social media sites (listed below) are totally different entities.</p>
		<p>a. Facebook’s Page (www.facebook.com/foodieguidances),<br>
		b. Facebook’s Groups (www.facebook.com/groups/foodieguidances),<br>
		c. Google + (www.google.com/)<br>
		d. Twitter (www.twitter.com/foodieguidances),<br>
		e. Instagram (www.instagram.com/foodieguidances_com),<br>
		f. Pinterest (www.pinterest.com/foodieguidances).</p>
		<p>Members need to separately agree upon their respective Terms of Use, Disclaimer, and Privacy Policy, whichever applied.</p>
		<p>Please, keep in mind that we reserve the rights to change this Terms of Use at any time without prior notice. If you AGREE with our Terms of Use, you are certainly more than WELCOME YOU TO JOIN US and hopefully you will enjoy the benefits of the service we provide.</p> 
		<h1 class="f-merah">Termination</h1>
		<p>We reserve the rights to terminate the membership of a member for foodieguidances.com if he or she failed to comply with our Term of Use listed above.  Once the member decided to quit, or being suspended, we will delete the entire personal data and profile photo from our website at any time, without prior notice to its members. The only exception is the name will remain for those reviews posted previously.</p>
		<p><i>Members have to agree with Term of Use. If you do not agree with these terms and conditions, please do not use this website. If you continue to use the foodieguidances.com website, you agree to be bound by these Terms and Conditions.</i></p>
		<h1 class="f-merah">Contact</h1>
		<p>If you have any questions about Terms of Use, feel free to contact us at: cs@foodieguidances.com</p>
	</div>
    <?php include"config/inc/footer.php"; ?>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/jquery.superslides.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/theme.js"></script>
  <script src="<?php echo"$base_url"; ?>/assets/js/iscroll.min.js"></script>
	<script src="<?php echo"$base_url"; ?>/assets/js/drawer.min.js" charset="utf-8"></script>
	<script>
		 $(document).ready(function() {
			 $('.drawer').drawer();

		 $('.tutup').click(function(){
			 $('.iklanan').toggleClass('hilang', 500);
		 });

		 $('.cart').click(function(e){
			 e.stopPropagation();
			 $('.dis-cart').toggle();
			 $('.dis-plus').hide();
			 $('.dis-share').hide();
		 });
		 $('.plus').click(function(e){
			 e.stopPropagation();
			 $('.dis-plus').toggle();
			 $('.dis-cart').hide();
			 $('.dis-share').hide();
		 });
		 $('.share').click(function(e){
			 e.stopPropagation();
			 $('.dis-share').toggle();
			 $('.dis-plus').hide();
			 $('.dis-cart').hide();
		 });
		 $('.drawer-toggle').click(function(){
			 $('.dis-cart').hide();
			 $('.dis-plus').hide();
			 $('.dis-share').hide();
		 });
		 $('.btn-caris').click(function(){
			 $('.search_kiri').toggleClass('search_kiri_tam', 500);
		 });
		 $(document).click(function () {
				var $el = $(".dis-share");
				if ($el.is(":visible")) {
					 $el.fadeOut(200);
				}
				var $ela = $(".dis-plus");
				if ($ela.is(":visible")) {
					 $ela.fadeOut(200);
				}
				var $elu = $(".dis-cart");
				if ($elu.is(":visible")) {
					 $elu.fadeOut(200);
				}
			});

		 });
	</script>
	<div id="fb-root"></div>
	<script>
	$(document).ready(function () {
		$('#slides').superslides({
			animation: 'fade',
			inherit_height_from: '.pencarian',
			play: 8000
		});
	});
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=348272391978609&version=v2.0";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
</body>
</html>