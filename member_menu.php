	

	<ul class="list-unstyled member-social">
		<?php
		if($u['social_fb']==""){
			echo"<li><img src='$base_url/assets/img/theme/fb_29_off.png'> Facebook</li>";
		}
		else{
			echo"<li><img src='$base_url/assets/img/theme/fb_29.png'> <a href='$u[social_fb]' target='_blank'>Facebook</a></li>";
		}
		if($u['social_twitter']==""){
			echo"<li><img src='$base_url/assets/img/theme/tw_29_off.png'> Twitter</li>";
		}
		else{
			echo"<li><img src='$base_url/assets/img/theme/tw_29.png'> <a href='$u[social_twitter]' target='_blank'>Twitter</a></li>";
		}
		if($u['social_pint']==""){
			echo"<li><img src='$base_url/assets/img/theme/pin_29_off.png'> Pinterest</li>";
		}
		else{
			echo"<li><img src='$base_url/assets/img/theme/pin_29.png'> <a href='$u[social_pint]' target='_blank'>Pinterest</a></li>";
		}
		if($u['social_google']==""){
			echo"<li><img src='$base_url/assets/img/theme/gog_29_off.png'> Google+</li>";
		}
		else{
			echo"<li><img src='$base_url/assets/img/theme/gog_29.png'> <a href='$u[social_google]' target='_blank'>Google+</a></li>";
		}
		if($u['social_insta']==""){
			echo"<li><img src='$base_url/assets/img/theme/insta_29_off.png'> Instagram</li>";
		}
		else{
			echo"<li><img src='$base_url/assets/img/theme/insta_29.png'> <a href='$u[social_insta]' target='_blank'>Instagram</a></li>";
		}
		?>
	</ul>
	<hr class="mtb10">
	<ul class="list-unstyled">
		<li><a href="<?php echo"$base_url/$u[username]/activity-feed"; ?>" <?php if ($menu == 'activity') { echo "class='f-merah'";}?>>Activity Feed</a></li>
   		<?php if($session_member==$_GET['id']){ ?>
		    <li><span class="buka">
				Account <div class="pull-right">+</div>
				<ul class="pl50 sub-menu">
					<li><a href="<?php echo"$base_url/$u[username]/edit-profile"; ?>" <?php if ($menu == 'edit-profile') { echo "class='f-merah'";}?>>Edit Profile</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/change-password"; ?>" <?php if ($menu == 'change-password') { echo "class='f-merah'";}?>>Change Password</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/activity-setting"; ?>" <?php if ($menu == 'activity-setting') { echo "class='f-merah'";}?>>Activity Feed Setting</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/social-media"; ?>" <?php if ($menu == 'social-media') { echo "class='f-merah'";}?>>Social Media</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/delete-account"; ?>" <?php if ($menu == 'delete-account') { echo "class='f-merah'";}?>>Delete Account</a></li>
				</ul>
				</span>
			</li>
			<li><span class="buka">
				Submit <div class="pull-right">+</div>
				<ul class="pl50 sub-menu">
					<li><a href="<?php echo"$base_url/$u[username]/my-restaurant/new"; ?>">Restaurant</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-food/new"; ?>">Food</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-beverage/new"; ?>">Beverage</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-recipe/new"; ?>">Recipe</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-article/new"; ?>">Article</a></li>
					<li><a href="<?php echo"$base_url/$u[username]/my-video/new"; ?>">Video</a></li>
				</ul>
				</span>
			</li>
		<?php } ?>
		<li><a href="<?php echo"$base_url/$u[username]/my-restaurant"; ?>" <?php if ($menu == 'restaurant') { echo "class='f-merah'";}?>>My Restaurant</a></li>
		<li><a href="<?php echo"$base_url/$u[username]/my-restaurant-photo"; ?>" <?php if ($menu == 'restaurant-photo') { echo "class='f-merah'";}?>>My Restaurant Photo</a></li>
		<li><a href="<?php echo"$base_url/$u[username]/my-restaurant-menu"; ?>" <?php if ($menu == 'restaurant-menu') { echo "class='f-merah'";}?>>My Restaurant Menu</a></li>
		<li><a href="<?php echo"$base_url/$u[username]/my-food"; ?>" <?php if ($menu == 'food') { echo "class='f-merah'";}?>>My Food</a></li>
		<li><a href="<?php echo"$base_url/$u[username]/my-food-photo"; ?>" <?php if ($menu == 'food-photo') { echo "class='f-merah'";}?>>My Food Photo</a></li>
		<li><a href="<?php echo"$base_url/$u[username]/my-beverage"; ?>" <?php if ($menu == 'beverage') { echo "class='f-merah'";}?>>My Beverage</a></li>
		<li><a href="<?php echo"$base_url/$u[username]/my-beverage-photo"; ?>" <?php if ($menu == 'beverage-photo') { echo "class='f-merah'";}?>>My Beverage Photo</a></li>
		<li><a href="<?php echo"$base_url/$u[username]/my-process"; ?>" <?php if ($menu == 'process') { echo "class='f-merah'";}?>>My Process F&B</a></li>
		<li><a href="<?php echo"$base_url/$u[username]/my-recipe"; ?>" <?php if ($menu == 'recipe') { echo "class='f-merah'";}?>>My Recipe</a></li>
		<li><a href="<?php echo"$base_url/$u[username]/my-recipe-photo"; ?>" <?php if ($menu == 'recipe-photo') { echo "class='f-merah'";}?>>My Recipe Photo</a></li>
		<li><a href="<?php echo"$base_url/$u[username]/my-article"; ?>" <?php if ($menu == 'article') { echo "class='f-merah'";}?>>My Article</a></li>
		<li><a href="<?php echo"$base_url/$u[username]/my-article-photo"; ?>" <?php if ($menu == 'article-photo') { echo "class='f-merah'";}?>>My Article Photo</a></li>
		<li><a href="<?php echo"$base_url/$u[username]/my-video"; ?>" <?php if ($menu == 'video') { echo "class='f-merah'";}?>>My Video</a></li>
		<li><a href="<?php echo"$base_url/$u[username]/my-bookmark"; ?>" <?php if ($menu == 'bookmark') { echo "class='f-merah'";}?>>Bookmark</a></li>
		<li><a href="<?php echo"$base_url/$u[username]/my-vote"; ?>" <?php if ($menu == 'vote') { echo "class='f-merah'";}?>>Give Vote to</a></li>
	</ul>
	<hr class="mtb10">
	<!-- <strong>FGMart</strong>
	<ul class="list-unstyled">
		<li><a href="<?php echo"$base_url/$u[username]/stock-cart"; ?>">Stock Cart</a></li>
		<li><a href="<?php echo"$base_url/$u[username]/download-link"; ?>">Download Link</a></li>
		<li><a href="<?php echo"$base_url/$u[username]/order-history"; ?>">Order History</a></li>
	</ul> -->
